processTreeMapDashJson (  {"id":"Clover database Sun Oct 13 2013 01:03:26 PDT0","name":"","data":{
    "$area":20800.0,"$color":66.74519,"title":
    " 20800 Elements, 66.7% Coverage"},"children":[{"id":
      "org.apache.zookeeper1496","name":"org.apache.zookeeper","data":{
        "$area":3726.0,"$color":69.00161,"title":
        "org.apache.zookeeper 3726 Elements, 69% Coverage"},"children":[]},{
      "id":"org.apache.zookeeper.jmx11814","name":
      "org.apache.zookeeper.jmx","data":{"$area":125.0,"$color":87.2,"title":
        "org.apache.zookeeper.jmx 125 Elements, 87.2% Coverage"},"children":[]},
    {"id":"org.apache.zookeeper.server.quorum.flexible5977","name":
      "org.apache.zookeeper.server.quorum.flexible","data":{"$area":293.0,
        "$color":78.157,"title":
        "org.apache.zookeeper.server.quorum.flexible 293 Elements, 78.2% Coverage"},
      "children":[]},{"id":"org.apache.zookeeper.cli1856","name":
      "org.apache.zookeeper.cli","data":{"$area":770.0,"$color":37.662334,
        "title":"org.apache.zookeeper.cli 770 Elements, 37.7% Coverage"},
      "children":[]},{"id":"org.apache.zookeeper.server.quorum818","name":
      "org.apache.zookeeper.server.quorum","data":{"$area":5783.0,"$color":
        73.4221,"title":
        "org.apache.zookeeper.server.quorum 5783 Elements, 73.4% Coverage"},
      "children":[]},{"id":"org.apache.zookeeper.server.auth1478","name":
      "org.apache.zookeeper.server.auth","data":{"$area":524.0,"$color":
        52.862595,"title":
        "org.apache.zookeeper.server.auth 524 Elements, 52.9% Coverage"},
      "children":[]},{"id":"org.apache.zookeeper.server.util1782","name":
      "org.apache.zookeeper.server.util","data":{"$area":247.0,"$color":
        62.348183,"title":
        "org.apache.zookeeper.server.util 247 Elements, 62.3% Coverage"},
      "children":[]},{"id":"org.apache.zookeeper.server0","name":
      "org.apache.zookeeper.server","data":{"$area":6201.0,"$color":
        78.906624,"title":
        "org.apache.zookeeper.server 6201 Elements, 78.9% Coverage"},
      "children":[]},{"id":"org.apache.zookeeper.server.persistence6291",
      "name":"org.apache.zookeeper.server.persistence","data":{"$area":
        686.0,"$color":76.96793,"title":
        "org.apache.zookeeper.server.persistence 686 Elements, 77% Coverage"},
      "children":[]},{"id":"org.apache.zookeeper.common1549","name":
      "org.apache.zookeeper.common","data":{"$area":311.0,"$color":
        76.84888,"title":
        "org.apache.zookeeper.common 311 Elements, 76.8% Coverage"},
      "children":[]},{"id":"org.apache.zookeeper.client4354","name":
      "org.apache.zookeeper.client","data":{"$area":501.0,"$color":
        69.26148,"title":
        "org.apache.zookeeper.client 501 Elements, 69.3% Coverage"},
      "children":[]},{"id":"org.apache.jute.compiler.generated19167","name":
      "org.apache.jute.compiler.generated","data":{"$area":1633.0,"$color":
        0.0,"title":
        "org.apache.jute.compiler.generated 1633 Elements, 0% Coverage"},
      "children":[]}]}

 ); 