processTreeMapJson (  {"id":"Clover database Sun Oct 13 2013 01:03:26 PDT0","name":"","data":{
    "$area":20800.0,"$color":66.74519,"title":
    " 20800 Elements, 66.7% Coverage"},"children":[{"id":
      "org.apache.zookeeper1496","name":"org.apache.zookeeper","data":{
        "$area":3726.0,"$color":69.00161,"title":
        "org.apache.zookeeper 3726 Elements, 69% Coverage"},"children":[{
          "id":"Version1496","name":"Version","data":{"$area":35.0,"$color":
            62.857143,"path":"org/apache/zookeeper/Version.html#Version",
            "title":"Version 35 Elements, 62.9% Coverage"},"children":[]},{
          "id":"Transaction1531","name":"Transaction","data":{"$area":18.0,
            "$color":100.0,"path":
            "org/apache/zookeeper/Transaction.html#Transaction","title":
            "Transaction 18 Elements, 100% Coverage"},"children":[]},{"id":
          "MultiResponse1870","name":"MultiResponse","data":{"$area":105.0,
            "$color":74.28571,"path":
            "org/apache/zookeeper/MultiResponse.html#MultiResponse","title":
            "MultiResponse 105 Elements, 74.3% Coverage"},"children":[]},{
          "id":"ZooKeeperMain3683","name":"ZooKeeperMain","data":{"$area":
            335.0,"$color":48.955223,"path":
            "org/apache/zookeeper/ZooKeeperMain.html#ZooKeeperMain","title":
            "ZooKeeperMain 335 Elements, 49% Coverage"},"children":[]},{"id":
          "ZooKeeperMain.MyWatcher3718","name":"ZooKeeperMain.MyWatcher",
          "data":{"$area":6.0,"$color":0.0,"path":
            "org/apache/zookeeper/ZooKeeperMain.html#ZooKeeperMain.MyWatcher",
            "title":"ZooKeeperMain.MyWatcher 6 Elements, 0% Coverage"},
          "children":[]},{"id":"ZooKeeperMain.MyCommandOptions3724","name":
          "ZooKeeperMain.MyCommandOptions","data":{"$area":63.0,"$color":
            84.12698,"path":
            "org/apache/zookeeper/ZooKeeperMain.html#ZooKeeperMain.MyCommandOptions",
            "title":
            "ZooKeeperMain.MyCommandOptions 63 Elements, 84.1% Coverage"},
          "children":[]},{"id":"ClientCnxn6532","name":"ClientCnxn","data":{
            "$area":143.0,"$color":93.00699,"path":
            "org/apache/zookeeper/ClientCnxn.html#ClientCnxn","title":
            "ClientCnxn 143 Elements, 93% Coverage"},"children":[]},{"id":
          "ClientCnxn.AuthData6538","name":"ClientCnxn.AuthData","data":{
            "$area":3.0,"$color":100.0,"path":
            "org/apache/zookeeper/ClientCnxn.html#ClientCnxn.AuthData",
            "title":"ClientCnxn.AuthData 3 Elements, 100% Coverage"},
          "children":[]},{"id":"ClientCnxn.Packet6553","name":
          "ClientCnxn.Packet","data":{"$area":42.0,"$color":73.809525,"path":
            "org/apache/zookeeper/ClientCnxn.html#ClientCnxn.Packet","title":
            "ClientCnxn.Packet 42 Elements, 73.8% Coverage"},"children":[]},{
          "id":"ClientCnxn.WatcherSetEventPair6619","name":
          "ClientCnxn.WatcherSetEventPair","data":{"$area":3.0,"$color":
            100.0,"path":
            "org/apache/zookeeper/ClientCnxn.html#ClientCnxn.WatcherSetEventPair",
            "title":
            "ClientCnxn.WatcherSetEventPair 3 Elements, 100% Coverage"},
          "children":[]},{"id":"ClientCnxn.EventThread6625","name":
          "ClientCnxn.EventThread","data":{"$area":174.0,"$color":
            89.655174,"path":
            "org/apache/zookeeper/ClientCnxn.html#ClientCnxn.EventThread",
            "title":"ClientCnxn.EventThread 174 Elements, 89.7% Coverage"},
          "children":[]},{"id":"ClientCnxn.EndOfStreamException6829","name":
          "ClientCnxn.EndOfStreamException","data":{"$area":4.0,"$color":
            50.0,"path":
            "org/apache/zookeeper/ClientCnxn.html#ClientCnxn.EndOfStreamException",
            "title":
            "ClientCnxn.EndOfStreamException 4 Elements, 50% Coverage"},
          "children":[]},{"id":"ClientCnxn.SessionTimeoutException6833",
          "name":"ClientCnxn.SessionTimeoutException","data":{"$area":2.0,
            "$color":100.0,"path":
            "org/apache/zookeeper/ClientCnxn.html#ClientCnxn.SessionTimeoutException",
            "title":
            "ClientCnxn.SessionTimeoutException 2 Elements, 100% Coverage"},
          "children":[]},{"id":"ClientCnxn.SessionExpiredException6835",
          "name":"ClientCnxn.SessionExpiredException","data":{"$area":2.0,
            "$color":100.0,"path":
            "org/apache/zookeeper/ClientCnxn.html#ClientCnxn.SessionExpiredException",
            "title":
            "ClientCnxn.SessionExpiredException 2 Elements, 100% Coverage"},
          "children":[]},{"id":"ClientCnxn.RWServerFoundException6837",
          "name":"ClientCnxn.RWServerFoundException","data":{"$area":2.0,
            "$color":100.0,"path":
            "org/apache/zookeeper/ClientCnxn.html#ClientCnxn.RWServerFoundException",
            "title":
            "ClientCnxn.RWServerFoundException 2 Elements, 100% Coverage"},
          "children":[]},{"id":"ClientCnxn.SendThread6839","name":
          "ClientCnxn.SendThread","data":{"$area":406.0,"$color":91.871925,
            "path":
            "org/apache/zookeeper/ClientCnxn.html#ClientCnxn.SendThread",
            "title":"ClientCnxn.SendThread 406 Elements, 91.9% Coverage"},
          "children":[]},{"id":"Op7470","name":"Op","data":{"$area":17.0,
            "$color":100.0,"path":"org/apache/zookeeper/Op.html#Op","title":
            "Op 17 Elements, 100% Coverage"},"children":[]},{"id":
          "Op.Create7487","name":"Op.Create","data":{"$area":42.0,"$color":
            76.190475,"path":"org/apache/zookeeper/Op.html#Op.Create",
            "title":"Op.Create 42 Elements, 76.2% Coverage"},"children":[]},{
          "id":"Op.Delete7529","name":"Op.Delete","data":{"$area":20.0,
            "$color":80.0,"path":"org/apache/zookeeper/Op.html#Op.Delete",
            "title":"Op.Delete 20 Elements, 80% Coverage"},"children":[]},{
          "id":"Op.SetData7549","name":"Op.SetData","data":{"$area":21.0,
            "$color":80.952385,"path":
            "org/apache/zookeeper/Op.html#Op.SetData","title":
            "Op.SetData 21 Elements, 81% Coverage"},"children":[]},{"id":
          "Op.Check7570","name":"Op.Check","data":{"$area":20.0,"$color":
            80.0,"path":"org/apache/zookeeper/Op.html#Op.Check","title":
            "Op.Check 20 Elements, 80% Coverage"},"children":[]},{"id":
          "MultiTransactionRecord7710","name":"MultiTransactionRecord",
          "data":{"$area":104.0,"$color":76.92308,"path":
            "org/apache/zookeeper/MultiTransactionRecord.html#MultiTransactionRecord",
            "title":"MultiTransactionRecord 104 Elements, 76.9% Coverage"},
          "children":[]},{"id":"KeeperException8430","name":
          "KeeperException","data":{"$area":142.0,"$color":54.929577,"path":
            "org/apache/zookeeper/KeeperException.html#KeeperException",
            "title":"KeeperException 142 Elements, 54.9% Coverage"},
          "children":[]},{"id":"KeeperException.CodeDeprecated8495","name":
          "KeeperException.CodeDeprecated","data":{"$area":0.0,"$color":
            -100.0,"path":
            "org/apache/zookeeper/KeeperException.html#KeeperException.CodeDeprecated",
            "title":
            "KeeperException.CodeDeprecated 0 Elements,  -  Coverage"},
          "children":[]},{"id":"KeeperException.Code8495","name":
          "KeeperException.Code","data":{"$area":9.0,"$color":100.0,"path":
            "org/apache/zookeeper/KeeperException.html#KeeperException.Code",
            "title":"KeeperException.Code 9 Elements, 100% Coverage"},
          "children":[]},{"id":"KeeperException.APIErrorException8581",
          "name":"KeeperException.APIErrorException","data":{"$area":2.0,
            "$color":0.0,"path":
            "org/apache/zookeeper/KeeperException.html#KeeperException.APIErrorException",
            "title":
            "KeeperException.APIErrorException 2 Elements, 0% Coverage"},
          "children":[]},{"id":"KeeperException.AuthFailedException8583",
          "name":"KeeperException.AuthFailedException","data":{"$area":2.0,
            "$color":100.0,"path":
            "org/apache/zookeeper/KeeperException.html#KeeperException.AuthFailedException",
            "title":
            "KeeperException.AuthFailedException 2 Elements, 100% Coverage"},
          "children":[]},{"id":"KeeperException.BadArgumentsException8585",
          "name":"KeeperException.BadArgumentsException","data":{"$area":
            4.0,"$color":100.0,"path":
            "org/apache/zookeeper/KeeperException.html#KeeperException.BadArgumentsException",
            "title":
            "KeeperException.BadArgumentsException 4 Elements, 100% Coverage"},
          "children":[]},{"id":"KeeperException.BadVersionException8589",
          "name":"KeeperException.BadVersionException","data":{"$area":4.0,
            "$color":100.0,"path":
            "org/apache/zookeeper/KeeperException.html#KeeperException.BadVersionException",
            "title":
            "KeeperException.BadVersionException 4 Elements, 100% Coverage"},
          "children":[]},{"id":
          "KeeperException.ConnectionLossException8593","name":
          "KeeperException.ConnectionLossException","data":{"$area":2.0,
            "$color":100.0,"path":
            "org/apache/zookeeper/KeeperException.html#KeeperException.ConnectionLossException",
            "title":
            "KeeperException.ConnectionLossException 2 Elements, 100% Coverage"},
          "children":[]},{"id":
          "KeeperException.DataInconsistencyException8595","name":
          "KeeperException.DataInconsistencyException","data":{"$area":2.0,
            "$color":0.0,"path":
            "org/apache/zookeeper/KeeperException.html#KeeperException.DataInconsistencyException",
            "title":
            "KeeperException.DataInconsistencyException 2 Elements, 0% Coverage"},
          "children":[]},{"id":"KeeperException.InvalidACLException8597",
          "name":"KeeperException.InvalidACLException","data":{"$area":4.0,
            "$color":100.0,"path":
            "org/apache/zookeeper/KeeperException.html#KeeperException.InvalidACLException",
            "title":
            "KeeperException.InvalidACLException 4 Elements, 100% Coverage"},
          "children":[]},{"id":
          "KeeperException.InvalidCallbackException8601","name":
          "KeeperException.InvalidCallbackException","data":{"$area":2.0,
            "$color":0.0,"path":
            "org/apache/zookeeper/KeeperException.html#KeeperException.InvalidCallbackException",
            "title":
            "KeeperException.InvalidCallbackException 2 Elements, 0% Coverage"},
          "children":[]},{"id":
          "KeeperException.MarshallingErrorException8603","name":
          "KeeperException.MarshallingErrorException","data":{"$area":2.0,
            "$color":0.0,"path":
            "org/apache/zookeeper/KeeperException.html#KeeperException.MarshallingErrorException",
            "title":
            "KeeperException.MarshallingErrorException 2 Elements, 0% Coverage"},
          "children":[]},{"id":"KeeperException.NoAuthException8605","name":
          "KeeperException.NoAuthException","data":{"$area":2.0,"$color":
            100.0,"path":
            "org/apache/zookeeper/KeeperException.html#KeeperException.NoAuthException",
            "title":
            "KeeperException.NoAuthException 2 Elements, 100% Coverage"},
          "children":[]},{"id":"KeeperException.NewConfigNoQuorum8607",
          "name":"KeeperException.NewConfigNoQuorum","data":{"$area":2.0,
            "$color":100.0,"path":
            "org/apache/zookeeper/KeeperException.html#KeeperException.NewConfigNoQuorum",
            "title":
            "KeeperException.NewConfigNoQuorum 2 Elements, 100% Coverage"},
          "children":[]},{"id":"KeeperException.ReconfigInProgress8609",
          "name":"KeeperException.ReconfigInProgress","data":{"$area":2.0,
            "$color":0.0,"path":
            "org/apache/zookeeper/KeeperException.html#KeeperException.ReconfigInProgress",
            "title":
            "KeeperException.ReconfigInProgress 2 Elements, 0% Coverage"},
          "children":[]},{"id":
          "KeeperException.NoChildrenForEphemeralsException8611","name":
          "KeeperException.NoChildrenForEphemeralsException","data":{"$area":
            4.0,"$color":0.0,"path":
            "org/apache/zookeeper/KeeperException.html#KeeperException.NoChildrenForEphemeralsException",
            "title":
            "KeeperException.NoChildrenForEphemeralsException 4 Elements, 0% Coverage"},
          "children":[]},{"id":"KeeperException.NodeExistsException8615",
          "name":"KeeperException.NodeExistsException","data":{"$area":4.0,
            "$color":100.0,"path":
            "org/apache/zookeeper/KeeperException.html#KeeperException.NodeExistsException",
            "title":
            "KeeperException.NodeExistsException 4 Elements, 100% Coverage"},
          "children":[]},{"id":"KeeperException.NoNodeException8619","name":
          "KeeperException.NoNodeException","data":{"$area":4.0,"$color":
            100.0,"path":
            "org/apache/zookeeper/KeeperException.html#KeeperException.NoNodeException",
            "title":
            "KeeperException.NoNodeException 4 Elements, 100% Coverage"},
          "children":[]},{"id":"KeeperException.NotEmptyException8623",
          "name":"KeeperException.NotEmptyException","data":{"$area":4.0,
            "$color":100.0,"path":
            "org/apache/zookeeper/KeeperException.html#KeeperException.NotEmptyException",
            "title":
            "KeeperException.NotEmptyException 4 Elements, 100% Coverage"},
          "children":[]},{"id":
          "KeeperException.OperationTimeoutException8627","name":
          "KeeperException.OperationTimeoutException","data":{"$area":2.0,
            "$color":0.0,"path":
            "org/apache/zookeeper/KeeperException.html#KeeperException.OperationTimeoutException",
            "title":
            "KeeperException.OperationTimeoutException 2 Elements, 0% Coverage"},
          "children":[]},{"id":
          "KeeperException.RuntimeInconsistencyException8629","name":
          "KeeperException.RuntimeInconsistencyException","data":{"$area":
            2.0,"$color":0.0,"path":
            "org/apache/zookeeper/KeeperException.html#KeeperException.RuntimeInconsistencyException",
            "title":
            "KeeperException.RuntimeInconsistencyException 2 Elements, 0% Coverage"},
          "children":[]},{"id":
          "KeeperException.SessionExpiredException8631","name":
          "KeeperException.SessionExpiredException","data":{"$area":2.0,
            "$color":100.0,"path":
            "org/apache/zookeeper/KeeperException.html#KeeperException.SessionExpiredException",
            "title":
            "KeeperException.SessionExpiredException 2 Elements, 100% Coverage"},
          "children":[]},{"id":
          "KeeperException.UnknownSessionException8633","name":
          "KeeperException.UnknownSessionException","data":{"$area":2.0,
            "$color":100.0,"path":
            "org/apache/zookeeper/KeeperException.html#KeeperException.UnknownSessionException",
            "title":
            "KeeperException.UnknownSessionException 2 Elements, 100% Coverage"},
          "children":[]},{"id":"KeeperException.SessionMovedException8635",
          "name":"KeeperException.SessionMovedException","data":{"$area":
            2.0,"$color":0.0,"path":
            "org/apache/zookeeper/KeeperException.html#KeeperException.SessionMovedException",
            "title":
            "KeeperException.SessionMovedException 2 Elements, 0% Coverage"},
          "children":[]},{"id":"KeeperException.NotReadOnlyException8637",
          "name":"KeeperException.NotReadOnlyException","data":{"$area":
            2.0,"$color":100.0,"path":
            "org/apache/zookeeper/KeeperException.html#KeeperException.NotReadOnlyException",
            "title":
            "KeeperException.NotReadOnlyException 2 Elements, 100% Coverage"},
          "children":[]},{"id":
          "KeeperException.EphemeralOnLocalSessionException8639","name":
          "KeeperException.EphemeralOnLocalSessionException","data":{"$area":
            2.0,"$color":100.0,"path":
            "org/apache/zookeeper/KeeperException.html#KeeperException.EphemeralOnLocalSessionException",
            "title":
            "KeeperException.EphemeralOnLocalSessionException 2 Elements, 100% Coverage"},
          "children":[]},{"id":"KeeperException.SystemErrorException8641",
          "name":"KeeperException.SystemErrorException","data":{"$area":
            2.0,"$color":0.0,"path":
            "org/apache/zookeeper/KeeperException.html#KeeperException.SystemErrorException",
            "title":
            "KeeperException.SystemErrorException 2 Elements, 0% Coverage"},
          "children":[]},{"id":
          "KeeperException.UnimplementedException8643","name":
          "KeeperException.UnimplementedException","data":{"$area":2.0,
            "$color":100.0,"path":
            "org/apache/zookeeper/KeeperException.html#KeeperException.UnimplementedException",
            "title":
            "KeeperException.UnimplementedException 2 Elements, 100% Coverage"},
          "children":[]},{"id":"JLineZNodeCompleter8789","name":
          "JLineZNodeCompleter","data":{"$area":44.0,"$color":0.0,"path":
            "org/apache/zookeeper/JLineZNodeCompleter.html#JLineZNodeCompleter",
            "title":"JLineZNodeCompleter 44 Elements, 0% Coverage"},
          "children":[]},{"id":"AsyncCallback8833","name":"AsyncCallback",
          "data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/zookeeper/AsyncCallback.html#AsyncCallback","title":
            "AsyncCallback 0 Elements,  -  Coverage"},"children":[]},{"id":
          "AsyncCallback.StatCallback8833","name":
          "AsyncCallback.StatCallback","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/zookeeper/AsyncCallback.html#AsyncCallback.StatCallback",
            "title":"AsyncCallback.StatCallback 0 Elements,  -  Coverage"},
          "children":[]},{"id":"AsyncCallback.DataCallback8833","name":
          "AsyncCallback.DataCallback","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/zookeeper/AsyncCallback.html#AsyncCallback.DataCallback",
            "title":"AsyncCallback.DataCallback 0 Elements,  -  Coverage"},
          "children":[]},{"id":"AsyncCallback.ACLCallback8833","name":
          "AsyncCallback.ACLCallback","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/zookeeper/AsyncCallback.html#AsyncCallback.ACLCallback",
            "title":"AsyncCallback.ACLCallback 0 Elements,  -  Coverage"},
          "children":[]},{"id":"AsyncCallback.ChildrenCallback8833","name":
          "AsyncCallback.ChildrenCallback","data":{"$area":0.0,"$color":
            -100.0,"path":
            "org/apache/zookeeper/AsyncCallback.html#AsyncCallback.ChildrenCallback",
            "title":
            "AsyncCallback.ChildrenCallback 0 Elements,  -  Coverage"},
          "children":[]},{"id":"AsyncCallback.Children2Callback8833","name":
          "AsyncCallback.Children2Callback","data":{"$area":0.0,"$color":
            -100.0,"path":
            "org/apache/zookeeper/AsyncCallback.html#AsyncCallback.Children2Callback",
            "title":
            "AsyncCallback.Children2Callback 0 Elements,  -  Coverage"},
          "children":[]},{"id":"AsyncCallback.Create2Callback8833","name":
          "AsyncCallback.Create2Callback","data":{"$area":0.0,"$color":
            -100.0,"path":
            "org/apache/zookeeper/AsyncCallback.html#AsyncCallback.Create2Callback",
            "title":
            "AsyncCallback.Create2Callback 0 Elements,  -  Coverage"},
          "children":[]},{"id":"AsyncCallback.StringCallback8833","name":
          "AsyncCallback.StringCallback","data":{"$area":0.0,"$color":
            -100.0,"path":
            "org/apache/zookeeper/AsyncCallback.html#AsyncCallback.StringCallback",
            "title":
            "AsyncCallback.StringCallback 0 Elements,  -  Coverage"},
          "children":[]},{"id":"AsyncCallback.VoidCallback8833","name":
          "AsyncCallback.VoidCallback","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/zookeeper/AsyncCallback.html#AsyncCallback.VoidCallback",
            "title":"AsyncCallback.VoidCallback 0 Elements,  -  Coverage"},
          "children":[]},{"id":"AsyncCallback.MultiCallback8833","name":
          "AsyncCallback.MultiCallback","data":{"$area":0.0,"$color":
            -100.0,"path":
            "org/apache/zookeeper/AsyncCallback.html#AsyncCallback.MultiCallback",
            "title":"AsyncCallback.MultiCallback 0 Elements,  -  Coverage"},
          "children":[]},{"id":"ClientWatchManager9035","name":
          "ClientWatchManager","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/zookeeper/ClientWatchManager.html#ClientWatchManager",
            "title":"ClientWatchManager 0 Elements,  -  Coverage"},
          "children":[]},{"id":"Login9754","name":"Login","data":{"$area":
            208.0,"$color":15.865384,"path":
            "org/apache/zookeeper/Login.html#Login","title":
            "Login 208 Elements, 15.9% Coverage"},"children":[]},{"id":
          "ClientCnxnSocketNIO9988","name":"ClientCnxnSocketNIO","data":{
            "$area":254.0,"$color":85.433075,"path":
            "org/apache/zookeeper/ClientCnxnSocketNIO.html#ClientCnxnSocketNIO",
            "title":"ClientCnxnSocketNIO 254 Elements, 85.4% Coverage"},
          "children":[]},{"id":"ZooKeeper10341","name":"ZooKeeper","data":{
            "$area":649.0,"$color":87.67334,"path":
            "org/apache/zookeeper/ZooKeeper.html#ZooKeeper","title":
            "ZooKeeper 649 Elements, 87.7% Coverage"},"children":[]},{"id":
          "ZooKeeper.ZKWatchManager10368","name":
          "ZooKeeper.ZKWatchManager","data":{"$area":62.0,"$color":
            88.70968,"path":
            "org/apache/zookeeper/ZooKeeper.html#ZooKeeper.ZKWatchManager",
            "title":"ZooKeeper.ZKWatchManager 62 Elements, 88.7% Coverage"},
          "children":[]},{"id":"ZooKeeper.WatchRegistration10430","name":
          "ZooKeeper.WatchRegistration","data":{"$area":18.0,"$color":
            100.0,"path":
            "org/apache/zookeeper/ZooKeeper.html#ZooKeeper.WatchRegistration",
            "title":
            "ZooKeeper.WatchRegistration 18 Elements, 100% Coverage"},
          "children":[]},{"id":"ZooKeeper.ExistsWatchRegistration10448",
          "name":"ZooKeeper.ExistsWatchRegistration","data":{"$area":8.0,
            "$color":100.0,"path":
            "org/apache/zookeeper/ZooKeeper.html#ZooKeeper.ExistsWatchRegistration",
            "title":
            "ZooKeeper.ExistsWatchRegistration 8 Elements, 100% Coverage"},
          "children":[]},{"id":"ZooKeeper.DataWatchRegistration10456","name":
          "ZooKeeper.DataWatchRegistration","data":{"$area":4.0,"$color":
            100.0,"path":
            "org/apache/zookeeper/ZooKeeper.html#ZooKeeper.DataWatchRegistration",
            "title":
            "ZooKeeper.DataWatchRegistration 4 Elements, 100% Coverage"},
          "children":[]},{"id":"ZooKeeper.ChildWatchRegistration10460",
          "name":"ZooKeeper.ChildWatchRegistration","data":{"$area":4.0,
            "$color":100.0,"path":
            "org/apache/zookeeper/ZooKeeper.html#ZooKeeper.ChildWatchRegistration",
            "title":
            "ZooKeeper.ChildWatchRegistration 4 Elements, 100% Coverage"},
          "children":[]},{"id":"ZooKeeper.States10464","name":
          "ZooKeeper.States","data":{"$area":4.0,"$color":100.0,"path":
            "org/apache/zookeeper/ZooKeeper.html#ZooKeeper.States","title":
            "ZooKeeper.States 4 Elements, 100% Coverage"},"children":[]},{
          "id":"ZKUtil11142","name":"ZKUtil","data":{"$area":35.0,"$color":
            100.0,"path":"org/apache/zookeeper/ZKUtil.html#ZKUtil","title":
            "ZKUtil 35 Elements, 100% Coverage"},"children":[]},{"id":
          "Quotas14494","name":"Quotas","data":{"$area":4.0,"$color":100.0,
            "path":"org/apache/zookeeper/Quotas.html#Quotas","title":
            "Quotas 4 Elements, 100% Coverage"},"children":[]},{"id":
          "ZooDefs15295","name":"ZooDefs","data":{"$area":0.0,"$color":
            -100.0,"path":"org/apache/zookeeper/ZooDefs.html#ZooDefs",
            "title":"ZooDefs 0 Elements,  -  Coverage"},"children":[]},{"id":
          "ZooDefs.OpCode15295","name":"ZooDefs.OpCode","data":{"$area":
            0.0,"$color":-100.0,"path":
            "org/apache/zookeeper/ZooDefs.html#ZooDefs.OpCode","title":
            "ZooDefs.OpCode 0 Elements,  -  Coverage"},"children":[]},{"id":
          "ZooDefs.Perms15295","name":"ZooDefs.Perms","data":{"$area":0.0,
            "$color":-100.0,"path":
            "org/apache/zookeeper/ZooDefs.html#ZooDefs.Perms","title":
            "ZooDefs.Perms 0 Elements,  -  Coverage"},"children":[]},{"id":
          "ZooDefs.Ids15295","name":"ZooDefs.Ids","data":{"$area":0.0,
            "$color":-100.0,"path":
            "org/apache/zookeeper/ZooDefs.html#ZooDefs.Ids","title":
            "ZooDefs.Ids 0 Elements,  -  Coverage"},"children":[]},{"id":
          "Environment15712","name":"Environment","data":{"$area":26.0,
            "$color":96.15385,"path":
            "org/apache/zookeeper/Environment.html#Environment","title":
            "Environment 26 Elements, 96.2% Coverage"},"children":[]},{"id":
          "Environment.Entry15712","name":"Environment.Entry","data":{
            "$area":9.0,"$color":100.0,"path":
            "org/apache/zookeeper/Environment.html#Environment.Entry",
            "title":"Environment.Entry 9 Elements, 100% Coverage"},
          "children":[]},{"id":"Shell16230","name":"Shell","data":{"$area":
            118.0,"$color":0.0,"path":
            "org/apache/zookeeper/Shell.html#Shell","title":
            "Shell 118 Elements, 0% Coverage"},"children":[]},{"id":
          "Shell.ExitCodeException16336","name":"Shell.ExitCodeException",
          "data":{"$area":5.0,"$color":0.0,"path":
            "org/apache/zookeeper/Shell.html#Shell.ExitCodeException",
            "title":"Shell.ExitCodeException 5 Elements, 0% Coverage"},
          "children":[]},{"id":"Shell.ShellCommandExecutor16341","name":
          "Shell.ShellCommandExecutor","data":{"$area":44.0,"$color":0.0,
            "path":
            "org/apache/zookeeper/Shell.html#Shell.ShellCommandExecutor",
            "title":"Shell.ShellCommandExecutor 44 Elements, 0% Coverage"},
          "children":[]},{"id":"Shell.ShellTimeoutTimerTask16397","name":
          "Shell.ShellTimeoutTimerTask","data":{"$area":11.0,"$color":0.0,
            "path":
            "org/apache/zookeeper/Shell.html#Shell.ShellTimeoutTimerTask",
            "title":"Shell.ShellTimeoutTimerTask 11 Elements, 0% Coverage"},
          "children":[]},{"id":"CreateMode17481","name":"CreateMode","data":{
            "$area":23.0,"$color":100.0,"path":
            "org/apache/zookeeper/CreateMode.html#CreateMode","title":
            "CreateMode 23 Elements, 100% Coverage"},"children":[]},{"id":
          "Watcher17523","name":"Watcher","data":{"$area":0.0,"$color":
            -100.0,"path":"org/apache/zookeeper/Watcher.html#Watcher",
            "title":"Watcher 0 Elements,  -  Coverage"},"children":[]},{"id":
          "Watcher.Event17523","name":"Watcher.Event","data":{"$area":0.0,
            "$color":-100.0,"path":
            "org/apache/zookeeper/Watcher.html#Watcher.Event","title":
            "Watcher.Event 0 Elements,  -  Coverage"},"children":[]},{"id":
          "Watcher.Event.KeeperState17523","name":
          "Watcher.Event.KeeperState","data":{"$area":24.0,"$color":100.0,
            "path":
            "org/apache/zookeeper/Watcher.html#Watcher.Event.KeeperState",
            "title":"Watcher.Event.KeeperState 24 Elements, 100% Coverage"},
          "children":[]},{"id":"Watcher.Event.EventType17547","name":
          "Watcher.Event.EventType","data":{"$area":18.0,"$color":100.0,
            "path":
            "org/apache/zookeeper/Watcher.html#Watcher.Event.EventType",
            "title":"Watcher.Event.EventType 18 Elements, 100% Coverage"},
          "children":[]},{"id":"ClientCnxnSocket17565","name":
          "ClientCnxnSocket","data":{"$area":46.0,"$color":80.434784,"path":
            "org/apache/zookeeper/ClientCnxnSocket.html#ClientCnxnSocket",
            "title":"ClientCnxnSocket 46 Elements, 80.4% Coverage"},
          "children":[]},{"id":"WatchedEvent17689","name":"WatchedEvent",
          "data":{"$area":18.0,"$color":100.0,"path":
            "org/apache/zookeeper/WatchedEvent.html#WatchedEvent","title":
            "WatchedEvent 18 Elements, 100% Coverage"},"children":[]},{"id":
          "OpResult17916","name":"OpResult","data":{"$area":4.0,"$color":
            100.0,"path":"org/apache/zookeeper/OpResult.html#OpResult",
            "title":"OpResult 4 Elements, 100% Coverage"},"children":[]},{
          "id":"OpResult.CreateResult17920","name":"OpResult.CreateResult",
          "data":{"$area":28.0,"$color":89.28571,"path":
            "org/apache/zookeeper/OpResult.html#OpResult.CreateResult",
            "title":"OpResult.CreateResult 28 Elements, 89.3% Coverage"},
          "children":[]},{"id":"OpResult.DeleteResult17948","name":
          "OpResult.DeleteResult","data":{"$area":15.0,"$color":100.0,"path":
            "org/apache/zookeeper/OpResult.html#OpResult.DeleteResult",
            "title":"OpResult.DeleteResult 15 Elements, 100% Coverage"},
          "children":[]},{"id":"OpResult.SetDataResult17963","name":
          "OpResult.SetDataResult","data":{"$area":18.0,"$color":100.0,
            "path":
            "org/apache/zookeeper/OpResult.html#OpResult.SetDataResult",
            "title":"OpResult.SetDataResult 18 Elements, 100% Coverage"},
          "children":[]},{"id":"OpResult.CheckResult17981","name":
          "OpResult.CheckResult","data":{"$area":15.0,"$color":100.0,"path":
            "org/apache/zookeeper/OpResult.html#OpResult.CheckResult",
            "title":"OpResult.CheckResult 15 Elements, 100% Coverage"},
          "children":[]},{"id":"OpResult.ErrorResult17996","name":
          "OpResult.ErrorResult","data":{"$area":18.0,"$color":100.0,"path":
            "org/apache/zookeeper/OpResult.html#OpResult.ErrorResult",
            "title":"OpResult.ErrorResult 18 Elements, 100% Coverage"},
          "children":[]},{"id":"StatsTrack18631","name":"StatsTrack","data":{
            "$area":24.0,"$color":91.66667,"path":
            "org/apache/zookeeper/StatsTrack.html#StatsTrack","title":
            "StatsTrack 24 Elements, 91.7% Coverage"},"children":[]},{"id":
          "ServerAdminClient18875","name":"ServerAdminClient","data":{
            "$area":186.0,"$color":0.0,"path":
            "org/apache/zookeeper/ServerAdminClient.html#ServerAdminClient",
            "title":"ServerAdminClient 186 Elements, 0% Coverage"},
          "children":[]}]},{"id":"org.apache.zookeeper.jmx11814","name":
      "org.apache.zookeeper.jmx","data":{"$area":125.0,"$color":87.2,"title":
        "org.apache.zookeeper.jmx 125 Elements, 87.2% Coverage"},"children":[
        {"id":"CommonNames11814","name":"CommonNames","data":{"$area":0.0,
            "$color":-100.0,"path":
            "org/apache/zookeeper/jmx/CommonNames.html#CommonNames","title":
            "CommonNames 0 Elements,  -  Coverage"},"children":[]},{"id":
          "ZKMBeanInfo14575","name":"ZKMBeanInfo","data":{"$area":0.0,
            "$color":-100.0,"path":
            "org/apache/zookeeper/jmx/ZKMBeanInfo.html#ZKMBeanInfo","title":
            "ZKMBeanInfo 0 Elements,  -  Coverage"},"children":[]},{"id":
          "MBeanRegistry14821","name":"MBeanRegistry","data":{"$area":
            106.0,"$color":86.79245,"path":
            "org/apache/zookeeper/jmx/MBeanRegistry.html#MBeanRegistry",
            "title":"MBeanRegistry 106 Elements, 86.8% Coverage"},"children":
          []},{"id":"ManagedUtil17504","name":"ManagedUtil","data":{"$area":
            19.0,"$color":89.47369,"path":
            "org/apache/zookeeper/jmx/ManagedUtil.html#ManagedUtil","title":
            "ManagedUtil 19 Elements, 89.5% Coverage"},"children":[]}]},{
      "id":"org.apache.zookeeper.server.quorum.flexible5977","name":
      "org.apache.zookeeper.server.quorum.flexible","data":{"$area":293.0,
        "$color":78.157,"title":
        "org.apache.zookeeper.server.quorum.flexible 293 Elements, 78.2% Coverage"},
      "children":[{"id":"QuorumHierarchical5977","name":
          "QuorumHierarchical","data":{"$area":213.0,"$color":72.30047,
            "path":
            "org/apache/zookeeper/server/quorum/flexible/QuorumHierarchical.html#QuorumHierarchical",
            "title":"QuorumHierarchical 213 Elements, 72.3% Coverage"},
          "children":[]},{"id":"QuorumVerifier11873","name":
          "QuorumVerifier","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/zookeeper/server/quorum/flexible/QuorumVerifier.html#QuorumVerifier",
            "title":"QuorumVerifier 0 Elements,  -  Coverage"},"children":[]},
        {"id":"QuorumMaj14070","name":"QuorumMaj","data":{"$area":80.0,
            "$color":93.75,"path":
            "org/apache/zookeeper/server/quorum/flexible/QuorumMaj.html#QuorumMaj",
            "title":"QuorumMaj 80 Elements, 93.8% Coverage"},"children":[]}]},
    {"id":"org.apache.zookeeper.cli1856","name":"org.apache.zookeeper.cli",
      "data":{"$area":770.0,"$color":37.662334,"title":
        "org.apache.zookeeper.cli 770 Elements, 37.7% Coverage"},"children":[
        {"id":"StatPrinter1856","name":"StatPrinter","data":{"$area":14.0,
            "$color":100.0,"path":
            "org/apache/zookeeper/cli/StatPrinter.html#StatPrinter","title":
            "StatPrinter 14 Elements, 100% Coverage"},"children":[]},{"id":
          "AclParser3513","name":"AclParser","data":{"$area":41.0,"$color":
            92.68293,"path":
            "org/apache/zookeeper/cli/AclParser.html#AclParser","title":
            "AclParser 41 Elements, 92.7% Coverage"},"children":[]},{"id":
          "GetConfigCommand4167","name":"GetConfigCommand","data":{"$area":
            32.0,"$color":18.75,"path":
            "org/apache/zookeeper/cli/GetConfigCommand.html#GetConfigCommand",
            "title":"GetConfigCommand 32 Elements, 18.8% Coverage"},
          "children":[]},{"id":"SetCommand4385","name":"SetCommand","data":{
            "$area":31.0,"$color":77.41935,"path":
            "org/apache/zookeeper/cli/SetCommand.html#SetCommand","title":
            "SetCommand 31 Elements, 77.4% Coverage"},"children":[]},{"id":
          "ReconfigCommand4881","name":"ReconfigCommand","data":{"$area":
            71.0,"$color":12.676056,"path":
            "org/apache/zookeeper/cli/ReconfigCommand.html#ReconfigCommand",
            "title":"ReconfigCommand 71 Elements, 12.7% Coverage"},
          "children":[]},{"id":"Ls2Command8303","name":"Ls2Command","data":{
            "$area":19.0,"$color":10.526316,"path":
            "org/apache/zookeeper/cli/Ls2Command.html#Ls2Command","title":
            "Ls2Command 19 Elements, 10.5% Coverage"},"children":[]},{"id":
          "SetQuotaCommand8322","name":"SetQuotaCommand","data":{"$area":
            108.0,"$color":6.4814816,"path":
            "org/apache/zookeeper/cli/SetQuotaCommand.html#SetQuotaCommand",
            "title":"SetQuotaCommand 108 Elements, 6.5% Coverage"},
          "children":[]},{"id":"GetAclCommand9707","name":"GetAclCommand",
          "data":{"$area":47.0,"$color":8.510638,"path":
            "org/apache/zookeeper/cli/GetAclCommand.html#GetAclCommand",
            "title":"GetAclCommand 47 Elements, 8.5% Coverage"},"children":[]},
        {"id":"DeleteAllCommand12185","name":"DeleteAllCommand","data":{
            "$area":23.0,"$color":91.30435,"path":
            "org/apache/zookeeper/cli/DeleteAllCommand.html#DeleteAllCommand",
            "title":"DeleteAllCommand 23 Elements, 91.3% Coverage"},
          "children":[]},{"id":"CloseCommand12280","name":"CloseCommand",
          "data":{"$area":7.0,"$color":28.57143,"path":
            "org/apache/zookeeper/cli/CloseCommand.html#CloseCommand",
            "title":"CloseCommand 7 Elements, 28.6% Coverage"},"children":[]},
        {"id":"DeleteCommand14031","name":"DeleteCommand","data":{"$area":
            39.0,"$color":92.30769,"path":
            "org/apache/zookeeper/cli/DeleteCommand.html#DeleteCommand",
            "title":"DeleteCommand 39 Elements, 92.3% Coverage"},"children":[]},
        {"id":"ListQuotaCommand14150","name":"ListQuotaCommand","data":{
            "$area":24.0,"$color":8.333334,"path":
            "org/apache/zookeeper/cli/ListQuotaCommand.html#ListQuotaCommand",
            "title":"ListQuotaCommand 24 Elements, 8.3% Coverage"},
          "children":[]},{"id":"DelQuotaCommand14414","name":
          "DelQuotaCommand","data":{"$area":75.0,"$color":8.0,"path":
            "org/apache/zookeeper/cli/DelQuotaCommand.html#DelQuotaCommand",
            "title":"DelQuotaCommand 75 Elements, 8% Coverage"},"children":[]},
        {"id":"SetAclCommand15472","name":"SetAclCommand","data":{"$area":
            33.0,"$color":15.151516,"path":
            "org/apache/zookeeper/cli/SetAclCommand.html#SetAclCommand",
            "title":"SetAclCommand 33 Elements, 15.2% Coverage"},"children":[]},
        {"id":"AddAuthCommand16925","name":"AddAuthCommand","data":{"$area":
            19.0,"$color":10.526316,"path":
            "org/apache/zookeeper/cli/AddAuthCommand.html#AddAuthCommand",
            "title":"AddAuthCommand 19 Elements, 10.5% Coverage"},"children":
          []},{"id":"StatCommand17611","name":"StatCommand","data":{"$area":
            33.0,"$color":75.757576,"path":
            "org/apache/zookeeper/cli/StatCommand.html#StatCommand","title":
            "StatCommand 33 Elements, 75.8% Coverage"},"children":[]},{"id":
          "CreateCommand17644","name":"CreateCommand","data":{"$area":45.0,
            "$color":95.55556,"path":
            "org/apache/zookeeper/cli/CreateCommand.html#CreateCommand",
            "title":"CreateCommand 45 Elements, 95.6% Coverage"},"children":[]},
        {"id":"LsCommand17711","name":"LsCommand","data":{"$area":37.0,
            "$color":59.45946,"path":
            "org/apache/zookeeper/cli/LsCommand.html#LsCommand","title":
            "LsCommand 37 Elements, 59.5% Coverage"},"children":[]},{"id":
          "SyncCommand17816","name":"SyncCommand","data":{"$area":17.0,
            "$color":11.764706,"path":
            "org/apache/zookeeper/cli/SyncCommand.html#SyncCommand","title":
            "SyncCommand 17 Elements, 11.8% Coverage"},"children":[]},{"id":
          "GetCommand18655","name":"GetCommand","data":{"$area":38.0,
            "$color":13.157895,"path":
            "org/apache/zookeeper/cli/GetCommand.html#GetCommand","title":
            "GetCommand 38 Elements, 13.2% Coverage"},"children":[]},{"id":
          "CliCommand18693","name":"CliCommand","data":{"$area":17.0,
            "$color":88.2353,"path":
            "org/apache/zookeeper/cli/CliCommand.html#CliCommand","title":
            "CliCommand 17 Elements, 88.2% Coverage"},"children":[]}]},{"id":
      "org.apache.zookeeper.server.quorum818","name":
      "org.apache.zookeeper.server.quorum","data":{"$area":5783.0,"$color":
        73.4221,"title":
        "org.apache.zookeeper.server.quorum 5783 Elements, 73.4% Coverage"},
      "children":[{"id":"Leader818","name":"Leader","data":{"$area":579.0,
            "$color":76.51123,"path":
            "org/apache/zookeeper/server/quorum/Leader.html#Leader","title":
            "Leader 579 Elements, 76.5% Coverage"},"children":[]},{"id":
          "Leader.Proposal820","name":"Leader.Proposal","data":{"$area":
            25.0,"$color":92.0,"path":
            "org/apache/zookeeper/server/quorum/Leader.html#Leader.Proposal",
            "title":"Leader.Proposal 25 Elements, 92% Coverage"},"children":[]},
        {"id":"Leader.Proposal.QuorumVerifierAcksetPair845","name":
          "Leader.Proposal.QuorumVerifierAcksetPair","data":{"$area":7.0,
            "$color":100.0,"path":
            "org/apache/zookeeper/server/quorum/Leader.html#Leader.Proposal.QuorumVerifierAcksetPair",
            "title":
            "Leader.Proposal.QuorumVerifierAcksetPair 7 Elements, 100% Coverage"},
          "children":[]},{"id":"Leader.LearnerCnxAcceptor915","name":
          "Leader.LearnerCnxAcceptor","data":{"$area":20.0,"$color":85.0,
            "path":
            "org/apache/zookeeper/server/quorum/Leader.html#Leader.LearnerCnxAcceptor",
            "title":"Leader.LearnerCnxAcceptor 20 Elements, 85% Coverage"},
          "children":[]},{"id":"Leader.ToBeAppliedRequestProcessor1173",
          "name":"Leader.ToBeAppliedRequestProcessor","data":{"$area":27.0,
            "$color":81.48148,"path":
            "org/apache/zookeeper/server/quorum/Leader.html#Leader.ToBeAppliedRequestProcessor",
            "title":
            "Leader.ToBeAppliedRequestProcessor 27 Elements, 81.5% Coverage"},
          "children":[]},{"id":"Leader.XidRolloverException1233","name":
          "Leader.XidRolloverException","data":{"$area":2.0,"$color":0.0,
            "path":
            "org/apache/zookeeper/server/quorum/Leader.html#Leader.XidRolloverException",
            "title":"Leader.XidRolloverException 2 Elements, 0% Coverage"},
          "children":[]},{"id":"ProposalRequestProcessor1824","name":
          "ProposalRequestProcessor","data":{"$area":24.0,"$color":
            95.83333,"path":
            "org/apache/zookeeper/server/quorum/ProposalRequestProcessor.html#ProposalRequestProcessor",
            "title":"ProposalRequestProcessor 24 Elements, 95.8% Coverage"},
          "children":[]},{"id":"Learner2685","name":"Learner","data":{
            "$area":348.0,"$color":79.02299,"path":
            "org/apache/zookeeper/server/quorum/Learner.html#Learner",
            "title":"Learner 348 Elements, 79% Coverage"},"children":[]},{
          "id":"Learner.PacketInFlight2685","name":
          "Learner.PacketInFlight","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/zookeeper/server/quorum/Learner.html#Learner.PacketInFlight",
            "title":"Learner.PacketInFlight 0 Elements,  -  Coverage"},
          "children":[]},{"id":"ReadOnlyZooKeeperServer3033","name":
          "ReadOnlyZooKeeperServer","data":{"$area":71.0,"$color":67.60563,
            "path":
            "org/apache/zookeeper/server/quorum/ReadOnlyZooKeeperServer.html#ReadOnlyZooKeeperServer",
            "title":"ReadOnlyZooKeeperServer 71 Elements, 67.6% Coverage"},
          "children":[]},{"id":"QuorumPeerConfig3104","name":
          "QuorumPeerConfig","data":{"$area":405.0,"$color":77.53087,"path":
            "org/apache/zookeeper/server/quorum/QuorumPeerConfig.html#QuorumPeerConfig",
            "title":"QuorumPeerConfig 405 Elements, 77.5% Coverage"},
          "children":[]},{"id":"QuorumPeerConfig.ConfigException3104","name":
          "QuorumPeerConfig.ConfigException","data":{"$area":4.0,"$color":
            100.0,"path":
            "org/apache/zookeeper/server/quorum/QuorumPeerConfig.html#QuorumPeerConfig.ConfigException",
            "title":
            "QuorumPeerConfig.ConfigException 4 Elements, 100% Coverage"},
          "children":[]},{"id":"QuorumMXBean3554","name":"QuorumMXBean",
          "data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/zookeeper/server/quorum/QuorumMXBean.html#QuorumMXBean",
            "title":"QuorumMXBean 0 Elements,  -  Coverage"},"children":[]},{
          "id":"LeaderSessionTracker3574","name":"LeaderSessionTracker",
          "data":{"$area":109.0,"$color":93.57798,"path":
            "org/apache/zookeeper/server/quorum/LeaderSessionTracker.html#LeaderSessionTracker",
            "title":"LeaderSessionTracker 109 Elements, 93.6% Coverage"},
          "children":[]},{"id":"Observer4087","name":"Observer","data":{
            "$area":80.0,"$color":76.25,"path":
            "org/apache/zookeeper/server/quorum/Observer.html#Observer",
            "title":"Observer 80 Elements, 76.2% Coverage"},"children":[]},{
          "id":"LeaderElection4199","name":"LeaderElection","data":{"$area":
            155.0,"$color":90.96774,"path":
            "org/apache/zookeeper/server/quorum/LeaderElection.html#LeaderElection",
            "title":"LeaderElection 155 Elements, 91% Coverage"},"children":[]},
        {"id":"LeaderElection.ElectionResult4201","name":
          "LeaderElection.ElectionResult","data":{"$area":0.0,"$color":
            -100.0,"path":
            "org/apache/zookeeper/server/quorum/LeaderElection.html#LeaderElection.ElectionResult",
            "title":
            "LeaderElection.ElectionResult 0 Elements,  -  Coverage"},
          "children":[]},{"id":"LeaderRequestProcessor6190","name":
          "LeaderRequestProcessor","data":{"$area":24.0,"$color":79.16667,
            "path":
            "org/apache/zookeeper/server/quorum/LeaderRequestProcessor.html#LeaderRequestProcessor",
            "title":"LeaderRequestProcessor 24 Elements, 79.2% Coverage"},
          "children":[]},{"id":"LeaderMXBean6214","name":"LeaderMXBean",
          "data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/zookeeper/server/quorum/LeaderMXBean.html#LeaderMXBean",
            "title":"LeaderMXBean 0 Elements,  -  Coverage"},"children":[]},{
          "id":"FollowerRequestProcessor6214","name":
          "FollowerRequestProcessor","data":{"$area":68.0,"$color":86.7647,
            "path":
            "org/apache/zookeeper/server/quorum/FollowerRequestProcessor.html#FollowerRequestProcessor",
            "title":"FollowerRequestProcessor 68 Elements, 86.8% Coverage"},
          "children":[]},{"id":"LocalPeerMXBean6291","name":
          "LocalPeerMXBean","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/zookeeper/server/quorum/LocalPeerMXBean.html#LocalPeerMXBean",
            "title":"LocalPeerMXBean 0 Elements,  -  Coverage"},"children":[]},
        {"id":"LearnerSessionTracker7590","name":"LearnerSessionTracker",
          "data":{"$area":120.0,"$color":78.333336,"path":
            "org/apache/zookeeper/server/quorum/LearnerSessionTracker.html#LearnerSessionTracker",
            "title":"LearnerSessionTracker 120 Elements, 78.3% Coverage"},
          "children":[]},{"id":"SendAckRequestProcessor8276","name":
          "SendAckRequestProcessor","data":{"$area":27.0,"$color":59.25926,
            "path":
            "org/apache/zookeeper/server/quorum/SendAckRequestProcessor.html#SendAckRequestProcessor",
            "title":"SendAckRequestProcessor 27 Elements, 59.3% Coverage"},
          "children":[]},{"id":"ObserverZooKeeperServer8705","name":
          "ObserverZooKeeperServer","data":{"$area":34.0,"$color":85.29411,
            "path":
            "org/apache/zookeeper/server/quorum/ObserverZooKeeperServer.html#ObserverZooKeeperServer",
            "title":"ObserverZooKeeperServer 34 Elements, 85.3% Coverage"},
          "children":[]},{"id":"QuorumCnxManager9124","name":
          "QuorumCnxManager","data":{"$area":256.0,"$color":85.15625,"path":
            "org/apache/zookeeper/server/quorum/QuorumCnxManager.html#QuorumCnxManager",
            "title":"QuorumCnxManager 256 Elements, 85.2% Coverage"},
          "children":[]},{"id":"QuorumCnxManager.Message9124","name":
          "QuorumCnxManager.Message","data":{"$area":3.0,"$color":100.0,
            "path":
            "org/apache/zookeeper/server/quorum/QuorumCnxManager.html#QuorumCnxManager.Message",
            "title":"QuorumCnxManager.Message 3 Elements, 100% Coverage"},
          "children":[]},{"id":"QuorumCnxManager.Listener9356","name":
          "QuorumCnxManager.Listener","data":{"$area":50.0,"$color":70.0,
            "path":
            "org/apache/zookeeper/server/quorum/QuorumCnxManager.html#QuorumCnxManager.Listener",
            "title":"QuorumCnxManager.Listener 50 Elements, 70% Coverage"},
          "children":[]},{"id":"QuorumCnxManager.SendWorker9406","name":
          "QuorumCnxManager.SendWorker","data":{"$area":85.0,"$color":
            82.35294,"path":
            "org/apache/zookeeper/server/quorum/QuorumCnxManager.html#QuorumCnxManager.SendWorker",
            "title":
            "QuorumCnxManager.SendWorker 85 Elements, 82.4% Coverage"},
          "children":[]},{"id":"QuorumCnxManager.RecvWorker9491","name":
          "QuorumCnxManager.RecvWorker","data":{"$area":42.0,"$color":
            85.71429,"path":
            "org/apache/zookeeper/server/quorum/QuorumCnxManager.html#QuorumCnxManager.RecvWorker",
            "title":
            "QuorumCnxManager.RecvWorker 42 Elements, 85.7% Coverage"},
          "children":[]},{"id":"CommitProcessor9560","name":
          "CommitProcessor","data":{"$area":128.0,"$color":87.5,"path":
            "org/apache/zookeeper/server/quorum/CommitProcessor.html#CommitProcessor",
            "title":"CommitProcessor 128 Elements, 87.5% Coverage"},
          "children":[]},{"id":"CommitProcessor.CommitWorkRequest9639",
          "name":"CommitProcessor.CommitWorkRequest","data":{"$area":19.0,
            "$color":68.42105,"path":
            "org/apache/zookeeper/server/quorum/CommitProcessor.html#CommitProcessor.CommitWorkRequest",
            "title":
            "CommitProcessor.CommitWorkRequest 19 Elements, 68.4% Coverage"},
          "children":[]},{"id":"LocalPeerBean9962","name":"LocalPeerBean",
          "data":{"$area":26.0,"$color":23.076923,"path":
            "org/apache/zookeeper/server/quorum/LocalPeerBean.html#LocalPeerBean",
            "title":"LocalPeerBean 26 Elements, 23.1% Coverage"},"children":[]},
        {"id":"LearnerZooKeeperServer10242","name":
          "LearnerZooKeeperServer","data":{"$area":54.0,"$color":81.48148,
            "path":
            "org/apache/zookeeper/server/quorum/LearnerZooKeeperServer.html#LearnerZooKeeperServer",
            "title":"LearnerZooKeeperServer 54 Elements, 81.5% Coverage"},
          "children":[]},{"id":"QuorumBean11090","name":"QuorumBean","data":{
            "$area":9.0,"$color":77.77778,"path":
            "org/apache/zookeeper/server/quorum/QuorumBean.html#QuorumBean",
            "title":"QuorumBean 9 Elements, 77.8% Coverage"},"children":[]},{
          "id":"ObserverRequestProcessor11177","name":
          "ObserverRequestProcessor","data":{"$area":68.0,"$color":
            72.05882,"path":
            "org/apache/zookeeper/server/quorum/ObserverRequestProcessor.html#ObserverRequestProcessor",
            "title":"ObserverRequestProcessor 68 Elements, 72.1% Coverage"},
          "children":[]},{"id":"AckRequestProcessor11245","name":
          "AckRequestProcessor","data":{"$area":10.0,"$color":80.0,"path":
            "org/apache/zookeeper/server/quorum/AckRequestProcessor.html#AckRequestProcessor",
            "title":"AckRequestProcessor 10 Elements, 80% Coverage"},
          "children":[]},{"id":"LeaderElectionBean11808","name":
          "LeaderElectionBean","data":{"$area":6.0,"$color":66.66667,"path":
            "org/apache/zookeeper/server/quorum/LeaderElectionBean.html#LeaderElectionBean",
            "title":"LeaderElectionBean 6 Elements, 66.7% Coverage"},
          "children":[]},{"id":"FollowerMXBean11814","name":
          "FollowerMXBean","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/zookeeper/server/quorum/FollowerMXBean.html#FollowerMXBean",
            "title":"FollowerMXBean 0 Elements,  -  Coverage"},"children":[]},
        {"id":"QuorumZooKeeperServer12303","name":"QuorumZooKeeperServer",
          "data":{"$area":77.0,"$color":88.31169,"path":
            "org/apache/zookeeper/server/quorum/QuorumZooKeeperServer.html#QuorumZooKeeperServer",
            "title":"QuorumZooKeeperServer 77 Elements, 88.3% Coverage"},
          "children":[]},{"id":"QuorumPeer12380","name":"QuorumPeer","data":{
            "$area":684.0,"$color":82.89474,"path":
            "org/apache/zookeeper/server/quorum/QuorumPeer.html#QuorumPeer",
            "title":"QuorumPeer 684 Elements, 82.9% Coverage"},"children":[]},
        {"id":"QuorumPeer.QuorumServer12380","name":
          "QuorumPeer.QuorumServer","data":{"$area":127.0,"$color":
            81.102356,"path":
            "org/apache/zookeeper/server/quorum/QuorumPeer.html#QuorumPeer.QuorumServer",
            "title":"QuorumPeer.QuorumServer 127 Elements, 81.1% Coverage"},
          "children":[]},{"id":"QuorumPeer.ServerState12507","name":
          "QuorumPeer.ServerState","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/zookeeper/server/quorum/QuorumPeer.html#QuorumPeer.ServerState",
            "title":"QuorumPeer.ServerState 0 Elements,  -  Coverage"},
          "children":[]},{"id":"QuorumPeer.LearnerType12507","name":
          "QuorumPeer.LearnerType","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/zookeeper/server/quorum/QuorumPeer.html#QuorumPeer.LearnerType",
            "title":"QuorumPeer.LearnerType 0 Elements,  -  Coverage"},
          "children":[]},{"id":"QuorumPeer.ResponderThread12529","name":
          "QuorumPeer.ResponderThread","data":{"$area":45.0,"$color":
            88.88889,"path":
            "org/apache/zookeeper/server/quorum/QuorumPeer.html#QuorumPeer.ResponderThread",
            "title":
            "QuorumPeer.ResponderThread 45 Elements, 88.9% Coverage"},
          "children":[]},{"id":"Follower13248","name":"Follower","data":{
            "$area":101.0,"$color":81.18812,"path":
            "org/apache/zookeeper/server/quorum/Follower.html#Follower",
            "title":"Follower 101 Elements, 81.2% Coverage"},"children":[]},{
          "id":"QuorumStats13478","name":"QuorumStats","data":{"$area":
            28.0,"$color":7.1428576,"path":
            "org/apache/zookeeper/server/quorum/QuorumStats.html#QuorumStats",
            "title":"QuorumStats 28 Elements, 7.1% Coverage"},"children":[]},
        {"id":"QuorumStats.Provider13478","name":"QuorumStats.Provider",
          "data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/zookeeper/server/quorum/QuorumStats.html#QuorumStats.Provider",
            "title":"QuorumStats.Provider 0 Elements,  -  Coverage"},
          "children":[]},{"id":"LearnerHandler13506","name":
          "LearnerHandler","data":{"$area":490.0,"$color":80.408165,"path":
            "org/apache/zookeeper/server/quorum/LearnerHandler.html#LearnerHandler",
            "title":"LearnerHandler 490 Elements, 80.4% Coverage"},
          "children":[]},{"id":"LearnerHandler.SyncLimitCheck13512","name":
          "LearnerHandler.SyncLimitCheck","data":{"$area":35.0,"$color":
            88.57143,"path":
            "org/apache/zookeeper/server/quorum/LearnerHandler.html#LearnerHandler.SyncLimitCheck",
            "title":
            "LearnerHandler.SyncLimitCheck 35 Elements, 88.6% Coverage"},
          "children":[]},{"id":"ServerBean14174","name":"ServerBean","data":{
            "$area":4.0,"$color":0.0,"path":
            "org/apache/zookeeper/server/quorum/ServerBean.html#ServerBean",
            "title":"ServerBean 4 Elements, 0% Coverage"},"children":[]},{
          "id":"Election14178","name":"Election","data":{"$area":0.0,
            "$color":-100.0,"path":
            "org/apache/zookeeper/server/quorum/Election.html#Election",
            "title":"Election 0 Elements,  -  Coverage"},"children":[]},{
          "id":"LearnerSyncRequest14489","name":"LearnerSyncRequest","data":{
            "$area":3.0,"$color":100.0,"path":
            "org/apache/zookeeper/server/quorum/LearnerSyncRequest.html#LearnerSyncRequest",
            "title":"LearnerSyncRequest 3 Elements, 100% Coverage"},
          "children":[]},{"id":"LeaderZooKeeperServer14750","name":
          "LeaderZooKeeperServer","data":{"$area":71.0,"$color":90.14085,
            "path":
            "org/apache/zookeeper/server/quorum/LeaderZooKeeperServer.html#LeaderZooKeeperServer",
            "title":"LeaderZooKeeperServer 71 Elements, 90.1% Coverage"},
          "children":[]},{"id":"UpgradeableSessionTracker15015","name":
          "UpgradeableSessionTracker","data":{"$area":24.0,"$color":62.5,
            "path":
            "org/apache/zookeeper/server/quorum/UpgradeableSessionTracker.html#UpgradeableSessionTracker",
            "title":
            "UpgradeableSessionTracker 24 Elements, 62.5% Coverage"},
          "children":[]},{"id":"FollowerBean15178","name":"FollowerBean",
          "data":{"$area":11.0,"$color":45.454548,"path":
            "org/apache/zookeeper/server/quorum/FollowerBean.html#FollowerBean",
            "title":"FollowerBean 11 Elements, 45.5% Coverage"},"children":[]},
        {"id":"LocalSessionTracker15747","name":"LocalSessionTracker","data":
          {"$area":8.0,"$color":25.0,"path":
            "org/apache/zookeeper/server/quorum/LocalSessionTracker.html#LocalSessionTracker",
            "title":"LocalSessionTracker 8 Elements, 25% Coverage"},
          "children":[]},{"id":"FastLeaderElection15755","name":
          "FastLeaderElection","data":{"$area":248.0,"$color":87.90323,
            "path":
            "org/apache/zookeeper/server/quorum/FastLeaderElection.html#FastLeaderElection",
            "title":"FastLeaderElection 248 Elements, 87.9% Coverage"},
          "children":[]},{"id":"FastLeaderElection.Notification15755","name":
          "FastLeaderElection.Notification","data":{"$area":0.0,"$color":
            -100.0,"path":
            "org/apache/zookeeper/server/quorum/FastLeaderElection.html#FastLeaderElection.Notification",
            "title":
            "FastLeaderElection.Notification 0 Elements,  -  Coverage"},
          "children":[]},{"id":"FastLeaderElection.ToSend15755","name":
          "FastLeaderElection.ToSend","data":{"$area":8.0,"$color":100.0,
            "path":
            "org/apache/zookeeper/server/quorum/FastLeaderElection.html#FastLeaderElection.ToSend",
            "title":"FastLeaderElection.ToSend 8 Elements, 100% Coverage"},
          "children":[]},{"id":"FastLeaderElection.ToSend.mType15755","name":
          "FastLeaderElection.ToSend.mType","data":{"$area":0.0,"$color":
            -100.0,"path":
            "org/apache/zookeeper/server/quorum/FastLeaderElection.html#FastLeaderElection.ToSend.mType",
            "title":
            "FastLeaderElection.ToSend.mType 0 Elements,  -  Coverage"},
          "children":[]},{"id":"FastLeaderElection.Messenger15763","name":
          "FastLeaderElection.Messenger","data":{"$area":12.0,"$color":
            100.0,"path":
            "org/apache/zookeeper/server/quorum/FastLeaderElection.html#FastLeaderElection.Messenger",
            "title":
            "FastLeaderElection.Messenger 12 Elements, 100% Coverage"},
          "children":[]},{"id":
          "FastLeaderElection.Messenger.WorkerReceiver15763","name":
          "FastLeaderElection.Messenger.WorkerReceiver","data":{"$area":
            120.0,"$color":86.666664,"path":
            "org/apache/zookeeper/server/quorum/FastLeaderElection.html#FastLeaderElection.Messenger.WorkerReceiver",
            "title":
            "FastLeaderElection.Messenger.WorkerReceiver 120 Elements, 86.7% Coverage"},
          "children":[]},{"id":
          "FastLeaderElection.Messenger.WorkerSender15883","name":
          "FastLeaderElection.Messenger.WorkerSender","data":{"$area":27.0,
            "$color":96.296295,"path":
            "org/apache/zookeeper/server/quorum/FastLeaderElection.html#FastLeaderElection.Messenger.WorkerSender",
            "title":
            "FastLeaderElection.Messenger.WorkerSender 27 Elements, 96.3% Coverage"},
          "children":[]},{"id":"ReadOnlyRequestProcessor16170","name":
          "ReadOnlyRequestProcessor","data":{"$area":60.0,"$color":
            63.333332,"path":
            "org/apache/zookeeper/server/quorum/ReadOnlyRequestProcessor.html#ReadOnlyRequestProcessor",
            "title":"ReadOnlyRequestProcessor 60 Elements, 63.3% Coverage"},
          "children":[]},{"id":"StateSummary16408","name":"StateSummary",
          "data":{"$area":18.0,"$color":50.0,"path":
            "org/apache/zookeeper/server/quorum/StateSummary.html#StateSummary",
            "title":"StateSummary 18 Elements, 50% Coverage"},"children":[]},
        {"id":"FollowerZooKeeperServer16858","name":
          "FollowerZooKeeperServer","data":{"$area":59.0,"$color":79.66102,
            "path":
            "org/apache/zookeeper/server/quorum/FollowerZooKeeperServer.html#FollowerZooKeeperServer",
            "title":"FollowerZooKeeperServer 59 Elements, 79.7% Coverage"},
          "children":[]},{"id":"RemotePeerMXBean16917","name":
          "RemotePeerMXBean","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/zookeeper/server/quorum/RemotePeerMXBean.html#RemotePeerMXBean",
            "title":"RemotePeerMXBean 0 Elements,  -  Coverage"},"children":[]},
        {"id":"RemotePeerBean16917","name":"RemotePeerBean","data":{"$area":
            8.0,"$color":75.0,"path":
            "org/apache/zookeeper/server/quorum/RemotePeerBean.html#RemotePeerBean",
            "title":"RemotePeerBean 8 Elements, 75% Coverage"},"children":[]},
        {"id":"ServerMXBean17504","name":"ServerMXBean","data":{"$area":
            0.0,"$color":-100.0,"path":
            "org/apache/zookeeper/server/quorum/ServerMXBean.html#ServerMXBean",
            "title":"ServerMXBean 0 Elements,  -  Coverage"},"children":[]},{
          "id":"ObserverMXBean17504","name":"ObserverMXBean","data":{"$area":
            0.0,"$color":-100.0,"path":
            "org/apache/zookeeper/server/quorum/ObserverMXBean.html#ObserverMXBean",
            "title":"ObserverMXBean 0 Elements,  -  Coverage"},"children":[]},
        {"id":"ReadOnlyBean17707","name":"ReadOnlyBean","data":{"$area":
            4.0,"$color":100.0,"path":
            "org/apache/zookeeper/server/quorum/ReadOnlyBean.html#ReadOnlyBean",
            "title":"ReadOnlyBean 4 Elements, 100% Coverage"},"children":[]},
        {"id":"QuorumPeerMain17748","name":"QuorumPeerMain","data":{"$area":
            68.0,"$color":70.588234,"path":
            "org/apache/zookeeper/server/quorum/QuorumPeerMain.html#QuorumPeerMain",
            "title":"QuorumPeerMain 68 Elements, 70.6% Coverage"},"children":
          []},{"id":"LeaderBean18014","name":"LeaderBean","data":{"$area":
            12.0,"$color":41.666664,"path":
            "org/apache/zookeeper/server/quorum/LeaderBean.html#LeaderBean",
            "title":"LeaderBean 12 Elements, 41.7% Coverage"},"children":[]},
        {"id":"LeaderElectionMXBean18085","name":"LeaderElectionMXBean",
          "data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/zookeeper/server/quorum/LeaderElectionMXBean.html#LeaderElectionMXBean",
            "title":"LeaderElectionMXBean 0 Elements,  -  Coverage"},
          "children":[]},{"id":"AuthFastLeaderElection18085","name":
          "AuthFastLeaderElection","data":{"$area":150.0,"$color":0.0,"path":
            
            "org/apache/zookeeper/server/quorum/AuthFastLeaderElection.html#AuthFastLeaderElection",
            "title":"AuthFastLeaderElection 150 Elements, 0% Coverage"},
          "children":[]},{"id":"AuthFastLeaderElection.Notification18085",
          "name":"AuthFastLeaderElection.Notification","data":{"$area":0.0,
            "$color":-100.0,"path":
            "org/apache/zookeeper/server/quorum/AuthFastLeaderElection.html#AuthFastLeaderElection.Notification",
            "title":
            "AuthFastLeaderElection.Notification 0 Elements,  -  Coverage"},
          "children":[]},{"id":"AuthFastLeaderElection.ToSend18085","name":
          "AuthFastLeaderElection.ToSend","data":{"$area":40.0,"$color":
            0.0,"path":
            "org/apache/zookeeper/server/quorum/AuthFastLeaderElection.html#AuthFastLeaderElection.ToSend",
            "title":
            "AuthFastLeaderElection.ToSend 40 Elements, 0% Coverage"},
          "children":[]},{"id":"AuthFastLeaderElection.ToSend.mType18085",
          "name":"AuthFastLeaderElection.ToSend.mType","data":{"$area":0.0,
            "$color":-100.0,"path":
            "org/apache/zookeeper/server/quorum/AuthFastLeaderElection.html#AuthFastLeaderElection.ToSend.mType",
            "title":
            "AuthFastLeaderElection.ToSend.mType 0 Elements,  -  Coverage"},
          "children":[]},{"id":"AuthFastLeaderElection.Messenger18125",
          "name":"AuthFastLeaderElection.Messenger","data":{"$area":21.0,
            "$color":0.0,"path":
            "org/apache/zookeeper/server/quorum/AuthFastLeaderElection.html#AuthFastLeaderElection.Messenger",
            "title":
            "AuthFastLeaderElection.Messenger 21 Elements, 0% Coverage"},
          "children":[]},{"id":
          "AuthFastLeaderElection.Messenger.WorkerReceiver18125","name":
          "AuthFastLeaderElection.Messenger.WorkerReceiver","data":{"$area":
            126.0,"$color":0.0,"path":
            "org/apache/zookeeper/server/quorum/AuthFastLeaderElection.html#AuthFastLeaderElection.Messenger.WorkerReceiver",
            "title":
            "AuthFastLeaderElection.Messenger.WorkerReceiver 126 Elements, 0% Coverage"},
          "children":[]},{"id":
          "AuthFastLeaderElection.Messenger.WorkerSender18251","name":
          "AuthFastLeaderElection.Messenger.WorkerSender","data":{"$area":
            164.0,"$color":0.0,"path":
            "org/apache/zookeeper/server/quorum/AuthFastLeaderElection.html#AuthFastLeaderElection.Messenger.WorkerSender",
            "title":
            "AuthFastLeaderElection.Messenger.WorkerSender 164 Elements, 0% Coverage"},
          "children":[]},{"id":"Vote18586","name":"Vote","data":{"$area":
            45.0,"$color":95.55556,"path":
            "org/apache/zookeeper/server/quorum/Vote.html#Vote","title":
            "Vote 45 Elements, 95.6% Coverage"},"children":[]}]},{"id":
      "org.apache.zookeeper.server.auth1478","name":
      "org.apache.zookeeper.server.auth","data":{"$area":524.0,"$color":
        52.862595,"title":
        "org.apache.zookeeper.server.auth 524 Elements, 52.9% Coverage"},
      "children":[{"id":"DigestLoginModule1478","name":"DigestLoginModule",
          "data":{"$area":18.0,"$color":77.77778,"path":
            "org/apache/zookeeper/server/auth/DigestLoginModule.html#DigestLoginModule",
            "title":"DigestLoginModule 18 Elements, 77.8% Coverage"},
          "children":[]},{"id":"KerberosName4675","name":"KerberosName",
          "data":{"$area":95.0,"$color":50.526314,"path":
            "org/apache/zookeeper/server/auth/KerberosName.html#KerberosName",
            "title":"KerberosName 95 Elements, 50.5% Coverage"},"children":[]},
        {"id":"KerberosName.Rule4723","name":"KerberosName.Rule","data":{
            "$area":105.0,"$color":7.619048,"path":
            "org/apache/zookeeper/server/auth/KerberosName.html#KerberosName.Rule",
            "title":"KerberosName.Rule 105 Elements, 7.6% Coverage"},
          "children":[]},{"id":"KerberosName.BadFormatString4849","name":
          "KerberosName.BadFormatString","data":{"$area":4.0,"$color":0.0,
            "path":
            "org/apache/zookeeper/server/auth/KerberosName.html#KerberosName.BadFormatString",
            "title":"KerberosName.BadFormatString 4 Elements, 0% Coverage"},
          "children":[]},{"id":"KerberosName.NoMatchingRule4853","name":
          "KerberosName.NoMatchingRule","data":{"$area":2.0,"$color":0.0,
            "path":
            "org/apache/zookeeper/server/auth/KerberosName.html#KerberosName.NoMatchingRule",
            "title":"KerberosName.NoMatchingRule 2 Elements, 0% Coverage"},
          "children":[]},{"id":"ProviderRegistry7434","name":
          "ProviderRegistry","data":{"$area":36.0,"$color":94.44444,"path":
            "org/apache/zookeeper/server/auth/ProviderRegistry.html#ProviderRegistry",
            "title":"ProviderRegistry 36 Elements, 94.4% Coverage"},
          "children":[]},{"id":"SASLAuthenticationProvider8833","name":
          "SASLAuthenticationProvider","data":{"$area":29.0,"$color":
            68.965515,"path":
            "org/apache/zookeeper/server/auth/SASLAuthenticationProvider.html#SASLAuthenticationProvider",
            "title":"SASLAuthenticationProvider 29 Elements, 69% Coverage"},
          "children":[]},{"id":"AuthenticationProvider13248","name":
          "AuthenticationProvider","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/zookeeper/server/auth/AuthenticationProvider.html#AuthenticationProvider",
            "title":"AuthenticationProvider 0 Elements,  -  Coverage"},
          "children":[]},{"id":"IPAuthenticationProvider13403","name":
          "IPAuthenticationProvider","data":{"$area":75.0,"$color":
            33.333336,"path":
            "org/apache/zookeeper/server/auth/IPAuthenticationProvider.html#IPAuthenticationProvider",
            "title":"IPAuthenticationProvider 75 Elements, 33.3% Coverage"},
          "children":[]},{"id":"DigestAuthenticationProvider15635","name":
          "DigestAuthenticationProvider","data":{"$area":77.0,"$color":
            81.818184,"path":
            "org/apache/zookeeper/server/auth/DigestAuthenticationProvider.html#DigestAuthenticationProvider",
            "title":
            "DigestAuthenticationProvider 77 Elements, 81.8% Coverage"},
          "children":[]},{"id":"SaslServerCallbackHandler17833","name":
          "SaslServerCallbackHandler","data":{"$area":83.0,"$color":
            78.313255,"path":
            "org/apache/zookeeper/server/auth/SaslServerCallbackHandler.html#SaslServerCallbackHandler",
            "title":
            "SaslServerCallbackHandler 83 Elements, 78.3% Coverage"},
          "children":[]}]},{"id":"org.apache.zookeeper.server.util1782",
      "name":"org.apache.zookeeper.server.util","data":{"$area":247.0,
        "$color":62.348183,"title":
        "org.apache.zookeeper.server.util 247 Elements, 62.3% Coverage"},
      "children":[{"id":"VerifyingFileFactory1782","name":
          "VerifyingFileFactory","data":{"$area":32.0,"$color":93.75,"path":
            "org/apache/zookeeper/server/util/VerifyingFileFactory.html#VerifyingFileFactory",
            "title":"VerifyingFileFactory 32 Elements, 93.8% Coverage"},
          "children":[]},{"id":"VerifyingFileFactory.Builder1814","name":
          "VerifyingFileFactory.Builder","data":{"$area":10.0,"$color":
            100.0,"path":
            "org/apache/zookeeper/server/util/VerifyingFileFactory.html#VerifyingFileFactory.Builder",
            "title":
            "VerifyingFileFactory.Builder 10 Elements, 100% Coverage"},
          "children":[]},{"id":"ZxidUtils1848","name":"ZxidUtils","data":{
            "$area":8.0,"$color":50.0,"path":
            "org/apache/zookeeper/server/util/ZxidUtils.html#ZxidUtils",
            "title":"ZxidUtils 8 Elements, 50% Coverage"},"children":[]},{
          "id":"ConfigUtils8645","name":"ConfigUtils","data":{"$area":32.0,
            "$color":0.0,"path":
            "org/apache/zookeeper/server/util/ConfigUtils.html#ConfigUtils",
            "title":"ConfigUtils 32 Elements, 0% Coverage"},"children":[]},{
          "id":"KerberosUtil9021","name":"KerberosUtil","data":{"$area":
            14.0,"$color":85.71429,"path":
            "org/apache/zookeeper/server/util/KerberosUtil.html#KerberosUtil",
            "title":"KerberosUtil 14 Elements, 85.7% Coverage"},"children":[]},
        {"id":"OSMXBean14498","name":"OSMXBean","data":{"$area":77.0,
            "$color":41.55844,"path":
            "org/apache/zookeeper/server/util/OSMXBean.html#OSMXBean",
            "title":"OSMXBean 77 Elements, 41.6% Coverage"},"children":[]},{
          "id":"SerializeUtils14676","name":"SerializeUtils","data":{"$area":
            74.0,"$color":89.189186,"path":
            "org/apache/zookeeper/server/util/SerializeUtils.html#SerializeUtils",
            "title":"SerializeUtils 74 Elements, 89.2% Coverage"},"children":
          []}]},{"id":"org.apache.zookeeper.server0","name":
      "org.apache.zookeeper.server","data":{"$area":6201.0,"$color":
        78.906624,"title":
        "org.apache.zookeeper.server 6201 Elements, 78.9% Coverage"},
      "children":[{"id":"DataTree0","name":"DataTree","data":{"$area":
            809.0,"$color":86.03214,"path":
            "org/apache/zookeeper/server/DataTree.html#DataTree","title":
            "DataTree 809 Elements, 86% Coverage"},"children":[]},{"id":
          "DataTree.ProcessTxnResult395","name":
          "DataTree.ProcessTxnResult","data":{"$area":9.0,"$color":0.0,
            "path":
            "org/apache/zookeeper/server/DataTree.html#DataTree.ProcessTxnResult",
            "title":"DataTree.ProcessTxnResult 9 Elements, 0% Coverage"},
          "children":[]},{"id":"DataTree.Counts557","name":
          "DataTree.Counts","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/zookeeper/server/DataTree.html#DataTree.Counts",
            "title":"DataTree.Counts 0 Elements,  -  Coverage"},"children":[]},
        {"id":"DataTreeMXBean818","name":"DataTreeMXBean","data":{"$area":
            0.0,"$color":-100.0,"path":
            "org/apache/zookeeper/server/DataTreeMXBean.html#DataTreeMXBean",
            "title":"DataTreeMXBean 0 Elements,  -  Coverage"},"children":[]},
        {"id":"ConnectionBean1617","name":"ConnectionBean","data":{"$area":
            73.0,"$color":27.39726,"path":
            "org/apache/zookeeper/server/ConnectionBean.html#ConnectionBean",
            "title":"ConnectionBean 73 Elements, 27.4% Coverage"},"children":
          []},{"id":"WorkerService1690","name":"WorkerService","data":{
            "$area":59.0,"$color":83.05085,"path":
            "org/apache/zookeeper/server/WorkerService.html#WorkerService",
            "title":"WorkerService 59 Elements, 83.1% Coverage"},"children":[]},
        {"id":"WorkerService.WorkRequest1697","name":
          "WorkerService.WorkRequest","data":{"$area":1.0,"$color":0.0,
            "path":
            "org/apache/zookeeper/server/WorkerService.html#WorkerService.WorkRequest",
            "title":"WorkerService.WorkRequest 1 Elements, 0% Coverage"},
          "children":[]},{"id":"WorkerService.ScheduledWorkRequest1718",
          "name":"WorkerService.ScheduledWorkRequest","data":{"$area":12.0,
            "$color":58.333332,"path":
            "org/apache/zookeeper/server/WorkerService.html#WorkerService.ScheduledWorkRequest",
            "title":
            "WorkerService.ScheduledWorkRequest 12 Elements, 58.3% Coverage"},
          "children":[]},{"id":"WorkerService.DaemonThreadFactory1730",
          "name":"WorkerService.DaemonThreadFactory","data":{"$area":20.0,
            "$color":85.0,"path":
            "org/apache/zookeeper/server/WorkerService.html#WorkerService.DaemonThreadFactory",
            "title":
            "WorkerService.DaemonThreadFactory 20 Elements, 85% Coverage"},
          "children":[]},{"id":"ZooKeeperServerMXBean1856","name":
          "ZooKeeperServerMXBean","data":{"$area":0.0,"$color":-100.0,"path":
            
            "org/apache/zookeeper/server/ZooKeeperServerMXBean.html#ZooKeeperServerMXBean",
            "title":"ZooKeeperServerMXBean 0 Elements,  -  Coverage"},
          "children":[]},{"id":"PrepRequestProcessor1975","name":
          "PrepRequestProcessor","data":{"$area":675.0,"$color":86.37038,
            "path":
            "org/apache/zookeeper/server/PrepRequestProcessor.html#PrepRequestProcessor",
            "title":"PrepRequestProcessor 675 Elements, 86.4% Coverage"},
          "children":[]},{"id":"ByteBufferInputStream2650","name":
          "ByteBufferInputStream","data":{"$area":35.0,"$color":54.285717,
            "path":
            "org/apache/zookeeper/server/ByteBufferInputStream.html#ByteBufferInputStream",
            "title":"ByteBufferInputStream 35 Elements, 54.3% Coverage"},
          "children":[]},{"id":"ZooTrace3554","name":"ZooTrace","data":{
            "$area":20.0,"$color":35.0,"path":
            "org/apache/zookeeper/server/ZooTrace.html#ZooTrace","title":
            "ZooTrace 20 Elements, 35% Coverage"},"children":[]},{"id":
          "ConnectionMXBean3574","name":"ConnectionMXBean","data":{"$area":
            0.0,"$color":-100.0,"path":
            "org/apache/zookeeper/server/ConnectionMXBean.html#ConnectionMXBean",
            "title":"ConnectionMXBean 0 Elements,  -  Coverage"},"children":[]},
        {"id":"Stats3683","name":"Stats","data":{"$area":0.0,"$color":
            -100.0,"path":"org/apache/zookeeper/server/Stats.html#Stats",
            "title":"Stats 0 Elements,  -  Coverage"},"children":[]},{"id":
          "Request4416","name":"Request","data":{"$area":152.0,"$color":
            78.289474,"path":
            "org/apache/zookeeper/server/Request.html#Request","title":
            "Request 152 Elements, 78.3% Coverage"},"children":[]},{"id":
          "ServerStats4568","name":"ServerStats","data":{"$area":68.0,
            "$color":95.588234,"path":
            "org/apache/zookeeper/server/ServerStats.html#ServerStats",
            "title":"ServerStats 68 Elements, 95.6% Coverage"},"children":[]},
        {"id":"ServerStats.Provider4568","name":"ServerStats.Provider",
          "data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/zookeeper/server/ServerStats.html#ServerStats.Provider",
            "title":"ServerStats.Provider 0 Elements,  -  Coverage"},
          "children":[]},{"id":"NIOServerCnxn4952","name":"NIOServerCnxn",
          "data":{"$area":491.0,"$color":78.61507,"path":
            "org/apache/zookeeper/server/NIOServerCnxn.html#NIOServerCnxn",
            "title":"NIOServerCnxn 491 Elements, 78.6% Coverage"},"children":
          []},{"id":"NIOServerCnxn.SendBufferWriter5183","name":
          "NIOServerCnxn.SendBufferWriter","data":{"$area":18.0,"$color":
            88.88889,"path":
            "org/apache/zookeeper/server/NIOServerCnxn.html#NIOServerCnxn.SendBufferWriter",
            "title":
            "NIOServerCnxn.SendBufferWriter 18 Elements, 88.9% Coverage"},
          "children":[]},{"id":"NIOServerCnxn.CommandThread5201","name":
          "NIOServerCnxn.CommandThread","data":{"$area":9.0,"$color":
            88.88889,"path":
            "org/apache/zookeeper/server/NIOServerCnxn.html#NIOServerCnxn.CommandThread",
            "title":
            "NIOServerCnxn.CommandThread 9 Elements, 88.9% Coverage"},
          "children":[]},{"id":"NIOServerCnxn.RuokCommand5210","name":
          "NIOServerCnxn.RuokCommand","data":{"$area":4.0,"$color":100.0,
            "path":
            "org/apache/zookeeper/server/NIOServerCnxn.html#NIOServerCnxn.RuokCommand",
            "title":"NIOServerCnxn.RuokCommand 4 Elements, 100% Coverage"},
          "children":[]},{"id":"NIOServerCnxn.TraceMaskCommand5214","name":
          "NIOServerCnxn.TraceMaskCommand","data":{"$area":5.0,"$color":
            0.0,"path":
            "org/apache/zookeeper/server/NIOServerCnxn.html#NIOServerCnxn.TraceMaskCommand",
            "title":
            "NIOServerCnxn.TraceMaskCommand 5 Elements, 0% Coverage"},
          "children":[]},{"id":"NIOServerCnxn.SetTraceMaskCommand5219",
          "name":"NIOServerCnxn.SetTraceMaskCommand","data":{"$area":5.0,
            "$color":0.0,"path":
            "org/apache/zookeeper/server/NIOServerCnxn.html#NIOServerCnxn.SetTraceMaskCommand",
            "title":
            "NIOServerCnxn.SetTraceMaskCommand 5 Elements, 0% Coverage"},
          "children":[]},{"id":"NIOServerCnxn.EnvCommand5224","name":
          "NIOServerCnxn.EnvCommand","data":{"$area":9.0,"$color":100.0,
            "path":
            "org/apache/zookeeper/server/NIOServerCnxn.html#NIOServerCnxn.EnvCommand",
            "title":"NIOServerCnxn.EnvCommand 9 Elements, 100% Coverage"},
          "children":[]},{"id":"NIOServerCnxn.ConfCommand5233","name":
          "NIOServerCnxn.ConfCommand","data":{"$area":8.0,"$color":75.0,
            "path":
            "org/apache/zookeeper/server/NIOServerCnxn.html#NIOServerCnxn.ConfCommand",
            "title":"NIOServerCnxn.ConfCommand 8 Elements, 75% Coverage"},
          "children":[]},{"id":"NIOServerCnxn.StatResetCommand5241","name":
          "NIOServerCnxn.StatResetCommand","data":{"$area":9.0,"$color":
            77.77778,"path":
            "org/apache/zookeeper/server/NIOServerCnxn.html#NIOServerCnxn.StatResetCommand",
            "title":
            "NIOServerCnxn.StatResetCommand 9 Elements, 77.8% Coverage"},
          "children":[]},{"id":"NIOServerCnxn.CnxnStatResetCommand5250",
          "name":"NIOServerCnxn.CnxnStatResetCommand","data":{"$area":10.0,
            "$color":80.0,"path":
            "org/apache/zookeeper/server/NIOServerCnxn.html#NIOServerCnxn.CnxnStatResetCommand",
            "title":
            "NIOServerCnxn.CnxnStatResetCommand 10 Elements, 80% Coverage"},
          "children":[]},{"id":"NIOServerCnxn.DumpCommand5260","name":
          "NIOServerCnxn.DumpCommand","data":{"$area":13.0,"$color":
            84.61539,"path":
            "org/apache/zookeeper/server/NIOServerCnxn.html#NIOServerCnxn.DumpCommand",
            "title":
            "NIOServerCnxn.DumpCommand 13 Elements, 84.6% Coverage"},
          "children":[]},{"id":"NIOServerCnxn.StatCommand5273","name":
          "NIOServerCnxn.StatCommand","data":{"$area":26.0,"$color":100.0,
            "path":
            "org/apache/zookeeper/server/NIOServerCnxn.html#NIOServerCnxn.StatCommand",
            "title":"NIOServerCnxn.StatCommand 26 Elements, 100% Coverage"},
          "children":[]},{"id":"NIOServerCnxn.ConsCommand5299","name":
          "NIOServerCnxn.ConsCommand","data":{"$area":11.0,"$color":
            81.818184,"path":
            "org/apache/zookeeper/server/NIOServerCnxn.html#NIOServerCnxn.ConsCommand",
            "title":
            "NIOServerCnxn.ConsCommand 11 Elements, 81.8% Coverage"},
          "children":[]},{"id":"NIOServerCnxn.WatchCommand5310","name":
          "NIOServerCnxn.WatchCommand","data":{"$area":19.0,"$color":
            89.47369,"path":
            "org/apache/zookeeper/server/NIOServerCnxn.html#NIOServerCnxn.WatchCommand",
            "title":
            "NIOServerCnxn.WatchCommand 19 Elements, 89.5% Coverage"},
          "children":[]},{"id":"NIOServerCnxn.MonitorCommand5329","name":
          "NIOServerCnxn.MonitorCommand","data":{"$area":43.0,"$color":
            90.69768,"path":
            "org/apache/zookeeper/server/NIOServerCnxn.html#NIOServerCnxn.MonitorCommand",
            "title":
            "NIOServerCnxn.MonitorCommand 43 Elements, 90.7% Coverage"},
          "children":[]},{"id":"NIOServerCnxn.IsroCommand5372","name":
          "NIOServerCnxn.IsroCommand","data":{"$area":12.0,"$color":100.0,
            "path":
            "org/apache/zookeeper/server/NIOServerCnxn.html#NIOServerCnxn.IsroCommand",
            "title":"NIOServerCnxn.IsroCommand 12 Elements, 100% Coverage"},
          "children":[]},{"id":"SnapshotFormatter5644","name":
          "SnapshotFormatter","data":{"$area":59.0,"$color":94.91525,"path":
            "org/apache/zookeeper/server/SnapshotFormatter.html#SnapshotFormatter",
            "title":"SnapshotFormatter 59 Elements, 94.9% Coverage"},
          "children":[]},{"id":"FinalRequestProcessor5703","name":
          "FinalRequestProcessor","data":{"$area":274.0,"$color":87.22628,
            "path":
            "org/apache/zookeeper/server/FinalRequestProcessor.html#FinalRequestProcessor",
            "title":"FinalRequestProcessor 274 Elements, 87.2% Coverage"},
          "children":[]},{"id":"UnimplementedRequestProcessor6282","name":
          "UnimplementedRequestProcessor","data":{"$area":9.0,"$color":
            77.77778,"path":
            "org/apache/zookeeper/server/UnimplementedRequestProcessor.html#UnimplementedRequestProcessor",
            "title":
            "UnimplementedRequestProcessor 9 Elements, 77.8% Coverage"},
          "children":[]},{"id":"PurgeTxnLog6291","name":"PurgeTxnLog","data":
          {"$area":49.0,"$color":44.897957,"path":
            "org/apache/zookeeper/server/PurgeTxnLog.html#PurgeTxnLog",
            "title":"PurgeTxnLog 49 Elements, 44.9% Coverage"},"children":[]},
        {"id":"PurgeTxnLog.MyFileFilter6315","name":
          "PurgeTxnLog.MyFileFilter","data":{"$area":8.0,"$color":100.0,
            "path":
            "org/apache/zookeeper/server/PurgeTxnLog.html#PurgeTxnLog.MyFileFilter",
            "title":"PurgeTxnLog.MyFileFilter 8 Elements, 100% Coverage"},
          "children":[]},{"id":"ZKDatabase6348","name":"ZKDatabase","data":{
            "$area":184.0,"$color":91.30435,"path":
            "org/apache/zookeeper/server/ZKDatabase.html#ZKDatabase","title":
            "ZKDatabase 184 Elements, 91.3% Coverage"},"children":[]},{"id":
          "NIOServerCnxnFactory7814","name":"NIOServerCnxnFactory","data":{
            "$area":204.0,"$color":86.7647,"path":
            "org/apache/zookeeper/server/NIOServerCnxnFactory.html#NIOServerCnxnFactory",
            "title":"NIOServerCnxnFactory 204 Elements, 86.8% Coverage"},
          "children":[]},{"id":
          "NIOServerCnxnFactory.AbstractSelectThread7822","name":
          "NIOServerCnxnFactory.AbstractSelectThread","data":{"$area":28.0,
            "$color":50.0,"path":
            "org/apache/zookeeper/server/NIOServerCnxnFactory.html#NIOServerCnxnFactory.AbstractSelectThread",
            "title":
            "NIOServerCnxnFactory.AbstractSelectThread 28 Elements, 50% Coverage"},
          "children":[]},{"id":"NIOServerCnxnFactory.AcceptThread7850",
          "name":"NIOServerCnxnFactory.AcceptThread","data":{"$area":79.0,
            "$color":72.1519,"path":
            "org/apache/zookeeper/server/NIOServerCnxnFactory.html#NIOServerCnxnFactory.AcceptThread",
            "title":
            "NIOServerCnxnFactory.AcceptThread 79 Elements, 72.2% Coverage"},
          "children":[]},{"id":"NIOServerCnxnFactory.SelectorThread7929",
          "name":"NIOServerCnxnFactory.SelectorThread","data":{"$area":
            103.0,"$color":82.52427,"path":
            "org/apache/zookeeper/server/NIOServerCnxnFactory.html#NIOServerCnxnFactory.SelectorThread",
            "title":
            "NIOServerCnxnFactory.SelectorThread 103 Elements, 82.5% Coverage"},
          "children":[]},{"id":"NIOServerCnxnFactory.IOWorkRequest8032",
          "name":"NIOServerCnxnFactory.IOWorkRequest","data":{"$area":32.0,
            "$color":75.0,"path":
            "org/apache/zookeeper/server/NIOServerCnxnFactory.html#NIOServerCnxnFactory.IOWorkRequest",
            "title":
            "NIOServerCnxnFactory.IOWorkRequest 32 Elements, 75% Coverage"},
          "children":[]},{"id":
          "NIOServerCnxnFactory.ConnectionExpirerThread8064","name":
          "NIOServerCnxnFactory.ConnectionExpirerThread","data":{"$area":
            16.0,"$color":93.75,"path":
            "org/apache/zookeeper/server/NIOServerCnxnFactory.html#NIOServerCnxnFactory.ConnectionExpirerThread",
            "title":
            "NIOServerCnxnFactory.ConnectionExpirerThread 16 Elements, 93.8% Coverage"},
          "children":[]},{"id":"DatadirCleanupManager8739","name":
          "DatadirCleanupManager","data":{"$area":40.0,"$color":87.5,"path":
            "org/apache/zookeeper/server/DatadirCleanupManager.html#DatadirCleanupManager",
            "title":"DatadirCleanupManager 40 Elements, 87.5% Coverage"},
          "children":[]},{"id":"DatadirCleanupManager.PurgeTaskStatus8739",
          "name":"DatadirCleanupManager.PurgeTaskStatus","data":{"$area":
            0.0,"$color":-100.0,"path":
            "org/apache/zookeeper/server/DatadirCleanupManager.html#DatadirCleanupManager.PurgeTaskStatus",
            "title":
            "DatadirCleanupManager.PurgeTaskStatus 0 Elements,  -  Coverage"},
          "children":[]},{"id":"DatadirCleanupManager.PurgeTask8769","name":
          "DatadirCleanupManager.PurgeTask","data":{"$area":10.0,"$color":
            90.0,"path":
            "org/apache/zookeeper/server/DatadirCleanupManager.html#DatadirCleanupManager.PurgeTask",
            "title":
            "DatadirCleanupManager.PurgeTask 10 Elements, 90% Coverage"},
          "children":[]},{"id":"ObserverBean8862","name":"ObserverBean",
          "data":{"$area":7.0,"$color":42.857143,"path":
            "org/apache/zookeeper/server/ObserverBean.html#ObserverBean",
            "title":"ObserverBean 7 Elements, 42.9% Coverage"},"children":[]},
        {"id":"SessionTrackerImpl8869","name":"SessionTrackerImpl","data":{
            "$area":140.0,"$color":81.428566,"path":
            "org/apache/zookeeper/server/SessionTrackerImpl.html#SessionTrackerImpl",
            "title":"SessionTrackerImpl 140 Elements, 81.4% Coverage"},
          "children":[]},{"id":"SessionTrackerImpl.SessionImpl8869","name":
          "SessionTrackerImpl.SessionImpl","data":{"$area":12.0,"$color":
            100.0,"path":
            "org/apache/zookeeper/server/SessionTrackerImpl.html#SessionTrackerImpl.SessionImpl",
            "title":
            "SessionTrackerImpl.SessionImpl 12 Elements, 100% Coverage"},
          "children":[]},{"id":"ExpiryQueue9035","name":"ExpiryQueue","data":
          {"$area":89.0,"$color":93.25843,"path":
            "org/apache/zookeeper/server/ExpiryQueue.html#ExpiryQueue",
            "title":"ExpiryQueue 89 Elements, 93.3% Coverage"},"children":[]},
        {"id":"LogFormatter10296","name":"LogFormatter","data":{"$area":
            45.0,"$color":71.111115,"path":
            "org/apache/zookeeper/server/LogFormatter.html#LogFormatter",
            "title":"LogFormatter 45 Elements, 71.1% Coverage"},"children":[]},
        {"id":"ZooKeeperServer11255","name":"ZooKeeperServer","data":{
            "$area":536.0,"$color":84.14179,"path":
            "org/apache/zookeeper/server/ZooKeeperServer.html#ZooKeeperServer",
            "title":"ZooKeeperServer 536 Elements, 84.1% Coverage"},
          "children":[]},{"id":
          "ZooKeeperServer.MissingSessionException11350","name":
          "ZooKeeperServer.MissingSessionException","data":{"$area":2.0,
            "$color":100.0,"path":
            "org/apache/zookeeper/server/ZooKeeperServer.html#ZooKeeperServer.MissingSessionException",
            "title":
            "ZooKeeperServer.MissingSessionException 2 Elements, 100% Coverage"},
          "children":[]},{"id":"ZooKeeperServer.ChangeRecord11443","name":
          "ZooKeeperServer.ChangeRecord","data":{"$area":15.0,"$color":
            86.666664,"path":
            "org/apache/zookeeper/server/ZooKeeperServer.html#ZooKeeperServer.ChangeRecord",
            "title":
            "ZooKeeperServer.ChangeRecord 15 Elements, 86.7% Coverage"},
          "children":[]},{"id":"DataNode11814","name":"DataNode","data":{
            "$area":59.0,"$color":89.830505,"path":
            "org/apache/zookeeper/server/DataNode.html#DataNode","title":
            "DataNode 59 Elements, 89.8% Coverage"},"children":[]},{"id":
          "ServerCnxnFactory12208","name":"ServerCnxnFactory","data":{
            "$area":72.0,"$color":75.0,"path":
            "org/apache/zookeeper/server/ServerCnxnFactory.html#ServerCnxnFactory",
            "title":"ServerCnxnFactory 72 Elements, 75% Coverage"},
          "children":[]},{"id":"ServerCnxnFactory.PacketProcessor12208",
          "name":"ServerCnxnFactory.PacketProcessor","data":{"$area":0.0,
            "$color":-100.0,"path":
            "org/apache/zookeeper/server/ServerCnxnFactory.html#ServerCnxnFactory.PacketProcessor",
            "title":
            "ServerCnxnFactory.PacketProcessor 0 Elements,  -  Coverage"},
          "children":[]},{"id":"DataTreeBean12287","name":"DataTreeBean",
          "data":{"$area":16.0,"$color":37.5,"path":
            "org/apache/zookeeper/server/DataTreeBean.html#DataTreeBean",
            "title":"DataTreeBean 16 Elements, 37.5% Coverage"},"children":[]},
        {"id":"ByteBufferOutputStream13236","name":
          "ByteBufferOutputStream","data":{"$area":12.0,"$color":83.33333,
            "path":
            "org/apache/zookeeper/server/ByteBufferOutputStream.html#ByteBufferOutputStream",
            "title":"ByteBufferOutputStream 12 Elements, 83.3% Coverage"},
          "children":[]},{"id":"RateLogger13371","name":"RateLogger","data":{
            "$area":32.0,"$color":21.875,"path":
            "org/apache/zookeeper/server/RateLogger.html#RateLogger","title":
            "RateLogger 32 Elements, 21.9% Coverage"},"children":[]},{"id":
          "NettyServerCnxnFactory14178","name":"NettyServerCnxnFactory",
          "data":{"$area":100.0,"$color":73.0,"path":
            "org/apache/zookeeper/server/NettyServerCnxnFactory.html#NettyServerCnxnFactory",
            "title":"NettyServerCnxnFactory 100 Elements, 73% Coverage"},
          "children":[]},{"id":
          "NettyServerCnxnFactory.CnxnChannelHandler14178","name":
          "NettyServerCnxnFactory.CnxnChannelHandler","data":{"$area":
            136.0,"$color":71.32353,"path":
            "org/apache/zookeeper/server/NettyServerCnxnFactory.html#NettyServerCnxnFactory.CnxnChannelHandler",
            "title":
            "NettyServerCnxnFactory.CnxnChannelHandler 136 Elements, 71.3% Coverage"},
          "children":[]},{"id":"RequestProcessor14492","name":
          "RequestProcessor","data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/zookeeper/server/RequestProcessor.html#RequestProcessor",
            "title":"RequestProcessor 0 Elements,  -  Coverage"},"children":[]},
        {"id":"RequestProcessor.RequestProcessorException14492","name":
          "RequestProcessor.RequestProcessorException","data":{"$area":2.0,
            "$color":0.0,"path":
            "org/apache/zookeeper/server/RequestProcessor.html#RequestProcessor.RequestProcessorException",
            "title":
            "RequestProcessor.RequestProcessorException 2 Elements, 0% Coverage"},
          "children":[]},{"id":"TxnLogProposalIterator14575","name":
          "TxnLogProposalIterator","data":{"$area":31.0,"$color":83.870964,
            "path":
            "org/apache/zookeeper/server/TxnLogProposalIterator.html#TxnLogProposalIterator",
            "title":"TxnLogProposalIterator 31 Elements, 83.9% Coverage"},
          "children":[]},{"id":"TraceFormatter14606","name":
          "TraceFormatter","data":{"$area":70.0,"$color":5.714286,"path":
            "org/apache/zookeeper/server/TraceFormatter.html#TraceFormatter",
            "title":"TraceFormatter 70 Elements, 5.7% Coverage"},"children":[]},
        {"id":"WatchManager14927","name":"WatchManager","data":{"$area":
            88.0,"$color":92.045456,"path":
            "org/apache/zookeeper/server/WatchManager.html#WatchManager",
            "title":"WatchManager 88 Elements, 92% Coverage"},"children":[]},
        {"id":"SessionTracker15015","name":"SessionTracker","data":{"$area":
            0.0,"$color":-100.0,"path":
            "org/apache/zookeeper/server/SessionTracker.html#SessionTracker",
            "title":"SessionTracker 0 Elements,  -  Coverage"},"children":[]},
        {"id":"SessionTracker.Session15015","name":
          "SessionTracker.Session","data":{"$area":0.0,"$color":-100.0,
            "path":
            "org/apache/zookeeper/server/SessionTracker.html#SessionTracker.Session",
            "title":"SessionTracker.Session 0 Elements,  -  Coverage"},
          "children":[]},{"id":"SessionTracker.SessionExpirer15015","name":
          "SessionTracker.SessionExpirer","data":{"$area":0.0,"$color":
            -100.0,"path":
            "org/apache/zookeeper/server/SessionTracker.html#SessionTracker.SessionExpirer",
            "title":
            "SessionTracker.SessionExpirer 0 Elements,  -  Coverage"},
          "children":[]},{"id":"ZooKeeperSaslServer15189","name":
          "ZooKeeperSaslServer","data":{"$area":64.0,"$color":28.125,"path":
            "org/apache/zookeeper/server/ZooKeeperSaslServer.html#ZooKeeperSaslServer",
            "title":"ZooKeeperSaslServer 64 Elements, 28.1% Coverage"},
          "children":[]},{"id":"ServerConfig15253","name":"ServerConfig",
          "data":{"$area":42.0,"$color":38.095238,"path":
            "org/apache/zookeeper/server/ServerConfig.html#ServerConfig",
            "title":"ServerConfig 42 Elements, 38.1% Coverage"},"children":[]},
        {"id":"ZooKeeperServerMain15428","name":"ZooKeeperServerMain","data":
          {"$area":44.0,"$color":56.81818,"path":
            "org/apache/zookeeper/server/ZooKeeperServerMain.html#ZooKeeperServerMain",
            "title":"ZooKeeperServerMain 44 Elements, 56.8% Coverage"},
          "children":[]},{"id":"NettyServerCnxn16944","name":
          "NettyServerCnxn","data":{"$area":321.0,"$color":77.57009,"path":
            "org/apache/zookeeper/server/NettyServerCnxn.html#NettyServerCnxn",
            "title":"NettyServerCnxn 321 Elements, 77.6% Coverage"},
          "children":[]},{"id":"NettyServerCnxn.ResumeMessageEvent16996",
          "name":"NettyServerCnxn.ResumeMessageEvent","data":{"$area":10.0,
            "$color":20.0,"path":
            "org/apache/zookeeper/server/NettyServerCnxn.html#NettyServerCnxn.ResumeMessageEvent",
            "title":
            "NettyServerCnxn.ResumeMessageEvent 10 Elements, 20% Coverage"},
          "children":[]},{"id":"NettyServerCnxn.SendBufferWriter17064",
          "name":"NettyServerCnxn.SendBufferWriter","data":{"$area":18.0,
            "$color":88.88889,"path":
            "org/apache/zookeeper/server/NettyServerCnxn.html#NettyServerCnxn.SendBufferWriter",
            "title":
            "NettyServerCnxn.SendBufferWriter 18 Elements, 88.9% Coverage"},
          "children":[]},{"id":"NettyServerCnxn.CommandThread17082","name":
          "NettyServerCnxn.CommandThread","data":{"$area":9.0,"$color":
            88.88889,"path":
            "org/apache/zookeeper/server/NettyServerCnxn.html#NettyServerCnxn.CommandThread",
            "title":
            "NettyServerCnxn.CommandThread 9 Elements, 88.9% Coverage"},
          "children":[]},{"id":"NettyServerCnxn.RuokCommand17091","name":
          "NettyServerCnxn.RuokCommand","data":{"$area":4.0,"$color":100.0,
            "path":
            "org/apache/zookeeper/server/NettyServerCnxn.html#NettyServerCnxn.RuokCommand",
            "title":
            "NettyServerCnxn.RuokCommand 4 Elements, 100% Coverage"},
          "children":[]},{"id":"NettyServerCnxn.TraceMaskCommand17095",
          "name":"NettyServerCnxn.TraceMaskCommand","data":{"$area":5.0,
            "$color":0.0,"path":
            "org/apache/zookeeper/server/NettyServerCnxn.html#NettyServerCnxn.TraceMaskCommand",
            "title":
            "NettyServerCnxn.TraceMaskCommand 5 Elements, 0% Coverage"},
          "children":[]},{"id":"NettyServerCnxn.SetTraceMaskCommand17100",
          "name":"NettyServerCnxn.SetTraceMaskCommand","data":{"$area":5.0,
            "$color":0.0,"path":
            "org/apache/zookeeper/server/NettyServerCnxn.html#NettyServerCnxn.SetTraceMaskCommand",
            "title":
            "NettyServerCnxn.SetTraceMaskCommand 5 Elements, 0% Coverage"},
          "children":[]},{"id":"NettyServerCnxn.EnvCommand17105","name":
          "NettyServerCnxn.EnvCommand","data":{"$area":9.0,"$color":100.0,
            "path":
            "org/apache/zookeeper/server/NettyServerCnxn.html#NettyServerCnxn.EnvCommand",
            "title":"NettyServerCnxn.EnvCommand 9 Elements, 100% Coverage"},
          "children":[]},{"id":"NettyServerCnxn.ConfCommand17114","name":
          "NettyServerCnxn.ConfCommand","data":{"$area":8.0,"$color":75.0,
            "path":
            "org/apache/zookeeper/server/NettyServerCnxn.html#NettyServerCnxn.ConfCommand",
            "title":"NettyServerCnxn.ConfCommand 8 Elements, 75% Coverage"},
          "children":[]},{"id":"NettyServerCnxn.StatResetCommand17122",
          "name":"NettyServerCnxn.StatResetCommand","data":{"$area":9.0,
            "$color":77.77778,"path":
            "org/apache/zookeeper/server/NettyServerCnxn.html#NettyServerCnxn.StatResetCommand",
            "title":
            "NettyServerCnxn.StatResetCommand 9 Elements, 77.8% Coverage"},
          "children":[]},{"id":"NettyServerCnxn.CnxnStatResetCommand17131",
          "name":"NettyServerCnxn.CnxnStatResetCommand","data":{"$area":
            11.0,"$color":81.818184,"path":
            "org/apache/zookeeper/server/NettyServerCnxn.html#NettyServerCnxn.CnxnStatResetCommand",
            "title":
            "NettyServerCnxn.CnxnStatResetCommand 11 Elements, 81.8% Coverage"},
          "children":[]},{"id":"NettyServerCnxn.DumpCommand17142","name":
          "NettyServerCnxn.DumpCommand","data":{"$area":11.0,"$color":
            81.818184,"path":
            "org/apache/zookeeper/server/NettyServerCnxn.html#NettyServerCnxn.DumpCommand",
            "title":
            "NettyServerCnxn.DumpCommand 11 Elements, 81.8% Coverage"},
          "children":[]},{"id":"NettyServerCnxn.StatCommand17153","name":
          "NettyServerCnxn.StatCommand","data":{"$area":29.0,"$color":
            93.10345,"path":
            "org/apache/zookeeper/server/NettyServerCnxn.html#NettyServerCnxn.StatCommand",
            "title":
            "NettyServerCnxn.StatCommand 29 Elements, 93.1% Coverage"},
          "children":[]},{"id":"NettyServerCnxn.ConsCommand17182","name":
          "NettyServerCnxn.ConsCommand","data":{"$area":14.0,"$color":
            85.71429,"path":
            "org/apache/zookeeper/server/NettyServerCnxn.html#NettyServerCnxn.ConsCommand",
            "title":
            "NettyServerCnxn.ConsCommand 14 Elements, 85.7% Coverage"},
          "children":[]},{"id":"NettyServerCnxn.WatchCommand17196","name":
          "NettyServerCnxn.WatchCommand","data":{"$area":19.0,"$color":
            89.47369,"path":
            "org/apache/zookeeper/server/NettyServerCnxn.html#NettyServerCnxn.WatchCommand",
            "title":
            "NettyServerCnxn.WatchCommand 19 Elements, 89.5% Coverage"},
          "children":[]},{"id":"NettyServerCnxn.MonitorCommand17215","name":
          "NettyServerCnxn.MonitorCommand","data":{"$area":43.0,"$color":
            79.06977,"path":
            "org/apache/zookeeper/server/NettyServerCnxn.html#NettyServerCnxn.MonitorCommand",
            "title":
            "NettyServerCnxn.MonitorCommand 43 Elements, 79.1% Coverage"},
          "children":[]},{"id":"NettyServerCnxn.IsroCommand17258","name":
          "NettyServerCnxn.IsroCommand","data":{"$area":12.0,"$color":0.0,
            "path":
            "org/apache/zookeeper/server/NettyServerCnxn.html#NettyServerCnxn.IsroCommand",
            "title":"NettyServerCnxn.IsroCommand 12 Elements, 0% Coverage"},
          "children":[]},{"id":"ZooKeeperServerBean18026","name":
          "ZooKeeperServerBean","data":{"$area":59.0,"$color":13.559322,
            "path":
            "org/apache/zookeeper/server/ZooKeeperServerBean.html#ZooKeeperServerBean",
            "title":"ZooKeeperServerBean 59 Elements, 13.6% Coverage"},
          "children":[]},{"id":"ServerCnxn18710","name":"ServerCnxn","data":{
            "$area":159.0,"$color":91.19497,"path":
            "org/apache/zookeeper/server/ServerCnxn.html#ServerCnxn","title":
            "ServerCnxn 159 Elements, 91.2% Coverage"},"children":[]},{"id":
          "ServerCnxn.CloseRequestException18719","name":
          "ServerCnxn.CloseRequestException","data":{"$area":2.0,"$color":
            100.0,"path":
            "org/apache/zookeeper/server/ServerCnxn.html#ServerCnxn.CloseRequestException",
            "title":
            "ServerCnxn.CloseRequestException 2 Elements, 100% Coverage"},
          "children":[]},{"id":"ServerCnxn.EndOfStreamException18721","name":
          "ServerCnxn.EndOfStreamException","data":{"$area":4.0,"$color":
            50.0,"path":
            "org/apache/zookeeper/server/ServerCnxn.html#ServerCnxn.EndOfStreamException",
            "title":
            "ServerCnxn.EndOfStreamException 4 Elements, 50% Coverage"},
          "children":[]},{"id":"SyncRequestProcessor19061","name":
          "SyncRequestProcessor","data":{"$area":106.0,"$color":85.84906,
            "path":
            "org/apache/zookeeper/server/SyncRequestProcessor.html#SyncRequestProcessor",
            "title":"SyncRequestProcessor 106 Elements, 85.8% Coverage"},
          "children":[]}]},{"id":
      "org.apache.zookeeper.server.persistence6291","name":
      "org.apache.zookeeper.server.persistence","data":{"$area":686.0,
        "$color":76.96793,"title":
        "org.apache.zookeeper.server.persistence 686 Elements, 77% Coverage"},
      "children":[{"id":"TxnLog6291","name":"TxnLog","data":{"$area":0.0,
            "$color":-100.0,"path":
            "org/apache/zookeeper/server/persistence/TxnLog.html#TxnLog",
            "title":"TxnLog 0 Elements,  -  Coverage"},"children":[]},{"id":
          "TxnLog.TxnIterator6291","name":"TxnLog.TxnIterator","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/zookeeper/server/persistence/TxnLog.html#TxnLog.TxnIterator",
            "title":"TxnLog.TxnIterator 0 Elements,  -  Coverage"},
          "children":[]},{"id":"Util7313","name":"Util","data":{"$area":
            107.0,"$color":69.158875,"path":
            "org/apache/zookeeper/server/persistence/Util.html#Util","title":
            "Util 107 Elements, 69.2% Coverage"},"children":[]},{"id":
          "Util.DataDirFileComparator7412","name":
          "Util.DataDirFileComparator","data":{"$area":14.0,"$color":100.0,
            "path":
            "org/apache/zookeeper/server/persistence/Util.html#Util.DataDirFileComparator",
            "title":
            "Util.DataDirFileComparator 14 Elements, 100% Coverage"},
          "children":[]},{"id":"SnapShot9021","name":"SnapShot","data":{
            "$area":0.0,"$color":-100.0,"path":
            "org/apache/zookeeper/server/persistence/SnapShot.html#SnapShot",
            "title":"SnapShot 0 Elements,  -  Coverage"},"children":[]},{
          "id":"FileTxnSnapLog15295","name":"FileTxnSnapLog","data":{"$area":
            129.0,"$color":78.29458,"path":
            "org/apache/zookeeper/server/persistence/FileTxnSnapLog.html#FileTxnSnapLog",
            "title":"FileTxnSnapLog 129 Elements, 78.3% Coverage"},
          "children":[]},{"id":"FileTxnSnapLog.PlayBackListener15295","name":
          "FileTxnSnapLog.PlayBackListener","data":{"$area":0.0,"$color":
            -100.0,"path":
            "org/apache/zookeeper/server/persistence/FileTxnSnapLog.html#FileTxnSnapLog.PlayBackListener",
            "title":
            "FileTxnSnapLog.PlayBackListener 0 Elements,  -  Coverage"},
          "children":[]},{"id":"FileTxnSnapLog.DatadirException15424","name":
          "FileTxnSnapLog.DatadirException","data":{"$area":4.0,"$color":
            50.0,"path":
            "org/apache/zookeeper/server/persistence/FileTxnSnapLog.html#FileTxnSnapLog.DatadirException",
            "title":
            "FileTxnSnapLog.DatadirException 4 Elements, 50% Coverage"},
          "children":[]},{"id":"FileTxnLog16426","name":"FileTxnLog","data":{
            "$area":173.0,"$color":67.63006,"path":
            "org/apache/zookeeper/server/persistence/FileTxnLog.html#FileTxnLog",
            "title":"FileTxnLog 173 Elements, 67.6% Coverage"},"children":[]},
        {"id":"FileTxnLog.PositionInputStream16599","name":
          "FileTxnLog.PositionInputStream","data":{"$area":39.0,"$color":
            46.153847,"path":
            "org/apache/zookeeper/server/persistence/FileTxnLog.html#FileTxnLog.PositionInputStream",
            "title":
            "FileTxnLog.PositionInputStream 39 Elements, 46.2% Coverage"},
          "children":[]},{"id":"FileTxnLog.FileTxnIterator16638","name":
          "FileTxnLog.FileTxnIterator","data":{"$area":107.0,"$color":
            92.52337,"path":
            "org/apache/zookeeper/server/persistence/FileTxnLog.html#FileTxnLog.FileTxnIterator",
            "title":
            "FileTxnLog.FileTxnIterator 107 Elements, 92.5% Coverage"},
          "children":[]},{"id":"FileSnap16745","name":"FileSnap","data":{
            "$area":113.0,"$color":91.150444,"path":
            "org/apache/zookeeper/server/persistence/FileSnap.html#FileSnap",
            "title":"FileSnap 113 Elements, 91.2% Coverage"},"children":[]}]},
    {"id":"org.apache.zookeeper.common1549","name":
      "org.apache.zookeeper.common","data":{"$area":311.0,"$color":
        76.84888,"title":
        "org.apache.zookeeper.common 311 Elements, 76.8% Coverage"},
      "children":[{"id":"PathUtils1549","name":"PathUtils","data":{"$area":
            68.0,"$color":100.0,"path":
            "org/apache/zookeeper/common/PathUtils.html#PathUtils","title":
            "PathUtils 68 Elements, 100% Coverage"},"children":[]},{"id":
          "AtomicFileOutputStream4636","name":"AtomicFileOutputStream",
          "data":{"$area":39.0,"$color":71.794876,"path":
            "org/apache/zookeeper/common/AtomicFileOutputStream.html#AtomicFileOutputStream",
            "title":"AtomicFileOutputStream 39 Elements, 71.8% Coverage"},
          "children":[]},{"id":"IOUtils11099","name":"IOUtils","data":{
            "$area":43.0,"$color":79.06977,"path":
            "org/apache/zookeeper/common/IOUtils.html#IOUtils","title":
            "IOUtils 43 Elements, 79.1% Coverage"},"children":[]},{"id":
          "StringUtils13349","name":"StringUtils","data":{"$area":22.0,
            "$color":95.454544,"path":
            "org/apache/zookeeper/common/StringUtils.html#StringUtils",
            "title":"StringUtils 22 Elements, 95.5% Coverage"},"children":[]},
        {"id":"PathTrie15039","name":"PathTrie","data":{"$area":91.0,
            "$color":63.736267,"path":
            "org/apache/zookeeper/common/PathTrie.html#PathTrie","title":
            "PathTrie 91 Elements, 63.7% Coverage"},"children":[]},{"id":
          "PathTrie.TrieNode15039","name":"PathTrie.TrieNode","data":{
            "$area":48.0,"$color":62.5,"path":
            "org/apache/zookeeper/common/PathTrie.html#PathTrie.TrieNode",
            "title":"PathTrie.TrieNode 48 Elements, 62.5% Coverage"},
          "children":[]}]},{"id":"org.apache.zookeeper.client4354","name":
      "org.apache.zookeeper.client","data":{"$area":501.0,"$color":
        69.26148,"title":
        "org.apache.zookeeper.client 501 Elements, 69.3% Coverage"},
      "children":[{"id":"ConnectStringParser4354","name":
          "ConnectStringParser","data":{"$area":31.0,"$color":90.32258,
            "path":
            "org/apache/zookeeper/client/ConnectStringParser.html#ConnectStringParser",
            "title":"ConnectStringParser 31 Elements, 90.3% Coverage"},
          "children":[]},{"id":"FourLetterWordMain8677","name":
          "FourLetterWordMain","data":{"$area":28.0,"$color":82.14286,"path":
            
            "org/apache/zookeeper/client/FourLetterWordMain.html#FourLetterWordMain",
            "title":"FourLetterWordMain 28 Elements, 82.1% Coverage"},
          "children":[]},{"id":"ZooKeeperSaslClient11873","name":
          "ZooKeeperSaslClient","data":{"$area":258.0,"$color":59.302322,
            "path":
            "org/apache/zookeeper/client/ZooKeeperSaslClient.html#ZooKeeperSaslClient",
            "title":"ZooKeeperSaslClient 258 Elements, 59.3% Coverage"},
          "children":[]},{"id":"ZooKeeperSaslClient.SaslState11875","name":
          "ZooKeeperSaslClient.SaslState","data":{"$area":0.0,"$color":
            -100.0,"path":
            "org/apache/zookeeper/client/ZooKeeperSaslClient.html#ZooKeeperSaslClient.SaslState",
            "title":
            "ZooKeeperSaslClient.SaslState 0 Elements,  -  Coverage"},
          "children":[]},{"id":
          "ZooKeeperSaslClient.ServerSaslResponseCallback11928","name":
          "ZooKeeperSaslClient.ServerSaslResponseCallback","data":{"$area":
            15.0,"$color":0.0,"path":
            "org/apache/zookeeper/client/ZooKeeperSaslClient.html#ZooKeeperSaslClient.ServerSaslResponseCallback",
            "title":
            "ZooKeeperSaslClient.ServerSaslResponseCallback 15 Elements, 0% Coverage"},
          "children":[]},{"id":
          "ZooKeeperSaslClient.ClientCallbackHandler12120","name":
          "ZooKeeperSaslClient.ClientCallbackHandler","data":{"$area":39.0,
            "$color":51.282055,"path":
            "org/apache/zookeeper/client/ZooKeeperSaslClient.html#ZooKeeperSaslClient.ClientCallbackHandler",
            "title":
            "ZooKeeperSaslClient.ClientCallbackHandler 39 Elements, 51.3% Coverage"},
          "children":[]},{"id":"StaticHostProvider15505","name":
          "StaticHostProvider","data":{"$area":130.0,"$color":94.61538,
            "path":
            "org/apache/zookeeper/client/StaticHostProvider.html#StaticHostProvider",
            "title":"StaticHostProvider 130 Elements, 94.6% Coverage"},
          "children":[]},{"id":"HostProvider17748","name":"HostProvider",
          "data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/zookeeper/client/HostProvider.html#HostProvider",
            "title":"HostProvider 0 Elements,  -  Coverage"},"children":[]}]},
    {"id":"org.apache.jute.compiler.generated19167","name":
      "org.apache.jute.compiler.generated","data":{"$area":1633.0,"$color":
        0.0,"title":
        "org.apache.jute.compiler.generated 1633 Elements, 0% Coverage"},
      "children":[{"id":"SimpleCharStream19167","name":"SimpleCharStream",
          "data":{"$area":256.0,"$color":0.0,"path":
            "org/apache/jute/compiler/generated/SimpleCharStream.html#SimpleCharStream",
            "title":"SimpleCharStream 256 Elements, 0% Coverage"},"children":
          []},{"id":"Token19423","name":"Token","data":{"$area":16.0,
            "$color":0.0,"path":
            "org/apache/jute/compiler/generated/Token.html#Token","title":
            "Token 16 Elements, 0% Coverage"},"children":[]},{"id":
          "RccConstants19439","name":"RccConstants","data":{"$area":0.0,
            "$color":-100.0,"path":
            "org/apache/jute/compiler/generated/RccConstants.html#RccConstants",
            "title":"RccConstants 0 Elements,  -  Coverage"},"children":[]},{
          "id":"RccTokenManager19439","name":"RccTokenManager","data":{
            "$area":742.0,"$color":0.0,"path":
            "org/apache/jute/compiler/generated/RccTokenManager.html#RccTokenManager",
            "title":"RccTokenManager 742 Elements, 0% Coverage"},"children":[]},
        {"id":"TokenMgrError20181","name":"TokenMgrError","data":{"$area":
            54.0,"$color":0.0,"path":
            "org/apache/jute/compiler/generated/TokenMgrError.html#TokenMgrError",
            "title":"TokenMgrError 54 Elements, 0% Coverage"},"children":[]},
        {"id":"ParseException20235","name":"ParseException","data":{"$area":
            99.0,"$color":0.0,"path":
            "org/apache/jute/compiler/generated/ParseException.html#ParseException",
            "title":"ParseException 99 Elements, 0% Coverage"},"children":[]},
        {"id":"Rcc20334","name":"Rcc","data":{"$area":466.0,"$color":0.0,
            "path":"org/apache/jute/compiler/generated/Rcc.html#Rcc","title":
            "Rcc 466 Elements, 0% Coverage"},"children":[]}]}]}

 ); 