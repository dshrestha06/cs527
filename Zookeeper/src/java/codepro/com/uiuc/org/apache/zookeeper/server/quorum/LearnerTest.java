package com.uiuc.org.apache.zookeeper.server.quorum;

import java.io.EOFException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.LinkedList;
import java.util.List;
import org.apache.zookeeper.data.Id;
import org.apache.zookeeper.server.NIOServerCnxn;
import org.apache.zookeeper.server.NIOServerCnxnFactory;
import org.apache.zookeeper.server.Request;
import org.apache.zookeeper.server.ServerCnxn;
import org.apache.zookeeper.server.ZooKeeperServer;
import org.apache.zookeeper.server.quorum.Learner;
import org.apache.zookeeper.server.quorum.QuorumPacket;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>LearnerTest</code> contains tests for the class <code>{@link Learner}</code>.
 *
 * @generatedBy CodePro at 10/16/13 8:00 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class LearnerTest {
	/**
	 * Run the Learner() constructor test.
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testLearner_1()
		throws Exception {
		Learner result = new Learner();
		assertNotNull(result);
		// add additional test code here
	}

	/**
	 * Run the void connectToLeader(InetSocketAddress) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testConnectToLeader_1()
		throws Exception {
		Learner fixture = new Learner();
		InetSocketAddress addr = new InetSocketAddress(1);

		fixture.connectToLeader(addr);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.connectToLeader(Learner.java:221)
	}

	/**
	 * Run the void connectToLeader(InetSocketAddress) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testConnectToLeader_2()
		throws Exception {
		Learner fixture = new Learner();
		InetSocketAddress addr = new InetSocketAddress(1);

		fixture.connectToLeader(addr);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.connectToLeader(Learner.java:221)
	}

	/**
	 * Run the void connectToLeader(InetSocketAddress) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testConnectToLeader_3()
		throws Exception {
		Learner fixture = new Learner();
		InetSocketAddress addr = new InetSocketAddress(1);

		fixture.connectToLeader(addr);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.connectToLeader(Learner.java:221)
	}

	/**
	 * Run the void connectToLeader(InetSocketAddress) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testConnectToLeader_4()
		throws Exception {
		Learner fixture = new Learner();
		InetSocketAddress addr = new InetSocketAddress(1);

		fixture.connectToLeader(addr);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.connectToLeader(Learner.java:221)
	}

	/**
	 * Run the void connectToLeader(InetSocketAddress) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testConnectToLeader_5()
		throws Exception {
		Learner fixture = new Learner();
		InetSocketAddress addr = new InetSocketAddress(1);

		fixture.connectToLeader(addr);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.connectToLeader(Learner.java:221)
	}

	/**
	 * Run the void connectToLeader(InetSocketAddress) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testConnectToLeader_6()
		throws Exception {
		Learner fixture = new Learner();
		InetSocketAddress addr = new InetSocketAddress(1);

		fixture.connectToLeader(addr);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.connectToLeader(Learner.java:221)
	}

	/**
	 * Run the void connectToLeader(InetSocketAddress) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testConnectToLeader_7()
		throws Exception {
		Learner fixture = new Learner();
		InetSocketAddress addr = new InetSocketAddress(1);

		fixture.connectToLeader(addr);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.connectToLeader(Learner.java:221)
	}

	/**
	 * Run the void connectToLeader(InetSocketAddress) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testConnectToLeader_8()
		throws Exception {
		Learner fixture = new Learner();
		InetSocketAddress addr = new InetSocketAddress(1);

		fixture.connectToLeader(addr);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.connectToLeader(Learner.java:221)
	}

	/**
	 * Run the InetSocketAddress findLeader() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testFindLeader_1()
		throws Exception {
		Learner fixture = new Learner();

		InetSocketAddress result = fixture.findLeader();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.findLeader(Learner.java:196)
		assertNotNull(result);
	}

	/**
	 * Run the InetSocketAddress findLeader() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testFindLeader_2()
		throws Exception {
		Learner fixture = new Learner();

		InetSocketAddress result = fixture.findLeader();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.findLeader(Learner.java:196)
		assertNotNull(result);
	}

	/**
	 * Run the InetSocketAddress findLeader() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testFindLeader_3()
		throws Exception {
		Learner fixture = new Learner();

		InetSocketAddress result = fixture.findLeader();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.findLeader(Learner.java:196)
		assertNotNull(result);
	}

	/**
	 * Run the int getPendingRevalidationsCount() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testGetPendingRevalidationsCount_1()
		throws Exception {
		Learner fixture = new Learner();

		int result = fixture.getPendingRevalidationsCount();

		// add additional test code here
		assertEquals(0, result);
	}

	/**
	 * Run the Socket getSocket() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testGetSocket_1()
		throws Exception {
		Learner fixture = new Learner();

		Socket result = fixture.getSocket();

		// add additional test code here
		assertEquals(null, result);
	}

	/**
	 * Run the void ping(QuorumPacket) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testPing_1()
		throws Exception {
		Learner fixture = new Learner();
		QuorumPacket qp = new QuorumPacket();

		fixture.ping(qp);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.ping(Learner.java:459)
	}

	/**
	 * Run the void ping(QuorumPacket) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testPing_2()
		throws Exception {
		Learner fixture = new Learner();
		QuorumPacket qp = new QuorumPacket();

		fixture.ping(qp);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.ping(Learner.java:459)
	}

	/**
	 * Run the void ping(QuorumPacket) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testPing_3()
		throws Exception {
		Learner fixture = new Learner();
		QuorumPacket qp = new QuorumPacket();

		fixture.ping(qp);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.ping(Learner.java:459)
	}

	/**
	 * Run the void ping(QuorumPacket) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testPing_4()
		throws Exception {
		Learner fixture = new Learner();
		QuorumPacket qp = new QuorumPacket();

		fixture.ping(qp);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.ping(Learner.java:459)
	}

	/**
	 * Run the void ping(QuorumPacket) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testPing_5()
		throws Exception {
		Learner fixture = new Learner();
		QuorumPacket qp = new QuorumPacket();

		fixture.ping(qp);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.ping(Learner.java:459)
	}

	/**
	 * Run the void readPacket(QuorumPacket) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testReadPacket_1()
		throws Exception {
		Learner fixture = new Learner();
		QuorumPacket pp = new QuorumPacket();

		fixture.readPacket(pp);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.readPacket(Learner.java:151)
	}

	/**
	 * Run the void readPacket(QuorumPacket) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testReadPacket_2()
		throws Exception {
		Learner fixture = new Learner();
		QuorumPacket pp = new QuorumPacket(5, 1L, new byte[] {}, new LinkedList());

		fixture.readPacket(pp);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.readPacket(Learner.java:151)
	}

	/**
	 * Run the void readPacket(QuorumPacket) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testReadPacket_3()
		throws Exception {
		Learner fixture = new Learner();
		QuorumPacket pp = new QuorumPacket(1, 1L, new byte[] {}, new LinkedList());

		fixture.readPacket(pp);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.readPacket(Learner.java:151)
	}

	/**
	 * Run the void readPacket(QuorumPacket) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testReadPacket_4()
		throws Exception {
		Learner fixture = new Learner();
		QuorumPacket pp = new QuorumPacket(5, 1L, new byte[] {}, new LinkedList());

		fixture.readPacket(pp);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.readPacket(Learner.java:151)
	}

	/**
	 * Run the void readPacket(QuorumPacket) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testReadPacket_5()
		throws Exception {
		Learner fixture = new Learner();
		QuorumPacket pp = new QuorumPacket(1, 1L, new byte[] {}, new LinkedList());

		fixture.readPacket(pp);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.readPacket(Learner.java:151)
	}

	/**
	 * Run the long registerWithLeader(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testRegisterWithLeader_1()
		throws Exception {
		Learner fixture = new Learner();
		int pktType = 1;

		long result = fixture.registerWithLeader(pktType);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.registerWithLeader(Learner.java:257)
		assertEquals(0L, result);
	}

	/**
	 * Run the long registerWithLeader(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testRegisterWithLeader_2()
		throws Exception {
		Learner fixture = new Learner();
		int pktType = 1;

		long result = fixture.registerWithLeader(pktType);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.registerWithLeader(Learner.java:257)
		assertEquals(0L, result);
	}

	/**
	 * Run the long registerWithLeader(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testRegisterWithLeader_3()
		throws Exception {
		Learner fixture = new Learner();
		int pktType = 1;

		long result = fixture.registerWithLeader(pktType);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.registerWithLeader(Learner.java:257)
		assertEquals(0L, result);
	}

	/**
	 * Run the long registerWithLeader(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testRegisterWithLeader_4()
		throws Exception {
		Learner fixture = new Learner();
		int pktType = 1;

		long result = fixture.registerWithLeader(pktType);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.registerWithLeader(Learner.java:257)
		assertEquals(0L, result);
	}

	/**
	 * Run the long registerWithLeader(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testRegisterWithLeader_5()
		throws Exception {
		Learner fixture = new Learner();
		int pktType = 1;

		long result = fixture.registerWithLeader(pktType);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.registerWithLeader(Learner.java:257)
		assertEquals(0L, result);
	}

	/**
	 * Run the long registerWithLeader(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testRegisterWithLeader_6()
		throws Exception {
		Learner fixture = new Learner();
		int pktType = 1;

		long result = fixture.registerWithLeader(pktType);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.registerWithLeader(Learner.java:257)
		assertEquals(0L, result);
	}

	/**
	 * Run the long registerWithLeader(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testRegisterWithLeader_7()
		throws Exception {
		Learner fixture = new Learner();
		int pktType = 1;

		long result = fixture.registerWithLeader(pktType);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.registerWithLeader(Learner.java:257)
		assertEquals(0L, result);
	}

	/**
	 * Run the long registerWithLeader(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testRegisterWithLeader_8()
		throws Exception {
		Learner fixture = new Learner();
		int pktType = 1;

		long result = fixture.registerWithLeader(pktType);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.registerWithLeader(Learner.java:257)
		assertEquals(0L, result);
	}

	/**
	 * Run the long registerWithLeader(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testRegisterWithLeader_9()
		throws Exception {
		Learner fixture = new Learner();
		int pktType = 1;

		long result = fixture.registerWithLeader(pktType);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.registerWithLeader(Learner.java:257)
		assertEquals(0L, result);
	}

	/**
	 * Run the long registerWithLeader(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testRegisterWithLeader_10()
		throws Exception {
		Learner fixture = new Learner();
		int pktType = 1;

		long result = fixture.registerWithLeader(pktType);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.registerWithLeader(Learner.java:257)
		assertEquals(0L, result);
	}

	/**
	 * Run the long registerWithLeader(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testRegisterWithLeader_11()
		throws Exception {
		Learner fixture = new Learner();
		int pktType = 1;

		long result = fixture.registerWithLeader(pktType);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.registerWithLeader(Learner.java:257)
		assertEquals(0L, result);
	}

	/**
	 * Run the long registerWithLeader(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testRegisterWithLeader_12()
		throws Exception {
		Learner fixture = new Learner();
		int pktType = 1;

		long result = fixture.registerWithLeader(pktType);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.registerWithLeader(Learner.java:257)
		assertEquals(0L, result);
	}

	/**
	 * Run the long registerWithLeader(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testRegisterWithLeader_13()
		throws Exception {
		Learner fixture = new Learner();
		int pktType = 1;

		long result = fixture.registerWithLeader(pktType);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.registerWithLeader(Learner.java:257)
		assertEquals(0L, result);
	}

	/**
	 * Run the long registerWithLeader(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testRegisterWithLeader_14()
		throws Exception {
		Learner fixture = new Learner();
		int pktType = 1;

		long result = fixture.registerWithLeader(pktType);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.registerWithLeader(Learner.java:257)
		assertEquals(0L, result);
	}

	/**
	 * Run the long registerWithLeader(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testRegisterWithLeader_15()
		throws Exception {
		Learner fixture = new Learner();
		int pktType = 1;

		long result = fixture.registerWithLeader(pktType);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.registerWithLeader(Learner.java:257)
		assertEquals(0L, result);
	}

	/**
	 * Run the long registerWithLeader(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testRegisterWithLeader_16()
		throws Exception {
		Learner fixture = new Learner();
		int pktType = 1;

		long result = fixture.registerWithLeader(pktType);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.registerWithLeader(Learner.java:257)
		assertEquals(0L, result);
	}

	/**
	 * Run the void request(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testRequest_1()
		throws Exception {
		Learner fixture = new Learner();
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());

		fixture.request(request);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void request(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testRequest_2()
		throws Exception {
		Learner fixture = new Learner();
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());

		fixture.request(request);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void request(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testRequest_3()
		throws Exception {
		Learner fixture = new Learner();
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());

		fixture.request(request);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void request(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testRequest_4()
		throws Exception {
		Learner fixture = new Learner();
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());

		fixture.request(request);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void request(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testRequest_5()
		throws Exception {
		Learner fixture = new Learner();
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());

		fixture.request(request);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void request(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testRequest_6()
		throws Exception {
		Learner fixture = new Learner();
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());

		fixture.request(request);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void revalidate(QuorumPacket) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test(expected = java.io.EOFException.class)
	public void testRevalidate_1()
		throws Exception {
		Learner fixture = new Learner();
		QuorumPacket qp = new QuorumPacket(1, 1L, new byte[] {}, new LinkedList());

		fixture.revalidate(qp);

		// add additional test code here
	}

	/**
	 * Run the void revalidate(QuorumPacket) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test(expected = java.io.EOFException.class)
	public void testRevalidate_2()
		throws Exception {
		Learner fixture = new Learner();
		QuorumPacket qp = new QuorumPacket(1, 1L, new byte[] {}, new LinkedList());

		fixture.revalidate(qp);

		// add additional test code here
	}

	/**
	 * Run the void revalidate(QuorumPacket) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test(expected = java.io.EOFException.class)
	public void testRevalidate_3()
		throws Exception {
		Learner fixture = new Learner();
		QuorumPacket qp = new QuorumPacket(1, 1L, new byte[] {}, new LinkedList());

		fixture.revalidate(qp);

		// add additional test code here
	}

	/**
	 * Run the void revalidate(QuorumPacket) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test(expected = java.io.EOFException.class)
	public void testRevalidate_4()
		throws Exception {
		Learner fixture = new Learner();
		QuorumPacket qp = new QuorumPacket(1, 1L, new byte[] {}, new LinkedList());

		fixture.revalidate(qp);

		// add additional test code here
	}

	/**
	 * Run the void revalidate(QuorumPacket) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test(expected = java.io.EOFException.class)
	public void testRevalidate_5()
		throws Exception {
		Learner fixture = new Learner();
		QuorumPacket qp = new QuorumPacket(1, 1L, new byte[] {}, new LinkedList());

		fixture.revalidate(qp);

		// add additional test code here
	}

	/**
	 * Run the void revalidate(QuorumPacket) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test(expected = java.io.EOFException.class)
	public void testRevalidate_6()
		throws Exception {
		Learner fixture = new Learner();
		QuorumPacket qp = new QuorumPacket(1, 1L, new byte[] {}, new LinkedList());

		fixture.revalidate(qp);

		// add additional test code here
	}

	/**
	 * Run the void shutdown() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testShutdown_1()
		throws Exception {
		Learner fixture = new Learner();

		fixture.shutdown();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.shutdown(Learner.java:474)
	}

	/**
	 * Run the void shutdown() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testShutdown_2()
		throws Exception {
		Learner fixture = new Learner();

		fixture.shutdown();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.shutdown(Learner.java:474)
	}

	/**
	 * Run the void syncWithLeader(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testSyncWithLeader_1()
		throws Exception {
		Learner fixture = new Learner();
		long newLeaderZxid = 1L;

		fixture.syncWithLeader(newLeaderZxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.readPacket(Learner.java:151)
		//       at org.apache.zookeeper.server.quorum.Learner.syncWithLeader(Learner.java:317)
	}

	/**
	 * Run the void syncWithLeader(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testSyncWithLeader_2()
		throws Exception {
		Learner fixture = new Learner();
		long newLeaderZxid = 1L;

		fixture.syncWithLeader(newLeaderZxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.readPacket(Learner.java:151)
		//       at org.apache.zookeeper.server.quorum.Learner.syncWithLeader(Learner.java:317)
	}

	/**
	 * Run the void syncWithLeader(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testSyncWithLeader_3()
		throws Exception {
		Learner fixture = new Learner();
		long newLeaderZxid = 1L;

		fixture.syncWithLeader(newLeaderZxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.readPacket(Learner.java:151)
		//       at org.apache.zookeeper.server.quorum.Learner.syncWithLeader(Learner.java:317)
	}

	/**
	 * Run the void syncWithLeader(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testSyncWithLeader_4()
		throws Exception {
		Learner fixture = new Learner();
		long newLeaderZxid = 1L;

		fixture.syncWithLeader(newLeaderZxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.readPacket(Learner.java:151)
		//       at org.apache.zookeeper.server.quorum.Learner.syncWithLeader(Learner.java:317)
	}

	/**
	 * Run the void syncWithLeader(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testSyncWithLeader_5()
		throws Exception {
		Learner fixture = new Learner();
		long newLeaderZxid = 1L;

		fixture.syncWithLeader(newLeaderZxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.readPacket(Learner.java:151)
		//       at org.apache.zookeeper.server.quorum.Learner.syncWithLeader(Learner.java:317)
	}

	/**
	 * Run the void syncWithLeader(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testSyncWithLeader_6()
		throws Exception {
		Learner fixture = new Learner();
		long newLeaderZxid = 1L;

		fixture.syncWithLeader(newLeaderZxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.readPacket(Learner.java:151)
		//       at org.apache.zookeeper.server.quorum.Learner.syncWithLeader(Learner.java:317)
	}

	/**
	 * Run the void syncWithLeader(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testSyncWithLeader_7()
		throws Exception {
		Learner fixture = new Learner();
		long newLeaderZxid = 1L;

		fixture.syncWithLeader(newLeaderZxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.readPacket(Learner.java:151)
		//       at org.apache.zookeeper.server.quorum.Learner.syncWithLeader(Learner.java:317)
	}

	/**
	 * Run the void syncWithLeader(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testSyncWithLeader_8()
		throws Exception {
		Learner fixture = new Learner();
		long newLeaderZxid = 1L;

		fixture.syncWithLeader(newLeaderZxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.readPacket(Learner.java:151)
		//       at org.apache.zookeeper.server.quorum.Learner.syncWithLeader(Learner.java:317)
	}

	/**
	 * Run the void syncWithLeader(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testSyncWithLeader_9()
		throws Exception {
		Learner fixture = new Learner();
		long newLeaderZxid = 1L;

		fixture.syncWithLeader(newLeaderZxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.readPacket(Learner.java:151)
		//       at org.apache.zookeeper.server.quorum.Learner.syncWithLeader(Learner.java:317)
	}

	/**
	 * Run the void syncWithLeader(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testSyncWithLeader_10()
		throws Exception {
		Learner fixture = new Learner();
		long newLeaderZxid = 1L;

		fixture.syncWithLeader(newLeaderZxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.readPacket(Learner.java:151)
		//       at org.apache.zookeeper.server.quorum.Learner.syncWithLeader(Learner.java:317)
	}

	/**
	 * Run the void syncWithLeader(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testSyncWithLeader_11()
		throws Exception {
		Learner fixture = new Learner();
		long newLeaderZxid = 1L;

		fixture.syncWithLeader(newLeaderZxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.readPacket(Learner.java:151)
		//       at org.apache.zookeeper.server.quorum.Learner.syncWithLeader(Learner.java:317)
	}

	/**
	 * Run the void syncWithLeader(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testSyncWithLeader_12()
		throws Exception {
		Learner fixture = new Learner();
		long newLeaderZxid = 1L;

		fixture.syncWithLeader(newLeaderZxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.readPacket(Learner.java:151)
		//       at org.apache.zookeeper.server.quorum.Learner.syncWithLeader(Learner.java:317)
	}

	/**
	 * Run the void syncWithLeader(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testSyncWithLeader_13()
		throws Exception {
		Learner fixture = new Learner();
		long newLeaderZxid = 1L;

		fixture.syncWithLeader(newLeaderZxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.readPacket(Learner.java:151)
		//       at org.apache.zookeeper.server.quorum.Learner.syncWithLeader(Learner.java:317)
	}

	/**
	 * Run the void syncWithLeader(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testSyncWithLeader_14()
		throws Exception {
		Learner fixture = new Learner();
		long newLeaderZxid = 1L;

		fixture.syncWithLeader(newLeaderZxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.readPacket(Learner.java:151)
		//       at org.apache.zookeeper.server.quorum.Learner.syncWithLeader(Learner.java:317)
	}

	/**
	 * Run the void syncWithLeader(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testSyncWithLeader_15()
		throws Exception {
		Learner fixture = new Learner();
		long newLeaderZxid = 1L;

		fixture.syncWithLeader(newLeaderZxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.readPacket(Learner.java:151)
		//       at org.apache.zookeeper.server.quorum.Learner.syncWithLeader(Learner.java:317)
	}

	/**
	 * Run the void syncWithLeader(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testSyncWithLeader_16()
		throws Exception {
		Learner fixture = new Learner();
		long newLeaderZxid = 1L;

		fixture.syncWithLeader(newLeaderZxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.readPacket(Learner.java:151)
		//       at org.apache.zookeeper.server.quorum.Learner.syncWithLeader(Learner.java:317)
	}

	/**
	 * Run the void validateSession(ServerCnxn,long,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testValidateSession_1()
		throws Exception {
		Learner fixture = new Learner();
		ServerCnxn cnxn = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		long clientId = 1L;
		int timeout = 1;

		fixture.validateSession(cnxn, clientId, timeout);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void validateSession(ServerCnxn,long,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testValidateSession_2()
		throws Exception {
		Learner fixture = new Learner();
		ServerCnxn cnxn = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		long clientId = 1L;
		int timeout = 1;

		fixture.validateSession(cnxn, clientId, timeout);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void validateSession(ServerCnxn,long,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testValidateSession_3()
		throws Exception {
		Learner fixture = new Learner();
		ServerCnxn cnxn = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		long clientId = 1L;
		int timeout = 1;

		fixture.validateSession(cnxn, clientId, timeout);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void validateSession(ServerCnxn,long,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testValidateSession_4()
		throws Exception {
		Learner fixture = new Learner();
		ServerCnxn cnxn = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		long clientId = 1L;
		int timeout = 1;

		fixture.validateSession(cnxn, clientId, timeout);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void validateSession(ServerCnxn,long,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testValidateSession_5()
		throws Exception {
		Learner fixture = new Learner();
		ServerCnxn cnxn = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		long clientId = 1L;
		int timeout = 1;

		fixture.validateSession(cnxn, clientId, timeout);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void validateSession(ServerCnxn,long,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testValidateSession_6()
		throws Exception {
		Learner fixture = new Learner();
		ServerCnxn cnxn = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		long clientId = 1L;
		int timeout = 1;

		fixture.validateSession(cnxn, clientId, timeout);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void writePacket(QuorumPacket,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testWritePacket_1()
		throws Exception {
		Learner fixture = new Learner();
		QuorumPacket pp = new QuorumPacket();
		boolean flush = true;

		fixture.writePacket(pp, flush);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.writePacket(Learner.java:133)
	}

	/**
	 * Run the void writePacket(QuorumPacket,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testWritePacket_2()
		throws Exception {
		Learner fixture = new Learner();
		QuorumPacket pp = null;
		boolean flush = true;

		fixture.writePacket(pp, flush);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.writePacket(Learner.java:133)
	}

	/**
	 * Run the void writePacket(QuorumPacket,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testWritePacket_3()
		throws Exception {
		Learner fixture = new Learner();
		QuorumPacket pp = new QuorumPacket();
		boolean flush = true;

		fixture.writePacket(pp, flush);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.writePacket(Learner.java:133)
	}

	/**
	 * Run the void writePacket(QuorumPacket,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testWritePacket_4()
		throws Exception {
		Learner fixture = new Learner();
		QuorumPacket pp = null;
		boolean flush = true;

		fixture.writePacket(pp, flush);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.writePacket(Learner.java:133)
	}

	/**
	 * Run the void writePacket(QuorumPacket,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testWritePacket_5()
		throws Exception {
		Learner fixture = new Learner();
		QuorumPacket pp = new QuorumPacket();
		boolean flush = false;

		fixture.writePacket(pp, flush);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.writePacket(Learner.java:133)
	}

	/**
	 * Run the void writePacket(QuorumPacket,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testWritePacket_6()
		throws Exception {
		Learner fixture = new Learner();
		QuorumPacket pp = null;
		boolean flush = false;

		fixture.writePacket(pp, flush);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Learner.writePacket(Learner.java:133)
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(LearnerTest.class);
	}
}