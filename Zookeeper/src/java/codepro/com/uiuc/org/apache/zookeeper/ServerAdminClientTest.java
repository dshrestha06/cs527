package com.uiuc.org.apache.zookeeper;

import org.apache.zookeeper.ServerAdminClient;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>ServerAdminClientTest</code> contains tests for the class <code>{@link ServerAdminClient}</code>.
 *
 * @generatedBy CodePro at 10/16/13 6:58 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class ServerAdminClientTest {
	/**
	 * Run the ServerAdminClient() constructor test.
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testServerAdminClient_1()
		throws Exception {
		ServerAdminClient result = new ServerAdminClient();
		assertNotNull(result);
		// add additional test code here
	}

	/**
	 * Run the void dump(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testDump_1()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.dump(host, port);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:70)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:219)
		//       at java.net.SocketOutputStream.<init>(SocketOutputStream.java:41)
		//       at java.net.PlainSocketImpl.getOutputStream(PlainSocketImpl.java:444)
		//       at java.net.Socket$3.run(Socket.java:839)
		//       at java.security.AccessController.doPrivileged(Native Method)
		//       at java.net.Socket.getOutputStream(Socket.java:836)
		//       at org.apache.zookeeper.ServerAdminClient.dump(ServerAdminClient.java:115)
	}

	/**
	 * Run the void dump(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testDump_2()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.dump(host, port);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:70)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:219)
		//       at java.net.SocketOutputStream.<init>(SocketOutputStream.java:41)
		//       at java.net.PlainSocketImpl.getOutputStream(PlainSocketImpl.java:444)
		//       at java.net.Socket$3.run(Socket.java:839)
		//       at java.security.AccessController.doPrivileged(Native Method)
		//       at java.net.Socket.getOutputStream(Socket.java:836)
		//       at org.apache.zookeeper.ServerAdminClient.dump(ServerAdminClient.java:115)
	}

	/**
	 * Run the void dump(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testDump_3()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.dump(host, port);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:70)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:219)
		//       at java.net.SocketOutputStream.<init>(SocketOutputStream.java:41)
		//       at java.net.PlainSocketImpl.getOutputStream(PlainSocketImpl.java:444)
		//       at java.net.Socket$3.run(Socket.java:839)
		//       at java.security.AccessController.doPrivileged(Native Method)
		//       at java.net.Socket.getOutputStream(Socket.java:836)
		//       at org.apache.zookeeper.ServerAdminClient.dump(ServerAdminClient.java:115)
	}

	/**
	 * Run the void dump(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testDump_4()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.dump(host, port);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:70)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:219)
		//       at java.net.SocketOutputStream.<init>(SocketOutputStream.java:41)
		//       at java.net.PlainSocketImpl.getOutputStream(PlainSocketImpl.java:444)
		//       at java.net.Socket$3.run(Socket.java:839)
		//       at java.security.AccessController.doPrivileged(Native Method)
		//       at java.net.Socket.getOutputStream(Socket.java:836)
		//       at org.apache.zookeeper.ServerAdminClient.dump(ServerAdminClient.java:115)
	}

	/**
	 * Run the void dump(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testDump_5()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.dump(host, port);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:70)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:219)
		//       at java.net.SocketOutputStream.<init>(SocketOutputStream.java:41)
		//       at java.net.PlainSocketImpl.getOutputStream(PlainSocketImpl.java:444)
		//       at java.net.Socket$3.run(Socket.java:839)
		//       at java.security.AccessController.doPrivileged(Native Method)
		//       at java.net.Socket.getOutputStream(Socket.java:836)
		//       at org.apache.zookeeper.ServerAdminClient.dump(ServerAdminClient.java:115)
	}

	/**
	 * Run the void dump(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testDump_6()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.dump(host, port);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:70)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:219)
		//       at java.net.SocketOutputStream.<init>(SocketOutputStream.java:41)
		//       at java.net.PlainSocketImpl.getOutputStream(PlainSocketImpl.java:444)
		//       at java.net.Socket$3.run(Socket.java:839)
		//       at java.security.AccessController.doPrivileged(Native Method)
		//       at java.net.Socket.getOutputStream(Socket.java:836)
		//       at org.apache.zookeeper.ServerAdminClient.dump(ServerAdminClient.java:115)
	}

	/**
	 * Run the void dump(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testDump_7()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.dump(host, port);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:70)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:219)
		//       at java.net.SocketOutputStream.<init>(SocketOutputStream.java:41)
		//       at java.net.PlainSocketImpl.getOutputStream(PlainSocketImpl.java:444)
		//       at java.net.Socket$3.run(Socket.java:839)
		//       at java.security.AccessController.doPrivileged(Native Method)
		//       at java.net.Socket.getOutputStream(Socket.java:836)
		//       at org.apache.zookeeper.ServerAdminClient.dump(ServerAdminClient.java:115)
	}

	/**
	 * Run the void dump(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testDump_8()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.dump(host, port);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:70)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:219)
		//       at java.net.SocketOutputStream.<init>(SocketOutputStream.java:41)
		//       at java.net.PlainSocketImpl.getOutputStream(PlainSocketImpl.java:444)
		//       at java.net.Socket$3.run(Socket.java:839)
		//       at java.security.AccessController.doPrivileged(Native Method)
		//       at java.net.Socket.getOutputStream(Socket.java:836)
		//       at org.apache.zookeeper.ServerAdminClient.dump(ServerAdminClient.java:115)
	}

	/**
	 * Run the void dump(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testDump_9()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.dump(host, port);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:70)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:219)
		//       at java.net.SocketOutputStream.<init>(SocketOutputStream.java:41)
		//       at java.net.PlainSocketImpl.getOutputStream(PlainSocketImpl.java:444)
		//       at java.net.Socket$3.run(Socket.java:839)
		//       at java.security.AccessController.doPrivileged(Native Method)
		//       at java.net.Socket.getOutputStream(Socket.java:836)
		//       at org.apache.zookeeper.ServerAdminClient.dump(ServerAdminClient.java:115)
	}

	/**
	 * Run the void dump(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testDump_10()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.dump(host, port);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:70)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:219)
		//       at java.net.SocketOutputStream.<init>(SocketOutputStream.java:41)
		//       at java.net.PlainSocketImpl.getOutputStream(PlainSocketImpl.java:444)
		//       at java.net.Socket$3.run(Socket.java:839)
		//       at java.security.AccessController.doPrivileged(Native Method)
		//       at java.net.Socket.getOutputStream(Socket.java:836)
		//       at org.apache.zookeeper.ServerAdminClient.dump(ServerAdminClient.java:115)
	}

	/**
	 * Run the void dump(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testDump_11()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.dump(host, port);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:70)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:219)
		//       at java.net.SocketOutputStream.<init>(SocketOutputStream.java:41)
		//       at java.net.PlainSocketImpl.getOutputStream(PlainSocketImpl.java:444)
		//       at java.net.Socket$3.run(Socket.java:839)
		//       at java.security.AccessController.doPrivileged(Native Method)
		//       at java.net.Socket.getOutputStream(Socket.java:836)
		//       at org.apache.zookeeper.ServerAdminClient.dump(ServerAdminClient.java:115)
	}

	/**
	 * Run the void dump(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testDump_12()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.dump(host, port);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:70)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:219)
		//       at java.net.SocketOutputStream.<init>(SocketOutputStream.java:41)
		//       at java.net.PlainSocketImpl.getOutputStream(PlainSocketImpl.java:444)
		//       at java.net.Socket$3.run(Socket.java:839)
		//       at java.security.AccessController.doPrivileged(Native Method)
		//       at java.net.Socket.getOutputStream(Socket.java:836)
		//       at org.apache.zookeeper.ServerAdminClient.dump(ServerAdminClient.java:115)
	}

	/**
	 * Run the void dump(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testDump_13()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.dump(host, port);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:70)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:219)
		//       at java.net.SocketOutputStream.<init>(SocketOutputStream.java:41)
		//       at java.net.PlainSocketImpl.getOutputStream(PlainSocketImpl.java:444)
		//       at java.net.Socket$3.run(Socket.java:839)
		//       at java.security.AccessController.doPrivileged(Native Method)
		//       at java.net.Socket.getOutputStream(Socket.java:836)
		//       at org.apache.zookeeper.ServerAdminClient.dump(ServerAdminClient.java:115)
	}

	/**
	 * Run the void dump(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testDump_14()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.dump(host, port);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:70)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:219)
		//       at java.net.SocketOutputStream.<init>(SocketOutputStream.java:41)
		//       at java.net.PlainSocketImpl.getOutputStream(PlainSocketImpl.java:444)
		//       at java.net.Socket$3.run(Socket.java:839)
		//       at java.security.AccessController.doPrivileged(Native Method)
		//       at java.net.Socket.getOutputStream(Socket.java:836)
		//       at org.apache.zookeeper.ServerAdminClient.dump(ServerAdminClient.java:115)
	}

	/**
	 * Run the void dump(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testDump_15()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.dump(host, port);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:70)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:219)
		//       at java.net.SocketOutputStream.<init>(SocketOutputStream.java:41)
		//       at java.net.PlainSocketImpl.getOutputStream(PlainSocketImpl.java:444)
		//       at java.net.Socket$3.run(Socket.java:839)
		//       at java.security.AccessController.doPrivileged(Native Method)
		//       at java.net.Socket.getOutputStream(Socket.java:836)
		//       at org.apache.zookeeper.ServerAdminClient.dump(ServerAdminClient.java:115)
	}

	/**
	 * Run the void dump(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testDump_16()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.dump(host, port);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:70)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:219)
		//       at java.net.SocketOutputStream.<init>(SocketOutputStream.java:41)
		//       at java.net.PlainSocketImpl.getOutputStream(PlainSocketImpl.java:444)
		//       at java.net.Socket$3.run(Socket.java:839)
		//       at java.security.AccessController.doPrivileged(Native Method)
		//       at java.net.Socket.getOutputStream(Socket.java:836)
		//       at org.apache.zookeeper.ServerAdminClient.dump(ServerAdminClient.java:115)
	}

	/**
	 * Run the void getTraceMask(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testGetTraceMask_1()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.getTraceMask(host, port);

		// add additional test code here
	}

	/**
	 * Run the void getTraceMask(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testGetTraceMask_2()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.getTraceMask(host, port);

		// add additional test code here
	}

	/**
	 * Run the void getTraceMask(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testGetTraceMask_3()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.getTraceMask(host, port);

		// add additional test code here
	}

	/**
	 * Run the void getTraceMask(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testGetTraceMask_4()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.getTraceMask(host, port);

		// add additional test code here
	}

	/**
	 * Run the void getTraceMask(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testGetTraceMask_5()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.getTraceMask(host, port);

		// add additional test code here
	}

	/**
	 * Run the void getTraceMask(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testGetTraceMask_6()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.getTraceMask(host, port);

		// add additional test code here
	}

	/**
	 * Run the void getTraceMask(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testGetTraceMask_7()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.getTraceMask(host, port);

		// add additional test code here
	}

	/**
	 * Run the void getTraceMask(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testGetTraceMask_8()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.getTraceMask(host, port);

		// add additional test code here
	}

	/**
	 * Run the void getTraceMask(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testGetTraceMask_9()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.getTraceMask(host, port);

		// add additional test code here
	}

	/**
	 * Run the void getTraceMask(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testGetTraceMask_10()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.getTraceMask(host, port);

		// add additional test code here
	}

	/**
	 * Run the void getTraceMask(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testGetTraceMask_11()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.getTraceMask(host, port);

		// add additional test code here
	}

	/**
	 * Run the void getTraceMask(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testGetTraceMask_12()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.getTraceMask(host, port);

		// add additional test code here
	}

	/**
	 * Run the void getTraceMask(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testGetTraceMask_13()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.getTraceMask(host, port);

		// add additional test code here
	}

	/**
	 * Run the void getTraceMask(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testGetTraceMask_14()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.getTraceMask(host, port);

		// add additional test code here
	}

	/**
	 * Run the void getTraceMask(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testGetTraceMask_15()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.getTraceMask(host, port);

		// add additional test code here
	}

	/**
	 * Run the void getTraceMask(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testGetTraceMask_16()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.getTraceMask(host, port);

		// add additional test code here
	}

	/**
	 * Run the void kill(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testKill_1()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.kill(host, port);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:70)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:219)
		//       at java.net.SocketOutputStream.<init>(SocketOutputStream.java:41)
		//       at java.net.PlainSocketImpl.getOutputStream(PlainSocketImpl.java:444)
		//       at java.net.Socket$3.run(Socket.java:839)
		//       at java.security.AccessController.doPrivileged(Native Method)
		//       at java.net.Socket.getOutputStream(Socket.java:836)
		//       at org.apache.zookeeper.ServerAdminClient.kill(ServerAdminClient.java:183)
	}

	/**
	 * Run the void kill(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testKill_2()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.kill(host, port);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:70)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:219)
		//       at java.net.SocketOutputStream.<init>(SocketOutputStream.java:41)
		//       at java.net.PlainSocketImpl.getOutputStream(PlainSocketImpl.java:444)
		//       at java.net.Socket$3.run(Socket.java:839)
		//       at java.security.AccessController.doPrivileged(Native Method)
		//       at java.net.Socket.getOutputStream(Socket.java:836)
		//       at org.apache.zookeeper.ServerAdminClient.kill(ServerAdminClient.java:183)
	}

	/**
	 * Run the void kill(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testKill_3()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.kill(host, port);

		// add additional test code here
	}

	/**
	 * Run the void kill(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testKill_4()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.kill(host, port);

		// add additional test code here
	}

	/**
	 * Run the void kill(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testKill_5()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.kill(host, port);

		// add additional test code here
	}

	/**
	 * Run the void kill(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testKill_6()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.kill(host, port);

		// add additional test code here
	}

	/**
	 * Run the void kill(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testKill_7()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.kill(host, port);

		// add additional test code here
	}

	/**
	 * Run the void kill(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testKill_8()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.kill(host, port);

		// add additional test code here
	}

	/**
	 * Run the void kill(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testKill_9()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.kill(host, port);

		// add additional test code here
	}

	/**
	 * Run the void kill(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testKill_10()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.kill(host, port);

		// add additional test code here
	}

	/**
	 * Run the void kill(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testKill_11()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.kill(host, port);

		// add additional test code here
	}

	/**
	 * Run the void kill(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testKill_12()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.kill(host, port);

		// add additional test code here
	}

	/**
	 * Run the void kill(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testKill_13()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.kill(host, port);

		// add additional test code here
	}

	/**
	 * Run the void kill(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testKill_14()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.kill(host, port);

		// add additional test code here
	}

	/**
	 * Run the void kill(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testKill_15()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.kill(host, port);

		// add additional test code here
	}

	/**
	 * Run the void kill(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testKill_16()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.kill(host, port);

		// add additional test code here
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testMain_1()
		throws Exception {
		String[] args = new String[] {};

		ServerAdminClient.main(args);

		// add additional test code here
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testMain_2()
		throws Exception {
		String[] args = new String[] {"a", "0", ""};

		ServerAdminClient.main(args);

		// add additional test code here
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testMain_3()
		throws Exception {
		String[] args = new String[] {"a", "0", ""};

		ServerAdminClient.main(args);

		// add additional test code here
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testMain_4()
		throws Exception {
		String[] args = new String[] {"a", "0", ""};

		ServerAdminClient.main(args);

		// add additional test code here
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testMain_5()
		throws Exception {
		String[] args = new String[] {"a", "0", ""};

		ServerAdminClient.main(args);

		// add additional test code here
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testMain_6()
		throws Exception {
		String[] args = new String[] {"a", "0", ""};

		ServerAdminClient.main(args);

		// add additional test code here
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testMain_7()
		throws Exception {
		String[] args = new String[] {"a", "0", "", "a"};

		ServerAdminClient.main(args);

		// add additional test code here
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testMain_8()
		throws Exception {
		String[] args = new String[] {"a", "0", ""};

		ServerAdminClient.main(args);

		// add additional test code here
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test(expected = java.lang.NumberFormatException.class)
	public void testMain_9()
		throws Exception {
		String[] args = new String[] {"", "", null};

		ServerAdminClient.main(args);

		// add additional test code here
	}

	/**
	 * Run the void ruok(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testRuok_1()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.ruok(host, port);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:70)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:219)
		//       at java.net.SocketOutputStream.<init>(SocketOutputStream.java:41)
		//       at java.net.PlainSocketImpl.getOutputStream(PlainSocketImpl.java:444)
		//       at java.net.Socket$3.run(Socket.java:839)
		//       at java.security.AccessController.doPrivileged(Native Method)
		//       at java.net.Socket.getOutputStream(Socket.java:836)
		//       at org.apache.zookeeper.ServerAdminClient.ruok(ServerAdminClient.java:81)
	}

	/**
	 * Run the void ruok(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testRuok_2()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.ruok(host, port);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:70)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:219)
		//       at java.net.SocketOutputStream.<init>(SocketOutputStream.java:41)
		//       at java.net.PlainSocketImpl.getOutputStream(PlainSocketImpl.java:444)
		//       at java.net.Socket$3.run(Socket.java:839)
		//       at java.security.AccessController.doPrivileged(Native Method)
		//       at java.net.Socket.getOutputStream(Socket.java:836)
		//       at org.apache.zookeeper.ServerAdminClient.ruok(ServerAdminClient.java:81)
	}

	/**
	 * Run the void ruok(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testRuok_3()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.ruok(host, port);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:70)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:219)
		//       at java.net.SocketOutputStream.<init>(SocketOutputStream.java:41)
		//       at java.net.PlainSocketImpl.getOutputStream(PlainSocketImpl.java:444)
		//       at java.net.Socket$3.run(Socket.java:839)
		//       at java.security.AccessController.doPrivileged(Native Method)
		//       at java.net.Socket.getOutputStream(Socket.java:836)
		//       at org.apache.zookeeper.ServerAdminClient.ruok(ServerAdminClient.java:81)
	}

	/**
	 * Run the void ruok(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testRuok_4()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.ruok(host, port);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:70)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:219)
		//       at java.net.SocketOutputStream.<init>(SocketOutputStream.java:41)
		//       at java.net.PlainSocketImpl.getOutputStream(PlainSocketImpl.java:444)
		//       at java.net.Socket$3.run(Socket.java:839)
		//       at java.security.AccessController.doPrivileged(Native Method)
		//       at java.net.Socket.getOutputStream(Socket.java:836)
		//       at org.apache.zookeeper.ServerAdminClient.ruok(ServerAdminClient.java:81)
	}

	/**
	 * Run the void ruok(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testRuok_5()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.ruok(host, port);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:70)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:219)
		//       at java.net.SocketOutputStream.<init>(SocketOutputStream.java:41)
		//       at java.net.PlainSocketImpl.getOutputStream(PlainSocketImpl.java:444)
		//       at java.net.Socket$3.run(Socket.java:839)
		//       at java.security.AccessController.doPrivileged(Native Method)
		//       at java.net.Socket.getOutputStream(Socket.java:836)
		//       at org.apache.zookeeper.ServerAdminClient.ruok(ServerAdminClient.java:81)
	}

	/**
	 * Run the void ruok(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testRuok_6()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.ruok(host, port);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:70)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:219)
		//       at java.net.SocketOutputStream.<init>(SocketOutputStream.java:41)
		//       at java.net.PlainSocketImpl.getOutputStream(PlainSocketImpl.java:444)
		//       at java.net.Socket$3.run(Socket.java:839)
		//       at java.security.AccessController.doPrivileged(Native Method)
		//       at java.net.Socket.getOutputStream(Socket.java:836)
		//       at org.apache.zookeeper.ServerAdminClient.ruok(ServerAdminClient.java:81)
	}

	/**
	 * Run the void ruok(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testRuok_7()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.ruok(host, port);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:70)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:219)
		//       at java.net.SocketOutputStream.<init>(SocketOutputStream.java:41)
		//       at java.net.PlainSocketImpl.getOutputStream(PlainSocketImpl.java:444)
		//       at java.net.Socket$3.run(Socket.java:839)
		//       at java.security.AccessController.doPrivileged(Native Method)
		//       at java.net.Socket.getOutputStream(Socket.java:836)
		//       at org.apache.zookeeper.ServerAdminClient.ruok(ServerAdminClient.java:81)
	}

	/**
	 * Run the void ruok(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testRuok_8()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.ruok(host, port);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:70)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:219)
		//       at java.net.SocketOutputStream.<init>(SocketOutputStream.java:41)
		//       at java.net.PlainSocketImpl.getOutputStream(PlainSocketImpl.java:444)
		//       at java.net.Socket$3.run(Socket.java:839)
		//       at java.security.AccessController.doPrivileged(Native Method)
		//       at java.net.Socket.getOutputStream(Socket.java:836)
		//       at org.apache.zookeeper.ServerAdminClient.ruok(ServerAdminClient.java:81)
	}

	/**
	 * Run the void ruok(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testRuok_9()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.ruok(host, port);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:70)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:219)
		//       at java.net.SocketOutputStream.<init>(SocketOutputStream.java:41)
		//       at java.net.PlainSocketImpl.getOutputStream(PlainSocketImpl.java:444)
		//       at java.net.Socket$3.run(Socket.java:839)
		//       at java.security.AccessController.doPrivileged(Native Method)
		//       at java.net.Socket.getOutputStream(Socket.java:836)
		//       at org.apache.zookeeper.ServerAdminClient.ruok(ServerAdminClient.java:81)
	}

	/**
	 * Run the void ruok(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testRuok_10()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.ruok(host, port);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:70)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:219)
		//       at java.net.SocketOutputStream.<init>(SocketOutputStream.java:41)
		//       at java.net.PlainSocketImpl.getOutputStream(PlainSocketImpl.java:444)
		//       at java.net.Socket$3.run(Socket.java:839)
		//       at java.security.AccessController.doPrivileged(Native Method)
		//       at java.net.Socket.getOutputStream(Socket.java:836)
		//       at org.apache.zookeeper.ServerAdminClient.ruok(ServerAdminClient.java:81)
	}

	/**
	 * Run the void ruok(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testRuok_11()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.ruok(host, port);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:70)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:219)
		//       at java.net.SocketOutputStream.<init>(SocketOutputStream.java:41)
		//       at java.net.PlainSocketImpl.getOutputStream(PlainSocketImpl.java:444)
		//       at java.net.Socket$3.run(Socket.java:839)
		//       at java.security.AccessController.doPrivileged(Native Method)
		//       at java.net.Socket.getOutputStream(Socket.java:836)
		//       at org.apache.zookeeper.ServerAdminClient.ruok(ServerAdminClient.java:81)
	}

	/**
	 * Run the void ruok(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testRuok_12()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.ruok(host, port);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:70)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:219)
		//       at java.net.SocketOutputStream.<init>(SocketOutputStream.java:41)
		//       at java.net.PlainSocketImpl.getOutputStream(PlainSocketImpl.java:444)
		//       at java.net.Socket$3.run(Socket.java:839)
		//       at java.security.AccessController.doPrivileged(Native Method)
		//       at java.net.Socket.getOutputStream(Socket.java:836)
		//       at org.apache.zookeeper.ServerAdminClient.ruok(ServerAdminClient.java:81)
	}

	/**
	 * Run the void ruok(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testRuok_13()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.ruok(host, port);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:70)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:219)
		//       at java.net.SocketOutputStream.<init>(SocketOutputStream.java:41)
		//       at java.net.PlainSocketImpl.getOutputStream(PlainSocketImpl.java:444)
		//       at java.net.Socket$3.run(Socket.java:839)
		//       at java.security.AccessController.doPrivileged(Native Method)
		//       at java.net.Socket.getOutputStream(Socket.java:836)
		//       at org.apache.zookeeper.ServerAdminClient.ruok(ServerAdminClient.java:81)
	}

	/**
	 * Run the void ruok(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testRuok_14()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.ruok(host, port);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:70)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:219)
		//       at java.net.SocketOutputStream.<init>(SocketOutputStream.java:41)
		//       at java.net.PlainSocketImpl.getOutputStream(PlainSocketImpl.java:444)
		//       at java.net.Socket$3.run(Socket.java:839)
		//       at java.security.AccessController.doPrivileged(Native Method)
		//       at java.net.Socket.getOutputStream(Socket.java:836)
		//       at org.apache.zookeeper.ServerAdminClient.ruok(ServerAdminClient.java:81)
	}

	/**
	 * Run the void ruok(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testRuok_15()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.ruok(host, port);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:70)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:219)
		//       at java.net.SocketOutputStream.<init>(SocketOutputStream.java:41)
		//       at java.net.PlainSocketImpl.getOutputStream(PlainSocketImpl.java:444)
		//       at java.net.Socket$3.run(Socket.java:839)
		//       at java.security.AccessController.doPrivileged(Native Method)
		//       at java.net.Socket.getOutputStream(Socket.java:836)
		//       at org.apache.zookeeper.ServerAdminClient.ruok(ServerAdminClient.java:81)
	}

	/**
	 * Run the void ruok(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testRuok_16()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.ruok(host, port);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:70)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:219)
		//       at java.net.SocketOutputStream.<init>(SocketOutputStream.java:41)
		//       at java.net.PlainSocketImpl.getOutputStream(PlainSocketImpl.java:444)
		//       at java.net.Socket$3.run(Socket.java:839)
		//       at java.security.AccessController.doPrivileged(Native Method)
		//       at java.net.Socket.getOutputStream(Socket.java:836)
		//       at org.apache.zookeeper.ServerAdminClient.ruok(ServerAdminClient.java:81)
	}

	/**
	 * Run the void setTraceMask(String,int,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testSetTraceMask_1()
		throws Exception {
		String host = "";
		int port = 1;
		String traceMaskStr = "";

		ServerAdminClient.setTraceMask(host, port, traceMaskStr);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NumberFormatException: For input string: ""
		//       at java.lang.NumberFormatException.forInputString(NumberFormatException.java:48)
		//       at java.lang.Long.parseLong(Long.java:431)
		//       at org.apache.zookeeper.ServerAdminClient.setTraceMask(ServerAdminClient.java:209)
	}

	/**
	 * Run the void setTraceMask(String,int,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testSetTraceMask_2()
		throws Exception {
		String host = "";
		int port = 1;
		String traceMaskStr = "";

		ServerAdminClient.setTraceMask(host, port, traceMaskStr);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NumberFormatException: For input string: ""
		//       at java.lang.NumberFormatException.forInputString(NumberFormatException.java:48)
		//       at java.lang.Long.parseLong(Long.java:431)
		//       at org.apache.zookeeper.ServerAdminClient.setTraceMask(ServerAdminClient.java:209)
	}

	/**
	 * Run the void setTraceMask(String,int,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testSetTraceMask_3()
		throws Exception {
		String host = "";
		int port = 1;
		String traceMaskStr = "";

		ServerAdminClient.setTraceMask(host, port, traceMaskStr);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NumberFormatException: For input string: ""
		//       at java.lang.NumberFormatException.forInputString(NumberFormatException.java:48)
		//       at java.lang.Long.parseLong(Long.java:431)
		//       at org.apache.zookeeper.ServerAdminClient.setTraceMask(ServerAdminClient.java:209)
	}

	/**
	 * Run the void setTraceMask(String,int,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testSetTraceMask_4()
		throws Exception {
		String host = "";
		int port = 1;
		String traceMaskStr = "";

		ServerAdminClient.setTraceMask(host, port, traceMaskStr);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NumberFormatException: For input string: ""
		//       at java.lang.NumberFormatException.forInputString(NumberFormatException.java:48)
		//       at java.lang.Long.parseLong(Long.java:431)
		//       at org.apache.zookeeper.ServerAdminClient.setTraceMask(ServerAdminClient.java:209)
	}

	/**
	 * Run the void setTraceMask(String,int,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testSetTraceMask_5()
		throws Exception {
		String host = "";
		int port = 1;
		String traceMaskStr = "";

		ServerAdminClient.setTraceMask(host, port, traceMaskStr);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NumberFormatException: For input string: ""
		//       at java.lang.NumberFormatException.forInputString(NumberFormatException.java:48)
		//       at java.lang.Long.parseLong(Long.java:431)
		//       at org.apache.zookeeper.ServerAdminClient.setTraceMask(ServerAdminClient.java:209)
	}

	/**
	 * Run the void setTraceMask(String,int,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testSetTraceMask_6()
		throws Exception {
		String host = "";
		int port = 1;
		String traceMaskStr = "";

		ServerAdminClient.setTraceMask(host, port, traceMaskStr);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NumberFormatException: For input string: ""
		//       at java.lang.NumberFormatException.forInputString(NumberFormatException.java:48)
		//       at java.lang.Long.parseLong(Long.java:431)
		//       at org.apache.zookeeper.ServerAdminClient.setTraceMask(ServerAdminClient.java:209)
	}

	/**
	 * Run the void setTraceMask(String,int,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testSetTraceMask_7()
		throws Exception {
		String host = "";
		int port = 1;
		String traceMaskStr = "";

		ServerAdminClient.setTraceMask(host, port, traceMaskStr);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NumberFormatException: For input string: ""
		//       at java.lang.NumberFormatException.forInputString(NumberFormatException.java:48)
		//       at java.lang.Long.parseLong(Long.java:431)
		//       at org.apache.zookeeper.ServerAdminClient.setTraceMask(ServerAdminClient.java:209)
	}

	/**
	 * Run the void setTraceMask(String,int,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testSetTraceMask_8()
		throws Exception {
		String host = "";
		int port = 1;
		String traceMaskStr = "";

		ServerAdminClient.setTraceMask(host, port, traceMaskStr);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NumberFormatException: For input string: ""
		//       at java.lang.NumberFormatException.forInputString(NumberFormatException.java:48)
		//       at java.lang.Long.parseLong(Long.java:431)
		//       at org.apache.zookeeper.ServerAdminClient.setTraceMask(ServerAdminClient.java:209)
	}

	/**
	 * Run the void setTraceMask(String,int,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testSetTraceMask_9()
		throws Exception {
		String host = "";
		int port = 1;
		String traceMaskStr = "";

		ServerAdminClient.setTraceMask(host, port, traceMaskStr);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NumberFormatException: For input string: ""
		//       at java.lang.NumberFormatException.forInputString(NumberFormatException.java:48)
		//       at java.lang.Long.parseLong(Long.java:431)
		//       at org.apache.zookeeper.ServerAdminClient.setTraceMask(ServerAdminClient.java:209)
	}

	/**
	 * Run the void setTraceMask(String,int,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testSetTraceMask_10()
		throws Exception {
		String host = "";
		int port = 1;
		String traceMaskStr = "";

		ServerAdminClient.setTraceMask(host, port, traceMaskStr);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NumberFormatException: For input string: ""
		//       at java.lang.NumberFormatException.forInputString(NumberFormatException.java:48)
		//       at java.lang.Long.parseLong(Long.java:431)
		//       at org.apache.zookeeper.ServerAdminClient.setTraceMask(ServerAdminClient.java:209)
	}

	/**
	 * Run the void setTraceMask(String,int,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testSetTraceMask_11()
		throws Exception {
		String host = "";
		int port = 1;
		String traceMaskStr = "";

		ServerAdminClient.setTraceMask(host, port, traceMaskStr);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NumberFormatException: For input string: ""
		//       at java.lang.NumberFormatException.forInputString(NumberFormatException.java:48)
		//       at java.lang.Long.parseLong(Long.java:431)
		//       at org.apache.zookeeper.ServerAdminClient.setTraceMask(ServerAdminClient.java:209)
	}

	/**
	 * Run the void setTraceMask(String,int,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testSetTraceMask_12()
		throws Exception {
		String host = "";
		int port = 1;
		String traceMaskStr = "";

		ServerAdminClient.setTraceMask(host, port, traceMaskStr);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NumberFormatException: For input string: ""
		//       at java.lang.NumberFormatException.forInputString(NumberFormatException.java:48)
		//       at java.lang.Long.parseLong(Long.java:431)
		//       at org.apache.zookeeper.ServerAdminClient.setTraceMask(ServerAdminClient.java:209)
	}

	/**
	 * Run the void setTraceMask(String,int,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testSetTraceMask_13()
		throws Exception {
		String host = "";
		int port = 1;
		String traceMaskStr = "";

		ServerAdminClient.setTraceMask(host, port, traceMaskStr);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NumberFormatException: For input string: ""
		//       at java.lang.NumberFormatException.forInputString(NumberFormatException.java:48)
		//       at java.lang.Long.parseLong(Long.java:431)
		//       at org.apache.zookeeper.ServerAdminClient.setTraceMask(ServerAdminClient.java:209)
	}

	/**
	 * Run the void setTraceMask(String,int,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testSetTraceMask_14()
		throws Exception {
		String host = "";
		int port = 1;
		String traceMaskStr = "";

		ServerAdminClient.setTraceMask(host, port, traceMaskStr);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NumberFormatException: For input string: ""
		//       at java.lang.NumberFormatException.forInputString(NumberFormatException.java:48)
		//       at java.lang.Long.parseLong(Long.java:431)
		//       at org.apache.zookeeper.ServerAdminClient.setTraceMask(ServerAdminClient.java:209)
	}

	/**
	 * Run the void setTraceMask(String,int,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testSetTraceMask_15()
		throws Exception {
		String host = "";
		int port = 1;
		String traceMaskStr = "";

		ServerAdminClient.setTraceMask(host, port, traceMaskStr);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NumberFormatException: For input string: ""
		//       at java.lang.NumberFormatException.forInputString(NumberFormatException.java:48)
		//       at java.lang.Long.parseLong(Long.java:431)
		//       at org.apache.zookeeper.ServerAdminClient.setTraceMask(ServerAdminClient.java:209)
	}

	/**
	 * Run the void setTraceMask(String,int,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testSetTraceMask_16()
		throws Exception {
		String host = "";
		int port = 1;
		String traceMaskStr = "";

		ServerAdminClient.setTraceMask(host, port, traceMaskStr);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NumberFormatException: For input string: ""
		//       at java.lang.NumberFormatException.forInputString(NumberFormatException.java:48)
		//       at java.lang.Long.parseLong(Long.java:431)
		//       at org.apache.zookeeper.ServerAdminClient.setTraceMask(ServerAdminClient.java:209)
	}

	/**
	 * Run the void stat(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testStat_1()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.stat(host, port);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:70)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:219)
		//       at java.net.SocketOutputStream.<init>(SocketOutputStream.java:41)
		//       at java.net.PlainSocketImpl.getOutputStream(PlainSocketImpl.java:444)
		//       at java.net.Socket$3.run(Socket.java:839)
		//       at java.security.AccessController.doPrivileged(Native Method)
		//       at java.net.Socket.getOutputStream(Socket.java:836)
		//       at org.apache.zookeeper.ServerAdminClient.stat(ServerAdminClient.java:149)
	}

	/**
	 * Run the void stat(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testStat_2()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.stat(host, port);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:70)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:219)
		//       at java.net.SocketOutputStream.<init>(SocketOutputStream.java:41)
		//       at java.net.PlainSocketImpl.getOutputStream(PlainSocketImpl.java:444)
		//       at java.net.Socket$3.run(Socket.java:839)
		//       at java.security.AccessController.doPrivileged(Native Method)
		//       at java.net.Socket.getOutputStream(Socket.java:836)
		//       at org.apache.zookeeper.ServerAdminClient.stat(ServerAdminClient.java:149)
	}

	/**
	 * Run the void stat(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testStat_3()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.stat(host, port);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:70)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:219)
		//       at java.net.SocketOutputStream.<init>(SocketOutputStream.java:41)
		//       at java.net.PlainSocketImpl.getOutputStream(PlainSocketImpl.java:444)
		//       at java.net.Socket$3.run(Socket.java:839)
		//       at java.security.AccessController.doPrivileged(Native Method)
		//       at java.net.Socket.getOutputStream(Socket.java:836)
		//       at org.apache.zookeeper.ServerAdminClient.stat(ServerAdminClient.java:149)
	}

	/**
	 * Run the void stat(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testStat_4()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.stat(host, port);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:70)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:219)
		//       at java.net.SocketOutputStream.<init>(SocketOutputStream.java:41)
		//       at java.net.PlainSocketImpl.getOutputStream(PlainSocketImpl.java:444)
		//       at java.net.Socket$3.run(Socket.java:839)
		//       at java.security.AccessController.doPrivileged(Native Method)
		//       at java.net.Socket.getOutputStream(Socket.java:836)
		//       at org.apache.zookeeper.ServerAdminClient.stat(ServerAdminClient.java:149)
	}

	/**
	 * Run the void stat(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testStat_5()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.stat(host, port);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:70)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:219)
		//       at java.net.SocketOutputStream.<init>(SocketOutputStream.java:41)
		//       at java.net.PlainSocketImpl.getOutputStream(PlainSocketImpl.java:444)
		//       at java.net.Socket$3.run(Socket.java:839)
		//       at java.security.AccessController.doPrivileged(Native Method)
		//       at java.net.Socket.getOutputStream(Socket.java:836)
		//       at org.apache.zookeeper.ServerAdminClient.stat(ServerAdminClient.java:149)
	}

	/**
	 * Run the void stat(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testStat_6()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.stat(host, port);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:70)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:219)
		//       at java.net.SocketOutputStream.<init>(SocketOutputStream.java:41)
		//       at java.net.PlainSocketImpl.getOutputStream(PlainSocketImpl.java:444)
		//       at java.net.Socket$3.run(Socket.java:839)
		//       at java.security.AccessController.doPrivileged(Native Method)
		//       at java.net.Socket.getOutputStream(Socket.java:836)
		//       at org.apache.zookeeper.ServerAdminClient.stat(ServerAdminClient.java:149)
	}

	/**
	 * Run the void stat(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testStat_7()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.stat(host, port);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:70)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:219)
		//       at java.net.SocketOutputStream.<init>(SocketOutputStream.java:41)
		//       at java.net.PlainSocketImpl.getOutputStream(PlainSocketImpl.java:444)
		//       at java.net.Socket$3.run(Socket.java:839)
		//       at java.security.AccessController.doPrivileged(Native Method)
		//       at java.net.Socket.getOutputStream(Socket.java:836)
		//       at org.apache.zookeeper.ServerAdminClient.stat(ServerAdminClient.java:149)
	}

	/**
	 * Run the void stat(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testStat_8()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.stat(host, port);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:70)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:219)
		//       at java.net.SocketOutputStream.<init>(SocketOutputStream.java:41)
		//       at java.net.PlainSocketImpl.getOutputStream(PlainSocketImpl.java:444)
		//       at java.net.Socket$3.run(Socket.java:839)
		//       at java.security.AccessController.doPrivileged(Native Method)
		//       at java.net.Socket.getOutputStream(Socket.java:836)
		//       at org.apache.zookeeper.ServerAdminClient.stat(ServerAdminClient.java:149)
	}

	/**
	 * Run the void stat(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testStat_9()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.stat(host, port);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:70)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:219)
		//       at java.net.SocketOutputStream.<init>(SocketOutputStream.java:41)
		//       at java.net.PlainSocketImpl.getOutputStream(PlainSocketImpl.java:444)
		//       at java.net.Socket$3.run(Socket.java:839)
		//       at java.security.AccessController.doPrivileged(Native Method)
		//       at java.net.Socket.getOutputStream(Socket.java:836)
		//       at org.apache.zookeeper.ServerAdminClient.stat(ServerAdminClient.java:149)
	}

	/**
	 * Run the void stat(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testStat_10()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.stat(host, port);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:70)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:219)
		//       at java.net.SocketOutputStream.<init>(SocketOutputStream.java:41)
		//       at java.net.PlainSocketImpl.getOutputStream(PlainSocketImpl.java:444)
		//       at java.net.Socket$3.run(Socket.java:839)
		//       at java.security.AccessController.doPrivileged(Native Method)
		//       at java.net.Socket.getOutputStream(Socket.java:836)
		//       at org.apache.zookeeper.ServerAdminClient.stat(ServerAdminClient.java:149)
	}

	/**
	 * Run the void stat(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testStat_11()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.stat(host, port);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:70)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:219)
		//       at java.net.SocketOutputStream.<init>(SocketOutputStream.java:41)
		//       at java.net.PlainSocketImpl.getOutputStream(PlainSocketImpl.java:444)
		//       at java.net.Socket$3.run(Socket.java:839)
		//       at java.security.AccessController.doPrivileged(Native Method)
		//       at java.net.Socket.getOutputStream(Socket.java:836)
		//       at org.apache.zookeeper.ServerAdminClient.stat(ServerAdminClient.java:149)
	}

	/**
	 * Run the void stat(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testStat_12()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.stat(host, port);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:70)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:219)
		//       at java.net.SocketOutputStream.<init>(SocketOutputStream.java:41)
		//       at java.net.PlainSocketImpl.getOutputStream(PlainSocketImpl.java:444)
		//       at java.net.Socket$3.run(Socket.java:839)
		//       at java.security.AccessController.doPrivileged(Native Method)
		//       at java.net.Socket.getOutputStream(Socket.java:836)
		//       at org.apache.zookeeper.ServerAdminClient.stat(ServerAdminClient.java:149)
	}

	/**
	 * Run the void stat(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testStat_13()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.stat(host, port);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:70)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:219)
		//       at java.net.SocketOutputStream.<init>(SocketOutputStream.java:41)
		//       at java.net.PlainSocketImpl.getOutputStream(PlainSocketImpl.java:444)
		//       at java.net.Socket$3.run(Socket.java:839)
		//       at java.security.AccessController.doPrivileged(Native Method)
		//       at java.net.Socket.getOutputStream(Socket.java:836)
		//       at org.apache.zookeeper.ServerAdminClient.stat(ServerAdminClient.java:149)
	}

	/**
	 * Run the void stat(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testStat_14()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.stat(host, port);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:70)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:219)
		//       at java.net.SocketOutputStream.<init>(SocketOutputStream.java:41)
		//       at java.net.PlainSocketImpl.getOutputStream(PlainSocketImpl.java:444)
		//       at java.net.Socket$3.run(Socket.java:839)
		//       at java.security.AccessController.doPrivileged(Native Method)
		//       at java.net.Socket.getOutputStream(Socket.java:836)
		//       at org.apache.zookeeper.ServerAdminClient.stat(ServerAdminClient.java:149)
	}

	/**
	 * Run the void stat(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testStat_15()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.stat(host, port);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:70)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:219)
		//       at java.net.SocketOutputStream.<init>(SocketOutputStream.java:41)
		//       at java.net.PlainSocketImpl.getOutputStream(PlainSocketImpl.java:444)
		//       at java.net.Socket$3.run(Socket.java:839)
		//       at java.security.AccessController.doPrivileged(Native Method)
		//       at java.net.Socket.getOutputStream(Socket.java:836)
		//       at org.apache.zookeeper.ServerAdminClient.stat(ServerAdminClient.java:149)
	}

	/**
	 * Run the void stat(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testStat_16()
		throws Exception {
		String host = "";
		int port = 1;

		ServerAdminClient.stat(host, port);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:70)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:219)
		//       at java.net.SocketOutputStream.<init>(SocketOutputStream.java:41)
		//       at java.net.PlainSocketImpl.getOutputStream(PlainSocketImpl.java:444)
		//       at java.net.Socket$3.run(Socket.java:839)
		//       at java.security.AccessController.doPrivileged(Native Method)
		//       at java.net.Socket.getOutputStream(Socket.java:836)
		//       at org.apache.zookeeper.ServerAdminClient.stat(ServerAdminClient.java:149)
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(ServerAdminClientTest.class);
	}
}