package com.uiuc.org.apache.zookeeper.server.util;

import java.io.ByteArrayOutputStream;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.util.HashMap;
import java.util.Map;
import org.apache.jute.BinaryInputArchive;
import org.apache.jute.BinaryOutputArchive;
import org.apache.jute.InputArchive;
import org.apache.jute.OutputArchive;
import org.apache.jute.Record;
import org.apache.zookeeper.server.DataTree;
import org.apache.zookeeper.server.util.SerializeUtils;
import org.apache.zookeeper.txn.TxnHeader;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>SerializeUtilsTest</code> contains tests for the class <code>{@link SerializeUtils}</code>.
 *
 * @generatedBy CodePro at 10/16/13 7:43 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class SerializeUtilsTest {
	/**
	 * Run the SerializeUtils() constructor test.
	 *
	 * @generatedBy CodePro at 10/16/13 7:43 PM
	 */
	@Test
	public void testSerializeUtils_1()
		throws Exception {
		SerializeUtils result = new SerializeUtils();
		assertNotNull(result);
		// add additional test code here
	}

	/**
	 * Run the void deserializeSnapshot(DataTree,InputArchive,Map<Long,Integer>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:43 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testDeserializeSnapshot_1()
		throws Exception {
		DataTree dt = new DataTree();
		InputArchive ia = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));
		Map<Long, Integer> sessions = new HashMap();

		SerializeUtils.deserializeSnapshot(dt, ia, sessions);

		// add additional test code here
	}

	/**
	 * Run the void deserializeSnapshot(DataTree,InputArchive,Map<Long,Integer>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:43 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testDeserializeSnapshot_2()
		throws Exception {
		DataTree dt = new DataTree();
		InputArchive ia = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));
		Map<Long, Integer> sessions = new HashMap();

		SerializeUtils.deserializeSnapshot(dt, ia, sessions);

		// add additional test code here
	}

	/**
	 * Run the void deserializeSnapshot(DataTree,InputArchive,Map<Long,Integer>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:43 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testDeserializeSnapshot_3()
		throws Exception {
		DataTree dt = new DataTree();
		InputArchive ia = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));
		Map<Long, Integer> sessions = new HashMap();

		SerializeUtils.deserializeSnapshot(dt, ia, sessions);

		// add additional test code here
	}

	/**
	 * Run the void deserializeSnapshot(DataTree,InputArchive,Map<Long,Integer>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:43 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testDeserializeSnapshot_4()
		throws Exception {
		DataTree dt = new DataTree();
		InputArchive ia = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));
		Map<Long, Integer> sessions = new HashMap();

		SerializeUtils.deserializeSnapshot(dt, ia, sessions);

		// add additional test code here
	}

	/**
	 * Run the void deserializeSnapshot(DataTree,InputArchive,Map<Long,Integer>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:43 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testDeserializeSnapshot_5()
		throws Exception {
		DataTree dt = new DataTree();
		InputArchive ia = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));
		Map<Long, Integer> sessions = new HashMap();

		SerializeUtils.deserializeSnapshot(dt, ia, sessions);

		// add additional test code here
	}

	/**
	 * Run the void deserializeSnapshot(DataTree,InputArchive,Map<Long,Integer>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:43 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testDeserializeSnapshot_6()
		throws Exception {
		DataTree dt = new DataTree();
		InputArchive ia = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));
		Map<Long, Integer> sessions = new HashMap();

		SerializeUtils.deserializeSnapshot(dt, ia, sessions);

		// add additional test code here
	}

	/**
	 * Run the void deserializeSnapshot(DataTree,InputArchive,Map<Long,Integer>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:43 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testDeserializeSnapshot_7()
		throws Exception {
		DataTree dt = new DataTree();
		InputArchive ia = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));
		Map<Long, Integer> sessions = new HashMap();

		SerializeUtils.deserializeSnapshot(dt, ia, sessions);

		// add additional test code here
	}

	/**
	 * Run the Record deserializeTxn(byte[],TxnHeader) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:43 PM
	 */
	@Test(expected = java.io.EOFException.class)
	public void testDeserializeTxn_1()
		throws Exception {
		byte[] txnBytes = new byte[] {};
		TxnHeader hdr = new TxnHeader();

		Record result = SerializeUtils.deserializeTxn(txnBytes, hdr);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the Record deserializeTxn(byte[],TxnHeader) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:43 PM
	 */
	@Test(expected = java.io.EOFException.class)
	public void testDeserializeTxn_2()
		throws Exception {
		byte[] txnBytes = new byte[] {};
		TxnHeader hdr = new TxnHeader(1L, 1, 1L, 1L, -11);

		Record result = SerializeUtils.deserializeTxn(txnBytes, hdr);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the Record deserializeTxn(byte[],TxnHeader) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:43 PM
	 */
	@Test(expected = java.io.EOFException.class)
	public void testDeserializeTxn_3()
		throws Exception {
		byte[] txnBytes = new byte[] {};
		TxnHeader hdr = new TxnHeader(1L, 1, 1L, 1L, 0);

		Record result = SerializeUtils.deserializeTxn(txnBytes, hdr);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the Record deserializeTxn(byte[],TxnHeader) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:43 PM
	 */
	@Test(expected = java.io.EOFException.class)
	public void testDeserializeTxn_4()
		throws Exception {
		byte[] txnBytes = new byte[] {};
		TxnHeader hdr = new TxnHeader(1L, 1, 1L, 1L, -10);

		Record result = SerializeUtils.deserializeTxn(txnBytes, hdr);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the Record deserializeTxn(byte[],TxnHeader) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:43 PM
	 */
	@Test(expected = java.io.EOFException.class)
	public void testDeserializeTxn_5()
		throws Exception {
		byte[] txnBytes = new byte[] {};
		TxnHeader hdr = new TxnHeader(1L, 1, 1L, 1L, 1);

		Record result = SerializeUtils.deserializeTxn(txnBytes, hdr);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the Record deserializeTxn(byte[],TxnHeader) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:43 PM
	 */
	@Test(expected = java.io.EOFException.class)
	public void testDeserializeTxn_6()
		throws Exception {
		byte[] txnBytes = new byte[] {};
		TxnHeader hdr = new TxnHeader(1L, 1, 1L, 1L, 2);

		Record result = SerializeUtils.deserializeTxn(txnBytes, hdr);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the Record deserializeTxn(byte[],TxnHeader) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:43 PM
	 */
	@Test(expected = java.io.EOFException.class)
	public void testDeserializeTxn_7()
		throws Exception {
		byte[] txnBytes = new byte[] {};
		TxnHeader hdr = new TxnHeader(1L, 1, 1L, 1L, 5);

		Record result = SerializeUtils.deserializeTxn(txnBytes, hdr);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the Record deserializeTxn(byte[],TxnHeader) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:43 PM
	 */
	@Test(expected = java.io.EOFException.class)
	public void testDeserializeTxn_8()
		throws Exception {
		byte[] txnBytes = new byte[] {};
		TxnHeader hdr = new TxnHeader(1L, 1, 1L, 1L, 7);

		Record result = SerializeUtils.deserializeTxn(txnBytes, hdr);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the Record deserializeTxn(byte[],TxnHeader) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:43 PM
	 */
	@Test(expected = java.io.EOFException.class)
	public void testDeserializeTxn_9()
		throws Exception {
		byte[] txnBytes = new byte[] {};
		TxnHeader hdr = new TxnHeader(1L, 1, 1L, 1L, -1);

		Record result = SerializeUtils.deserializeTxn(txnBytes, hdr);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the Record deserializeTxn(byte[],TxnHeader) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:43 PM
	 */
	@Test(expected = java.io.EOFException.class)
	public void testDeserializeTxn_10()
		throws Exception {
		byte[] txnBytes = new byte[] {};
		TxnHeader hdr = new TxnHeader(1L, 1, 1L, 1L, 14);

		Record result = SerializeUtils.deserializeTxn(txnBytes, hdr);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the void serializeSnapshot(DataTree,OutputArchive,Map<Long,Integer>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:43 PM
	 */
	@Test
	public void testSerializeSnapshot_1()
		throws Exception {
		DataTree dt = new DataTree();
		OutputArchive oa = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		Map<Long, Integer> sessions = new HashMap();

		SerializeUtils.serializeSnapshot(dt, oa, sessions);

		// add additional test code here
	}

	/**
	 * Run the void serializeSnapshot(DataTree,OutputArchive,Map<Long,Integer>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:43 PM
	 */
	@Test
	public void testSerializeSnapshot_2()
		throws Exception {
		DataTree dt = new DataTree();
		OutputArchive oa = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		Map<Long, Integer> sessions = new HashMap();

		SerializeUtils.serializeSnapshot(dt, oa, sessions);

		// add additional test code here
	}

	/**
	 * Run the void serializeSnapshot(DataTree,OutputArchive,Map<Long,Integer>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:43 PM
	 */
	@Test
	public void testSerializeSnapshot_3()
		throws Exception {
		DataTree dt = new DataTree();
		OutputArchive oa = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		Map<Long, Integer> sessions = new HashMap();

		SerializeUtils.serializeSnapshot(dt, oa, sessions);

		// add additional test code here
	}

	/**
	 * Run the void serializeSnapshot(DataTree,OutputArchive,Map<Long,Integer>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:43 PM
	 */
	@Test
	public void testSerializeSnapshot_4()
		throws Exception {
		DataTree dt = new DataTree();
		OutputArchive oa = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		Map<Long, Integer> sessions = new HashMap();

		SerializeUtils.serializeSnapshot(dt, oa, sessions);

		// add additional test code here
	}

	/**
	 * Run the void serializeSnapshot(DataTree,OutputArchive,Map<Long,Integer>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:43 PM
	 */
	@Test
	public void testSerializeSnapshot_5()
		throws Exception {
		DataTree dt = new DataTree();
		OutputArchive oa = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		Map<Long, Integer> sessions = new HashMap();

		SerializeUtils.serializeSnapshot(dt, oa, sessions);

		// add additional test code here
	}

	/**
	 * Run the void serializeSnapshot(DataTree,OutputArchive,Map<Long,Integer>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:43 PM
	 */
	@Test
	public void testSerializeSnapshot_6()
		throws Exception {
		DataTree dt = new DataTree();
		OutputArchive oa = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		Map<Long, Integer> sessions = new HashMap();

		SerializeUtils.serializeSnapshot(dt, oa, sessions);

		// add additional test code here
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:43 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:43 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:43 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(SerializeUtilsTest.class);
	}
}