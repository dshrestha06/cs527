package com.uiuc.org.apache.zookeeper.server.quorum;

import java.net.Socket;
import java.util.LinkedList;
import java.util.List;
import org.apache.zookeeper.data.Id;
import org.apache.zookeeper.server.quorum.Leader;
import org.apache.zookeeper.server.quorum.LearnerHandler;
import org.apache.zookeeper.server.quorum.QuorumPacket;
import org.apache.zookeeper.server.quorum.QuorumPeer;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>LearnerHandlerTest</code> contains tests for the class <code>{@link LearnerHandler}</code>.
 *
 * @generatedBy CodePro at 10/16/13 7:59 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class LearnerHandlerTest {
	/**
	 * Run the LearnerHandler(Socket,Leader) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@Test
	public void testLearnerHandler_1()
		throws Exception {
		Socket sock = new Socket();
		Leader leader = null;

		LearnerHandler result = new LearnerHandler(sock, leader);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.LearnerHandler.<init>(LearnerHandler.java:100)
		assertNotNull(result);
	}

	/**
	 * Run the String packetToString(QuorumPacket) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@Test
	public void testPacketToString_1()
		throws Exception {
		QuorumPacket p = new QuorumPacket();

		String result = LearnerHandler.packetToString(p);

		// add additional test code here
		assertEquals(null, result);
	}

	/**
	 * Run the String packetToString(QuorumPacket) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@Test
	public void testPacketToString_2()
		throws Exception {
		QuorumPacket p = new QuorumPacket(2, 1L, new byte[] {}, new LinkedList());

		String result = LearnerHandler.packetToString(p);

		// add additional test code here
		assertEquals(null, result);
	}

	/**
	 * Run the String packetToString(QuorumPacket) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@Test
	public void testPacketToString_3()
		throws Exception {
		QuorumPacket p = new QuorumPacket(7, 1L, new byte[] {}, new LinkedList());

		String result = LearnerHandler.packetToString(p);

		// add additional test code here
		assertEquals(null, result);
	}

	/**
	 * Run the String packetToString(QuorumPacket) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@Test
	public void testPacketToString_4()
		throws Exception {
		QuorumPacket p = new QuorumPacket(3, 1L, new byte[] {}, new LinkedList());

		String result = LearnerHandler.packetToString(p);

		// add additional test code here
		assertEquals(null, result);
	}

	/**
	 * Run the String packetToString(QuorumPacket) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@Test
	public void testPacketToString_5()
		throws Exception {
		QuorumPacket p = new QuorumPacket(4, 1L, new byte[] {}, new LinkedList());

		String result = LearnerHandler.packetToString(p);

		// add additional test code here
		assertEquals(null, result);
	}

	/**
	 * Run the String packetToString(QuorumPacket) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@Test
	public void testPacketToString_6()
		throws Exception {
		QuorumPacket p = new QuorumPacket(11, 1L, new byte[] {}, new LinkedList());

		String result = LearnerHandler.packetToString(p);

		// add additional test code here
		assertEquals(null, result);
	}

	/**
	 * Run the String packetToString(QuorumPacket) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@Test
	public void testPacketToString_7()
		throws Exception {
		QuorumPacket p = new QuorumPacket(10, 1L, new byte[] {}, new LinkedList());

		String result = LearnerHandler.packetToString(p);

		// add additional test code here
		assertEquals(null, result);
	}

	/**
	 * Run the String packetToString(QuorumPacket) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@Test
	public void testPacketToString_8()
		throws Exception {
		QuorumPacket p = new QuorumPacket(5, 1L, new byte[] {}, new LinkedList());

		String result = LearnerHandler.packetToString(p);

		// add additional test code here
		assertEquals(null, result);
	}

	/**
	 * Run the String packetToString(QuorumPacket) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@Test
	public void testPacketToString_9()
		throws Exception {
		QuorumPacket p = new QuorumPacket(1, 1L, new byte[] {}, new LinkedList());

		String result = LearnerHandler.packetToString(p);

		// add additional test code here
		assertEquals(null, result);
	}

	/**
	 * Run the String packetToString(QuorumPacket) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@Test
	public void testPacketToString_10()
		throws Exception {
		QuorumPacket p = new QuorumPacket(6, 1L, new byte[] {}, new LinkedList());

		String result = LearnerHandler.packetToString(p);

		// add additional test code here
		assertEquals(null, result);
	}

	/**
	 * Run the String packetToString(QuorumPacket) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@Test
	public void testPacketToString_11()
		throws Exception {
		QuorumPacket p = new QuorumPacket(12, 1L, new byte[] {}, new LinkedList());

		String result = LearnerHandler.packetToString(p);

		// add additional test code here
		assertEquals(null, result);
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(LearnerHandlerTest.class);
	}
}