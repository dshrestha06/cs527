package com.uiuc.org.apache.zookeeper.version.util;

import java.io.File;
import org.apache.zookeeper.version.util.VerGen;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>VerGenTest</code> contains tests for the class <code>{@link VerGen}</code>.
 *
 * @generatedBy CodePro at 10/16/13 7:40 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class VerGenTest {
	/**
	 * Run the VerGen() constructor test.
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test
	public void testVerGen_1()
		throws Exception {
		VerGen result = new VerGen();
		assertNotNull(result);
		// add additional test code here
	}

	/**
	 * Run the void generateFile(File,Version,int,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test
	public void testGenerateFile_1()
		throws Exception {
		File outputDir = new File("");
		org.apache.zookeeper.version.util.VerGen.Version version = new org.apache.zookeeper.version.util.VerGen.Version();
		int rev = 1;
		String buildDate = "";

		VerGen.generateFile(outputDir, version, rev, buildDate);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:183)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:145)
		//       at java.io.FileWriter.<init>(FileWriter.java:73)
		//       at org.apache.zookeeper.version.util.VerGen.generateFile(VerGen.java:56)
	}

	/**
	 * Run the void generateFile(File,Version,int,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test
	public void testGenerateFile_2()
		throws Exception {
		File outputDir = new File("");
		org.apache.zookeeper.version.util.VerGen.Version version = new org.apache.zookeeper.version.util.VerGen.Version();
		int rev = 1;
		String buildDate = "";

		VerGen.generateFile(outputDir, version, rev, buildDate);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:183)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:145)
		//       at java.io.FileWriter.<init>(FileWriter.java:73)
		//       at org.apache.zookeeper.version.util.VerGen.generateFile(VerGen.java:56)
	}

	/**
	 * Run the void generateFile(File,Version,int,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test
	public void testGenerateFile_3()
		throws Exception {
		File outputDir = new File("");
		org.apache.zookeeper.version.util.VerGen.Version version = new org.apache.zookeeper.version.util.VerGen.Version();
		int rev = 1;
		String buildDate = "";

		VerGen.generateFile(outputDir, version, rev, buildDate);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:183)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:145)
		//       at java.io.FileWriter.<init>(FileWriter.java:73)
		//       at org.apache.zookeeper.version.util.VerGen.generateFile(VerGen.java:56)
	}

	/**
	 * Run the void generateFile(File,Version,int,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test
	public void testGenerateFile_4()
		throws Exception {
		File outputDir = new File("");
		org.apache.zookeeper.version.util.VerGen.Version version = new org.apache.zookeeper.version.util.VerGen.Version();
		int rev = 1;
		String buildDate = "";

		VerGen.generateFile(outputDir, version, rev, buildDate);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:183)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:145)
		//       at java.io.FileWriter.<init>(FileWriter.java:73)
		//       at org.apache.zookeeper.version.util.VerGen.generateFile(VerGen.java:56)
	}

	/**
	 * Run the void generateFile(File,Version,int,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test
	public void testGenerateFile_5()
		throws Exception {
		File outputDir = new File("");
		org.apache.zookeeper.version.util.VerGen.Version version = new org.apache.zookeeper.version.util.VerGen.Version();
		int rev = 1;
		String buildDate = "";

		VerGen.generateFile(outputDir, version, rev, buildDate);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:183)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:145)
		//       at java.io.FileWriter.<init>(FileWriter.java:73)
		//       at org.apache.zookeeper.version.util.VerGen.generateFile(VerGen.java:56)
	}

	/**
	 * Run the void generateFile(File,Version,int,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test
	public void testGenerateFile_6()
		throws Exception {
		File outputDir = new File("");
		org.apache.zookeeper.version.util.VerGen.Version version = new org.apache.zookeeper.version.util.VerGen.Version();
		int rev = 1;
		String buildDate = "";

		VerGen.generateFile(outputDir, version, rev, buildDate);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:183)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:145)
		//       at java.io.FileWriter.<init>(FileWriter.java:73)
		//       at org.apache.zookeeper.version.util.VerGen.generateFile(VerGen.java:56)
	}

	/**
	 * Run the void generateFile(File,Version,int,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test
	public void testGenerateFile_7()
		throws Exception {
		File outputDir = new File("");
		org.apache.zookeeper.version.util.VerGen.Version version = new org.apache.zookeeper.version.util.VerGen.Version();
		int rev = 1;
		String buildDate = "";

		VerGen.generateFile(outputDir, version, rev, buildDate);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:183)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:145)
		//       at java.io.FileWriter.<init>(FileWriter.java:73)
		//       at org.apache.zookeeper.version.util.VerGen.generateFile(VerGen.java:56)
	}

	/**
	 * Run the void generateFile(File,Version,int,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test
	public void testGenerateFile_8()
		throws Exception {
		File outputDir = new File("");
		org.apache.zookeeper.version.util.VerGen.Version version = new org.apache.zookeeper.version.util.VerGen.Version();
		int rev = 1;
		String buildDate = "";

		VerGen.generateFile(outputDir, version, rev, buildDate);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:183)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:145)
		//       at java.io.FileWriter.<init>(FileWriter.java:73)
		//       at org.apache.zookeeper.version.util.VerGen.generateFile(VerGen.java:56)
	}

	/**
	 * Run the void generateFile(File,Version,int,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test
	public void testGenerateFile_9()
		throws Exception {
		File outputDir = new File("");
		org.apache.zookeeper.version.util.VerGen.Version version = new org.apache.zookeeper.version.util.VerGen.Version();
		int rev = 1;
		String buildDate = "";

		VerGen.generateFile(outputDir, version, rev, buildDate);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:183)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:145)
		//       at java.io.FileWriter.<init>(FileWriter.java:73)
		//       at org.apache.zookeeper.version.util.VerGen.generateFile(VerGen.java:56)
	}

	/**
	 * Run the void generateFile(File,Version,int,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test
	public void testGenerateFile_10()
		throws Exception {
		File outputDir = new File("");
		org.apache.zookeeper.version.util.VerGen.Version version = new org.apache.zookeeper.version.util.VerGen.Version();
		int rev = 1;
		String buildDate = "";

		VerGen.generateFile(outputDir, version, rev, buildDate);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:183)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:145)
		//       at java.io.FileWriter.<init>(FileWriter.java:73)
		//       at org.apache.zookeeper.version.util.VerGen.generateFile(VerGen.java:56)
	}

	/**
	 * Run the void generateFile(File,Version,int,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test
	public void testGenerateFile_11()
		throws Exception {
		File outputDir = new File("");
		org.apache.zookeeper.version.util.VerGen.Version version = new org.apache.zookeeper.version.util.VerGen.Version();
		int rev = 1;
		String buildDate = "";

		VerGen.generateFile(outputDir, version, rev, buildDate);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:183)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:145)
		//       at java.io.FileWriter.<init>(FileWriter.java:73)
		//       at org.apache.zookeeper.version.util.VerGen.generateFile(VerGen.java:56)
	}

	/**
	 * Run the void generateFile(File,Version,int,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test
	public void testGenerateFile_12()
		throws Exception {
		File outputDir = new File("");
		org.apache.zookeeper.version.util.VerGen.Version version = new org.apache.zookeeper.version.util.VerGen.Version();
		int rev = 1;
		String buildDate = "";

		VerGen.generateFile(outputDir, version, rev, buildDate);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:183)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:145)
		//       at java.io.FileWriter.<init>(FileWriter.java:73)
		//       at org.apache.zookeeper.version.util.VerGen.generateFile(VerGen.java:56)
	}

	/**
	 * Run the void generateFile(File,Version,int,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test
	public void testGenerateFile_13()
		throws Exception {
		File outputDir = new File("");
		org.apache.zookeeper.version.util.VerGen.Version version = new org.apache.zookeeper.version.util.VerGen.Version();
		version.micro = 1;
		version.maj = 1;
		version.qualifier = null;
		version.min = 1;
		int rev = 1;
		String buildDate = "";

		VerGen.generateFile(outputDir, version, rev, buildDate);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:183)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:145)
		//       at java.io.FileWriter.<init>(FileWriter.java:73)
		//       at org.apache.zookeeper.version.util.VerGen.generateFile(VerGen.java:56)
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test
	public void testMain_1()
		throws Exception {
		String[] args = new String[] {"a", "0", "a", null};

		VerGen.main(args);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Exit while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkExit(CodeProJUnitSecurityManager.java:57)
		//       at java.lang.Runtime.exit(Runtime.java:88)
		//       at java.lang.System.exit(System.java:904)
		//       at org.apache.zookeeper.version.util.VerGen.printUsage(VerGen.java:34)
		//       at org.apache.zookeeper.version.util.VerGen.main(VerGen.java:155)
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test
	public void testMain_2()
		throws Exception {
		String[] args = new String[] {"a", "0", "a"};

		VerGen.main(args);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Exit while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkExit(CodeProJUnitSecurityManager.java:57)
		//       at java.lang.Runtime.exit(Runtime.java:88)
		//       at java.lang.System.exit(System.java:904)
		//       at org.apache.zookeeper.version.util.VerGen.main(VerGen.java:161)
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test
	public void testMain_3()
		throws Exception {
		String[] args = new String[] {"a", "0", "a", null};

		VerGen.main(args);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Exit while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkExit(CodeProJUnitSecurityManager.java:57)
		//       at java.lang.Runtime.exit(Runtime.java:88)
		//       at java.lang.System.exit(System.java:904)
		//       at org.apache.zookeeper.version.util.VerGen.printUsage(VerGen.java:34)
		//       at org.apache.zookeeper.version.util.VerGen.main(VerGen.java:155)
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test
	public void testMain_4()
		throws Exception {
		String[] args = new String[] {"a", "0", "a"};

		VerGen.main(args);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Exit while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkExit(CodeProJUnitSecurityManager.java:57)
		//       at java.lang.Runtime.exit(Runtime.java:88)
		//       at java.lang.System.exit(System.java:904)
		//       at org.apache.zookeeper.version.util.VerGen.main(VerGen.java:161)
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test
	public void testMain_5()
		throws Exception {
		String[] args = new String[] {"", "", "", null};

		VerGen.main(args);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Exit while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkExit(CodeProJUnitSecurityManager.java:57)
		//       at java.lang.Runtime.exit(Runtime.java:88)
		//       at java.lang.System.exit(System.java:904)
		//       at org.apache.zookeeper.version.util.VerGen.printUsage(VerGen.java:34)
		//       at org.apache.zookeeper.version.util.VerGen.main(VerGen.java:155)
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test
	public void testMain_6()
		throws Exception {
		String[] args = new String[] {"", "", ""};

		VerGen.main(args);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Exit while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkExit(CodeProJUnitSecurityManager.java:57)
		//       at java.lang.Runtime.exit(Runtime.java:88)
		//       at java.lang.System.exit(System.java:904)
		//       at org.apache.zookeeper.version.util.VerGen.main(VerGen.java:161)
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test
	public void testMain_7()
		throws Exception {
		String[] args = new String[] {"", "", "", null};

		VerGen.main(args);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Exit while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkExit(CodeProJUnitSecurityManager.java:57)
		//       at java.lang.Runtime.exit(Runtime.java:88)
		//       at java.lang.System.exit(System.java:904)
		//       at org.apache.zookeeper.version.util.VerGen.printUsage(VerGen.java:34)
		//       at org.apache.zookeeper.version.util.VerGen.main(VerGen.java:155)
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test
	public void testMain_8()
		throws Exception {
		String[] args = new String[] {"", "", ""};

		VerGen.main(args);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Exit while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkExit(CodeProJUnitSecurityManager.java:57)
		//       at java.lang.Runtime.exit(Runtime.java:88)
		//       at java.lang.System.exit(System.java:904)
		//       at org.apache.zookeeper.version.util.VerGen.main(VerGen.java:161)
	}

	/**
	 * Run the org.apache.zookeeper.version.util.VerGen.Version parseVersionString(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test
	public void testParseVersionString_1()
		throws Exception {
		String input = "";

		org.apache.zookeeper.version.util.VerGen.Version result = VerGen.parseVersionString(input);

		// add additional test code here
		assertEquals(null, result);
	}

	/**
	 * Run the org.apache.zookeeper.version.util.VerGen.Version parseVersionString(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test
	public void testParseVersionString_2()
		throws Exception {
		String input = "";

		org.apache.zookeeper.version.util.VerGen.Version result = VerGen.parseVersionString(input);

		// add additional test code here
		assertEquals(null, result);
	}

	/**
	 * Run the org.apache.zookeeper.version.util.VerGen.Version parseVersionString(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test
	public void testParseVersionString_3()
		throws Exception {
		String input = "a";

		org.apache.zookeeper.version.util.VerGen.Version result = VerGen.parseVersionString(input);

		// add additional test code here
		assertEquals(null, result);
	}

	/**
	 * Run the org.apache.zookeeper.version.util.VerGen.Version parseVersionString(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test
	public void testParseVersionString_4()
		throws Exception {
		String input = "a";

		org.apache.zookeeper.version.util.VerGen.Version result = VerGen.parseVersionString(input);

		// add additional test code here
		assertEquals(null, result);
	}

	/**
	 * Run the org.apache.zookeeper.version.util.VerGen.Version parseVersionString(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test
	public void testParseVersionString_5()
		throws Exception {
		String input = "a";

		org.apache.zookeeper.version.util.VerGen.Version result = VerGen.parseVersionString(input);

		// add additional test code here
		assertEquals(null, result);
	}

	/**
	 * Run the org.apache.zookeeper.version.util.VerGen.Version parseVersionString(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test
	public void testParseVersionString_6()
		throws Exception {
		String input = "a";

		org.apache.zookeeper.version.util.VerGen.Version result = VerGen.parseVersionString(input);

		// add additional test code here
		assertEquals(null, result);
	}

	/**
	 * Run the void printUsage() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test
	public void testPrintUsage_1()
		throws Exception {

		VerGen.printUsage();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Exit while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkExit(CodeProJUnitSecurityManager.java:57)
		//       at java.lang.Runtime.exit(Runtime.java:88)
		//       at java.lang.System.exit(System.java:904)
		//       at org.apache.zookeeper.version.util.VerGen.printUsage(VerGen.java:34)
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(VerGenTest.class);
	}
}