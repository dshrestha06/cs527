package com.uiuc.org.apache.zookeeper.server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.login.Configuration;
import org.apache.zookeeper.JaasConfiguration;
import org.apache.zookeeper.Login;
import org.apache.zookeeper.server.NIOServerCnxn;
import org.apache.zookeeper.server.NIOServerCnxnFactory;
import org.apache.zookeeper.server.ServerCnxn;
import org.apache.zookeeper.server.ServerCnxnFactory;
import org.apache.zookeeper.server.ZooKeeperServer;
import org.apache.zookeeper.server.auth.SaslServerCallbackHandler;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>ServerCnxnFactoryTest</code> contains tests for the class <code>{@link ServerCnxnFactory}</code>.
 *
 * @generatedBy CodePro at 10/16/13 8:24 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class ServerCnxnFactoryTest {
	/**
	 * Run the void configureSaslLogin() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testConfigureSaslLogin_1()
		throws Exception {
		NIOServerCnxnFactory fixture = new NIOServerCnxnFactory();
		fixture.login = new Login("", new SaslServerCallbackHandler(new JaasConfiguration()));

		fixture.configureSaslLogin();

		// add additional test code here
	}

	/**
	 * Run the void configureSaslLogin() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testConfigureSaslLogin_2()
		throws Exception {
		NIOServerCnxnFactory fixture = new NIOServerCnxnFactory();
		fixture.login = new Login("", new SaslServerCallbackHandler(new JaasConfiguration()));

		fixture.configureSaslLogin();

		// add additional test code here
	}

	/**
	 * Run the void configureSaslLogin() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testConfigureSaslLogin_3()
		throws Exception {
		NIOServerCnxnFactory fixture = new NIOServerCnxnFactory();
		fixture.login = new Login("", new SaslServerCallbackHandler(new JaasConfiguration()));

		fixture.configureSaslLogin();

		// add additional test code here
	}

	/**
	 * Run the void configureSaslLogin() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testConfigureSaslLogin_4()
		throws Exception {
		NIOServerCnxnFactory fixture = new NIOServerCnxnFactory();
		fixture.login = new Login("", new SaslServerCallbackHandler(new JaasConfiguration()));

		fixture.configureSaslLogin();

		// add additional test code here
	}

	/**
	 * Run the void configureSaslLogin() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testConfigureSaslLogin_5()
		throws Exception {
		NIOServerCnxnFactory fixture = new NIOServerCnxnFactory();
		fixture.login = new Login("", new SaslServerCallbackHandler(new JaasConfiguration()));

		fixture.configureSaslLogin();

		// add additional test code here
	}

	/**
	 * Run the void configureSaslLogin() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testConfigureSaslLogin_6()
		throws Exception {
		NIOServerCnxnFactory fixture = new NIOServerCnxnFactory();
		fixture.login = new Login("", new SaslServerCallbackHandler(new JaasConfiguration()));

		fixture.configureSaslLogin();

		// add additional test code here
	}

	/**
	 * Run the void configureSaslLogin() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testConfigureSaslLogin_7()
		throws Exception {
		NIOServerCnxnFactory fixture = new NIOServerCnxnFactory();
		fixture.login = new Login("", new SaslServerCallbackHandler(new JaasConfiguration()));

		fixture.configureSaslLogin();

		// add additional test code here
	}

	/**
	 * Run the ServerCnxnFactory createFactory() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testCreateFactory_1()
		throws Exception {

		ServerCnxnFactory result = ServerCnxnFactory.createFactory();

		// add additional test code here
		assertNotNull(result);
		assertEquals(0, result.getNumAliveConnections());
		assertEquals(60, result.getMaxClientCnxnsPerHost());
	}

	/**
	 * Run the ServerCnxnFactory createFactory() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testCreateFactory_2()
		throws Exception {

		ServerCnxnFactory result = ServerCnxnFactory.createFactory();

		// add additional test code here
		assertNotNull(result);
		assertEquals(0, result.getNumAliveConnections());
		assertEquals(60, result.getMaxClientCnxnsPerHost());
	}

	/**
	 * Run the ServerCnxnFactory createFactory(int,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testCreateFactory_3()
		throws Exception {
		int clientPort = 1;
		int maxClientCnxns = 1;

		ServerCnxnFactory result = ServerCnxnFactory.createFactory(clientPort, maxClientCnxns);

		// add additional test code here
		assertNotNull(result);
		assertEquals(1, result.getLocalPort());
		assertEquals(0, result.getNumAliveConnections());
		assertEquals(1, result.getMaxClientCnxnsPerHost());
	}

	/**
	 * Run the ServerCnxnFactory createFactory(int,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testCreateFactory_4()
		throws Exception {
		int clientPort = 1;
		int maxClientCnxns = 1;

		ServerCnxnFactory result = ServerCnxnFactory.createFactory(clientPort, maxClientCnxns);

		// add additional test code here
		assertNotNull(result);
		assertEquals(1, result.getLocalPort());
		assertEquals(0, result.getNumAliveConnections());
		assertEquals(1, result.getMaxClientCnxnsPerHost());
	}

	/**
	 * Run the ServerCnxnFactory createFactory(InetSocketAddress,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testCreateFactory_5()
		throws Exception {
		InetSocketAddress addr = new InetSocketAddress(1);
		int maxClientCnxns = 1;

		ServerCnxnFactory result = ServerCnxnFactory.createFactory(addr, maxClientCnxns);

		// add additional test code here
		assertNotNull(result);
		assertEquals(1, result.getLocalPort());
		assertEquals(0, result.getNumAliveConnections());
		assertEquals(1, result.getMaxClientCnxnsPerHost());
	}

	/**
	 * Run the ServerCnxnFactory createFactory(InetSocketAddress,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testCreateFactory_6()
		throws Exception {
		InetSocketAddress addr = new InetSocketAddress(1);
		int maxClientCnxns = 1;

		ServerCnxnFactory result = ServerCnxnFactory.createFactory(addr, maxClientCnxns);

		// add additional test code here
		assertNotNull(result);
		assertEquals(1, result.getLocalPort());
		assertEquals(0, result.getNumAliveConnections());
		assertEquals(1, result.getMaxClientCnxnsPerHost());
	}

	/**
	 * Run the ServerCnxnFactory createFactory(InetSocketAddress,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testCreateFactory_7()
		throws Exception {
		InetSocketAddress addr = new InetSocketAddress(1);
		int maxClientCnxns = 1;

		ServerCnxnFactory result = ServerCnxnFactory.createFactory(addr, maxClientCnxns);

		// add additional test code here
		assertNotNull(result);
		assertEquals(1, result.getLocalPort());
		assertEquals(0, result.getNumAliveConnections());
		assertEquals(1, result.getMaxClientCnxnsPerHost());
	}

	/**
	 * Run the int getNumAliveConnections() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testGetNumAliveConnections_1()
		throws Exception {
		NIOServerCnxnFactory fixture = new NIOServerCnxnFactory();
		fixture.login = new Login("", new SaslServerCallbackHandler(new JaasConfiguration()));

		int result = fixture.getNumAliveConnections();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.io.IOException: Could not find a 'Server' entry in this configuration: Server cannot start.
		//       at org.apache.zookeeper.server.auth.SaslServerCallbackHandler.<init>(SaslServerCallbackHandler.java:57)
		assertEquals(0, result);
	}

	/**
	 * Run the void registerConnection(ServerCnxn) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testRegisterConnection_1()
		throws Exception {
		NIOServerCnxnFactory fixture = new NIOServerCnxnFactory();
		fixture.login = new Login("", new SaslServerCallbackHandler(new JaasConfiguration()));
		ServerCnxn serverCnxn = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());

		fixture.registerConnection(serverCnxn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.io.IOException: Could not find a 'Server' entry in this configuration: Server cannot start.
		//       at org.apache.zookeeper.server.auth.SaslServerCallbackHandler.<init>(SaslServerCallbackHandler.java:57)
	}

	/**
	 * Run the void registerConnection(ServerCnxn) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testRegisterConnection_2()
		throws Exception {
		NIOServerCnxnFactory fixture = new NIOServerCnxnFactory();
		fixture.login = new Login("", new SaslServerCallbackHandler(new JaasConfiguration()));
		ServerCnxn serverCnxn = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());

		fixture.registerConnection(serverCnxn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.io.IOException: Could not find a 'Server' entry in this configuration: Server cannot start.
		//       at org.apache.zookeeper.server.auth.SaslServerCallbackHandler.<init>(SaslServerCallbackHandler.java:57)
	}

	/**
	 * Run the void registerConnection(ServerCnxn) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testRegisterConnection_3()
		throws Exception {
		NIOServerCnxnFactory fixture = new NIOServerCnxnFactory();
		fixture.login = new Login("", new SaslServerCallbackHandler(new JaasConfiguration()));
		ServerCnxn serverCnxn = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());

		fixture.registerConnection(serverCnxn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.io.IOException: Could not find a 'Server' entry in this configuration: Server cannot start.
		//       at org.apache.zookeeper.server.auth.SaslServerCallbackHandler.<init>(SaslServerCallbackHandler.java:57)
	}

	/**
	 * Run the void setZooKeeperServer(ZooKeeperServer) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testSetZooKeeperServer_1()
		throws Exception {
		NIOServerCnxnFactory fixture = new NIOServerCnxnFactory();
		fixture.login = new Login("", new SaslServerCallbackHandler(new JaasConfiguration()));
		ZooKeeperServer zk = new ZooKeeperServer();

		fixture.setZooKeeperServer(zk);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.io.IOException: Could not find a 'Server' entry in this configuration: Server cannot start.
		//       at org.apache.zookeeper.server.auth.SaslServerCallbackHandler.<init>(SaslServerCallbackHandler.java:57)
	}

	/**
	 * Run the void setZooKeeperServer(ZooKeeperServer) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testSetZooKeeperServer_2()
		throws Exception {
		NIOServerCnxnFactory fixture = new NIOServerCnxnFactory();
		fixture.login = new Login("", new SaslServerCallbackHandler(new JaasConfiguration()));
		ZooKeeperServer zk = null;

		fixture.setZooKeeperServer(zk);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.io.IOException: Could not find a 'Server' entry in this configuration: Server cannot start.
		//       at org.apache.zookeeper.server.auth.SaslServerCallbackHandler.<init>(SaslServerCallbackHandler.java:57)
	}

	/**
	 * Run the void unregisterConnection(ServerCnxn) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testUnregisterConnection_1()
		throws Exception {
		NIOServerCnxnFactory fixture = new NIOServerCnxnFactory();
		fixture.login = new Login("", new SaslServerCallbackHandler(new JaasConfiguration()));
		ServerCnxn serverCnxn = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());

		fixture.unregisterConnection(serverCnxn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.io.IOException: Could not find a 'Server' entry in this configuration: Server cannot start.
		//       at org.apache.zookeeper.server.auth.SaslServerCallbackHandler.<init>(SaslServerCallbackHandler.java:57)
	}

	/**
	 * Run the void unregisterConnection(ServerCnxn) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testUnregisterConnection_2()
		throws Exception {
		NIOServerCnxnFactory fixture = new NIOServerCnxnFactory();
		fixture.login = new Login("", new SaslServerCallbackHandler(new JaasConfiguration()));
		ServerCnxn serverCnxn = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());

		fixture.unregisterConnection(serverCnxn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.io.IOException: Could not find a 'Server' entry in this configuration: Server cannot start.
		//       at org.apache.zookeeper.server.auth.SaslServerCallbackHandler.<init>(SaslServerCallbackHandler.java:57)
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(ServerCnxnFactoryTest.class);
	}
}