package com.uiuc.org.apache.zookeeper;

import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>CreateModeTest</code> contains tests for the class <code>{@link CreateMode}</code>.
 *
 * @generatedBy CodePro at 10/16/13 7:23 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class CreateModeTest {
	/**
	 * Run the CreateMode(int,boolean,boolean) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
/*	@Test
	public void testCreateMode_1()
		throws Exception {
		int flag = 1;
		boolean ephemeral = true;
		boolean sequential = true;

		CreateMode result = new CreateMode(flag, ephemeral, sequential);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NoSuchMethodException: org.apache.zookeeper.CreateMode.<init>(int, boolean, boolean)
		//       at java.lang.Class.getConstructor0(Class.java:2706)
		//       at java.lang.Class.getDeclaredConstructor(Class.java:1985)
		//       at com.instantiations.eclipse.analysis.expression.model.InstanceCreationExpression.findConstructor(InstanceCreationExpression.java:572)
		//       at com.instantiations.eclipse.analysis.expression.model.InstanceCreationExpression.execute(InstanceCreationExpression.java:452)
		//       at com.instantiations.assist.eclipse.junit.execution.core.ExecutionRequest.execute(ExecutionRequest.java:286)
		//       at com.instantiations.assist.eclipse.junit.execution.communication.LocalExecutionClient$1.run(LocalExecutionClient.java:158)
		//       at java.lang.Thread.run(Thread.java:662)
		assertNotNull(result);
	}*/

	/**
	 * Run the CreateMode fromFlag(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testFromFlag_1()
		throws Exception {
		int flag = 0;

		CreateMode result = CreateMode.fromFlag(flag);

		// add additional test code here
		assertNotNull(result);
		assertEquals(0, result.toFlag());
		assertEquals(false, result.isSequential());
		assertEquals(false, result.isEphemeral());
		assertEquals("PERSISTENT", result.name());
		assertEquals("PERSISTENT", result.toString());
		assertEquals(0, result.ordinal());
	}

	/**
	 * Run the CreateMode fromFlag(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testFromFlag_2()
		throws Exception {
		int flag = 1;

		CreateMode result = CreateMode.fromFlag(flag);

		// add additional test code here
		assertNotNull(result);
		assertEquals(1, result.toFlag());
		assertEquals(false, result.isSequential());
		assertEquals(true, result.isEphemeral());
		assertEquals("EPHEMERAL", result.name());
		assertEquals("EPHEMERAL", result.toString());
		assertEquals(2, result.ordinal());
	}

	/**
	 * Run the CreateMode fromFlag(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testFromFlag_3()
		throws Exception {
		int flag = 2;

		CreateMode result = CreateMode.fromFlag(flag);

		// add additional test code here
		assertNotNull(result);
		assertEquals(2, result.toFlag());
		assertEquals(true, result.isSequential());
		assertEquals(false, result.isEphemeral());
		assertEquals("PERSISTENT_SEQUENTIAL", result.name());
		assertEquals("PERSISTENT_SEQUENTIAL", result.toString());
		assertEquals(1, result.ordinal());
	}

	/**
	 * Run the CreateMode fromFlag(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testFromFlag_4()
		throws Exception {
		int flag = 3;

		CreateMode result = CreateMode.fromFlag(flag);

		// add additional test code here
		assertNotNull(result);
		assertEquals(3, result.toFlag());
		assertEquals(true, result.isSequential());
		assertEquals(true, result.isEphemeral());
		assertEquals("EPHEMERAL_SEQUENTIAL", result.name());
		assertEquals("EPHEMERAL_SEQUENTIAL", result.toString());
		assertEquals(3, result.ordinal());
	}

	/**
	 * Run the CreateMode fromFlag(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test(expected = org.apache.zookeeper.KeeperException.BadArgumentsException.class)
	public void testFromFlag_5()
		throws Exception {
		int flag = 4;

		CreateMode result = CreateMode.fromFlag(flag);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the boolean isEphemeral() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testIsEphemeral_1()
		throws Exception {
		CreateMode fixture = CreateMode.EPHEMERAL;

		boolean result = fixture.isEphemeral();

		// add additional test code here
		assertEquals(true, result);
	}

	/**
	 * Run the boolean isEphemeral() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testIsEphemeral_2()
		throws Exception {
		CreateMode fixture = CreateMode.EPHEMERAL;

		boolean result = fixture.isEphemeral();

		// add additional test code here
		assertEquals(true, result);
	}

	/**
	 * Run the boolean isSequential() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testIsSequential_1()
		throws Exception {
		CreateMode fixture = CreateMode.EPHEMERAL;

		boolean result = fixture.isSequential();

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean isSequential() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testIsSequential_2()
		throws Exception {
		CreateMode fixture = CreateMode.EPHEMERAL;

		boolean result = fixture.isSequential();

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the int toFlag() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testToFlag_1()
		throws Exception {
		CreateMode fixture = CreateMode.EPHEMERAL;

		int result = fixture.toFlag();

		// add additional test code here
		assertEquals(1, result);
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(CreateModeTest.class);
	}
}