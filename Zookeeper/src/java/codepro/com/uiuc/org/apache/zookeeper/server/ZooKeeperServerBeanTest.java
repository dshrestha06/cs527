package com.uiuc.org.apache.zookeeper.server;

import org.apache.zookeeper.server.ZooKeeperServer;
import org.apache.zookeeper.server.ZooKeeperServerBean;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>ZooKeeperServerBeanTest</code> contains tests for the class <code>{@link ZooKeeperServerBean}</code>.
 *
 * @generatedBy CodePro at 10/16/13 8:27 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class ZooKeeperServerBeanTest {
	/**
	 * Run the ZooKeeperServerBean(ZooKeeperServer) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:27 PM
	 */
	@Test
	public void testZooKeeperServerBean_1()
		throws Exception {
		ZooKeeperServer zks = new ZooKeeperServer();

		ZooKeeperServerBean result = new ZooKeeperServerBean(zks);

		// add additional test code here
		assertNotNull(result);
		assertEquals("StandaloneServer_port-1", result.getName());
		assertEquals(false, result.isHidden());
		assertEquals("3.4.5--1, built on 10/16/2013 23:44 GMT", result.getVersion());
		assertEquals("Wed Oct 16 20:27:01 CDT 2013", result.getStartTime());
		assertEquals(-1, result.getMaxClientCnxnsPerHost());
		assertEquals(6000, result.getMinSessionTimeout());
		assertEquals(60000, result.getMaxSessionTimeout());
		assertEquals("10.9.16.66:-1", result.getClientPort());
		assertEquals(3000, result.getTickTime());
		assertEquals(0L, result.getOutstandingRequests());
		assertEquals(0L, result.getPacketsReceived());
		assertEquals(0L, result.getPacketsSent());
		assertEquals(0L, result.getAvgRequestLatency());
		assertEquals(0L, result.getMaxRequestLatency());
		assertEquals(0L, result.getMinRequestLatency());
	}

	/**
	 * Run the long getAvgRequestLatency() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:27 PM
	 */
	@Test
	public void testGetAvgRequestLatency_1()
		throws Exception {
		ZooKeeperServerBean fixture = new ZooKeeperServerBean(new ZooKeeperServer());

		long result = fixture.getAvgRequestLatency();

		// add additional test code here
		assertEquals(0L, result);
	}

	/**
	 * Run the String getClientPort() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:27 PM
	 */
	@Test
	public void testGetClientPort_1()
		throws Exception {
		ZooKeeperServerBean fixture = new ZooKeeperServerBean(new ZooKeeperServer());

		String result = fixture.getClientPort();

		// add additional test code here
		assertEquals("10.9.16.66:-1", result);
	}

	/**
	 * Run the String getClientPort() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:27 PM
	 */
	@Test
	public void testGetClientPort_2()
		throws Exception {
		ZooKeeperServerBean fixture = new ZooKeeperServerBean(new ZooKeeperServer());

		String result = fixture.getClientPort();

		// add additional test code here
		assertEquals("10.9.16.66:-1", result);
	}

	/**
	 * Run the int getMaxClientCnxnsPerHost() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:27 PM
	 */
	@Test
	public void testGetMaxClientCnxnsPerHost_1()
		throws Exception {
		ZooKeeperServerBean fixture = new ZooKeeperServerBean(new ZooKeeperServer());

		int result = fixture.getMaxClientCnxnsPerHost();

		// add additional test code here
		assertEquals(-1, result);
	}

	/**
	 * Run the int getMaxClientCnxnsPerHost() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:27 PM
	 */
	@Test
	public void testGetMaxClientCnxnsPerHost_2()
		throws Exception {
		ZooKeeperServerBean fixture = new ZooKeeperServerBean(new ZooKeeperServer());

		int result = fixture.getMaxClientCnxnsPerHost();

		// add additional test code here
		assertEquals(-1, result);
	}

	/**
	 * Run the long getMaxRequestLatency() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:27 PM
	 */
	@Test
	public void testGetMaxRequestLatency_1()
		throws Exception {
		ZooKeeperServerBean fixture = new ZooKeeperServerBean(new ZooKeeperServer());

		long result = fixture.getMaxRequestLatency();

		// add additional test code here
		assertEquals(0L, result);
	}

	/**
	 * Run the int getMaxSessionTimeout() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:27 PM
	 */
	@Test
	public void testGetMaxSessionTimeout_1()
		throws Exception {
		ZooKeeperServerBean fixture = new ZooKeeperServerBean(new ZooKeeperServer());

		int result = fixture.getMaxSessionTimeout();

		// add additional test code here
		assertEquals(60000, result);
	}

	/**
	 * Run the long getMinRequestLatency() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:27 PM
	 */
	@Test
	public void testGetMinRequestLatency_1()
		throws Exception {
		ZooKeeperServerBean fixture = new ZooKeeperServerBean(new ZooKeeperServer());

		long result = fixture.getMinRequestLatency();

		// add additional test code here
		assertEquals(0L, result);
	}

	/**
	 * Run the int getMinSessionTimeout() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:27 PM
	 */
	@Test
	public void testGetMinSessionTimeout_1()
		throws Exception {
		ZooKeeperServerBean fixture = new ZooKeeperServerBean(new ZooKeeperServer());

		int result = fixture.getMinSessionTimeout();

		// add additional test code here
		assertEquals(6000, result);
	}

	/**
	 * Run the String getName() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:27 PM
	 */
	@Test
	public void testGetName_1()
		throws Exception {
		ZooKeeperServerBean fixture = new ZooKeeperServerBean(new ZooKeeperServer());

		String result = fixture.getName();

		// add additional test code here
		assertEquals("StandaloneServer_port-1", result);
	}

	/**
	 * Run the long getNumAliveConnections() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:27 PM
	 */
	@Test
	public void testGetNumAliveConnections_1()
		throws Exception {
		ZooKeeperServerBean fixture = new ZooKeeperServerBean(new ZooKeeperServer());

		long result = fixture.getNumAliveConnections();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.ZooKeeperServer.getNumAliveConnections(ZooKeeperServer.java:773)
		//       at org.apache.zookeeper.server.ZooKeeperServerBean.getNumAliveConnections(ZooKeeperServerBean.java:145)
		assertEquals(0L, result);
	}

	/**
	 * Run the long getOutstandingRequests() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:27 PM
	 */
	@Test
	public void testGetOutstandingRequests_1()
		throws Exception {
		ZooKeeperServerBean fixture = new ZooKeeperServerBean(new ZooKeeperServer());

		long result = fixture.getOutstandingRequests();

		// add additional test code here
		assertEquals(0L, result);
	}

	/**
	 * Run the long getPacketsReceived() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:27 PM
	 */
	@Test
	public void testGetPacketsReceived_1()
		throws Exception {
		ZooKeeperServerBean fixture = new ZooKeeperServerBean(new ZooKeeperServer());

		long result = fixture.getPacketsReceived();

		// add additional test code here
		assertEquals(0L, result);
	}

	/**
	 * Run the long getPacketsSent() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:27 PM
	 */
	@Test
	public void testGetPacketsSent_1()
		throws Exception {
		ZooKeeperServerBean fixture = new ZooKeeperServerBean(new ZooKeeperServer());

		long result = fixture.getPacketsSent();

		// add additional test code here
		assertEquals(0L, result);
	}

	/**
	 * Run the String getStartTime() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:27 PM
	 */
	@Test
	public void testGetStartTime_1()
		throws Exception {
		ZooKeeperServerBean fixture = new ZooKeeperServerBean(new ZooKeeperServer());

		String result = fixture.getStartTime();

		// add additional test code here
		assertEquals("Wed Oct 16 20:27:01 CDT 2013", result);
	}

	/**
	 * Run the int getTickTime() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:27 PM
	 */
	@Test
	public void testGetTickTime_1()
		throws Exception {
		ZooKeeperServerBean fixture = new ZooKeeperServerBean(new ZooKeeperServer());

		int result = fixture.getTickTime();

		// add additional test code here
		assertEquals(3000, result);
	}

	/**
	 * Run the String getVersion() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:27 PM
	 */
	@Test
	public void testGetVersion_1()
		throws Exception {
		ZooKeeperServerBean fixture = new ZooKeeperServerBean(new ZooKeeperServer());

		String result = fixture.getVersion();

		// add additional test code here
		assertEquals("3.4.5--1, built on 10/16/2013 23:44 GMT", result);
	}

	/**
	 * Run the boolean isHidden() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:27 PM
	 */
	@Test
	public void testIsHidden_1()
		throws Exception {
		ZooKeeperServerBean fixture = new ZooKeeperServerBean(new ZooKeeperServer());

		boolean result = fixture.isHidden();

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the void resetLatency() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:27 PM
	 */
	@Test
	public void testResetLatency_1()
		throws Exception {
		ZooKeeperServerBean fixture = new ZooKeeperServerBean(new ZooKeeperServer());

		fixture.resetLatency();

		// add additional test code here
	}

	/**
	 * Run the void resetMaxLatency() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:27 PM
	 */
	@Test
	public void testResetMaxLatency_1()
		throws Exception {
		ZooKeeperServerBean fixture = new ZooKeeperServerBean(new ZooKeeperServer());

		fixture.resetMaxLatency();

		// add additional test code here
	}

	/**
	 * Run the void resetStatistics() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:27 PM
	 */
	@Test
	public void testResetStatistics_1()
		throws Exception {
		ZooKeeperServerBean fixture = new ZooKeeperServerBean(new ZooKeeperServer());

		fixture.resetStatistics();

		// add additional test code here
	}

	/**
	 * Run the void setMaxClientCnxnsPerHost(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:27 PM
	 */
	@Test
	public void testSetMaxClientCnxnsPerHost_1()
		throws Exception {
		ZooKeeperServerBean fixture = new ZooKeeperServerBean(new ZooKeeperServer());
		int max = 1;

		fixture.setMaxClientCnxnsPerHost(max);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.ZooKeeperServerBean.setMaxClientCnxnsPerHost(ZooKeeperServerBean.java:102)
	}

	/**
	 * Run the void setMaxSessionTimeout(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:27 PM
	 */
	@Test
	public void testSetMaxSessionTimeout_1()
		throws Exception {
		ZooKeeperServerBean fixture = new ZooKeeperServerBean(new ZooKeeperServer());
		int max = 1;

		fixture.setMaxSessionTimeout(max);

		// add additional test code here
	}

	/**
	 * Run the void setMinSessionTimeout(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:27 PM
	 */
	@Test
	public void testSetMinSessionTimeout_1()
		throws Exception {
		ZooKeeperServerBean fixture = new ZooKeeperServerBean(new ZooKeeperServer());
		int min = 1;

		fixture.setMinSessionTimeout(min);

		// add additional test code here
	}

	/**
	 * Run the void setTickTime(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:27 PM
	 */
	@Test
	public void testSetTickTime_1()
		throws Exception {
		ZooKeeperServerBean fixture = new ZooKeeperServerBean(new ZooKeeperServer());
		int tickTime = 1;

		fixture.setTickTime(tickTime);

		// add additional test code here
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:27 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:27 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 8:27 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(ZooKeeperServerBeanTest.class);
	}
}