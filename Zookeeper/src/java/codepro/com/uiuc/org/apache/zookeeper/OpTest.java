package com.uiuc.org.apache.zookeeper;

import java.util.LinkedList;
import java.util.List;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.Op;
import org.apache.zookeeper.data.ACL;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>OpTest</code> contains tests for the class <code>{@link Op}</code>.
 *
 * @generatedBy CodePro at 10/16/13 7:23 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class OpTest {
	/**
	 * Run the Op check(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testCheck_1()
		throws Exception {
		String path = "";
		int version = 1;

		Op result = Op.check(path, version);

		// add additional test code here
		assertNotNull(result);
		assertEquals(13, result.getType());
		assertEquals("", result.getPath());
	}

	/**
	 * Run the Op create(String,byte[],List<ACL>,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testCreate_1()
		throws Exception {
		String path = "";
		byte[] data = new byte[] {};
		List<ACL> acl = new LinkedList();
		int flags = 1;

		Op result = Op.create(path, data, acl, flags);

		// add additional test code here
		assertNotNull(result);
		assertEquals(1, result.getType());
		assertEquals("", result.getPath());
	}

	/**
	 * Run the Op create(String,byte[],List<ACL>,CreateMode) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testCreate_2()
		throws Exception {
		String path = "";
		byte[] data = new byte[] {};
		List<ACL> acl = new LinkedList();
		CreateMode createMode = CreateMode.EPHEMERAL;

		Op result = Op.create(path, data, acl, createMode);

		// add additional test code here
		assertNotNull(result);
		assertEquals(1, result.getType());
		assertEquals("", result.getPath());
	}

	/**
	 * Run the Op delete(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testDelete_1()
		throws Exception {
		String path = "";
		int version = 1;

		Op result = Op.delete(path, version);

		// add additional test code here
		assertNotNull(result);
		assertEquals(2, result.getType());
		assertEquals("", result.getPath());
	}

	/**
	 * Run the String getPath() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testGetPath_1()
		throws Exception {
		Op fixture = Op.check("", 0);

		String result = fixture.getPath();

		// add additional test code here
		assertEquals("", result);
	}

	/**
	 * Run the int getType() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testGetType_1()
		throws Exception {
		Op fixture = Op.check("", 0);

		int result = fixture.getType();

		// add additional test code here
		assertEquals(13, result);
	}

	/**
	 * Run the Op setData(String,byte[],int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testSetData_1()
		throws Exception {
		String path = "";
		byte[] data = new byte[] {};
		int version = 1;

		Op result = Op.setData(path, data, version);

		// add additional test code here
		assertNotNull(result);
		assertEquals(5, result.getType());
		assertEquals("", result.getPath());
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(OpTest.class);
	}
}