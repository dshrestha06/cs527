package com.uiuc.org.apache.zookeeper.server.quorum;

import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.concurrent.TimeUnit;
import org.apache.zookeeper.server.quorum.QuorumCnxManager;
import org.apache.zookeeper.server.quorum.QuorumPeer;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>QuorumCnxManagerTest</code> contains tests for the class <code>{@link QuorumCnxManager}</code>.
 *
 * @generatedBy CodePro at 10/16/13 6:57 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class QuorumCnxManagerTest {
	/**
	 * Run the QuorumCnxManager(QuorumPeer) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testQuorumCnxManager_1()
		throws Exception {
		QuorumPeer self = new QuorumPeer();

		QuorumCnxManager result = new QuorumCnxManager(self);

		// add additional test code here
		assertNotNull(result);
		assertEquals(0L, result.getThreadCount());
	}

	/**
	 * Run the QuorumCnxManager(QuorumPeer) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testQuorumCnxManager_2()
		throws Exception {
		QuorumPeer self = new QuorumPeer();

		QuorumCnxManager result = new QuorumCnxManager(self);

		// add additional test code here
		assertNotNull(result);
		assertEquals(0L, result.getThreadCount());
	}

	/**
	 * Run the QuorumCnxManager(QuorumPeer) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testQuorumCnxManager_3()
		throws Exception {
		QuorumPeer self = new QuorumPeer();

		QuorumCnxManager result = new QuorumCnxManager(self);

		// add additional test code here
		assertNotNull(result);
		assertEquals(0L, result.getThreadCount());
	}

	/**
	 * Run the void addToRecvQueue(Message) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testAddToRecvQueue_1()
		throws Exception {
		QuorumCnxManager fixture = new QuorumCnxManager(new QuorumPeer());
		fixture.addToRecvQueue((org.apache.zookeeper.server.quorum.QuorumCnxManager.Message) null);
		org.apache.zookeeper.server.quorum.QuorumCnxManager.Message msg = null;

		fixture.addToRecvQueue(msg);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.util.concurrent.ArrayBlockingQueue.offer(ArrayBlockingQueue.java:222)
		//       at java.util.AbstractQueue.add(AbstractQueue.java:68)
		//       at java.util.concurrent.ArrayBlockingQueue.add(ArrayBlockingQueue.java:209)
		//       at org.apache.zookeeper.server.quorum.QuorumCnxManager.addToRecvQueue(QuorumCnxManager.java:866)
	}

	/**
	 * Run the void addToRecvQueue(Message) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testAddToRecvQueue_2()
		throws Exception {
		QuorumCnxManager fixture = new QuorumCnxManager(new QuorumPeer());
		fixture.addToRecvQueue((org.apache.zookeeper.server.quorum.QuorumCnxManager.Message) null);
		org.apache.zookeeper.server.quorum.QuorumCnxManager.Message msg = null;

		fixture.addToRecvQueue(msg);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.util.concurrent.ArrayBlockingQueue.offer(ArrayBlockingQueue.java:222)
		//       at java.util.AbstractQueue.add(AbstractQueue.java:68)
		//       at java.util.concurrent.ArrayBlockingQueue.add(ArrayBlockingQueue.java:209)
		//       at org.apache.zookeeper.server.quorum.QuorumCnxManager.addToRecvQueue(QuorumCnxManager.java:866)
	}

	/**
	 * Run the void connectAll() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testConnectAll_1()
		throws Exception {
		QuorumCnxManager fixture = new QuorumCnxManager(new QuorumPeer());
		fixture.addToRecvQueue((org.apache.zookeeper.server.quorum.QuorumCnxManager.Message) null);

		fixture.connectAll();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.util.concurrent.ArrayBlockingQueue.offer(ArrayBlockingQueue.java:222)
		//       at java.util.AbstractQueue.add(AbstractQueue.java:68)
		//       at java.util.concurrent.ArrayBlockingQueue.add(ArrayBlockingQueue.java:209)
		//       at org.apache.zookeeper.server.quorum.QuorumCnxManager.addToRecvQueue(QuorumCnxManager.java:866)
	}

	/**
	 * Run the void connectAll() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testConnectAll_2()
		throws Exception {
		QuorumCnxManager fixture = new QuorumCnxManager(new QuorumPeer());
		fixture.addToRecvQueue((org.apache.zookeeper.server.quorum.QuorumCnxManager.Message) null);

		fixture.connectAll();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.util.concurrent.ArrayBlockingQueue.offer(ArrayBlockingQueue.java:222)
		//       at java.util.AbstractQueue.add(AbstractQueue.java:68)
		//       at java.util.concurrent.ArrayBlockingQueue.add(ArrayBlockingQueue.java:209)
		//       at org.apache.zookeeper.server.quorum.QuorumCnxManager.addToRecvQueue(QuorumCnxManager.java:866)
	}

	/**
	 * Run the void connectOne(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testConnectOne_1()
		throws Exception {
		QuorumCnxManager fixture = new QuorumCnxManager(new QuorumPeer());
		fixture.addToRecvQueue((org.apache.zookeeper.server.quorum.QuorumCnxManager.Message) null);
		long sid = 1L;

		fixture.connectOne(sid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.util.concurrent.ArrayBlockingQueue.offer(ArrayBlockingQueue.java:222)
		//       at java.util.AbstractQueue.add(AbstractQueue.java:68)
		//       at java.util.concurrent.ArrayBlockingQueue.add(ArrayBlockingQueue.java:209)
		//       at org.apache.zookeeper.server.quorum.QuorumCnxManager.addToRecvQueue(QuorumCnxManager.java:866)
	}

	/**
	 * Run the void connectOne(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testConnectOne_2()
		throws Exception {
		QuorumCnxManager fixture = new QuorumCnxManager(new QuorumPeer());
		fixture.addToRecvQueue((org.apache.zookeeper.server.quorum.QuorumCnxManager.Message) null);
		long sid = 1L;

		fixture.connectOne(sid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.util.concurrent.ArrayBlockingQueue.offer(ArrayBlockingQueue.java:222)
		//       at java.util.AbstractQueue.add(AbstractQueue.java:68)
		//       at java.util.concurrent.ArrayBlockingQueue.add(ArrayBlockingQueue.java:209)
		//       at org.apache.zookeeper.server.quorum.QuorumCnxManager.addToRecvQueue(QuorumCnxManager.java:866)
	}

	/**
	 * Run the void connectOne(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testConnectOne_3()
		throws Exception {
		QuorumCnxManager fixture = new QuorumCnxManager(new QuorumPeer());
		fixture.addToRecvQueue((org.apache.zookeeper.server.quorum.QuorumCnxManager.Message) null);
		long sid = 1L;

		fixture.connectOne(sid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.util.concurrent.ArrayBlockingQueue.offer(ArrayBlockingQueue.java:222)
		//       at java.util.AbstractQueue.add(AbstractQueue.java:68)
		//       at java.util.concurrent.ArrayBlockingQueue.add(ArrayBlockingQueue.java:209)
		//       at org.apache.zookeeper.server.quorum.QuorumCnxManager.addToRecvQueue(QuorumCnxManager.java:866)
	}

	/**
	 * Run the void connectOne(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testConnectOne_4()
		throws Exception {
		QuorumCnxManager fixture = new QuorumCnxManager(new QuorumPeer());
		fixture.addToRecvQueue((org.apache.zookeeper.server.quorum.QuorumCnxManager.Message) null);
		long sid = 1L;

		fixture.connectOne(sid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.util.concurrent.ArrayBlockingQueue.offer(ArrayBlockingQueue.java:222)
		//       at java.util.AbstractQueue.add(AbstractQueue.java:68)
		//       at java.util.concurrent.ArrayBlockingQueue.add(ArrayBlockingQueue.java:209)
		//       at org.apache.zookeeper.server.quorum.QuorumCnxManager.addToRecvQueue(QuorumCnxManager.java:866)
	}

	/**
	 * Run the void connectOne(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testConnectOne_5()
		throws Exception {
		QuorumCnxManager fixture = new QuorumCnxManager(new QuorumPeer());
		fixture.addToRecvQueue((org.apache.zookeeper.server.quorum.QuorumCnxManager.Message) null);
		long sid = 1L;

		fixture.connectOne(sid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.util.concurrent.ArrayBlockingQueue.offer(ArrayBlockingQueue.java:222)
		//       at java.util.AbstractQueue.add(AbstractQueue.java:68)
		//       at java.util.concurrent.ArrayBlockingQueue.add(ArrayBlockingQueue.java:209)
		//       at org.apache.zookeeper.server.quorum.QuorumCnxManager.addToRecvQueue(QuorumCnxManager.java:866)
	}

	/**
	 * Run the void connectOne(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testConnectOne_6()
		throws Exception {
		QuorumCnxManager fixture = new QuorumCnxManager(new QuorumPeer());
		fixture.addToRecvQueue((org.apache.zookeeper.server.quorum.QuorumCnxManager.Message) null);
		long sid = 1L;

		fixture.connectOne(sid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.util.concurrent.ArrayBlockingQueue.offer(ArrayBlockingQueue.java:222)
		//       at java.util.AbstractQueue.add(AbstractQueue.java:68)
		//       at java.util.concurrent.ArrayBlockingQueue.add(ArrayBlockingQueue.java:209)
		//       at org.apache.zookeeper.server.quorum.QuorumCnxManager.addToRecvQueue(QuorumCnxManager.java:866)
	}

	/**
	 * Run the void connectOne(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testConnectOne_7()
		throws Exception {
		QuorumCnxManager fixture = new QuorumCnxManager(new QuorumPeer());
		fixture.addToRecvQueue((org.apache.zookeeper.server.quorum.QuorumCnxManager.Message) null);
		long sid = 1L;

		fixture.connectOne(sid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.util.concurrent.ArrayBlockingQueue.offer(ArrayBlockingQueue.java:222)
		//       at java.util.AbstractQueue.add(AbstractQueue.java:68)
		//       at java.util.concurrent.ArrayBlockingQueue.add(ArrayBlockingQueue.java:209)
		//       at org.apache.zookeeper.server.quorum.QuorumCnxManager.addToRecvQueue(QuorumCnxManager.java:866)
	}

	/**
	 * Run the void connectOne(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testConnectOne_8()
		throws Exception {
		QuorumCnxManager fixture = new QuorumCnxManager(new QuorumPeer());
		fixture.addToRecvQueue((org.apache.zookeeper.server.quorum.QuorumCnxManager.Message) null);
		long sid = 1L;

		fixture.connectOne(sid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.util.concurrent.ArrayBlockingQueue.offer(ArrayBlockingQueue.java:222)
		//       at java.util.AbstractQueue.add(AbstractQueue.java:68)
		//       at java.util.concurrent.ArrayBlockingQueue.add(ArrayBlockingQueue.java:209)
		//       at org.apache.zookeeper.server.quorum.QuorumCnxManager.addToRecvQueue(QuorumCnxManager.java:866)
	}

	/**
	 * Run the void connectOne(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testConnectOne_9()
		throws Exception {
		QuorumCnxManager fixture = new QuorumCnxManager(new QuorumPeer());
		fixture.addToRecvQueue((org.apache.zookeeper.server.quorum.QuorumCnxManager.Message) null);
		long sid = 1L;

		fixture.connectOne(sid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.util.concurrent.ArrayBlockingQueue.offer(ArrayBlockingQueue.java:222)
		//       at java.util.AbstractQueue.add(AbstractQueue.java:68)
		//       at java.util.concurrent.ArrayBlockingQueue.add(ArrayBlockingQueue.java:209)
		//       at org.apache.zookeeper.server.quorum.QuorumCnxManager.addToRecvQueue(QuorumCnxManager.java:866)
	}

	/**
	 * Run the void connectOne(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testConnectOne_10()
		throws Exception {
		QuorumCnxManager fixture = new QuorumCnxManager(new QuorumPeer());
		fixture.addToRecvQueue((org.apache.zookeeper.server.quorum.QuorumCnxManager.Message) null);
		long sid = 1L;

		fixture.connectOne(sid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.util.concurrent.ArrayBlockingQueue.offer(ArrayBlockingQueue.java:222)
		//       at java.util.AbstractQueue.add(AbstractQueue.java:68)
		//       at java.util.concurrent.ArrayBlockingQueue.add(ArrayBlockingQueue.java:209)
		//       at org.apache.zookeeper.server.quorum.QuorumCnxManager.addToRecvQueue(QuorumCnxManager.java:866)
	}

	/**
	 * Run the QuorumPeer getQuorumPeer() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testGetQuorumPeer_1()
		throws Exception {
		QuorumCnxManager fixture = new QuorumCnxManager(new QuorumPeer());
		fixture.addToRecvQueue((org.apache.zookeeper.server.quorum.QuorumCnxManager.Message) null);

		QuorumPeer result = fixture.getQuorumPeer();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.util.concurrent.ArrayBlockingQueue.offer(ArrayBlockingQueue.java:222)
		//       at java.util.AbstractQueue.add(AbstractQueue.java:68)
		//       at java.util.concurrent.ArrayBlockingQueue.add(ArrayBlockingQueue.java:209)
		//       at org.apache.zookeeper.server.quorum.QuorumCnxManager.addToRecvQueue(QuorumCnxManager.java:866)
		assertNotNull(result);
	}

	/**
	 * Run the long getThreadCount() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testGetThreadCount_1()
		throws Exception {
		QuorumCnxManager fixture = new QuorumCnxManager(new QuorumPeer());
		fixture.addToRecvQueue((org.apache.zookeeper.server.quorum.QuorumCnxManager.Message) null);

		long result = fixture.getThreadCount();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.util.concurrent.ArrayBlockingQueue.offer(ArrayBlockingQueue.java:222)
		//       at java.util.AbstractQueue.add(AbstractQueue.java:68)
		//       at java.util.concurrent.ArrayBlockingQueue.add(ArrayBlockingQueue.java:209)
		//       at org.apache.zookeeper.server.quorum.QuorumCnxManager.addToRecvQueue(QuorumCnxManager.java:866)
		assertEquals(0L, result);
	}

	/**
	 * Run the void halt() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testHalt_1()
		throws Exception {
		QuorumCnxManager fixture = new QuorumCnxManager(new QuorumPeer());
		fixture.addToRecvQueue((org.apache.zookeeper.server.quorum.QuorumCnxManager.Message) null);

		fixture.halt();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.util.concurrent.ArrayBlockingQueue.offer(ArrayBlockingQueue.java:222)
		//       at java.util.AbstractQueue.add(AbstractQueue.java:68)
		//       at java.util.concurrent.ArrayBlockingQueue.add(ArrayBlockingQueue.java:209)
		//       at org.apache.zookeeper.server.quorum.QuorumCnxManager.addToRecvQueue(QuorumCnxManager.java:866)
	}

	/**
	 * Run the boolean haveDelivered() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testHaveDelivered_1()
		throws Exception {
		QuorumCnxManager fixture = new QuorumCnxManager(new QuorumPeer());
		fixture.addToRecvQueue((org.apache.zookeeper.server.quorum.QuorumCnxManager.Message) null);

		boolean result = fixture.haveDelivered();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.util.concurrent.ArrayBlockingQueue.offer(ArrayBlockingQueue.java:222)
		//       at java.util.AbstractQueue.add(AbstractQueue.java:68)
		//       at java.util.concurrent.ArrayBlockingQueue.add(ArrayBlockingQueue.java:209)
		//       at org.apache.zookeeper.server.quorum.QuorumCnxManager.addToRecvQueue(QuorumCnxManager.java:866)
		assertTrue(result);
	}

	/**
	 * Run the boolean haveDelivered() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testHaveDelivered_2()
		throws Exception {
		QuorumCnxManager fixture = new QuorumCnxManager(new QuorumPeer());
		fixture.addToRecvQueue((org.apache.zookeeper.server.quorum.QuorumCnxManager.Message) null);

		boolean result = fixture.haveDelivered();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.util.concurrent.ArrayBlockingQueue.offer(ArrayBlockingQueue.java:222)
		//       at java.util.AbstractQueue.add(AbstractQueue.java:68)
		//       at java.util.concurrent.ArrayBlockingQueue.add(ArrayBlockingQueue.java:209)
		//       at org.apache.zookeeper.server.quorum.QuorumCnxManager.addToRecvQueue(QuorumCnxManager.java:866)
		assertTrue(result);
	}

	/**
	 * Run the boolean haveDelivered() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testHaveDelivered_3()
		throws Exception {
		QuorumCnxManager fixture = new QuorumCnxManager(new QuorumPeer());
		fixture.addToRecvQueue((org.apache.zookeeper.server.quorum.QuorumCnxManager.Message) null);

		boolean result = fixture.haveDelivered();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.util.concurrent.ArrayBlockingQueue.offer(ArrayBlockingQueue.java:222)
		//       at java.util.AbstractQueue.add(AbstractQueue.java:68)
		//       at java.util.concurrent.ArrayBlockingQueue.add(ArrayBlockingQueue.java:209)
		//       at org.apache.zookeeper.server.quorum.QuorumCnxManager.addToRecvQueue(QuorumCnxManager.java:866)
		assertTrue(result);
	}

	/**
	 * Run the boolean initiateConnection(Socket,Long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testInitiateConnection_1()
		throws Exception {
		QuorumCnxManager fixture = new QuorumCnxManager(new QuorumPeer());
		fixture.addToRecvQueue((org.apache.zookeeper.server.quorum.QuorumCnxManager.Message) null);
		Socket sock = new Socket();
		Long sid = new Long(1L);

		boolean result = fixture.initiateConnection(sock, sid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.util.concurrent.ArrayBlockingQueue.offer(ArrayBlockingQueue.java:222)
		//       at java.util.AbstractQueue.add(AbstractQueue.java:68)
		//       at java.util.concurrent.ArrayBlockingQueue.add(ArrayBlockingQueue.java:209)
		//       at org.apache.zookeeper.server.quorum.QuorumCnxManager.addToRecvQueue(QuorumCnxManager.java:866)
		assertTrue(result);
	}

	/**
	 * Run the boolean initiateConnection(Socket,Long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testInitiateConnection_2()
		throws Exception {
		QuorumCnxManager fixture = new QuorumCnxManager(new QuorumPeer());
		fixture.addToRecvQueue((org.apache.zookeeper.server.quorum.QuorumCnxManager.Message) null);
		Socket sock = new Socket();
		Long sid = new Long(1L);

		boolean result = fixture.initiateConnection(sock, sid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.util.concurrent.ArrayBlockingQueue.offer(ArrayBlockingQueue.java:222)
		//       at java.util.AbstractQueue.add(AbstractQueue.java:68)
		//       at java.util.concurrent.ArrayBlockingQueue.add(ArrayBlockingQueue.java:209)
		//       at org.apache.zookeeper.server.quorum.QuorumCnxManager.addToRecvQueue(QuorumCnxManager.java:866)
		assertTrue(result);
	}

	/**
	 * Run the boolean initiateConnection(Socket,Long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testInitiateConnection_3()
		throws Exception {
		QuorumCnxManager fixture = new QuorumCnxManager(new QuorumPeer());
		fixture.addToRecvQueue((org.apache.zookeeper.server.quorum.QuorumCnxManager.Message) null);
		Socket sock = new Socket();
		Long sid = new Long(1L);

		boolean result = fixture.initiateConnection(sock, sid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.util.concurrent.ArrayBlockingQueue.offer(ArrayBlockingQueue.java:222)
		//       at java.util.AbstractQueue.add(AbstractQueue.java:68)
		//       at java.util.concurrent.ArrayBlockingQueue.add(ArrayBlockingQueue.java:209)
		//       at org.apache.zookeeper.server.quorum.QuorumCnxManager.addToRecvQueue(QuorumCnxManager.java:866)
		assertTrue(result);
	}

	/**
	 * Run the boolean initiateConnection(Socket,Long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testInitiateConnection_4()
		throws Exception {
		QuorumCnxManager fixture = new QuorumCnxManager(new QuorumPeer());
		fixture.addToRecvQueue((org.apache.zookeeper.server.quorum.QuorumCnxManager.Message) null);
		Socket sock = new Socket();
		Long sid = Long.valueOf(1L);

		boolean result = fixture.initiateConnection(sock, sid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.util.concurrent.ArrayBlockingQueue.offer(ArrayBlockingQueue.java:222)
		//       at java.util.AbstractQueue.add(AbstractQueue.java:68)
		//       at java.util.concurrent.ArrayBlockingQueue.add(ArrayBlockingQueue.java:209)
		//       at org.apache.zookeeper.server.quorum.QuorumCnxManager.addToRecvQueue(QuorumCnxManager.java:866)
		assertTrue(result);
	}

	/**
	 * Run the boolean initiateConnection(Socket,Long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testInitiateConnection_5()
		throws Exception {
		QuorumCnxManager fixture = new QuorumCnxManager(new QuorumPeer());
		fixture.addToRecvQueue((org.apache.zookeeper.server.quorum.QuorumCnxManager.Message) null);
		Socket sock = new Socket();
		Long sid = Long.valueOf(1L);

		boolean result = fixture.initiateConnection(sock, sid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.util.concurrent.ArrayBlockingQueue.offer(ArrayBlockingQueue.java:222)
		//       at java.util.AbstractQueue.add(AbstractQueue.java:68)
		//       at java.util.concurrent.ArrayBlockingQueue.add(ArrayBlockingQueue.java:209)
		//       at org.apache.zookeeper.server.quorum.QuorumCnxManager.addToRecvQueue(QuorumCnxManager.java:866)
		assertTrue(result);
	}

	/**
	 * Run the boolean initiateConnection(Socket,Long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testInitiateConnection_6()
		throws Exception {
		QuorumCnxManager fixture = new QuorumCnxManager(new QuorumPeer());
		fixture.addToRecvQueue((org.apache.zookeeper.server.quorum.QuorumCnxManager.Message) null);
		Socket sock = new Socket();
		Long sid = Long.valueOf(1L);

		boolean result = fixture.initiateConnection(sock, sid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.util.concurrent.ArrayBlockingQueue.offer(ArrayBlockingQueue.java:222)
		//       at java.util.AbstractQueue.add(AbstractQueue.java:68)
		//       at java.util.concurrent.ArrayBlockingQueue.add(ArrayBlockingQueue.java:209)
		//       at org.apache.zookeeper.server.quorum.QuorumCnxManager.addToRecvQueue(QuorumCnxManager.java:866)
		assertTrue(result);
	}

	/**
	 * Run the org.apache.zookeeper.server.quorum.QuorumCnxManager.Message pollRecvQueue(long,TimeUnit) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testPollRecvQueue_1()
		throws Exception {
		QuorumCnxManager fixture = new QuorumCnxManager(new QuorumPeer());
		fixture.addToRecvQueue((org.apache.zookeeper.server.quorum.QuorumCnxManager.Message) null);
		long timeout = 1L;
		TimeUnit unit = TimeUnit.DAYS;

		org.apache.zookeeper.server.quorum.QuorumCnxManager.Message result = fixture.pollRecvQueue(timeout, unit);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.util.concurrent.ArrayBlockingQueue.offer(ArrayBlockingQueue.java:222)
		//       at java.util.AbstractQueue.add(AbstractQueue.java:68)
		//       at java.util.concurrent.ArrayBlockingQueue.add(ArrayBlockingQueue.java:209)
		//       at org.apache.zookeeper.server.quorum.QuorumCnxManager.addToRecvQueue(QuorumCnxManager.java:866)
		assertNotNull(result);
	}

	/**
	 * Run the org.apache.zookeeper.server.quorum.QuorumCnxManager.Message pollRecvQueue(long,TimeUnit) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testPollRecvQueue_2()
		throws Exception {
		QuorumCnxManager fixture = new QuorumCnxManager(new QuorumPeer());
		fixture.addToRecvQueue((org.apache.zookeeper.server.quorum.QuorumCnxManager.Message) null);
		long timeout = 1L;
		TimeUnit unit = TimeUnit.DAYS;

		org.apache.zookeeper.server.quorum.QuorumCnxManager.Message result = fixture.pollRecvQueue(timeout, unit);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.util.concurrent.ArrayBlockingQueue.offer(ArrayBlockingQueue.java:222)
		//       at java.util.AbstractQueue.add(AbstractQueue.java:68)
		//       at java.util.concurrent.ArrayBlockingQueue.add(ArrayBlockingQueue.java:209)
		//       at org.apache.zookeeper.server.quorum.QuorumCnxManager.addToRecvQueue(QuorumCnxManager.java:866)
		assertNotNull(result);
	}

	/**
	 * Run the void softHalt() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testSoftHalt_1()
		throws Exception {
		QuorumCnxManager fixture = new QuorumCnxManager(new QuorumPeer());
		fixture.addToRecvQueue((org.apache.zookeeper.server.quorum.QuorumCnxManager.Message) null);

		fixture.softHalt();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.util.concurrent.ArrayBlockingQueue.offer(ArrayBlockingQueue.java:222)
		//       at java.util.AbstractQueue.add(AbstractQueue.java:68)
		//       at java.util.concurrent.ArrayBlockingQueue.add(ArrayBlockingQueue.java:209)
		//       at org.apache.zookeeper.server.quorum.QuorumCnxManager.addToRecvQueue(QuorumCnxManager.java:866)
	}

	/**
	 * Run the void softHalt() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testSoftHalt_2()
		throws Exception {
		QuorumCnxManager fixture = new QuorumCnxManager(new QuorumPeer());
		fixture.addToRecvQueue((org.apache.zookeeper.server.quorum.QuorumCnxManager.Message) null);

		fixture.softHalt();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.util.concurrent.ArrayBlockingQueue.offer(ArrayBlockingQueue.java:222)
		//       at java.util.AbstractQueue.add(AbstractQueue.java:68)
		//       at java.util.concurrent.ArrayBlockingQueue.add(ArrayBlockingQueue.java:209)
		//       at org.apache.zookeeper.server.quorum.QuorumCnxManager.addToRecvQueue(QuorumCnxManager.java:866)
	}

	/**
	 * Run the void testInitiateConnection(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test(expected = java.lang.NullPointerException.class)
	public void testTestInitiateConnection_1()
		throws Exception {
		QuorumCnxManager fixture = new QuorumCnxManager(new QuorumPeer());
		fixture.addToRecvQueue((org.apache.zookeeper.server.quorum.QuorumCnxManager.Message) null);
		long sid = 1L;

		fixture.testInitiateConnection(sid);

		// add additional test code here
	}

	/**
	 * Run the void testInitiateConnection(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test(expected = java.lang.NullPointerException.class)
	public void testTestInitiateConnection_2()
		throws Exception {
		QuorumCnxManager fixture = new QuorumCnxManager(new QuorumPeer());
		fixture.addToRecvQueue((org.apache.zookeeper.server.quorum.QuorumCnxManager.Message) null);
		long sid = 1L;

		fixture.testInitiateConnection(sid);

		// add additional test code here
	}

	/**
	 * Run the void testInitiateConnection(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test(expected = java.lang.NullPointerException.class)
	public void testTestInitiateConnection_3()
		throws Exception {
		QuorumCnxManager fixture = new QuorumCnxManager(new QuorumPeer());
		fixture.addToRecvQueue((org.apache.zookeeper.server.quorum.QuorumCnxManager.Message) null);
		long sid = 1L;

		fixture.testInitiateConnection(sid);

		// add additional test code here
	}

	/**
	 * Run the void testInitiateConnection(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test(expected = java.lang.NullPointerException.class)
	public void testTestInitiateConnection_4()
		throws Exception {
		QuorumCnxManager fixture = new QuorumCnxManager(new QuorumPeer());
		fixture.addToRecvQueue((org.apache.zookeeper.server.quorum.QuorumCnxManager.Message) null);
		long sid = 1L;

		fixture.testInitiateConnection(sid);

		// add additional test code here
	}

	/**
	 * Run the void toSend(Long,ByteBuffer) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testToSend_1()
		throws Exception {
		QuorumCnxManager fixture = new QuorumCnxManager(new QuorumPeer());
		fixture.addToRecvQueue((org.apache.zookeeper.server.quorum.QuorumCnxManager.Message) null);
		Long sid = Long.valueOf(1L);
		ByteBuffer b = ByteBuffer.allocate(0);

		fixture.toSend(sid, b);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.util.concurrent.ArrayBlockingQueue.offer(ArrayBlockingQueue.java:222)
		//       at java.util.AbstractQueue.add(AbstractQueue.java:68)
		//       at java.util.concurrent.ArrayBlockingQueue.add(ArrayBlockingQueue.java:209)
		//       at org.apache.zookeeper.server.quorum.QuorumCnxManager.addToRecvQueue(QuorumCnxManager.java:866)
	}

	/**
	 * Run the void toSend(Long,ByteBuffer) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testToSend_2()
		throws Exception {
		QuorumCnxManager fixture = new QuorumCnxManager(new QuorumPeer());
		fixture.addToRecvQueue((org.apache.zookeeper.server.quorum.QuorumCnxManager.Message) null);
		Long sid = Long.valueOf(1L);
		ByteBuffer b = ByteBuffer.allocate(0);

		fixture.toSend(sid, b);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.util.concurrent.ArrayBlockingQueue.offer(ArrayBlockingQueue.java:222)
		//       at java.util.AbstractQueue.add(AbstractQueue.java:68)
		//       at java.util.concurrent.ArrayBlockingQueue.add(ArrayBlockingQueue.java:209)
		//       at org.apache.zookeeper.server.quorum.QuorumCnxManager.addToRecvQueue(QuorumCnxManager.java:866)
	}

	/**
	 * Run the void toSend(Long,ByteBuffer) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testToSend_3()
		throws Exception {
		QuorumCnxManager fixture = new QuorumCnxManager(new QuorumPeer());
		fixture.addToRecvQueue((org.apache.zookeeper.server.quorum.QuorumCnxManager.Message) null);
		Long sid = Long.valueOf(1L);
		ByteBuffer b = ByteBuffer.allocate(0);

		fixture.toSend(sid, b);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.util.concurrent.ArrayBlockingQueue.offer(ArrayBlockingQueue.java:222)
		//       at java.util.AbstractQueue.add(AbstractQueue.java:68)
		//       at java.util.concurrent.ArrayBlockingQueue.add(ArrayBlockingQueue.java:209)
		//       at org.apache.zookeeper.server.quorum.QuorumCnxManager.addToRecvQueue(QuorumCnxManager.java:866)
	}

	/**
	 * Run the void toSend(Long,ByteBuffer) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testToSend_4()
		throws Exception {
		QuorumCnxManager fixture = new QuorumCnxManager(new QuorumPeer());
		fixture.addToRecvQueue((org.apache.zookeeper.server.quorum.QuorumCnxManager.Message) null);
		Long sid = Long.valueOf(1L);
		ByteBuffer b = ByteBuffer.allocate(0);

		fixture.toSend(sid, b);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.util.concurrent.ArrayBlockingQueue.offer(ArrayBlockingQueue.java:222)
		//       at java.util.AbstractQueue.add(AbstractQueue.java:68)
		//       at java.util.concurrent.ArrayBlockingQueue.add(ArrayBlockingQueue.java:209)
		//       at org.apache.zookeeper.server.quorum.QuorumCnxManager.addToRecvQueue(QuorumCnxManager.java:866)
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(QuorumCnxManagerTest.class);
	}
}