package com.uiuc.org.apache.zookeeper.server.quorum;

import org.apache.zookeeper.server.quorum.LocalPeerBean;
import org.apache.zookeeper.server.quorum.QuorumPeer;
import org.apache.zookeeper.server.quorum.ServerBean;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>ServerBeanTest</code> contains tests for the class <code>{@link ServerBean}</code>.
 *
 * @generatedBy CodePro at 10/16/13 8:17 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class ServerBeanTest {
	/**
	 * Run the String getStartTime() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@Test
	public void testGetStartTime_1()
		throws Exception {
		ServerBean fixture = new LocalPeerBean(new QuorumPeer());

		String result = fixture.getStartTime();

		// add additional test code here
		assertEquals("Wed Oct 16 20:17:01 CDT 2013", result);
	}

	/**
	 * Run the boolean isHidden() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@Test
	public void testIsHidden_1()
		throws Exception {
		ServerBean fixture = new LocalPeerBean(new QuorumPeer());

		boolean result = fixture.isHidden();

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(ServerBeanTest.class);
	}
}