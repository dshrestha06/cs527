package com.uiuc.org.apache.zookeeper.common;

import java.io.File;
import org.apache.zookeeper.common.AtomicFileOutputStream;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>AtomicFileOutputStreamTest</code> contains tests for the class <code>{@link AtomicFileOutputStream}</code>.
 *
 * @generatedBy CodePro at 10/16/13 7:32 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class AtomicFileOutputStreamTest {
	/**
	 * Run the AtomicFileOutputStream(File) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test
	public void testAtomicFileOutputStream_1()
		throws Exception {
		File f = new File("");

		AtomicFileOutputStream result = new AtomicFileOutputStream(f);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:183)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:145)
		//       at org.apache.zookeeper.common.AtomicFileOutputStream.<init>(AtomicFileOutputStream.java:60)
		assertNotNull(result);
	}

	/**
	 * Run the AtomicFileOutputStream(File) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test
	public void testAtomicFileOutputStream_2()
		throws Exception {
		File f = new File("");

		AtomicFileOutputStream result = new AtomicFileOutputStream(f);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:183)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:145)
		//       at org.apache.zookeeper.common.AtomicFileOutputStream.<init>(AtomicFileOutputStream.java:60)
		assertNotNull(result);
	}

	/**
	 * Run the void abort() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test
	public void testAbort_1()
		throws Exception {
		AtomicFileOutputStream fixture = new AtomicFileOutputStream(new File(""));

		fixture.abort();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:183)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:145)
		//       at org.apache.zookeeper.common.AtomicFileOutputStream.<init>(AtomicFileOutputStream.java:60)
	}

	/**
	 * Run the void abort() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test
	public void testAbort_2()
		throws Exception {
		AtomicFileOutputStream fixture = new AtomicFileOutputStream(new File(""));

		fixture.abort();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:183)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:145)
		//       at org.apache.zookeeper.common.AtomicFileOutputStream.<init>(AtomicFileOutputStream.java:60)
	}

	/**
	 * Run the void abort() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test
	public void testAbort_3()
		throws Exception {
		AtomicFileOutputStream fixture = new AtomicFileOutputStream(new File(""));

		fixture.abort();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:183)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:145)
		//       at org.apache.zookeeper.common.AtomicFileOutputStream.<init>(AtomicFileOutputStream.java:60)
	}

	/**
	 * Run the void abort() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test
	public void testAbort_4()
		throws Exception {
		AtomicFileOutputStream fixture = new AtomicFileOutputStream(new File(""));

		fixture.abort();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:183)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:145)
		//       at org.apache.zookeeper.common.AtomicFileOutputStream.<init>(AtomicFileOutputStream.java:60)
	}

	/**
	 * Run the void close() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test
	public void testClose_1()
		throws Exception {
		AtomicFileOutputStream fixture = new AtomicFileOutputStream(new File(""));

		fixture.close();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:183)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:145)
		//       at org.apache.zookeeper.common.AtomicFileOutputStream.<init>(AtomicFileOutputStream.java:60)
	}

	/**
	 * Run the void close() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test
	public void testClose_2()
		throws Exception {
		AtomicFileOutputStream fixture = new AtomicFileOutputStream(new File(""));

		fixture.close();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:183)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:145)
		//       at org.apache.zookeeper.common.AtomicFileOutputStream.<init>(AtomicFileOutputStream.java:60)
	}

	/**
	 * Run the void close() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test
	public void testClose_3()
		throws Exception {
		AtomicFileOutputStream fixture = new AtomicFileOutputStream(new File(""));

		fixture.close();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:183)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:145)
		//       at org.apache.zookeeper.common.AtomicFileOutputStream.<init>(AtomicFileOutputStream.java:60)
	}

	/**
	 * Run the void close() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test
	public void testClose_4()
		throws Exception {
		AtomicFileOutputStream fixture = new AtomicFileOutputStream(new File(""));

		fixture.close();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:183)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:145)
		//       at org.apache.zookeeper.common.AtomicFileOutputStream.<init>(AtomicFileOutputStream.java:60)
	}

	/**
	 * Run the void close() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test
	public void testClose_5()
		throws Exception {
		AtomicFileOutputStream fixture = new AtomicFileOutputStream(new File(""));

		fixture.close();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:183)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:145)
		//       at org.apache.zookeeper.common.AtomicFileOutputStream.<init>(AtomicFileOutputStream.java:60)
	}

	/**
	 * Run the void close() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test
	public void testClose_6()
		throws Exception {
		AtomicFileOutputStream fixture = new AtomicFileOutputStream(new File(""));

		fixture.close();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:183)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:145)
		//       at org.apache.zookeeper.common.AtomicFileOutputStream.<init>(AtomicFileOutputStream.java:60)
	}

	/**
	 * Run the void close() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test
	public void testClose_7()
		throws Exception {
		AtomicFileOutputStream fixture = new AtomicFileOutputStream(new File(""));

		fixture.close();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:183)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:145)
		//       at org.apache.zookeeper.common.AtomicFileOutputStream.<init>(AtomicFileOutputStream.java:60)
	}

	/**
	 * Run the void close() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test
	public void testClose_8()
		throws Exception {
		AtomicFileOutputStream fixture = new AtomicFileOutputStream(new File(""));

		fixture.close();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:183)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:145)
		//       at org.apache.zookeeper.common.AtomicFileOutputStream.<init>(AtomicFileOutputStream.java:60)
	}

	/**
	 * Run the void close() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test
	public void testClose_9()
		throws Exception {
		AtomicFileOutputStream fixture = new AtomicFileOutputStream(new File(""));

		fixture.close();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:183)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:145)
		//       at org.apache.zookeeper.common.AtomicFileOutputStream.<init>(AtomicFileOutputStream.java:60)
	}

	/**
	 * Run the void close() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test
	public void testClose_10()
		throws Exception {
		AtomicFileOutputStream fixture = new AtomicFileOutputStream(new File(""));

		fixture.close();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:183)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:145)
		//       at org.apache.zookeeper.common.AtomicFileOutputStream.<init>(AtomicFileOutputStream.java:60)
	}

	/**
	 * Run the void close() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test
	public void testClose_11()
		throws Exception {
		AtomicFileOutputStream fixture = new AtomicFileOutputStream(new File(""));

		fixture.close();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:183)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:145)
		//       at org.apache.zookeeper.common.AtomicFileOutputStream.<init>(AtomicFileOutputStream.java:60)
	}

	/**
	 * Run the void close() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test
	public void testClose_12()
		throws Exception {
		AtomicFileOutputStream fixture = new AtomicFileOutputStream(new File(""));

		fixture.close();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:183)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:145)
		//       at org.apache.zookeeper.common.AtomicFileOutputStream.<init>(AtomicFileOutputStream.java:60)
	}

	/**
	 * Run the void close() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test
	public void testClose_13()
		throws Exception {
		AtomicFileOutputStream fixture = new AtomicFileOutputStream(new File(""));

		fixture.close();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:183)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:145)
		//       at org.apache.zookeeper.common.AtomicFileOutputStream.<init>(AtomicFileOutputStream.java:60)
	}

	/**
	 * Run the void close() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test
	public void testClose_14()
		throws Exception {
		AtomicFileOutputStream fixture = new AtomicFileOutputStream(new File(""));

		fixture.close();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:183)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:145)
		//       at org.apache.zookeeper.common.AtomicFileOutputStream.<init>(AtomicFileOutputStream.java:60)
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(AtomicFileOutputStreamTest.class);
	}
}