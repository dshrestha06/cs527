package com.uiuc.org.apache.zookeeper.server.util;

import java.lang.reflect.InvocationTargetException;
import org.apache.zookeeper.server.util.KerberosUtil;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>KerberosUtilTest</code> contains tests for the class <code>{@link KerberosUtil}</code>.
 *
 * @generatedBy CodePro at 10/16/13 7:41 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class KerberosUtilTest {
	/**
	 * Run the String getDefaultRealm() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test(expected = java.lang.reflect.InvocationTargetException.class)
	public void testGetDefaultRealm_1()
		throws Exception {

		String result = KerberosUtil.getDefaultRealm();

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the String getDefaultRealm() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test(expected = java.lang.reflect.InvocationTargetException.class)
	public void testGetDefaultRealm_2()
		throws Exception {

		String result = KerberosUtil.getDefaultRealm();

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the String getDefaultRealm() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test(expected = java.lang.reflect.InvocationTargetException.class)
	public void testGetDefaultRealm_3()
		throws Exception {

		String result = KerberosUtil.getDefaultRealm();

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the String getDefaultRealm() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test(expected = java.lang.reflect.InvocationTargetException.class)
	public void testGetDefaultRealm_4()
		throws Exception {

		String result = KerberosUtil.getDefaultRealm();

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the String getDefaultRealm() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test(expected = java.lang.reflect.InvocationTargetException.class)
	public void testGetDefaultRealm_5()
		throws Exception {

		String result = KerberosUtil.getDefaultRealm();

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the String getDefaultRealm() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test(expected = java.lang.reflect.InvocationTargetException.class)
	public void testGetDefaultRealm_6()
		throws Exception {

		String result = KerberosUtil.getDefaultRealm();

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the String getDefaultRealm() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test(expected = java.lang.reflect.InvocationTargetException.class)
	public void testGetDefaultRealm_7()
		throws Exception {

		String result = KerberosUtil.getDefaultRealm();

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the String getDefaultRealm() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test(expected = java.lang.reflect.InvocationTargetException.class)
	public void testGetDefaultRealm_8()
		throws Exception {

		String result = KerberosUtil.getDefaultRealm();

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the String getDefaultRealm() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test(expected = java.lang.reflect.InvocationTargetException.class)
	public void testGetDefaultRealm_9()
		throws Exception {

		String result = KerberosUtil.getDefaultRealm();

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the String getDefaultRealm() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test(expected = java.lang.reflect.InvocationTargetException.class)
	public void testGetDefaultRealm_10()
		throws Exception {

		String result = KerberosUtil.getDefaultRealm();

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the String getDefaultRealm() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test(expected = java.lang.reflect.InvocationTargetException.class)
	public void testGetDefaultRealm_11()
		throws Exception {

		String result = KerberosUtil.getDefaultRealm();

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the String getDefaultRealm() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test(expected = java.lang.reflect.InvocationTargetException.class)
	public void testGetDefaultRealm_12()
		throws Exception {

		String result = KerberosUtil.getDefaultRealm();

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the String getDefaultRealm() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test(expected = java.lang.reflect.InvocationTargetException.class)
	public void testGetDefaultRealm_13()
		throws Exception {

		String result = KerberosUtil.getDefaultRealm();

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(KerberosUtilTest.class);
	}
}