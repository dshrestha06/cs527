package com.uiuc.org.apache.zookeeper.server.quorum;

import org.apache.zookeeper.server.quorum.LeaderElectionBean;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>LeaderElectionBeanTest</code> contains tests for the class <code>{@link LeaderElectionBean}</code>.
 *
 * @generatedBy CodePro at 10/16/13 7:58 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class LeaderElectionBeanTest {
	/**
	 * Run the LeaderElectionBean() constructor test.
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testLeaderElectionBean_1()
		throws Exception {
		LeaderElectionBean result = new LeaderElectionBean();
		assertNotNull(result);
		// add additional test code here
	}

	/**
	 * Run the String getName() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testGetName_1()
		throws Exception {
		LeaderElectionBean fixture = new LeaderElectionBean();

		String result = fixture.getName();

		// add additional test code here
		assertEquals("LeaderElection", result);
	}

	/**
	 * Run the String getStartTime() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testGetStartTime_1()
		throws Exception {
		LeaderElectionBean fixture = new LeaderElectionBean();

		String result = fixture.getStartTime();

		// add additional test code here
		assertEquals("Wed Oct 16 19:58:07 CDT 2013", result);
	}

	/**
	 * Run the boolean isHidden() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testIsHidden_1()
		throws Exception {
		LeaderElectionBean fixture = new LeaderElectionBean();

		boolean result = fixture.isHidden();

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(LeaderElectionBeanTest.class);
	}
}