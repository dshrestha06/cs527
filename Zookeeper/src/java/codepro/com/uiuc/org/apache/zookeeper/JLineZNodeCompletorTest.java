package com.uiuc.org.apache.zookeeper;

import java.util.LinkedList;
import java.util.List;
import org.apache.zookeeper.JLineZNodeCompletor;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.server.CRCTest;
import org.apache.zookeeper.test.system.SimpleClient;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>JLineZNodeCompletorTest</code> contains tests for the class <code>{@link JLineZNodeCompletor}</code>.
 *
 * @generatedBy CodePro at 10/16/13 6:57 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class JLineZNodeCompletorTest {
	/**
	 * Run the JLineZNodeCompletor(ZooKeeper) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testJLineZNodeCompletor_1()
		throws Exception {
		ZooKeeper zk = new ZooKeeper("", 1, new SimpleClient());

		JLineZNodeCompletor result = new JLineZNodeCompletor(zk);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the int complete(String,int,List) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testComplete_1()
		throws Exception {
		JLineZNodeCompletor fixture = new JLineZNodeCompletor(new ZooKeeper("", 0, new CRCTest()));
		String buffer = "";
		int cursor = 1;
		List candidates = new LinkedList();

		int result = fixture.complete(buffer, cursor, candidates);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.StringIndexOutOfBoundsException: String index out of range: 1
		//       at java.lang.String.substring(String.java:1934)
		//       at org.apache.zookeeper.JLineZNodeCompletor.complete(JLineZNodeCompletor.java:35)
		assertEquals(0, result);
	}

	/**
	 * Run the int complete(String,int,List) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testComplete_2()
		throws Exception {
		JLineZNodeCompletor fixture = new JLineZNodeCompletor(new ZooKeeper("", 0, new CRCTest()));
		String buffer = "";
		int cursor = 1;
		List candidates = new LinkedList();

		int result = fixture.complete(buffer, cursor, candidates);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.StringIndexOutOfBoundsException: String index out of range: 1
		//       at java.lang.String.substring(String.java:1934)
		//       at org.apache.zookeeper.JLineZNodeCompletor.complete(JLineZNodeCompletor.java:35)
		assertEquals(0, result);
	}

	/**
	 * Run the int complete(String,int,List) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testComplete_3()
		throws Exception {
		JLineZNodeCompletor fixture = new JLineZNodeCompletor(new ZooKeeper("", 0, new CRCTest()));
		String buffer = "";
		int cursor = 1;
		List candidates = new LinkedList();

		int result = fixture.complete(buffer, cursor, candidates);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.StringIndexOutOfBoundsException: String index out of range: 1
		//       at java.lang.String.substring(String.java:1934)
		//       at org.apache.zookeeper.JLineZNodeCompletor.complete(JLineZNodeCompletor.java:35)
		assertEquals(0, result);
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(JLineZNodeCompletorTest.class);
	}
}