package com.uiuc.org.apache.zookeeper.server.quorum;

import org.apache.zookeeper.server.quorum.QuorumPeer;
import org.apache.zookeeper.server.quorum.Vote;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>VoteTest</code> contains tests for the class <code>{@link Vote}</code>.
 *
 * @generatedBy CodePro at 10/16/13 8:17 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class VoteTest {
	/**
	 * Run the Vote(long,long) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@Test
	public void testVote_1()
		throws Exception {
		long id = 1L;
		long zxid = 1L;

		Vote result = new Vote(id, zxid);

		// add additional test code here
		assertNotNull(result);
		assertEquals("(1, 1, ffffffffffffffff)", result.toString());
		assertEquals(1L, result.getId());
		assertEquals(1L, result.getZxid());
		assertEquals(-1L, result.getElectionEpoch());
		assertEquals(-1L, result.getPeerEpoch());
	}

	/**
	 * Run the Vote(long,long,long) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@Test
	public void testVote_2()
		throws Exception {
		long id = 1L;
		long zxid = 1L;
		long peerEpoch = 1L;

		Vote result = new Vote(id, zxid, peerEpoch);

		// add additional test code here
		assertNotNull(result);
		assertEquals("(1, 1, 1)", result.toString());
		assertEquals(1L, result.getId());
		assertEquals(1L, result.getZxid());
		assertEquals(-1L, result.getElectionEpoch());
		assertEquals(1L, result.getPeerEpoch());
	}

	/**
	 * Run the Vote(long,long,long,long) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@Test
	public void testVote_3()
		throws Exception {
		long id = 1L;
		long zxid = 1L;
		long electionEpoch = 1L;
		long peerEpoch = 1L;

		Vote result = new Vote(id, zxid, electionEpoch, peerEpoch);

		// add additional test code here
		assertNotNull(result);
		assertEquals("(1, 1, 1)", result.toString());
		assertEquals(1L, result.getId());
		assertEquals(1L, result.getZxid());
		assertEquals(1L, result.getElectionEpoch());
		assertEquals(1L, result.getPeerEpoch());
	}

	/**
	 * Run the Vote(long,long,long,long,ServerState) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@Test
	public void testVote_4()
		throws Exception {
		long id = 1L;
		long zxid = 1L;
		long electionEpoch = 1L;
		long peerEpoch = 1L;
		org.apache.zookeeper.server.quorum.QuorumPeer.ServerState state = org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING;

		Vote result = new Vote(id, zxid, electionEpoch, peerEpoch, state);

		// add additional test code here
		assertNotNull(result);
		assertEquals("(1, 1, 1)", result.toString());
		assertEquals(1L, result.getId());
		assertEquals(1L, result.getZxid());
		assertEquals(1L, result.getElectionEpoch());
		assertEquals(1L, result.getPeerEpoch());
	}

	/**
	 * Run the boolean equals(Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@Test
	public void testEquals_1()
		throws Exception {
		Vote fixture = new Vote(1L, 1L, 1L, 1L, org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		Object o = new Object();

		boolean result = fixture.equals(o);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean equals(Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@Test
	public void testEquals_2()
		throws Exception {
		Vote fixture = new Vote(1L, 1L, 1L, 1L, org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		Object o = new Vote(1L, 1L, 1L, 1L);

		boolean result = fixture.equals(o);

		// add additional test code here
		assertEquals(true, result);
	}

	/**
	 * Run the boolean equals(Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@Test
	public void testEquals_3()
		throws Exception {
		Vote fixture = new Vote(1L, 1L, 1L, 1L, org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		Object o = new Vote(1L, 1L);

		boolean result = fixture.equals(o);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean equals(Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@Test
	public void testEquals_4()
		throws Exception {
		Vote fixture = new Vote(1L, 1L, 1L, 1L, org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		Object o = new Vote(1L, 1L);

		boolean result = fixture.equals(o);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean equals(Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@Test
	public void testEquals_5()
		throws Exception {
		Vote fixture = new Vote(1L, 1L, 1L, 1L, org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		Object o = new Vote(1L, 1L, 1L, 1L);

		boolean result = fixture.equals(o);

		// add additional test code here
		assertEquals(true, result);
	}

	/**
	 * Run the boolean equals(Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@Test
	public void testEquals_6()
		throws Exception {
		Vote fixture = new Vote(1L, 1L, 1L, 1L, org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		Object o = new Vote(1L, 1L, 1L, 1L);

		boolean result = fixture.equals(o);

		// add additional test code here
		assertEquals(true, result);
	}

	/**
	 * Run the long getElectionEpoch() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@Test
	public void testGetElectionEpoch_1()
		throws Exception {
		Vote fixture = new Vote(1L, 1L, 1L, 1L, org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);

		long result = fixture.getElectionEpoch();

		// add additional test code here
		assertEquals(1L, result);
	}

	/**
	 * Run the long getId() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@Test
	public void testGetId_1()
		throws Exception {
		Vote fixture = new Vote(1L, 1L, 1L, 1L, org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);

		long result = fixture.getId();

		// add additional test code here
		assertEquals(1L, result);
	}

	/**
	 * Run the long getPeerEpoch() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@Test
	public void testGetPeerEpoch_1()
		throws Exception {
		Vote fixture = new Vote(1L, 1L, 1L, 1L, org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);

		long result = fixture.getPeerEpoch();

		// add additional test code here
		assertEquals(1L, result);
	}

	/**
	 * Run the org.apache.zookeeper.server.quorum.QuorumPeer.ServerState getState() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@Test
	public void testGetState_1()
		throws Exception {
		Vote fixture = new Vote(1L, 1L, 1L, 1L, org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);

		org.apache.zookeeper.server.quorum.QuorumPeer.ServerState result = fixture.getState();

		// add additional test code here
		assertNotNull(result);
		assertEquals("FOLLOWING", result.name());
		assertEquals("FOLLOWING", result.toString());
		assertEquals(1, result.ordinal());
	}

	/**
	 * Run the long getZxid() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@Test
	public void testGetZxid_1()
		throws Exception {
		Vote fixture = new Vote(1L, 1L, 1L, 1L, org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);

		long result = fixture.getZxid();

		// add additional test code here
		assertEquals(1L, result);
	}

	/**
	 * Run the int hashCode() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@Test
	public void testHashCode_1()
		throws Exception {
		Vote fixture = new Vote(1L, 1L, 1L, 1L, org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);

		int result = fixture.hashCode();

		// add additional test code here
		assertEquals(1, result);
	}

	/**
	 * Run the String toString() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@Test
	public void testToString_1()
		throws Exception {
		Vote fixture = new Vote(1L, 1L, 1L, 1L, org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);

		String result = fixture.toString();

		// add additional test code here
		assertEquals("(1, 1, 1)", result);
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(VoteTest.class);
	}
}