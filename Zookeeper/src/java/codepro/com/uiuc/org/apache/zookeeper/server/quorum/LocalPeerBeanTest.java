package com.uiuc.org.apache.zookeeper.server.quorum;

import org.apache.zookeeper.server.quorum.LocalPeerBean;
import org.apache.zookeeper.server.quorum.QuorumPeer;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>LocalPeerBeanTest</code> contains tests for the class <code>{@link LocalPeerBean}</code>.
 *
 * @generatedBy CodePro at 10/16/13 8:00 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class LocalPeerBeanTest {
	/**
	 * Run the LocalPeerBean(QuorumPeer) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testLocalPeerBean_1()
		throws Exception {
		QuorumPeer peer = new QuorumPeer();

		LocalPeerBean result = new LocalPeerBean(peer);

		// add additional test code here
		assertNotNull(result);
		assertEquals("replica.0", result.getName());
		assertEquals("NEW", result.getState());
		assertEquals(false, result.isHidden());
		assertEquals(-1, result.getMaxClientCnxnsPerHost());
		assertEquals(0, result.getMinSessionTimeout());
		assertEquals(0, result.getMaxSessionTimeout());
		assertEquals(0, result.getTickTime());
		assertEquals(0, result.getInitLimit());
		assertEquals(0, result.getSyncLimit());
		assertEquals(0, result.getTick());
		assertEquals(0, result.getElectionType());
		assertEquals("Wed Oct 16 20:00:12 CDT 2013", result.getStartTime());
	}

	/**
	 * Run the int getElectionType() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testGetElectionType_1()
		throws Exception {
		LocalPeerBean fixture = new LocalPeerBean(new QuorumPeer());

		int result = fixture.getElectionType();

		// add additional test code here
		assertEquals(0, result);
	}

	/**
	 * Run the int getInitLimit() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testGetInitLimit_1()
		throws Exception {
		LocalPeerBean fixture = new LocalPeerBean(new QuorumPeer());

		int result = fixture.getInitLimit();

		// add additional test code here
		assertEquals(0, result);
	}

	/**
	 * Run the int getMaxClientCnxnsPerHost() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testGetMaxClientCnxnsPerHost_1()
		throws Exception {
		LocalPeerBean fixture = new LocalPeerBean(new QuorumPeer());

		int result = fixture.getMaxClientCnxnsPerHost();

		// add additional test code here
		assertEquals(-1, result);
	}

	/**
	 * Run the int getMaxSessionTimeout() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testGetMaxSessionTimeout_1()
		throws Exception {
		LocalPeerBean fixture = new LocalPeerBean(new QuorumPeer());

		int result = fixture.getMaxSessionTimeout();

		// add additional test code here
		assertEquals(0, result);
	}

	/**
	 * Run the int getMinSessionTimeout() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testGetMinSessionTimeout_1()
		throws Exception {
		LocalPeerBean fixture = new LocalPeerBean(new QuorumPeer());

		int result = fixture.getMinSessionTimeout();

		// add additional test code here
		assertEquals(0, result);
	}

	/**
	 * Run the String getName() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testGetName_1()
		throws Exception {
		LocalPeerBean fixture = new LocalPeerBean(new QuorumPeer());

		String result = fixture.getName();

		// add additional test code here
		assertEquals("replica.0", result);
	}

	/**
	 * Run the String getQuorumAddress() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testGetQuorumAddress_1()
		throws Exception {
		LocalPeerBean fixture = new LocalPeerBean(new QuorumPeer());

		String result = fixture.getQuorumAddress();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.LocalPeerBean.getQuorumAddress(LocalPeerBean.java:73)
		assertNotNull(result);
	}

	/**
	 * Run the String getState() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testGetState_1()
		throws Exception {
		LocalPeerBean fixture = new LocalPeerBean(new QuorumPeer());

		String result = fixture.getState();

		// add additional test code here
		assertEquals("NEW", result);
	}

	/**
	 * Run the int getSyncLimit() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testGetSyncLimit_1()
		throws Exception {
		LocalPeerBean fixture = new LocalPeerBean(new QuorumPeer());

		int result = fixture.getSyncLimit();

		// add additional test code here
		assertEquals(0, result);
	}

	/**
	 * Run the int getTick() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testGetTick_1()
		throws Exception {
		LocalPeerBean fixture = new LocalPeerBean(new QuorumPeer());

		int result = fixture.getTick();

		// add additional test code here
		assertEquals(0, result);
	}

	/**
	 * Run the int getTickTime() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testGetTickTime_1()
		throws Exception {
		LocalPeerBean fixture = new LocalPeerBean(new QuorumPeer());

		int result = fixture.getTickTime();

		// add additional test code here
		assertEquals(0, result);
	}

	/**
	 * Run the boolean isHidden() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testIsHidden_1()
		throws Exception {
		LocalPeerBean fixture = new LocalPeerBean(new QuorumPeer());

		boolean result = fixture.isHidden();

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(LocalPeerBeanTest.class);
	}
}