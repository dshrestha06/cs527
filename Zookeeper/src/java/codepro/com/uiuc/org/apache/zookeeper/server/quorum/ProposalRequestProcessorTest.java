package com.uiuc.org.apache.zookeeper.server.quorum;

import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.LinkedList;
import java.util.List;
import org.apache.zookeeper.data.Id;
import org.apache.zookeeper.server.FinalRequestProcessor;
import org.apache.zookeeper.server.NIOServerCnxn;
import org.apache.zookeeper.server.NIOServerCnxnFactory;
import org.apache.zookeeper.server.Request;
import org.apache.zookeeper.server.RequestProcessor;
import org.apache.zookeeper.server.ServerCnxn;
import org.apache.zookeeper.server.ZooKeeperServer;
import org.apache.zookeeper.server.quorum.LeaderZooKeeperServer;
import org.apache.zookeeper.server.quorum.LearnerHandler;
import org.apache.zookeeper.server.quorum.LearnerSyncRequest;
import org.apache.zookeeper.server.quorum.ProposalRequestProcessor;
import org.apache.zookeeper.txn.TxnHeader;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>ProposalRequestProcessorTest</code> contains tests for the class <code>{@link ProposalRequestProcessor}</code>.
 *
 * @generatedBy CodePro at 10/16/13 7:59 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class ProposalRequestProcessorTest {
	/**
	 * Run the ProposalRequestProcessor(LeaderZooKeeperServer,RequestProcessor) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@Test
	public void testProposalRequestProcessor_1()
		throws Exception {
		LeaderZooKeeperServer zks = null;
		RequestProcessor nextProcessor = new FinalRequestProcessor(new ZooKeeperServer());

		ProposalRequestProcessor result = new ProposalRequestProcessor(zks, nextProcessor);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.ProposalRequestProcessor.<init>(ProposalRequestProcessor.java:46)
		assertNotNull(result);
	}

	/**
	 * Run the void initialize() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@Test
	public void testInitialize_1()
		throws Exception {
		ProposalRequestProcessor fixture = new ProposalRequestProcessor((LeaderZooKeeperServer) null, new FinalRequestProcessor(new ZooKeeperServer()));

		fixture.initialize();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.ProposalRequestProcessor.<init>(ProposalRequestProcessor.java:46)
	}

	/**
	 * Run the void processRequest(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@Test
	public void testProcessRequest_1()
		throws Exception {
		ProposalRequestProcessor fixture = new ProposalRequestProcessor((LeaderZooKeeperServer) null, new FinalRequestProcessor(new ZooKeeperServer()));
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());

		fixture.processRequest(request);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.ProposalRequestProcessor.<init>(ProposalRequestProcessor.java:46)
	}

	/**
	 * Run the void processRequest(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@Test
	public void testProcessRequest_2()
		throws Exception {
		ProposalRequestProcessor fixture = new ProposalRequestProcessor((LeaderZooKeeperServer) null, new FinalRequestProcessor(new ZooKeeperServer()));
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());
		request.hdr = new TxnHeader();

		fixture.processRequest(request);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void processRequest(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@Test
	public void testProcessRequest_3()
		throws Exception {
		ProposalRequestProcessor fixture = new ProposalRequestProcessor((LeaderZooKeeperServer) null, new FinalRequestProcessor(new ZooKeeperServer()));
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());
		request.hdr = new TxnHeader();

		fixture.processRequest(request);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void processRequest(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@Test
	public void testProcessRequest_4()
		throws Exception {
		ProposalRequestProcessor fixture = new ProposalRequestProcessor((LeaderZooKeeperServer) null, new FinalRequestProcessor(new ZooKeeperServer()));
		Request request = new LearnerSyncRequest((LearnerHandler) null, 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());

		fixture.processRequest(request);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.ProposalRequestProcessor.<init>(ProposalRequestProcessor.java:46)
	}

	/**
	 * Run the void shutdown() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@Test
	public void testShutdown_1()
		throws Exception {
		ProposalRequestProcessor fixture = new ProposalRequestProcessor((LeaderZooKeeperServer) null, new FinalRequestProcessor(new ZooKeeperServer()));

		fixture.shutdown();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.ProposalRequestProcessor.<init>(ProposalRequestProcessor.java:46)
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(ProposalRequestProcessorTest.class);
	}
}