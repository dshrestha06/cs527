package com.uiuc.org.apache.zookeeper.server;

import java.io.CharArrayWriter;
import java.io.File;
import java.io.PrintWriter;
import java.io.Writer;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.LinkedList;
import java.util.List;
import org.apache.jute.Record;
import org.apache.zookeeper.data.ACL;
import org.apache.zookeeper.data.Id;
import org.apache.zookeeper.proto.RequestHeader;
import org.apache.zookeeper.server.DataTree;
import org.apache.zookeeper.server.NIOServerCnxn;
import org.apache.zookeeper.server.NIOServerCnxnFactory;
import org.apache.zookeeper.server.Request;
import org.apache.zookeeper.server.ServerCnxn;
import org.apache.zookeeper.server.ServerCnxnFactory;
import org.apache.zookeeper.server.ServerStats;
import org.apache.zookeeper.server.SessionTracker;
import org.apache.zookeeper.server.ZKDatabase;
import org.apache.zookeeper.server.ZooKeeperServer;
import org.apache.zookeeper.server.persistence.FileTxnSnapLog;
import org.apache.zookeeper.txn.CreateSessionTxn;
import org.apache.zookeeper.txn.TxnHeader;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>ZooKeeperServerTest</code> contains tests for the class <code>{@link ZooKeeperServer}</code>.
 *
 * @generatedBy CodePro at 10/16/13 8:25 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class ZooKeeperServerTest {
	/**
	 * Run the ZooKeeperServer() constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testZooKeeperServer_1()
		throws Exception {

		ZooKeeperServer result = new ZooKeeperServer();

		// add additional test code here
		assertNotNull(result);
		assertEquals("standalone", result.getState());
		assertEquals(false, result.isRunning());
		assertEquals(0L, result.getServerId());
		assertEquals(6000, result.getMinSessionTimeout());
		assertEquals(60000, result.getMaxSessionTimeout());
		assertEquals(-1, result.getClientPort());
		assertEquals(3000, result.getTickTime());
		assertEquals(null, result.getZKDatabase());
		assertEquals(0L, result.getZxid());
		assertEquals(0, result.getInProcess());
		assertEquals(1000, result.getGlobalOutstandingLimit());
		assertEquals(null, result.getServerCnxnFactory());
		assertEquals(0L, result.getOutstandingRequests());
		assertEquals(null, result.getTxnLogFactory());
	}

	/**
	 * Run the ZooKeeperServer(FileTxnSnapLog,DataTreeBuilder) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testZooKeeperServer_2()
		throws Exception {
		FileTxnSnapLog txnLogFactory = new FileTxnSnapLog(new File(""), new File(""));
		org.apache.zookeeper.server.ZooKeeperServer.DataTreeBuilder treeBuilder = new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder();

		ZooKeeperServer result = new ZooKeeperServer(txnLogFactory, treeBuilder);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertNotNull(result);
	}

	/**
	 * Run the ZooKeeperServer(File,File,int) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testZooKeeperServer_3()
		throws Exception {
		File snapDir = new File("");
		File logDir = new File("");
		int tickTime = 1;

		ZooKeeperServer result = new ZooKeeperServer(snapDir, logDir, tickTime);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		//       at org.apache.zookeeper.server.ZooKeeperServer.<init>(ZooKeeperServer.java:213)
		assertNotNull(result);
	}

	/**
	 * Run the ZooKeeperServer(File,File,int) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testZooKeeperServer_4()
		throws Exception {
		File snapDir = new File("");
		File logDir = new File("");
		int tickTime = 1;

		ZooKeeperServer result = new ZooKeeperServer(snapDir, logDir, tickTime);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		//       at org.apache.zookeeper.server.ZooKeeperServer.<init>(ZooKeeperServer.java:213)
		assertNotNull(result);
	}

	/**
	 * Run the ZooKeeperServer(FileTxnSnapLog,int,DataTreeBuilder) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testZooKeeperServer_5()
		throws Exception {
		FileTxnSnapLog txnLogFactory = new FileTxnSnapLog(new File(""), new File(""));
		int tickTime = 1;
		org.apache.zookeeper.server.ZooKeeperServer.DataTreeBuilder treeBuilder = new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder();

		ZooKeeperServer result = new ZooKeeperServer(txnLogFactory, tickTime, treeBuilder);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertNotNull(result);
	}

	/**
	 * Run the ZooKeeperServer(FileTxnSnapLog,int,int,int,DataTreeBuilder,ZKDatabase) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testZooKeeperServer_6()
		throws Exception {
		FileTxnSnapLog txnLogFactory = new FileTxnSnapLog(new File(""), new File(""));
		int tickTime = 1;
		int minSessionTimeout = 1;
		int maxSessionTimeout = 1;
		org.apache.zookeeper.server.ZooKeeperServer.DataTreeBuilder treeBuilder = new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder();
		ZKDatabase zkDb = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));

		ZooKeeperServer result = new ZooKeeperServer(txnLogFactory, tickTime, minSessionTimeout, maxSessionTimeout, treeBuilder, zkDb);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertNotNull(result);
	}

	/**
	 * Run the boolean checkPasswd(long,byte[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testCheckPasswd_1()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		long sessionId = 1L;
		byte[] passwd = new byte[] {};

		boolean result = fixture.checkPasswd(sessionId, passwd);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertTrue(result);
	}

	/**
	 * Run the boolean checkPasswd(long,byte[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testCheckPasswd_2()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		long sessionId = 0;
		byte[] passwd = new byte[] {};

		boolean result = fixture.checkPasswd(sessionId, passwd);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertTrue(result);
	}

	/**
	 * Run the boolean checkPasswd(long,byte[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testCheckPasswd_3()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		long sessionId = 1L;
		byte[] passwd = new byte[] {};

		boolean result = fixture.checkPasswd(sessionId, passwd);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertTrue(result);
	}

	/**
	 * Run the void closeSession(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testCloseSession_1()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		long sessionId = 1L;

		fixture.closeSession(sessionId);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void closeSession(ServerCnxn,RequestHeader) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testCloseSession_2()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		ServerCnxn cnxn = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		RequestHeader requestHeader = new RequestHeader();

		fixture.closeSession(cnxn, requestHeader);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the long createSession(ServerCnxn,byte[],int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testCreateSession_1()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		ServerCnxn cnxn = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		byte[] passwd = new byte[] {};
		int timeout = 1;

		long result = fixture.createSession(cnxn, passwd, timeout);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertEquals(0L, result);
	}

	/**
	 * Run the void createSessionTracker() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testCreateSessionTracker_1()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		fixture.createSessionTracker();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void decInProcess() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testDecInProcess_1()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		fixture.decInProcess();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void dumpConf(PrintWriter) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testDumpConf_1()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		PrintWriter pwriter = new PrintWriter(new CharArrayWriter());

		fixture.dumpConf(pwriter);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void dumpEphemerals(PrintWriter) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testDumpEphemerals_1()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		PrintWriter pwriter = new PrintWriter(new CharArrayWriter());

		fixture.dumpEphemerals(pwriter);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void expire(Session) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testExpire_1()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		org.apache.zookeeper.server.SessionTracker.Session session = null;

		fixture.expire(session);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the byte[] generatePasswd(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testGeneratePasswd_1()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		long id = 1L;

		byte[] result = fixture.generatePasswd(id);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertNotNull(result);
	}

	/**
	 * Run the int getClientPort() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testGetClientPort_1()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory((ServerCnxnFactory) null);

		int result = fixture.getClientPort();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertEquals(0, result);
	}

	/**
	 * Run the int getClientPort() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testGetClientPort_2()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		int result = fixture.getClientPort();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertEquals(0, result);
	}

	/**
	 * Run the int getGlobalOutstandingLimit() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testGetGlobalOutstandingLimit_1()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		int result = fixture.getGlobalOutstandingLimit();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertEquals(0, result);
	}

	/**
	 * Run the int getInProcess() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testGetInProcess_1()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		int result = fixture.getInProcess();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertEquals(0, result);
	}

	/**
	 * Run the long getLastProcessedZxid() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testGetLastProcessedZxid_1()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		long result = fixture.getLastProcessedZxid();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertEquals(0L, result);
	}

	/**
	 * Run the int getMaxSessionTimeout() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testGetMaxSessionTimeout_1()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		int result = fixture.getMaxSessionTimeout();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertEquals(0, result);
	}

	/**
	 * Run the int getMaxSessionTimeout() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testGetMaxSessionTimeout_2()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, -1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		int result = fixture.getMaxSessionTimeout();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertEquals(0, result);
	}

	/**
	 * Run the int getMinSessionTimeout() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testGetMinSessionTimeout_1()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		int result = fixture.getMinSessionTimeout();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertEquals(0, result);
	}

	/**
	 * Run the int getMinSessionTimeout() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testGetMinSessionTimeout_2()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, -1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		int result = fixture.getMinSessionTimeout();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertEquals(0, result);
	}

	/**
	 * Run the long getNextZxid() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testGetNextZxid_1()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		long result = fixture.getNextZxid();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertEquals(0L, result);
	}

	/**
	 * Run the int getNumAliveConnections() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testGetNumAliveConnections_1()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		int result = fixture.getNumAliveConnections();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertEquals(0, result);
	}

	/**
	 * Run the long getOutstandingRequests() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testGetOutstandingRequests_1()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		long result = fixture.getOutstandingRequests();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertEquals(0L, result);
	}

	/**
	 * Run the ServerCnxnFactory getServerCnxnFactory() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testGetServerCnxnFactory_1()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		ServerCnxnFactory result = fixture.getServerCnxnFactory();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertNotNull(result);
	}

	/**
	 * Run the long getServerId() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testGetServerId_1()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		long result = fixture.getServerId();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertEquals(0L, result);
	}

	/**
	 * Run the int getSnapCount() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testGetSnapCount_1()
		throws Exception {

		int result = ZooKeeperServer.getSnapCount();

		// add additional test code here
		assertEquals(100000, result);
	}

	/**
	 * Run the int getSnapCount() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testGetSnapCount_2()
		throws Exception {

		int result = ZooKeeperServer.getSnapCount();

		// add additional test code here
		assertEquals(100000, result);
	}

	/**
	 * Run the String getState() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testGetState_1()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		String result = fixture.getState();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertNotNull(result);
	}

	/**
	 * Run the int getTickTime() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testGetTickTime_1()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		int result = fixture.getTickTime();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertEquals(0, result);
	}

	/**
	 * Run the long getTime() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testGetTime_1()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		long result = fixture.getTime();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertEquals(0L, result);
	}

	/**
	 * Run the FileTxnSnapLog getTxnLogFactory() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testGetTxnLogFactory_1()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		FileTxnSnapLog result = fixture.getTxnLogFactory();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertNotNull(result);
	}

	/**
	 * Run the ZKDatabase getZKDatabase() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testGetZKDatabase_1()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		ZKDatabase result = fixture.getZKDatabase();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertNotNull(result);
	}

	/**
	 * Run the long getZxid() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testGetZxid_1()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		long result = fixture.getZxid();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertEquals(0L, result);
	}

	/**
	 * Run the void incInProcess() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testIncInProcess_1()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		fixture.incInProcess();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the boolean isRunning() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testIsRunning_1()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		boolean result = fixture.isRunning();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertTrue(result);
	}

	/**
	 * Run the boolean isRunning() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testIsRunning_2()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		boolean result = fixture.isRunning();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertTrue(result);
	}

	/**
	 * Run the void killSession(long,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testKillSession_1()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		long sessionId = 1L;
		long zxid = 1L;

		fixture.killSession(sessionId, zxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void killSession(long,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testKillSession_2()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		long sessionId = 1L;
		long zxid = 1L;

		fixture.killSession(sessionId, zxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void killSession(long,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testKillSession_3()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		long sessionId = 1L;
		long zxid = 1L;

		fixture.killSession(sessionId, zxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void killSession(long,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testKillSession_4()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		long sessionId = 1L;
		long zxid = 1L;

		fixture.killSession(sessionId, zxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void loadData() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testLoadData_1()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		fixture.loadData();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void loadData() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testLoadData_2()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		fixture.loadData();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void loadData() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testLoadData_3()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		fixture.loadData();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void loadData() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testLoadData_4()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		fixture.loadData();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void loadData() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testLoadData_5()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		fixture.loadData();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void loadData() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testLoadData_6()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		fixture.loadData();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void loadData() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testLoadData_7()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		fixture.loadData();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void processConnectRequest(ServerCnxn,ByteBuffer) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testProcessConnectRequest_1()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		ServerCnxn cnxn = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		ByteBuffer incomingBuffer = ByteBuffer.allocate(0);

		fixture.processConnectRequest(cnxn, incomingBuffer);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void processConnectRequest(ServerCnxn,ByteBuffer) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testProcessConnectRequest_2()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		ServerCnxn cnxn = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		ByteBuffer incomingBuffer = ByteBuffer.allocate(0);

		fixture.processConnectRequest(cnxn, incomingBuffer);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void processConnectRequest(ServerCnxn,ByteBuffer) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testProcessConnectRequest_3()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		ServerCnxn cnxn = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		ByteBuffer incomingBuffer = ByteBuffer.allocate(0);

		fixture.processConnectRequest(cnxn, incomingBuffer);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void processConnectRequest(ServerCnxn,ByteBuffer) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testProcessConnectRequest_4()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		ServerCnxn cnxn = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		ByteBuffer incomingBuffer = ByteBuffer.allocate(0);

		fixture.processConnectRequest(cnxn, incomingBuffer);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void processConnectRequest(ServerCnxn,ByteBuffer) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testProcessConnectRequest_5()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		ServerCnxn cnxn = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		ByteBuffer incomingBuffer = ByteBuffer.allocate(0);

		fixture.processConnectRequest(cnxn, incomingBuffer);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void processConnectRequest(ServerCnxn,ByteBuffer) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testProcessConnectRequest_6()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		ServerCnxn cnxn = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		ByteBuffer incomingBuffer = ByteBuffer.allocate(0);

		fixture.processConnectRequest(cnxn, incomingBuffer);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void processConnectRequest(ServerCnxn,ByteBuffer) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testProcessConnectRequest_7()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		ServerCnxn cnxn = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		ByteBuffer incomingBuffer = ByteBuffer.allocate(0);

		fixture.processConnectRequest(cnxn, incomingBuffer);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void processConnectRequest(ServerCnxn,ByteBuffer) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testProcessConnectRequest_8()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		ServerCnxn cnxn = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		ByteBuffer incomingBuffer = ByteBuffer.allocate(0);

		fixture.processConnectRequest(cnxn, incomingBuffer);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void processConnectRequest(ServerCnxn,ByteBuffer) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testProcessConnectRequest_9()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		ServerCnxn cnxn = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		ByteBuffer incomingBuffer = ByteBuffer.allocate(0);

		fixture.processConnectRequest(cnxn, incomingBuffer);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void processConnectRequest(ServerCnxn,ByteBuffer) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testProcessConnectRequest_10()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		ServerCnxn cnxn = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		ByteBuffer incomingBuffer = ByteBuffer.allocate(0);

		fixture.processConnectRequest(cnxn, incomingBuffer);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void processConnectRequest(ServerCnxn,ByteBuffer) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testProcessConnectRequest_11()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		ServerCnxn cnxn = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		ByteBuffer incomingBuffer = ByteBuffer.allocate(0);

		fixture.processConnectRequest(cnxn, incomingBuffer);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void processConnectRequest(ServerCnxn,ByteBuffer) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testProcessConnectRequest_12()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		ServerCnxn cnxn = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		ByteBuffer incomingBuffer = ByteBuffer.allocate(0);

		fixture.processConnectRequest(cnxn, incomingBuffer);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void processConnectRequest(ServerCnxn,ByteBuffer) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testProcessConnectRequest_13()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		ServerCnxn cnxn = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		ByteBuffer incomingBuffer = ByteBuffer.allocate(0);

		fixture.processConnectRequest(cnxn, incomingBuffer);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void processConnectRequest(ServerCnxn,ByteBuffer) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testProcessConnectRequest_14()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		ServerCnxn cnxn = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		ByteBuffer incomingBuffer = ByteBuffer.allocate(0);

		fixture.processConnectRequest(cnxn, incomingBuffer);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void processConnectRequest(ServerCnxn,ByteBuffer) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testProcessConnectRequest_15()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		ServerCnxn cnxn = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		ByteBuffer incomingBuffer = ByteBuffer.allocate(0);

		fixture.processConnectRequest(cnxn, incomingBuffer);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void processConnectRequest(ServerCnxn,ByteBuffer) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testProcessConnectRequest_16()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		ServerCnxn cnxn = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		ByteBuffer incomingBuffer = ByteBuffer.allocate(0);

		fixture.processConnectRequest(cnxn, incomingBuffer);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void processPacket(ServerCnxn,ByteBuffer) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testProcessPacket_1()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		ServerCnxn cnxn = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		ByteBuffer incomingBuffer = ByteBuffer.allocate(0);

		fixture.processPacket(cnxn, incomingBuffer);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void processPacket(ServerCnxn,ByteBuffer) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testProcessPacket_2()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		ServerCnxn cnxn = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		ByteBuffer incomingBuffer = ByteBuffer.allocate(0);

		fixture.processPacket(cnxn, incomingBuffer);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void processPacket(ServerCnxn,ByteBuffer) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testProcessPacket_3()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		ServerCnxn cnxn = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		ByteBuffer incomingBuffer = ByteBuffer.allocate(0);

		fixture.processPacket(cnxn, incomingBuffer);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void processPacket(ServerCnxn,ByteBuffer) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testProcessPacket_4()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		ServerCnxn cnxn = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		ByteBuffer incomingBuffer = ByteBuffer.allocate(0);

		fixture.processPacket(cnxn, incomingBuffer);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void processPacket(ServerCnxn,ByteBuffer) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testProcessPacket_5()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		ServerCnxn cnxn = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		ByteBuffer incomingBuffer = ByteBuffer.allocate(0);

		fixture.processPacket(cnxn, incomingBuffer);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void processPacket(ServerCnxn,ByteBuffer) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testProcessPacket_6()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		ServerCnxn cnxn = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		ByteBuffer incomingBuffer = ByteBuffer.allocate(0);

		fixture.processPacket(cnxn, incomingBuffer);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void processPacket(ServerCnxn,ByteBuffer) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testProcessPacket_7()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		ServerCnxn cnxn = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		ByteBuffer incomingBuffer = ByteBuffer.allocate(0);

		fixture.processPacket(cnxn, incomingBuffer);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void processPacket(ServerCnxn,ByteBuffer) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testProcessPacket_8()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		ServerCnxn cnxn = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		ByteBuffer incomingBuffer = ByteBuffer.allocate(0);

		fixture.processPacket(cnxn, incomingBuffer);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void processPacket(ServerCnxn,ByteBuffer) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testProcessPacket_9()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		ServerCnxn cnxn = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		ByteBuffer incomingBuffer = ByteBuffer.allocate(0);

		fixture.processPacket(cnxn, incomingBuffer);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void processPacket(ServerCnxn,ByteBuffer) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testProcessPacket_10()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		ServerCnxn cnxn = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		ByteBuffer incomingBuffer = ByteBuffer.allocate(0);

		fixture.processPacket(cnxn, incomingBuffer);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the org.apache.zookeeper.server.DataTree.ProcessTxnResult processTxn(TxnHeader,Record) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testProcessTxn_1()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		TxnHeader hdr = new TxnHeader(1L, 1, 1L, 1L, -11);
		Record txn = new ACL();

		org.apache.zookeeper.server.DataTree.ProcessTxnResult result = fixture.processTxn(hdr, txn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertNotNull(result);
	}

	/**
	 * Run the org.apache.zookeeper.server.DataTree.ProcessTxnResult processTxn(TxnHeader,Record) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testProcessTxn_2()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		TxnHeader hdr = new TxnHeader(1L, 1, 1L, 1L, 1);
		Record txn = new ACL();

		org.apache.zookeeper.server.DataTree.ProcessTxnResult result = fixture.processTxn(hdr, txn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertNotNull(result);
	}

	/**
	 * Run the org.apache.zookeeper.server.DataTree.ProcessTxnResult processTxn(TxnHeader,Record) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testProcessTxn_3()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		TxnHeader hdr = new TxnHeader(1L, 1, 1L, 1L, -10);
		Record txn = new ACL();

		org.apache.zookeeper.server.DataTree.ProcessTxnResult result = fixture.processTxn(hdr, txn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertNotNull(result);
	}

	/**
	 * Run the org.apache.zookeeper.server.DataTree.ProcessTxnResult processTxn(TxnHeader,Record) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testProcessTxn_4()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		TxnHeader hdr = new TxnHeader(1L, 1, 1L, 1L, -10);
		Record txn = new CreateSessionTxn(1);

		org.apache.zookeeper.server.DataTree.ProcessTxnResult result = fixture.processTxn(hdr, txn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertNotNull(result);
	}

	/**
	 * Run the void registerJMX() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testRegisterJMX_1()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		fixture.registerJMX();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void registerJMX() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testRegisterJMX_2()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		fixture.registerJMX();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void registerJMX() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testRegisterJMX_3()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		fixture.registerJMX();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void removeCnxn(ServerCnxn) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testRemoveCnxn_1()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		ServerCnxn cnxn = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());

		fixture.removeCnxn(cnxn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void reopenSession(ServerCnxn,long,byte[],int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testReopenSession_1()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		ServerCnxn cnxn = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		long sessionId = 1L;
		byte[] passwd = new byte[] {};
		int sessionTimeout = 1;

		fixture.reopenSession(cnxn, sessionId, passwd, sessionTimeout);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void reopenSession(ServerCnxn,long,byte[],int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testReopenSession_2()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		ServerCnxn cnxn = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		long sessionId = 1L;
		byte[] passwd = new byte[] {};
		int sessionTimeout = 1;

		fixture.reopenSession(cnxn, sessionId, passwd, sessionTimeout);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void reopenSession(ServerCnxn,long,byte[],int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testReopenSession_3()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		ServerCnxn cnxn = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		long sessionId = 1L;
		byte[] passwd = new byte[] {};
		int sessionTimeout = 1;

		fixture.reopenSession(cnxn, sessionId, passwd, sessionTimeout);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void revalidateSession(ServerCnxn,long,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testRevalidateSession_1()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		ServerCnxn cnxn = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		long sessionId = 1L;
		int sessionTimeout = 1;

		fixture.revalidateSession(cnxn, sessionId, sessionTimeout);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void revalidateSession(ServerCnxn,long,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testRevalidateSession_2()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		ServerCnxn cnxn = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		long sessionId = 1L;
		int sessionTimeout = 1;

		fixture.revalidateSession(cnxn, sessionId, sessionTimeout);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the ServerStats serverStats() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testServerStats_1()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		ServerStats result = fixture.serverStats();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertNotNull(result);
	}

	/**
	 * Run the void setMaxSessionTimeout(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testSetMaxSessionTimeout_1()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		int max = 1;

		fixture.setMaxSessionTimeout(max);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void setMinSessionTimeout(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testSetMinSessionTimeout_1()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		int min = 1;

		fixture.setMinSessionTimeout(min);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void setOwner(long,Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testSetOwner_1()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		long id = 1L;
		Object owner = new Object();

		fixture.setOwner(id, owner);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void setOwner(long,Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testSetOwner_2()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		long id = 1L;
		Object owner = new Object();

		fixture.setOwner(id, owner);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void setServerCnxnFactory(ServerCnxnFactory) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testSetServerCnxnFactory_1()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		ServerCnxnFactory factory = new NIOServerCnxnFactory();

		fixture.setServerCnxnFactory(factory);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void setTickTime(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testSetTickTime_1()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		int tickTime = 1;

		fixture.setTickTime(tickTime);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void setTxnLogFactory(FileTxnSnapLog) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testSetTxnLogFactory_1()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		FileTxnSnapLog txnLog = new FileTxnSnapLog(new File(""), new File(""));

		fixture.setTxnLogFactory(txnLog);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void setZKDatabase(ZKDatabase) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testSetZKDatabase_1()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		ZKDatabase zkDb = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));

		fixture.setZKDatabase(zkDb);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void setZxid(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testSetZxid_1()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		long zxid = 1L;

		fixture.setZxid(zxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void setupRequestProcessors() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testSetupRequestProcessors_1()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		fixture.setupRequestProcessors();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the boolean shouldThrottle(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testShouldThrottle_1()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		long outStandingCount = 1L;

		boolean result = fixture.shouldThrottle(outStandingCount);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertTrue(result);
	}

	/**
	 * Run the boolean shouldThrottle(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testShouldThrottle_2()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		long outStandingCount = 0L;

		boolean result = fixture.shouldThrottle(outStandingCount);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertTrue(result);
	}

	/**
	 * Run the boolean shouldThrottle(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testShouldThrottle_3()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		long outStandingCount = 1L;

		boolean result = fixture.shouldThrottle(outStandingCount);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertTrue(result);
	}

	/**
	 * Run the void shutdown() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testShutdown_1()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		fixture.shutdown();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void shutdown() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testShutdown_2()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		fixture.shutdown();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void shutdown() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testShutdown_3()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		fixture.shutdown();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void shutdown() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testShutdown_4()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		fixture.shutdown();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void shutdown() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testShutdown_5()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), (ZKDatabase) null);
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		fixture.shutdown();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void shutdown() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testShutdown_6()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), (ZKDatabase) null);
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		fixture.shutdown();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void shutdown() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testShutdown_7()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), (ZKDatabase) null);
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		fixture.shutdown();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void shutdown() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testShutdown_8()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), (ZKDatabase) null);
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		fixture.shutdown();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void startSessionTracker() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testStartSessionTracker_1()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		fixture.startSessionTracker();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void startdata() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testStartdata_1()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		fixture.startdata();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void startdata() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testStartdata_2()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), (ZKDatabase) null);
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		fixture.startdata();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void startdata() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testStartdata_3()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), (ZKDatabase) null);
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		fixture.startdata();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void startdata() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testStartdata_4()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		fixture.startdata();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void startdata() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testStartdata_5()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), (ZKDatabase) null);
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		fixture.startdata();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void startdata() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testStartdata_6()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		fixture.startdata();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void startup() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testStartup_1()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		fixture.startup();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void startup() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testStartup_2()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		fixture.startup();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void submitRequest(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testSubmitRequest_1()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		Request si = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());

		fixture.submitRequest(si);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void submitRequest(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testSubmitRequest_2()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		Request si = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());

		fixture.submitRequest(si);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void submitRequest(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testSubmitRequest_3()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		Request si = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());

		fixture.submitRequest(si);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void submitRequest(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testSubmitRequest_4()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		Request si = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());

		fixture.submitRequest(si);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void submitRequest(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testSubmitRequest_5()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		Request si = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());

		fixture.submitRequest(si);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void takeSnapshot() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testTakeSnapshot_1()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		fixture.takeSnapshot();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void takeSnapshot() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testTakeSnapshot_2()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		fixture.takeSnapshot();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void touch(ServerCnxn) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testTouch_1()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		ServerCnxn cnxn = null;

		fixture.touch(cnxn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void touch(ServerCnxn) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testTouch_2()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		ServerCnxn cnxn = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());

		fixture.touch(cnxn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void touch(ServerCnxn) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testTouch_3()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		ServerCnxn cnxn = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());

		fixture.touch(cnxn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void truncateLog(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testTruncateLog_1()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		long zxid = 1L;

		fixture.truncateLog(zxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void truncateLog(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testTruncateLog_2()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());
		long zxid = 1L;

		fixture.truncateLog(zxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void unregisterJMX() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testUnregisterJMX_1()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		fixture.unregisterJMX();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void unregisterJMX() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testUnregisterJMX_2()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		fixture.unregisterJMX();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void unregisterJMX() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testUnregisterJMX_3()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		fixture.unregisterJMX();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void unregisterJMX() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testUnregisterJMX_4()
		throws Exception {
		ZooKeeperServer fixture = new ZooKeeperServer(new FileTxnSnapLog(new File(""), new File("")), 1, 1, 1, new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder(), new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setZxid(1L);
		fixture.setServerCnxnFactory(new NIOServerCnxnFactory());

		fixture.unregisterJMX();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(ZooKeeperServerTest.class);
	}
}