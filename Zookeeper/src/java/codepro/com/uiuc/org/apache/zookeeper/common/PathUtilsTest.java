package com.uiuc.org.apache.zookeeper.common;

import org.apache.zookeeper.common.PathUtils;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>PathUtilsTest</code> contains tests for the class <code>{@link PathUtils}</code>.
 *
 * @generatedBy CodePro at 10/16/13 7:32 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class PathUtilsTest {
	/**
	 * Run the void validatePath(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testValidatePath_1()
		throws Exception {
		String path = "aa";

		PathUtils.validatePath(path);

		// add additional test code here
	}

	/**
	 * Run the void validatePath(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testValidatePath_2()
		throws Exception {
		String path = "aa";

		PathUtils.validatePath(path);

		// add additional test code here
	}

	/**
	 * Run the void validatePath(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testValidatePath_3()
		throws Exception {
		String path = "aa";

		PathUtils.validatePath(path);

		// add additional test code here
	}

	/**
	 * Run the void validatePath(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testValidatePath_4()
		throws Exception {
		String path = "aa";

		PathUtils.validatePath(path);

		// add additional test code here
	}

	/**
	 * Run the void validatePath(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testValidatePath_5()
		throws Exception {
		String path = "aa";

		PathUtils.validatePath(path);

		// add additional test code here
	}

	/**
	 * Run the void validatePath(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testValidatePath_6()
		throws Exception {
		String path = "aa";

		PathUtils.validatePath(path);

		// add additional test code here
	}

	/**
	 * Run the void validatePath(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testValidatePath_7()
		throws Exception {
		String path = "aa";

		PathUtils.validatePath(path);

		// add additional test code here
	}

	/**
	 * Run the void validatePath(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testValidatePath_8()
		throws Exception {
		String path = "aa";

		PathUtils.validatePath(path);

		// add additional test code here
	}

	/**
	 * Run the void validatePath(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testValidatePath_9()
		throws Exception {
		String path = "aa";

		PathUtils.validatePath(path);

		// add additional test code here
	}

	/**
	 * Run the void validatePath(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testValidatePath_10()
		throws Exception {
		String path = "aa";

		PathUtils.validatePath(path);

		// add additional test code here
	}

	/**
	 * Run the void validatePath(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testValidatePath_11()
		throws Exception {
		String path = "aa";

		PathUtils.validatePath(path);

		// add additional test code here
	}

	/**
	 * Run the void validatePath(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testValidatePath_12()
		throws Exception {
		String path = "aa";

		PathUtils.validatePath(path);

		// add additional test code here
	}

	/**
	 * Run the void validatePath(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testValidatePath_13()
		throws Exception {
		String path = "aa";

		PathUtils.validatePath(path);

		// add additional test code here
	}

	/**
	 * Run the void validatePath(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testValidatePath_14()
		throws Exception {
		String path = "aa";

		PathUtils.validatePath(path);

		// add additional test code here
	}

	/**
	 * Run the void validatePath(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testValidatePath_15()
		throws Exception {
		String path = "aa";

		PathUtils.validatePath(path);

		// add additional test code here
	}

	/**
	 * Run the void validatePath(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testValidatePath_16()
		throws Exception {
		String path = "aa";

		PathUtils.validatePath(path);

		// add additional test code here
	}

	/**
	 * Run the void validatePath(String,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testValidatePath_17()
		throws Exception {
		String path = "";
		boolean isSequential = false;

		PathUtils.validatePath(path, isSequential);

		// add additional test code here
	}

	/**
	 * Run the void validatePath(String,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testValidatePath_18()
		throws Exception {
		String path = "";
		boolean isSequential = false;

		PathUtils.validatePath(path, isSequential);

		// add additional test code here
	}

	/**
	 * Run the void validatePath(String,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testValidatePath_19()
		throws Exception {
		String path = "";
		boolean isSequential = true;

		PathUtils.validatePath(path, isSequential);

		// add additional test code here
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(PathUtilsTest.class);
	}
}