package com.uiuc.org.apache.zookeeper.server.util;

import org.apache.zookeeper.server.util.Profiler;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>ProfilerTest</code> contains tests for the class <code>{@link Profiler}</code>.
 *
 * @generatedBy CodePro at 10/16/13 7:40 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class ProfilerTest {
	/**
	 * Run the Profiler() constructor test.
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test
	public void testProfiler_1()
		throws Exception {
		Profiler result = new Profiler();
		assertNotNull(result);
		// add additional test code here
	}

	/**
	 * Run the Object profile(Operation<T>,long,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test(expected = java.lang.NullPointerException.class)
	public void testProfile_1()
		throws Exception {
		org.apache.zookeeper.server.util.Profiler.Operation<Object> op = null;
		long timeout = 1L;
		String message = "";

		Object result = Profiler.profile(op, timeout, message);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the Object profile(Operation<T>,long,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test(expected = java.lang.NullPointerException.class)
	public void testProfile_2()
		throws Exception {
		org.apache.zookeeper.server.util.Profiler.Operation<Object> op = null;
		long timeout = 1L;
		String message = "";

		Object result = Profiler.profile(op, timeout, message);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the Object profile(Operation<T>,long,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test(expected = java.lang.NullPointerException.class)
	public void testProfile_3()
		throws Exception {
		org.apache.zookeeper.server.util.Profiler.Operation<Object> op = null;
		long timeout = 1L;
		String message = "";

		Object result = Profiler.profile(op, timeout, message);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(ProfilerTest.class);
	}
}