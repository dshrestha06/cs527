package com.uiuc.org.apache.zookeeper;

import org.apache.zookeeper.StatsTrack;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>StatsTrackTest</code> contains tests for the class <code>{@link StatsTrack}</code>.
 *
 * @generatedBy CodePro at 10/16/13 7:30 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class StatsTrackTest {
	/**
	 * Run the StatsTrack() constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:30 PM
	 */
	@Test
	public void testStatsTrack_1()
		throws Exception {

		StatsTrack result = new StatsTrack();

		// add additional test code here
		assertNotNull(result);
		assertEquals("count=-1,bytes=-1", result.toString());
		assertEquals(-1L, result.getBytes());
		assertEquals(-1, result.getCount());
	}

	/**
	 * Run the StatsTrack(String) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:30 PM
	 */
	@Test
	public void testStatsTrack_2()
		throws Exception {
		String stats = null;

		StatsTrack result = new StatsTrack(stats);

		// add additional test code here
		assertNotNull(result);
		assertEquals("count=-1,bytes=-1", result.toString());
		assertEquals(-1L, result.getBytes());
		assertEquals(-1, result.getCount());
	}

	/**
	 * Run the StatsTrack(String) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:30 PM
	 */
	@Test
	public void testStatsTrack_3()
		throws Exception {
		String stats = "";

		StatsTrack result = new StatsTrack(stats);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: invalid string 
		//       at org.apache.zookeeper.StatsTrack.<init>(StatsTrack.java:49)
		assertNotNull(result);
	}

	/**
	 * Run the StatsTrack(String) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:30 PM
	 */
	@Test
	public void testStatsTrack_4()
		throws Exception {
		String stats = null;

		StatsTrack result = new StatsTrack(stats);

		// add additional test code here
		assertNotNull(result);
		assertEquals("count=-1,bytes=-1", result.toString());
		assertEquals(-1L, result.getBytes());
		assertEquals(-1, result.getCount());
	}

	/**
	 * Run the StatsTrack(String) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:30 PM
	 */
	@Test
	public void testStatsTrack_5()
		throws Exception {
		String stats = "";

		StatsTrack result = new StatsTrack(stats);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: invalid string 
		//       at org.apache.zookeeper.StatsTrack.<init>(StatsTrack.java:49)
		assertNotNull(result);
	}

	/**
	 * Run the StatsTrack(String) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:30 PM
	 */
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testStatsTrack_6()
		throws Exception {
		String stats = "";

		StatsTrack result = new StatsTrack(stats);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the long getBytes() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:30 PM
	 */
	@Test
	public void testGetBytes_1()
		throws Exception {
		StatsTrack fixture = new StatsTrack();
		fixture.setBytes(1L);
		fixture.setCount(1);

		long result = fixture.getBytes();

		// add additional test code here
		assertEquals(1L, result);
	}

	/**
	 * Run the int getCount() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:30 PM
	 */
	@Test
	public void testGetCount_1()
		throws Exception {
		StatsTrack fixture = new StatsTrack();
		fixture.setBytes(1L);
		fixture.setCount(1);

		int result = fixture.getCount();

		// add additional test code here
		assertEquals(1, result);
	}

	/**
	 * Run the void setBytes(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:30 PM
	 */
	@Test
	public void testSetBytes_1()
		throws Exception {
		StatsTrack fixture = new StatsTrack();
		fixture.setBytes(1L);
		fixture.setCount(1);
		long bytes = 1L;

		fixture.setBytes(bytes);

		// add additional test code here
	}

	/**
	 * Run the void setCount(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:30 PM
	 */
	@Test
	public void testSetCount_1()
		throws Exception {
		StatsTrack fixture = new StatsTrack();
		fixture.setBytes(1L);
		fixture.setCount(1);
		int count = 1;

		fixture.setCount(count);

		// add additional test code here
	}

	/**
	 * Run the String toString() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:30 PM
	 */
	@Test
	public void testToString_1()
		throws Exception {
		StatsTrack fixture = new StatsTrack();
		fixture.setBytes(1L);
		fixture.setCount(1);

		String result = fixture.toString();

		// add additional test code here
		assertEquals("count=1,bytes=1", result);
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:30 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:30 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:30 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(StatsTrackTest.class);
	}
}