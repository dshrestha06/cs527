package com.uiuc.org.apache.zookeeper.server;

import java.io.CharArrayWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.zookeeper.server.SessionTracker;
import org.apache.zookeeper.server.SessionTrackerImpl;
import org.apache.zookeeper.server.ZooKeeperServer;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>SessionTrackerImplTest</code> contains tests for the class <code>{@link SessionTrackerImpl}</code>.
 *
 * @generatedBy CodePro at 10/16/13 8:24 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class SessionTrackerImplTest {
	/**
	 * Run the SessionTrackerImpl(SessionExpirer,ConcurrentHashMap<Long,Integer>,int,long) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testSessionTrackerImpl_1()
		throws Exception {
		org.apache.zookeeper.server.SessionTracker.SessionExpirer expirer = new ZooKeeperServer();
		ConcurrentHashMap<Long, Integer> sessionsWithTimeout = new ConcurrentHashMap();
		int tickTime = 1;
		long sid = 1L;

		SessionTrackerImpl result = new SessionTrackerImpl(expirer, sessionsWithTimeout, tickTime, sid);

		// add additional test code here
		assertNotNull(result);
		assertEquals("Session Sets (0):\r\n", result.toString());
		assertEquals(false, result.isInterrupted());
		assertEquals("SessionTracker", result.getName());
		assertEquals(0, result.countStackFrames());
		assertEquals(9600L, result.getId());
		assertEquals(1, result.getPriority());
		assertEquals(false, result.isAlive());
		assertEquals(true, result.isDaemon());
	}

	/**
	 * Run the SessionTrackerImpl(SessionExpirer,ConcurrentHashMap<Long,Integer>,int,long) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testSessionTrackerImpl_2()
		throws Exception {
		org.apache.zookeeper.server.SessionTracker.SessionExpirer expirer = new ZooKeeperServer();
		ConcurrentHashMap<Long, Integer> sessionsWithTimeout = new ConcurrentHashMap();
		int tickTime = 1;
		long sid = 1L;

		SessionTrackerImpl result = new SessionTrackerImpl(expirer, sessionsWithTimeout, tickTime, sid);

		// add additional test code here
		assertNotNull(result);
		assertEquals("Session Sets (0):\r\n", result.toString());
		assertEquals(false, result.isInterrupted());
		assertEquals("SessionTracker", result.getName());
		assertEquals(0, result.countStackFrames());
		assertEquals(9602L, result.getId());
		assertEquals(1, result.getPriority());
		assertEquals(false, result.isAlive());
		assertEquals(true, result.isDaemon());
	}

	/**
	 * Run the void addSession(long,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testAddSession_1()
		throws Exception {
		SessionTrackerImpl fixture = new SessionTrackerImpl(new ZooKeeperServer(), new ConcurrentHashMap(), 1, 1L);
		long id = 1L;
		int sessionTimeout = 1;

		fixture.addSession(id, sessionTimeout);

		// add additional test code here
	}

	/**
	 * Run the void addSession(long,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testAddSession_2()
		throws Exception {
		SessionTrackerImpl fixture = new SessionTrackerImpl(new ZooKeeperServer(), new ConcurrentHashMap(), 1, 1L);
		long id = 1L;
		int sessionTimeout = 1;

		fixture.addSession(id, sessionTimeout);

		// add additional test code here
	}

	/**
	 * Run the void addSession(long,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testAddSession_3()
		throws Exception {
		SessionTrackerImpl fixture = new SessionTrackerImpl(new ZooKeeperServer(), new ConcurrentHashMap(), 1, 1L);
		long id = 1L;
		int sessionTimeout = 1;

		fixture.addSession(id, sessionTimeout);

		// add additional test code here
	}

	/**
	 * Run the void addSession(long,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testAddSession_4()
		throws Exception {
		SessionTrackerImpl fixture = new SessionTrackerImpl(new ZooKeeperServer(), new ConcurrentHashMap(), 1, 1L);
		long id = 1L;
		int sessionTimeout = 1;

		fixture.addSession(id, sessionTimeout);

		// add additional test code here
	}

	/**
	 * Run the void checkSession(long,Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testCheckSession_1()
		throws Exception {
		SessionTrackerImpl fixture = new SessionTrackerImpl(new ZooKeeperServer(), new ConcurrentHashMap(), 1, 1L);
		long sessionId = 1L;
		Object owner = new Object();

		fixture.checkSession(sessionId, owner);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.KeeperException$SessionExpiredException: KeeperErrorCode = Session expired
		//       at org.apache.zookeeper.server.SessionTrackerImpl.checkSession(SessionTrackerImpl.java:263)
	}

	/**
	 * Run the void checkSession(long,Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testCheckSession_2()
		throws Exception {
		SessionTrackerImpl fixture = new SessionTrackerImpl(new ZooKeeperServer(), new ConcurrentHashMap(), 1, 1L);
		long sessionId = 1L;
		Object owner = new Object();

		fixture.checkSession(sessionId, owner);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.KeeperException$SessionExpiredException: KeeperErrorCode = Session expired
		//       at org.apache.zookeeper.server.SessionTrackerImpl.checkSession(SessionTrackerImpl.java:263)
	}

	/**
	 * Run the void checkSession(long,Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testCheckSession_3()
		throws Exception {
		SessionTrackerImpl fixture = new SessionTrackerImpl(new ZooKeeperServer(), new ConcurrentHashMap(), 1, 1L);
		long sessionId = 1L;
		Object owner = new Object();

		fixture.checkSession(sessionId, owner);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.KeeperException$SessionExpiredException: KeeperErrorCode = Session expired
		//       at org.apache.zookeeper.server.SessionTrackerImpl.checkSession(SessionTrackerImpl.java:263)
	}

	/**
	 * Run the void checkSession(long,Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testCheckSession_4()
		throws Exception {
		SessionTrackerImpl fixture = new SessionTrackerImpl(new ZooKeeperServer(), new ConcurrentHashMap(), 1, 1L);
		long sessionId = 1L;
		Object owner = new Object();

		fixture.checkSession(sessionId, owner);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.KeeperException$SessionExpiredException: KeeperErrorCode = Session expired
		//       at org.apache.zookeeper.server.SessionTrackerImpl.checkSession(SessionTrackerImpl.java:263)
	}

	/**
	 * Run the long createSession(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testCreateSession_1()
		throws Exception {
		SessionTrackerImpl fixture = new SessionTrackerImpl(new ZooKeeperServer(), new ConcurrentHashMap(), 1, 1L);
		int sessionTimeout = 1;

		long result = fixture.createSession(sessionTimeout);

		// add additional test code here
		assertEquals(90568988662104064L, result);
	}

	/**
	 * Run the void dumpSessions(PrintWriter) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testDumpSessions_1()
		throws Exception {
		SessionTrackerImpl fixture = new SessionTrackerImpl(new ZooKeeperServer(), new ConcurrentHashMap(), 1, 1L);
		PrintWriter pwriter = new PrintWriter(new CharArrayWriter());

		fixture.dumpSessions(pwriter);

		// add additional test code here
	}

	/**
	 * Run the void dumpSessions(PrintWriter) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testDumpSessions_2()
		throws Exception {
		SessionTrackerImpl fixture = new SessionTrackerImpl(new ZooKeeperServer(), new ConcurrentHashMap(), 1, 1L);
		PrintWriter pwriter = new PrintWriter(new CharArrayWriter());

		fixture.dumpSessions(pwriter);

		// add additional test code here
	}

	/**
	 * Run the void dumpSessions(PrintWriter) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testDumpSessions_3()
		throws Exception {
		SessionTrackerImpl fixture = new SessionTrackerImpl(new ZooKeeperServer(), new ConcurrentHashMap(), 1, 1L);
		PrintWriter pwriter = new PrintWriter(new CharArrayWriter());

		fixture.dumpSessions(pwriter);

		// add additional test code here
	}

	/**
	 * Run the long initializeNextSession(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testInitializeNextSession_1()
		throws Exception {
		long id = 1L;

		long result = SessionTrackerImpl.initializeNextSession(id);

		// add additional test code here
		assertEquals(90568987210285056L, result);
	}

	/**
	 * Run the void removeSession(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testRemoveSession_1()
		throws Exception {
		SessionTrackerImpl fixture = new SessionTrackerImpl(new ZooKeeperServer(), new ConcurrentHashMap(), 1, 1L);
		long sessionId = 1L;

		fixture.removeSession(sessionId);

		// add additional test code here
	}

	/**
	 * Run the void removeSession(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testRemoveSession_2()
		throws Exception {
		SessionTrackerImpl fixture = new SessionTrackerImpl(new ZooKeeperServer(), new ConcurrentHashMap(), 1, 1L);
		long sessionId = 1L;

		fixture.removeSession(sessionId);

		// add additional test code here
	}

	/**
	 * Run the void removeSession(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testRemoveSession_3()
		throws Exception {
		SessionTrackerImpl fixture = new SessionTrackerImpl(new ZooKeeperServer(), new ConcurrentHashMap(), 1, 1L);
		long sessionId = 1L;

		fixture.removeSession(sessionId);

		// add additional test code here
	}

	/**
	 * Run the void removeSession(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testRemoveSession_4()
		throws Exception {
		SessionTrackerImpl fixture = new SessionTrackerImpl(new ZooKeeperServer(), new ConcurrentHashMap(), 1, 1L);
		long sessionId = 1L;

		fixture.removeSession(sessionId);

		// add additional test code here
	}

	/**
	 * Run the void removeSession(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testRemoveSession_5()
		throws Exception {
		SessionTrackerImpl fixture = new SessionTrackerImpl(new ZooKeeperServer(), new ConcurrentHashMap(), 1, 1L);
		long sessionId = 1L;

		fixture.removeSession(sessionId);

		// add additional test code here
	}

	/**
	 * Run the void removeSession(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testRemoveSession_6()
		throws Exception {
		SessionTrackerImpl fixture = new SessionTrackerImpl(new ZooKeeperServer(), new ConcurrentHashMap(), 1, 1L);
		long sessionId = 1L;

		fixture.removeSession(sessionId);

		// add additional test code here
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testRun_1()
		throws Exception {
		SessionTrackerImpl fixture = new SessionTrackerImpl(new ZooKeeperServer(), new ConcurrentHashMap(), 1, 1L);

		fixture.run();

		// add additional test code here
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testRun_2()
		throws Exception {
		SessionTrackerImpl fixture = new SessionTrackerImpl(new ZooKeeperServer(), new ConcurrentHashMap(), 1, 1L);

		fixture.run();

		// add additional test code here
	}

	/**
	 * Run the void setOwner(long,Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testSetOwner_1()
		throws Exception {
		SessionTrackerImpl fixture = new SessionTrackerImpl(new ZooKeeperServer(), new ConcurrentHashMap(), 1, 1L);
		long id = 1L;
		Object owner = new Object();

		fixture.setOwner(id, owner);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.KeeperException$SessionExpiredException: KeeperErrorCode = Session expired
		//       at org.apache.zookeeper.server.SessionTrackerImpl.setOwner(SessionTrackerImpl.java:275)
	}

	/**
	 * Run the void setOwner(long,Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testSetOwner_2()
		throws Exception {
		SessionTrackerImpl fixture = new SessionTrackerImpl(new ZooKeeperServer(), new ConcurrentHashMap(), 1, 1L);
		long id = 1L;
		Object owner = new Object();

		fixture.setOwner(id, owner);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.KeeperException$SessionExpiredException: KeeperErrorCode = Session expired
		//       at org.apache.zookeeper.server.SessionTrackerImpl.setOwner(SessionTrackerImpl.java:275)
	}

	/**
	 * Run the void setOwner(long,Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testSetOwner_3()
		throws Exception {
		SessionTrackerImpl fixture = new SessionTrackerImpl(new ZooKeeperServer(), new ConcurrentHashMap(), 1, 1L);
		long id = 1L;
		Object owner = new Object();

		fixture.setOwner(id, owner);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.KeeperException$SessionExpiredException: KeeperErrorCode = Session expired
		//       at org.apache.zookeeper.server.SessionTrackerImpl.setOwner(SessionTrackerImpl.java:275)
	}

	/**
	 * Run the void setSessionClosing(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testSetSessionClosing_1()
		throws Exception {
		SessionTrackerImpl fixture = new SessionTrackerImpl(new ZooKeeperServer(), new ConcurrentHashMap(), 1, 1L);
		long sessionId = 1L;

		fixture.setSessionClosing(sessionId);

		// add additional test code here
	}

	/**
	 * Run the void setSessionClosing(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testSetSessionClosing_2()
		throws Exception {
		SessionTrackerImpl fixture = new SessionTrackerImpl(new ZooKeeperServer(), new ConcurrentHashMap(), 1, 1L);
		long sessionId = 1L;

		fixture.setSessionClosing(sessionId);

		// add additional test code here
	}

	/**
	 * Run the void setSessionClosing(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testSetSessionClosing_3()
		throws Exception {
		SessionTrackerImpl fixture = new SessionTrackerImpl(new ZooKeeperServer(), new ConcurrentHashMap(), 1, 1L);
		long sessionId = 1L;

		fixture.setSessionClosing(sessionId);

		// add additional test code here
	}

	/**
	 * Run the void shutdown() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testShutdown_1()
		throws Exception {
		SessionTrackerImpl fixture = new SessionTrackerImpl(new ZooKeeperServer(), new ConcurrentHashMap(), 1, 1L);

		fixture.shutdown();

		// add additional test code here
	}

	/**
	 * Run the void shutdown() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testShutdown_2()
		throws Exception {
		SessionTrackerImpl fixture = new SessionTrackerImpl(new ZooKeeperServer(), new ConcurrentHashMap(), 1, 1L);

		fixture.shutdown();

		// add additional test code here
	}

	/**
	 * Run the String toString() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testToString_1()
		throws Exception {
		SessionTrackerImpl fixture = new SessionTrackerImpl(new ZooKeeperServer(), new ConcurrentHashMap(), 1, 1L);

		String result = fixture.toString();

		// add additional test code here
		assertEquals("Session Sets (0):\r\n", result);
	}

	/**
	 * Run the boolean touchSession(long,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testTouchSession_1()
		throws Exception {
		SessionTrackerImpl fixture = new SessionTrackerImpl(new ZooKeeperServer(), new ConcurrentHashMap(), 1, 1L);
		long sessionId = 1L;
		int timeout = 1;

		boolean result = fixture.touchSession(sessionId, timeout);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean touchSession(long,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testTouchSession_2()
		throws Exception {
		SessionTrackerImpl fixture = new SessionTrackerImpl(new ZooKeeperServer(), new ConcurrentHashMap(), 1, 1L);
		long sessionId = 1L;
		int timeout = 1;

		boolean result = fixture.touchSession(sessionId, timeout);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean touchSession(long,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testTouchSession_3()
		throws Exception {
		SessionTrackerImpl fixture = new SessionTrackerImpl(new ZooKeeperServer(), new ConcurrentHashMap(), 1, 1L);
		long sessionId = 1L;
		int timeout = 1;

		boolean result = fixture.touchSession(sessionId, timeout);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean touchSession(long,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testTouchSession_4()
		throws Exception {
		SessionTrackerImpl fixture = new SessionTrackerImpl(new ZooKeeperServer(), new ConcurrentHashMap(), 1, 1L);
		long sessionId = 1L;
		int timeout = 1;

		boolean result = fixture.touchSession(sessionId, timeout);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean touchSession(long,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testTouchSession_5()
		throws Exception {
		SessionTrackerImpl fixture = new SessionTrackerImpl(new ZooKeeperServer(), new ConcurrentHashMap(), 1, 1L);
		long sessionId = 1L;
		int timeout = 1;

		boolean result = fixture.touchSession(sessionId, timeout);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(SessionTrackerImplTest.class);
	}
}