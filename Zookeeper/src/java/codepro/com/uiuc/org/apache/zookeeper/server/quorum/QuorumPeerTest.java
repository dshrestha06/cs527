package com.uiuc.org.apache.zookeeper.server.quorum;

import java.io.File;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Map;
import org.apache.zookeeper.server.NIOServerCnxnFactory;
import org.apache.zookeeper.server.ServerCnxnFactory;
import org.apache.zookeeper.server.ZKDatabase;
import org.apache.zookeeper.server.ZooKeeperServer;
import org.apache.zookeeper.server.persistence.FileTxnSnapLog;
import org.apache.zookeeper.server.quorum.Election;
import org.apache.zookeeper.server.quorum.Follower;
import org.apache.zookeeper.server.quorum.Leader;
import org.apache.zookeeper.server.quorum.Observer;
import org.apache.zookeeper.server.quorum.QuorumCnxManager;
import org.apache.zookeeper.server.quorum.QuorumPeer;
import org.apache.zookeeper.server.quorum.QuorumStats;
import org.apache.zookeeper.server.quorum.Vote;
import org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical;
import org.apache.zookeeper.server.quorum.flexible.QuorumVerifier;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>QuorumPeerTest</code> contains tests for the class <code>{@link QuorumPeer}</code>.
 *
 * @generatedBy CodePro at 10/16/13 8:14 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class QuorumPeerTest {
	/**
	 * Run the QuorumPeer() constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testQuorumPeer_1()
		throws Exception {

		QuorumPeer result = new QuorumPeer();

		// add additional test code here
		assertNotNull(result);
		assertEquals(0L, result.getId());
		assertEquals(true, result.isRunning());
		assertEquals("leaderelection", result.getServerState());
		assertEquals(-1, result.getMaxClientCnxnsPerHost());
		assertEquals(0, result.getMinSessionTimeout());
		assertEquals(0, result.getMaxSessionTimeout());
		assertEquals(0, result.getTickTime());
		assertEquals(0, result.getInitLimit());
		assertEquals(0, result.getSyncLimit());
		assertEquals(0, result.getTick());
		assertEquals(null, result.getQuorumAddress());
		assertEquals(0, result.getElectionType());
		assertEquals(null, result.getCurrentVote());
		assertEquals(null, result.getActiveServer());
		assertEquals(null, result.getElectionAlg());
		assertEquals(0L, result.getMyid());
		assertEquals(null, result.getCnxnFactory());
		assertEquals(null, result.getQuorumVerifier());
		assertEquals(null, result.getTxnFactory());
		assertEquals(null, result.getQuorumCnxManager());
		assertEquals("Thread[QuorumPeer,1,main]", result.toString());
		assertEquals(false, result.isInterrupted());
		assertEquals("QuorumPeer", result.getName());
		assertEquals(0, result.countStackFrames());
		assertEquals(1, result.getPriority());
		assertEquals(false, result.isAlive());
		assertEquals(true, result.isDaemon());
	}

	/**
	 * Run the QuorumPeer(Map<Long,QuorumServer>,File,File,int,int,long,int,int,int) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testQuorumPeer_2()
		throws Exception {
		Map<Long, org.apache.zookeeper.server.quorum.QuorumPeer.QuorumServer> quorumPeers = new HashMap();
		File snapDir = new File("");
		File logDir = new File("");
		int clientPort = 1;
		int electionAlg = 1;
		long myid = 1L;
		int tickTime = 1;
		int initLimit = 1;
		int syncLimit = 1;

		QuorumPeer result = new QuorumPeer(quorumPeers, snapDir, logDir, clientPort, electionAlg, myid, tickTime, initLimit, syncLimit);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.<init>(QuorumPeer.java:396)
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.<init>(QuorumPeer.java:520)
		assertNotNull(result);
	}

	/**
	 * Run the QuorumPeer(Map<Long,QuorumServer>,File,File,int,int,long,int,int,int) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testQuorumPeer_3()
		throws Exception {
		Map<Long, org.apache.zookeeper.server.quorum.QuorumPeer.QuorumServer> quorumPeers = new HashMap();
		File snapDir = new File("");
		File logDir = new File("");
		int clientPort = 1;
		int electionAlg = 1;
		long myid = 1L;
		int tickTime = 1;
		int initLimit = 1;
		int syncLimit = 1;

		QuorumPeer result = new QuorumPeer(quorumPeers, snapDir, logDir, clientPort, electionAlg, myid, tickTime, initLimit, syncLimit);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.<init>(QuorumPeer.java:396)
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.<init>(QuorumPeer.java:520)
		assertNotNull(result);
	}

	/**
	 * Run the QuorumPeer(Map<Long,QuorumServer>,File,File,int,long,int,int,int,ServerCnxnFactory) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testQuorumPeer_4()
		throws Exception {
		Map<Long, org.apache.zookeeper.server.quorum.QuorumPeer.QuorumServer> quorumPeers = new HashMap();
		File dataDir = new File("");
		File dataLogDir = new File("");
		int electionType = 1;
		long myid = 1L;
		int tickTime = 1;
		int initLimit = 1;
		int syncLimit = 1;
		ServerCnxnFactory cnxnFactory = new NIOServerCnxnFactory();

		QuorumPeer result = new QuorumPeer(quorumPeers, dataDir, dataLogDir, electionType, myid, tickTime, initLimit, syncLimit, cnxnFactory);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.<init>(QuorumPeer.java:396)
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.<init>(QuorumPeer.java:380)
		assertNotNull(result);
	}

	/**
	 * Run the QuorumPeer(Map<Long,QuorumServer>,File,File,int,int,long,int,int,int,QuorumVerifier) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testQuorumPeer_5()
		throws Exception {
		Map<Long, org.apache.zookeeper.server.quorum.QuorumPeer.QuorumServer> quorumPeers = new HashMap();
		File snapDir = new File("");
		File logDir = new File("");
		int clientPort = 1;
		int electionAlg = 1;
		long myid = 1L;
		int tickTime = 1;
		int initLimit = 1;
		int syncLimit = 1;
		QuorumVerifier quorumConfig = new QuorumHierarchical("");

		QuorumPeer result = new QuorumPeer(quorumPeers, snapDir, logDir, clientPort, electionAlg, myid, tickTime, initLimit, syncLimit, quorumConfig);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertNotNull(result);
	}

	/**
	 * Run the QuorumPeer(Map<Long,QuorumServer>,File,File,int,int,long,int,int,int,QuorumVerifier) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testQuorumPeer_6()
		throws Exception {
		Map<Long, org.apache.zookeeper.server.quorum.QuorumPeer.QuorumServer> quorumPeers = new HashMap();
		File snapDir = new File("");
		File logDir = new File("");
		int clientPort = 1;
		int electionAlg = 1;
		long myid = 1L;
		int tickTime = 1;
		int initLimit = 1;
		int syncLimit = 1;
		QuorumVerifier quorumConfig = new QuorumHierarchical("");

		QuorumPeer result = new QuorumPeer(quorumPeers, snapDir, logDir, clientPort, electionAlg, myid, tickTime, initLimit, syncLimit, quorumConfig);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertNotNull(result);
	}

	/**
	 * Run the QuorumPeer(Map<Long,QuorumServer>,File,File,int,long,int,int,int,ServerCnxnFactory,QuorumVerifier) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testQuorumPeer_7()
		throws Exception {
		Map<Long, org.apache.zookeeper.server.quorum.QuorumPeer.QuorumServer> quorumPeers = new HashMap();
		File dataDir = new File("");
		File dataLogDir = new File("");
		int electionType = 1;
		long myid = 1L;
		int tickTime = 1;
		int initLimit = 1;
		int syncLimit = 1;
		ServerCnxnFactory cnxnFactory = new NIOServerCnxnFactory();
		QuorumVerifier quorumConfig = new QuorumHierarchical("");

		QuorumPeer result = new QuorumPeer(quorumPeers, dataDir, dataLogDir, electionType, myid, tickTime, initLimit, syncLimit, cnxnFactory, quorumConfig);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertNotNull(result);
	}

	/**
	 * Run the QuorumPeer(Map<Long,QuorumServer>,File,File,int,long,int,int,int,ServerCnxnFactory,QuorumVerifier) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testQuorumPeer_8()
		throws Exception {
		Map<Long, org.apache.zookeeper.server.quorum.QuorumPeer.QuorumServer> quorumPeers = new HashMap();
		File dataDir = new File("");
		File dataLogDir = new File("");
		int electionType = 1;
		long myid = 1L;
		int tickTime = 1;
		int initLimit = 1;
		int syncLimit = 1;
		ServerCnxnFactory cnxnFactory = new NIOServerCnxnFactory();
		QuorumVerifier quorumConfig = new QuorumHierarchical("");

		QuorumPeer result = new QuorumPeer(quorumPeers, dataDir, dataLogDir, electionType, myid, tickTime, initLimit, syncLimit, cnxnFactory, quorumConfig);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertNotNull(result);
	}

	/**
	 * Run the QuorumPeer(Map<Long,QuorumServer>,File,File,int,long,int,int,int,ServerCnxnFactory,QuorumVerifier) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testQuorumPeer_9()
		throws Exception {
		Map<Long, org.apache.zookeeper.server.quorum.QuorumPeer.QuorumServer> quorumPeers = new HashMap();
		File dataDir = new File("");
		File dataLogDir = new File("");
		int electionType = 1;
		long myid = 1L;
		int tickTime = 1;
		int initLimit = 1;
		int syncLimit = 1;
		ServerCnxnFactory cnxnFactory = new NIOServerCnxnFactory();
		QuorumVerifier quorumConfig = null;

		QuorumPeer result = new QuorumPeer(quorumPeers, dataDir, dataLogDir, electionType, myid, tickTime, initLimit, syncLimit, cnxnFactory, quorumConfig);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.<init>(QuorumPeer.java:396)
		assertNotNull(result);
	}

	/**
	 * Run the int countParticipants(Map<Long,QuorumServer>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testCountParticipants_1()
		throws Exception {
		Map<Long, org.apache.zookeeper.server.quorum.QuorumPeer.QuorumServer> peers = new HashMap();

		int result = QuorumPeer.countParticipants(peers);

		// add additional test code here
		assertEquals(0, result);
	}

	/**
	 * Run the int countParticipants(Map<Long,QuorumServer>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testCountParticipants_2()
		throws Exception {
		Map<Long, org.apache.zookeeper.server.quorum.QuorumPeer.QuorumServer> peers = new HashMap();

		int result = QuorumPeer.countParticipants(peers);

		// add additional test code here
		assertEquals(0, result);
	}

	/**
	 * Run the int countParticipants(Map<Long,QuorumServer>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testCountParticipants_3()
		throws Exception {
		Map<Long, org.apache.zookeeper.server.quorum.QuorumPeer.QuorumServer> peers = new HashMap();

		int result = QuorumPeer.countParticipants(peers);

		// add additional test code here
		assertEquals(0, result);
	}

	/**
	 * Run the Election createElectionAlgorithm(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testCreateElectionAlgorithm_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;
		int electionAlgorithm = 4;

		Election result = fixture.createElectionAlgorithm(electionAlgorithm);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertNotNull(result);
	}

	/**
	 * Run the Election createElectionAlgorithm(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testCreateElectionAlgorithm_2()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;
		int electionAlgorithm = 4;

		Election result = fixture.createElectionAlgorithm(electionAlgorithm);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertNotNull(result);
	}

	/**
	 * Run the Election createElectionAlgorithm(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testCreateElectionAlgorithm_3()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;
		int electionAlgorithm = 0;

		Election result = fixture.createElectionAlgorithm(electionAlgorithm);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertNotNull(result);
	}

	/**
	 * Run the Election createElectionAlgorithm(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testCreateElectionAlgorithm_4()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;
		int electionAlgorithm = 1;

		Election result = fixture.createElectionAlgorithm(electionAlgorithm);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertNotNull(result);
	}

	/**
	 * Run the Election createElectionAlgorithm(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testCreateElectionAlgorithm_5()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;
		int electionAlgorithm = 2;

		Election result = fixture.createElectionAlgorithm(electionAlgorithm);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertNotNull(result);
	}

	/**
	 * Run the Election createElectionAlgorithm(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testCreateElectionAlgorithm_6()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;
		int electionAlgorithm = 3;

		Election result = fixture.createElectionAlgorithm(electionAlgorithm);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertNotNull(result);
	}

	/**
	 * Run the Election createElectionAlgorithm(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testCreateElectionAlgorithm_7()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;
		int electionAlgorithm = 3;

		Election result = fixture.createElectionAlgorithm(electionAlgorithm);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertNotNull(result);
	}

	/**
	 * Run the long getAcceptedEpoch() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testGetAcceptedEpoch_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(-1);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		long result = fixture.getAcceptedEpoch();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertEquals(0L, result);
	}

	/**
	 * Run the long getAcceptedEpoch() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testGetAcceptedEpoch_2()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		long result = fixture.getAcceptedEpoch();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertEquals(0L, result);
	}

	/**
	 * Run the ZooKeeperServer getActiveServer() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testGetActiveServer_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		ZooKeeperServer result = fixture.getActiveServer();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertNotNull(result);
	}

	/**
	 * Run the ZooKeeperServer getActiveServer() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testGetActiveServer_2()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		ZooKeeperServer result = fixture.getActiveServer();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertNotNull(result);
	}

	/**
	 * Run the ZooKeeperServer getActiveServer() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testGetActiveServer_3()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		ZooKeeperServer result = fixture.getActiveServer();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertNotNull(result);
	}

	/**
	 * Run the ZooKeeperServer getActiveServer() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testGetActiveServer_4()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		ZooKeeperServer result = fixture.getActiveServer();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertNotNull(result);
	}

	/**
	 * Run the int getClientPort() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testGetClientPort_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		int result = fixture.getClientPort();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertEquals(0, result);
	}

	/**
	 * Run the ServerCnxnFactory getCnxnFactory() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testGetCnxnFactory_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		ServerCnxnFactory result = fixture.getCnxnFactory();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertNotNull(result);
	}

	/**
	 * Run the long getCurrentEpoch() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testGetCurrentEpoch_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(-1);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		long result = fixture.getCurrentEpoch();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertEquals(0L, result);
	}

	/**
	 * Run the long getCurrentEpoch() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testGetCurrentEpoch_2()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		long result = fixture.getCurrentEpoch();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertEquals(0L, result);
	}

	/**
	 * Run the Vote getCurrentVote() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testGetCurrentVote_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		Vote result = fixture.getCurrentVote();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertNotNull(result);
	}

	/**
	 * Run the Election getElectionAlg() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testGetElectionAlg_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		Election result = fixture.getElectionAlg();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertNotNull(result);
	}

	/**
	 * Run the int getElectionType() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testGetElectionType_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		int result = fixture.getElectionType();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertEquals(0, result);
	}

	/**
	 * Run the long getId() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testGetId_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		long result = fixture.getId();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertEquals(0L, result);
	}

	/**
	 * Run the int getInitLimit() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testGetInitLimit_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		int result = fixture.getInitLimit();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertEquals(0, result);
	}

	/**
	 * Run the long getLastLoggedZxid() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testGetLastLoggedZxid_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		long result = fixture.getLastLoggedZxid();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertEquals(0L, result);
	}

	/**
	 * Run the long getLastLoggedZxid() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testGetLastLoggedZxid_2()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		long result = fixture.getLastLoggedZxid();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertEquals(0L, result);
	}

	/**
	 * Run the org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType getLearnerType() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testGetLearnerType_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType result = fixture.getLearnerType();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertNotNull(result);
	}

	/**
	 * Run the int getMaxClientCnxnsPerHost() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testGetMaxClientCnxnsPerHost_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, (ServerCnxnFactory) null, new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		int result = fixture.getMaxClientCnxnsPerHost();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertEquals(0, result);
	}

	/**
	 * Run the int getMaxClientCnxnsPerHost() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testGetMaxClientCnxnsPerHost_2()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		int result = fixture.getMaxClientCnxnsPerHost();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertEquals(0, result);
	}

	/**
	 * Run the int getMaxSessionTimeout() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testGetMaxSessionTimeout_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		int result = fixture.getMaxSessionTimeout();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertEquals(0, result);
	}

	/**
	 * Run the int getMaxSessionTimeout() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testGetMaxSessionTimeout_2()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(-1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		int result = fixture.getMaxSessionTimeout();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertEquals(0, result);
	}

	/**
	 * Run the int getMinSessionTimeout() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testGetMinSessionTimeout_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		int result = fixture.getMinSessionTimeout();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertEquals(0, result);
	}

	/**
	 * Run the int getMinSessionTimeout() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testGetMinSessionTimeout_2()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(-1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		int result = fixture.getMinSessionTimeout();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertEquals(0, result);
	}

	/**
	 * Run the long getMyid() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testGetMyid_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		long result = fixture.getMyid();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertEquals(0L, result);
	}

	/**
	 * Run the Map<Long, org.apache.zookeeper.server.quorum.QuorumPeer.QuorumServer> getObservingView() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testGetObservingView_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		Map<Long, org.apache.zookeeper.server.quorum.QuorumPeer.QuorumServer> result = fixture.getObservingView();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertNotNull(result);
	}

	/**
	 * Run the Map<Long, org.apache.zookeeper.server.quorum.QuorumPeer.QuorumServer> getObservingView() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testGetObservingView_2()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		Map<Long, org.apache.zookeeper.server.quorum.QuorumPeer.QuorumServer> result = fixture.getObservingView();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertNotNull(result);
	}

	/**
	 * Run the Map<Long, org.apache.zookeeper.server.quorum.QuorumPeer.QuorumServer> getObservingView() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testGetObservingView_3()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		Map<Long, org.apache.zookeeper.server.quorum.QuorumPeer.QuorumServer> result = fixture.getObservingView();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertNotNull(result);
	}

	/**
	 * Run the org.apache.zookeeper.server.quorum.QuorumPeer.ServerState getPeerState() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testGetPeerState_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		org.apache.zookeeper.server.quorum.QuorumPeer.ServerState result = fixture.getPeerState();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertNotNull(result);
	}

	/**
	 * Run the InetSocketAddress getQuorumAddress() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testGetQuorumAddress_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		InetSocketAddress result = fixture.getQuorumAddress();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertNotNull(result);
	}

	/**
	 * Run the QuorumCnxManager getQuorumCnxManager() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testGetQuorumCnxManager_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		QuorumCnxManager result = fixture.getQuorumCnxManager();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertNotNull(result);
	}

	/**
	 * Run the String[] getQuorumPeers() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testGetQuorumPeers_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		String[] result = fixture.getQuorumPeers();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertNotNull(result);
	}

	/**
	 * Run the String[] getQuorumPeers() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testGetQuorumPeers_2()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		String[] result = fixture.getQuorumPeers();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertNotNull(result);
	}

	/**
	 * Run the String[] getQuorumPeers() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testGetQuorumPeers_3()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		String[] result = fixture.getQuorumPeers();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertNotNull(result);
	}

	/**
	 * Run the String[] getQuorumPeers() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testGetQuorumPeers_4()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		String[] result = fixture.getQuorumPeers();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertNotNull(result);
	}

	/**
	 * Run the String[] getQuorumPeers() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testGetQuorumPeers_5()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		String[] result = fixture.getQuorumPeers();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertNotNull(result);
	}

	/**
	 * Run the String[] getQuorumPeers() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testGetQuorumPeers_6()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		String[] result = fixture.getQuorumPeers();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertNotNull(result);
	}

	/**
	 * Run the int getQuorumSize() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testGetQuorumSize_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		int result = fixture.getQuorumSize();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertEquals(0, result);
	}

	/**
	 * Run the QuorumVerifier getQuorumVerifier() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testGetQuorumVerifier_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		QuorumVerifier result = fixture.getQuorumVerifier();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertNotNull(result);
	}

	/**
	 * Run the String getServerState() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testGetServerState_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		String result = fixture.getServerState();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertNotNull(result);
	}

	/**
	 * Run the String getServerState() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testGetServerState_2()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		String result = fixture.getServerState();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertNotNull(result);
	}

	/**
	 * Run the String getServerState() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testGetServerState_3()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		String result = fixture.getServerState();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertNotNull(result);
	}

	/**
	 * Run the String getServerState() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testGetServerState_4()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		String result = fixture.getServerState();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertNotNull(result);
	}

	/**
	 * Run the int getSyncLimit() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testGetSyncLimit_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		int result = fixture.getSyncLimit();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertEquals(0, result);
	}

	/**
	 * Run the int getTick() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testGetTick_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		int result = fixture.getTick();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertEquals(0, result);
	}

	/**
	 * Run the int getTickTime() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testGetTickTime_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		int result = fixture.getTickTime();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertEquals(0, result);
	}

	/**
	 * Run the FileTxnSnapLog getTxnFactory() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testGetTxnFactory_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		FileTxnSnapLog result = fixture.getTxnFactory();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertNotNull(result);
	}

	/**
	 * Run the Map<Long, org.apache.zookeeper.server.quorum.QuorumPeer.QuorumServer> getView() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testGetView_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		Map<Long, org.apache.zookeeper.server.quorum.QuorumPeer.QuorumServer> result = fixture.getView();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertNotNull(result);
	}

	/**
	 * Run the Map<Long, org.apache.zookeeper.server.quorum.QuorumPeer.QuorumServer> getVotingView() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testGetVotingView_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		Map<Long, org.apache.zookeeper.server.quorum.QuorumPeer.QuorumServer> result = fixture.getVotingView();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertNotNull(result);
	}

	/**
	 * Run the Map<Long, org.apache.zookeeper.server.quorum.QuorumPeer.QuorumServer> getVotingView() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testGetVotingView_2()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		Map<Long, org.apache.zookeeper.server.quorum.QuorumPeer.QuorumServer> result = fixture.getVotingView();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertNotNull(result);
	}

	/**
	 * Run the Map<Long, org.apache.zookeeper.server.quorum.QuorumPeer.QuorumServer> getVotingView() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testGetVotingView_3()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		Map<Long, org.apache.zookeeper.server.quorum.QuorumPeer.QuorumServer> result = fixture.getVotingView();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertNotNull(result);
	}

	/**
	 * Run the boolean isRunning() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testIsRunning_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		boolean result = fixture.isRunning();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertTrue(result);
	}

	/**
	 * Run the boolean isRunning() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testIsRunning_2()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(false);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		boolean result = fixture.isRunning();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertTrue(result);
	}

	/**
	 * Run the Follower makeFollower(FileTxnSnapLog) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testMakeFollower_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;
		FileTxnSnapLog logFactory = new FileTxnSnapLog(new File(""), new File(""));

		Follower result = fixture.makeFollower(logFactory);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertNotNull(result);
	}

	/**
	 * Run the Follower makeFollower(FileTxnSnapLog) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testMakeFollower_2()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;
		FileTxnSnapLog logFactory = new FileTxnSnapLog(new File(""), new File(""));

		Follower result = fixture.makeFollower(logFactory);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertNotNull(result);
	}

	/**
	 * Run the Election makeLEStrategy() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testMakeLEStrategy_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 0, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		Election result = fixture.makeLEStrategy();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertNotNull(result);
	}

	/**
	 * Run the Election makeLEStrategy() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testMakeLEStrategy_2()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		Election result = fixture.makeLEStrategy();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertNotNull(result);
	}

	/**
	 * Run the Leader makeLeader(FileTxnSnapLog) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testMakeLeader_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;
		FileTxnSnapLog logFactory = new FileTxnSnapLog(new File(""), new File(""));

		Leader result = fixture.makeLeader(logFactory);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertNotNull(result);
	}

	/**
	 * Run the Leader makeLeader(FileTxnSnapLog) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testMakeLeader_2()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;
		FileTxnSnapLog logFactory = new FileTxnSnapLog(new File(""), new File(""));

		Leader result = fixture.makeLeader(logFactory);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertNotNull(result);
	}

	/**
	 * Run the Leader makeLeader(FileTxnSnapLog) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testMakeLeader_3()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;
		FileTxnSnapLog logFactory = new FileTxnSnapLog(new File(""), new File(""));

		Leader result = fixture.makeLeader(logFactory);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertNotNull(result);
	}

	/**
	 * Run the Observer makeObserver(FileTxnSnapLog) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testMakeObserver_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;
		FileTxnSnapLog logFactory = new FileTxnSnapLog(new File(""), new File(""));

		Observer result = fixture.makeObserver(logFactory);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertNotNull(result);
	}

	/**
	 * Run the Observer makeObserver(FileTxnSnapLog) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testMakeObserver_2()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;
		FileTxnSnapLog logFactory = new FileTxnSnapLog(new File(""), new File(""));

		Observer result = fixture.makeObserver(logFactory);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertNotNull(result);
	}

	/**
	 * Run the QuorumStats quorumStats() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testQuorumStats_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		QuorumStats result = fixture.quorumStats();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertNotNull(result);
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testRun_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(false);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testRun_2()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(false);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testRun_3()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(false);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testRun_4()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(false);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testRun_5()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(false);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testRun_6()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(false);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
	}

	/**
	 * Run the void setAcceptedEpoch(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testSetAcceptedEpoch_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;
		long e = 1L;

		fixture.setAcceptedEpoch(e);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
	}

	/**
	 * Run the void setAcceptedEpoch(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testSetAcceptedEpoch_2()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;
		long e = 1L;

		fixture.setAcceptedEpoch(e);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
	}

	/**
	 * Run the void setClientPortAddress(InetSocketAddress) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testSetClientPortAddress_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;
		InetSocketAddress addr = new InetSocketAddress(1);

		fixture.setClientPortAddress(addr);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
	}

	/**
	 * Run the void setCnxnFactory(ServerCnxnFactory) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testSetCnxnFactory_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;
		ServerCnxnFactory cnxnFactory = new NIOServerCnxnFactory();

		fixture.setCnxnFactory(cnxnFactory);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
	}

	/**
	 * Run the void setCurrentEpoch(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testSetCurrentEpoch_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;
		long e = 1L;

		fixture.setCurrentEpoch(e);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
	}

	/**
	 * Run the void setCurrentEpoch(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testSetCurrentEpoch_2()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;
		long e = 1L;

		fixture.setCurrentEpoch(e);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
	}

	/**
	 * Run the void setCurrentVote(Vote) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testSetCurrentVote_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;
		Vote v = new Vote(1L, 1L);

		fixture.setCurrentVote(v);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
	}

	/**
	 * Run the void setElectionType(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testSetElectionType_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;
		int electionType = 1;

		fixture.setElectionType(electionType);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
	}

	/**
	 * Run the void setFollower(Follower) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testSetFollower_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;
		Follower newFollower = null;

		fixture.setFollower(newFollower);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
	}

	/**
	 * Run the void setInitLimit(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testSetInitLimit_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;
		int initLimit = 1;

		fixture.setInitLimit(initLimit);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
	}

	/**
	 * Run the void setLeader(Leader) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testSetLeader_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;
		Leader newLeader = null;

		fixture.setLeader(newLeader);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
	}

	/**
	 * Run the void setLearnerType(LearnerType) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testSetLearnerType_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;
		org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType p = org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER;

		fixture.setLearnerType(p);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
	}

	/**
	 * Run the void setLearnerType(LearnerType) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testSetLearnerType_2()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;
		org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType p = org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER;

		fixture.setLearnerType(p);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
	}

	/**
	 * Run the void setMaxSessionTimeout(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testSetMaxSessionTimeout_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;
		int max = 1;

		fixture.setMaxSessionTimeout(max);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
	}

	/**
	 * Run the void setMinSessionTimeout(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testSetMinSessionTimeout_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;
		int min = 1;

		fixture.setMinSessionTimeout(min);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
	}

	/**
	 * Run the void setMyid(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testSetMyid_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;
		long myid = 1L;

		fixture.setMyid(myid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
	}

	/**
	 * Run the void setObserver(Observer) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testSetObserver_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;
		Observer newObserver = null;

		fixture.setObserver(newObserver);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
	}

	/**
	 * Run the void setPeerState(ServerState) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testSetPeerState_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;
		org.apache.zookeeper.server.quorum.QuorumPeer.ServerState newState = org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING;

		fixture.setPeerState(newState);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
	}

	/**
	 * Run the void setQuorumPeers(Map<Long,QuorumServer>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testSetQuorumPeers_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;
		Map<Long, org.apache.zookeeper.server.quorum.QuorumPeer.QuorumServer> quorumPeers = new HashMap();

		fixture.setQuorumPeers(quorumPeers);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
	}

	/**
	 * Run the void setQuorumVerifier(QuorumVerifier) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testSetQuorumVerifier_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;
		QuorumVerifier quorumConfig = new QuorumHierarchical("");

		fixture.setQuorumVerifier(quorumConfig);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
	}

	/**
	 * Run the void setRunning(boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testSetRunning_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;
		boolean running = true;

		fixture.setRunning(running);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
	}

	/**
	 * Run the void setSyncLimit(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testSetSyncLimit_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;
		int syncLimit = 1;

		fixture.setSyncLimit(syncLimit);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
	}

	/**
	 * Run the void setTickTime(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testSetTickTime_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;
		int tickTime = 1;

		fixture.setTickTime(tickTime);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
	}

	/**
	 * Run the void setTxnFactory(FileTxnSnapLog) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testSetTxnFactory_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;
		FileTxnSnapLog factory = new FileTxnSnapLog(new File(""), new File(""));

		fixture.setTxnFactory(factory);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
	}

	/**
	 * Run the void setZKDatabase(ZKDatabase) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testSetZKDatabase_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;
		ZKDatabase database = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));

		fixture.setZKDatabase(database);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
	}

	/**
	 * Run the void shutdown() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testShutdown_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		fixture.shutdown();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
	}

	/**
	 * Run the void shutdown() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testShutdown_2()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		fixture.shutdown();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
	}

	/**
	 * Run the void shutdown() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testShutdown_3()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		fixture.shutdown();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
	}

	/**
	 * Run the void shutdown() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testShutdown_4()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		fixture.shutdown();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
	}

	/**
	 * Run the void shutdown() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testShutdown_5()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		fixture.shutdown();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
	}

	/**
	 * Run the void shutdown() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testShutdown_6()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		fixture.shutdown();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
	}

	/**
	 * Run the void shutdown() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testShutdown_7()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		fixture.shutdown();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
	}

	/**
	 * Run the void shutdown() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testShutdown_8()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		fixture.shutdown();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
	}

	/**
	 * Run the void shutdown() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testShutdown_9()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		fixture.shutdown();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
	}

	/**
	 * Run the void shutdown() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testShutdown_10()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		fixture.shutdown();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
	}

	/**
	 * Run the void shutdown() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testShutdown_11()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		fixture.shutdown();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
	}

	/**
	 * Run the void shutdown() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testShutdown_12()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		fixture.shutdown();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
	}

	/**
	 * Run the void shutdown() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testShutdown_13()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		fixture.shutdown();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
	}

	/**
	 * Run the void shutdown() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testShutdown_14()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		fixture.shutdown();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
	}

	/**
	 * Run the void shutdown() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testShutdown_15()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		fixture.shutdown();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
	}

	/**
	 * Run the void shutdown() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testShutdown_16()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		fixture.shutdown();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
	}

	/**
	 * Run the void start() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testStart_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		fixture.start();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
	}

	/**
	 * Run the void startLeaderElection() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testStartLeaderElection_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		fixture.startLeaderElection();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
	}

	/**
	 * Run the void startLeaderElection() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testStartLeaderElection_2()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		fixture.startLeaderElection();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
	}

	/**
	 * Run the void startLeaderElection() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testStartLeaderElection_3()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 0, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		fixture.startLeaderElection();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
	}

	/**
	 * Run the void startLeaderElection() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testStartLeaderElection_4()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 0, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		fixture.startLeaderElection();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
	}

	/**
	 * Run the void startLeaderElection() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testStartLeaderElection_5()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		fixture.startLeaderElection();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
	}

	/**
	 * Run the void startLeaderElection() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testStartLeaderElection_6()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		fixture.startLeaderElection();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
	}

	/**
	 * Run the void stopLeaderElection() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testStopLeaderElection_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;

		fixture.stopLeaderElection();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
	}

	/**
	 * Run the boolean viewContains(Long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testViewContains_1()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;
		Long sid = new Long(1L);

		boolean result = fixture.viewContains(sid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertTrue(result);
	}

	/**
	 * Run the boolean viewContains(Long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testViewContains_2()
		throws Exception {
		QuorumPeer fixture = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		fixture.setZKDatabase(new ZKDatabase(new FileTxnSnapLog(new File(""), new File(""))));
		fixture.setMaxSessionTimeout(1);
		fixture.setCurrentVote(new Vote(1L, 1L));
		fixture.setMinSessionTimeout(1);
		fixture.setTxnFactory(new FileTxnSnapLog(new File(""), new File("")));
		fixture.setPeerState(org.apache.zookeeper.server.quorum.QuorumPeer.ServerState.FOLLOWING);
		fixture.setLearnerType(org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType.OBSERVER);
		fixture.setAcceptedEpoch(1L);
		fixture.setRunning(true);
		fixture.setQuorumVerifier(new QuorumHierarchical(""));
		fixture.setCurrentEpoch(1L);
		fixture.observer = null;
		fixture.start_fle = 1L;
		fixture.follower = null;
		fixture.leader = null;
		fixture.end_fle = 1L;
		Long sid = new Long(1L);

		boolean result = fixture.viewContains(sid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertTrue(result);
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(QuorumPeerTest.class);
	}
}