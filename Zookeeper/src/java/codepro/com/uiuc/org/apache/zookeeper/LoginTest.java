package com.uiuc.org.apache.zookeeper;

import javax.security.auth.Subject;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.login.Configuration;
import org.apache.zookeeper.JaasConfiguration;
import org.apache.zookeeper.Login;
import org.apache.zookeeper.server.auth.SaslServerCallbackHandler;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>LoginTest</code> contains tests for the class <code>{@link Login}</code>.
 *
 * @generatedBy CodePro at 10/16/13 7:23 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class LoginTest {
	/**
	 * Run the Login(String,CallbackHandler) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testLogin_1()
		throws Exception {
		String loginContextName = "";
		CallbackHandler callbackHandler = new SaslServerCallbackHandler(new JaasConfiguration());

		Login result = new Login(loginContextName, callbackHandler);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.io.IOException: Could not find a 'Server' entry in this configuration: Server cannot start.
		//       at org.apache.zookeeper.server.auth.SaslServerCallbackHandler.<init>(SaslServerCallbackHandler.java:57)
		assertNotNull(result);
	}

	/**
	 * Run the Login(String,CallbackHandler) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testLogin_2()
		throws Exception {
		String loginContextName = "";
		CallbackHandler callbackHandler = new SaslServerCallbackHandler(new JaasConfiguration());

		Login result = new Login(loginContextName, callbackHandler);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.io.IOException: Could not find a 'Server' entry in this configuration: Server cannot start.
		//       at org.apache.zookeeper.server.auth.SaslServerCallbackHandler.<init>(SaslServerCallbackHandler.java:57)
		assertNotNull(result);
	}

	/**
	 * Run the Login(String,CallbackHandler) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testLogin_3()
		throws Exception {
		String loginContextName = "";
		CallbackHandler callbackHandler = new SaslServerCallbackHandler(new JaasConfiguration());

		Login result = new Login(loginContextName, callbackHandler);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.io.IOException: Could not find a 'Server' entry in this configuration: Server cannot start.
		//       at org.apache.zookeeper.server.auth.SaslServerCallbackHandler.<init>(SaslServerCallbackHandler.java:57)
		assertNotNull(result);
	}

	/**
	 * Run the Login(String,CallbackHandler) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testLogin_4()
		throws Exception {
		String loginContextName = "";
		CallbackHandler callbackHandler = new SaslServerCallbackHandler(new JaasConfiguration());

		Login result = new Login(loginContextName, callbackHandler);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.io.IOException: Could not find a 'Server' entry in this configuration: Server cannot start.
		//       at org.apache.zookeeper.server.auth.SaslServerCallbackHandler.<init>(SaslServerCallbackHandler.java:57)
		assertNotNull(result);
	}

	/**
	 * Run the Login(String,CallbackHandler) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testLogin_5()
		throws Exception {
		String loginContextName = "";
		CallbackHandler callbackHandler = new SaslServerCallbackHandler(new JaasConfiguration());

		Login result = new Login(loginContextName, callbackHandler);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.io.IOException: Could not find a 'Server' entry in this configuration: Server cannot start.
		//       at org.apache.zookeeper.server.auth.SaslServerCallbackHandler.<init>(SaslServerCallbackHandler.java:57)
		assertNotNull(result);
	}

	/**
	 * Run the Login(String,CallbackHandler) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testLogin_6()
		throws Exception {
		String loginContextName = "";
		CallbackHandler callbackHandler = new SaslServerCallbackHandler(new JaasConfiguration());

		Login result = new Login(loginContextName, callbackHandler);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.io.IOException: Could not find a 'Server' entry in this configuration: Server cannot start.
		//       at org.apache.zookeeper.server.auth.SaslServerCallbackHandler.<init>(SaslServerCallbackHandler.java:57)
		assertNotNull(result);
	}

	/**
	 * Run the Login(String,CallbackHandler) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testLogin_7()
		throws Exception {
		String loginContextName = "";
		CallbackHandler callbackHandler = new SaslServerCallbackHandler(new JaasConfiguration());

		Login result = new Login(loginContextName, callbackHandler);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.io.IOException: Could not find a 'Server' entry in this configuration: Server cannot start.
		//       at org.apache.zookeeper.server.auth.SaslServerCallbackHandler.<init>(SaslServerCallbackHandler.java:57)
		assertNotNull(result);
	}

	/**
	 * Run the Login(String,CallbackHandler) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testLogin_8()
		throws Exception {
		String loginContextName = "";
		CallbackHandler callbackHandler = new SaslServerCallbackHandler(new JaasConfiguration());

		Login result = new Login(loginContextName, callbackHandler);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.io.IOException: Could not find a 'Server' entry in this configuration: Server cannot start.
		//       at org.apache.zookeeper.server.auth.SaslServerCallbackHandler.<init>(SaslServerCallbackHandler.java:57)
		assertNotNull(result);
	}

	/**
	 * Run the Login(String,CallbackHandler) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testLogin_9()
		throws Exception {
		String loginContextName = "";
		CallbackHandler callbackHandler = new SaslServerCallbackHandler(new JaasConfiguration());

		Login result = new Login(loginContextName, callbackHandler);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.io.IOException: Could not find a 'Server' entry in this configuration: Server cannot start.
		//       at org.apache.zookeeper.server.auth.SaslServerCallbackHandler.<init>(SaslServerCallbackHandler.java:57)
		assertNotNull(result);
	}

	/**
	 * Run the Login(String,CallbackHandler) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testLogin_10()
		throws Exception {
		String loginContextName = "";
		CallbackHandler callbackHandler = new SaslServerCallbackHandler(new JaasConfiguration());

		Login result = new Login(loginContextName, callbackHandler);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.io.IOException: Could not find a 'Server' entry in this configuration: Server cannot start.
		//       at org.apache.zookeeper.server.auth.SaslServerCallbackHandler.<init>(SaslServerCallbackHandler.java:57)
		assertNotNull(result);
	}

	/**
	 * Run the Login(String,CallbackHandler) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testLogin_11()
		throws Exception {
		String loginContextName = "";
		CallbackHandler callbackHandler = new SaslServerCallbackHandler(new JaasConfiguration());

		Login result = new Login(loginContextName, callbackHandler);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.io.IOException: Could not find a 'Server' entry in this configuration: Server cannot start.
		//       at org.apache.zookeeper.server.auth.SaslServerCallbackHandler.<init>(SaslServerCallbackHandler.java:57)
		assertNotNull(result);
	}

	/**
	 * Run the Login(String,CallbackHandler) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testLogin_12()
		throws Exception {
		String loginContextName = "";
		CallbackHandler callbackHandler = new SaslServerCallbackHandler(new JaasConfiguration());

		Login result = new Login(loginContextName, callbackHandler);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.io.IOException: Could not find a 'Server' entry in this configuration: Server cannot start.
		//       at org.apache.zookeeper.server.auth.SaslServerCallbackHandler.<init>(SaslServerCallbackHandler.java:57)
		assertNotNull(result);
	}

	/**
	 * Run the Login(String,CallbackHandler) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testLogin_13()
		throws Exception {
		String loginContextName = "";
		CallbackHandler callbackHandler = new SaslServerCallbackHandler(new JaasConfiguration());

		Login result = new Login(loginContextName, callbackHandler);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.io.IOException: Could not find a 'Server' entry in this configuration: Server cannot start.
		//       at org.apache.zookeeper.server.auth.SaslServerCallbackHandler.<init>(SaslServerCallbackHandler.java:57)
		assertNotNull(result);
	}

	/**
	 * Run the Login(String,CallbackHandler) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testLogin_14()
		throws Exception {
		String loginContextName = "";
		CallbackHandler callbackHandler = new SaslServerCallbackHandler(new JaasConfiguration());

		Login result = new Login(loginContextName, callbackHandler);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.io.IOException: Could not find a 'Server' entry in this configuration: Server cannot start.
		//       at org.apache.zookeeper.server.auth.SaslServerCallbackHandler.<init>(SaslServerCallbackHandler.java:57)
		assertNotNull(result);
	}

	/**
	 * Run the Login(String,CallbackHandler) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testLogin_15()
		throws Exception {
		String loginContextName = "";
		CallbackHandler callbackHandler = new SaslServerCallbackHandler(new JaasConfiguration());

		Login result = new Login(loginContextName, callbackHandler);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.io.IOException: Could not find a 'Server' entry in this configuration: Server cannot start.
		//       at org.apache.zookeeper.server.auth.SaslServerCallbackHandler.<init>(SaslServerCallbackHandler.java:57)
		assertNotNull(result);
	}

	/**
	 * Run the String getLoginContextName() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testGetLoginContextName_1()
		throws Exception {
		Login fixture = new Login("", new SaslServerCallbackHandler(new JaasConfiguration()));

		String result = fixture.getLoginContextName();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.io.IOException: Could not find a 'Server' entry in this configuration: Server cannot start.
		//       at org.apache.zookeeper.server.auth.SaslServerCallbackHandler.<init>(SaslServerCallbackHandler.java:57)
		assertNotNull(result);
	}

	/**
	 * Run the Subject getSubject() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testGetSubject_1()
		throws Exception {
		Login fixture = new Login("", new SaslServerCallbackHandler(new JaasConfiguration()));

		Subject result = fixture.getSubject();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.io.IOException: Could not find a 'Server' entry in this configuration: Server cannot start.
		//       at org.apache.zookeeper.server.auth.SaslServerCallbackHandler.<init>(SaslServerCallbackHandler.java:57)
		assertNotNull(result);
	}

	/**
	 * Run the void shutdown() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testShutdown_1()
		throws Exception {
		Login fixture = new Login("", new SaslServerCallbackHandler(new JaasConfiguration()));

		fixture.shutdown();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.io.IOException: Could not find a 'Server' entry in this configuration: Server cannot start.
		//       at org.apache.zookeeper.server.auth.SaslServerCallbackHandler.<init>(SaslServerCallbackHandler.java:57)
	}

	/**
	 * Run the void shutdown() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testShutdown_2()
		throws Exception {
		Login fixture = new Login("", new SaslServerCallbackHandler(new JaasConfiguration()));

		fixture.shutdown();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.io.IOException: Could not find a 'Server' entry in this configuration: Server cannot start.
		//       at org.apache.zookeeper.server.auth.SaslServerCallbackHandler.<init>(SaslServerCallbackHandler.java:57)
	}

	/**
	 * Run the void shutdown() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testShutdown_3()
		throws Exception {
		Login fixture = new Login("", new SaslServerCallbackHandler(new JaasConfiguration()));

		fixture.shutdown();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.io.IOException: Could not find a 'Server' entry in this configuration: Server cannot start.
		//       at org.apache.zookeeper.server.auth.SaslServerCallbackHandler.<init>(SaslServerCallbackHandler.java:57)
	}

	/**
	 * Run the void shutdown() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testShutdown_4()
		throws Exception {
		Login fixture = new Login("", new SaslServerCallbackHandler(new JaasConfiguration()));

		fixture.shutdown();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.io.IOException: Could not find a 'Server' entry in this configuration: Server cannot start.
		//       at org.apache.zookeeper.server.auth.SaslServerCallbackHandler.<init>(SaslServerCallbackHandler.java:57)
	}

	/**
	 * Run the void startThreadIfNeeded() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testStartThreadIfNeeded_1()
		throws Exception {
		Login fixture = new Login("", new SaslServerCallbackHandler(new JaasConfiguration()));

		fixture.startThreadIfNeeded();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.io.IOException: Could not find a 'Server' entry in this configuration: Server cannot start.
		//       at org.apache.zookeeper.server.auth.SaslServerCallbackHandler.<init>(SaslServerCallbackHandler.java:57)
	}

	/**
	 * Run the void startThreadIfNeeded() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testStartThreadIfNeeded_2()
		throws Exception {
		Login fixture = new Login("", new SaslServerCallbackHandler(new JaasConfiguration()));

		fixture.startThreadIfNeeded();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.io.IOException: Could not find a 'Server' entry in this configuration: Server cannot start.
		//       at org.apache.zookeeper.server.auth.SaslServerCallbackHandler.<init>(SaslServerCallbackHandler.java:57)
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(LoginTest.class);
	}
}