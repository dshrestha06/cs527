package com.uiuc.org.apache.zookeeper.server;

import java.io.FileNotFoundException;
import org.apache.zookeeper.server.TraceFormatter;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>TraceFormatterTest</code> contains tests for the class <code>{@link TraceFormatter}</code>.
 *
 * @generatedBy CodePro at 10/16/13 8:24 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class TraceFormatterTest {
	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testMain_1()
		throws Exception {
		String[] args = new String[] {"", null};

		TraceFormatter.main(args);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Exit while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkExit(CodeProJUnitSecurityManager.java:57)
		//       at java.lang.Runtime.exit(Runtime.java:88)
		//       at java.lang.System.exit(System.java:904)
		//       at org.apache.zookeeper.server.TraceFormatter.main(TraceFormatter.java:76)
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testMain_2()
		throws Exception {
		String[] args = new String[] {"", null};

		TraceFormatter.main(args);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Exit while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkExit(CodeProJUnitSecurityManager.java:57)
		//       at java.lang.Runtime.exit(Runtime.java:88)
		//       at java.lang.System.exit(System.java:904)
		//       at org.apache.zookeeper.server.TraceFormatter.main(TraceFormatter.java:76)
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testMain_3()
		throws Exception {
		String[] args = new String[] {"", null};

		TraceFormatter.main(args);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Exit while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkExit(CodeProJUnitSecurityManager.java:57)
		//       at java.lang.Runtime.exit(Runtime.java:88)
		//       at java.lang.System.exit(System.java:904)
		//       at org.apache.zookeeper.server.TraceFormatter.main(TraceFormatter.java:76)
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testMain_4()
		throws Exception {
		String[] args = new String[] {"", null};

		TraceFormatter.main(args);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Exit while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkExit(CodeProJUnitSecurityManager.java:57)
		//       at java.lang.Runtime.exit(Runtime.java:88)
		//       at java.lang.System.exit(System.java:904)
		//       at org.apache.zookeeper.server.TraceFormatter.main(TraceFormatter.java:76)
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testMain_5()
		throws Exception {
		String[] args = new String[] {"", null};

		TraceFormatter.main(args);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Exit while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkExit(CodeProJUnitSecurityManager.java:57)
		//       at java.lang.Runtime.exit(Runtime.java:88)
		//       at java.lang.System.exit(System.java:904)
		//       at org.apache.zookeeper.server.TraceFormatter.main(TraceFormatter.java:76)
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testMain_6()
		throws Exception {
		String[] args = new String[] {"", null};

		TraceFormatter.main(args);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Exit while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkExit(CodeProJUnitSecurityManager.java:57)
		//       at java.lang.Runtime.exit(Runtime.java:88)
		//       at java.lang.System.exit(System.java:904)
		//       at org.apache.zookeeper.server.TraceFormatter.main(TraceFormatter.java:76)
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testMain_7()
		throws Exception {
		String[] args = new String[] {"", null};

		TraceFormatter.main(args);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Exit while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkExit(CodeProJUnitSecurityManager.java:57)
		//       at java.lang.Runtime.exit(Runtime.java:88)
		//       at java.lang.System.exit(System.java:904)
		//       at org.apache.zookeeper.server.TraceFormatter.main(TraceFormatter.java:76)
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test(expected = java.io.FileNotFoundException.class)
	public void testMain_8()
		throws Exception {
		String[] args = new String[] {""};

		TraceFormatter.main(args);

		// add additional test code here
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test(expected = java.io.FileNotFoundException.class)
	public void testMain_9()
		throws Exception {
		String[] args = new String[] {""};

		TraceFormatter.main(args);

		// add additional test code here
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test(expected = java.io.FileNotFoundException.class)
	public void testMain_10()
		throws Exception {
		String[] args = new String[] {""};

		TraceFormatter.main(args);

		// add additional test code here
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test(expected = java.io.FileNotFoundException.class)
	public void testMain_11()
		throws Exception {
		String[] args = new String[] {""};

		TraceFormatter.main(args);

		// add additional test code here
	}

	/**
	 * Run the String op2String(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testOp2String_1()
		throws Exception {
		int op = 0;

		String result = TraceFormatter.op2String(op);

		// add additional test code here
		assertEquals("notification", result);
	}

	/**
	 * Run the String op2String(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testOp2String_2()
		throws Exception {
		int op = 1;

		String result = TraceFormatter.op2String(op);

		// add additional test code here
		assertEquals("create", result);
	}

	/**
	 * Run the String op2String(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testOp2String_3()
		throws Exception {
		int op = 2;

		String result = TraceFormatter.op2String(op);

		// add additional test code here
		assertEquals("delete", result);
	}

	/**
	 * Run the String op2String(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testOp2String_4()
		throws Exception {
		int op = 3;

		String result = TraceFormatter.op2String(op);

		// add additional test code here
		assertEquals("exists", result);
	}

	/**
	 * Run the String op2String(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testOp2String_5()
		throws Exception {
		int op = 4;

		String result = TraceFormatter.op2String(op);

		// add additional test code here
		assertEquals("getDate", result);
	}

	/**
	 * Run the String op2String(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testOp2String_6()
		throws Exception {
		int op = 5;

		String result = TraceFormatter.op2String(op);

		// add additional test code here
		assertEquals("setData", result);
	}

	/**
	 * Run the String op2String(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testOp2String_7()
		throws Exception {
		int op = 14;

		String result = TraceFormatter.op2String(op);

		// add additional test code here
		assertEquals("multi", result);
	}

	/**
	 * Run the String op2String(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testOp2String_8()
		throws Exception {
		int op = 6;

		String result = TraceFormatter.op2String(op);

		// add additional test code here
		assertEquals("getACL", result);
	}

	/**
	 * Run the String op2String(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testOp2String_9()
		throws Exception {
		int op = 7;

		String result = TraceFormatter.op2String(op);

		// add additional test code here
		assertEquals("setACL", result);
	}

	/**
	 * Run the String op2String(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testOp2String_10()
		throws Exception {
		int op = 8;

		String result = TraceFormatter.op2String(op);

		// add additional test code here
		assertEquals("getChildren", result);
	}

	/**
	 * Run the String op2String(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testOp2String_11()
		throws Exception {
		int op = 12;

		String result = TraceFormatter.op2String(op);

		// add additional test code here
		assertEquals("getChildren2", result);
	}

	/**
	 * Run the String op2String(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testOp2String_12()
		throws Exception {
		int op = 11;

		String result = TraceFormatter.op2String(op);

		// add additional test code here
		assertEquals("ping", result);
	}

	/**
	 * Run the String op2String(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testOp2String_13()
		throws Exception {
		int op = -10;

		String result = TraceFormatter.op2String(op);

		// add additional test code here
		assertEquals("createSession", result);
	}

	/**
	 * Run the String op2String(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testOp2String_14()
		throws Exception {
		int op = -11;

		String result = TraceFormatter.op2String(op);

		// add additional test code here
		assertEquals("closeSession", result);
	}

	/**
	 * Run the String op2String(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testOp2String_15()
		throws Exception {
		int op = -1;

		String result = TraceFormatter.op2String(op);

		// add additional test code here
		assertEquals("error", result);
	}

	/**
	 * Run the String op2String(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testOp2String_16()
		throws Exception {
		int op = -2;

		String result = TraceFormatter.op2String(op);

		// add additional test code here
		assertEquals("unknown -2", result);
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(TraceFormatterTest.class);
	}
}