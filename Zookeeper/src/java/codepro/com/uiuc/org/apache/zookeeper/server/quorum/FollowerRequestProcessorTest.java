package com.uiuc.org.apache.zookeeper.server.quorum;

import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.LinkedList;
import java.util.List;
import org.apache.zookeeper.data.Id;
import org.apache.zookeeper.server.FinalRequestProcessor;
import org.apache.zookeeper.server.NIOServerCnxn;
import org.apache.zookeeper.server.NIOServerCnxnFactory;
import org.apache.zookeeper.server.Request;
import org.apache.zookeeper.server.RequestProcessor;
import org.apache.zookeeper.server.ServerCnxn;
import org.apache.zookeeper.server.ZooKeeperServer;
import org.apache.zookeeper.server.quorum.FollowerRequestProcessor;
import org.apache.zookeeper.server.quorum.FollowerZooKeeperServer;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>FollowerRequestProcessorTest</code> contains tests for the class <code>{@link FollowerRequestProcessor}</code>.
 *
 * @generatedBy CodePro at 10/16/13 7:57 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class FollowerRequestProcessorTest {
	/**
	 * Run the FollowerRequestProcessor(FollowerZooKeeperServer,RequestProcessor) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:57 PM
	 */
	@Test
	public void testFollowerRequestProcessor_1()
		throws Exception {
		FollowerZooKeeperServer zks = null;
		RequestProcessor nextProcessor = new FinalRequestProcessor(new ZooKeeperServer());

		FollowerRequestProcessor result = new FollowerRequestProcessor(zks, nextProcessor);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.FollowerRequestProcessor.<init>(FollowerRequestProcessor.java:49)
		assertNotNull(result);
	}

	/**
	 * Run the void processRequest(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:57 PM
	 */
	@Test
	public void testProcessRequest_1()
		throws Exception {
		FollowerRequestProcessor fixture = new FollowerRequestProcessor((FollowerZooKeeperServer) null, new FinalRequestProcessor(new ZooKeeperServer()));
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());

		fixture.processRequest(request);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.FollowerRequestProcessor.<init>(FollowerRequestProcessor.java:49)
	}

	/**
	 * Run the void processRequest(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:57 PM
	 */
	@Test
	public void testProcessRequest_2()
		throws Exception {
		FollowerRequestProcessor fixture = new FollowerRequestProcessor((FollowerZooKeeperServer) null, new FinalRequestProcessor(new ZooKeeperServer()));
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());

		fixture.processRequest(request);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.FollowerRequestProcessor.<init>(FollowerRequestProcessor.java:49)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:57 PM
	 */
	@Test
	public void testRun_1()
		throws Exception {
		FollowerRequestProcessor fixture = new FollowerRequestProcessor((FollowerZooKeeperServer) null, new FinalRequestProcessor(new ZooKeeperServer()));

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.FollowerRequestProcessor.<init>(FollowerRequestProcessor.java:49)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:57 PM
	 */
	@Test
	public void testRun_2()
		throws Exception {
		FollowerRequestProcessor fixture = new FollowerRequestProcessor((FollowerZooKeeperServer) null, new FinalRequestProcessor(new ZooKeeperServer()));

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.FollowerRequestProcessor.<init>(FollowerRequestProcessor.java:49)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:57 PM
	 */
	@Test
	public void testRun_3()
		throws Exception {
		FollowerRequestProcessor fixture = new FollowerRequestProcessor((FollowerZooKeeperServer) null, new FinalRequestProcessor(new ZooKeeperServer()));

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.FollowerRequestProcessor.<init>(FollowerRequestProcessor.java:49)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:57 PM
	 */
	@Test
	public void testRun_4()
		throws Exception {
		FollowerRequestProcessor fixture = new FollowerRequestProcessor((FollowerZooKeeperServer) null, new FinalRequestProcessor(new ZooKeeperServer()));

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.FollowerRequestProcessor.<init>(FollowerRequestProcessor.java:49)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:57 PM
	 */
	@Test
	public void testRun_5()
		throws Exception {
		FollowerRequestProcessor fixture = new FollowerRequestProcessor((FollowerZooKeeperServer) null, new FinalRequestProcessor(new ZooKeeperServer()));

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.FollowerRequestProcessor.<init>(FollowerRequestProcessor.java:49)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:57 PM
	 */
	@Test
	public void testRun_6()
		throws Exception {
		FollowerRequestProcessor fixture = new FollowerRequestProcessor((FollowerZooKeeperServer) null, new FinalRequestProcessor(new ZooKeeperServer()));

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.FollowerRequestProcessor.<init>(FollowerRequestProcessor.java:49)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:57 PM
	 */
	@Test
	public void testRun_7()
		throws Exception {
		FollowerRequestProcessor fixture = new FollowerRequestProcessor((FollowerZooKeeperServer) null, new FinalRequestProcessor(new ZooKeeperServer()));

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.FollowerRequestProcessor.<init>(FollowerRequestProcessor.java:49)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:57 PM
	 */
	@Test
	public void testRun_8()
		throws Exception {
		FollowerRequestProcessor fixture = new FollowerRequestProcessor((FollowerZooKeeperServer) null, new FinalRequestProcessor(new ZooKeeperServer()));

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.FollowerRequestProcessor.<init>(FollowerRequestProcessor.java:49)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:57 PM
	 */
	@Test
	public void testRun_9()
		throws Exception {
		FollowerRequestProcessor fixture = new FollowerRequestProcessor((FollowerZooKeeperServer) null, new FinalRequestProcessor(new ZooKeeperServer()));

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.FollowerRequestProcessor.<init>(FollowerRequestProcessor.java:49)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:57 PM
	 */
	@Test
	public void testRun_10()
		throws Exception {
		FollowerRequestProcessor fixture = new FollowerRequestProcessor((FollowerZooKeeperServer) null, new FinalRequestProcessor(new ZooKeeperServer()));

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.FollowerRequestProcessor.<init>(FollowerRequestProcessor.java:49)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:57 PM
	 */
	@Test
	public void testRun_11()
		throws Exception {
		FollowerRequestProcessor fixture = new FollowerRequestProcessor((FollowerZooKeeperServer) null, new FinalRequestProcessor(new ZooKeeperServer()));

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.FollowerRequestProcessor.<init>(FollowerRequestProcessor.java:49)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:57 PM
	 */
	@Test
	public void testRun_12()
		throws Exception {
		FollowerRequestProcessor fixture = new FollowerRequestProcessor((FollowerZooKeeperServer) null, new FinalRequestProcessor(new ZooKeeperServer()));

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.FollowerRequestProcessor.<init>(FollowerRequestProcessor.java:49)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:57 PM
	 */
	@Test
	public void testRun_13()
		throws Exception {
		FollowerRequestProcessor fixture = new FollowerRequestProcessor((FollowerZooKeeperServer) null, new FinalRequestProcessor(new ZooKeeperServer()));

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.FollowerRequestProcessor.<init>(FollowerRequestProcessor.java:49)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:57 PM
	 */
	@Test
	public void testRun_14()
		throws Exception {
		FollowerRequestProcessor fixture = new FollowerRequestProcessor((FollowerZooKeeperServer) null, new FinalRequestProcessor(new ZooKeeperServer()));

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.FollowerRequestProcessor.<init>(FollowerRequestProcessor.java:49)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:57 PM
	 */
	@Test
	public void testRun_15()
		throws Exception {
		FollowerRequestProcessor fixture = new FollowerRequestProcessor((FollowerZooKeeperServer) null, new FinalRequestProcessor(new ZooKeeperServer()));

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.FollowerRequestProcessor.<init>(FollowerRequestProcessor.java:49)
	}

	/**
	 * Run the void shutdown() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:57 PM
	 */
	@Test
	public void testShutdown_1()
		throws Exception {
		FollowerRequestProcessor fixture = new FollowerRequestProcessor((FollowerZooKeeperServer) null, new FinalRequestProcessor(new ZooKeeperServer()));

		fixture.shutdown();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.FollowerRequestProcessor.<init>(FollowerRequestProcessor.java:49)
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:57 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:57 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:57 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(FollowerRequestProcessorTest.class);
	}
}