package com.uiuc.org.apache.zookeeper.common;

import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.PrintStream;
import org.apache.zookeeper.common.IOUtils;
import org.junit.*;
import static org.junit.Assert.*;
import org.slf4j.Logger;
import org.slf4j.helpers.NOPLogger;

/**
 * The class <code>IOUtilsTest</code> contains tests for the class <code>{@link IOUtils}</code>.
 *
 * @generatedBy CodePro at 10/16/13 7:32 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class IOUtilsTest {
	/**
	 * Run the void cleanup(Logger,Closeable[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test
	public void testCleanup_1()
		throws Exception {
		Logger log = NOPLogger.NOP_LOGGER;

		IOUtils.cleanup(log);

		// add additional test code here
	}

	/**
	 * Run the void cleanup(Logger,Closeable[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test
	public void testCleanup_2()
		throws Exception {
		Logger log = NOPLogger.NOP_LOGGER;

		IOUtils.cleanup(log);

		// add additional test code here
	}

	/**
	 * Run the void cleanup(Logger,Closeable[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test
	public void testCleanup_3()
		throws Exception {
		Logger log = NOPLogger.NOP_LOGGER;

		IOUtils.cleanup(log);

		// add additional test code here
	}

	/**
	 * Run the void cleanup(Logger,Closeable[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test
	public void testCleanup_4()
		throws Exception {
		Logger log = NOPLogger.NOP_LOGGER;

		IOUtils.cleanup(log);

		// add additional test code here
	}

	/**
	 * Run the void cleanup(Logger,Closeable[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test
	public void testCleanup_5()
		throws Exception {
		Logger log = NOPLogger.NOP_LOGGER;

		IOUtils.cleanup(log);

		// add additional test code here
	}

	/**
	 * Run the void closeStream(Closeable) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test
	public void testCloseStream_1()
		throws Exception {
		Closeable stream = null;

		IOUtils.closeStream(stream);

		// add additional test code here
	}

	/**
	 * Run the void copyBytes(InputStream,OutputStream,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testCopyBytes_1()
		throws Exception {
		InputStream in = new PipedInputStream();
		OutputStream out = new PrintStream(new ByteArrayOutputStream());
		int buffSize = 1;

		IOUtils.copyBytes(in, out, buffSize);

		// add additional test code here
	}

	/**
	 * Run the void copyBytes(InputStream,OutputStream,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testCopyBytes_2()
		throws Exception {
		InputStream in = new PipedInputStream();
		OutputStream out = new PrintStream(new ByteArrayOutputStream());
		int buffSize = 1;

		IOUtils.copyBytes(in, out, buffSize);

		// add additional test code here
	}

	/**
	 * Run the void copyBytes(InputStream,OutputStream,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testCopyBytes_3()
		throws Exception {
		InputStream in = new PipedInputStream();
		OutputStream out = new PrintStream(new ByteArrayOutputStream());
		int buffSize = 1;

		IOUtils.copyBytes(in, out, buffSize);

		// add additional test code here
	}

	/**
	 * Run the void copyBytes(InputStream,OutputStream,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testCopyBytes_4()
		throws Exception {
		InputStream in = new PipedInputStream();
		OutputStream out = new PrintStream(new ByteArrayOutputStream());
		int buffSize = 1;

		IOUtils.copyBytes(in, out, buffSize);

		// add additional test code here
	}

	/**
	 * Run the void copyBytes(InputStream,OutputStream,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testCopyBytes_5()
		throws Exception {
		InputStream in = new PipedInputStream();
		OutputStream out = new ByteArrayOutputStream();
		int buffSize = 1;

		IOUtils.copyBytes(in, out, buffSize);

		// add additional test code here
	}

	/**
	 * Run the void copyBytes(InputStream,OutputStream,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testCopyBytes_6()
		throws Exception {
		InputStream in = new PipedInputStream();
		OutputStream out = new ByteArrayOutputStream();
		int buffSize = 1;

		IOUtils.copyBytes(in, out, buffSize);

		// add additional test code here
	}

	/**
	 * Run the void copyBytes(InputStream,OutputStream,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testCopyBytes_7()
		throws Exception {
		InputStream in = new PipedInputStream();
		OutputStream out = new PrintStream(new ByteArrayOutputStream());
		int buffSize = 1;

		IOUtils.copyBytes(in, out, buffSize);

		// add additional test code here
	}

	/**
	 * Run the void copyBytes(InputStream,OutputStream,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testCopyBytes_8()
		throws Exception {
		InputStream in = new PipedInputStream();
		OutputStream out = new ByteArrayOutputStream();
		int buffSize = 1;

		IOUtils.copyBytes(in, out, buffSize);

		// add additional test code here
	}

	/**
	 * Run the void copyBytes(InputStream,OutputStream,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testCopyBytes_9()
		throws Exception {
		InputStream in = new PipedInputStream();
		OutputStream out = new PrintStream(new ByteArrayOutputStream());
		int buffSize = 1;

		IOUtils.copyBytes(in, out, buffSize);

		// add additional test code here
	}

	/**
	 * Run the void copyBytes(InputStream,OutputStream,int,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test
	public void testCopyBytes_10()
		throws Exception {
		InputStream in = null;
		OutputStream out = null;
		int buffSize = 1;
		boolean close = true;

		IOUtils.copyBytes(in, out, buffSize, close);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.common.IOUtils.copyBytes(IOUtils.java:113)
		//       at org.apache.zookeeper.common.IOUtils.copyBytes(IOUtils.java:84)
	}

	/**
	 * Run the void copyBytes(InputStream,OutputStream,int,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test
	public void testCopyBytes_11()
		throws Exception {
		InputStream in = null;
		OutputStream out = new ByteArrayOutputStream();
		int buffSize = 1;
		boolean close = true;

		IOUtils.copyBytes(in, out, buffSize, close);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.common.IOUtils.copyBytes(IOUtils.java:113)
		//       at org.apache.zookeeper.common.IOUtils.copyBytes(IOUtils.java:84)
	}

	/**
	 * Run the void copyBytes(InputStream,OutputStream,int,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testCopyBytes_12()
		throws Exception {
		InputStream in = new PipedInputStream();
		OutputStream out = new ByteArrayOutputStream();
		int buffSize = 1;
		boolean close = false;

		IOUtils.copyBytes(in, out, buffSize, close);

		// add additional test code here
	}

	/**
	 * Run the void copyBytes(InputStream,OutputStream,int,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testCopyBytes_13()
		throws Exception {
		InputStream in = new PipedInputStream();
		OutputStream out = new ByteArrayOutputStream();
		int buffSize = 1;
		boolean close = true;

		IOUtils.copyBytes(in, out, buffSize, close);

		// add additional test code here
	}

	/**
	 * Run the void copyBytes(InputStream,OutputStream,int,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testCopyBytes_14()
		throws Exception {
		InputStream in = new PipedInputStream();
		OutputStream out = new ByteArrayOutputStream();
		int buffSize = 1;
		boolean close = false;

		IOUtils.copyBytes(in, out, buffSize, close);

		// add additional test code here
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(IOUtilsTest.class);
	}
}