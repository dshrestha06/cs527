package com.uiuc.org.apache.zookeeper.server.quorum;

import org.apache.zookeeper.server.ZooKeeperServer;
import org.apache.zookeeper.server.quorum.ReadOnlyBean;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>ReadOnlyBeanTest</code> contains tests for the class <code>{@link ReadOnlyBean}</code>.
 *
 * @generatedBy CodePro at 10/16/13 6:56 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class ReadOnlyBeanTest {
	/**
	 * Run the ReadOnlyBean(ZooKeeperServer) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:56 PM
	 */
	@Test
	public void testReadOnlyBean_1()
		throws Exception {
		ZooKeeperServer zks = new ZooKeeperServer();

		ReadOnlyBean result = new ReadOnlyBean(zks);

		// add additional test code here
		assertNotNull(result);
		assertEquals("ReadOnlyServer", result.getName());
		assertEquals(false, result.isHidden());
		assertEquals("3.4.5--1, built on 10/16/2013 23:44 GMT", result.getVersion());
		assertEquals("Wed Oct 16 18:56:04 CDT 2013", result.getStartTime());
		assertEquals(-1, result.getMaxClientCnxnsPerHost());
		assertEquals(6000, result.getMinSessionTimeout());
		assertEquals(60000, result.getMaxSessionTimeout());
		assertEquals("10.9.16.66:-1", result.getClientPort());
		assertEquals(3000, result.getTickTime());
		assertEquals(0L, result.getOutstandingRequests());
		assertEquals(0L, result.getPacketsReceived());
		assertEquals(0L, result.getPacketsSent());
		assertEquals(0L, result.getAvgRequestLatency());
		assertEquals(0L, result.getMaxRequestLatency());
		assertEquals(0L, result.getMinRequestLatency());
	}

	/**
	 * Run the String getName() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:56 PM
	 */
	@Test
	public void testGetName_1()
		throws Exception {
		ReadOnlyBean fixture = new ReadOnlyBean(new ZooKeeperServer());

		String result = fixture.getName();

		// add additional test code here
		assertEquals("ReadOnlyServer", result);
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 6:56 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 6:56 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 6:56 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(ReadOnlyBeanTest.class);
	}
}