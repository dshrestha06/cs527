package com.uiuc.org.apache.zookeeper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.ZooKeeperMain;
import org.apache.zookeeper.test.system.SimpleClient;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The class <code>ZooKeeperMainTest</code> contains tests for the class <code>{@link ZooKeeperMain}</code>.
 *
 * @generatedBy CodePro at 10/16/13 7:29 PM
 * @author dshresth
 * @version .Revision: 1.0 .
 */
public class ZooKeeperMainTest {
	/**
	 * Run the ZooKeeperMain(ZooKeeper) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testZooKeeperMain_1()
		throws Exception {
		ZooKeeper zk = new ZooKeeper("", 1, new SimpleClient());

		ZooKeeperMain result = new ZooKeeperMain(zk);

		// add additional test code here
		assertNotNull(result);
		assertEquals(true, result.getPrintWatches());
	}

	/**
	 * Run the ZooKeeperMain(String[]) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testZooKeeperMain_2()
		throws Exception {
		String[] args = new String[] {};

		ZooKeeperMain result = new ZooKeeperMain(args);

		// add additional test code here
		assertNotNull(result);
		assertEquals(true, result.getPrintWatches());
	}

	/**
	 * Run the ZooKeeperMain(String[]) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testZooKeeperMain_3()
		throws Exception {
		String[] args = new String[] {};

		ZooKeeperMain result = new ZooKeeperMain(args);

		// add additional test code here
		assertNotNull(result);
		assertEquals(true, result.getPrintWatches());
	}

	/**
	 * Run the ZooKeeperMain(String[]) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testZooKeeperMain_4()
		throws Exception {
		String[] args = new String[] {};

		ZooKeeperMain result = new ZooKeeperMain(args);

		// add additional test code here
		assertNotNull(result);
		assertEquals(true, result.getPrintWatches());
	}

	/**
	 * Run the void addToHistory(int,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testAddToHistory_1()
		throws Exception {
		ZooKeeperMain fixture = new ZooKeeperMain(new ZooKeeper("", 1, new SimpleClient()));
		int i = 1;
		String cmd = "";

		fixture.addToHistory(i, cmd);

		// add additional test code here
	}

	/**
	 * Run the void connectToZK(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testConnectToZK_1()
		throws Exception {
		ZooKeeperMain fixture = new ZooKeeperMain(new ZooKeeper("", 1, new SimpleClient()));
		String newHost = "";

		fixture.connectToZK(newHost);

		// add additional test code here
	}

	/**
	 * Run the void connectToZK(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testConnectToZK_2()
		throws Exception {
		ZooKeeperMain fixture = new ZooKeeperMain(new ZooKeeper("", 1, new SimpleClient()));
		String newHost = "";

		fixture.connectToZK(newHost);

		// add additional test code here
	}

	/**
	 * Run the void connectToZK(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testConnectToZK_3()
		throws Exception {
		ZooKeeperMain fixture = new ZooKeeperMain(new ZooKeeper("a", 1, new SimpleClient()));
		String newHost = "a";

		fixture.connectToZK(newHost);

		// add additional test code here
	}

	/**
	 * Run the void connectToZK(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testConnectToZK_4()
		throws Exception {
		ZooKeeperMain fixture = new ZooKeeperMain(new ZooKeeper("a", 1, new SimpleClient()));
		String newHost = "a";

		fixture.connectToZK(newHost);

		// add additional test code here
	}

	/**
	 * Run the void connectToZK(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testConnectToZK_5()
		throws Exception {
		ZooKeeperMain fixture = new ZooKeeperMain((ZooKeeper) null);
		String newHost = "a";

		fixture.connectToZK(newHost);

		// add additional test code here
	}

	/**
	 * Run the void connectToZK(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testConnectToZK_6()
		throws Exception {
		ZooKeeperMain fixture = new ZooKeeperMain(new ZooKeeper("a", 1, new SimpleClient()));
		String newHost = "a";

		fixture.connectToZK(newHost);

		// add additional test code here
	}

	/**
	 * Run the boolean createQuota(ZooKeeper,String,long,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testCreateQuota_1()
		throws Exception {
		ZooKeeper zk = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		long bytes = 1L;
		int numNodes = 1;

		boolean result = ZooKeeperMain.createQuota(zk, path, bytes, numNodes);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.exists(ZooKeeper.java:1020)
		//       at org.apache.zookeeper.ZooKeeper.exists(ZooKeeper.java:1069)
		//       at org.apache.zookeeper.ZooKeeperMain.createQuota(ZooKeeperMain.java:505)
		assertTrue(result);
	}

	/**
	 * Run the boolean createQuota(ZooKeeper,String,long,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testCreateQuota_2()
		throws Exception {
		ZooKeeper zk = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		long bytes = 1L;
		int numNodes = 1;

		boolean result = ZooKeeperMain.createQuota(zk, path, bytes, numNodes);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.exists(ZooKeeper.java:1020)
		//       at org.apache.zookeeper.ZooKeeper.exists(ZooKeeper.java:1069)
		//       at org.apache.zookeeper.ZooKeeperMain.createQuota(ZooKeeperMain.java:505)
		assertTrue(result);
	}

	/**
	 * Run the boolean createQuota(ZooKeeper,String,long,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testCreateQuota_3()
		throws Exception {
		ZooKeeper zk = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		long bytes = 1L;
		int numNodes = 1;

		boolean result = ZooKeeperMain.createQuota(zk, path, bytes, numNodes);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.exists(ZooKeeper.java:1020)
		//       at org.apache.zookeeper.ZooKeeper.exists(ZooKeeper.java:1069)
		//       at org.apache.zookeeper.ZooKeeperMain.createQuota(ZooKeeperMain.java:505)
		assertTrue(result);
	}

	/**
	 * Run the boolean createQuota(ZooKeeper,String,long,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testCreateQuota_4()
		throws Exception {
		ZooKeeper zk = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		long bytes = 1L;
		int numNodes = 1;

		boolean result = ZooKeeperMain.createQuota(zk, path, bytes, numNodes);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.exists(ZooKeeper.java:1020)
		//       at org.apache.zookeeper.ZooKeeper.exists(ZooKeeper.java:1069)
		//       at org.apache.zookeeper.ZooKeeperMain.createQuota(ZooKeeperMain.java:505)
		assertTrue(result);
	}

	/**
	 * Run the boolean createQuota(ZooKeeper,String,long,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testCreateQuota_5()
		throws Exception {
		ZooKeeper zk = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		long bytes = 1L;
		int numNodes = 1;

		boolean result = ZooKeeperMain.createQuota(zk, path, bytes, numNodes);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.exists(ZooKeeper.java:1020)
		//       at org.apache.zookeeper.ZooKeeper.exists(ZooKeeper.java:1069)
		//       at org.apache.zookeeper.ZooKeeperMain.createQuota(ZooKeeperMain.java:505)
		assertTrue(result);
	}

	/**
	 * Run the boolean createQuota(ZooKeeper,String,long,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testCreateQuota_6()
		throws Exception {
		ZooKeeper zk = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		long bytes = 1L;
		int numNodes = 1;

		boolean result = ZooKeeperMain.createQuota(zk, path, bytes, numNodes);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.exists(ZooKeeper.java:1020)
		//       at org.apache.zookeeper.ZooKeeper.exists(ZooKeeper.java:1069)
		//       at org.apache.zookeeper.ZooKeeperMain.createQuota(ZooKeeperMain.java:505)
		assertTrue(result);
	}

	/**
	 * Run the boolean createQuota(ZooKeeper,String,long,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testCreateQuota_7()
		throws Exception {
		ZooKeeper zk = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		long bytes = 1L;
		int numNodes = 1;

		boolean result = ZooKeeperMain.createQuota(zk, path, bytes, numNodes);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.exists(ZooKeeper.java:1020)
		//       at org.apache.zookeeper.ZooKeeper.exists(ZooKeeper.java:1069)
		//       at org.apache.zookeeper.ZooKeeperMain.createQuota(ZooKeeperMain.java:505)
		assertTrue(result);
	}

	/**
	 * Run the boolean createQuota(ZooKeeper,String,long,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testCreateQuota_8()
		throws Exception {
		ZooKeeper zk = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		long bytes = 1L;
		int numNodes = 1;

		boolean result = ZooKeeperMain.createQuota(zk, path, bytes, numNodes);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.exists(ZooKeeper.java:1020)
		//       at org.apache.zookeeper.ZooKeeper.exists(ZooKeeper.java:1069)
		//       at org.apache.zookeeper.ZooKeeperMain.createQuota(ZooKeeperMain.java:505)
		assertTrue(result);
	}

	/**
	 * Run the boolean createQuota(ZooKeeper,String,long,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testCreateQuota_9()
		throws Exception {
		ZooKeeper zk = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		long bytes = 1L;
		int numNodes = 1;

		boolean result = ZooKeeperMain.createQuota(zk, path, bytes, numNodes);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.exists(ZooKeeper.java:1020)
		//       at org.apache.zookeeper.ZooKeeper.exists(ZooKeeper.java:1069)
		//       at org.apache.zookeeper.ZooKeeperMain.createQuota(ZooKeeperMain.java:505)
		assertTrue(result);
	}

	/**
	 * Run the boolean createQuota(ZooKeeper,String,long,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testCreateQuota_10()
		throws Exception {
		ZooKeeper zk = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		long bytes = 1L;
		int numNodes = 1;

		boolean result = ZooKeeperMain.createQuota(zk, path, bytes, numNodes);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.exists(ZooKeeper.java:1020)
		//       at org.apache.zookeeper.ZooKeeper.exists(ZooKeeper.java:1069)
		//       at org.apache.zookeeper.ZooKeeperMain.createQuota(ZooKeeperMain.java:505)
		assertTrue(result);
	}

	/**
	 * Run the boolean createQuota(ZooKeeper,String,long,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testCreateQuota_11()
		throws Exception {
		ZooKeeper zk = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		long bytes = 1L;
		int numNodes = 1;

		boolean result = ZooKeeperMain.createQuota(zk, path, bytes, numNodes);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.exists(ZooKeeper.java:1020)
		//       at org.apache.zookeeper.ZooKeeper.exists(ZooKeeper.java:1069)
		//       at org.apache.zookeeper.ZooKeeperMain.createQuota(ZooKeeperMain.java:505)
		assertTrue(result);
	}

	/**
	 * Run the boolean createQuota(ZooKeeper,String,long,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testCreateQuota_12()
		throws Exception {
		ZooKeeper zk = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		long bytes = 1L;
		int numNodes = 1;

		boolean result = ZooKeeperMain.createQuota(zk, path, bytes, numNodes);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.exists(ZooKeeper.java:1020)
		//       at org.apache.zookeeper.ZooKeeper.exists(ZooKeeper.java:1069)
		//       at org.apache.zookeeper.ZooKeeperMain.createQuota(ZooKeeperMain.java:505)
		assertTrue(result);
	}

	/**
	 * Run the boolean createQuota(ZooKeeper,String,long,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testCreateQuota_13()
		throws Exception {
		ZooKeeper zk = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		long bytes = 1L;
		int numNodes = 1;

		boolean result = ZooKeeperMain.createQuota(zk, path, bytes, numNodes);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.exists(ZooKeeper.java:1020)
		//       at org.apache.zookeeper.ZooKeeper.exists(ZooKeeper.java:1069)
		//       at org.apache.zookeeper.ZooKeeperMain.createQuota(ZooKeeperMain.java:505)
		assertTrue(result);
	}

	/**
	 * Run the boolean createQuota(ZooKeeper,String,long,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testCreateQuota_14()
		throws Exception {
		ZooKeeper zk = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		long bytes = 1L;
		int numNodes = 1;

		boolean result = ZooKeeperMain.createQuota(zk, path, bytes, numNodes);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.exists(ZooKeeper.java:1020)
		//       at org.apache.zookeeper.ZooKeeper.exists(ZooKeeper.java:1069)
		//       at org.apache.zookeeper.ZooKeeperMain.createQuota(ZooKeeperMain.java:505)
		assertTrue(result);
	}

	/**
	 * Run the boolean createQuota(ZooKeeper,String,long,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testCreateQuota_15()
		throws Exception {
		ZooKeeper zk = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		long bytes = 1L;
		int numNodes = 1;

		boolean result = ZooKeeperMain.createQuota(zk, path, bytes, numNodes);

		// add additional test code here
		assertTrue(result);
	}

	/**
	 * Run the boolean createQuota(ZooKeeper,String,long,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testCreateQuota_16()
		throws Exception {
		ZooKeeper zk = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		long bytes = 1L;
		int numNodes = 1;

		boolean result = ZooKeeperMain.createQuota(zk, path, bytes, numNodes);

		// add additional test code here
		assertTrue(result);
	}

	/**
	 * Run the boolean delQuota(ZooKeeper,String,boolean,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = org.apache.zookeeper.KeeperException.ConnectionLossException.class)
	public void testDelQuota_1()
		throws Exception {
		ZooKeeper zk = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		boolean bytes = true;
		boolean numNodes = true;

		boolean result = ZooKeeperMain.delQuota(zk, path, bytes, numNodes);

		// add additional test code here
		assertTrue(result);
	}

	/**
	 * Run the boolean delQuota(ZooKeeper,String,boolean,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = org.apache.zookeeper.KeeperException.ConnectionLossException.class)
	public void testDelQuota_2()
		throws Exception {
		ZooKeeper zk = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		boolean bytes = true;
		boolean numNodes = true;

		boolean result = ZooKeeperMain.delQuota(zk, path, bytes, numNodes);

		// add additional test code here
		assertTrue(result);
	}

	/**
	 * Run the boolean delQuota(ZooKeeper,String,boolean,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = org.apache.zookeeper.KeeperException.ConnectionLossException.class)
	public void testDelQuota_3()
		throws Exception {
		ZooKeeper zk = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		boolean bytes = true;
		boolean numNodes = false;

		boolean result = ZooKeeperMain.delQuota(zk, path, bytes, numNodes);

		// add additional test code here
		assertTrue(result);
	}

	/**
	 * Run the boolean delQuota(ZooKeeper,String,boolean,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = org.apache.zookeeper.KeeperException.ConnectionLossException.class)
	public void testDelQuota_4()
		throws Exception {
		ZooKeeper zk = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		boolean bytes = false;
		boolean numNodes = true;

		boolean result = ZooKeeperMain.delQuota(zk, path, bytes, numNodes);

		// add additional test code here
		assertTrue(result);
	}

	/**
	 * Run the boolean delQuota(ZooKeeper,String,boolean,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = org.apache.zookeeper.KeeperException.ConnectionLossException.class)
	public void testDelQuota_5()
		throws Exception {
		ZooKeeper zk = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		boolean bytes = true;
		boolean numNodes = true;

		boolean result = ZooKeeperMain.delQuota(zk, path, bytes, numNodes);

		// add additional test code here
		assertTrue(result);
	}

	/**
	 * Run the boolean delQuota(ZooKeeper,String,boolean,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = org.apache.zookeeper.KeeperException.ConnectionLossException.class)
	public void testDelQuota_6()
		throws Exception {
		ZooKeeper zk = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		boolean bytes = true;
		boolean numNodes = true;

		boolean result = ZooKeeperMain.delQuota(zk, path, bytes, numNodes);

		// add additional test code here
		assertTrue(result);
	}

	/**
	 * Run the boolean delQuota(ZooKeeper,String,boolean,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = org.apache.zookeeper.KeeperException.ConnectionLossException.class)
	public void testDelQuota_7()
		throws Exception {
		ZooKeeper zk = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		boolean bytes = false;
		boolean numNodes = false;

		boolean result = ZooKeeperMain.delQuota(zk, path, bytes, numNodes);

		// add additional test code here
		assertTrue(result);
	}

	/**
	 * Run the void executeLine(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testExecuteLine_1()
		throws Exception {
		ZooKeeperMain fixture = new ZooKeeperMain(new ZooKeeper("", 1, new SimpleClient()));
		String line = "";

		fixture.executeLine(line);

		// add additional test code here
	}

	/**
	 * Run the void executeLine(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testExecuteLine_2()
		throws Exception {
		ZooKeeperMain fixture = new ZooKeeperMain(new ZooKeeper("", 1, new SimpleClient()));
		String line = "";

		fixture.executeLine(line);

		// add additional test code here
	}

	/**
	 * Run the void executeLine(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testExecuteLine_3()
		throws Exception {
		ZooKeeperMain fixture = new ZooKeeperMain(new ZooKeeper("", 1, new SimpleClient()));
		String line = "";

		fixture.executeLine(line);

		// add additional test code here
	}

	/**
	 * Run the void executeLine(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testExecuteLine_4()
		throws Exception {
		ZooKeeperMain fixture = new ZooKeeperMain(new ZooKeeper("", 1, new SimpleClient()));
		String line = "";

		fixture.executeLine(line);

		// add additional test code here
	}

	/**
	 * Run the void executeLine(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testExecuteLine_5()
		throws Exception {
		ZooKeeperMain fixture = new ZooKeeperMain(new ZooKeeper("", 1, new SimpleClient()));
		String line = "";

		fixture.executeLine(line);

		// add additional test code here
	}

	/**
	 * Run the List<String> getCommands() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testGetCommands_1()
		throws Exception {

		List<String> result = ZooKeeperMain.getCommands();

		// add additional test code here
		assertNotNull(result);
		assertEquals(21, result.size());
		assertTrue(result.contains("connect"));
		assertTrue(result.contains("get"));
		assertTrue(result.contains("ls"));
		assertTrue(result.contains("set"));
		assertTrue(result.contains("rmr"));
		assertTrue(result.contains("delquota"));
		assertTrue(result.contains("quit"));
		assertTrue(result.contains("printwatches"));
		assertTrue(result.contains("create"));
		assertTrue(result.contains("stat"));
		assertTrue(result.contains("close"));
		assertTrue(result.contains("ls2"));
		assertTrue(result.contains("history"));
		assertTrue(result.contains("listquota"));
		assertTrue(result.contains("setAcl"));
		assertTrue(result.contains("getAcl"));
		assertTrue(result.contains("sync"));
		assertTrue(result.contains("redo"));
		assertTrue(result.contains("addauth"));
		assertTrue(result.contains("delete"));
		assertTrue(result.contains("setquota"));
	}

	/**
	 * Run the boolean getPrintWatches() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testGetPrintWatches_1()
		throws Exception {
		ZooKeeperMain fixture = new ZooKeeperMain(new ZooKeeper("", 1, new SimpleClient()));

		boolean result = fixture.getPrintWatches();

		// add additional test code here
		assertEquals(true, result);
	}

	/**
	 * Run the boolean getPrintWatches() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testGetPrintWatches_2()
		throws Exception {
		ZooKeeperMain fixture = new ZooKeeperMain(new ZooKeeper("", 1, new SimpleClient()));

		boolean result = fixture.getPrintWatches();

		// add additional test code here
		assertEquals(true, result);
	}

	/**
	 * Run the String getPrompt() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testGetPrompt_1()
		throws Exception {
		ZooKeeperMain fixture = new ZooKeeperMain(new ZooKeeper("", 1, new SimpleClient()));

		String result = fixture.getPrompt();

		// add additional test code here
		assertEquals("[zk: (CONNECTING) 0] ", result);
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testMain_1()
		throws Exception {
		String[] args = new String[] {};

		ZooKeeperMain.main(args);

		// add additional test code here
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testMain_2()
		throws Exception {
		String[] args = new String[] {};

		ZooKeeperMain.main(args);

		// add additional test code here
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testMain_3()
		throws Exception {
		String[] args = new String[] {};

		ZooKeeperMain.main(args);

		// add additional test code here
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testMain_4()
		throws Exception {
		String[] args = new String[] {};

		ZooKeeperMain.main(args);

		// add additional test code here
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testMain_5()
		throws Exception {
		String[] args = new String[] {};

		ZooKeeperMain.main(args);

		// add additional test code here
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testMain_6()
		throws Exception {
		String[] args = new String[] {};

		ZooKeeperMain.main(args);

		// add additional test code here
	}

	/**
	 * Run the void printMessage(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testPrintMessage_1()
		throws Exception {
		String msg = "";

		ZooKeeperMain.printMessage(msg);

		// add additional test code here
	}

	/**
	 * Run the boolean processCmd(MyCommandOptions) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testProcessCmd_1()
		throws Exception {
		ZooKeeperMain fixture = new ZooKeeperMain(new ZooKeeper("", 1, new SimpleClient()));

		boolean result = fixture.processCmd(new org.apache.zookeeper.ZooKeeperMain.MyCommandOptions());

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.ZooKeeperMain.MyCommandOptions.getArgArray(ZooKeeperMain.java:179)
		//       at org.apache.zookeeper.ZooKeeperMain.processZKCmd(ZooKeeperMain.java:623)
		//       at org.apache.zookeeper.ZooKeeperMain.processCmd(ZooKeeperMain.java:593)
		assertTrue(result);
	}

	/**
	 * Run the boolean processCmd(MyCommandOptions) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testProcessCmd_2()
		throws Exception {
		ZooKeeperMain fixture = new ZooKeeperMain(new ZooKeeper("", 1, new SimpleClient()));

		boolean result = fixture.processCmd(new org.apache.zookeeper.ZooKeeperMain.MyCommandOptions());

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.ZooKeeperMain.MyCommandOptions.getArgArray(ZooKeeperMain.java:179)
		//       at org.apache.zookeeper.ZooKeeperMain.processZKCmd(ZooKeeperMain.java:623)
		//       at org.apache.zookeeper.ZooKeeperMain.processCmd(ZooKeeperMain.java:593)
		assertTrue(result);
	}

	/**
	 * Run the boolean processCmd(MyCommandOptions) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testProcessCmd_3()
		throws Exception {
		ZooKeeperMain fixture = new ZooKeeperMain(new ZooKeeper("", 1, new SimpleClient()));

		boolean result = fixture.processCmd(new org.apache.zookeeper.ZooKeeperMain.MyCommandOptions());

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.ZooKeeperMain.MyCommandOptions.getArgArray(ZooKeeperMain.java:179)
		//       at org.apache.zookeeper.ZooKeeperMain.processZKCmd(ZooKeeperMain.java:623)
		//       at org.apache.zookeeper.ZooKeeperMain.processCmd(ZooKeeperMain.java:593)
		assertTrue(result);
	}

	/**
	 * Run the boolean processCmd(MyCommandOptions) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testProcessCmd_4()
		throws Exception {
		ZooKeeperMain fixture = new ZooKeeperMain(new ZooKeeper("", 1, new SimpleClient()));

		boolean result = fixture.processCmd(new org.apache.zookeeper.ZooKeeperMain.MyCommandOptions());

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.ZooKeeperMain.MyCommandOptions.getArgArray(ZooKeeperMain.java:179)
		//       at org.apache.zookeeper.ZooKeeperMain.processZKCmd(ZooKeeperMain.java:623)
		//       at org.apache.zookeeper.ZooKeeperMain.processCmd(ZooKeeperMain.java:593)
		assertTrue(result);
	}

	/**
	 * Run the boolean processCmd(MyCommandOptions) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testProcessCmd_5()
		throws Exception {
		ZooKeeperMain fixture = new ZooKeeperMain(new ZooKeeper("", 1, new SimpleClient()));

		boolean result = fixture.processCmd(new org.apache.zookeeper.ZooKeeperMain.MyCommandOptions());

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.ZooKeeperMain.MyCommandOptions.getArgArray(ZooKeeperMain.java:179)
		//       at org.apache.zookeeper.ZooKeeperMain.processZKCmd(ZooKeeperMain.java:623)
		//       at org.apache.zookeeper.ZooKeeperMain.processCmd(ZooKeeperMain.java:593)
		assertTrue(result);
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testRun_1()
		throws Exception {
		ZooKeeperMain fixture = new ZooKeeperMain(new ZooKeeper("", 1, new SimpleClient()));

		fixture.run();

		// add additional test code here
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testRun_2()
		throws Exception {
		ZooKeeperMain fixture = new ZooKeeperMain(new ZooKeeper("", 1, new SimpleClient()));

		fixture.run();

		// add additional test code here
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testRun_3()
		throws Exception {
		ZooKeeperMain fixture = new ZooKeeperMain(new ZooKeeper("", 1, new SimpleClient()));

		fixture.run();

		// add additional test code here
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testRun_4()
		throws Exception {
		ZooKeeperMain fixture = new ZooKeeperMain(new ZooKeeper("", 1, new SimpleClient()));

		fixture.run();

		// add additional test code here
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testRun_5()
		throws Exception {
		ZooKeeperMain fixture = new ZooKeeperMain(new ZooKeeper("", 1, new SimpleClient()));

		fixture.run();

		// add additional test code here
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testRun_6()
		throws Exception {
		ZooKeeperMain fixture = new ZooKeeperMain(new ZooKeeper("", 1, new SimpleClient()));

		fixture.run();

		// add additional test code here
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testRun_7()
		throws Exception {
		ZooKeeperMain fixture = new ZooKeeperMain(new ZooKeeper("", 1, new SimpleClient()));

		fixture.run();

		// add additional test code here
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testRun_8()
		throws Exception {
		ZooKeeperMain fixture = new ZooKeeperMain(new ZooKeeper("", 1, new SimpleClient()));

		fixture.run();

		// add additional test code here
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testRun_9()
		throws Exception {
		ZooKeeperMain fixture = new ZooKeeperMain(new ZooKeeper("", 1, new SimpleClient()));

		fixture.run();

		// add additional test code here
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testRun_10()
		throws Exception {
		ZooKeeperMain fixture = new ZooKeeperMain(new ZooKeeper("", 1, new SimpleClient()));

		fixture.run();

		// add additional test code here
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testRun_11()
		throws Exception {
		ZooKeeperMain fixture = new ZooKeeperMain(new ZooKeeper("", 1, new SimpleClient()));

		fixture.run();

		// add additional test code here
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testRun_12()
		throws Exception {
		ZooKeeperMain fixture = new ZooKeeperMain(new ZooKeeper("", 1, new SimpleClient()));

		fixture.run();

		// add additional test code here
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testRun_13()
		throws Exception {
		ZooKeeperMain fixture = new ZooKeeperMain(new ZooKeeper("", 1, new SimpleClient()));

		fixture.run();

		// add additional test code here
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testRun_14()
		throws Exception {
		ZooKeeperMain fixture = new ZooKeeperMain(new ZooKeeper("", 1, new SimpleClient()));

		fixture.run();

		// add additional test code here
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testRun_15()
		throws Exception {
		ZooKeeperMain fixture = new ZooKeeperMain(new ZooKeeper("", 1, new SimpleClient()));

		fixture.run();

		// add additional test code here
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testRun_16()
		throws Exception {
		ZooKeeperMain fixture = new ZooKeeperMain(new ZooKeeper("", 1, new SimpleClient()));

		fixture.run();

		// add additional test code here
	}

	/**
	 * Run the void usage() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testUsage_1()
		throws Exception {

		ZooKeeperMain.usage();

		// add additional test code here
	}

	/**
	 * Run the void usage() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testUsage_2()
		throws Exception {

		ZooKeeperMain.usage();

		// add additional test code here
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(ZooKeeperMainTest.class);
	}
}