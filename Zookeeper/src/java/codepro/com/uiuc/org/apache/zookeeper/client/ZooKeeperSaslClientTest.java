package com.uiuc.org.apache.zookeeper.client;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.LinkedList;

import org.apache.zookeeper.ClientCnxn;
import org.apache.zookeeper.ClientWatchManager;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.client.StaticHostProvider;
import org.apache.zookeeper.client.ZooKeeperSaslClient;
import org.apache.zookeeper.test.system.SimpleClient;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The class <code>ZooKeeperSaslClientTest</code> contains tests for the class <code>{@link ZooKeeperSaslClient}</code>.
 *
 * @generatedBy CodePro at 10/16/13 7:31 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class ZooKeeperSaslClientTest {
	/**
	 * Run the ZooKeeperSaslClient(String) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testZooKeeperSaslClient_1()
		throws Exception {
		String serverPrincipal = "";

		ZooKeeperSaslClient result = new ZooKeeperSaslClient(serverPrincipal);

		// add additional test code here
		assertNotNull(result);
		assertEquals(false, result.isComplete());
		assertEquals(false, result.clientTunneledAuthenticationInProgress());
		assertEquals("Will not attempt to authenticate using SASL (Unable to locate a login configuration)", result.getConfigStatus());
		assertEquals(null, result.getKeeperState());
		assertEquals(null, result.getLoginContext());
		assertEquals(true, result.isFailed());
	}

	/**
	 * Run the ZooKeeperSaslClient(String) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testZooKeeperSaslClient_2()
		throws Exception {
		String serverPrincipal = "";

		ZooKeeperSaslClient result = new ZooKeeperSaslClient(serverPrincipal);

		// add additional test code here
		assertNotNull(result);
		assertEquals(false, result.isComplete());
		assertEquals(false, result.clientTunneledAuthenticationInProgress());
		assertEquals("Will not attempt to authenticate using SASL (Unable to locate a login configuration)", result.getConfigStatus());
		assertEquals(null, result.getKeeperState());
		assertEquals(null, result.getLoginContext());
		assertEquals(true, result.isFailed());
	}

	/**
	 * Run the ZooKeeperSaslClient(String) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testZooKeeperSaslClient_3()
		throws Exception {
		String serverPrincipal = "";

		ZooKeeperSaslClient result = new ZooKeeperSaslClient(serverPrincipal);

		// add additional test code here
		assertNotNull(result);
		assertEquals(false, result.isComplete());
		assertEquals(false, result.clientTunneledAuthenticationInProgress());
		assertEquals("Will not attempt to authenticate using SASL (Unable to locate a login configuration)", result.getConfigStatus());
		assertEquals(null, result.getKeeperState());
		assertEquals(null, result.getLoginContext());
		assertEquals(true, result.isFailed());
	}

	/**
	 * Run the ZooKeeperSaslClient(String) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testZooKeeperSaslClient_4()
		throws Exception {
		String serverPrincipal = "";

		ZooKeeperSaslClient result = new ZooKeeperSaslClient(serverPrincipal);

		// add additional test code here
		assertNotNull(result);
		assertEquals(false, result.isComplete());
		assertEquals(false, result.clientTunneledAuthenticationInProgress());
		assertEquals("Will not attempt to authenticate using SASL (Unable to locate a login configuration)", result.getConfigStatus());
		assertEquals(null, result.getKeeperState());
		assertEquals(null, result.getLoginContext());
		assertEquals(true, result.isFailed());
	}

	/**
	 * Run the ZooKeeperSaslClient(String) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testZooKeeperSaslClient_5()
		throws Exception {
		String serverPrincipal = "";

		ZooKeeperSaslClient result = new ZooKeeperSaslClient(serverPrincipal);

		// add additional test code here
		assertNotNull(result);
		assertEquals(false, result.isComplete());
		assertEquals(false, result.clientTunneledAuthenticationInProgress());
		assertEquals("Will not attempt to authenticate using SASL (Unable to locate a login configuration)", result.getConfigStatus());
		assertEquals(null, result.getKeeperState());
		assertEquals(null, result.getLoginContext());
		assertEquals(true, result.isFailed());
	}

	/**
	 * Run the ZooKeeperSaslClient(String) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testZooKeeperSaslClient_6()
		throws Exception {
		String serverPrincipal = "";

		ZooKeeperSaslClient result = new ZooKeeperSaslClient(serverPrincipal);

		// add additional test code here
		assertNotNull(result);
		assertEquals(false, result.isComplete());
		assertEquals(false, result.clientTunneledAuthenticationInProgress());
		assertEquals("Will not attempt to authenticate using SASL (Unable to locate a login configuration)", result.getConfigStatus());
		assertEquals(null, result.getKeeperState());
		assertEquals(null, result.getLoginContext());
		assertEquals(true, result.isFailed());
	}

	/**
	 * Run the ZooKeeperSaslClient(String) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testZooKeeperSaslClient_7()
		throws Exception {
		String serverPrincipal = "";

		ZooKeeperSaslClient result = new ZooKeeperSaslClient(serverPrincipal);

		// add additional test code here
		assertNotNull(result);
		assertEquals(false, result.isComplete());
		assertEquals(false, result.clientTunneledAuthenticationInProgress());
		assertEquals("Will not attempt to authenticate using SASL (Unable to locate a login configuration)", result.getConfigStatus());
		assertEquals(null, result.getKeeperState());
		assertEquals(null, result.getLoginContext());
		assertEquals(true, result.isFailed());
	}

	/**
	 * Run the ZooKeeperSaslClient(String) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testZooKeeperSaslClient_8()
		throws Exception {
		String serverPrincipal = "";

		ZooKeeperSaslClient result = new ZooKeeperSaslClient(serverPrincipal);

		// add additional test code here
		assertNotNull(result);
		assertEquals(false, result.isComplete());
		assertEquals(false, result.clientTunneledAuthenticationInProgress());
		assertEquals("Will not attempt to authenticate using SASL (Unable to locate a login configuration)", result.getConfigStatus());
		assertEquals(null, result.getKeeperState());
		assertEquals(null, result.getLoginContext());
		assertEquals(true, result.isFailed());
	}

	/**
	 * Run the boolean clientTunneledAuthenticationInProgress() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testClientTunneledAuthenticationInProgress_1()
		throws Exception {
		ZooKeeperSaslClient fixture = new ZooKeeperSaslClient("");

		boolean result = fixture.clientTunneledAuthenticationInProgress();

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean clientTunneledAuthenticationInProgress() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testClientTunneledAuthenticationInProgress_2()
		throws Exception {
		ZooKeeperSaslClient fixture = new ZooKeeperSaslClient("");

		boolean result = fixture.clientTunneledAuthenticationInProgress();

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean clientTunneledAuthenticationInProgress() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testClientTunneledAuthenticationInProgress_3()
		throws Exception {
		ZooKeeperSaslClient fixture = new ZooKeeperSaslClient("");

		boolean result = fixture.clientTunneledAuthenticationInProgress();

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean clientTunneledAuthenticationInProgress() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testClientTunneledAuthenticationInProgress_4()
		throws Exception {
		ZooKeeperSaslClient fixture = new ZooKeeperSaslClient("");

		boolean result = fixture.clientTunneledAuthenticationInProgress();

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean clientTunneledAuthenticationInProgress() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testClientTunneledAuthenticationInProgress_5()
		throws Exception {
		ZooKeeperSaslClient fixture = new ZooKeeperSaslClient("");

		boolean result = fixture.clientTunneledAuthenticationInProgress();

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean clientTunneledAuthenticationInProgress() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testClientTunneledAuthenticationInProgress_6()
		throws Exception {
		ZooKeeperSaslClient fixture = new ZooKeeperSaslClient("");

		boolean result = fixture.clientTunneledAuthenticationInProgress();

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the String getConfigStatus() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testGetConfigStatus_1()
		throws Exception {
		ZooKeeperSaslClient fixture = new ZooKeeperSaslClient("");

		String result = fixture.getConfigStatus();

		// add additional test code here
		assertEquals("Will not attempt to authenticate using SASL (Unable to locate a login configuration)", result);
	}

	/**
	 * Run the org.apache.zookeeper.Watcher.Event.KeeperState getKeeperState() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testGetKeeperState_1()
		throws Exception {
		ZooKeeperSaslClient fixture = new ZooKeeperSaslClient("");

		org.apache.zookeeper.Watcher.Event.KeeperState result = fixture.getKeeperState();

		// add additional test code here
		assertEquals(null, result);
	}

	/**
	 * Run the org.apache.zookeeper.Watcher.Event.KeeperState getKeeperState() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testGetKeeperState_2()
		throws Exception {
		ZooKeeperSaslClient fixture = new ZooKeeperSaslClient("");

		org.apache.zookeeper.Watcher.Event.KeeperState result = fixture.getKeeperState();

		// add additional test code here
		assertEquals(null, result);
	}

	/**
	 * Run the org.apache.zookeeper.Watcher.Event.KeeperState getKeeperState() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testGetKeeperState_3()
		throws Exception {
		ZooKeeperSaslClient fixture = new ZooKeeperSaslClient("");

		org.apache.zookeeper.Watcher.Event.KeeperState result = fixture.getKeeperState();

		// add additional test code here
		assertEquals(null, result);
	}

	/**
	 * Run the org.apache.zookeeper.Watcher.Event.KeeperState getKeeperState() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testGetKeeperState_4()
		throws Exception {
		ZooKeeperSaslClient fixture = new ZooKeeperSaslClient("");

		org.apache.zookeeper.Watcher.Event.KeeperState result = fixture.getKeeperState();

		// add additional test code here
		assertEquals(null, result);
	}

	/**
	 * Run the org.apache.zookeeper.Watcher.Event.KeeperState getKeeperState() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testGetKeeperState_5()
		throws Exception {
		ZooKeeperSaslClient fixture = new ZooKeeperSaslClient("");

		org.apache.zookeeper.Watcher.Event.KeeperState result = fixture.getKeeperState();

		// add additional test code here
		assertEquals(null, result);
	}

	/**
	 * Run the String getLoginContext() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testGetLoginContext_1()
		throws Exception {
		ZooKeeperSaslClient fixture = new ZooKeeperSaslClient("");

		String result = fixture.getLoginContext();

		// add additional test code here
		assertEquals(null, result);
	}

	/**
	 * Run the String getLoginContext() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testGetLoginContext_2()
		throws Exception {
		ZooKeeperSaslClient fixture = new ZooKeeperSaslClient("");

		String result = fixture.getLoginContext();

		// add additional test code here
		assertEquals(null, result);
	}

	/**
	 * Run the org.apache.zookeeper.client.ZooKeeperSaslClient.SaslState getSaslState() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testGetSaslState_1()
		throws Exception {
		ZooKeeperSaslClient fixture = new ZooKeeperSaslClient("");

		org.apache.zookeeper.client.ZooKeeperSaslClient.SaslState result = fixture.getSaslState();

		// add additional test code here
		assertNotNull(result);
		assertEquals("FAILED", result.name());
		assertEquals("FAILED", result.toString());
		assertEquals(3, result.ordinal());
	}

	/**
	 * Run the void initialize(ClientCnxn) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testInitialize_1()
		throws Exception {
		ZooKeeperSaslClient fixture = new ZooKeeperSaslClient("");
		ClientCnxn cnxn = new ClientCnxn("", new StaticHostProvider(new LinkedList()), 1, new ZooKeeper("", 1, new SimpleClient()), (ClientWatchManager) null, null, true);

		fixture.initialize(cnxn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: A HostProvider may not be empty!
		//       at org.apache.zookeeper.client.StaticHostProvider.<init>(StaticHostProvider.java:69)
	}

	/**
	 * Run the void initialize(ClientCnxn) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testInitialize_2()
		throws Exception {
		ZooKeeperSaslClient fixture = new ZooKeeperSaslClient("");
		ClientCnxn cnxn = new ClientCnxn("", new StaticHostProvider(new LinkedList()), 1, new ZooKeeper("", 1, new SimpleClient()), (ClientWatchManager) null, null, true);

		fixture.initialize(cnxn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: A HostProvider may not be empty!
		//       at org.apache.zookeeper.client.StaticHostProvider.<init>(StaticHostProvider.java:69)
	}

	/**
	 * Run the void initialize(ClientCnxn) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testInitialize_3()
		throws Exception {
		ZooKeeperSaslClient fixture = new ZooKeeperSaslClient("");
		ClientCnxn cnxn = new ClientCnxn("", new StaticHostProvider(new LinkedList()), 1, new ZooKeeper("", 1, new SimpleClient()), (ClientWatchManager) null, null, true);

		fixture.initialize(cnxn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: A HostProvider may not be empty!
		//       at org.apache.zookeeper.client.StaticHostProvider.<init>(StaticHostProvider.java:69)
	}

	/**
	 * Run the void initialize(ClientCnxn) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testInitialize_4()
		throws Exception {
		ZooKeeperSaslClient fixture = new ZooKeeperSaslClient("");
		ClientCnxn cnxn = new ClientCnxn("", new StaticHostProvider(new LinkedList()), 1, new ZooKeeper("", 1, new SimpleClient()), (ClientWatchManager) null, null, true);

		fixture.initialize(cnxn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: A HostProvider may not be empty!
		//       at org.apache.zookeeper.client.StaticHostProvider.<init>(StaticHostProvider.java:69)
	}

	/**
	 * Run the void initialize(ClientCnxn) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testInitialize_5()
		throws Exception {
		ZooKeeperSaslClient fixture = new ZooKeeperSaslClient("");
		ClientCnxn cnxn = new ClientCnxn("", new StaticHostProvider(new LinkedList()), 1, new ZooKeeper("", 1, new SimpleClient()), (ClientWatchManager) null, null, true);

		fixture.initialize(cnxn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: A HostProvider may not be empty!
		//       at org.apache.zookeeper.client.StaticHostProvider.<init>(StaticHostProvider.java:69)
	}

	/**
	 * Run the void initialize(ClientCnxn) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testInitialize_6()
		throws Exception {
		ZooKeeperSaslClient fixture = new ZooKeeperSaslClient("");
		ClientCnxn cnxn = new ClientCnxn("", new StaticHostProvider(new LinkedList()), 1, new ZooKeeper("", 1, new SimpleClient()), (ClientWatchManager) null, null, true);

		fixture.initialize(cnxn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: A HostProvider may not be empty!
		//       at org.apache.zookeeper.client.StaticHostProvider.<init>(StaticHostProvider.java:69)
	}

	/**
	 * Run the boolean isComplete() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testIsComplete_1()
		throws Exception {
		ZooKeeperSaslClient fixture = new ZooKeeperSaslClient("");

		boolean result = fixture.isComplete();

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean isComplete() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testIsComplete_2()
		throws Exception {
		ZooKeeperSaslClient fixture = new ZooKeeperSaslClient("");

		boolean result = fixture.isComplete();

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean isFailed() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testIsFailed_1()
		throws Exception {
		ZooKeeperSaslClient fixture = new ZooKeeperSaslClient("");

		boolean result = fixture.isFailed();

		// add additional test code here
		assertEquals(true, result);
	}

	/**
	 * Run the boolean isFailed() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testIsFailed_2()
		throws Exception {
		ZooKeeperSaslClient fixture = new ZooKeeperSaslClient("");

		boolean result = fixture.isFailed();

		// add additional test code here
		assertEquals(true, result);
	}

	/**
	 * Run the void respondToServer(byte[],ClientCnxn) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testRespondToServer_1()
		throws Exception {
		ZooKeeperSaslClient fixture = new ZooKeeperSaslClient("");
		byte[] serverToken = new byte[] {};
		ClientCnxn cnxn = new ClientCnxn("", new StaticHostProvider(new LinkedList()), 1, new ZooKeeper("", 1, new SimpleClient()), (ClientWatchManager) null, null, true);

		fixture.respondToServer(serverToken, cnxn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: A HostProvider may not be empty!
		//       at org.apache.zookeeper.client.StaticHostProvider.<init>(StaticHostProvider.java:69)
	}

	/**
	 * Run the void respondToServer(byte[],ClientCnxn) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testRespondToServer_2()
		throws Exception {
		ZooKeeperSaslClient fixture = new ZooKeeperSaslClient("");
		byte[] serverToken = new byte[] {};
		ClientCnxn cnxn = new ClientCnxn("", new StaticHostProvider(new LinkedList()), 1, new ZooKeeper("", 1, new SimpleClient()), (ClientWatchManager) null, null, true);

		fixture.respondToServer(serverToken, cnxn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: A HostProvider may not be empty!
		//       at org.apache.zookeeper.client.StaticHostProvider.<init>(StaticHostProvider.java:69)
	}

	/**
	 * Run the void respondToServer(byte[],ClientCnxn) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testRespondToServer_3()
		throws Exception {
		ZooKeeperSaslClient fixture = new ZooKeeperSaslClient("");
		byte[] serverToken = new byte[] {};
		ClientCnxn cnxn = new ClientCnxn("", new StaticHostProvider(new LinkedList()), 1, new ZooKeeper("", 1, new SimpleClient()), (ClientWatchManager) null, null, true);

		fixture.respondToServer(serverToken, cnxn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: A HostProvider may not be empty!
		//       at org.apache.zookeeper.client.StaticHostProvider.<init>(StaticHostProvider.java:69)
	}

	/**
	 * Run the void respondToServer(byte[],ClientCnxn) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testRespondToServer_4()
		throws Exception {
		ZooKeeperSaslClient fixture = new ZooKeeperSaslClient("");
		byte[] serverToken = new byte[] {};
		ClientCnxn cnxn = new ClientCnxn("", new StaticHostProvider(new LinkedList()), 1, new ZooKeeper("", 1, new SimpleClient()), (ClientWatchManager) null, null, true);

		fixture.respondToServer(serverToken, cnxn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: A HostProvider may not be empty!
		//       at org.apache.zookeeper.client.StaticHostProvider.<init>(StaticHostProvider.java:69)
	}

	/**
	 * Run the void respondToServer(byte[],ClientCnxn) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testRespondToServer_5()
		throws Exception {
		ZooKeeperSaslClient fixture = new ZooKeeperSaslClient("");
		byte[] serverToken = new byte[] {};
		ClientCnxn cnxn = new ClientCnxn("", new StaticHostProvider(new LinkedList()), 1, new ZooKeeper("", 1, new SimpleClient()), (ClientWatchManager) null, null, true);

		fixture.respondToServer(serverToken, cnxn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: A HostProvider may not be empty!
		//       at org.apache.zookeeper.client.StaticHostProvider.<init>(StaticHostProvider.java:69)
	}

	/**
	 * Run the void respondToServer(byte[],ClientCnxn) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testRespondToServer_6()
		throws Exception {
		ZooKeeperSaslClient fixture = new ZooKeeperSaslClient("");
		byte[] serverToken = new byte[] {};
		ClientCnxn cnxn = new ClientCnxn("", new StaticHostProvider(new LinkedList()), 1, new ZooKeeper("", 1, new SimpleClient()), (ClientWatchManager) null, null, true);

		fixture.respondToServer(serverToken, cnxn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: A HostProvider may not be empty!
		//       at org.apache.zookeeper.client.StaticHostProvider.<init>(StaticHostProvider.java:69)
	}

	/**
	 * Run the void respondToServer(byte[],ClientCnxn) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testRespondToServer_7()
		throws Exception {
		ZooKeeperSaslClient fixture = new ZooKeeperSaslClient("");
		byte[] serverToken = new byte[] {};
		ClientCnxn cnxn = new ClientCnxn("", new StaticHostProvider(new LinkedList()), 1, new ZooKeeper("", 1, new SimpleClient()), (ClientWatchManager) null, null, true);

		fixture.respondToServer(serverToken, cnxn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: A HostProvider may not be empty!
		//       at org.apache.zookeeper.client.StaticHostProvider.<init>(StaticHostProvider.java:69)
	}

	/**
	 * Run the void respondToServer(byte[],ClientCnxn) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testRespondToServer_8()
		throws Exception {
		ZooKeeperSaslClient fixture = new ZooKeeperSaslClient("");
		byte[] serverToken = new byte[] {};
		ClientCnxn cnxn = new ClientCnxn("", new StaticHostProvider(new LinkedList()), 1, new ZooKeeper("", 1, new SimpleClient()), (ClientWatchManager) null, null, true);

		fixture.respondToServer(serverToken, cnxn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: A HostProvider may not be empty!
		//       at org.apache.zookeeper.client.StaticHostProvider.<init>(StaticHostProvider.java:69)
	}

	/**
	 * Run the void respondToServer(byte[],ClientCnxn) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testRespondToServer_9()
		throws Exception {
		ZooKeeperSaslClient fixture = new ZooKeeperSaslClient("");
		byte[] serverToken = new byte[] {};
		ClientCnxn cnxn = new ClientCnxn("", new StaticHostProvider(new LinkedList()), 1, new ZooKeeper("", 1, new SimpleClient()), (ClientWatchManager) null, null, true);

		fixture.respondToServer(serverToken, cnxn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: A HostProvider may not be empty!
		//       at org.apache.zookeeper.client.StaticHostProvider.<init>(StaticHostProvider.java:69)
	}

	/**
	 * Run the void respondToServer(byte[],ClientCnxn) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testRespondToServer_10()
		throws Exception {
		ZooKeeperSaslClient fixture = new ZooKeeperSaslClient("");
		byte[] serverToken = new byte[] {};
		ClientCnxn cnxn = new ClientCnxn("", new StaticHostProvider(new LinkedList()), 1, new ZooKeeper("", 1, new SimpleClient()), (ClientWatchManager) null, null, true);

		fixture.respondToServer(serverToken, cnxn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: A HostProvider may not be empty!
		//       at org.apache.zookeeper.client.StaticHostProvider.<init>(StaticHostProvider.java:69)
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(ZooKeeperSaslClientTest.class);
	}
}