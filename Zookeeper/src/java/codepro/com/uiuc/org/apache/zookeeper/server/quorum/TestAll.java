package com.uiuc.org.apache.zookeeper.server.quorum;

import org.junit.runner.JUnitCore;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * The class <code>TestAll</code> builds a suite that can be used to run all
 * of the tests within its package as well as within any subpackages of its
 * package.
 *
 * @generatedBy CodePro at 10/16/13 8:17 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
	ElectionTest.class,
	ObserverTest.class,
	QuorumCnxManagerTest.class,
	LeaderBeanTest.class,
	ObserverRequestProcessorTest.class,
	FollowerMXBeanTest.class,
	LocalPeerMXBeanTest.class,
	ServerMXBeanTest.class,
	LeaderMXBeanTest.class,
	ReadOnlyZooKeeperServerTest.class,
	LeaderTest.class,
	FollowerTest.class,
	ServerBeanTest.class,
	QuorumStatsTest.class,
	ReadOnlyBeanTest.class,
	ProposalRequestProcessorTest.class,
	AuthFastLeaderElectionTest.class,
	LearnerSessionTrackerTest.class,
	LearnerHandlerTest.class,
	RemotePeerMXBeanTest.class,
	RemotePeerBeanTest.class,
	LearnerSyncRequestTest.class,
	LocalPeerBeanTest.class,
	LeaderElectionMXBeanTest.class,
	LeaderZooKeeperServerTest.class,
	ObserverZooKeeperServerTest.class,
	CommitProcessorTest.class,
	QuorumBeanTest.class,
	ObserverMXBeanTest.class,
	StateSummaryTest.class,
	LeaderElectionBeanTest.class,
	ReadOnlyRequestProcessorTest.class,
	FastLeaderElectionTest.class,
	FollowerRequestProcessorTest.class,
	LearnerTest.class,
	SendAckRequestProcessorTest.class,
	LeaderElectionTest.class,
	FollowerZooKeeperServerTest.class,
	AckRequestProcessorTest.class,
	FollowerBeanTest.class,
	LearnerZooKeeperServerTest.class,
	VoteTest.class,
	QuorumPeerTest.class,
})
public class TestAll {

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	public static void main(String[] args) {
		JUnitCore.runClasses(new Class[] { TestAll.class });
	}
}
