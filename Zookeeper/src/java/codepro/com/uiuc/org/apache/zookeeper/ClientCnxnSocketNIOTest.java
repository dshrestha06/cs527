package com.uiuc.org.apache.zookeeper;

import static org.junit.Assert.assertNotNull;

import org.apache.zookeeper.ClientCnxnSocketNIO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The class <code>ClientCnxnSocketNIOTest</code> contains tests for the class <code>{@link ClientCnxnSocketNIO}</code>.
 *
 * @generatedBy CodePro at 10/16/13 7:23 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class ClientCnxnSocketNIOTest {
	/**
	 * Run the ClientCnxnSocketNIO() constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testClientCnxnSocketNIO_1()
		throws Exception {

		ClientCnxnSocketNIO result = new ClientCnxnSocketNIO();

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(ClientCnxnSocketNIOTest.class);
	}
}