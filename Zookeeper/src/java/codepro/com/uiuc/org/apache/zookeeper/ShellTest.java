package com.uiuc.org.apache.zookeeper;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import org.apache.zookeeper.Shell;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>ShellTest</code> contains tests for the class <code>{@link Shell}</code>.
 *
 * @generatedBy CodePro at 10/16/13 6:57 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class ShellTest {
	/**
	 * Run the String execCommand(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testExecCommand_1()
		throws Exception {

		String result = Shell.execCommand();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.ArrayIndexOutOfBoundsException: 0
		//       at java.lang.ProcessBuilder.start(ProcessBuilder.java:444)
		//       at org.apache.zookeeper.Shell.runCommand(Shell.java:170)
		//       at org.apache.zookeeper.Shell.run(Shell.java:152)
		//       at org.apache.zookeeper.Shell$ShellCommandExecutor.execute(Shell.java:345)
		//       at org.apache.zookeeper.Shell.execCommand(Shell.java:431)
		//       at org.apache.zookeeper.Shell.execCommand(Shell.java:414)
		assertNotNull(result);
	}

	/**
	 * Run the String execCommand(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testExecCommand_2()
		throws Exception {

		String result = Shell.execCommand();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.ArrayIndexOutOfBoundsException: 0
		//       at java.lang.ProcessBuilder.start(ProcessBuilder.java:444)
		//       at org.apache.zookeeper.Shell.runCommand(Shell.java:170)
		//       at org.apache.zookeeper.Shell.run(Shell.java:152)
		//       at org.apache.zookeeper.Shell$ShellCommandExecutor.execute(Shell.java:345)
		//       at org.apache.zookeeper.Shell.execCommand(Shell.java:431)
		//       at org.apache.zookeeper.Shell.execCommand(Shell.java:414)
		assertNotNull(result);
	}

	/**
	 * Run the String execCommand(Map<String,String>,String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testExecCommand_3()
		throws Exception {
		Map<String, String> env = new HashMap();

		String result = Shell.execCommand(env);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.ArrayIndexOutOfBoundsException: 0
		//       at java.lang.ProcessBuilder.start(ProcessBuilder.java:444)
		//       at org.apache.zookeeper.Shell.runCommand(Shell.java:170)
		//       at org.apache.zookeeper.Shell.run(Shell.java:152)
		//       at org.apache.zookeeper.Shell$ShellCommandExecutor.execute(Shell.java:345)
		//       at org.apache.zookeeper.Shell.execCommand(Shell.java:431)
		//       at org.apache.zookeeper.Shell.execCommand(Shell.java:445)
		assertNotNull(result);
	}

	/**
	 * Run the String execCommand(Map<String,String>,String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testExecCommand_4()
		throws Exception {
		Map<String, String> env = new HashMap();

		String result = Shell.execCommand(env);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.ArrayIndexOutOfBoundsException: 0
		//       at java.lang.ProcessBuilder.start(ProcessBuilder.java:444)
		//       at org.apache.zookeeper.Shell.runCommand(Shell.java:170)
		//       at org.apache.zookeeper.Shell.run(Shell.java:152)
		//       at org.apache.zookeeper.Shell$ShellCommandExecutor.execute(Shell.java:345)
		//       at org.apache.zookeeper.Shell.execCommand(Shell.java:431)
		//       at org.apache.zookeeper.Shell.execCommand(Shell.java:445)
		assertNotNull(result);
	}

	/**
	 * Run the String execCommand(Map<String,String>,String[],long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testExecCommand_5()
		throws Exception {
		Map<String, String> env = new HashMap();
		String[] cmd = new String[] {};
		long timeout = 1L;

		String result = Shell.execCommand(env, cmd, timeout);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.ArrayIndexOutOfBoundsException: 0
		//       at java.lang.ProcessBuilder.start(ProcessBuilder.java:444)
		//       at org.apache.zookeeper.Shell.runCommand(Shell.java:170)
		//       at org.apache.zookeeper.Shell.run(Shell.java:152)
		//       at org.apache.zookeeper.Shell$ShellCommandExecutor.execute(Shell.java:345)
		//       at org.apache.zookeeper.Shell.execCommand(Shell.java:431)
		assertNotNull(result);
	}

	/**
	 * Run the String execCommand(Map<String,String>,String[],long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testExecCommand_6()
		throws Exception {
		Map<String, String> env = new HashMap();
		String[] cmd = new String[] {};
		long timeout = 1L;

		String result = Shell.execCommand(env, cmd, timeout);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.ArrayIndexOutOfBoundsException: 0
		//       at java.lang.ProcessBuilder.start(ProcessBuilder.java:444)
		//       at org.apache.zookeeper.Shell.runCommand(Shell.java:170)
		//       at org.apache.zookeeper.Shell.run(Shell.java:152)
		//       at org.apache.zookeeper.Shell$ShellCommandExecutor.execute(Shell.java:345)
		//       at org.apache.zookeeper.Shell.execCommand(Shell.java:431)
		assertNotNull(result);
	}

	/**
	 * Run the int getExitCode() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testGetExitCode_1()
		throws Exception {
		Shell fixture = new org.apache.zookeeper.Shell.ShellCommandExecutor(new String[] {}, new File(""), new HashMap(), 1L);

		int result = fixture.getExitCode();

		// add additional test code here
		assertEquals(0, result);
	}

	/**
	 * Run the String[] getGET_PERMISSION_COMMAND() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testGetGET_PERMISSION_COMMAND_1()
		throws Exception {

		String[] result = Shell.getGET_PERMISSION_COMMAND();

		// add additional test code here
		assertNotNull(result);
		assertEquals(2, result.length);
		assertEquals("ls", result[0]);
		assertEquals("-ld", result[1]);
	}

	/**
	 * Run the String[] getGET_PERMISSION_COMMAND() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testGetGET_PERMISSION_COMMAND_2()
		throws Exception {

		String[] result = Shell.getGET_PERMISSION_COMMAND();

		// add additional test code here
		assertNotNull(result);
		assertEquals(2, result.length);
		assertEquals("ls", result[0]);
		assertEquals("-ld", result[1]);
	}

	/**
	 * Run the String[] getGroupsCommand() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testGetGroupsCommand_1()
		throws Exception {

		String[] result = Shell.getGroupsCommand();

		// add additional test code here
		assertNotNull(result);
		assertEquals(3, result.length);
		assertEquals("bash", result[0]);
		assertEquals("-c", result[1]);
		assertEquals("groups", result[2]);
	}

	/**
	 * Run the String[] getGroupsForUserCommand(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testGetGroupsForUserCommand_1()
		throws Exception {
		String user = "";

		String[] result = Shell.getGroupsForUserCommand(user);

		// add additional test code here
		assertNotNull(result);
		assertEquals(3, result.length);
		assertEquals("bash", result[0]);
		assertEquals("-c", result[1]);
		assertEquals("id -Gn ", result[2]);
	}

	/**
	 * Run the Process getProcess() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testGetProcess_1()
		throws Exception {
		Shell fixture = new org.apache.zookeeper.Shell.ShellCommandExecutor(new String[] {}, new File(""), new HashMap(), 1L);

		Process result = fixture.getProcess();

		// add additional test code here
		assertEquals(null, result);
	}

	/**
	 * Run the String[] getUlimitMemoryCommand(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testGetUlimitMemoryCommand_1()
		throws Exception {
		int memoryLimit = 1;

		String[] result = Shell.getUlimitMemoryCommand(memoryLimit);

		// add additional test code here
		assertEquals(null, result);
	}

	/**
	 * Run the String[] getUlimitMemoryCommand(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testGetUlimitMemoryCommand_2()
		throws Exception {
		int memoryLimit = 1;

		String[] result = Shell.getUlimitMemoryCommand(memoryLimit);

		// add additional test code here
		assertEquals(null, result);
	}

	/**
	 * Run the boolean isTimedOut() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testIsTimedOut_1()
		throws Exception {
		Shell fixture = new org.apache.zookeeper.Shell.ShellCommandExecutor(new String[] {}, new File(""), new HashMap(), 1L);

		boolean result = fixture.isTimedOut();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.Shell.isTimedOut(Shell.java:395)
		assertTrue(result);
	}

	/**
	 * Run the boolean isTimedOut() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testIsTimedOut_2()
		throws Exception {
		Shell fixture = new org.apache.zookeeper.Shell.ShellCommandExecutor(new String[] {}, new File(""), new HashMap(), 1L);

		boolean result = fixture.isTimedOut();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.Shell.isTimedOut(Shell.java:395)
		assertTrue(result);
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testRun_1()
		throws Exception {
		Shell fixture = new org.apache.zookeeper.Shell.ShellCommandExecutor(new String[] {}, new File(""), new HashMap(), 1L);

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.ArrayIndexOutOfBoundsException: 0
		//       at java.lang.ProcessBuilder.start(ProcessBuilder.java:444)
		//       at org.apache.zookeeper.Shell.runCommand(Shell.java:170)
		//       at org.apache.zookeeper.Shell.run(Shell.java:152)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testRun_2()
		throws Exception {
		Shell fixture = new org.apache.zookeeper.Shell.ShellCommandExecutor(new String[] {}, new File(""), new HashMap(), 1L);

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.ArrayIndexOutOfBoundsException: 0
		//       at java.lang.ProcessBuilder.start(ProcessBuilder.java:444)
		//       at org.apache.zookeeper.Shell.runCommand(Shell.java:170)
		//       at org.apache.zookeeper.Shell.run(Shell.java:152)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testRun_3()
		throws Exception {
		Shell fixture = new org.apache.zookeeper.Shell.ShellCommandExecutor(new String[] {}, new File(""), new HashMap(), 1L);

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.ArrayIndexOutOfBoundsException: 0
		//       at java.lang.ProcessBuilder.start(ProcessBuilder.java:444)
		//       at org.apache.zookeeper.Shell.runCommand(Shell.java:170)
		//       at org.apache.zookeeper.Shell.run(Shell.java:152)
	}

	/**
	 * Run the void setEnvironment(Map<String,String>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testSetEnvironment_1()
		throws Exception {
		Shell fixture = new org.apache.zookeeper.Shell.ShellCommandExecutor(new String[] {}, new File(""), new HashMap(), 1L);
		Map<String, String> env = new HashMap();

		fixture.setEnvironment(env);

		// add additional test code here
	}

	/**
	 * Run the void setWorkingDirectory(File) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testSetWorkingDirectory_1()
		throws Exception {
		Shell fixture = new org.apache.zookeeper.Shell.ShellCommandExecutor(new String[] {}, new File(""), new HashMap(), 1L);
		File dir = new File("");

		fixture.setWorkingDirectory(dir);

		// add additional test code here
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(ShellTest.class);
	}
}