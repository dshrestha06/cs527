package com.uiuc.org.apache.zookeeper.server.quorum;

import org.apache.zookeeper.server.quorum.QuorumPeer;
import org.apache.zookeeper.server.quorum.QuorumStats;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>QuorumStatsTest</code> contains tests for the class <code>{@link QuorumStats}</code>.
 *
 * @generatedBy CodePro at 10/16/13 8:14 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class QuorumStatsTest {
	/**
	 * Run the QuorumStats(Provider) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testQuorumStats_1()
		throws Exception {
		org.apache.zookeeper.server.quorum.QuorumStats.Provider provider = new QuorumPeer();

		QuorumStats result = new QuorumStats(provider);

		// add additional test code here
		assertNotNull(result);
		assertEquals("leaderelection", result.getServerState());
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(QuorumStatsTest.class);
	}
}