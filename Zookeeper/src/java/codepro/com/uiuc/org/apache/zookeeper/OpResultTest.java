package com.uiuc.org.apache.zookeeper;

import org.apache.zookeeper.OpResult;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>OpResultTest</code> contains tests for the class <code>{@link OpResult}</code>.
 *
 * @generatedBy CodePro at 10/16/13 6:58 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class OpResultTest {
	/**
	 * Run the int getType() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testGetType_1()
		throws Exception {
		OpResult fixture = new org.apache.zookeeper.OpResult.CheckResult();

		int result = fixture.getType();

		// add additional test code here
		assertEquals(13, result);
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(OpResultTest.class);
	}
}