package com.uiuc.org.apache.zookeeper.server.quorum;

import org.apache.zookeeper.server.quorum.FastLeaderElection;
import org.apache.zookeeper.server.quorum.QuorumCnxManager;
import org.apache.zookeeper.server.quorum.QuorumPeer;
import org.apache.zookeeper.server.quorum.Vote;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>FastLeaderElectionTest</code> contains tests for the class <code>{@link FastLeaderElection}</code>.
 *
 * @generatedBy CodePro at 10/16/13 7:58 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class FastLeaderElectionTest {
	/**
	 * Run the FastLeaderElection(QuorumPeer,QuorumCnxManager) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testFastLeaderElection_1()
		throws Exception {
		QuorumPeer self = new QuorumPeer();
		QuorumCnxManager manager = new QuorumCnxManager(new QuorumPeer());

		FastLeaderElection result = new FastLeaderElection(self, manager);

		// add additional test code here
		assertNotNull(result);
		assertEquals(1L, result.getLogicalClock());
	}

	/**
	 * Run the QuorumCnxManager getCnxManager() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testGetCnxManager_1()
		throws Exception {
		FastLeaderElection fixture = new FastLeaderElection(new QuorumPeer(), new QuorumCnxManager(new QuorumPeer()));

		QuorumCnxManager result = fixture.getCnxManager();

		// add additional test code here
		assertNotNull(result);
		assertEquals(0L, result.getThreadCount());
	}

	/**
	 * Run the long getLogicalClock() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testGetLogicalClock_1()
		throws Exception {
		FastLeaderElection fixture = new FastLeaderElection(new QuorumPeer(), new QuorumCnxManager(new QuorumPeer()));

		long result = fixture.getLogicalClock();

		// add additional test code here
		assertEquals(0L, result);
	}

	/**
	 * Run the Vote getVote() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testGetVote_1()
		throws Exception {
		FastLeaderElection fixture = new FastLeaderElection(new QuorumPeer(), new QuorumCnxManager(new QuorumPeer()));

		Vote result = fixture.getVote();

		// add additional test code here
		assertNotNull(result);
		assertEquals("(-1, ffffffffffffffff, 0)", result.toString());
		assertEquals(-1L, result.getId());
		assertEquals(-1L, result.getZxid());
		assertEquals(-1L, result.getElectionEpoch());
		assertEquals(0L, result.getPeerEpoch());
	}

	/**
	 * Run the Vote lookForLeader() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testLookForLeader_1()
		throws Exception {
		FastLeaderElection fixture = new FastLeaderElection(new QuorumPeer(), new QuorumCnxManager(new QuorumPeer()));

		Vote result = fixture.lookForLeader();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getLastLoggedZxid(QuorumPeer.java:545)
		//       at org.apache.zookeeper.server.quorum.FastLeaderElection.getInitLastLoggedZxid(FastLeaderElection.java:690)
		//       at org.apache.zookeeper.server.quorum.FastLeaderElection.lookForLeader(FastLeaderElection.java:737)
		assertNotNull(result);
	}

	/**
	 * Run the Vote lookForLeader() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testLookForLeader_2()
		throws Exception {
		FastLeaderElection fixture = new FastLeaderElection(new QuorumPeer(), new QuorumCnxManager(new QuorumPeer()));

		Vote result = fixture.lookForLeader();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getLastLoggedZxid(QuorumPeer.java:545)
		//       at org.apache.zookeeper.server.quorum.FastLeaderElection.getInitLastLoggedZxid(FastLeaderElection.java:690)
		//       at org.apache.zookeeper.server.quorum.FastLeaderElection.lookForLeader(FastLeaderElection.java:737)
		assertNotNull(result);
	}

	/**
	 * Run the Vote lookForLeader() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testLookForLeader_3()
		throws Exception {
		FastLeaderElection fixture = new FastLeaderElection(new QuorumPeer(), new QuorumCnxManager(new QuorumPeer()));

		Vote result = fixture.lookForLeader();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getLastLoggedZxid(QuorumPeer.java:545)
		//       at org.apache.zookeeper.server.quorum.FastLeaderElection.getInitLastLoggedZxid(FastLeaderElection.java:690)
		//       at org.apache.zookeeper.server.quorum.FastLeaderElection.lookForLeader(FastLeaderElection.java:737)
		assertNotNull(result);
	}

	/**
	 * Run the Vote lookForLeader() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testLookForLeader_4()
		throws Exception {
		FastLeaderElection fixture = new FastLeaderElection(new QuorumPeer(), new QuorumCnxManager(new QuorumPeer()));

		Vote result = fixture.lookForLeader();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getLastLoggedZxid(QuorumPeer.java:545)
		//       at org.apache.zookeeper.server.quorum.FastLeaderElection.getInitLastLoggedZxid(FastLeaderElection.java:690)
		//       at org.apache.zookeeper.server.quorum.FastLeaderElection.lookForLeader(FastLeaderElection.java:737)
		assertNotNull(result);
	}

	/**
	 * Run the Vote lookForLeader() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testLookForLeader_5()
		throws Exception {
		FastLeaderElection fixture = new FastLeaderElection(new QuorumPeer(), new QuorumCnxManager(new QuorumPeer()));

		Vote result = fixture.lookForLeader();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getLastLoggedZxid(QuorumPeer.java:545)
		//       at org.apache.zookeeper.server.quorum.FastLeaderElection.getInitLastLoggedZxid(FastLeaderElection.java:690)
		//       at org.apache.zookeeper.server.quorum.FastLeaderElection.lookForLeader(FastLeaderElection.java:737)
		assertNotNull(result);
	}

	/**
	 * Run the Vote lookForLeader() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testLookForLeader_6()
		throws Exception {
		FastLeaderElection fixture = new FastLeaderElection(new QuorumPeer(), new QuorumCnxManager(new QuorumPeer()));

		Vote result = fixture.lookForLeader();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getLastLoggedZxid(QuorumPeer.java:545)
		//       at org.apache.zookeeper.server.quorum.FastLeaderElection.getInitLastLoggedZxid(FastLeaderElection.java:690)
		//       at org.apache.zookeeper.server.quorum.FastLeaderElection.lookForLeader(FastLeaderElection.java:737)
		assertNotNull(result);
	}

	/**
	 * Run the Vote lookForLeader() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testLookForLeader_7()
		throws Exception {
		FastLeaderElection fixture = new FastLeaderElection(new QuorumPeer(), new QuorumCnxManager(new QuorumPeer()));

		Vote result = fixture.lookForLeader();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getLastLoggedZxid(QuorumPeer.java:545)
		//       at org.apache.zookeeper.server.quorum.FastLeaderElection.getInitLastLoggedZxid(FastLeaderElection.java:690)
		//       at org.apache.zookeeper.server.quorum.FastLeaderElection.lookForLeader(FastLeaderElection.java:737)
		assertNotNull(result);
	}

	/**
	 * Run the Vote lookForLeader() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testLookForLeader_8()
		throws Exception {
		FastLeaderElection fixture = new FastLeaderElection(new QuorumPeer(), new QuorumCnxManager(new QuorumPeer()));

		Vote result = fixture.lookForLeader();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getLastLoggedZxid(QuorumPeer.java:545)
		//       at org.apache.zookeeper.server.quorum.FastLeaderElection.getInitLastLoggedZxid(FastLeaderElection.java:690)
		//       at org.apache.zookeeper.server.quorum.FastLeaderElection.lookForLeader(FastLeaderElection.java:737)
		assertNotNull(result);
	}

	/**
	 * Run the Vote lookForLeader() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testLookForLeader_9()
		throws Exception {
		FastLeaderElection fixture = new FastLeaderElection(new QuorumPeer(), new QuorumCnxManager(new QuorumPeer()));

		Vote result = fixture.lookForLeader();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getLastLoggedZxid(QuorumPeer.java:545)
		//       at org.apache.zookeeper.server.quorum.FastLeaderElection.getInitLastLoggedZxid(FastLeaderElection.java:690)
		//       at org.apache.zookeeper.server.quorum.FastLeaderElection.lookForLeader(FastLeaderElection.java:737)
		assertNotNull(result);
	}

	/**
	 * Run the Vote lookForLeader() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testLookForLeader_10()
		throws Exception {
		FastLeaderElection fixture = new FastLeaderElection(new QuorumPeer(), new QuorumCnxManager(new QuorumPeer()));

		Vote result = fixture.lookForLeader();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getLastLoggedZxid(QuorumPeer.java:545)
		//       at org.apache.zookeeper.server.quorum.FastLeaderElection.getInitLastLoggedZxid(FastLeaderElection.java:690)
		//       at org.apache.zookeeper.server.quorum.FastLeaderElection.lookForLeader(FastLeaderElection.java:737)
		assertNotNull(result);
	}

	/**
	 * Run the Vote lookForLeader() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testLookForLeader_11()
		throws Exception {
		FastLeaderElection fixture = new FastLeaderElection(new QuorumPeer(), new QuorumCnxManager(new QuorumPeer()));

		Vote result = fixture.lookForLeader();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getLastLoggedZxid(QuorumPeer.java:545)
		//       at org.apache.zookeeper.server.quorum.FastLeaderElection.getInitLastLoggedZxid(FastLeaderElection.java:690)
		//       at org.apache.zookeeper.server.quorum.FastLeaderElection.lookForLeader(FastLeaderElection.java:737)
		assertNotNull(result);
	}

	/**
	 * Run the Vote lookForLeader() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testLookForLeader_12()
		throws Exception {
		FastLeaderElection fixture = new FastLeaderElection(new QuorumPeer(), new QuorumCnxManager(new QuorumPeer()));

		Vote result = fixture.lookForLeader();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getLastLoggedZxid(QuorumPeer.java:545)
		//       at org.apache.zookeeper.server.quorum.FastLeaderElection.getInitLastLoggedZxid(FastLeaderElection.java:690)
		//       at org.apache.zookeeper.server.quorum.FastLeaderElection.lookForLeader(FastLeaderElection.java:737)
		assertNotNull(result);
	}

	/**
	 * Run the Vote lookForLeader() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testLookForLeader_13()
		throws Exception {
		FastLeaderElection fixture = new FastLeaderElection(new QuorumPeer(), new QuorumCnxManager(new QuorumPeer()));

		Vote result = fixture.lookForLeader();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getLastLoggedZxid(QuorumPeer.java:545)
		//       at org.apache.zookeeper.server.quorum.FastLeaderElection.getInitLastLoggedZxid(FastLeaderElection.java:690)
		//       at org.apache.zookeeper.server.quorum.FastLeaderElection.lookForLeader(FastLeaderElection.java:737)
		assertNotNull(result);
	}

	/**
	 * Run the Vote lookForLeader() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testLookForLeader_14()
		throws Exception {
		FastLeaderElection fixture = new FastLeaderElection(new QuorumPeer(), new QuorumCnxManager(new QuorumPeer()));

		Vote result = fixture.lookForLeader();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getLastLoggedZxid(QuorumPeer.java:545)
		//       at org.apache.zookeeper.server.quorum.FastLeaderElection.getInitLastLoggedZxid(FastLeaderElection.java:690)
		//       at org.apache.zookeeper.server.quorum.FastLeaderElection.lookForLeader(FastLeaderElection.java:737)
		assertNotNull(result);
	}

	/**
	 * Run the Vote lookForLeader() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testLookForLeader_15()
		throws Exception {
		FastLeaderElection fixture = new FastLeaderElection(new QuorumPeer(), new QuorumCnxManager(new QuorumPeer()));

		Vote result = fixture.lookForLeader();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getLastLoggedZxid(QuorumPeer.java:545)
		//       at org.apache.zookeeper.server.quorum.FastLeaderElection.getInitLastLoggedZxid(FastLeaderElection.java:690)
		//       at org.apache.zookeeper.server.quorum.FastLeaderElection.lookForLeader(FastLeaderElection.java:737)
		assertNotNull(result);
	}

	/**
	 * Run the Vote lookForLeader() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testLookForLeader_16()
		throws Exception {
		FastLeaderElection fixture = new FastLeaderElection(new QuorumPeer(), new QuorumCnxManager(new QuorumPeer()));

		Vote result = fixture.lookForLeader();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getLastLoggedZxid(QuorumPeer.java:545)
		//       at org.apache.zookeeper.server.quorum.FastLeaderElection.getInitLastLoggedZxid(FastLeaderElection.java:690)
		//       at org.apache.zookeeper.server.quorum.FastLeaderElection.lookForLeader(FastLeaderElection.java:737)
		assertNotNull(result);
	}

	/**
	 * Run the void shutdown() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testShutdown_1()
		throws Exception {
		FastLeaderElection fixture = new FastLeaderElection(new QuorumPeer(), new QuorumCnxManager(new QuorumPeer()));

		fixture.shutdown();

		// add additional test code here
	}

	/**
	 * Run the boolean totalOrderPredicate(long,long,long,long,long,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testTotalOrderPredicate_1()
		throws Exception {
		FastLeaderElection fixture = new FastLeaderElection(new QuorumPeer(), new QuorumCnxManager(new QuorumPeer()));
		long newId = 1L;
		long newZxid = 1L;
		long newEpoch = 1L;
		long curId = 1L;
		long curZxid = 1L;
		long curEpoch = 1L;

		boolean result = fixture.totalOrderPredicate(newId, newZxid, newEpoch, curId, curZxid, curEpoch);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.FastLeaderElection.totalOrderPredicate(FastLeaderElection.java:560)
		assertTrue(result);
	}

	/**
	 * Run the boolean totalOrderPredicate(long,long,long,long,long,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testTotalOrderPredicate_2()
		throws Exception {
		FastLeaderElection fixture = new FastLeaderElection(new QuorumPeer(), new QuorumCnxManager(new QuorumPeer()));
		long newId = 1L;
		long newZxid = 1L;
		long newEpoch = 1L;
		long curId = 1L;
		long curZxid = 1L;
		long curEpoch = 1L;

		boolean result = fixture.totalOrderPredicate(newId, newZxid, newEpoch, curId, curZxid, curEpoch);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.FastLeaderElection.totalOrderPredicate(FastLeaderElection.java:560)
		assertTrue(result);
	}

	/**
	 * Run the boolean totalOrderPredicate(long,long,long,long,long,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testTotalOrderPredicate_3()
		throws Exception {
		FastLeaderElection fixture = new FastLeaderElection(new QuorumPeer(), new QuorumCnxManager(new QuorumPeer()));
		long newId = 1L;
		long newZxid = 1L;
		long newEpoch = 1L;
		long curId = 1L;
		long curZxid = 1L;
		long curEpoch = 1L;

		boolean result = fixture.totalOrderPredicate(newId, newZxid, newEpoch, curId, curZxid, curEpoch);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.FastLeaderElection.totalOrderPredicate(FastLeaderElection.java:560)
		assertTrue(result);
	}

	/**
	 * Run the boolean totalOrderPredicate(long,long,long,long,long,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testTotalOrderPredicate_4()
		throws Exception {
		FastLeaderElection fixture = new FastLeaderElection(new QuorumPeer(), new QuorumCnxManager(new QuorumPeer()));
		long newId = 1L;
		long newZxid = 1L;
		long newEpoch = 1L;
		long curId = 1L;
		long curZxid = 1L;
		long curEpoch = 1L;

		boolean result = fixture.totalOrderPredicate(newId, newZxid, newEpoch, curId, curZxid, curEpoch);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.FastLeaderElection.totalOrderPredicate(FastLeaderElection.java:560)
		assertTrue(result);
	}

	/**
	 * Run the boolean totalOrderPredicate(long,long,long,long,long,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testTotalOrderPredicate_5()
		throws Exception {
		FastLeaderElection fixture = new FastLeaderElection(new QuorumPeer(), new QuorumCnxManager(new QuorumPeer()));
		long newId = 1L;
		long newZxid = 1L;
		long newEpoch = 1L;
		long curId = 1L;
		long curZxid = 1L;
		long curEpoch = 1L;

		boolean result = fixture.totalOrderPredicate(newId, newZxid, newEpoch, curId, curZxid, curEpoch);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.FastLeaderElection.totalOrderPredicate(FastLeaderElection.java:560)
		assertTrue(result);
	}

	/**
	 * Run the boolean totalOrderPredicate(long,long,long,long,long,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testTotalOrderPredicate_6()
		throws Exception {
		FastLeaderElection fixture = new FastLeaderElection(new QuorumPeer(), new QuorumCnxManager(new QuorumPeer()));
		long newId = 1L;
		long newZxid = 1L;
		long newEpoch = 1L;
		long curId = 1L;
		long curZxid = 1L;
		long curEpoch = 1L;

		boolean result = fixture.totalOrderPredicate(newId, newZxid, newEpoch, curId, curZxid, curEpoch);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.FastLeaderElection.totalOrderPredicate(FastLeaderElection.java:560)
		assertTrue(result);
	}

	/**
	 * Run the boolean totalOrderPredicate(long,long,long,long,long,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testTotalOrderPredicate_7()
		throws Exception {
		FastLeaderElection fixture = new FastLeaderElection(new QuorumPeer(), new QuorumCnxManager(new QuorumPeer()));
		long newId = 1L;
		long newZxid = 1L;
		long newEpoch = 1L;
		long curId = 1L;
		long curZxid = 1L;
		long curEpoch = 1L;

		boolean result = fixture.totalOrderPredicate(newId, newZxid, newEpoch, curId, curZxid, curEpoch);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.FastLeaderElection.totalOrderPredicate(FastLeaderElection.java:560)
		assertTrue(result);
	}

	/**
	 * Run the void updateProposal(long,long,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testUpdateProposal_1()
		throws Exception {
		FastLeaderElection fixture = new FastLeaderElection(new QuorumPeer(), new QuorumCnxManager(new QuorumPeer()));
		long leader = 1L;
		long zxid = 1L;
		long epoch = 1L;

		fixture.updateProposal(leader, zxid, epoch);

		// add additional test code here
	}

	/**
	 * Run the void updateProposal(long,long,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testUpdateProposal_2()
		throws Exception {
		FastLeaderElection fixture = new FastLeaderElection(new QuorumPeer(), new QuorumCnxManager(new QuorumPeer()));
		long leader = 1L;
		long zxid = 1L;
		long epoch = 1L;

		fixture.updateProposal(leader, zxid, epoch);

		// add additional test code here
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(FastLeaderElectionTest.class);
	}
}