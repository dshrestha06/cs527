package com.uiuc.org.apache.zookeeper.server;

import org.apache.zookeeper.server.DatadirCleanupManager;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>DatadirCleanupManagerTest</code> contains tests for the class <code>{@link DatadirCleanupManager}</code>.
 *
 * @generatedBy CodePro at 10/16/13 8:20 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class DatadirCleanupManagerTest {
	/**
	 * Run the DatadirCleanupManager(String,String,int,int) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:20 PM
	 */
	@Test
	public void testDatadirCleanupManager_1()
		throws Exception {
		String snapDir = "";
		String dataLogDir = "";
		int snapRetainCount = 1;
		int purgeInterval = 1;

		DatadirCleanupManager result = new DatadirCleanupManager(snapDir, dataLogDir, snapRetainCount, purgeInterval);

		// add additional test code here
		assertNotNull(result);
		assertEquals("", result.getSnapDir());
		assertEquals("", result.getDataLogDir());
		assertEquals(1, result.getPurgeInterval());
		assertEquals(1, result.getSnapRetainCount());
	}

	/**
	 * Run the String getDataLogDir() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:20 PM
	 */
	@Test
	public void testGetDataLogDir_1()
		throws Exception {
		DatadirCleanupManager fixture = new DatadirCleanupManager("", "", 1, 1);

		String result = fixture.getDataLogDir();

		// add additional test code here
		assertEquals("", result);
	}

	/**
	 * Run the int getPurgeInterval() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:20 PM
	 */
	@Test
	public void testGetPurgeInterval_1()
		throws Exception {
		DatadirCleanupManager fixture = new DatadirCleanupManager("", "", 1, 1);

		int result = fixture.getPurgeInterval();

		// add additional test code here
		assertEquals(1, result);
	}

	/**
	 * Run the org.apache.zookeeper.server.DatadirCleanupManager.PurgeTaskStatus getPurgeTaskStatus() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:20 PM
	 */
	@Test
	public void testGetPurgeTaskStatus_1()
		throws Exception {
		DatadirCleanupManager fixture = new DatadirCleanupManager("", "", 1, 1);

		org.apache.zookeeper.server.DatadirCleanupManager.PurgeTaskStatus result = fixture.getPurgeTaskStatus();

		// add additional test code here
		assertNotNull(result);
		assertEquals("NOT_STARTED", result.name());
		assertEquals("NOT_STARTED", result.toString());
		assertEquals(0, result.ordinal());
	}

	/**
	 * Run the String getSnapDir() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:20 PM
	 */
	@Test
	public void testGetSnapDir_1()
		throws Exception {
		DatadirCleanupManager fixture = new DatadirCleanupManager("", "", 1, 1);

		String result = fixture.getSnapDir();

		// add additional test code here
		assertEquals("", result);
	}

	/**
	 * Run the int getSnapRetainCount() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:20 PM
	 */
	@Test
	public void testGetSnapRetainCount_1()
		throws Exception {
		DatadirCleanupManager fixture = new DatadirCleanupManager("", "", 1, 1);

		int result = fixture.getSnapRetainCount();

		// add additional test code here
		assertEquals(1, result);
	}

	/**
	 * Run the void shutdown() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:20 PM
	 */
	@Test
	public void testShutdown_1()
		throws Exception {
		DatadirCleanupManager fixture = new DatadirCleanupManager("", "", 1, 1);

		fixture.shutdown();

		// add additional test code here
	}

	/**
	 * Run the void shutdown() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:20 PM
	 */
	@Test
	public void testShutdown_2()
		throws Exception {
		DatadirCleanupManager fixture = new DatadirCleanupManager("", "", 1, 1);

		fixture.shutdown();

		// add additional test code here
	}

	/**
	 * Run the void start() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:20 PM
	 */
	@Test
	public void testStart_1()
		throws Exception {
		DatadirCleanupManager fixture = new DatadirCleanupManager("", "", 1, 1);

		fixture.start();

		// add additional test code here
	}

	/**
	 * Run the void start() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:20 PM
	 */
	@Test
	public void testStart_2()
		throws Exception {
		DatadirCleanupManager fixture = new DatadirCleanupManager("", "", 1, 0);

		fixture.start();

		// add additional test code here
	}

	/**
	 * Run the void start() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:20 PM
	 */
	@Test
	public void testStart_3()
		throws Exception {
		DatadirCleanupManager fixture = new DatadirCleanupManager("", "", 1, 1);

		fixture.start();

		// add additional test code here
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:20 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:20 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 8:20 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(DatadirCleanupManagerTest.class);
	}
}