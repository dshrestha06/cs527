package com.uiuc.org.apache.zookeeper.server.quorum;

import java.net.InetSocketAddress;
import org.apache.zookeeper.server.quorum.QuorumPeer;
import org.apache.zookeeper.server.quorum.RemotePeerBean;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>RemotePeerBeanTest</code> contains tests for the class <code>{@link RemotePeerBean}</code>.
 *
 * @generatedBy CodePro at 10/16/13 6:55 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class RemotePeerBeanTest {
	/**
	 * Run the RemotePeerBean(QuorumServer) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:55 PM
	 */
	@Test
	public void testRemotePeerBean_1()
		throws Exception {
		org.apache.zookeeper.server.quorum.QuorumPeer.QuorumServer peer = new org.apache.zookeeper.server.quorum.QuorumPeer.QuorumServer(1L, new InetSocketAddress(1));

		RemotePeerBean result = new RemotePeerBean(peer);

		// add additional test code here
		assertNotNull(result);
		assertEquals("replica.1", result.getName());
		assertEquals(false, result.isHidden());
		assertEquals("0.0.0.0:1", result.getQuorumAddress());
	}

	/**
	 * Run the String getName() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:55 PM
	 */
	@Test
	public void testGetName_1()
		throws Exception {
		RemotePeerBean fixture = new RemotePeerBean(new org.apache.zookeeper.server.quorum.QuorumPeer.QuorumServer(1L, new InetSocketAddress(1)));

		String result = fixture.getName();

		// add additional test code here
		assertEquals("replica.1", result);
	}

	/**
	 * Run the String getQuorumAddress() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:55 PM
	 */
	@Test
	public void testGetQuorumAddress_1()
		throws Exception {
		RemotePeerBean fixture = new RemotePeerBean(new org.apache.zookeeper.server.quorum.QuorumPeer.QuorumServer(1L, new InetSocketAddress(1)));

		String result = fixture.getQuorumAddress();

		// add additional test code here
		assertEquals("0.0.0.0:1", result);
	}

	/**
	 * Run the boolean isHidden() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:55 PM
	 */
	@Test
	public void testIsHidden_1()
		throws Exception {
		RemotePeerBean fixture = new RemotePeerBean(new org.apache.zookeeper.server.quorum.QuorumPeer.QuorumServer(1L, new InetSocketAddress(1)));

		boolean result = fixture.isHidden();

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 6:55 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 6:55 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 6:55 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(RemotePeerBeanTest.class);
	}
}