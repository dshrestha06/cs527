package com.uiuc.org.apache.zookeeper.server;

import org.apache.zookeeper.server.ServerConfig;
import org.apache.zookeeper.server.ZooKeeperServerMain;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>ZooKeeperServerMainTest</code> contains tests for the class <code>{@link ZooKeeperServerMain}</code>.
 *
 * @generatedBy CodePro at 10/16/13 8:27 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class ZooKeeperServerMainTest {
	/**
	 * Run the ZooKeeperServerMain() constructor test.
	 *
	 * @generatedBy CodePro at 10/16/13 8:27 PM
	 */
	@Test
	public void testZooKeeperServerMain_1()
		throws Exception {
		ZooKeeperServerMain result = new ZooKeeperServerMain();
		assertNotNull(result);
		// add additional test code here
	}

	/**
	 * Run the void initializeAndRun(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:27 PM
	 */
	@Test
	public void testInitializeAndRun_1()
		throws Exception {
		ZooKeeperServerMain fixture = new ZooKeeperServerMain();
		String[] args = new String[] {""};

		fixture.initializeAndRun(args);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.QuorumPeerConfig.parse(QuorumPeerConfig.java:121)
		//       at org.apache.zookeeper.server.ServerConfig.parse(ServerConfig.java:79)
		//       at org.apache.zookeeper.server.ZooKeeperServerMain.initializeAndRun(ZooKeeperServerMain.java:81)
	}

	/**
	 * Run the void initializeAndRun(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:27 PM
	 */
	@Test
	public void testInitializeAndRun_2()
		throws Exception {
		ZooKeeperServerMain fixture = new ZooKeeperServerMain();
		String[] args = new String[] {""};

		fixture.initializeAndRun(args);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.QuorumPeerConfig.parse(QuorumPeerConfig.java:121)
		//       at org.apache.zookeeper.server.ServerConfig.parse(ServerConfig.java:79)
		//       at org.apache.zookeeper.server.ZooKeeperServerMain.initializeAndRun(ZooKeeperServerMain.java:81)
	}

	/**
	 * Run the void initializeAndRun(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:27 PM
	 */
	@Test
	public void testInitializeAndRun_3()
		throws Exception {
		ZooKeeperServerMain fixture = new ZooKeeperServerMain();
		String[] args = new String[] {};

		fixture.initializeAndRun(args);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Invalid args:[]
		//       at org.apache.zookeeper.server.ServerConfig.parse(ServerConfig.java:56)
		//       at org.apache.zookeeper.server.ZooKeeperServerMain.initializeAndRun(ZooKeeperServerMain.java:83)
	}

	/**
	 * Run the void initializeAndRun(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:27 PM
	 */
	@Test
	public void testInitializeAndRun_4()
		throws Exception {
		ZooKeeperServerMain fixture = new ZooKeeperServerMain();
		String[] args = new String[] {};

		fixture.initializeAndRun(args);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Invalid args:[]
		//       at org.apache.zookeeper.server.ServerConfig.parse(ServerConfig.java:56)
		//       at org.apache.zookeeper.server.ZooKeeperServerMain.initializeAndRun(ZooKeeperServerMain.java:83)
	}

	/**
	 * Run the void initializeAndRun(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:27 PM
	 */
	@Test
	public void testInitializeAndRun_5()
		throws Exception {
		ZooKeeperServerMain fixture = new ZooKeeperServerMain();
		String[] args = new String[] {""};

		fixture.initializeAndRun(args);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.QuorumPeerConfig.parse(QuorumPeerConfig.java:121)
		//       at org.apache.zookeeper.server.ServerConfig.parse(ServerConfig.java:79)
		//       at org.apache.zookeeper.server.ZooKeeperServerMain.initializeAndRun(ZooKeeperServerMain.java:81)
	}

	/**
	 * Run the void initializeAndRun(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:27 PM
	 */
	@Test
	public void testInitializeAndRun_6()
		throws Exception {
		ZooKeeperServerMain fixture = new ZooKeeperServerMain();
		String[] args = new String[] {""};

		fixture.initializeAndRun(args);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.QuorumPeerConfig.parse(QuorumPeerConfig.java:121)
		//       at org.apache.zookeeper.server.ServerConfig.parse(ServerConfig.java:79)
		//       at org.apache.zookeeper.server.ZooKeeperServerMain.initializeAndRun(ZooKeeperServerMain.java:81)
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:27 PM
	 */
	@Test
	public void testMain_1()
		throws Exception {
		String[] args = new String[] {};

		ZooKeeperServerMain.main(args);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Exit while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkExit(CodeProJUnitSecurityManager.java:57)
		//       at java.lang.Runtime.exit(Runtime.java:88)
		//       at java.lang.System.exit(System.java:904)
		//       at org.apache.zookeeper.server.ZooKeeperServerMain.main(ZooKeeperServerMain.java:57)
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:27 PM
	 */
	@Test
	public void testMain_2()
		throws Exception {
		String[] args = new String[] {};

		ZooKeeperServerMain.main(args);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Exit while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkExit(CodeProJUnitSecurityManager.java:57)
		//       at java.lang.Runtime.exit(Runtime.java:88)
		//       at java.lang.System.exit(System.java:904)
		//       at org.apache.zookeeper.server.ZooKeeperServerMain.main(ZooKeeperServerMain.java:57)
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:27 PM
	 */
	@Test
	public void testMain_3()
		throws Exception {
		String[] args = new String[] {};

		ZooKeeperServerMain.main(args);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Exit while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkExit(CodeProJUnitSecurityManager.java:57)
		//       at java.lang.Runtime.exit(Runtime.java:88)
		//       at java.lang.System.exit(System.java:904)
		//       at org.apache.zookeeper.server.ZooKeeperServerMain.main(ZooKeeperServerMain.java:57)
	}

	/**
	 * Run the void runFromConfig(ServerConfig) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:27 PM
	 */
	@Test
	public void testRunFromConfig_1()
		throws Exception {
		ZooKeeperServerMain fixture = new ZooKeeperServerMain();
		ServerConfig config = new ServerConfig();

		fixture.runFromConfig(config);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.io.File.<init>(File.java:222)
		//       at org.apache.zookeeper.server.ZooKeeperServerMain.runFromConfig(ZooKeeperServerMain.java:103)
	}

	/**
	 * Run the void runFromConfig(ServerConfig) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:27 PM
	 */
	@Test
	public void testRunFromConfig_2()
		throws Exception {
		ZooKeeperServerMain fixture = new ZooKeeperServerMain();
		ServerConfig config = new ServerConfig();

		fixture.runFromConfig(config);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.io.File.<init>(File.java:222)
		//       at org.apache.zookeeper.server.ZooKeeperServerMain.runFromConfig(ZooKeeperServerMain.java:103)
	}

	/**
	 * Run the void runFromConfig(ServerConfig) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:27 PM
	 */
	@Test
	public void testRunFromConfig_3()
		throws Exception {
		ZooKeeperServerMain fixture = new ZooKeeperServerMain();
		ServerConfig config = new ServerConfig();

		fixture.runFromConfig(config);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.io.File.<init>(File.java:222)
		//       at org.apache.zookeeper.server.ZooKeeperServerMain.runFromConfig(ZooKeeperServerMain.java:103)
	}

	/**
	 * Run the void runFromConfig(ServerConfig) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:27 PM
	 */
	@Test
	public void testRunFromConfig_4()
		throws Exception {
		ZooKeeperServerMain fixture = new ZooKeeperServerMain();
		ServerConfig config = new ServerConfig();

		fixture.runFromConfig(config);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.io.File.<init>(File.java:222)
		//       at org.apache.zookeeper.server.ZooKeeperServerMain.runFromConfig(ZooKeeperServerMain.java:103)
	}

	/**
	 * Run the void runFromConfig(ServerConfig) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:27 PM
	 */
	@Test
	public void testRunFromConfig_5()
		throws Exception {
		ZooKeeperServerMain fixture = new ZooKeeperServerMain();
		ServerConfig config = new ServerConfig();

		fixture.runFromConfig(config);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.io.File.<init>(File.java:222)
		//       at org.apache.zookeeper.server.ZooKeeperServerMain.runFromConfig(ZooKeeperServerMain.java:103)
	}

	/**
	 * Run the void runFromConfig(ServerConfig) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:27 PM
	 */
	@Test
	public void testRunFromConfig_6()
		throws Exception {
		ZooKeeperServerMain fixture = new ZooKeeperServerMain();
		ServerConfig config = new ServerConfig();

		fixture.runFromConfig(config);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.io.File.<init>(File.java:222)
		//       at org.apache.zookeeper.server.ZooKeeperServerMain.runFromConfig(ZooKeeperServerMain.java:103)
	}

	/**
	 * Run the void runFromConfig(ServerConfig) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:27 PM
	 */
	@Test
	public void testRunFromConfig_7()
		throws Exception {
		ZooKeeperServerMain fixture = new ZooKeeperServerMain();
		ServerConfig config = new ServerConfig();

		fixture.runFromConfig(config);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.io.File.<init>(File.java:222)
		//       at org.apache.zookeeper.server.ZooKeeperServerMain.runFromConfig(ZooKeeperServerMain.java:103)
	}

	/**
	 * Run the void runFromConfig(ServerConfig) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:27 PM
	 */
	@Test
	public void testRunFromConfig_8()
		throws Exception {
		ZooKeeperServerMain fixture = new ZooKeeperServerMain();
		ServerConfig config = new ServerConfig();

		fixture.runFromConfig(config);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.io.File.<init>(File.java:222)
		//       at org.apache.zookeeper.server.ZooKeeperServerMain.runFromConfig(ZooKeeperServerMain.java:103)
	}

	/**
	 * Run the void shutdown() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:27 PM
	 */
	@Test
	public void testShutdown_1()
		throws Exception {
		ZooKeeperServerMain fixture = new ZooKeeperServerMain();

		fixture.shutdown();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.ZooKeeperServerMain.shutdown(ZooKeeperServerMain.java:127)
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:27 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:27 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 8:27 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(ZooKeeperServerMainTest.class);
	}
}