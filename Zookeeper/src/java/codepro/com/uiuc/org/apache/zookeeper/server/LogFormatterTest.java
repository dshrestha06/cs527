package com.uiuc.org.apache.zookeeper.server;

import java.io.FileNotFoundException;
import org.apache.zookeeper.server.LogFormatter;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>LogFormatterTest</code> contains tests for the class <code>{@link LogFormatter}</code>.
 *
 * @generatedBy CodePro at 10/16/13 8:17 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class LogFormatterTest {
	/**
	 * Run the LogFormatter() constructor test.
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@Test
	public void testLogFormatter_1()
		throws Exception {
		LogFormatter result = new LogFormatter();
		assertNotNull(result);
		// add additional test code here
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@Test(expected = java.io.FileNotFoundException.class)
	public void testMain_1()
		throws Exception {
		String[] args = new String[] {""};

		LogFormatter.main(args);

		// add additional test code here
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@Test(expected = java.io.FileNotFoundException.class)
	public void testMain_2()
		throws Exception {
		String[] args = new String[] {""};

		LogFormatter.main(args);

		// add additional test code here
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@Test(expected = java.io.FileNotFoundException.class)
	public void testMain_3()
		throws Exception {
		String[] args = new String[] {""};

		LogFormatter.main(args);

		// add additional test code here
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@Test(expected = java.io.FileNotFoundException.class)
	public void testMain_4()
		throws Exception {
		String[] args = new String[] {""};

		LogFormatter.main(args);

		// add additional test code here
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@Test(expected = java.io.FileNotFoundException.class)
	public void testMain_5()
		throws Exception {
		String[] args = new String[] {""};

		LogFormatter.main(args);

		// add additional test code here
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@Test(expected = java.io.FileNotFoundException.class)
	public void testMain_6()
		throws Exception {
		String[] args = new String[] {""};

		LogFormatter.main(args);

		// add additional test code here
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@Test(expected = java.lang.SecurityException.class)
	public void testMain_7()
		throws Exception {
		String[] args = new String[] {"", null};

		LogFormatter.main(args);

		// add additional test code here
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@Test(expected = java.lang.SecurityException.class)
	public void testMain_8()
		throws Exception {
		String[] args = new String[] {"", null};

		LogFormatter.main(args);

		// add additional test code here
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@Test(expected = java.lang.SecurityException.class)
	public void testMain_9()
		throws Exception {
		String[] args = new String[] {"", null};

		LogFormatter.main(args);

		// add additional test code here
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@Test(expected = java.lang.SecurityException.class)
	public void testMain_10()
		throws Exception {
		String[] args = new String[] {"", null};

		LogFormatter.main(args);

		// add additional test code here
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@Test(expected = java.lang.SecurityException.class)
	public void testMain_11()
		throws Exception {
		String[] args = new String[] {"", null};

		LogFormatter.main(args);

		// add additional test code here
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@Test(expected = java.lang.SecurityException.class)
	public void testMain_12()
		throws Exception {
		String[] args = new String[] {"", null};

		LogFormatter.main(args);

		// add additional test code here
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@Test(expected = java.lang.SecurityException.class)
	public void testMain_13()
		throws Exception {
		String[] args = new String[] {"", null};

		LogFormatter.main(args);

		// add additional test code here
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@Test(expected = java.lang.SecurityException.class)
	public void testMain_14()
		throws Exception {
		String[] args = new String[] {"", null};

		LogFormatter.main(args);

		// add additional test code here
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@Test(expected = java.lang.SecurityException.class)
	public void testMain_15()
		throws Exception {
		String[] args = new String[] {"", null};

		LogFormatter.main(args);

		// add additional test code here
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@Test(expected = java.lang.SecurityException.class)
	public void testMain_16()
		throws Exception {
		String[] args = new String[] {"", null};

		LogFormatter.main(args);

		// add additional test code here
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(LogFormatterTest.class);
	}
}