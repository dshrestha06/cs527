package com.uiuc.org.apache.zookeeper;

import org.apache.zookeeper.Quotas;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>QuotasTest</code> contains tests for the class <code>{@link Quotas}</code>.
 *
 * @generatedBy CodePro at 10/16/13 7:22 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class QuotasTest {
	/**
	 * Run the Quotas() constructor test.
	 *
	 * @generatedBy CodePro at 10/16/13 7:22 PM
	 */
	@Test
	public void testQuotas_1()
		throws Exception {
		Quotas result = new Quotas();
		assertNotNull(result);
		// add additional test code here
	}

	/**
	 * Run the String quotaPath(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:22 PM
	 */
	@Test
	public void testQuotaPath_1()
		throws Exception {
		String path = "";

		String result = Quotas.quotaPath(path);

		// add additional test code here
		assertEquals("/zookeeper/quota/zookeeper_limits", result);
	}

	/**
	 * Run the String statPath(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:22 PM
	 */
	@Test
	public void testStatPath_1()
		throws Exception {
		String path = "";

		String result = Quotas.statPath(path);

		// add additional test code here
		assertEquals("/zookeeper/quota/zookeeper_stats", result);
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:22 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:22 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:22 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(QuotasTest.class);
	}
}