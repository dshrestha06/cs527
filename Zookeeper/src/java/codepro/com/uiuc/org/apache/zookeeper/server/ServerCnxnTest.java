package com.uiuc.org.apache.zookeeper.server;

import java.io.CharArrayWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.Date;
import java.util.List;
import org.apache.zookeeper.data.Id;
import org.apache.zookeeper.proto.RequestHeader;
import org.apache.zookeeper.server.NIOServerCnxn;
import org.apache.zookeeper.server.NIOServerCnxnFactory;
import org.apache.zookeeper.server.ServerCnxn;
import org.apache.zookeeper.server.ZooKeeperServer;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>ServerCnxnTest</code> contains tests for the class <code>{@link ServerCnxn}</code>.
 *
 * @generatedBy CodePro at 10/16/13 8:25 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class ServerCnxnTest {
	/**
	 * Run the void addAuthInfo(Id) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testAddAuthInfo_1()
		throws Exception {
		ServerCnxn fixture = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		Id id = new Id();

		fixture.addAuthInfo(id);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void addAuthInfo(Id) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testAddAuthInfo_2()
		throws Exception {
		ServerCnxn fixture = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		Id id = new Id();

		fixture.addAuthInfo(id);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void dumpConnectionInfo(PrintWriter,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testDumpConnectionInfo_1()
		throws Exception {
		ServerCnxn fixture = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		PrintWriter pwriter = new PrintWriter(new CharArrayWriter());
		boolean brief = false;

		fixture.dumpConnectionInfo(pwriter, brief);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void dumpConnectionInfo(PrintWriter,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testDumpConnectionInfo_2()
		throws Exception {
		ServerCnxn fixture = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		PrintWriter pwriter = new PrintWriter(new CharArrayWriter());
		boolean brief = false;

		fixture.dumpConnectionInfo(pwriter, brief);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void dumpConnectionInfo(PrintWriter,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testDumpConnectionInfo_3()
		throws Exception {
		ServerCnxn fixture = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		PrintWriter pwriter = new PrintWriter(new CharArrayWriter());
		boolean brief = false;

		fixture.dumpConnectionInfo(pwriter, brief);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void dumpConnectionInfo(PrintWriter,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testDumpConnectionInfo_4()
		throws Exception {
		ServerCnxn fixture = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		PrintWriter pwriter = new PrintWriter(new CharArrayWriter());
		boolean brief = false;

		fixture.dumpConnectionInfo(pwriter, brief);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void dumpConnectionInfo(PrintWriter,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testDumpConnectionInfo_5()
		throws Exception {
		ServerCnxn fixture = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		PrintWriter pwriter = new PrintWriter(new CharArrayWriter());
		boolean brief = false;

		fixture.dumpConnectionInfo(pwriter, brief);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void dumpConnectionInfo(PrintWriter,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testDumpConnectionInfo_6()
		throws Exception {
		ServerCnxn fixture = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		PrintWriter pwriter = new PrintWriter(new CharArrayWriter());
		boolean brief = false;

		fixture.dumpConnectionInfo(pwriter, brief);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void dumpConnectionInfo(PrintWriter,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testDumpConnectionInfo_7()
		throws Exception {
		ServerCnxn fixture = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		PrintWriter pwriter = new PrintWriter(new CharArrayWriter());
		boolean brief = true;

		fixture.dumpConnectionInfo(pwriter, brief);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void dumpConnectionInfo(PrintWriter,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testDumpConnectionInfo_8()
		throws Exception {
		ServerCnxn fixture = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		PrintWriter pwriter = new PrintWriter(new CharArrayWriter());
		boolean brief = true;

		fixture.dumpConnectionInfo(pwriter, brief);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the List<Id> getAuthInfo() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testGetAuthInfo_1()
		throws Exception {
		ServerCnxn fixture = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());

		List<Id> result = fixture.getAuthInfo();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
		assertNotNull(result);
	}

	/**
	 * Run the long getAvgLatency() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testGetAvgLatency_1()
		throws Exception {
		ServerCnxn fixture = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());

		long result = fixture.getAvgLatency();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
		assertEquals(0L, result);
	}

	/**
	 * Run the long getAvgLatency() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testGetAvgLatency_2()
		throws Exception {
		ServerCnxn fixture = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());

		long result = fixture.getAvgLatency();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
		assertEquals(0L, result);
	}

	/**
	 * Run the Date getEstablished() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testGetEstablished_1()
		throws Exception {
		ServerCnxn fixture = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());

		Date result = fixture.getEstablished();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
		assertNotNull(result);
	}

	/**
	 * Run the long getLastCxid() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testGetLastCxid_1()
		throws Exception {
		ServerCnxn fixture = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());

		long result = fixture.getLastCxid();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
		assertEquals(0L, result);
	}

	/**
	 * Run the long getLastLatency() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testGetLastLatency_1()
		throws Exception {
		ServerCnxn fixture = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());

		long result = fixture.getLastLatency();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
		assertEquals(0L, result);
	}

	/**
	 * Run the String getLastOperation() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testGetLastOperation_1()
		throws Exception {
		ServerCnxn fixture = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());

		String result = fixture.getLastOperation();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
		assertNotNull(result);
	}

	/**
	 * Run the long getLastResponseTime() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testGetLastResponseTime_1()
		throws Exception {
		ServerCnxn fixture = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());

		long result = fixture.getLastResponseTime();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
		assertEquals(0L, result);
	}

	/**
	 * Run the long getLastZxid() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testGetLastZxid_1()
		throws Exception {
		ServerCnxn fixture = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());

		long result = fixture.getLastZxid();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
		assertEquals(0L, result);
	}

	/**
	 * Run the long getMaxLatency() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testGetMaxLatency_1()
		throws Exception {
		ServerCnxn fixture = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());

		long result = fixture.getMaxLatency();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
		assertEquals(0L, result);
	}

	/**
	 * Run the long getMinLatency() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testGetMinLatency_1()
		throws Exception {
		ServerCnxn fixture = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());

		long result = fixture.getMinLatency();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
		assertEquals(0L, result);
	}

	/**
	 * Run the long getMinLatency() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testGetMinLatency_2()
		throws Exception {
		ServerCnxn fixture = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());

		long result = fixture.getMinLatency();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
		assertEquals(0L, result);
	}

	/**
	 * Run the long getPacketsReceived() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testGetPacketsReceived_1()
		throws Exception {
		ServerCnxn fixture = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());

		long result = fixture.getPacketsReceived();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
		assertEquals(0L, result);
	}

	/**
	 * Run the long getPacketsSent() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testGetPacketsSent_1()
		throws Exception {
		ServerCnxn fixture = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());

		long result = fixture.getPacketsSent();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
		assertEquals(0L, result);
	}

	/**
	 * Run the void incrOutstandingRequests(RequestHeader) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testIncrOutstandingRequests_1()
		throws Exception {
		ServerCnxn fixture = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		RequestHeader h = new RequestHeader();

		fixture.incrOutstandingRequests(h);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the long incrPacketsReceived() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testIncrPacketsReceived_1()
		throws Exception {
		ServerCnxn fixture = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());

		long result = fixture.incrPacketsReceived();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
		assertEquals(0L, result);
	}

	/**
	 * Run the long incrPacketsSent() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testIncrPacketsSent_1()
		throws Exception {
		ServerCnxn fixture = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());

		long result = fixture.incrPacketsSent();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
		assertEquals(0L, result);
	}

	/**
	 * Run the void packetReceived() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testPacketReceived_1()
		throws Exception {
		ServerCnxn fixture = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());

		fixture.packetReceived();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void packetReceived() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testPacketReceived_2()
		throws Exception {
		ServerCnxn fixture = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());

		fixture.packetReceived();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void packetSent() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testPacketSent_1()
		throws Exception {
		ServerCnxn fixture = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());

		fixture.packetSent();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void packetSent() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testPacketSent_2()
		throws Exception {
		ServerCnxn fixture = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());

		fixture.packetSent();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the boolean removeAuthInfo(Id) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testRemoveAuthInfo_1()
		throws Exception {
		ServerCnxn fixture = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		Id id = new Id();

		boolean result = fixture.removeAuthInfo(id);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
		assertTrue(result);
	}

	/**
	 * Run the boolean removeAuthInfo(Id) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testRemoveAuthInfo_2()
		throws Exception {
		ServerCnxn fixture = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		Id id = new Id();

		boolean result = fixture.removeAuthInfo(id);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
		assertTrue(result);
	}

	/**
	 * Run the void resetStats() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testResetStats_1()
		throws Exception {
		ServerCnxn fixture = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());

		fixture.resetStats();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the String toString() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testToString_1()
		throws Exception {
		ServerCnxn fixture = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());

		String result = fixture.toString();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
		assertNotNull(result);
	}

	/**
	 * Run the void updateStatsForResponse(long,long,String,long,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testUpdateStatsForResponse_1()
		throws Exception {
		ServerCnxn fixture = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		long cxid = 1L;
		long zxid = 1L;
		String op = "";
		long start = 1L;
		long end = 1L;

		fixture.updateStatsForResponse(cxid, zxid, op, start, end);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void updateStatsForResponse(long,long,String,long,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testUpdateStatsForResponse_2()
		throws Exception {
		ServerCnxn fixture = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		long cxid = -1L;
		long zxid = 1L;
		String op = "";
		long start = 1L;
		long end = 1L;

		fixture.updateStatsForResponse(cxid, zxid, op, start, end);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void updateStatsForResponse(long,long,String,long,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testUpdateStatsForResponse_3()
		throws Exception {
		ServerCnxn fixture = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		long cxid = 1L;
		long zxid = 1L;
		String op = "";
		long start = 1L;
		long end = 1L;

		fixture.updateStatsForResponse(cxid, zxid, op, start, end);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void updateStatsForResponse(long,long,String,long,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testUpdateStatsForResponse_4()
		throws Exception {
		ServerCnxn fixture = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		long cxid = -1L;
		long zxid = 1L;
		String op = "";
		long start = 1L;
		long end = 1L;

		fixture.updateStatsForResponse(cxid, zxid, op, start, end);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void updateStatsForResponse(long,long,String,long,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testUpdateStatsForResponse_5()
		throws Exception {
		ServerCnxn fixture = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		long cxid = 1L;
		long zxid = 1L;
		String op = "";
		long start = 1L;
		long end = 1L;

		fixture.updateStatsForResponse(cxid, zxid, op, start, end);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void updateStatsForResponse(long,long,String,long,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testUpdateStatsForResponse_6()
		throws Exception {
		ServerCnxn fixture = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		long cxid = -1L;
		long zxid = 1L;
		String op = "";
		long start = 1L;
		long end = 1L;

		fixture.updateStatsForResponse(cxid, zxid, op, start, end);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void updateStatsForResponse(long,long,String,long,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testUpdateStatsForResponse_7()
		throws Exception {
		ServerCnxn fixture = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		long cxid = 1L;
		long zxid = 1L;
		String op = "";
		long start = 1L;
		long end = 1L;

		fixture.updateStatsForResponse(cxid, zxid, op, start, end);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void updateStatsForResponse(long,long,String,long,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testUpdateStatsForResponse_8()
		throws Exception {
		ServerCnxn fixture = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		long cxid = -1L;
		long zxid = 1L;
		String op = "";
		long start = 1L;
		long end = 1L;

		fixture.updateStatsForResponse(cxid, zxid, op, start, end);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(ServerCnxnTest.class);
	}
}