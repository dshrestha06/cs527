package com.uiuc.org.apache.zookeeper;

import java.util.List;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.OpResult;
import org.apache.zookeeper.Transaction;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.data.ACL;
import org.apache.zookeeper.test.system.SimpleClient;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>TransactionTest</code> contains tests for the class <code>{@link Transaction}</code>.
 *
 * @generatedBy CodePro at 10/16/13 6:55 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class TransactionTest {
	/**
	 * Run the Transaction(ZooKeeper) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:55 PM
	 */
	@Test
	public void testTransaction_1()
		throws Exception {
		ZooKeeper zk = new ZooKeeper("", 1, new SimpleClient());

		Transaction result = new Transaction(zk);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 6:55 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 6:55 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 6:55 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(TransactionTest.class);
	}
}