package com.uiuc.org.apache.zookeeper.server;

import java.io.ByteArrayOutputStream;
import java.io.CharArrayWriter;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.apache.jute.BinaryInputArchive;
import org.apache.jute.BinaryOutputArchive;
import org.apache.jute.InputArchive;
import org.apache.jute.OutputArchive;
import org.apache.jute.Record;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.data.ACL;
import org.apache.zookeeper.data.Stat;
import org.apache.zookeeper.data.StatPersisted;
import org.apache.zookeeper.server.DataNode;
import org.apache.zookeeper.server.DataTree;
import org.apache.zookeeper.test.system.SimpleClient;
import org.apache.zookeeper.txn.TxnHeader;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>DataTreeTest</code> contains tests for the class <code>{@link DataTree}</code>.
 *
 * @generatedBy CodePro at 10/16/13 8:19 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class DataTreeTest {
	/**
	 * Run the DataTree() constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testDataTree_1()
		throws Exception {

		DataTree result = new DataTree();

		// add additional test code here
		assertNotNull(result);
		assertEquals(4, result.getNodeCount());
		assertEquals(0, result.getWatchCount());
		assertEquals(0, result.getEphemeralsCount());
		assertEquals(27L, result.approximateDataSize());
	}

	/**
	 * Run the void addDataNode(String,DataNode) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testAddDataNode_1()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String path = "";
		DataNode node = new DataNode(new DataNode((DataNode) null, new byte[] {(byte) -1, (byte) 0, (byte) 1, Byte.MAX_VALUE, Byte.MIN_VALUE}, new Long(-1L), new StatPersisted()), new byte[] {}, new Long(1L), new StatPersisted());

		fixture.addDataNode(path, node);

		// add additional test code here
	}

	/**
	 * Run the long approximateDataSize() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testApproximateDataSize_1()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;

		long result = fixture.approximateDataSize();

		// add additional test code here
		assertEquals(27L, result);
	}

	/**
	 * Run the long approximateDataSize() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testApproximateDataSize_2()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;

		long result = fixture.approximateDataSize();

		// add additional test code here
		assertEquals(27L, result);
	}

	/**
	 * Run the long approximateDataSize() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testApproximateDataSize_3()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;

		long result = fixture.approximateDataSize();

		// add additional test code here
		assertEquals(27L, result);
	}

	/**
	 * Run the void clear() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testClear_1()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;

		fixture.clear();

		// add additional test code here
	}

	/**
	 * Run the Long convertAcls(List<ACL>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testConvertAcls_1()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		List<ACL> acls = null;

		Long result = fixture.convertAcls(acls);

		// add additional test code here
		assertNotNull(result);
		assertEquals("-1", result.toString());
		assertEquals((byte) -1, result.byteValue());
		assertEquals((short) -1, result.shortValue());
		assertEquals(-1, result.intValue());
		assertEquals(-1L, result.longValue());
		assertEquals(-1.0f, result.floatValue(), 1.0f);
		assertEquals(-1.0, result.doubleValue(), 1.0);
	}

	/**
	 * Run the Long convertAcls(List<ACL>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testConvertAcls_2()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		List<ACL> acls = new LinkedList();

		Long result = fixture.convertAcls(acls);

		// add additional test code here
		assertNotNull(result);
		assertEquals("1", result.toString());
		assertEquals((byte) 1, result.byteValue());
		assertEquals((short) 1, result.shortValue());
		assertEquals(1, result.intValue());
		assertEquals(1L, result.longValue());
		assertEquals(1.0f, result.floatValue(), 1.0f);
		assertEquals(1.0, result.doubleValue(), 1.0);
	}

	/**
	 * Run the Long convertAcls(List<ACL>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testConvertAcls_3()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		List<ACL> acls = new LinkedList();

		Long result = fixture.convertAcls(acls);

		// add additional test code here
		assertNotNull(result);
		assertEquals("1", result.toString());
		assertEquals((byte) 1, result.byteValue());
		assertEquals((short) 1, result.shortValue());
		assertEquals(1, result.intValue());
		assertEquals(1L, result.longValue());
		assertEquals(1.0f, result.floatValue(), 1.0f);
		assertEquals(1.0, result.doubleValue(), 1.0);
	}

	/**
	 * Run the List<ACL> convertLong(Long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testConvertLong_1()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		Long longVal = null;

		List<ACL> result = fixture.convertLong(longVal);

		// add additional test code here
		assertEquals(null, result);
	}

	/**
	 * Run the List<ACL> convertLong(Long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testConvertLong_2()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		Long longVal = new Long(1L);

		List<ACL> result = fixture.convertLong(longVal);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.RuntimeException: Failed to fetch acls for 1
		//       at org.apache.zookeeper.server.DataTree.convertLong(DataTree.java:218)
		assertNotNull(result);
	}

	/**
	 * Run the List<ACL> convertLong(Long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testConvertLong_3()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		Long longVal = new Long(1L);

		List<ACL> result = fixture.convertLong(longVal);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.RuntimeException: Failed to fetch acls for 1
		//       at org.apache.zookeeper.server.DataTree.convertLong(DataTree.java:218)
		assertNotNull(result);
	}

	/**
	 * Run the List<ACL> convertLong(Long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test(expected = java.lang.RuntimeException.class)
	public void testConvertLong_4()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		Long longVal = new Long(1L);

		List<ACL> result = fixture.convertLong(longVal);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the void copyStat(Stat,Stat) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testCopyStat_1()
		throws Exception {
		Stat from = new Stat(1L, 1L, 1L, 1L, 1, 1, 1, 1L, 1, 1, 1L);
		Stat to = new Stat();

		DataTree.copyStat(from, to);

		// add additional test code here
	}

	/**
	 * Run the void copyStatPersisted(StatPersisted,StatPersisted) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testCopyStatPersisted_1()
		throws Exception {
		StatPersisted from = new StatPersisted(1L, 1L, 1L, 1L, 1, 1, 1, 1L, 1L);
		StatPersisted to = new StatPersisted();

		DataTree.copyStatPersisted(from, to);

		// add additional test code here
	}

	/**
	 * Run the String createNode(String,byte[],List<ACL>,long,int,long,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testCreateNode_1()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String path = "";
		byte[] data = new byte[] {};
		List<ACL> acl = new LinkedList();
		long ephemeralOwner = 1L;
		int parentCVersion = 1;
		long zxid = 1L;
		long time = 1L;

		String result = fixture.createNode(path, data, acl, ephemeralOwner, parentCVersion, zxid, time);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.StringIndexOutOfBoundsException: String index out of range: -1
		//       at java.lang.String.substring(String.java:1937)
		//       at org.apache.zookeeper.server.DataTree.createNode(DataTree.java:458)
		assertNotNull(result);
	}

	/**
	 * Run the String createNode(String,byte[],List<ACL>,long,int,long,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testCreateNode_2()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String path = "";
		byte[] data = new byte[] {};
		List<ACL> acl = new LinkedList();
		long ephemeralOwner = 1L;
		int parentCVersion = 1;
		long zxid = 1L;
		long time = 1L;

		String result = fixture.createNode(path, data, acl, ephemeralOwner, parentCVersion, zxid, time);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.StringIndexOutOfBoundsException: String index out of range: -1
		//       at java.lang.String.substring(String.java:1937)
		//       at org.apache.zookeeper.server.DataTree.createNode(DataTree.java:458)
		assertNotNull(result);
	}

	/**
	 * Run the String createNode(String,byte[],List<ACL>,long,int,long,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testCreateNode_3()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String path = "";
		byte[] data = new byte[] {};
		List<ACL> acl = new LinkedList();
		long ephemeralOwner = 1L;
		int parentCVersion = 1;
		long zxid = 1L;
		long time = 1L;

		String result = fixture.createNode(path, data, acl, ephemeralOwner, parentCVersion, zxid, time);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.StringIndexOutOfBoundsException: String index out of range: -1
		//       at java.lang.String.substring(String.java:1937)
		//       at org.apache.zookeeper.server.DataTree.createNode(DataTree.java:458)
		assertNotNull(result);
	}

	/**
	 * Run the String createNode(String,byte[],List<ACL>,long,int,long,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testCreateNode_4()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String path = "";
		byte[] data = new byte[] {};
		List<ACL> acl = new LinkedList();
		long ephemeralOwner = 1L;
		int parentCVersion = 1;
		long zxid = 1L;
		long time = 1L;

		String result = fixture.createNode(path, data, acl, ephemeralOwner, parentCVersion, zxid, time);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.StringIndexOutOfBoundsException: String index out of range: -1
		//       at java.lang.String.substring(String.java:1937)
		//       at org.apache.zookeeper.server.DataTree.createNode(DataTree.java:458)
		assertNotNull(result);
	}

	/**
	 * Run the String createNode(String,byte[],List<ACL>,long,int,long,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testCreateNode_5()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String path = "";
		byte[] data = new byte[] {};
		List<ACL> acl = new LinkedList();
		long ephemeralOwner = 0;
		int parentCVersion = -1;
		long zxid = 1L;
		long time = 1L;

		String result = fixture.createNode(path, data, acl, ephemeralOwner, parentCVersion, zxid, time);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.StringIndexOutOfBoundsException: String index out of range: -1
		//       at java.lang.String.substring(String.java:1937)
		//       at org.apache.zookeeper.server.DataTree.createNode(DataTree.java:458)
		assertNotNull(result);
	}

	/**
	 * Run the void deleteNode(String,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testDeleteNode_1()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String path = "";
		long zxid = 1L;

		fixture.deleteNode(path, zxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.StringIndexOutOfBoundsException: String index out of range: -1
		//       at java.lang.String.substring(String.java:1937)
		//       at org.apache.zookeeper.server.DataTree.deleteNode(DataTree.java:540)
	}

	/**
	 * Run the void deleteNode(String,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testDeleteNode_2()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String path = "";
		long zxid = 1L;

		fixture.deleteNode(path, zxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.StringIndexOutOfBoundsException: String index out of range: -1
		//       at java.lang.String.substring(String.java:1937)
		//       at org.apache.zookeeper.server.DataTree.deleteNode(DataTree.java:540)
	}

	/**
	 * Run the void deleteNode(String,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testDeleteNode_3()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String path = "";
		long zxid = 1L;

		fixture.deleteNode(path, zxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.StringIndexOutOfBoundsException: String index out of range: -1
		//       at java.lang.String.substring(String.java:1937)
		//       at org.apache.zookeeper.server.DataTree.deleteNode(DataTree.java:540)
	}

	/**
	 * Run the void deleteNode(String,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testDeleteNode_4()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String path = "";
		long zxid = 1L;

		fixture.deleteNode(path, zxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.StringIndexOutOfBoundsException: String index out of range: -1
		//       at java.lang.String.substring(String.java:1937)
		//       at org.apache.zookeeper.server.DataTree.deleteNode(DataTree.java:540)
	}

	/**
	 * Run the void deleteNode(String,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testDeleteNode_5()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String path = "";
		long zxid = 1L;

		fixture.deleteNode(path, zxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.StringIndexOutOfBoundsException: String index out of range: -1
		//       at java.lang.String.substring(String.java:1937)
		//       at org.apache.zookeeper.server.DataTree.deleteNode(DataTree.java:540)
	}

	/**
	 * Run the void deleteNode(String,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testDeleteNode_6()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String path = "";
		long zxid = 1L;

		fixture.deleteNode(path, zxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.StringIndexOutOfBoundsException: String index out of range: -1
		//       at java.lang.String.substring(String.java:1937)
		//       at org.apache.zookeeper.server.DataTree.deleteNode(DataTree.java:540)
	}

	/**
	 * Run the void deleteNode(String,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testDeleteNode_7()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String path = "";
		long zxid = 1L;

		fixture.deleteNode(path, zxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.StringIndexOutOfBoundsException: String index out of range: -1
		//       at java.lang.String.substring(String.java:1937)
		//       at org.apache.zookeeper.server.DataTree.deleteNode(DataTree.java:540)
	}

	/**
	 * Run the void deleteNode(String,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testDeleteNode_8()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String path = "";
		long zxid = 1L;

		fixture.deleteNode(path, zxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.StringIndexOutOfBoundsException: String index out of range: -1
		//       at java.lang.String.substring(String.java:1937)
		//       at org.apache.zookeeper.server.DataTree.deleteNode(DataTree.java:540)
	}

	/**
	 * Run the void deleteNode(String,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testDeleteNode_9()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String path = "";
		long zxid = 1L;

		fixture.deleteNode(path, zxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.StringIndexOutOfBoundsException: String index out of range: -1
		//       at java.lang.String.substring(String.java:1937)
		//       at org.apache.zookeeper.server.DataTree.deleteNode(DataTree.java:540)
	}

	/**
	 * Run the void deleteNode(String,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testDeleteNode_10()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String path = "";
		long zxid = 1L;

		fixture.deleteNode(path, zxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.StringIndexOutOfBoundsException: String index out of range: -1
		//       at java.lang.String.substring(String.java:1937)
		//       at org.apache.zookeeper.server.DataTree.deleteNode(DataTree.java:540)
	}

	/**
	 * Run the void deleteNode(String,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testDeleteNode_11()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String path = "";
		long zxid = 1L;

		fixture.deleteNode(path, zxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.StringIndexOutOfBoundsException: String index out of range: -1
		//       at java.lang.String.substring(String.java:1937)
		//       at org.apache.zookeeper.server.DataTree.deleteNode(DataTree.java:540)
	}

	/**
	 * Run the void deleteNode(String,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testDeleteNode_12()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String path = "";
		long zxid = 1L;

		fixture.deleteNode(path, zxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.StringIndexOutOfBoundsException: String index out of range: -1
		//       at java.lang.String.substring(String.java:1937)
		//       at org.apache.zookeeper.server.DataTree.deleteNode(DataTree.java:540)
	}

	/**
	 * Run the void deleteNode(String,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testDeleteNode_13()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String path = "";
		long zxid = 1L;

		fixture.deleteNode(path, zxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.StringIndexOutOfBoundsException: String index out of range: -1
		//       at java.lang.String.substring(String.java:1937)
		//       at org.apache.zookeeper.server.DataTree.deleteNode(DataTree.java:540)
	}

	/**
	 * Run the void deleteNode(String,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testDeleteNode_14()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String path = "";
		long zxid = 1L;

		fixture.deleteNode(path, zxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.StringIndexOutOfBoundsException: String index out of range: -1
		//       at java.lang.String.substring(String.java:1937)
		//       at org.apache.zookeeper.server.DataTree.deleteNode(DataTree.java:540)
	}

	/**
	 * Run the void deleteNode(String,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testDeleteNode_15()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String path = "";
		long zxid = 1L;

		fixture.deleteNode(path, zxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.StringIndexOutOfBoundsException: String index out of range: -1
		//       at java.lang.String.substring(String.java:1937)
		//       at org.apache.zookeeper.server.DataTree.deleteNode(DataTree.java:540)
	}

	/**
	 * Run the void deleteNode(String,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testDeleteNode_16()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String path = "";
		long zxid = 1L;

		fixture.deleteNode(path, zxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.StringIndexOutOfBoundsException: String index out of range: -1
		//       at java.lang.String.substring(String.java:1937)
		//       at org.apache.zookeeper.server.DataTree.deleteNode(DataTree.java:540)
	}

	/**
	 * Run the void deserialize(InputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testDeserialize_1()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		InputArchive ia = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));
		String tag = "";

		fixture.deserialize(ia, tag);

		// add additional test code here
	}

	/**
	 * Run the void deserialize(InputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testDeserialize_2()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		InputArchive ia = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));
		String tag = "";

		fixture.deserialize(ia, tag);

		// add additional test code here
	}

	/**
	 * Run the void deserialize(InputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testDeserialize_3()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		InputArchive ia = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));
		String tag = "";

		fixture.deserialize(ia, tag);

		// add additional test code here
	}

	/**
	 * Run the void deserialize(InputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testDeserialize_4()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		InputArchive ia = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));
		String tag = "";

		fixture.deserialize(ia, tag);

		// add additional test code here
	}

	/**
	 * Run the void deserialize(InputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testDeserialize_5()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		InputArchive ia = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));
		String tag = "";

		fixture.deserialize(ia, tag);

		// add additional test code here
	}

	/**
	 * Run the void deserialize(InputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testDeserialize_6()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		InputArchive ia = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));
		String tag = "";

		fixture.deserialize(ia, tag);

		// add additional test code here
	}

	/**
	 * Run the void deserialize(InputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testDeserialize_7()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		InputArchive ia = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));
		String tag = "";

		fixture.deserialize(ia, tag);

		// add additional test code here
	}

	/**
	 * Run the void deserialize(InputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testDeserialize_8()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		InputArchive ia = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));
		String tag = "";

		fixture.deserialize(ia, tag);

		// add additional test code here
	}

	/**
	 * Run the void deserialize(InputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testDeserialize_9()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		InputArchive ia = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));
		String tag = "";

		fixture.deserialize(ia, tag);

		// add additional test code here
	}

	/**
	 * Run the void deserialize(InputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testDeserialize_10()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		InputArchive ia = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));
		String tag = "";

		fixture.deserialize(ia, tag);

		// add additional test code here
	}

	/**
	 * Run the void dumpEphemerals(PrintWriter) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testDumpEphemerals_1()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		PrintWriter pwriter = new PrintWriter(new CharArrayWriter());

		fixture.dumpEphemerals(pwriter);

		// add additional test code here
	}

	/**
	 * Run the void dumpEphemerals(PrintWriter) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testDumpEphemerals_2()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		PrintWriter pwriter = new PrintWriter(new CharArrayWriter());

		fixture.dumpEphemerals(pwriter);

		// add additional test code here
	}

	/**
	 * Run the void dumpEphemerals(PrintWriter) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testDumpEphemerals_3()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		PrintWriter pwriter = new PrintWriter(new CharArrayWriter());

		fixture.dumpEphemerals(pwriter);

		// add additional test code here
	}

	/**
	 * Run the void dumpWatches(PrintWriter,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testDumpWatches_1()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		PrintWriter pwriter = new PrintWriter(new CharArrayWriter());
		boolean byPath = true;

		fixture.dumpWatches(pwriter, byPath);

		// add additional test code here
	}

	/**
	 * Run the void dumpWatchesSummary(PrintWriter) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testDumpWatchesSummary_1()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		PrintWriter pwriter = new PrintWriter(new CharArrayWriter());

		fixture.dumpWatchesSummary(pwriter);

		// add additional test code here
	}

	/**
	 * Run the List<ACL> getACL(String,Stat) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testGetACL_1()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String path = "";
		Stat stat = new Stat();

		List<ACL> result = fixture.getACL(path, stat);

		// add additional test code here
		assertNotNull(result);
		assertEquals(1, result.size());
	}

	/**
	 * Run the List<ACL> getACL(String,Stat) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testGetACL_2()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String path = "";
		Stat stat = new Stat();

		List<ACL> result = fixture.getACL(path, stat);

		// add additional test code here
		assertNotNull(result);
		assertEquals(1, result.size());
	}

	/**
	 * Run the List<String> getChildren(String,Stat,Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testGetChildren_1()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String path = "";
		Stat stat = new Stat();
		Watcher watcher = new SimpleClient();

		List<String> result = fixture.getChildren(path, stat, watcher);

		// add additional test code here
		assertNotNull(result);
		assertEquals(1, result.size());
		assertTrue(result.contains("zookeeper"));
	}

	/**
	 * Run the List<String> getChildren(String,Stat,Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testGetChildren_2()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String path = "";
		Stat stat = new Stat();
		Watcher watcher = new SimpleClient();

		List<String> result = fixture.getChildren(path, stat, watcher);

		// add additional test code here
		assertNotNull(result);
		assertEquals(1, result.size());
		assertTrue(result.contains("zookeeper"));
	}

	/**
	 * Run the List<String> getChildren(String,Stat,Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testGetChildren_3()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String path = "";
		Stat stat = null;
		Watcher watcher = null;

		List<String> result = fixture.getChildren(path, stat, watcher);

		// add additional test code here
		assertNotNull(result);
		assertEquals(1, result.size());
		assertTrue(result.contains("zookeeper"));
	}

	/**
	 * Run the byte[] getData(String,Stat,Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testGetData_1()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String path = "";
		Stat stat = new Stat();
		Watcher watcher = new SimpleClient();

		byte[] result = fixture.getData(path, stat, watcher);

		// add additional test code here
		assertNotNull(result);
		assertEquals(0, result.length);
	}

	/**
	 * Run the byte[] getData(String,Stat,Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testGetData_2()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String path = "";
		Stat stat = new Stat();
		Watcher watcher = new SimpleClient();

		byte[] result = fixture.getData(path, stat, watcher);

		// add additional test code here
		assertNotNull(result);
		assertEquals(0, result.length);
	}

	/**
	 * Run the byte[] getData(String,Stat,Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testGetData_3()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String path = "";
		Stat stat = new Stat();
		Watcher watcher = null;

		byte[] result = fixture.getData(path, stat, watcher);

		// add additional test code here
		assertNotNull(result);
		assertEquals(0, result.length);
	}

	/**
	 * Run the HashSet<String> getEphemerals(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testGetEphemerals_1()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		long sessionId = 1L;

		HashSet<String> result = fixture.getEphemerals(sessionId);

		// add additional test code here
		assertNotNull(result);
		assertEquals(0, result.size());
	}

	/**
	 * Run the HashSet<String> getEphemerals(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testGetEphemerals_2()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		long sessionId = 1L;

		HashSet<String> result = fixture.getEphemerals(sessionId);

		// add additional test code here
		assertNotNull(result);
		assertEquals(0, result.size());
	}

	/**
	 * Run the int getEphemeralsCount() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testGetEphemeralsCount_1()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;

		int result = fixture.getEphemeralsCount();

		// add additional test code here
		assertEquals(0, result);
	}

	/**
	 * Run the int getEphemeralsCount() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testGetEphemeralsCount_2()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;

		int result = fixture.getEphemeralsCount();

		// add additional test code here
		assertEquals(0, result);
	}

	/**
	 * Run the Map<Long, HashSet<String>> getEphemeralsMap() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testGetEphemeralsMap_1()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;

		Map<Long, HashSet<String>> result = fixture.getEphemeralsMap();

		// add additional test code here
		assertNotNull(result);
		assertEquals(0, result.size());
	}

	/**
	 * Run the String getMaxPrefixWithQuota(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testGetMaxPrefixWithQuota_1()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String path = "";

		String result = fixture.getMaxPrefixWithQuota(path);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Invalid path 
		//       at org.apache.zookeeper.common.PathTrie.findMaxPrefix(PathTrie.java:259)
		//       at org.apache.zookeeper.server.DataTree.getMaxPrefixWithQuota(DataTree.java:634)
		assertNotNull(result);
	}

	/**
	 * Run the String getMaxPrefixWithQuota(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testGetMaxPrefixWithQuota_2()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String path = "";

		String result = fixture.getMaxPrefixWithQuota(path);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Invalid path 
		//       at org.apache.zookeeper.common.PathTrie.findMaxPrefix(PathTrie.java:259)
		//       at org.apache.zookeeper.server.DataTree.getMaxPrefixWithQuota(DataTree.java:634)
		assertNotNull(result);
	}

	/**
	 * Run the String getMaxPrefixWithQuota(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testGetMaxPrefixWithQuota_3()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String path = "";

		String result = fixture.getMaxPrefixWithQuota(path);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Invalid path 
		//       at org.apache.zookeeper.common.PathTrie.findMaxPrefix(PathTrie.java:259)
		//       at org.apache.zookeeper.server.DataTree.getMaxPrefixWithQuota(DataTree.java:634)
		assertNotNull(result);
	}

	/**
	 * Run the DataNode getNode(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testGetNode_1()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String path = "";

		DataNode result = fixture.getNode(path);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the int getNodeCount() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testGetNodeCount_1()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;

		int result = fixture.getNodeCount();

		// add additional test code here
		assertEquals(4, result);
	}

	/**
	 * Run the Collection<Long> getSessions() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testGetSessions_1()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;

		Collection<Long> result = fixture.getSessions();

		// add additional test code here
		assertNotNull(result);
		assertEquals(0, result.size());
	}

	/**
	 * Run the int getWatchCount() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testGetWatchCount_1()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;

		int result = fixture.getWatchCount();

		// add additional test code here
		assertEquals(0, result);
	}

	/**
	 * Run the boolean isSpecialPath(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testIsSpecialPath_1()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String path = "/";

		boolean result = fixture.isSpecialPath(path);

		// add additional test code here
		assertEquals(true, result);
	}

	/**
	 * Run the boolean isSpecialPath(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testIsSpecialPath_2()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String path = "/zookeeper";

		boolean result = fixture.isSpecialPath(path);

		// add additional test code here
		assertEquals(true, result);
	}

	/**
	 * Run the boolean isSpecialPath(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testIsSpecialPath_3()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String path = "/zookeeper/quota";

		boolean result = fixture.isSpecialPath(path);

		// add additional test code here
		assertEquals(true, result);
	}

	/**
	 * Run the boolean isSpecialPath(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testIsSpecialPath_4()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String path = "";

		boolean result = fixture.isSpecialPath(path);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the void killSession(long,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testKillSession_1()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		long session = 1L;
		long zxid = 1L;

		fixture.killSession(session, zxid);

		// add additional test code here
	}

	/**
	 * Run the void killSession(long,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testKillSession_2()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		long session = 1L;
		long zxid = 1L;

		fixture.killSession(session, zxid);

		// add additional test code here
	}

	/**
	 * Run the void killSession(long,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testKillSession_3()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		long session = 1L;
		long zxid = 1L;

		fixture.killSession(session, zxid);

		// add additional test code here
	}

	/**
	 * Run the void killSession(long,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testKillSession_4()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		long session = 1L;
		long zxid = 1L;

		fixture.killSession(session, zxid);

		// add additional test code here
	}

	/**
	 * Run the void killSession(long,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testKillSession_5()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		long session = 1L;
		long zxid = 1L;

		fixture.killSession(session, zxid);

		// add additional test code here
	}

	/**
	 * Run the void removeCnxn(Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testRemoveCnxn_1()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		Watcher watcher = new SimpleClient();

		fixture.removeCnxn(watcher);

		// add additional test code here
	}

	/**
	 * Run the void serialize(OutputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testSerialize_1()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		OutputArchive oa = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		String tag = "";

		fixture.serialize(oa, tag);

		// add additional test code here
	}

	/**
	 * Run the void serialize(OutputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testSerialize_2()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		OutputArchive oa = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		String tag = "";

		fixture.serialize(oa, tag);

		// add additional test code here
	}

	/**
	 * Run the void serialize(OutputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testSerialize_3()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		OutputArchive oa = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		String tag = "";

		fixture.serialize(oa, tag);

		// add additional test code here
	}

	/**
	 * Run the void serialize(OutputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testSerialize_4()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		OutputArchive oa = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		String tag = "";

		fixture.serialize(oa, tag);

		// add additional test code here
	}

	/**
	 * Run the void serialize(OutputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testSerialize_5()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		OutputArchive oa = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		String tag = "";

		fixture.serialize(oa, tag);

		// add additional test code here
	}

	/**
	 * Run the void serializeNode(OutputArchive,StringBuilder) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testSerializeNode_1()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		OutputArchive oa = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		StringBuilder path = new StringBuilder();

		fixture.serializeNode(oa, path);

		// add additional test code here
	}

	/**
	 * Run the void serializeNode(OutputArchive,StringBuilder) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testSerializeNode_2()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		OutputArchive oa = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		StringBuilder path = new StringBuilder();

		fixture.serializeNode(oa, path);

		// add additional test code here
	}

	/**
	 * Run the void serializeNode(OutputArchive,StringBuilder) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testSerializeNode_3()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		OutputArchive oa = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		StringBuilder path = new StringBuilder();

		fixture.serializeNode(oa, path);

		// add additional test code here
	}

	/**
	 * Run the void serializeNode(OutputArchive,StringBuilder) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testSerializeNode_4()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		OutputArchive oa = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		StringBuilder path = new StringBuilder();

		fixture.serializeNode(oa, path);

		// add additional test code here
	}

	/**
	 * Run the void serializeNode(OutputArchive,StringBuilder) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testSerializeNode_5()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		OutputArchive oa = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		StringBuilder path = new StringBuilder();

		fixture.serializeNode(oa, path);

		// add additional test code here
	}

	/**
	 * Run the void serializeNode(OutputArchive,StringBuilder) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testSerializeNode_6()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		OutputArchive oa = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		StringBuilder path = new StringBuilder();

		fixture.serializeNode(oa, path);

		// add additional test code here
	}

	/**
	 * Run the void serializeNode(OutputArchive,StringBuilder) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testSerializeNode_7()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		OutputArchive oa = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		StringBuilder path = new StringBuilder();

		fixture.serializeNode(oa, path);

		// add additional test code here
	}

	/**
	 * Run the void serializeNode(OutputArchive,StringBuilder) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testSerializeNode_8()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		OutputArchive oa = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		StringBuilder path = new StringBuilder();

		fixture.serializeNode(oa, path);

		// add additional test code here
	}

	/**
	 * Run the void serializeNode(OutputArchive,StringBuilder) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testSerializeNode_9()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		OutputArchive oa = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		StringBuilder path = new StringBuilder();

		fixture.serializeNode(oa, path);

		// add additional test code here
	}

	/**
	 * Run the void serializeNode(OutputArchive,StringBuilder) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testSerializeNode_10()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		OutputArchive oa = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		StringBuilder path = new StringBuilder();

		fixture.serializeNode(oa, path);

		// add additional test code here
	}

	/**
	 * Run the Stat setACL(String,List<ACL>,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testSetACL_1()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String path = "";
		List<ACL> acl = new LinkedList();
		int version = 1;

		Stat result = fixture.setACL(path, acl, version);

		// add additional test code here
		assertNotNull(result);
		assertEquals("0,0,0,0,0,-1,1,0,0,1,0\n", result.toString());
		assertEquals(0, result.getVersion());
		assertEquals(0, result.getDataLength());
		assertEquals(1, result.getNumChildren());
		assertEquals(0L, result.getCzxid());
		assertEquals(0L, result.getMzxid());
		assertEquals(0L, result.getCtime());
		assertEquals(0L, result.getMtime());
		assertEquals(-1, result.getCversion());
		assertEquals(1, result.getAversion());
		assertEquals(0L, result.getEphemeralOwner());
		assertEquals(0L, result.getPzxid());
	}

	/**
	 * Run the Stat setACL(String,List<ACL>,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testSetACL_2()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String path = "";
		List<ACL> acl = new LinkedList();
		int version = 1;

		Stat result = fixture.setACL(path, acl, version);

		// add additional test code here
		assertNotNull(result);
		assertEquals("0,0,0,0,0,-1,1,0,0,1,0\n", result.toString());
		assertEquals(0, result.getVersion());
		assertEquals(0, result.getDataLength());
		assertEquals(1, result.getNumChildren());
		assertEquals(0L, result.getCzxid());
		assertEquals(0L, result.getMzxid());
		assertEquals(0L, result.getCtime());
		assertEquals(0L, result.getMtime());
		assertEquals(-1, result.getCversion());
		assertEquals(1, result.getAversion());
		assertEquals(0L, result.getEphemeralOwner());
		assertEquals(0L, result.getPzxid());
	}

	/**
	 * Run the void setCversionPzxid(String,int,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testSetCversionPzxid_1()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String path = "/";
		int newCversion = 1;
		long zxid = 1L;

		fixture.setCversionPzxid(path, newCversion, zxid);

		// add additional test code here
	}

	/**
	 * Run the void setCversionPzxid(String,int,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testSetCversionPzxid_2()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String path = "/";
		int newCversion = -1;
		long zxid = 1L;

		fixture.setCversionPzxid(path, newCversion, zxid);

		// add additional test code here
	}

	/**
	 * Run the void setCversionPzxid(String,int,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testSetCversionPzxid_3()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String path = "";
		int newCversion = -1;
		long zxid = 1L;

		fixture.setCversionPzxid(path, newCversion, zxid);

		// add additional test code here
	}

	/**
	 * Run the void setCversionPzxid(String,int,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testSetCversionPzxid_4()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String path = "/";
		int newCversion = 1;
		long zxid = 1L;

		fixture.setCversionPzxid(path, newCversion, zxid);

		// add additional test code here
	}

	/**
	 * Run the void setCversionPzxid(String,int,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testSetCversionPzxid_5()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String path = "";
		int newCversion = 1;
		long zxid = 1L;

		fixture.setCversionPzxid(path, newCversion, zxid);

		// add additional test code here
	}

	/**
	 * Run the void setCversionPzxid(String,int,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testSetCversionPzxid_6()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String path = "/";
		int newCversion = -1;
		long zxid = 1L;

		fixture.setCversionPzxid(path, newCversion, zxid);

		// add additional test code here
	}

	/**
	 * Run the void setCversionPzxid(String,int,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testSetCversionPzxid_7()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String path = "";
		int newCversion = -1;
		long zxid = 1L;

		fixture.setCversionPzxid(path, newCversion, zxid);

		// add additional test code here
	}

	/**
	 * Run the void setCversionPzxid(String,int,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testSetCversionPzxid_8()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String path = "/";
		int newCversion = 1;
		long zxid = 1L;

		fixture.setCversionPzxid(path, newCversion, zxid);

		// add additional test code here
	}

	/**
	 * Run the void setCversionPzxid(String,int,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testSetCversionPzxid_9()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String path = "";
		int newCversion = 1;
		long zxid = 1L;

		fixture.setCversionPzxid(path, newCversion, zxid);

		// add additional test code here
	}

	/**
	 * Run the Stat setData(String,byte[],int,long,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testSetData_1()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String path = "";
		byte[] data = new byte[] {};
		int version = 1;
		long zxid = 1L;
		long time = 1L;

		Stat result = fixture.setData(path, data, version, zxid, time);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Invalid path 
		//       at org.apache.zookeeper.common.PathTrie.findMaxPrefix(PathTrie.java:259)
		//       at org.apache.zookeeper.server.DataTree.getMaxPrefixWithQuota(DataTree.java:634)
		//       at org.apache.zookeeper.server.DataTree.setData(DataTree.java:616)
		assertNotNull(result);
	}

	/**
	 * Run the Stat setData(String,byte[],int,long,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testSetData_2()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String path = "";
		byte[] data = new byte[] {};
		int version = 1;
		long zxid = 1L;
		long time = 1L;

		Stat result = fixture.setData(path, data, version, zxid, time);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Invalid path 
		//       at org.apache.zookeeper.common.PathTrie.findMaxPrefix(PathTrie.java:259)
		//       at org.apache.zookeeper.server.DataTree.getMaxPrefixWithQuota(DataTree.java:634)
		//       at org.apache.zookeeper.server.DataTree.setData(DataTree.java:616)
		assertNotNull(result);
	}

	/**
	 * Run the Stat setData(String,byte[],int,long,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testSetData_3()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String path = "";
		byte[] data = new byte[] {};
		int version = 1;
		long zxid = 1L;
		long time = 1L;

		Stat result = fixture.setData(path, data, version, zxid, time);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Invalid path 
		//       at org.apache.zookeeper.common.PathTrie.findMaxPrefix(PathTrie.java:259)
		//       at org.apache.zookeeper.server.DataTree.getMaxPrefixWithQuota(DataTree.java:634)
		//       at org.apache.zookeeper.server.DataTree.setData(DataTree.java:616)
		assertNotNull(result);
	}

	/**
	 * Run the Stat setData(String,byte[],int,long,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testSetData_4()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String path = "";
		byte[] data = new byte[] {};
		int version = 1;
		long zxid = 1L;
		long time = 1L;

		Stat result = fixture.setData(path, data, version, zxid, time);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Invalid path 
		//       at org.apache.zookeeper.common.PathTrie.findMaxPrefix(PathTrie.java:259)
		//       at org.apache.zookeeper.server.DataTree.getMaxPrefixWithQuota(DataTree.java:634)
		//       at org.apache.zookeeper.server.DataTree.setData(DataTree.java:616)
		assertNotNull(result);
	}

	/**
	 * Run the void setWatches(long,List<String>,List<String>,List<String>,Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testSetWatches_1()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		long relativeZxid = 1L;
		List<String> dataWatches = new LinkedList();
		List<String> existWatches = new LinkedList();
		List<String> childWatches = new LinkedList();
		Watcher watcher = new SimpleClient();

		fixture.setWatches(relativeZxid, dataWatches, existWatches, childWatches, watcher);

		// add additional test code here
	}

	/**
	 * Run the void setWatches(long,List<String>,List<String>,List<String>,Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testSetWatches_2()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		long relativeZxid = 1L;
		List<String> dataWatches = new LinkedList();
		List<String> existWatches = new LinkedList();
		List<String> childWatches = new LinkedList();
		Watcher watcher = new SimpleClient();

		fixture.setWatches(relativeZxid, dataWatches, existWatches, childWatches, watcher);

		// add additional test code here
	}

	/**
	 * Run the void setWatches(long,List<String>,List<String>,List<String>,Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testSetWatches_3()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		long relativeZxid = 1L;
		List<String> dataWatches = new LinkedList();
		List<String> existWatches = new LinkedList();
		List<String> childWatches = new LinkedList();
		Watcher watcher = new SimpleClient();

		fixture.setWatches(relativeZxid, dataWatches, existWatches, childWatches, watcher);

		// add additional test code here
	}

	/**
	 * Run the void setWatches(long,List<String>,List<String>,List<String>,Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testSetWatches_4()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		long relativeZxid = 1L;
		List<String> dataWatches = new LinkedList();
		List<String> existWatches = new LinkedList();
		List<String> childWatches = new LinkedList();
		Watcher watcher = new SimpleClient();

		fixture.setWatches(relativeZxid, dataWatches, existWatches, childWatches, watcher);

		// add additional test code here
	}

	/**
	 * Run the void setWatches(long,List<String>,List<String>,List<String>,Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testSetWatches_5()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		long relativeZxid = 1L;
		List<String> dataWatches = new LinkedList();
		List<String> existWatches = new LinkedList();
		List<String> childWatches = new LinkedList();
		Watcher watcher = new SimpleClient();

		fixture.setWatches(relativeZxid, dataWatches, existWatches, childWatches, watcher);

		// add additional test code here
	}

	/**
	 * Run the void setWatches(long,List<String>,List<String>,List<String>,Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testSetWatches_6()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		long relativeZxid = 1L;
		List<String> dataWatches = new LinkedList();
		List<String> existWatches = new LinkedList();
		List<String> childWatches = new LinkedList();
		Watcher watcher = new SimpleClient();

		fixture.setWatches(relativeZxid, dataWatches, existWatches, childWatches, watcher);

		// add additional test code here
	}

	/**
	 * Run the void setWatches(long,List<String>,List<String>,List<String>,Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testSetWatches_7()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		long relativeZxid = 1L;
		List<String> dataWatches = new LinkedList();
		List<String> existWatches = new LinkedList();
		List<String> childWatches = new LinkedList();
		Watcher watcher = new SimpleClient();

		fixture.setWatches(relativeZxid, dataWatches, existWatches, childWatches, watcher);

		// add additional test code here
	}

	/**
	 * Run the void setWatches(long,List<String>,List<String>,List<String>,Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testSetWatches_8()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		long relativeZxid = 1L;
		List<String> dataWatches = new LinkedList();
		List<String> existWatches = new LinkedList();
		List<String> childWatches = new LinkedList();
		Watcher watcher = new SimpleClient();

		fixture.setWatches(relativeZxid, dataWatches, existWatches, childWatches, watcher);

		// add additional test code here
	}

	/**
	 * Run the void setWatches(long,List<String>,List<String>,List<String>,Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testSetWatches_9()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		long relativeZxid = 1L;
		List<String> dataWatches = new LinkedList();
		List<String> existWatches = new LinkedList();
		List<String> childWatches = new LinkedList();
		Watcher watcher = new SimpleClient();

		fixture.setWatches(relativeZxid, dataWatches, existWatches, childWatches, watcher);

		// add additional test code here
	}

	/**
	 * Run the void setWatches(long,List<String>,List<String>,List<String>,Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testSetWatches_10()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		long relativeZxid = 1L;
		List<String> dataWatches = new LinkedList();
		List<String> existWatches = new LinkedList();
		List<String> childWatches = new LinkedList();
		Watcher watcher = new SimpleClient();

		fixture.setWatches(relativeZxid, dataWatches, existWatches, childWatches, watcher);

		// add additional test code here
	}

	/**
	 * Run the void setWatches(long,List<String>,List<String>,List<String>,Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testSetWatches_11()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		long relativeZxid = 1L;
		List<String> dataWatches = new LinkedList();
		List<String> existWatches = new LinkedList();
		List<String> childWatches = new LinkedList();
		Watcher watcher = new SimpleClient();

		fixture.setWatches(relativeZxid, dataWatches, existWatches, childWatches, watcher);

		// add additional test code here
	}

	/**
	 * Run the void setWatches(long,List<String>,List<String>,List<String>,Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testSetWatches_12()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		long relativeZxid = 1L;
		List<String> dataWatches = new LinkedList();
		List<String> existWatches = new LinkedList();
		List<String> childWatches = new LinkedList();
		Watcher watcher = new SimpleClient();

		fixture.setWatches(relativeZxid, dataWatches, existWatches, childWatches, watcher);

		// add additional test code here
	}

	/**
	 * Run the void setWatches(long,List<String>,List<String>,List<String>,Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testSetWatches_13()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		long relativeZxid = 1L;
		List<String> dataWatches = new LinkedList();
		List<String> existWatches = new LinkedList();
		List<String> childWatches = new LinkedList();
		Watcher watcher = new SimpleClient();

		fixture.setWatches(relativeZxid, dataWatches, existWatches, childWatches, watcher);

		// add additional test code here
	}

	/**
	 * Run the void setWatches(long,List<String>,List<String>,List<String>,Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testSetWatches_14()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		long relativeZxid = 1L;
		List<String> dataWatches = new LinkedList();
		List<String> existWatches = new LinkedList();
		List<String> childWatches = new LinkedList();
		Watcher watcher = new SimpleClient();

		fixture.setWatches(relativeZxid, dataWatches, existWatches, childWatches, watcher);

		// add additional test code here
	}

	/**
	 * Run the void setWatches(long,List<String>,List<String>,List<String>,Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testSetWatches_15()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		long relativeZxid = 1L;
		List<String> dataWatches = new LinkedList();
		List<String> existWatches = new LinkedList();
		List<String> childWatches = new LinkedList();
		Watcher watcher = new SimpleClient();

		fixture.setWatches(relativeZxid, dataWatches, existWatches, childWatches, watcher);

		// add additional test code here
	}

	/**
	 * Run the void setWatches(long,List<String>,List<String>,List<String>,Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testSetWatches_16()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		long relativeZxid = 1L;
		List<String> dataWatches = new LinkedList();
		List<String> existWatches = new LinkedList();
		List<String> childWatches = new LinkedList();
		Watcher watcher = new SimpleClient();

		fixture.setWatches(relativeZxid, dataWatches, existWatches, childWatches, watcher);

		// add additional test code here
	}

	/**
	 * Run the Stat statNode(String,Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testStatNode_1()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String path = "";
		Watcher watcher = null;

		Stat result = fixture.statNode(path, watcher);

		// add additional test code here
		assertNotNull(result);
		assertEquals("0,0,0,0,0,-1,0,0,0,1,0\n", result.toString());
		assertEquals(0, result.getVersion());
		assertEquals(0, result.getDataLength());
		assertEquals(1, result.getNumChildren());
		assertEquals(0L, result.getCzxid());
		assertEquals(0L, result.getMzxid());
		assertEquals(0L, result.getCtime());
		assertEquals(0L, result.getMtime());
		assertEquals(-1, result.getCversion());
		assertEquals(0, result.getAversion());
		assertEquals(0L, result.getEphemeralOwner());
		assertEquals(0L, result.getPzxid());
	}

	/**
	 * Run the Stat statNode(String,Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testStatNode_2()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String path = "";
		Watcher watcher = new SimpleClient();

		Stat result = fixture.statNode(path, watcher);

		// add additional test code here
		assertNotNull(result);
		assertEquals("0,0,0,0,0,-1,0,0,0,1,0\n", result.toString());
		assertEquals(0, result.getVersion());
		assertEquals(0, result.getDataLength());
		assertEquals(1, result.getNumChildren());
		assertEquals(0L, result.getCzxid());
		assertEquals(0L, result.getMzxid());
		assertEquals(0L, result.getCtime());
		assertEquals(0L, result.getMtime());
		assertEquals(-1, result.getCversion());
		assertEquals(0, result.getAversion());
		assertEquals(0L, result.getEphemeralOwner());
		assertEquals(0L, result.getPzxid());
	}

	/**
	 * Run the void updateBytes(String,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testUpdateBytes_1()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String lastPrefix = "";
		long diff = 1L;

		fixture.updateBytes(lastPrefix, diff);

		// add additional test code here
	}

	/**
	 * Run the void updateBytes(String,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testUpdateBytes_2()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String lastPrefix = "";
		long diff = 1L;

		fixture.updateBytes(lastPrefix, diff);

		// add additional test code here
	}

	/**
	 * Run the void updateBytes(String,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testUpdateBytes_3()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String lastPrefix = "";
		long diff = 1L;

		fixture.updateBytes(lastPrefix, diff);

		// add additional test code here
	}

	/**
	 * Run the void updateBytes(String,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testUpdateBytes_4()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String lastPrefix = "";
		long diff = 1L;

		fixture.updateBytes(lastPrefix, diff);

		// add additional test code here
	}

	/**
	 * Run the void updateBytes(String,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testUpdateBytes_5()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String lastPrefix = "";
		long diff = 1L;

		fixture.updateBytes(lastPrefix, diff);

		// add additional test code here
	}

	/**
	 * Run the void updateCount(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testUpdateCount_1()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String lastPrefix = "";
		int diff = 1;

		fixture.updateCount(lastPrefix, diff);

		// add additional test code here
	}

	/**
	 * Run the void updateCount(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testUpdateCount_2()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String lastPrefix = "";
		int diff = 1;

		fixture.updateCount(lastPrefix, diff);

		// add additional test code here
	}

	/**
	 * Run the void updateCount(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testUpdateCount_3()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String lastPrefix = "";
		int diff = 1;

		fixture.updateCount(lastPrefix, diff);

		// add additional test code here
	}

	/**
	 * Run the void updateCount(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testUpdateCount_4()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String lastPrefix = "";
		int diff = 1;

		fixture.updateCount(lastPrefix, diff);

		// add additional test code here
	}

	/**
	 * Run the void updateCount(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Test
	public void testUpdateCount_5()
		throws Exception {
		DataTree fixture = new DataTree();
		fixture.initialized = true;
		fixture.lastProcessedZxid = 1L;
		String lastPrefix = "";
		int diff = 1;

		fixture.updateCount(lastPrefix, diff);

		// add additional test code here
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 8:19 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(DataTreeTest.class);
	}
}