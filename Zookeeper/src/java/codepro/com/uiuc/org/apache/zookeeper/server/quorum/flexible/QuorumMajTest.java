package com.uiuc.org.apache.zookeeper.server.quorum.flexible;

import java.util.HashSet;
import org.apache.zookeeper.server.quorum.flexible.QuorumMaj;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>QuorumMajTest</code> contains tests for the class <code>{@link QuorumMaj}</code>.
 *
 * @generatedBy CodePro at 10/16/13 7:40 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class QuorumMajTest {
	/**
	 * Run the QuorumMaj(int) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test
	public void testQuorumMaj_1()
		throws Exception {
		int n = 1;

		QuorumMaj result = new QuorumMaj(n);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the boolean containsQuorum(HashSet<Long>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test
	public void testContainsQuorum_1()
		throws Exception {
		QuorumMaj fixture = new QuorumMaj(1);
		HashSet<Long> set = new HashSet();

		boolean result = fixture.containsQuorum(set);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean containsQuorum(HashSet<Long>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test
	public void testContainsQuorum_2()
		throws Exception {
		QuorumMaj fixture = new QuorumMaj(1);
		HashSet<Long> set = new HashSet();

		boolean result = fixture.containsQuorum(set);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the long getWeight(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test
	public void testGetWeight_1()
		throws Exception {
		QuorumMaj fixture = new QuorumMaj(1);
		long id = 1L;

		long result = fixture.getWeight(id);

		// add additional test code here
		assertEquals(1L, result);
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(QuorumMajTest.class);
	}
}