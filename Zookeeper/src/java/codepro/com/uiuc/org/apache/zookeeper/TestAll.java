package com.uiuc.org.apache.zookeeper;

import org.junit.runner.JUnitCore;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * The class <code>TestAll</code> builds a suite that can be used to run all
 * of the tests within its package as well as within any subpackages of its
 * package.
 *
 * @generatedBy CodePro at 10/16/13 8:27 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
	LoginTest.class,
	JLineZNodeCompletorTest.class,
	ZooDefsTest.class,
	ClientCnxnTest.class,
	AsyncCallbackTest.class,
	TransactionTest.class,
	OpTest.class,
	WatcherTest.class,
	QuotasTest.class,
	ZKUtilTest.class,
	MultiTransactionRecordTest.class,
	ClientCnxnSocketTest.class,
	OpResultTest.class,
	ShellTest.class,
	CreateModeTest.class,
	ServerAdminClientTest.class,
	EnvironmentTest.class,
	ClientCnxnSocketNIOTest.class,
	StatsTrackTest.class,
	MultiResponseTest.class,
	KeeperExceptionTest.class,
	WatchedEventTest.class,
	ZooKeeperTest.class,
	ClientWatchManagerTest.class,
	ZooKeeperMainTest.class,
	com.uiuc.org.apache.zookeeper.client.TestAll.class,
	com.uiuc.org.apache.zookeeper.common.TestAll.class,
	com.uiuc.org.apache.zookeeper.jmx.TestAll.class,
	com.uiuc.org.apache.zookeeper.server.TestAll.class,
})
public class TestAll {

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 8:27 PM
	 */
	public static void main(String[] args) {
		JUnitCore.runClasses(new Class[] { TestAll.class });
	}
}
