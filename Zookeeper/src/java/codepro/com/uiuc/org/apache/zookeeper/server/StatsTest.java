package com.uiuc.org.apache.zookeeper.server;

import org.junit.After;
import org.junit.Before;

/**
 * The class <code>StatsTest</code> contains tests for the class <code>{@link Stats}</code>.
 *
 * @generatedBy CodePro at 10/16/13 8:25 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class StatsTest {
	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(StatsTest.class);
	}
}