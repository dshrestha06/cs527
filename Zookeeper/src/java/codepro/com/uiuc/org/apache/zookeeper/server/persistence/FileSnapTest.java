package com.uiuc.org.apache.zookeeper.server.persistence;

import java.io.ByteArrayOutputStream;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.jute.BinaryInputArchive;
import org.apache.jute.BinaryOutputArchive;
import org.apache.jute.InputArchive;
import org.apache.jute.OutputArchive;
import org.apache.zookeeper.server.DataTree;
import org.apache.zookeeper.server.persistence.FileHeader;
import org.apache.zookeeper.server.persistence.FileSnap;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>FileSnapTest</code> contains tests for the class <code>{@link FileSnap}</code>.
 *
 * @generatedBy CodePro at 10/16/13 7:37 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class FileSnapTest {
	/**
	 * Run the FileSnap(File) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:37 PM
	 */
	@Test
	public void testFileSnap_1()
		throws Exception {
		File snapDir = new File("");

		FileSnap result = new FileSnap(snapDir);

		// add additional test code here
		assertNotNull(result);
		assertEquals(null, result.findMostRecentSnapshot());
	}

	/**
	 * Run the void close() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:37 PM
	 */
	@Test
	public void testClose_1()
		throws Exception {
		FileSnap fixture = new FileSnap(new File(""));

		fixture.close();

		// add additional test code here
	}

	/**
	 * Run the long deserialize(DataTree,Map<Long,Integer>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:37 PM
	 */
	@Test
	public void testDeserialize_1()
		throws Exception {
		FileSnap fixture = new FileSnap(new File(""));
		DataTree dt = new DataTree();
		Map<Long, Integer> sessions = new HashMap();

		long result = fixture.deserialize(dt, sessions);

		// add additional test code here
		assertEquals(-1L, result);
	}

	/**
	 * Run the long deserialize(DataTree,Map<Long,Integer>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:37 PM
	 */
	@Test
	public void testDeserialize_2()
		throws Exception {
		FileSnap fixture = new FileSnap(new File(""));
		DataTree dt = new DataTree();
		Map<Long, Integer> sessions = new HashMap();

		long result = fixture.deserialize(dt, sessions);

		// add additional test code here
		assertEquals(-1L, result);
	}

	/**
	 * Run the long deserialize(DataTree,Map<Long,Integer>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:37 PM
	 */
	@Test
	public void testDeserialize_3()
		throws Exception {
		FileSnap fixture = new FileSnap(new File(""));
		DataTree dt = new DataTree();
		Map<Long, Integer> sessions = new HashMap();

		long result = fixture.deserialize(dt, sessions);

		// add additional test code here
		assertEquals(-1L, result);
	}

	/**
	 * Run the long deserialize(DataTree,Map<Long,Integer>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:37 PM
	 */
	@Test
	public void testDeserialize_4()
		throws Exception {
		FileSnap fixture = new FileSnap(new File(""));
		DataTree dt = new DataTree();
		Map<Long, Integer> sessions = new HashMap();

		long result = fixture.deserialize(dt, sessions);

		// add additional test code here
		assertEquals(-1L, result);
	}

	/**
	 * Run the long deserialize(DataTree,Map<Long,Integer>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:37 PM
	 */
	@Test
	public void testDeserialize_5()
		throws Exception {
		FileSnap fixture = new FileSnap(new File(""));
		DataTree dt = new DataTree();
		Map<Long, Integer> sessions = new HashMap();

		long result = fixture.deserialize(dt, sessions);

		// add additional test code here
		assertEquals(-1L, result);
	}

	/**
	 * Run the long deserialize(DataTree,Map<Long,Integer>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:37 PM
	 */
	@Test
	public void testDeserialize_6()
		throws Exception {
		FileSnap fixture = new FileSnap(new File(""));
		DataTree dt = new DataTree();
		dt.lastProcessedZxid = 1L;
		Map<Long, Integer> sessions = new HashMap();

		long result = fixture.deserialize(dt, sessions);

		// add additional test code here
		assertEquals(-1L, result);
	}

	/**
	 * Run the long deserialize(DataTree,Map<Long,Integer>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:37 PM
	 */
	@Test
	public void testDeserialize_7()
		throws Exception {
		FileSnap fixture = new FileSnap(new File(""));
		DataTree dt = new DataTree();
		dt.lastProcessedZxid = 1L;
		Map<Long, Integer> sessions = new HashMap();

		long result = fixture.deserialize(dt, sessions);

		// add additional test code here
		assertEquals(-1L, result);
	}

	/**
	 * Run the long deserialize(DataTree,Map<Long,Integer>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:37 PM
	 */
	@Test
	public void testDeserialize_8()
		throws Exception {
		FileSnap fixture = new FileSnap(new File(""));
		DataTree dt = new DataTree();
		dt.lastProcessedZxid = 1L;
		Map<Long, Integer> sessions = new HashMap();

		long result = fixture.deserialize(dt, sessions);

		// add additional test code here
		assertEquals(-1L, result);
	}

	/**
	 * Run the void deserialize(DataTree,Map<Long,Integer>,InputArchive) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:37 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testDeserialize_9()
		throws Exception {
		FileSnap fixture = new FileSnap(new File(""));
		DataTree dt = new DataTree();
		Map<Long, Integer> sessions = new HashMap();
		InputArchive ia = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));

		fixture.deserialize(dt, sessions, ia);

		// add additional test code here
	}

	/**
	 * Run the void deserialize(DataTree,Map<Long,Integer>,InputArchive) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:37 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testDeserialize_10()
		throws Exception {
		FileSnap fixture = new FileSnap(new File(""));
		DataTree dt = new DataTree();
		Map<Long, Integer> sessions = new HashMap();
		InputArchive ia = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));

		fixture.deserialize(dt, sessions, ia);

		// add additional test code here
	}

	/**
	 * Run the void deserialize(DataTree,Map<Long,Integer>,InputArchive) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:37 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testDeserialize_11()
		throws Exception {
		FileSnap fixture = new FileSnap(new File(""));
		DataTree dt = new DataTree();
		Map<Long, Integer> sessions = new HashMap();
		InputArchive ia = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));

		fixture.deserialize(dt, sessions, ia);

		// add additional test code here
	}

	/**
	 * Run the void deserialize(DataTree,Map<Long,Integer>,InputArchive) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:37 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testDeserialize_12()
		throws Exception {
		FileSnap fixture = new FileSnap(new File(""));
		DataTree dt = new DataTree();
		Map<Long, Integer> sessions = new HashMap();
		InputArchive ia = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));

		fixture.deserialize(dt, sessions, ia);

		// add additional test code here
	}

	/**
	 * Run the File findMostRecentSnapshot() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:37 PM
	 */
	@Test
	public void testFindMostRecentSnapshot_1()
		throws Exception {
		FileSnap fixture = new FileSnap(new File(""));

		File result = fixture.findMostRecentSnapshot();

		// add additional test code here
		assertEquals(null, result);
	}

	/**
	 * Run the File findMostRecentSnapshot() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:37 PM
	 */
	@Test
	public void testFindMostRecentSnapshot_2()
		throws Exception {
		FileSnap fixture = new FileSnap(new File(""));

		File result = fixture.findMostRecentSnapshot();

		// add additional test code here
		assertEquals(null, result);
	}

	/**
	 * Run the File findMostRecentSnapshot() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:37 PM
	 */
	@Test
	public void testFindMostRecentSnapshot_3()
		throws Exception {
		FileSnap fixture = new FileSnap(new File(""));

		File result = fixture.findMostRecentSnapshot();

		// add additional test code here
		assertEquals(null, result);
	}

	/**
	 * Run the List<File> findNRecentSnapshots(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:37 PM
	 */
	@Test
	public void testFindNRecentSnapshots_1()
		throws Exception {
		FileSnap fixture = new FileSnap(new File(""));
		int n = 1;

		List<File> result = fixture.findNRecentSnapshots(n);

		// add additional test code here
		assertNotNull(result);
		assertEquals(0, result.size());
	}

	/**
	 * Run the List<File> findNRecentSnapshots(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:37 PM
	 */
	@Test
	public void testFindNRecentSnapshots_2()
		throws Exception {
		FileSnap fixture = new FileSnap(new File(""));
		int n = 1;

		List<File> result = fixture.findNRecentSnapshots(n);

		// add additional test code here
		assertNotNull(result);
		assertEquals(0, result.size());
	}

	/**
	 * Run the List<File> findNRecentSnapshots(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:37 PM
	 */
	@Test
	public void testFindNRecentSnapshots_3()
		throws Exception {
		FileSnap fixture = new FileSnap(new File(""));
		int n = 1;

		List<File> result = fixture.findNRecentSnapshots(n);

		// add additional test code here
		assertNotNull(result);
		assertEquals(0, result.size());
	}

	/**
	 * Run the void serialize(DataTree,Map<Long,Integer>,File) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:37 PM
	 */
	@Test
	public void testSerialize_1()
		throws Exception {
		FileSnap fixture = new FileSnap(new File(""));
		DataTree dt = new DataTree();
		Map<Long, Integer> sessions = new HashMap();
		File snapShot = new File("");

		fixture.serialize(dt, sessions, snapShot);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:183)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:145)
		//       at org.apache.zookeeper.server.persistence.FileSnap.serialize(FileSnap.java:225)
	}

	/**
	 * Run the void serialize(DataTree,Map<Long,Integer>,File) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:37 PM
	 */
	@Test
	public void testSerialize_2()
		throws Exception {
		FileSnap fixture = new FileSnap(new File(""));
		DataTree dt = new DataTree();
		Map<Long, Integer> sessions = new HashMap();
		File snapShot = new File("");

		fixture.serialize(dt, sessions, snapShot);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:183)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:145)
		//       at org.apache.zookeeper.server.persistence.FileSnap.serialize(FileSnap.java:225)
	}

	/**
	 * Run the void serialize(DataTree,Map<Long,Integer>,File) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:37 PM
	 */
	@Test
	public void testSerialize_3()
		throws Exception {
		FileSnap fixture = new FileSnap(new File(""));
		DataTree dt = new DataTree();
		Map<Long, Integer> sessions = new HashMap();
		File snapShot = new File("");

		fixture.serialize(dt, sessions, snapShot);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:183)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:145)
		//       at org.apache.zookeeper.server.persistence.FileSnap.serialize(FileSnap.java:225)
	}

	/**
	 * Run the void serialize(DataTree,Map<Long,Integer>,File) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:37 PM
	 */
	@Test
	public void testSerialize_4()
		throws Exception {
		FileSnap fixture = new FileSnap(new File(""));
		DataTree dt = new DataTree();
		Map<Long, Integer> sessions = new HashMap();
		File snapShot = new File("");

		fixture.serialize(dt, sessions, snapShot);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:183)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:145)
		//       at org.apache.zookeeper.server.persistence.FileSnap.serialize(FileSnap.java:225)
	}

	/**
	 * Run the void serialize(DataTree,Map<Long,Integer>,File) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:37 PM
	 */
	@Test
	public void testSerialize_5()
		throws Exception {
		FileSnap fixture = new FileSnap(new File(""));
		DataTree dt = new DataTree();
		Map<Long, Integer> sessions = new HashMap();
		File snapShot = new File("");

		fixture.serialize(dt, sessions, snapShot);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:183)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:145)
		//       at org.apache.zookeeper.server.persistence.FileSnap.serialize(FileSnap.java:225)
	}

	/**
	 * Run the void serialize(DataTree,Map<Long,Integer>,File) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:37 PM
	 */
	@Test
	public void testSerialize_6()
		throws Exception {
		FileSnap fixture = new FileSnap(new File(""));
		DataTree dt = new DataTree();
		Map<Long, Integer> sessions = new HashMap();
		File snapShot = new File("");

		fixture.serialize(dt, sessions, snapShot);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:183)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:145)
		//       at org.apache.zookeeper.server.persistence.FileSnap.serialize(FileSnap.java:225)
	}

	/**
	 * Run the void serialize(DataTree,Map<Long,Integer>,File) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:37 PM
	 */
	@Test
	public void testSerialize_7()
		throws Exception {
		FileSnap fixture = new FileSnap(new File(""));
		DataTree dt = new DataTree();
		Map<Long, Integer> sessions = new HashMap();
		File snapShot = new File("");

		fixture.serialize(dt, sessions, snapShot);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:183)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:145)
		//       at org.apache.zookeeper.server.persistence.FileSnap.serialize(FileSnap.java:225)
	}

	/**
	 * Run the void serialize(DataTree,Map<Long,Integer>,File) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:37 PM
	 */
	@Test
	public void testSerialize_8()
		throws Exception {
		FileSnap fixture = new FileSnap(new File(""));
		DataTree dt = new DataTree();
		Map<Long, Integer> sessions = new HashMap();
		File snapShot = new File("");

		fixture.serialize(dt, sessions, snapShot);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:183)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:145)
		//       at org.apache.zookeeper.server.persistence.FileSnap.serialize(FileSnap.java:225)
	}

	/**
	 * Run the void serialize(DataTree,Map<Long,Integer>,File) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:37 PM
	 */
	@Test
	public void testSerialize_9()
		throws Exception {
		FileSnap fixture = new FileSnap(new File(""));
		DataTree dt = new DataTree();
		Map<Long, Integer> sessions = new HashMap();
		File snapShot = new File("");

		fixture.serialize(dt, sessions, snapShot);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:183)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:145)
		//       at org.apache.zookeeper.server.persistence.FileSnap.serialize(FileSnap.java:225)
	}

	/**
	 * Run the void serialize(DataTree,Map<Long,Integer>,OutputArchive,FileHeader) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:37 PM
	 */
	@Test
	public void testSerialize_10()
		throws Exception {
		FileSnap fixture = new FileSnap(new File(""));
		DataTree dt = new DataTree();
		Map<Long, Integer> sessions = new HashMap();
		OutputArchive oa = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		FileHeader header = new FileHeader();

		fixture.serialize(dt, sessions, oa, header);

		// add additional test code here
	}

	/**
	 * Run the void serialize(DataTree,Map<Long,Integer>,OutputArchive,FileHeader) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:37 PM
	 */
	@Test
	public void testSerialize_11()
		throws Exception {
		FileSnap fixture = new FileSnap(new File(""));
		DataTree dt = new DataTree();
		Map<Long, Integer> sessions = new HashMap();
		OutputArchive oa = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		FileHeader header = new FileHeader();

		fixture.serialize(dt, sessions, oa, header);

		// add additional test code here
	}

	/**
	 * Run the void serialize(DataTree,Map<Long,Integer>,OutputArchive,FileHeader) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:37 PM
	 */
	@Test
	public void testSerialize_12()
		throws Exception {
		FileSnap fixture = new FileSnap(new File(""));
		DataTree dt = new DataTree();
		Map<Long, Integer> sessions = new HashMap();
		OutputArchive oa = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		FileHeader header = new FileHeader();

		fixture.serialize(dt, sessions, oa, header);

		// add additional test code here
	}

	/**
	 * Run the void serialize(DataTree,Map<Long,Integer>,OutputArchive,FileHeader) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:37 PM
	 */
	@Test(expected = java.lang.IllegalStateException.class)
	public void testSerialize_13()
		throws Exception {
		FileSnap fixture = new FileSnap(new File(""));
		DataTree dt = new DataTree();
		Map<Long, Integer> sessions = new HashMap();
		OutputArchive oa = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		FileHeader header = null;

		fixture.serialize(dt, sessions, oa, header);

		// add additional test code here
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:37 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:37 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:37 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(FileSnapTest.class);
	}
}