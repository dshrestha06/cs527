package com.uiuc.org.apache.zookeeper.server;

import org.junit.runner.JUnitCore;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * The class <code>TestAll</code> builds a suite that can be used to run all
 * of the tests within its package as well as within any subpackages of its
 * package.
 *
 * @generatedBy CodePro at 10/16/13 8:27 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
	StatsTest.class,
	ZooKeeperSaslServerTest.class,
	ZooKeeperServerBeanTest.class,
	SnapshotFormatterTest.class,
	LogFormatterTest.class,
	ZooKeeperServerTest.class,
	DataTreeMXBeanTest.class,
	RequestTest.class,
	NettyServerCnxnFactoryTest.class,
	SessionTrackerImplTest.class,
	PrepRequestProcessorTest.class,
	SyncRequestProcessorTest.class,
	DataTreeBeanTest.class,
	ZKDatabaseTest.class,
	ByteBufferInputStreamTest.class,
	ZooTraceTest.class,
	ZooKeeperServerMainTest.class,
	ZooKeeperServerMXBeanTest.class,
	TraceFormatterTest.class,
	SessionTrackerTest.class,
	FinalRequestProcessorTest.class,
	DatadirCleanupManagerTest.class,
	DataTreeTest.class,
	ConnectionBeanTest.class,
	ByteBufferOutputStreamTest.class,
	ServerCnxnTest.class,
	ServerCnxnFactoryTest.class,
	DataNodeTest.class,
	RequestProcessorTest.class,
	WatchManagerTest.class,
	ConnectionMXBeanTest.class,
	com.uiuc.org.apache.zookeeper.server.auth.TestAll.class,
	com.uiuc.org.apache.zookeeper.server.persistence.TestAll.class,
	com.uiuc.org.apache.zookeeper.server.quorum.TestAll.class,
})
public class TestAll {

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 8:27 PM
	 */
	public static void main(String[] args) {
		JUnitCore.runClasses(new Class[] { TestAll.class });
	}
}
