package com.uiuc.org.apache.zookeeper.server.quorum;

import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.HashSet;
import org.apache.zookeeper.server.quorum.LeaderElection;
import org.apache.zookeeper.server.quorum.QuorumPeer;
import org.apache.zookeeper.server.quorum.Vote;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>LeaderElectionTest</code> contains tests for the class <code>{@link LeaderElection}</code>.
 *
 * @generatedBy CodePro at 10/16/13 7:58 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class LeaderElectionTest {
	/**
	 * Run the LeaderElection(QuorumPeer) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testLeaderElection_1()
		throws Exception {
		QuorumPeer self = new QuorumPeer();

		LeaderElection result = new LeaderElection(self);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the org.apache.zookeeper.server.quorum.LeaderElection.ElectionResult countVotes(HashMap<InetSocketAddress,Vote>,HashSet<Long>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testCountVotes_1()
		throws Exception {
		LeaderElection fixture = new LeaderElection(new QuorumPeer());
		HashMap<InetSocketAddress, Vote> votes = new HashMap();
		HashSet<Long> heardFrom = new HashSet();

		org.apache.zookeeper.server.quorum.LeaderElection.ElectionResult result = fixture.countVotes(votes, heardFrom);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the org.apache.zookeeper.server.quorum.LeaderElection.ElectionResult countVotes(HashMap<InetSocketAddress,Vote>,HashSet<Long>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testCountVotes_2()
		throws Exception {
		LeaderElection fixture = new LeaderElection(new QuorumPeer());
		HashMap<InetSocketAddress, Vote> votes = new HashMap();
		HashSet<Long> heardFrom = new HashSet();

		org.apache.zookeeper.server.quorum.LeaderElection.ElectionResult result = fixture.countVotes(votes, heardFrom);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the org.apache.zookeeper.server.quorum.LeaderElection.ElectionResult countVotes(HashMap<InetSocketAddress,Vote>,HashSet<Long>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testCountVotes_3()
		throws Exception {
		LeaderElection fixture = new LeaderElection(new QuorumPeer());
		HashMap<InetSocketAddress, Vote> votes = new HashMap();
		HashSet<Long> heardFrom = new HashSet();

		org.apache.zookeeper.server.quorum.LeaderElection.ElectionResult result = fixture.countVotes(votes, heardFrom);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the org.apache.zookeeper.server.quorum.LeaderElection.ElectionResult countVotes(HashMap<InetSocketAddress,Vote>,HashSet<Long>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testCountVotes_4()
		throws Exception {
		LeaderElection fixture = new LeaderElection(new QuorumPeer());
		HashMap<InetSocketAddress, Vote> votes = new HashMap();
		HashSet<Long> heardFrom = new HashSet();

		org.apache.zookeeper.server.quorum.LeaderElection.ElectionResult result = fixture.countVotes(votes, heardFrom);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the org.apache.zookeeper.server.quorum.LeaderElection.ElectionResult countVotes(HashMap<InetSocketAddress,Vote>,HashSet<Long>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testCountVotes_5()
		throws Exception {
		LeaderElection fixture = new LeaderElection(new QuorumPeer());
		HashMap<InetSocketAddress, Vote> votes = new HashMap();
		HashSet<Long> heardFrom = new HashSet();

		org.apache.zookeeper.server.quorum.LeaderElection.ElectionResult result = fixture.countVotes(votes, heardFrom);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the org.apache.zookeeper.server.quorum.LeaderElection.ElectionResult countVotes(HashMap<InetSocketAddress,Vote>,HashSet<Long>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testCountVotes_6()
		throws Exception {
		LeaderElection fixture = new LeaderElection(new QuorumPeer());
		HashMap<InetSocketAddress, Vote> votes = new HashMap();
		HashSet<Long> heardFrom = new HashSet();

		org.apache.zookeeper.server.quorum.LeaderElection.ElectionResult result = fixture.countVotes(votes, heardFrom);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the Vote lookForLeader() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testLookForLeader_1()
		throws Exception {
		LeaderElection fixture = new LeaderElection(new QuorumPeer());

		Vote result = fixture.lookForLeader();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getLastLoggedZxid(QuorumPeer.java:545)
		//       at org.apache.zookeeper.server.quorum.LeaderElection.lookForLeader(LeaderElection.java:158)
		assertNotNull(result);
	}

	/**
	 * Run the Vote lookForLeader() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testLookForLeader_2()
		throws Exception {
		LeaderElection fixture = new LeaderElection(new QuorumPeer());

		Vote result = fixture.lookForLeader();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getLastLoggedZxid(QuorumPeer.java:545)
		//       at org.apache.zookeeper.server.quorum.LeaderElection.lookForLeader(LeaderElection.java:158)
		assertNotNull(result);
	}

	/**
	 * Run the Vote lookForLeader() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testLookForLeader_3()
		throws Exception {
		LeaderElection fixture = new LeaderElection(new QuorumPeer());

		Vote result = fixture.lookForLeader();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getLastLoggedZxid(QuorumPeer.java:545)
		//       at org.apache.zookeeper.server.quorum.LeaderElection.lookForLeader(LeaderElection.java:158)
		assertNotNull(result);
	}

	/**
	 * Run the Vote lookForLeader() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testLookForLeader_4()
		throws Exception {
		LeaderElection fixture = new LeaderElection(new QuorumPeer());

		Vote result = fixture.lookForLeader();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getLastLoggedZxid(QuorumPeer.java:545)
		//       at org.apache.zookeeper.server.quorum.LeaderElection.lookForLeader(LeaderElection.java:158)
		assertNotNull(result);
	}

	/**
	 * Run the Vote lookForLeader() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testLookForLeader_5()
		throws Exception {
		LeaderElection fixture = new LeaderElection(new QuorumPeer());

		Vote result = fixture.lookForLeader();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getLastLoggedZxid(QuorumPeer.java:545)
		//       at org.apache.zookeeper.server.quorum.LeaderElection.lookForLeader(LeaderElection.java:158)
		assertNotNull(result);
	}

	/**
	 * Run the Vote lookForLeader() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testLookForLeader_6()
		throws Exception {
		LeaderElection fixture = new LeaderElection(new QuorumPeer());

		Vote result = fixture.lookForLeader();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getLastLoggedZxid(QuorumPeer.java:545)
		//       at org.apache.zookeeper.server.quorum.LeaderElection.lookForLeader(LeaderElection.java:158)
		assertNotNull(result);
	}

	/**
	 * Run the Vote lookForLeader() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testLookForLeader_7()
		throws Exception {
		LeaderElection fixture = new LeaderElection(new QuorumPeer());

		Vote result = fixture.lookForLeader();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getLastLoggedZxid(QuorumPeer.java:545)
		//       at org.apache.zookeeper.server.quorum.LeaderElection.lookForLeader(LeaderElection.java:158)
		assertNotNull(result);
	}

	/**
	 * Run the Vote lookForLeader() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testLookForLeader_8()
		throws Exception {
		LeaderElection fixture = new LeaderElection(new QuorumPeer());

		Vote result = fixture.lookForLeader();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getLastLoggedZxid(QuorumPeer.java:545)
		//       at org.apache.zookeeper.server.quorum.LeaderElection.lookForLeader(LeaderElection.java:158)
		assertNotNull(result);
	}

	/**
	 * Run the Vote lookForLeader() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testLookForLeader_9()
		throws Exception {
		LeaderElection fixture = new LeaderElection(new QuorumPeer());

		Vote result = fixture.lookForLeader();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getLastLoggedZxid(QuorumPeer.java:545)
		//       at org.apache.zookeeper.server.quorum.LeaderElection.lookForLeader(LeaderElection.java:158)
		assertNotNull(result);
	}

	/**
	 * Run the Vote lookForLeader() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testLookForLeader_10()
		throws Exception {
		LeaderElection fixture = new LeaderElection(new QuorumPeer());

		Vote result = fixture.lookForLeader();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getLastLoggedZxid(QuorumPeer.java:545)
		//       at org.apache.zookeeper.server.quorum.LeaderElection.lookForLeader(LeaderElection.java:158)
		assertNotNull(result);
	}

	/**
	 * Run the void shutdown() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testShutdown_1()
		throws Exception {
		LeaderElection fixture = new LeaderElection(new QuorumPeer());

		fixture.shutdown();

		// add additional test code here
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(LeaderElectionTest.class);
	}
}