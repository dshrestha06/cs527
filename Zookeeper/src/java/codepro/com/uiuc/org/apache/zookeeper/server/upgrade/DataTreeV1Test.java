package com.uiuc.org.apache.zookeeper.server.upgrade;

import java.io.ByteArrayOutputStream;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.apache.jute.BinaryInputArchive;
import org.apache.jute.BinaryOutputArchive;
import org.apache.jute.InputArchive;
import org.apache.jute.OutputArchive;
import org.apache.jute.Record;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.data.ACL;
import org.apache.zookeeper.data.Stat;
import org.apache.zookeeper.data.StatPersistedV1;
import org.apache.zookeeper.server.upgrade.DataNodeV1;
import org.apache.zookeeper.server.upgrade.DataTreeV1;
import org.apache.zookeeper.test.system.SimpleClient;
import org.apache.zookeeper.txn.CreateTxn;
import org.apache.zookeeper.txn.DeleteTxn;
import org.apache.zookeeper.txn.ErrorTxn;
import org.apache.zookeeper.txn.SetACLTxn;
import org.apache.zookeeper.txn.SetDataTxn;
import org.apache.zookeeper.txn.TxnHeader;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>DataTreeV1Test</code> contains tests for the class <code>{@link DataTreeV1}</code>.
 *
 * @generatedBy CodePro at 10/16/13 7:41 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class DataTreeV1Test {
	/**
	 * Run the DataTreeV1() constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testDataTreeV1_1()
		throws Exception {

		DataTreeV1 result = new DataTreeV1();

		// add additional test code here
		assertNotNull(result);
		assertEquals("Sessions with Ephemerals (0):\n", result.dumpEphemerals());
	}

	/**
	 * Run the void clear() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testClear_1()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;

		fixture.clear();

		// add additional test code here
	}

	/**
	 * Run the void copyStat(Stat,Stat) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testCopyStat_1()
		throws Exception {
		Stat from = new Stat(1L, 1L, 1L, 1L, 1, 1, 1, 1L, 1, 1, 1L);
		Stat to = new Stat();

		DataTreeV1.copyStat(from, to);

		// add additional test code here
	}

	/**
	 * Run the void copyStatPersisted(StatPersistedV1,StatPersistedV1) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testCopyStatPersisted_1()
		throws Exception {
		StatPersistedV1 from = new StatPersistedV1(1L, 1L, 1L, 1L, 1, 1, 1, 1L);
		StatPersistedV1 to = new StatPersistedV1();

		DataTreeV1.copyStatPersisted(from, to);

		// add additional test code here
	}

	/**
	 * Run the String createNode(String,byte[],List<ACL>,long,long,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testCreateNode_1()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		String path = "";
		byte[] data = new byte[] {};
		List<ACL> acl = new LinkedList();
		long ephemeralOwner = 1L;
		long zxid = 1L;
		long time = 1L;

		String result = fixture.createNode(path, data, acl, ephemeralOwner, zxid, time);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.StringIndexOutOfBoundsException: String index out of range: -1
		//       at java.lang.String.substring(String.java:1937)
		//       at org.apache.zookeeper.server.upgrade.DataTreeV1.createNode(DataTreeV1.java:182)
		assertNotNull(result);
	}

	/**
	 * Run the String createNode(String,byte[],List<ACL>,long,long,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testCreateNode_2()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		String path = "";
		byte[] data = new byte[] {};
		List<ACL> acl = new LinkedList();
		long ephemeralOwner = 1L;
		long zxid = 1L;
		long time = 1L;

		String result = fixture.createNode(path, data, acl, ephemeralOwner, zxid, time);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.StringIndexOutOfBoundsException: String index out of range: -1
		//       at java.lang.String.substring(String.java:1937)
		//       at org.apache.zookeeper.server.upgrade.DataTreeV1.createNode(DataTreeV1.java:182)
		assertNotNull(result);
	}

	/**
	 * Run the String createNode(String,byte[],List<ACL>,long,long,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testCreateNode_3()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		String path = "";
		byte[] data = new byte[] {};
		List<ACL> acl = new LinkedList();
		long ephemeralOwner = 1L;
		long zxid = 1L;
		long time = 1L;

		String result = fixture.createNode(path, data, acl, ephemeralOwner, zxid, time);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.StringIndexOutOfBoundsException: String index out of range: -1
		//       at java.lang.String.substring(String.java:1937)
		//       at org.apache.zookeeper.server.upgrade.DataTreeV1.createNode(DataTreeV1.java:182)
		assertNotNull(result);
	}

	/**
	 * Run the String createNode(String,byte[],List<ACL>,long,long,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testCreateNode_4()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		String path = "";
		byte[] data = new byte[] {};
		List<ACL> acl = new LinkedList();
		long ephemeralOwner = 1L;
		long zxid = 1L;
		long time = 1L;

		String result = fixture.createNode(path, data, acl, ephemeralOwner, zxid, time);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.StringIndexOutOfBoundsException: String index out of range: -1
		//       at java.lang.String.substring(String.java:1937)
		//       at org.apache.zookeeper.server.upgrade.DataTreeV1.createNode(DataTreeV1.java:182)
		assertNotNull(result);
	}

	/**
	 * Run the String createNode(String,byte[],List<ACL>,long,long,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testCreateNode_5()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		String path = "";
		byte[] data = new byte[] {};
		List<ACL> acl = new LinkedList();
		long ephemeralOwner = 0;
		long zxid = 1L;
		long time = 1L;

		String result = fixture.createNode(path, data, acl, ephemeralOwner, zxid, time);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.StringIndexOutOfBoundsException: String index out of range: -1
		//       at java.lang.String.substring(String.java:1937)
		//       at org.apache.zookeeper.server.upgrade.DataTreeV1.createNode(DataTreeV1.java:182)
		assertNotNull(result);
	}

	/**
	 * Run the void deleteNode(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testDeleteNode_1()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		String path = "";

		fixture.deleteNode(path);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.StringIndexOutOfBoundsException: String index out of range: -1
		//       at java.lang.String.substring(String.java:1937)
		//       at org.apache.zookeeper.server.upgrade.DataTreeV1.deleteNode(DataTreeV1.java:224)
	}

	/**
	 * Run the void deleteNode(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testDeleteNode_2()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		String path = "";

		fixture.deleteNode(path);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.StringIndexOutOfBoundsException: String index out of range: -1
		//       at java.lang.String.substring(String.java:1937)
		//       at org.apache.zookeeper.server.upgrade.DataTreeV1.deleteNode(DataTreeV1.java:224)
	}

	/**
	 * Run the void deleteNode(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testDeleteNode_3()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		String path = "";

		fixture.deleteNode(path);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.StringIndexOutOfBoundsException: String index out of range: -1
		//       at java.lang.String.substring(String.java:1937)
		//       at org.apache.zookeeper.server.upgrade.DataTreeV1.deleteNode(DataTreeV1.java:224)
	}

	/**
	 * Run the void deleteNode(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testDeleteNode_4()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		String path = "";

		fixture.deleteNode(path);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.StringIndexOutOfBoundsException: String index out of range: -1
		//       at java.lang.String.substring(String.java:1937)
		//       at org.apache.zookeeper.server.upgrade.DataTreeV1.deleteNode(DataTreeV1.java:224)
	}

	/**
	 * Run the void deleteNode(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testDeleteNode_5()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		String path = "";

		fixture.deleteNode(path);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.StringIndexOutOfBoundsException: String index out of range: -1
		//       at java.lang.String.substring(String.java:1937)
		//       at org.apache.zookeeper.server.upgrade.DataTreeV1.deleteNode(DataTreeV1.java:224)
	}

	/**
	 * Run the void deleteNode(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testDeleteNode_6()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		String path = "";

		fixture.deleteNode(path);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.StringIndexOutOfBoundsException: String index out of range: -1
		//       at java.lang.String.substring(String.java:1937)
		//       at org.apache.zookeeper.server.upgrade.DataTreeV1.deleteNode(DataTreeV1.java:224)
	}

	/**
	 * Run the void deleteNode(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testDeleteNode_7()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		String path = "";

		fixture.deleteNode(path);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.StringIndexOutOfBoundsException: String index out of range: -1
		//       at java.lang.String.substring(String.java:1937)
		//       at org.apache.zookeeper.server.upgrade.DataTreeV1.deleteNode(DataTreeV1.java:224)
	}

	/**
	 * Run the void deleteNode(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testDeleteNode_8()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		String path = "";

		fixture.deleteNode(path);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.StringIndexOutOfBoundsException: String index out of range: -1
		//       at java.lang.String.substring(String.java:1937)
		//       at org.apache.zookeeper.server.upgrade.DataTreeV1.deleteNode(DataTreeV1.java:224)
	}

	/**
	 * Run the void deserialize(InputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testDeserialize_1()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		InputArchive ia = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));
		String tag = "";

		fixture.deserialize(ia, tag);

		// add additional test code here
	}

	/**
	 * Run the void deserialize(InputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testDeserialize_2()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		InputArchive ia = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));
		String tag = "";

		fixture.deserialize(ia, tag);

		// add additional test code here
	}

	/**
	 * Run the void deserialize(InputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testDeserialize_3()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		InputArchive ia = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));
		String tag = "";

		fixture.deserialize(ia, tag);

		// add additional test code here
	}

	/**
	 * Run the void deserialize(InputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testDeserialize_4()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		InputArchive ia = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));
		String tag = "";

		fixture.deserialize(ia, tag);

		// add additional test code here
	}

	/**
	 * Run the void deserialize(InputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testDeserialize_5()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		InputArchive ia = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));
		String tag = "";

		fixture.deserialize(ia, tag);

		// add additional test code here
	}

	/**
	 * Run the void deserialize(InputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testDeserialize_6()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		InputArchive ia = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));
		String tag = "";

		fixture.deserialize(ia, tag);

		// add additional test code here
	}

	/**
	 * Run the void deserialize(InputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testDeserialize_7()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		InputArchive ia = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));
		String tag = "";

		fixture.deserialize(ia, tag);

		// add additional test code here
	}

	/**
	 * Run the void deserialize(InputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testDeserialize_8()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		InputArchive ia = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));
		String tag = "";

		fixture.deserialize(ia, tag);

		// add additional test code here
	}

	/**
	 * Run the String dumpEphemerals() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testDumpEphemerals_1()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;

		String result = fixture.dumpEphemerals();

		// add additional test code here
		assertEquals("Sessions with Ephemerals (0):\n", result);
	}

	/**
	 * Run the String dumpEphemerals() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testDumpEphemerals_2()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;

		String result = fixture.dumpEphemerals();

		// add additional test code here
		assertEquals("Sessions with Ephemerals (0):\n", result);
	}

	/**
	 * Run the String dumpEphemerals() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testDumpEphemerals_3()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;

		String result = fixture.dumpEphemerals();

		// add additional test code here
		assertEquals("Sessions with Ephemerals (0):\n", result);
	}

	/**
	 * Run the List<ACL> getACL(String,Stat) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testGetACL_1()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		String path = "";
		Stat stat = new Stat();

		List<ACL> result = fixture.getACL(path, stat);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.util.ArrayList.<init>(ArrayList.java:131)
		//       at org.apache.zookeeper.server.upgrade.DataTreeV1.getACL(DataTreeV1.java:339)
		assertNotNull(result);
	}

	/**
	 * Run the List<ACL> getACL(String,Stat) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testGetACL_2()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		String path = "";
		Stat stat = new Stat();

		List<ACL> result = fixture.getACL(path, stat);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.util.ArrayList.<init>(ArrayList.java:131)
		//       at org.apache.zookeeper.server.upgrade.DataTreeV1.getACL(DataTreeV1.java:339)
		assertNotNull(result);
	}

	/**
	 * Run the ArrayList<String> getChildren(String,Stat,Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testGetChildren_1()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		String path = "";
		Stat stat = new Stat();
		Watcher watcher = new SimpleClient();

		ArrayList<String> result = fixture.getChildren(path, stat, watcher);

		// add additional test code here
		assertNotNull(result);
		assertEquals(0, result.size());
	}

	/**
	 * Run the ArrayList<String> getChildren(String,Stat,Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testGetChildren_2()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		String path = "";
		Stat stat = new Stat();
		Watcher watcher = new SimpleClient();

		ArrayList<String> result = fixture.getChildren(path, stat, watcher);

		// add additional test code here
		assertNotNull(result);
		assertEquals(0, result.size());
	}

	/**
	 * Run the ArrayList<String> getChildren(String,Stat,Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testGetChildren_3()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		String path = "";
		Stat stat = new Stat();
		Watcher watcher = null;

		ArrayList<String> result = fixture.getChildren(path, stat, watcher);

		// add additional test code here
		assertNotNull(result);
		assertEquals(0, result.size());
	}

	/**
	 * Run the byte[] getData(String,Stat,Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testGetData_1()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		String path = "";
		Stat stat = new Stat();
		Watcher watcher = new SimpleClient();

		byte[] result = fixture.getData(path, stat, watcher);

		// add additional test code here
		assertNotNull(result);
		assertEquals(0, result.length);
	}

	/**
	 * Run the byte[] getData(String,Stat,Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testGetData_2()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		String path = "";
		Stat stat = new Stat();
		Watcher watcher = new SimpleClient();

		byte[] result = fixture.getData(path, stat, watcher);

		// add additional test code here
		assertNotNull(result);
		assertEquals(0, result.length);
	}

	/**
	 * Run the byte[] getData(String,Stat,Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testGetData_3()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		String path = "";
		Stat stat = new Stat();
		Watcher watcher = null;

		byte[] result = fixture.getData(path, stat, watcher);

		// add additional test code here
		assertNotNull(result);
		assertEquals(0, result.length);
	}

	/**
	 * Run the HashSet<String> getEphemerals(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testGetEphemerals_1()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		long sessionId = 1L;

		HashSet<String> result = fixture.getEphemerals(sessionId);

		// add additional test code here
		assertNotNull(result);
		assertEquals(0, result.size());
	}

	/**
	 * Run the HashSet<String> getEphemerals(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testGetEphemerals_2()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		long sessionId = 1L;

		HashSet<String> result = fixture.getEphemerals(sessionId);

		// add additional test code here
		assertNotNull(result);
		assertEquals(0, result.size());
	}

	/**
	 * Run the Map<Long, HashSet<String>> getEphemeralsMap() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testGetEphemeralsMap_1()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;

		Map<Long, HashSet<String>> result = fixture.getEphemeralsMap();

		// add additional test code here
		assertNotNull(result);
		assertEquals(0, result.size());
	}

	/**
	 * Run the DataNodeV1 getNode(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testGetNode_1()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		String path = "";

		DataNodeV1 result = fixture.getNode(path);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the Collection<Long> getSessions() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testGetSessions_1()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;

		Collection<Long> result = fixture.getSessions();

		// add additional test code here
		assertNotNull(result);
		assertEquals(0, result.size());
	}

	/**
	 * Run the void killSession(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testKillSession_1()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		long session = 1L;

		fixture.killSession(session);

		// add additional test code here
	}

	/**
	 * Run the void killSession(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testKillSession_2()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		long session = 1L;

		fixture.killSession(session);

		// add additional test code here
	}

	/**
	 * Run the void killSession(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testKillSession_3()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		long session = 1L;

		fixture.killSession(session);

		// add additional test code here
	}

	/**
	 * Run the void killSession(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testKillSession_4()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		long session = 1L;

		fixture.killSession(session);

		// add additional test code here
	}

	/**
	 * Run the void killSession(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testKillSession_5()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		long session = 1L;

		fixture.killSession(session);

		// add additional test code here
	}

	/**
	 * Run the org.apache.zookeeper.server.upgrade.DataTreeV1.ProcessTxnResult processTxn(TxnHeader,Record) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testProcessTxn_1()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		TxnHeader header = new TxnHeader(1L, 1, 1L, 1L, -11);
		Record txn = new ACL();

		org.apache.zookeeper.server.upgrade.DataTreeV1.ProcessTxnResult result = fixture.processTxn(header, txn);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the org.apache.zookeeper.server.upgrade.DataTreeV1.ProcessTxnResult processTxn(TxnHeader,Record) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testProcessTxn_2()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		TxnHeader header = new TxnHeader(1L, 1, 1L, 1L, -1);
		Record txn = new ErrorTxn(1);

		org.apache.zookeeper.server.upgrade.DataTreeV1.ProcessTxnResult result = fixture.processTxn(header, txn);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the org.apache.zookeeper.server.upgrade.DataTreeV1.ProcessTxnResult processTxn(TxnHeader,Record) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testProcessTxn_3()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		TxnHeader header = new TxnHeader(1L, 1, 1L, 1L, 7);
		Record txn = new SetACLTxn("", new LinkedList(), 1);

		org.apache.zookeeper.server.upgrade.DataTreeV1.ProcessTxnResult result = fixture.processTxn(header, txn);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the org.apache.zookeeper.server.upgrade.DataTreeV1.ProcessTxnResult processTxn(TxnHeader,Record) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testProcessTxn_4()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = false;
		TxnHeader header = new TxnHeader(1L, 1, 1L, 1L, 1);
		Record txn = new CreateTxn("", new byte[] {}, new LinkedList(), false, 1);

		org.apache.zookeeper.server.upgrade.DataTreeV1.ProcessTxnResult result = fixture.processTxn(header, txn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.StringIndexOutOfBoundsException: String index out of range: -1
		//       at java.lang.String.substring(String.java:1937)
		//       at org.apache.zookeeper.server.upgrade.DataTreeV1.createNode(DataTreeV1.java:182)
		//       at org.apache.zookeeper.server.upgrade.DataTreeV1.processTxn(DataTreeV1.java:406)
		assertNotNull(result);
	}

	/**
	 * Run the org.apache.zookeeper.server.upgrade.DataTreeV1.ProcessTxnResult processTxn(TxnHeader,Record) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testProcessTxn_5()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = false;
		TxnHeader header = new TxnHeader(1L, 1, 1L, 1L, 5);
		Record txn = new SetDataTxn("", new byte[] {}, 1);

		org.apache.zookeeper.server.upgrade.DataTreeV1.ProcessTxnResult result = fixture.processTxn(header, txn);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the org.apache.zookeeper.server.upgrade.DataTreeV1.ProcessTxnResult processTxn(TxnHeader,Record) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testProcessTxn_6()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = false;
		TxnHeader header = new TxnHeader(1L, 1, 1L, 1L, 1);
		Record txn = new CreateTxn("", new byte[] {}, new LinkedList(), true, 1);

		org.apache.zookeeper.server.upgrade.DataTreeV1.ProcessTxnResult result = fixture.processTxn(header, txn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.StringIndexOutOfBoundsException: String index out of range: -1
		//       at java.lang.String.substring(String.java:1937)
		//       at org.apache.zookeeper.server.upgrade.DataTreeV1.createNode(DataTreeV1.java:182)
		//       at org.apache.zookeeper.server.upgrade.DataTreeV1.processTxn(DataTreeV1.java:406)
		assertNotNull(result);
	}

	/**
	 * Run the org.apache.zookeeper.server.upgrade.DataTreeV1.ProcessTxnResult processTxn(TxnHeader,Record) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testProcessTxn_7()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = false;
		TxnHeader header = new TxnHeader(1L, 1, 1L, 1L, 2);
		Record txn = new DeleteTxn("");

		org.apache.zookeeper.server.upgrade.DataTreeV1.ProcessTxnResult result = fixture.processTxn(header, txn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.StringIndexOutOfBoundsException: String index out of range: -1
		//       at java.lang.String.substring(String.java:1937)
		//       at org.apache.zookeeper.server.upgrade.DataTreeV1.deleteNode(DataTreeV1.java:224)
		//       at org.apache.zookeeper.server.upgrade.DataTreeV1.processTxn(DataTreeV1.java:414)
		assertNotNull(result);
	}

	/**
	 * Run the void removeCnxn(Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testRemoveCnxn_1()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		Watcher watcher = new SimpleClient();

		fixture.removeCnxn(watcher);

		// add additional test code here
	}

	/**
	 * Run the void serialize(OutputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testSerialize_1()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		OutputArchive oa = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		String tag = "";

		fixture.serialize(oa, tag);

		// add additional test code here
	}

	/**
	 * Run the void serialize(OutputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testSerialize_2()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		OutputArchive oa = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		String tag = "";

		fixture.serialize(oa, tag);

		// add additional test code here
	}

	/**
	 * Run the void serialize(OutputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testSerialize_3()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		OutputArchive oa = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		String tag = "";

		fixture.serialize(oa, tag);

		// add additional test code here
	}

	/**
	 * Run the void serialize(OutputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testSerialize_4()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		OutputArchive oa = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		String tag = "";

		fixture.serialize(oa, tag);

		// add additional test code here
	}

	/**
	 * Run the void serialize(OutputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testSerialize_5()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		OutputArchive oa = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		String tag = "";

		fixture.serialize(oa, tag);

		// add additional test code here
	}

	/**
	 * Run the void serializeNode(OutputArchive,StringBuilder) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testSerializeNode_1()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		OutputArchive oa = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		StringBuilder path = new StringBuilder();

		fixture.serializeNode(oa, path);

		// add additional test code here
	}

	/**
	 * Run the void serializeNode(OutputArchive,StringBuilder) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testSerializeNode_2()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		OutputArchive oa = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		StringBuilder path = new StringBuilder();

		fixture.serializeNode(oa, path);

		// add additional test code here
	}

	/**
	 * Run the void serializeNode(OutputArchive,StringBuilder) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testSerializeNode_3()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		OutputArchive oa = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		StringBuilder path = new StringBuilder();

		fixture.serializeNode(oa, path);

		// add additional test code here
	}

	/**
	 * Run the void serializeNode(OutputArchive,StringBuilder) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testSerializeNode_4()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		OutputArchive oa = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		StringBuilder path = new StringBuilder();

		fixture.serializeNode(oa, path);

		// add additional test code here
	}

	/**
	 * Run the void serializeNode(OutputArchive,StringBuilder) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testSerializeNode_5()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		OutputArchive oa = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		StringBuilder path = new StringBuilder();

		fixture.serializeNode(oa, path);

		// add additional test code here
	}

	/**
	 * Run the void serializeNode(OutputArchive,StringBuilder) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testSerializeNode_6()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		OutputArchive oa = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		StringBuilder path = new StringBuilder();

		fixture.serializeNode(oa, path);

		// add additional test code here
	}

	/**
	 * Run the void serializeNode(OutputArchive,StringBuilder) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testSerializeNode_7()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		OutputArchive oa = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		StringBuilder path = new StringBuilder();

		fixture.serializeNode(oa, path);

		// add additional test code here
	}

	/**
	 * Run the void serializeNode(OutputArchive,StringBuilder) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testSerializeNode_8()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		OutputArchive oa = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		StringBuilder path = new StringBuilder();

		fixture.serializeNode(oa, path);

		// add additional test code here
	}

	/**
	 * Run the Stat setACL(String,List<ACL>,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testSetACL_1()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		String path = "";
		List<ACL> acl = new LinkedList();
		int version = 1;

		Stat result = fixture.setACL(path, acl, version);

		// add additional test code here
		assertNotNull(result);
		assertEquals("0,0,0,0,0,0,1,0,0,0,0\n", result.toString());
		assertEquals(0, result.getVersion());
		assertEquals(0, result.getDataLength());
		assertEquals(0, result.getNumChildren());
		assertEquals(0L, result.getCzxid());
		assertEquals(0L, result.getMzxid());
		assertEquals(0L, result.getCtime());
		assertEquals(0L, result.getMtime());
		assertEquals(0, result.getCversion());
		assertEquals(1, result.getAversion());
		assertEquals(0L, result.getEphemeralOwner());
		assertEquals(0L, result.getPzxid());
	}

	/**
	 * Run the Stat setACL(String,List<ACL>,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testSetACL_2()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		String path = "";
		List<ACL> acl = new LinkedList();
		int version = 1;

		Stat result = fixture.setACL(path, acl, version);

		// add additional test code here
		assertNotNull(result);
		assertEquals("0,0,0,0,0,0,1,0,0,0,0\n", result.toString());
		assertEquals(0, result.getVersion());
		assertEquals(0, result.getDataLength());
		assertEquals(0, result.getNumChildren());
		assertEquals(0L, result.getCzxid());
		assertEquals(0L, result.getMzxid());
		assertEquals(0L, result.getCtime());
		assertEquals(0L, result.getMtime());
		assertEquals(0, result.getCversion());
		assertEquals(1, result.getAversion());
		assertEquals(0L, result.getEphemeralOwner());
		assertEquals(0L, result.getPzxid());
	}

	/**
	 * Run the Stat setData(String,byte[],int,long,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testSetData_1()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		String path = "";
		byte[] data = new byte[] {};
		int version = 1;
		long zxid = 1L;
		long time = 1L;

		Stat result = fixture.setData(path, data, version, zxid, time);

		// add additional test code here
		assertNotNull(result);
		assertEquals("0,1,0,1,1,0,0,0,0,0,0\n", result.toString());
		assertEquals(1, result.getVersion());
		assertEquals(0, result.getDataLength());
		assertEquals(0, result.getNumChildren());
		assertEquals(0L, result.getCzxid());
		assertEquals(1L, result.getMzxid());
		assertEquals(0L, result.getCtime());
		assertEquals(1L, result.getMtime());
		assertEquals(0, result.getCversion());
		assertEquals(0, result.getAversion());
		assertEquals(0L, result.getEphemeralOwner());
		assertEquals(0L, result.getPzxid());
	}

	/**
	 * Run the Stat setData(String,byte[],int,long,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testSetData_2()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		String path = "";
		byte[] data = new byte[] {};
		int version = 1;
		long zxid = 1L;
		long time = 1L;

		Stat result = fixture.setData(path, data, version, zxid, time);

		// add additional test code here
		assertNotNull(result);
		assertEquals("0,1,0,1,1,0,0,0,0,0,0\n", result.toString());
		assertEquals(1, result.getVersion());
		assertEquals(0, result.getDataLength());
		assertEquals(0, result.getNumChildren());
		assertEquals(0L, result.getCzxid());
		assertEquals(1L, result.getMzxid());
		assertEquals(0L, result.getCtime());
		assertEquals(1L, result.getMtime());
		assertEquals(0, result.getCversion());
		assertEquals(0, result.getAversion());
		assertEquals(0L, result.getEphemeralOwner());
		assertEquals(0L, result.getPzxid());
	}

	/**
	 * Run the void setEphemeralsMap(Map<Long,HashSet<String>>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testSetEphemeralsMap_1()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		Map<Long, HashSet<String>> ephemerals = new HashMap();

		fixture.setEphemeralsMap(ephemerals);

		// add additional test code here
	}

	/**
	 * Run the Stat statNode(String,Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testStatNode_1()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		String path = "";
		Watcher watcher = null;

		Stat result = fixture.statNode(path, watcher);

		// add additional test code here
		assertNotNull(result);
		assertEquals("0,0,0,0,0,0,0,0,0,0,0\n", result.toString());
		assertEquals(0, result.getVersion());
		assertEquals(0, result.getDataLength());
		assertEquals(0, result.getNumChildren());
		assertEquals(0L, result.getCzxid());
		assertEquals(0L, result.getMzxid());
		assertEquals(0L, result.getCtime());
		assertEquals(0L, result.getMtime());
		assertEquals(0, result.getCversion());
		assertEquals(0, result.getAversion());
		assertEquals(0L, result.getEphemeralOwner());
		assertEquals(0L, result.getPzxid());
	}

	/**
	 * Run the Stat statNode(String,Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testStatNode_2()
		throws Exception {
		DataTreeV1 fixture = new DataTreeV1();
		fixture.setEphemeralsMap(new HashMap());
		fixture.lastProcessedZxid = 1L;
		fixture.initialized = true;
		String path = "";
		Watcher watcher = new SimpleClient();

		Stat result = fixture.statNode(path, watcher);

		// add additional test code here
		assertNotNull(result);
		assertEquals("0,0,0,0,0,0,0,0,0,0,0\n", result.toString());
		assertEquals(0, result.getVersion());
		assertEquals(0, result.getDataLength());
		assertEquals(0, result.getNumChildren());
		assertEquals(0L, result.getCzxid());
		assertEquals(0L, result.getMzxid());
		assertEquals(0L, result.getCtime());
		assertEquals(0L, result.getMtime());
		assertEquals(0, result.getCversion());
		assertEquals(0, result.getAversion());
		assertEquals(0L, result.getEphemeralOwner());
		assertEquals(0L, result.getPzxid());
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(DataTreeV1Test.class);
	}
}