package com.uiuc.org.apache.zookeeper.jmx;

import javax.management.InstanceAlreadyExistsException;
import org.apache.zookeeper.jmx.ManagedUtil;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>ManagedUtilTest</code> contains tests for the class <code>{@link ManagedUtil}</code>.
 *
 * @generatedBy CodePro at 10/16/13 7:33 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class ManagedUtilTest {
	/**
	 * Run the void registerLog4jMBeans() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test(expected = javax.management.InstanceAlreadyExistsException.class)
	public void testRegisterLog4jMBeans_1()
		throws Exception {

		ManagedUtil.registerLog4jMBeans();

		// add additional test code here
	}

	/**
	 * Run the void registerLog4jMBeans() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test(expected = javax.management.InstanceAlreadyExistsException.class)
	public void testRegisterLog4jMBeans_2()
		throws Exception {

		ManagedUtil.registerLog4jMBeans();

		// add additional test code here
	}

	/**
	 * Run the void registerLog4jMBeans() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test(expected = javax.management.InstanceAlreadyExistsException.class)
	public void testRegisterLog4jMBeans_3()
		throws Exception {

		ManagedUtil.registerLog4jMBeans();

		// add additional test code here
	}

	/**
	 * Run the void registerLog4jMBeans() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test(expected = javax.management.InstanceAlreadyExistsException.class)
	public void testRegisterLog4jMBeans_4()
		throws Exception {

		ManagedUtil.registerLog4jMBeans();

		// add additional test code here
	}

	/**
	 * Run the void registerLog4jMBeans() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test(expected = javax.management.InstanceAlreadyExistsException.class)
	public void testRegisterLog4jMBeans_5()
		throws Exception {

		ManagedUtil.registerLog4jMBeans();

		// add additional test code here
	}

	/**
	 * Run the void registerLog4jMBeans() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test(expected = javax.management.InstanceAlreadyExistsException.class)
	public void testRegisterLog4jMBeans_6()
		throws Exception {

		ManagedUtil.registerLog4jMBeans();

		// add additional test code here
	}

	/**
	 * Run the void registerLog4jMBeans() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test(expected = javax.management.InstanceAlreadyExistsException.class)
	public void testRegisterLog4jMBeans_7()
		throws Exception {

		ManagedUtil.registerLog4jMBeans();

		// add additional test code here
	}

	/**
	 * Run the void registerLog4jMBeans() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test(expected = javax.management.InstanceAlreadyExistsException.class)
	public void testRegisterLog4jMBeans_8()
		throws Exception {

		ManagedUtil.registerLog4jMBeans();

		// add additional test code here
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(ManagedUtilTest.class);
	}
}