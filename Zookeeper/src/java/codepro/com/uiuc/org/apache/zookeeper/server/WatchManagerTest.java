package com.uiuc.org.apache.zookeeper.server;

import java.io.CharArrayWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.HashSet;
import java.util.Set;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.server.WatchManager;
import org.apache.zookeeper.test.system.SimpleClient;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>WatchManagerTest</code> contains tests for the class <code>{@link WatchManager}</code>.
 *
 * @generatedBy CodePro at 10/16/13 8:24 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class WatchManagerTest {
	/**
	 * Run the WatchManager() constructor test.
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testWatchManager_1()
		throws Exception {
		WatchManager result = new WatchManager();
		assertNotNull(result);
		// add additional test code here
	}

	/**
	 * Run the void addWatch(String,Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testAddWatch_1()
		throws Exception {
		WatchManager fixture = new WatchManager();
		String path = "";
		Watcher watcher = new SimpleClient();

		fixture.addWatch(path, watcher);

		// add additional test code here
	}

	/**
	 * Run the void addWatch(String,Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testAddWatch_2()
		throws Exception {
		WatchManager fixture = new WatchManager();
		String path = "";
		Watcher watcher = new SimpleClient();

		fixture.addWatch(path, watcher);

		// add additional test code here
	}

	/**
	 * Run the void addWatch(String,Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testAddWatch_3()
		throws Exception {
		WatchManager fixture = new WatchManager();
		String path = "";
		Watcher watcher = new SimpleClient();

		fixture.addWatch(path, watcher);

		// add additional test code here
	}

	/**
	 * Run the void addWatch(String,Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testAddWatch_4()
		throws Exception {
		WatchManager fixture = new WatchManager();
		String path = "";
		Watcher watcher = new SimpleClient();

		fixture.addWatch(path, watcher);

		// add additional test code here
	}

	/**
	 * Run the void dumpWatches(PrintWriter,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testDumpWatches_1()
		throws Exception {
		WatchManager fixture = new WatchManager();
		PrintWriter pwriter = new PrintWriter(new CharArrayWriter());
		boolean byPath = false;

		fixture.dumpWatches(pwriter, byPath);

		// add additional test code here
	}

	/**
	 * Run the void dumpWatches(PrintWriter,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testDumpWatches_2()
		throws Exception {
		WatchManager fixture = new WatchManager();
		PrintWriter pwriter = new PrintWriter(new CharArrayWriter());
		boolean byPath = false;

		fixture.dumpWatches(pwriter, byPath);

		// add additional test code here
	}

	/**
	 * Run the void dumpWatches(PrintWriter,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testDumpWatches_3()
		throws Exception {
		WatchManager fixture = new WatchManager();
		PrintWriter pwriter = new PrintWriter(new CharArrayWriter());
		boolean byPath = false;

		fixture.dumpWatches(pwriter, byPath);

		// add additional test code here
	}

	/**
	 * Run the void dumpWatches(PrintWriter,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testDumpWatches_4()
		throws Exception {
		WatchManager fixture = new WatchManager();
		PrintWriter pwriter = new PrintWriter(new CharArrayWriter());
		boolean byPath = true;

		fixture.dumpWatches(pwriter, byPath);

		// add additional test code here
	}

	/**
	 * Run the void dumpWatches(PrintWriter,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testDumpWatches_5()
		throws Exception {
		WatchManager fixture = new WatchManager();
		PrintWriter pwriter = new PrintWriter(new CharArrayWriter());
		boolean byPath = true;

		fixture.dumpWatches(pwriter, byPath);

		// add additional test code here
	}

	/**
	 * Run the void dumpWatches(PrintWriter,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testDumpWatches_6()
		throws Exception {
		WatchManager fixture = new WatchManager();
		PrintWriter pwriter = new PrintWriter(new CharArrayWriter());
		boolean byPath = true;

		fixture.dumpWatches(pwriter, byPath);

		// add additional test code here
	}

	/**
	 * Run the void removeWatcher(Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testRemoveWatcher_1()
		throws Exception {
		WatchManager fixture = new WatchManager();
		Watcher watcher = new SimpleClient();

		fixture.removeWatcher(watcher);

		// add additional test code here
	}

	/**
	 * Run the void removeWatcher(Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testRemoveWatcher_2()
		throws Exception {
		WatchManager fixture = new WatchManager();
		Watcher watcher = new SimpleClient();

		fixture.removeWatcher(watcher);

		// add additional test code here
	}

	/**
	 * Run the void removeWatcher(Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testRemoveWatcher_3()
		throws Exception {
		WatchManager fixture = new WatchManager();
		Watcher watcher = new SimpleClient();

		fixture.removeWatcher(watcher);

		// add additional test code here
	}

	/**
	 * Run the void removeWatcher(Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testRemoveWatcher_4()
		throws Exception {
		WatchManager fixture = new WatchManager();
		Watcher watcher = new SimpleClient();

		fixture.removeWatcher(watcher);

		// add additional test code here
	}

	/**
	 * Run the void removeWatcher(Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testRemoveWatcher_5()
		throws Exception {
		WatchManager fixture = new WatchManager();
		Watcher watcher = new SimpleClient();

		fixture.removeWatcher(watcher);

		// add additional test code here
	}

	/**
	 * Run the int size() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testSize_1()
		throws Exception {
		WatchManager fixture = new WatchManager();

		int result = fixture.size();

		// add additional test code here
		assertEquals(0, result);
	}

	/**
	 * Run the int size() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testSize_2()
		throws Exception {
		WatchManager fixture = new WatchManager();

		int result = fixture.size();

		// add additional test code here
		assertEquals(0, result);
	}

	/**
	 * Run the String toString() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testToString_1()
		throws Exception {
		WatchManager fixture = new WatchManager();

		String result = fixture.toString();

		// add additional test code here
		assertEquals("0 connections watching 0 paths\nTotal watches:0", result);
	}

	/**
	 * Run the String toString() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testToString_2()
		throws Exception {
		WatchManager fixture = new WatchManager();

		String result = fixture.toString();

		// add additional test code here
		assertEquals("0 connections watching 0 paths\nTotal watches:0", result);
	}

	/**
	 * Run the Set<Watcher> triggerWatch(String,EventType) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testTriggerWatch_1()
		throws Exception {
		WatchManager fixture = new WatchManager();
		String path = "";
		org.apache.zookeeper.Watcher.Event.EventType type = org.apache.zookeeper.Watcher.Event.EventType.NodeChildrenChanged;

		Set<Watcher> result = fixture.triggerWatch(path, type);

		// add additional test code here
		assertEquals(null, result);
	}

	/**
	 * Run the Set<Watcher> triggerWatch(String,EventType,Set<Watcher>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testTriggerWatch_2()
		throws Exception {
		WatchManager fixture = new WatchManager();
		String path = "";
		org.apache.zookeeper.Watcher.Event.EventType type = org.apache.zookeeper.Watcher.Event.EventType.NodeChildrenChanged;
		Set<Watcher> supress = new HashSet();

		Set<Watcher> result = fixture.triggerWatch(path, type, supress);

		// add additional test code here
		assertEquals(null, result);
	}

	/**
	 * Run the Set<Watcher> triggerWatch(String,EventType,Set<Watcher>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testTriggerWatch_3()
		throws Exception {
		WatchManager fixture = new WatchManager();
		String path = "";
		org.apache.zookeeper.Watcher.Event.EventType type = org.apache.zookeeper.Watcher.Event.EventType.NodeChildrenChanged;
		Set<Watcher> supress = new HashSet();

		Set<Watcher> result = fixture.triggerWatch(path, type, supress);

		// add additional test code here
		assertEquals(null, result);
	}

	/**
	 * Run the Set<Watcher> triggerWatch(String,EventType,Set<Watcher>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testTriggerWatch_4()
		throws Exception {
		WatchManager fixture = new WatchManager();
		String path = "";
		org.apache.zookeeper.Watcher.Event.EventType type = org.apache.zookeeper.Watcher.Event.EventType.NodeChildrenChanged;
		Set<Watcher> supress = new HashSet();

		Set<Watcher> result = fixture.triggerWatch(path, type, supress);

		// add additional test code here
		assertEquals(null, result);
	}

	/**
	 * Run the Set<Watcher> triggerWatch(String,EventType,Set<Watcher>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testTriggerWatch_5()
		throws Exception {
		WatchManager fixture = new WatchManager();
		String path = "";
		org.apache.zookeeper.Watcher.Event.EventType type = org.apache.zookeeper.Watcher.Event.EventType.NodeChildrenChanged;
		Set<Watcher> supress = new HashSet();

		Set<Watcher> result = fixture.triggerWatch(path, type, supress);

		// add additional test code here
		assertEquals(null, result);
	}

	/**
	 * Run the Set<Watcher> triggerWatch(String,EventType,Set<Watcher>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testTriggerWatch_6()
		throws Exception {
		WatchManager fixture = new WatchManager();
		String path = "";
		org.apache.zookeeper.Watcher.Event.EventType type = org.apache.zookeeper.Watcher.Event.EventType.NodeChildrenChanged;
		Set<Watcher> supress = new HashSet();

		Set<Watcher> result = fixture.triggerWatch(path, type, supress);

		// add additional test code here
		assertEquals(null, result);
	}

	/**
	 * Run the Set<Watcher> triggerWatch(String,EventType,Set<Watcher>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testTriggerWatch_7()
		throws Exception {
		WatchManager fixture = new WatchManager();
		String path = "";
		org.apache.zookeeper.Watcher.Event.EventType type = org.apache.zookeeper.Watcher.Event.EventType.NodeChildrenChanged;
		Set<Watcher> supress = new HashSet();

		Set<Watcher> result = fixture.triggerWatch(path, type, supress);

		// add additional test code here
		assertEquals(null, result);
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(WatchManagerTest.class);
	}
}