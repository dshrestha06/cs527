package com.uiuc.org.apache.zookeeper.server.quorum;

import org.apache.zookeeper.server.quorum.AuthFastLeaderElection;
import org.apache.zookeeper.server.quorum.QuorumPeer;
import org.apache.zookeeper.server.quorum.Vote;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>AuthFastLeaderElectionTest</code> contains tests for the class <code>{@link AuthFastLeaderElection}</code>.
 *
 * @generatedBy CodePro at 10/16/13 7:57 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class AuthFastLeaderElectionTest {
	/**
	 * Run the AuthFastLeaderElection(QuorumPeer) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:57 PM
	 */
	@Test
	public void testAuthFastLeaderElection_1()
		throws Exception {
		QuorumPeer self = new QuorumPeer();

		AuthFastLeaderElection result = new AuthFastLeaderElection(self);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.util.Collections$UnmodifiableMap.<init>(Collections.java:1274)
		//       at java.util.Collections.unmodifiableMap(Collections.java:1260)
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getView(QuorumPeer.java:808)
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getVotingView(QuorumPeer.java:818)
		//       at org.apache.zookeeper.server.quorum.AuthFastLeaderElection.starter(AuthFastLeaderElection.java:757)
		//       at org.apache.zookeeper.server.quorum.AuthFastLeaderElection.<init>(AuthFastLeaderElection.java:752)
		assertNotNull(result);
	}

	/**
	 * Run the AuthFastLeaderElection(QuorumPeer,boolean) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:57 PM
	 */
	@Test
	public void testAuthFastLeaderElection_2()
		throws Exception {
		QuorumPeer self = new QuorumPeer();
		boolean auth = true;

		AuthFastLeaderElection result = new AuthFastLeaderElection(self, auth);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.util.Collections$UnmodifiableMap.<init>(Collections.java:1274)
		//       at java.util.Collections.unmodifiableMap(Collections.java:1260)
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getView(QuorumPeer.java:808)
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getVotingView(QuorumPeer.java:818)
		//       at org.apache.zookeeper.server.quorum.AuthFastLeaderElection.starter(AuthFastLeaderElection.java:757)
		//       at org.apache.zookeeper.server.quorum.AuthFastLeaderElection.<init>(AuthFastLeaderElection.java:748)
		assertNotNull(result);
	}

	/**
	 * Run the Vote lookForLeader() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:57 PM
	 */
	@Test
	public void testLookForLeader_1()
		throws Exception {
		AuthFastLeaderElection fixture = new AuthFastLeaderElection(new QuorumPeer(), true);

		Vote result = fixture.lookForLeader();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.util.Collections$UnmodifiableMap.<init>(Collections.java:1274)
		//       at java.util.Collections.unmodifiableMap(Collections.java:1260)
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getView(QuorumPeer.java:808)
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getVotingView(QuorumPeer.java:818)
		//       at org.apache.zookeeper.server.quorum.AuthFastLeaderElection.starter(AuthFastLeaderElection.java:757)
		//       at org.apache.zookeeper.server.quorum.AuthFastLeaderElection.<init>(AuthFastLeaderElection.java:748)
		assertNotNull(result);
	}

	/**
	 * Run the Vote lookForLeader() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:57 PM
	 */
	@Test
	public void testLookForLeader_2()
		throws Exception {
		AuthFastLeaderElection fixture = new AuthFastLeaderElection(new QuorumPeer(), true);

		Vote result = fixture.lookForLeader();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.util.Collections$UnmodifiableMap.<init>(Collections.java:1274)
		//       at java.util.Collections.unmodifiableMap(Collections.java:1260)
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getView(QuorumPeer.java:808)
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getVotingView(QuorumPeer.java:818)
		//       at org.apache.zookeeper.server.quorum.AuthFastLeaderElection.starter(AuthFastLeaderElection.java:757)
		//       at org.apache.zookeeper.server.quorum.AuthFastLeaderElection.<init>(AuthFastLeaderElection.java:748)
		assertNotNull(result);
	}

	/**
	 * Run the Vote lookForLeader() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:57 PM
	 */
	@Test
	public void testLookForLeader_3()
		throws Exception {
		AuthFastLeaderElection fixture = new AuthFastLeaderElection(new QuorumPeer(), true);

		Vote result = fixture.lookForLeader();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.util.Collections$UnmodifiableMap.<init>(Collections.java:1274)
		//       at java.util.Collections.unmodifiableMap(Collections.java:1260)
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getView(QuorumPeer.java:808)
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getVotingView(QuorumPeer.java:818)
		//       at org.apache.zookeeper.server.quorum.AuthFastLeaderElection.starter(AuthFastLeaderElection.java:757)
		//       at org.apache.zookeeper.server.quorum.AuthFastLeaderElection.<init>(AuthFastLeaderElection.java:748)
		assertNotNull(result);
	}

	/**
	 * Run the Vote lookForLeader() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:57 PM
	 */
	@Test
	public void testLookForLeader_4()
		throws Exception {
		AuthFastLeaderElection fixture = new AuthFastLeaderElection(new QuorumPeer(), true);

		Vote result = fixture.lookForLeader();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.util.Collections$UnmodifiableMap.<init>(Collections.java:1274)
		//       at java.util.Collections.unmodifiableMap(Collections.java:1260)
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getView(QuorumPeer.java:808)
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getVotingView(QuorumPeer.java:818)
		//       at org.apache.zookeeper.server.quorum.AuthFastLeaderElection.starter(AuthFastLeaderElection.java:757)
		//       at org.apache.zookeeper.server.quorum.AuthFastLeaderElection.<init>(AuthFastLeaderElection.java:748)
		assertNotNull(result);
	}

	/**
	 * Run the Vote lookForLeader() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:57 PM
	 */
	@Test
	public void testLookForLeader_5()
		throws Exception {
		AuthFastLeaderElection fixture = new AuthFastLeaderElection(new QuorumPeer(), true);

		Vote result = fixture.lookForLeader();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.util.Collections$UnmodifiableMap.<init>(Collections.java:1274)
		//       at java.util.Collections.unmodifiableMap(Collections.java:1260)
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getView(QuorumPeer.java:808)
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getVotingView(QuorumPeer.java:818)
		//       at org.apache.zookeeper.server.quorum.AuthFastLeaderElection.starter(AuthFastLeaderElection.java:757)
		//       at org.apache.zookeeper.server.quorum.AuthFastLeaderElection.<init>(AuthFastLeaderElection.java:748)
		assertNotNull(result);
	}

	/**
	 * Run the Vote lookForLeader() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:57 PM
	 */
	@Test
	public void testLookForLeader_6()
		throws Exception {
		AuthFastLeaderElection fixture = new AuthFastLeaderElection(new QuorumPeer(), true);

		Vote result = fixture.lookForLeader();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.util.Collections$UnmodifiableMap.<init>(Collections.java:1274)
		//       at java.util.Collections.unmodifiableMap(Collections.java:1260)
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getView(QuorumPeer.java:808)
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getVotingView(QuorumPeer.java:818)
		//       at org.apache.zookeeper.server.quorum.AuthFastLeaderElection.starter(AuthFastLeaderElection.java:757)
		//       at org.apache.zookeeper.server.quorum.AuthFastLeaderElection.<init>(AuthFastLeaderElection.java:748)
		assertNotNull(result);
	}

	/**
	 * Run the Vote lookForLeader() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:57 PM
	 */
	@Test
	public void testLookForLeader_7()
		throws Exception {
		AuthFastLeaderElection fixture = new AuthFastLeaderElection(new QuorumPeer(), true);

		Vote result = fixture.lookForLeader();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.util.Collections$UnmodifiableMap.<init>(Collections.java:1274)
		//       at java.util.Collections.unmodifiableMap(Collections.java:1260)
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getView(QuorumPeer.java:808)
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getVotingView(QuorumPeer.java:818)
		//       at org.apache.zookeeper.server.quorum.AuthFastLeaderElection.starter(AuthFastLeaderElection.java:757)
		//       at org.apache.zookeeper.server.quorum.AuthFastLeaderElection.<init>(AuthFastLeaderElection.java:748)
		assertNotNull(result);
	}

	/**
	 * Run the Vote lookForLeader() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:57 PM
	 */
	@Test
	public void testLookForLeader_8()
		throws Exception {
		AuthFastLeaderElection fixture = new AuthFastLeaderElection(new QuorumPeer(), true);

		Vote result = fixture.lookForLeader();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.util.Collections$UnmodifiableMap.<init>(Collections.java:1274)
		//       at java.util.Collections.unmodifiableMap(Collections.java:1260)
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getView(QuorumPeer.java:808)
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getVotingView(QuorumPeer.java:818)
		//       at org.apache.zookeeper.server.quorum.AuthFastLeaderElection.starter(AuthFastLeaderElection.java:757)
		//       at org.apache.zookeeper.server.quorum.AuthFastLeaderElection.<init>(AuthFastLeaderElection.java:748)
		assertNotNull(result);
	}

	/**
	 * Run the Vote lookForLeader() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:57 PM
	 */
	@Test
	public void testLookForLeader_9()
		throws Exception {
		AuthFastLeaderElection fixture = new AuthFastLeaderElection(new QuorumPeer(), true);

		Vote result = fixture.lookForLeader();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.util.Collections$UnmodifiableMap.<init>(Collections.java:1274)
		//       at java.util.Collections.unmodifiableMap(Collections.java:1260)
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getView(QuorumPeer.java:808)
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getVotingView(QuorumPeer.java:818)
		//       at org.apache.zookeeper.server.quorum.AuthFastLeaderElection.starter(AuthFastLeaderElection.java:757)
		//       at org.apache.zookeeper.server.quorum.AuthFastLeaderElection.<init>(AuthFastLeaderElection.java:748)
		assertNotNull(result);
	}

	/**
	 * Run the Vote lookForLeader() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:57 PM
	 */
	@Test
	public void testLookForLeader_10()
		throws Exception {
		AuthFastLeaderElection fixture = new AuthFastLeaderElection(new QuorumPeer(), true);

		Vote result = fixture.lookForLeader();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.util.Collections$UnmodifiableMap.<init>(Collections.java:1274)
		//       at java.util.Collections.unmodifiableMap(Collections.java:1260)
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getView(QuorumPeer.java:808)
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getVotingView(QuorumPeer.java:818)
		//       at org.apache.zookeeper.server.quorum.AuthFastLeaderElection.starter(AuthFastLeaderElection.java:757)
		//       at org.apache.zookeeper.server.quorum.AuthFastLeaderElection.<init>(AuthFastLeaderElection.java:748)
		assertNotNull(result);
	}

	/**
	 * Run the Vote lookForLeader() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:57 PM
	 */
	@Test
	public void testLookForLeader_11()
		throws Exception {
		AuthFastLeaderElection fixture = new AuthFastLeaderElection(new QuorumPeer(), true);

		Vote result = fixture.lookForLeader();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.util.Collections$UnmodifiableMap.<init>(Collections.java:1274)
		//       at java.util.Collections.unmodifiableMap(Collections.java:1260)
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getView(QuorumPeer.java:808)
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getVotingView(QuorumPeer.java:818)
		//       at org.apache.zookeeper.server.quorum.AuthFastLeaderElection.starter(AuthFastLeaderElection.java:757)
		//       at org.apache.zookeeper.server.quorum.AuthFastLeaderElection.<init>(AuthFastLeaderElection.java:748)
		assertNotNull(result);
	}

	/**
	 * Run the Vote lookForLeader() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:57 PM
	 */
	@Test
	public void testLookForLeader_12()
		throws Exception {
		AuthFastLeaderElection fixture = new AuthFastLeaderElection(new QuorumPeer(), true);

		Vote result = fixture.lookForLeader();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.util.Collections$UnmodifiableMap.<init>(Collections.java:1274)
		//       at java.util.Collections.unmodifiableMap(Collections.java:1260)
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getView(QuorumPeer.java:808)
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getVotingView(QuorumPeer.java:818)
		//       at org.apache.zookeeper.server.quorum.AuthFastLeaderElection.starter(AuthFastLeaderElection.java:757)
		//       at org.apache.zookeeper.server.quorum.AuthFastLeaderElection.<init>(AuthFastLeaderElection.java:748)
		assertNotNull(result);
	}

	/**
	 * Run the Vote lookForLeader() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:57 PM
	 */
	@Test
	public void testLookForLeader_13()
		throws Exception {
		AuthFastLeaderElection fixture = new AuthFastLeaderElection(new QuorumPeer(), true);

		Vote result = fixture.lookForLeader();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.util.Collections$UnmodifiableMap.<init>(Collections.java:1274)
		//       at java.util.Collections.unmodifiableMap(Collections.java:1260)
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getView(QuorumPeer.java:808)
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getVotingView(QuorumPeer.java:818)
		//       at org.apache.zookeeper.server.quorum.AuthFastLeaderElection.starter(AuthFastLeaderElection.java:757)
		//       at org.apache.zookeeper.server.quorum.AuthFastLeaderElection.<init>(AuthFastLeaderElection.java:748)
		assertNotNull(result);
	}

	/**
	 * Run the Vote lookForLeader() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:57 PM
	 */
	@Test
	public void testLookForLeader_14()
		throws Exception {
		AuthFastLeaderElection fixture = new AuthFastLeaderElection(new QuorumPeer(), true);

		Vote result = fixture.lookForLeader();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.util.Collections$UnmodifiableMap.<init>(Collections.java:1274)
		//       at java.util.Collections.unmodifiableMap(Collections.java:1260)
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getView(QuorumPeer.java:808)
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getVotingView(QuorumPeer.java:818)
		//       at org.apache.zookeeper.server.quorum.AuthFastLeaderElection.starter(AuthFastLeaderElection.java:757)
		//       at org.apache.zookeeper.server.quorum.AuthFastLeaderElection.<init>(AuthFastLeaderElection.java:748)
		assertNotNull(result);
	}

	/**
	 * Run the Vote lookForLeader() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:57 PM
	 */
	@Test
	public void testLookForLeader_15()
		throws Exception {
		AuthFastLeaderElection fixture = new AuthFastLeaderElection(new QuorumPeer(), true);

		Vote result = fixture.lookForLeader();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.util.Collections$UnmodifiableMap.<init>(Collections.java:1274)
		//       at java.util.Collections.unmodifiableMap(Collections.java:1260)
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getView(QuorumPeer.java:808)
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getVotingView(QuorumPeer.java:818)
		//       at org.apache.zookeeper.server.quorum.AuthFastLeaderElection.starter(AuthFastLeaderElection.java:757)
		//       at org.apache.zookeeper.server.quorum.AuthFastLeaderElection.<init>(AuthFastLeaderElection.java:748)
		assertNotNull(result);
	}

	/**
	 * Run the Vote lookForLeader() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:57 PM
	 */
	@Test
	public void testLookForLeader_16()
		throws Exception {
		AuthFastLeaderElection fixture = new AuthFastLeaderElection(new QuorumPeer(), true);

		Vote result = fixture.lookForLeader();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.util.Collections$UnmodifiableMap.<init>(Collections.java:1274)
		//       at java.util.Collections.unmodifiableMap(Collections.java:1260)
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getView(QuorumPeer.java:808)
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getVotingView(QuorumPeer.java:818)
		//       at org.apache.zookeeper.server.quorum.AuthFastLeaderElection.starter(AuthFastLeaderElection.java:757)
		//       at org.apache.zookeeper.server.quorum.AuthFastLeaderElection.<init>(AuthFastLeaderElection.java:748)
		assertNotNull(result);
	}

	/**
	 * Run the void shutdown() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:57 PM
	 */
	@Test
	public void testShutdown_1()
		throws Exception {
		AuthFastLeaderElection fixture = new AuthFastLeaderElection(new QuorumPeer(), true);

		fixture.shutdown();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.util.Collections$UnmodifiableMap.<init>(Collections.java:1274)
		//       at java.util.Collections.unmodifiableMap(Collections.java:1260)
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getView(QuorumPeer.java:808)
		//       at org.apache.zookeeper.server.quorum.QuorumPeer.getVotingView(QuorumPeer.java:818)
		//       at org.apache.zookeeper.server.quorum.AuthFastLeaderElection.starter(AuthFastLeaderElection.java:757)
		//       at org.apache.zookeeper.server.quorum.AuthFastLeaderElection.<init>(AuthFastLeaderElection.java:748)
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:57 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:57 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:57 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(AuthFastLeaderElectionTest.class);
	}
}