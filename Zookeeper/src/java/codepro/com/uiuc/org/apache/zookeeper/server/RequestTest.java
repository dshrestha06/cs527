package com.uiuc.org.apache.zookeeper.server;

import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.LinkedList;
import java.util.List;
import org.apache.jute.Record;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.data.ACL;
import org.apache.zookeeper.data.Id;
import org.apache.zookeeper.server.NIOServerCnxn;
import org.apache.zookeeper.server.NIOServerCnxnFactory;
import org.apache.zookeeper.server.Request;
import org.apache.zookeeper.server.ServerCnxn;
import org.apache.zookeeper.server.ZooKeeperServer;
import org.apache.zookeeper.txn.TxnHeader;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>RequestTest</code> contains tests for the class <code>{@link Request}</code>.
 *
 * @generatedBy CodePro at 10/16/13 8:21 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class RequestTest {
	/**
	 * Run the Request(ServerCnxn,long,int,int,ByteBuffer,List<Id>) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testRequest_1()
		throws Exception {
		ServerCnxn cnxn = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		long sessionId = 1L;
		int xid = 1;
		int type = 1;
		ByteBuffer bb = ByteBuffer.allocate(0);
		List<Id> authInfo = new LinkedList();

		Request result = new Request(cnxn, sessionId, xid, type, bb, authInfo);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
		assertNotNull(result);
	}

	/**
	 * Run the KeeperException getException() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testGetException_1()
		throws Exception {
		Request fixture = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());
		fixture.setException(new org.apache.zookeeper.KeeperException.UnimplementedException());
		fixture.setOwner(new Object());
		fixture.txn = new ACL();
		fixture.hdr = new TxnHeader();
		fixture.zxid = 1L;

		KeeperException result = fixture.getException();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
		assertNotNull(result);
	}

	/**
	 * Run the Object getOwner() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testGetOwner_1()
		throws Exception {
		Request fixture = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());
		fixture.setException(new org.apache.zookeeper.KeeperException.UnimplementedException());
		fixture.setOwner(new Object());
		fixture.txn = new ACL();
		fixture.hdr = new TxnHeader();
		fixture.zxid = 1L;

		Object result = fixture.getOwner();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
		assertNotNull(result);
	}

	/**
	 * Run the boolean isQuorum(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testIsQuorum_1()
		throws Exception {
		int type = 4;

		boolean result = Request.isQuorum(type);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean isQuorum(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testIsQuorum_2()
		throws Exception {
		int type = 12;

		boolean result = Request.isQuorum(type);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean isQuorum(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testIsQuorum_3()
		throws Exception {
		int type = 8;

		boolean result = Request.isQuorum(type);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean isQuorum(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testIsQuorum_4()
		throws Exception {
		int type = 6;

		boolean result = Request.isQuorum(type);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean isQuorum(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testIsQuorum_5()
		throws Exception {
		int type = 3;

		boolean result = Request.isQuorum(type);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean isQuorum(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testIsQuorum_6()
		throws Exception {
		int type = 14;

		boolean result = Request.isQuorum(type);

		// add additional test code here
		assertEquals(true, result);
	}

	/**
	 * Run the boolean isQuorum(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testIsQuorum_7()
		throws Exception {
		int type = 13;

		boolean result = Request.isQuorum(type);

		// add additional test code here
		assertEquals(true, result);
	}

	/**
	 * Run the boolean isQuorum(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testIsQuorum_8()
		throws Exception {
		int type = 5;

		boolean result = Request.isQuorum(type);

		// add additional test code here
		assertEquals(true, result);
	}

	/**
	 * Run the boolean isQuorum(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testIsQuorum_9()
		throws Exception {
		int type = 7;

		boolean result = Request.isQuorum(type);

		// add additional test code here
		assertEquals(true, result);
	}

	/**
	 * Run the boolean isQuorum(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testIsQuorum_10()
		throws Exception {
		int type = 2;

		boolean result = Request.isQuorum(type);

		// add additional test code here
		assertEquals(true, result);
	}

	/**
	 * Run the boolean isQuorum(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testIsQuorum_11()
		throws Exception {
		int type = -10;

		boolean result = Request.isQuorum(type);

		// add additional test code here
		assertEquals(true, result);
	}

	/**
	 * Run the boolean isQuorum(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testIsQuorum_12()
		throws Exception {
		int type = 1;

		boolean result = Request.isQuorum(type);

		// add additional test code here
		assertEquals(true, result);
	}

	/**
	 * Run the boolean isQuorum(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testIsQuorum_13()
		throws Exception {
		int type = -11;

		boolean result = Request.isQuorum(type);

		// add additional test code here
		assertEquals(true, result);
	}

	/**
	 * Run the boolean isQuorum(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testIsQuorum_14()
		throws Exception {
		int type = -1;

		boolean result = Request.isQuorum(type);

		// add additional test code here
		assertEquals(true, result);
	}

	/**
	 * Run the boolean isQuorum(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testIsQuorum_15()
		throws Exception {
		int type = -2;

		boolean result = Request.isQuorum(type);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean isValid(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testIsValid_1()
		throws Exception {
		int type = 0;

		boolean result = Request.isValid(type);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean isValid(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testIsValid_2()
		throws Exception {
		int type = 11;

		boolean result = Request.isValid(type);

		// add additional test code here
		assertEquals(true, result);
	}

	/**
	 * Run the boolean isValid(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testIsValid_3()
		throws Exception {
		int type = 12;

		boolean result = Request.isValid(type);

		// add additional test code here
		assertEquals(true, result);
	}

	/**
	 * Run the boolean isValid(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testIsValid_4()
		throws Exception {
		int type = 8;

		boolean result = Request.isValid(type);

		// add additional test code here
		assertEquals(true, result);
	}

	/**
	 * Run the boolean isValid(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testIsValid_5()
		throws Exception {
		int type = 7;

		boolean result = Request.isValid(type);

		// add additional test code here
		assertEquals(true, result);
	}

	/**
	 * Run the boolean isValid(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testIsValid_6()
		throws Exception {
		int type = 6;

		boolean result = Request.isValid(type);

		// add additional test code here
		assertEquals(true, result);
	}

	/**
	 * Run the boolean isValid(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testIsValid_7()
		throws Exception {
		int type = 9;

		boolean result = Request.isValid(type);

		// add additional test code here
		assertEquals(true, result);
	}

	/**
	 * Run the boolean isValid(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testIsValid_8()
		throws Exception {
		int type = 5;

		boolean result = Request.isValid(type);

		// add additional test code here
		assertEquals(true, result);
	}

	/**
	 * Run the boolean isValid(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testIsValid_9()
		throws Exception {
		int type = 14;

		boolean result = Request.isValid(type);

		// add additional test code here
		assertEquals(true, result);
	}

	/**
	 * Run the boolean isValid(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testIsValid_10()
		throws Exception {
		int type = 13;

		boolean result = Request.isValid(type);

		// add additional test code here
		assertEquals(true, result);
	}

	/**
	 * Run the boolean isValid(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testIsValid_11()
		throws Exception {
		int type = 4;

		boolean result = Request.isValid(type);

		// add additional test code here
		assertEquals(true, result);
	}

	/**
	 * Run the boolean isValid(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testIsValid_12()
		throws Exception {
		int type = 3;

		boolean result = Request.isValid(type);

		// add additional test code here
		assertEquals(true, result);
	}

	/**
	 * Run the boolean isValid(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testIsValid_13()
		throws Exception {
		int type = -10;

		boolean result = Request.isValid(type);

		// add additional test code here
		assertEquals(true, result);
	}

	/**
	 * Run the boolean isValid(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testIsValid_14()
		throws Exception {
		int type = 2;

		boolean result = Request.isValid(type);

		// add additional test code here
		assertEquals(true, result);
	}

	/**
	 * Run the boolean isValid(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testIsValid_15()
		throws Exception {
		int type = 1;

		boolean result = Request.isValid(type);

		// add additional test code here
		assertEquals(true, result);
	}

	/**
	 * Run the boolean isValid(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testIsValid_16()
		throws Exception {
		int type = -1;

		boolean result = Request.isValid(type);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the String op2String(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testOp2String_1()
		throws Exception {
		int op = 0;

		String result = Request.op2String(op);

		// add additional test code here
		assertEquals("notification", result);
	}

	/**
	 * Run the String op2String(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testOp2String_2()
		throws Exception {
		int op = 1;

		String result = Request.op2String(op);

		// add additional test code here
		assertEquals("create", result);
	}

	/**
	 * Run the String op2String(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testOp2String_3()
		throws Exception {
		int op = 101;

		String result = Request.op2String(op);

		// add additional test code here
		assertEquals("setWatches", result);
	}

	/**
	 * Run the String op2String(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testOp2String_4()
		throws Exception {
		int op = 2;

		String result = Request.op2String(op);

		// add additional test code here
		assertEquals("delete", result);
	}

	/**
	 * Run the String op2String(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testOp2String_5()
		throws Exception {
		int op = 3;

		String result = Request.op2String(op);

		// add additional test code here
		assertEquals("exists", result);
	}

	/**
	 * Run the String op2String(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testOp2String_6()
		throws Exception {
		int op = 4;

		String result = Request.op2String(op);

		// add additional test code here
		assertEquals("getData", result);
	}

	/**
	 * Run the String op2String(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testOp2String_7()
		throws Exception {
		int op = 13;

		String result = Request.op2String(op);

		// add additional test code here
		assertEquals("check", result);
	}

	/**
	 * Run the String op2String(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testOp2String_8()
		throws Exception {
		int op = 14;

		String result = Request.op2String(op);

		// add additional test code here
		assertEquals("multi", result);
	}

	/**
	 * Run the String op2String(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testOp2String_9()
		throws Exception {
		int op = 5;

		String result = Request.op2String(op);

		// add additional test code here
		assertEquals("setData", result);
	}

	/**
	 * Run the String op2String(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testOp2String_10()
		throws Exception {
		int op = 9;

		String result = Request.op2String(op);

		// add additional test code here
		assertEquals("sync:", result);
	}

	/**
	 * Run the String op2String(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testOp2String_11()
		throws Exception {
		int op = 6;

		String result = Request.op2String(op);

		// add additional test code here
		assertEquals("getACL", result);
	}

	/**
	 * Run the String op2String(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testOp2String_12()
		throws Exception {
		int op = 7;

		String result = Request.op2String(op);

		// add additional test code here
		assertEquals("setACL", result);
	}

	/**
	 * Run the String op2String(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testOp2String_13()
		throws Exception {
		int op = 8;

		String result = Request.op2String(op);

		// add additional test code here
		assertEquals("getChildren", result);
	}

	/**
	 * Run the String op2String(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testOp2String_14()
		throws Exception {
		int op = 12;

		String result = Request.op2String(op);

		// add additional test code here
		assertEquals("getChildren2", result);
	}

	/**
	 * Run the String op2String(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testOp2String_15()
		throws Exception {
		int op = 11;

		String result = Request.op2String(op);

		// add additional test code here
		assertEquals("ping", result);
	}

	/**
	 * Run the String op2String(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testOp2String_16()
		throws Exception {
		int op = -2;

		String result = Request.op2String(op);

		// add additional test code here
		assertEquals("unknown -2", result);
	}

	/**
	 * Run the void setException(KeeperException) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testSetException_1()
		throws Exception {
		Request fixture = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());
		fixture.setException(new org.apache.zookeeper.KeeperException.UnimplementedException());
		fixture.setOwner(new Object());
		fixture.txn = new ACL();
		fixture.hdr = new TxnHeader();
		fixture.zxid = 1L;
		KeeperException e = new org.apache.zookeeper.KeeperException.UnimplementedException();

		fixture.setException(e);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void setOwner(Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testSetOwner_1()
		throws Exception {
		Request fixture = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());
		fixture.setException(new org.apache.zookeeper.KeeperException.UnimplementedException());
		fixture.setOwner(new Object());
		fixture.txn = new ACL();
		fixture.hdr = new TxnHeader();
		fixture.zxid = 1L;
		Object owner = new Object();

		fixture.setOwner(owner);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the String toString() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testToString_1()
		throws Exception {
		Request fixture = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, -2, ByteBuffer.allocate(0), new LinkedList());
		fixture.setException(new org.apache.zookeeper.KeeperException.UnimplementedException());
		fixture.setOwner(new Object());
		fixture.txn = new ACL();
		fixture.hdr = null;
		fixture.zxid = 1L;

		String result = fixture.toString();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
		assertNotNull(result);
	}

	/**
	 * Run the String toString() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testToString_2()
		throws Exception {
		Request fixture = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, -2, ByteBuffer.allocate(0), new LinkedList());
		fixture.setException(new org.apache.zookeeper.KeeperException.UnimplementedException());
		fixture.setOwner(new Object());
		fixture.txn = new ACL();
		fixture.hdr = null;
		fixture.zxid = 1L;

		String result = fixture.toString();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
		assertNotNull(result);
	}

	/**
	 * Run the String toString() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testToString_3()
		throws Exception {
		Request fixture = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, -2, ByteBuffer.allocate(0), new LinkedList());
		fixture.setException(new org.apache.zookeeper.KeeperException.UnimplementedException());
		fixture.setOwner(new Object());
		fixture.txn = new ACL();
		fixture.hdr = null;
		fixture.zxid = 1L;

		String result = fixture.toString();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
		assertNotNull(result);
	}

	/**
	 * Run the String toString() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testToString_4()
		throws Exception {
		Request fixture = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, -2, ByteBuffer.allocate(0), new LinkedList());
		fixture.setException(new org.apache.zookeeper.KeeperException.UnimplementedException());
		fixture.setOwner(new Object());
		fixture.txn = new ACL();
		fixture.hdr = new TxnHeader(1L, 1, 1L, 1L, 1);
		fixture.zxid = 1L;

		String result = fixture.toString();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
		assertNotNull(result);
	}

	/**
	 * Run the String toString() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testToString_5()
		throws Exception {
		Request fixture = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, -2, ByteBuffer.allocate(0), new LinkedList());
		fixture.setException(new org.apache.zookeeper.KeeperException.UnimplementedException());
		fixture.setOwner(new Object());
		fixture.txn = new ACL();
		fixture.hdr = null;
		fixture.zxid = 1L;

		String result = fixture.toString();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
		assertNotNull(result);
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(RequestTest.class);
	}
}