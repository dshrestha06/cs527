package com.uiuc.org.apache.zookeeper.client;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import org.apache.zookeeper.client.ConnectStringParser;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>ConnectStringParserTest</code> contains tests for the class <code>{@link ConnectStringParser}</code>.
 *
 * @generatedBy CodePro at 10/16/13 7:31 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class ConnectStringParserTest {
	/**
	 * Run the ConnectStringParser(String) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testConnectStringParser_1()
		throws Exception {
		String connectString = "";

		ConnectStringParser result = new ConnectStringParser(connectString);

		// add additional test code here
		assertNotNull(result);
		assertEquals(null, result.getChrootPath());
	}

	/**
	 * Run the ConnectStringParser(String) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testConnectStringParser_2()
		throws Exception {
		String connectString = "a";

		ConnectStringParser result = new ConnectStringParser(connectString);

		// add additional test code here
		assertNotNull(result);
		assertEquals(null, result.getChrootPath());
	}

	/**
	 * Run the ConnectStringParser(String) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testConnectStringParser_3()
		throws Exception {
		String connectString = "";

		ConnectStringParser result = new ConnectStringParser(connectString);

		// add additional test code here
		assertNotNull(result);
		assertEquals(null, result.getChrootPath());
	}

	/**
	 * Run the ConnectStringParser(String) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testConnectStringParser_4()
		throws Exception {
		String connectString = "";

		ConnectStringParser result = new ConnectStringParser(connectString);

		// add additional test code here
		assertNotNull(result);
		assertEquals(null, result.getChrootPath());
	}

	/**
	 * Run the ConnectStringParser(String) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testConnectStringParser_5()
		throws Exception {
		String connectString = "a";

		ConnectStringParser result = new ConnectStringParser(connectString);

		// add additional test code here
		assertNotNull(result);
		assertEquals(null, result.getChrootPath());
	}

	/**
	 * Run the ConnectStringParser(String) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testConnectStringParser_6()
		throws Exception {
		String connectString = "";

		ConnectStringParser result = new ConnectStringParser(connectString);

		// add additional test code here
		assertNotNull(result);
		assertEquals(null, result.getChrootPath());
	}

	/**
	 * Run the ConnectStringParser(String) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testConnectStringParser_7()
		throws Exception {
		String connectString = "";

		ConnectStringParser result = new ConnectStringParser(connectString);

		// add additional test code here
		assertNotNull(result);
		assertEquals(null, result.getChrootPath());
	}

	/**
	 * Run the ConnectStringParser(String) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testConnectStringParser_8()
		throws Exception {
		String connectString = "a";

		ConnectStringParser result = new ConnectStringParser(connectString);

		// add additional test code here
		assertNotNull(result);
		assertEquals(null, result.getChrootPath());
	}

	/**
	 * Run the ConnectStringParser(String) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testConnectStringParser_9()
		throws Exception {
		String connectString = "";

		ConnectStringParser result = new ConnectStringParser(connectString);

		// add additional test code here
		assertNotNull(result);
		assertEquals(null, result.getChrootPath());
	}

	/**
	 * Run the ConnectStringParser(String) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testConnectStringParser_10()
		throws Exception {
		String connectString = "";

		ConnectStringParser result = new ConnectStringParser(connectString);

		// add additional test code here
		assertNotNull(result);
		assertEquals(null, result.getChrootPath());
	}

	/**
	 * Run the ConnectStringParser(String) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testConnectStringParser_11()
		throws Exception {
		String connectString = "a";

		ConnectStringParser result = new ConnectStringParser(connectString);

		// add additional test code here
		assertNotNull(result);
		assertEquals(null, result.getChrootPath());
	}

	/**
	 * Run the ConnectStringParser(String) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testConnectStringParser_12()
		throws Exception {
		String connectString = "";

		ConnectStringParser result = new ConnectStringParser(connectString);

		// add additional test code here
		assertNotNull(result);
		assertEquals(null, result.getChrootPath());
	}

	/**
	 * Run the ConnectStringParser(String) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testConnectStringParser_13()
		throws Exception {
		String connectString = "";

		ConnectStringParser result = new ConnectStringParser(connectString);

		// add additional test code here
		assertNotNull(result);
		assertEquals(null, result.getChrootPath());
	}

	/**
	 * Run the ConnectStringParser(String) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testConnectStringParser_14()
		throws Exception {
		String connectString = "a";

		ConnectStringParser result = new ConnectStringParser(connectString);

		// add additional test code here
		assertNotNull(result);
		assertEquals(null, result.getChrootPath());
	}

	/**
	 * Run the String getChrootPath() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testGetChrootPath_1()
		throws Exception {
		ConnectStringParser fixture = new ConnectStringParser("");

		String result = fixture.getChrootPath();

		// add additional test code here
		assertEquals(null, result);
	}

	/**
	 * Run the ArrayList<InetSocketAddress> getServerAddresses() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testGetServerAddresses_1()
		throws Exception {
		ConnectStringParser fixture = new ConnectStringParser("");

		ArrayList<InetSocketAddress> result = fixture.getServerAddresses();

		// add additional test code here
		assertNotNull(result);
		assertEquals(1, result.size());
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(ConnectStringParserTest.class);
	}
}