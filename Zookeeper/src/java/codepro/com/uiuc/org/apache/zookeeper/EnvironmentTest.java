package com.uiuc.org.apache.zookeeper;

import java.util.List;
import org.apache.zookeeper.Environment;
import org.junit.*;
import static org.junit.Assert.*;
import org.slf4j.Logger;
import org.slf4j.helpers.NOPLogger;

/**
 * The class <code>EnvironmentTest</code> contains tests for the class <code>{@link Environment}</code>.
 *
 * @generatedBy CodePro at 10/16/13 6:58 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class EnvironmentTest {
	/**
	 * Run the Environment() constructor test.
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testEnvironment_1()
		throws Exception {
		Environment result = new Environment();
		assertNotNull(result);
		// add additional test code here
	}

	/**
	 * Run the List<org.apache.zookeeper.Environment.Entry> list() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testList_1()
		throws Exception {

		List<org.apache.zookeeper.Environment.Entry> result = Environment.list();

		// add additional test code here
		assertNotNull(result);
		assertEquals(15, result.size());
	}

	/**
	 * Run the void logEnv(String,Logger) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testLogEnv_1()
		throws Exception {
		String msg = "";
		Logger log = NOPLogger.NOP_LOGGER;

		Environment.logEnv(msg, log);

		// add additional test code here
	}

	/**
	 * Run the void logEnv(String,Logger) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Test
	public void testLogEnv_2()
		throws Exception {
		String msg = "";
		Logger log = NOPLogger.NOP_LOGGER;

		Environment.logEnv(msg, log);

		// add additional test code here
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 6:58 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(EnvironmentTest.class);
	}
}