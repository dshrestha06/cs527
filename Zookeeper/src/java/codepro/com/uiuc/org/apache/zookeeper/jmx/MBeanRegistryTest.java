package com.uiuc.org.apache.zookeeper.jmx;

import javax.management.InstanceAlreadyExistsException;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import org.apache.zookeeper.jmx.MBeanRegistry;
import org.apache.zookeeper.jmx.ZKMBeanInfo;
import org.apache.zookeeper.server.quorum.LeaderElectionBean;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>MBeanRegistryTest</code> contains tests for the class <code>{@link MBeanRegistry}</code>.
 *
 * @generatedBy CodePro at 10/16/13 7:33 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class MBeanRegistryTest {
	/**
	 * Run the MBeanRegistry() constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testMBeanRegistry_1()
		throws Exception {

		MBeanRegistry result = new MBeanRegistry();

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the MBeanRegistry getInstance() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testGetInstance_1()
		throws Exception {

		MBeanRegistry result = MBeanRegistry.getInstance();

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the MBeanServer getPlatformMBeanServer() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testGetPlatformMBeanServer_1()
		throws Exception {
		MBeanRegistry fixture = new MBeanRegistry();

		MBeanServer result = fixture.getPlatformMBeanServer();

		// add additional test code here
		assertNotNull(result);
		assertEquals("DefaultDomain", result.getDefaultDomain());
		assertEquals(new Integer(22), result.getMBeanCount());
	}

	/**
	 * Run the String makeFullPath(String,ZKMBeanInfo) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testMakeFullPath_1()
		throws Exception {
		MBeanRegistry fixture = new MBeanRegistry();
		String prefix = "";
		ZKMBeanInfo bean = new LeaderElectionBean();

		String result = fixture.makeFullPath(prefix, bean);

		// add additional test code here
		assertEquals("/LeaderElection", result);
	}

	/**
	 * Run the String makeFullPath(String,ZKMBeanInfo) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testMakeFullPath_2()
		throws Exception {
		MBeanRegistry fixture = new MBeanRegistry();
		String prefix = "";
		ZKMBeanInfo bean = null;

		String result = fixture.makeFullPath(prefix, bean);

		// add additional test code here
		assertEquals("/", result);
	}

	/**
	 * Run the String makeFullPath(String,String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testMakeFullPath_3()
		throws Exception {
		MBeanRegistry fixture = new MBeanRegistry();
		String prefix = null;

		String result = fixture.makeFullPath(prefix);

		// add additional test code here
		assertEquals("/", result);
	}

	/**
	 * Run the String makeFullPath(String,String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testMakeFullPath_4()
		throws Exception {
		MBeanRegistry fixture = new MBeanRegistry();
		String prefix = "";

		String result = fixture.makeFullPath(prefix);

		// add additional test code here
		assertEquals("/", result);
	}

	/**
	 * Run the String makeFullPath(String,String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testMakeFullPath_5()
		throws Exception {
		MBeanRegistry fixture = new MBeanRegistry();
		String prefix = "/";

		String result = fixture.makeFullPath(prefix);

		// add additional test code here
		assertEquals("/", result);
	}

	/**
	 * Run the String makeFullPath(String,String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testMakeFullPath_6()
		throws Exception {
		MBeanRegistry fixture = new MBeanRegistry();
		String prefix = null;

		String result = fixture.makeFullPath(prefix);

		// add additional test code here
		assertEquals("/", result);
	}

	/**
	 * Run the ObjectName makeObjectName(String,ZKMBeanInfo) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testMakeObjectName_1()
		throws Exception {
		MBeanRegistry fixture = new MBeanRegistry();
		String path = null;
		ZKMBeanInfo bean = new LeaderElectionBean();

		ObjectName result = fixture.makeObjectName(path, bean);

		// add additional test code here
		assertEquals(null, result);
	}

	/**
	 * Run the ObjectName makeObjectName(String,ZKMBeanInfo) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testMakeObjectName_2()
		throws Exception {
		MBeanRegistry fixture = new MBeanRegistry();
		String path = "";
		ZKMBeanInfo bean = new LeaderElectionBean();

		ObjectName result = fixture.makeObjectName(path, bean);

		// add additional test code here
		assertNotNull(result);
		assertEquals("org.apache.ZooKeeperService:name0=LeaderElection", result.toString());
		assertEquals("org.apache.ZooKeeperService:name0=LeaderElection", result.getCanonicalName());
		assertEquals("org.apache.ZooKeeperService", result.getDomain());
		assertEquals("name0=LeaderElection", result.getCanonicalKeyPropertyListString());
		assertEquals("name0=LeaderElection", result.getKeyPropertyListString());
		assertEquals(false, result.isDomainPattern());
		assertEquals(false, result.isPattern());
		assertEquals(false, result.isPropertyListPattern());
		assertEquals(false, result.isPropertyPattern());
		assertEquals(false, result.isPropertyValuePattern());
	}

	/**
	 * Run the ObjectName makeObjectName(String,ZKMBeanInfo) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testMakeObjectName_3()
		throws Exception {
		MBeanRegistry fixture = new MBeanRegistry();
		String path = "";
		ZKMBeanInfo bean = new LeaderElectionBean();

		ObjectName result = fixture.makeObjectName(path, bean);

		// add additional test code here
		assertNotNull(result);
		assertEquals("org.apache.ZooKeeperService:name0=LeaderElection", result.toString());
		assertEquals("org.apache.ZooKeeperService:name0=LeaderElection", result.getCanonicalName());
		assertEquals("org.apache.ZooKeeperService", result.getDomain());
		assertEquals("name0=LeaderElection", result.getCanonicalKeyPropertyListString());
		assertEquals("name0=LeaderElection", result.getKeyPropertyListString());
		assertEquals(false, result.isDomainPattern());
		assertEquals(false, result.isPattern());
		assertEquals(false, result.isPropertyListPattern());
		assertEquals(false, result.isPropertyPattern());
		assertEquals(false, result.isPropertyValuePattern());
	}

	/**
	 * Run the void register(ZKMBeanInfo,ZKMBeanInfo) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testRegister_1()
		throws Exception {
		MBeanRegistry fixture = new MBeanRegistry();
		ZKMBeanInfo bean = null;
		ZKMBeanInfo parent = new LeaderElectionBean();

		fixture.register(bean, parent);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.util.concurrent.ConcurrentHashMap.put(ConcurrentHashMap.java:882)
		//       at org.apache.zookeeper.jmx.MBeanRegistry.register(MBeanRegistry.java:92)
	}

	/**
	 * Run the void register(ZKMBeanInfo,ZKMBeanInfo) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testRegister_2()
		throws Exception {
		MBeanRegistry fixture = new MBeanRegistry();
		ZKMBeanInfo bean = new LeaderElectionBean();
		ZKMBeanInfo parent = new LeaderElectionBean();

		fixture.register(bean, parent);

		// add additional test code here
	}

	/**
	 * Run the void register(ZKMBeanInfo,ZKMBeanInfo) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testRegister_3()
		throws Exception {
		MBeanRegistry fixture = new MBeanRegistry();
		ZKMBeanInfo bean = new LeaderElectionBean();
		ZKMBeanInfo parent = null;

		fixture.register(bean, parent);

		// add additional test code here
	}

	/**
	 * Run the void register(ZKMBeanInfo,ZKMBeanInfo) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test(expected = javax.management.InstanceAlreadyExistsException.class)
	public void testRegister_4()
		throws Exception {
		MBeanRegistry fixture = new MBeanRegistry();
		ZKMBeanInfo bean = new LeaderElectionBean();
		ZKMBeanInfo parent = new LeaderElectionBean();

		fixture.register(bean, parent);

		// add additional test code here
	}

	/**
	 * Run the void register(ZKMBeanInfo,ZKMBeanInfo) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test(expected = javax.management.InstanceAlreadyExistsException.class)
	public void testRegister_5()
		throws Exception {
		MBeanRegistry fixture = new MBeanRegistry();
		ZKMBeanInfo bean = new LeaderElectionBean();
		ZKMBeanInfo parent = new LeaderElectionBean();

		fixture.register(bean, parent);

		// add additional test code here
	}

	/**
	 * Run the void register(ZKMBeanInfo,ZKMBeanInfo) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test(expected = javax.management.InstanceAlreadyExistsException.class)
	public void testRegister_6()
		throws Exception {
		MBeanRegistry fixture = new MBeanRegistry();
		ZKMBeanInfo bean = new LeaderElectionBean();
		ZKMBeanInfo parent = null;

		fixture.register(bean, parent);

		// add additional test code here
	}

	/**
	 * Run the void unregister(ZKMBeanInfo) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testUnregister_1()
		throws Exception {
		MBeanRegistry fixture = new MBeanRegistry();
		ZKMBeanInfo bean = null;

		fixture.unregister(bean);

		// add additional test code here
	}

	/**
	 * Run the void unregister(ZKMBeanInfo) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testUnregister_2()
		throws Exception {
		MBeanRegistry fixture = new MBeanRegistry();
		ZKMBeanInfo bean = new LeaderElectionBean();

		fixture.unregister(bean);

		// add additional test code here
	}

	/**
	 * Run the void unregister(ZKMBeanInfo) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testUnregister_3()
		throws Exception {
		MBeanRegistry fixture = new MBeanRegistry();
		ZKMBeanInfo bean = new LeaderElectionBean();

		fixture.unregister(bean);

		// add additional test code here
	}

	/**
	 * Run the void unregisterAll() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testUnregisterAll_1()
		throws Exception {
		MBeanRegistry fixture = new MBeanRegistry();

		fixture.unregisterAll();

		// add additional test code here
	}

	/**
	 * Run the void unregisterAll() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testUnregisterAll_2()
		throws Exception {
		MBeanRegistry fixture = new MBeanRegistry();

		fixture.unregisterAll();

		// add additional test code here
	}

	/**
	 * Run the void unregisterAll() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testUnregisterAll_3()
		throws Exception {
		MBeanRegistry fixture = new MBeanRegistry();

		fixture.unregisterAll();

		// add additional test code here
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(MBeanRegistryTest.class);
	}
}