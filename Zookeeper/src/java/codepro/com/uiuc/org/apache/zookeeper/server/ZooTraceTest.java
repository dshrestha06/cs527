package com.uiuc.org.apache.zookeeper.server;

import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.LinkedList;
import java.util.List;
import org.apache.zookeeper.data.Id;
import org.apache.zookeeper.server.NIOServerCnxn;
import org.apache.zookeeper.server.NIOServerCnxnFactory;
import org.apache.zookeeper.server.Request;
import org.apache.zookeeper.server.ServerCnxn;
import org.apache.zookeeper.server.ZooKeeperServer;
import org.apache.zookeeper.server.ZooTrace;
import org.apache.zookeeper.server.quorum.QuorumPacket;
import org.junit.*;
import static org.junit.Assert.*;
import org.slf4j.Logger;
import org.slf4j.helpers.NOPLogger;

/**
 * The class <code>ZooTraceTest</code> contains tests for the class <code>{@link ZooTrace}</code>.
 *
 * @generatedBy CodePro at 10/16/13 8:25 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class ZooTraceTest {
	/**
	 * Run the ZooTrace() constructor test.
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testZooTrace_1()
		throws Exception {
		ZooTrace result = new ZooTrace();
		assertNotNull(result);
		// add additional test code here
	}

	/**
	 * Run the long getTextTraceLevel() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testGetTextTraceLevel_1()
		throws Exception {

		long result = ZooTrace.getTextTraceLevel();

		// add additional test code here
		assertEquals(306L, result);
	}

	/**
	 * Run the boolean isTraceEnabled(Logger,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testIsTraceEnabled_1()
		throws Exception {
		Logger log = NOPLogger.NOP_LOGGER;
		long mask = 1L;

		boolean result = ZooTrace.isTraceEnabled(log, mask);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean isTraceEnabled(Logger,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testIsTraceEnabled_2()
		throws Exception {
		Logger log = NOPLogger.NOP_LOGGER;
		long mask = 1L;

		boolean result = ZooTrace.isTraceEnabled(log, mask);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean isTraceEnabled(Logger,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testIsTraceEnabled_3()
		throws Exception {
		Logger log = NOPLogger.NOP_LOGGER;
		long mask = 1L;

		boolean result = ZooTrace.isTraceEnabled(log, mask);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the void logQuorumPacket(Logger,long,char,QuorumPacket) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testLogQuorumPacket_1()
		throws Exception {
		Logger log = NOPLogger.NOP_LOGGER;
		long mask = 1L;
		char direction = '';
		QuorumPacket qp = new QuorumPacket();

		ZooTrace.logQuorumPacket(log, mask, direction, qp);

		// add additional test code here
	}

	/**
	 * Run the void logRequest(Logger,long,char,Request,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testLogRequest_1()
		throws Exception {
		Logger log = NOPLogger.NOP_LOGGER;
		long mask = 1L;
		char rp = '';
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());
		String header = "";

		ZooTrace.logRequest(log, mask, rp, request, header);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void logRequest(Logger,long,char,Request,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testLogRequest_2()
		throws Exception {
		Logger log = NOPLogger.NOP_LOGGER;
		long mask = 1L;
		char rp = '';
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());
		String header = "";

		ZooTrace.logRequest(log, mask, rp, request, header);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void logTraceMessage(Logger,long,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testLogTraceMessage_1()
		throws Exception {
		Logger log = NOPLogger.NOP_LOGGER;
		long mask = 1L;
		String msg = "";

		ZooTrace.logTraceMessage(log, mask, msg);

		// add additional test code here
	}

	/**
	 * Run the void logTraceMessage(Logger,long,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testLogTraceMessage_2()
		throws Exception {
		Logger log = NOPLogger.NOP_LOGGER;
		long mask = 1L;
		String msg = "";

		ZooTrace.logTraceMessage(log, mask, msg);

		// add additional test code here
	}

	/**
	 * Run the void setTextTraceLevel(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Test
	public void testSetTextTraceLevel_1()
		throws Exception {
		long mask = 1L;

		ZooTrace.setTextTraceLevel(mask);

		// add additional test code here
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 8:25 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(ZooTraceTest.class);
	}
}