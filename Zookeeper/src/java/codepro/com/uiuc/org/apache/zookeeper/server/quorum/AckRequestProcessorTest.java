package com.uiuc.org.apache.zookeeper.server.quorum;

import static org.junit.Assert.assertNotNull;

import org.apache.zookeeper.server.quorum.AckRequestProcessor;
import org.apache.zookeeper.server.quorum.Leader;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The class <code>AckRequestProcessorTest</code> contains tests for the class <code>{@link AckRequestProcessor}</code>.
 *
 * @generatedBy CodePro at 10/16/13 7:58 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class AckRequestProcessorTest {
	/**
	 * Run the AckRequestProcessor(Leader) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testAckRequestProcessor_1()
		throws Exception {
		Leader leader = null;

		AckRequestProcessor result = new AckRequestProcessor(leader);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(AckRequestProcessorTest.class);
	}
}