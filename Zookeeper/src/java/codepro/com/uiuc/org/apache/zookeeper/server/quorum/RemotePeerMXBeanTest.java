package com.uiuc.org.apache.zookeeper.server.quorum;

import org.apache.zookeeper.server.quorum.RemotePeerMXBean;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>RemotePeerMXBeanTest</code> contains tests for the class <code>{@link RemotePeerMXBean}</code>.
 *
 * @generatedBy CodePro at 10/16/13 8:17 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class RemotePeerMXBeanTest {
	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(RemotePeerMXBeanTest.class);
	}
}