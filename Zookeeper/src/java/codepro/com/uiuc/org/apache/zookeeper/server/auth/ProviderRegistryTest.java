package com.uiuc.org.apache.zookeeper.server.auth;

import org.apache.zookeeper.server.auth.AuthenticationProvider;
import org.apache.zookeeper.server.auth.ProviderRegistry;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>ProviderRegistryTest</code> contains tests for the class <code>{@link ProviderRegistry}</code>.
 *
 * @generatedBy CodePro at 10/16/13 7:33 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class ProviderRegistryTest {
	/**
	 * Run the ProviderRegistry() constructor test.
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testProviderRegistry_1()
		throws Exception {
		ProviderRegistry result = new ProviderRegistry();
		assertNotNull(result);
		// add additional test code here
	}

	/**
	 * Run the AuthenticationProvider getProvider(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testGetProvider_1()
		throws Exception {
		String scheme = "";

		AuthenticationProvider result = ProviderRegistry.getProvider(scheme);

		// add additional test code here
		assertEquals(null, result);
	}

	/**
	 * Run the AuthenticationProvider getProvider(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testGetProvider_2()
		throws Exception {
		String scheme = "";

		AuthenticationProvider result = ProviderRegistry.getProvider(scheme);

		// add additional test code here
		assertEquals(null, result);
	}

	/**
	 * Run the void initialize() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testInitialize_1()
		throws Exception {

		ProviderRegistry.initialize();

		// add additional test code here
	}

	/**
	 * Run the void initialize() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testInitialize_2()
		throws Exception {

		ProviderRegistry.initialize();

		// add additional test code here
	}

	/**
	 * Run the void initialize() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testInitialize_3()
		throws Exception {

		ProviderRegistry.initialize();

		// add additional test code here
	}

	/**
	 * Run the void initialize() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testInitialize_4()
		throws Exception {

		ProviderRegistry.initialize();

		// add additional test code here
	}

	/**
	 * Run the void initialize() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testInitialize_5()
		throws Exception {

		ProviderRegistry.initialize();

		// add additional test code here
	}

	/**
	 * Run the void initialize() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testInitialize_6()
		throws Exception {

		ProviderRegistry.initialize();

		// add additional test code here
	}

	/**
	 * Run the void initialize() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testInitialize_7()
		throws Exception {

		ProviderRegistry.initialize();

		// add additional test code here
	}

	/**
	 * Run the String listProviders() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testListProviders_1()
		throws Exception {

		String result = ProviderRegistry.listProviders();

		// add additional test code here
		assertEquals("digest ip ", result);
	}

	/**
	 * Run the String listProviders() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testListProviders_2()
		throws Exception {

		String result = ProviderRegistry.listProviders();

		// add additional test code here
		assertEquals("digest ip ", result);
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(ProviderRegistryTest.class);
	}
}