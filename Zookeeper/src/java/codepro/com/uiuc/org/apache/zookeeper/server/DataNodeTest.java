package com.uiuc.org.apache.zookeeper.server;

import java.io.ByteArrayOutputStream;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.util.HashSet;
import java.util.Set;
import org.apache.jute.BinaryInputArchive;
import org.apache.jute.BinaryOutputArchive;
import org.apache.jute.InputArchive;
import org.apache.jute.OutputArchive;
import org.apache.zookeeper.data.Stat;
import org.apache.zookeeper.data.StatPersisted;
import org.apache.zookeeper.server.DataNode;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>DataNodeTest</code> contains tests for the class <code>{@link DataNode}</code>.
 *
 * @generatedBy CodePro at 10/16/13 8:20 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class DataNodeTest {
	/**
	 * Run the DataNode() constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:20 PM
	 */
	@Test
	public void testDataNode_1()
		throws Exception {

		DataNode result = new DataNode();

		// add additional test code here
		assertNotNull(result);
		assertEquals(null, result.getChildren());
	}

	/**
	 * Run the DataNode(DataNode,byte[],Long,StatPersisted) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:20 PM
	 */
	@Test
	public void testDataNode_2()
		throws Exception {
		DataNode parent = new DataNode(new DataNode((DataNode) null, new byte[] {(byte) -1, (byte) 0, (byte) 1, Byte.MAX_VALUE, Byte.MIN_VALUE}, new Long(-1L), new StatPersisted()), new byte[] {}, new Long(1L), new StatPersisted());
		byte[] data = new byte[] {};
		Long acl = new Long(1L);
		StatPersisted stat = new StatPersisted();

		DataNode result = new DataNode(parent, data, acl, stat);

		// add additional test code here
		assertNotNull(result);
		assertEquals(null, result.getChildren());
	}

	/**
	 * Run the boolean addChild(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:20 PM
	 */
	@Test
	public void testAddChild_1()
		throws Exception {
		DataNode fixture = new DataNode(new DataNode(new DataNode((DataNode) null, new byte[] {(byte) -1, (byte) 0, (byte) 1, Byte.MAX_VALUE, Byte.MIN_VALUE}, new Long(-1L), new StatPersisted()), new byte[] {}, new Long(1L), new StatPersisted()), new byte[] {}, new Long(1L), new StatPersisted());
		fixture.setChildren(new HashSet());
		String child = "";

		boolean result = fixture.addChild(child);

		// add additional test code here
		assertEquals(true, result);
	}

	/**
	 * Run the boolean addChild(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:20 PM
	 */
	@Test
	public void testAddChild_2()
		throws Exception {
		DataNode fixture = new DataNode(new DataNode(new DataNode((DataNode) null, new byte[] {(byte) -1, (byte) 0, (byte) 1, Byte.MAX_VALUE, Byte.MIN_VALUE}, new Long(-1L), new StatPersisted()), new byte[] {}, new Long(1L), new StatPersisted()), new byte[] {}, new Long(1L), new StatPersisted());
		fixture.setChildren(null);
		String child = "";

		boolean result = fixture.addChild(child);

		// add additional test code here
		assertEquals(true, result);
	}

	/**
	 * Run the void copyStat(Stat) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:20 PM
	 */
	@Test
	public void testCopyStat_1()
		throws Exception {
		DataNode fixture = new DataNode(new DataNode(new DataNode((DataNode) null, new byte[] {(byte) -1, (byte) 0, (byte) 1, Byte.MAX_VALUE, Byte.MIN_VALUE}, new Long(-1L), new StatPersisted()), new byte[] {}, new Long(1L), new StatPersisted()), new byte[] {}, new Long(1L), new StatPersisted());
		fixture.setChildren(new HashSet());
		Stat to = new Stat();

		fixture.copyStat(to);

		// add additional test code here
	}

	/**
	 * Run the void copyStat(Stat) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:20 PM
	 */
	@Test
	public void testCopyStat_2()
		throws Exception {
		DataNode fixture = new DataNode(new DataNode(new DataNode((DataNode) null, new byte[] {(byte) -1, (byte) 0, (byte) 1, Byte.MAX_VALUE, Byte.MIN_VALUE}, new Long(-1L), new StatPersisted()), new byte[] {}, new Long(1L), new StatPersisted()), new byte[] {}, new Long(1L), new StatPersisted());
		fixture.setChildren(new HashSet());
		Stat to = new Stat();

		fixture.copyStat(to);

		// add additional test code here
	}

	/**
	 * Run the void copyStat(Stat) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:20 PM
	 */
	@Test
	public void testCopyStat_3()
		throws Exception {
		DataNode fixture = new DataNode(new DataNode(new DataNode((DataNode) null, new byte[] {(byte) -1, (byte) 0, (byte) 1, Byte.MAX_VALUE, Byte.MIN_VALUE}, new Long(-1L), new StatPersisted()), new byte[] {}, new Long(1L), new StatPersisted()), new byte[] {}, new Long(1L), new StatPersisted());
		fixture.setChildren(null);
		Stat to = new Stat();

		fixture.copyStat(to);

		// add additional test code here
	}

	/**
	 * Run the void copyStat(Stat) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:20 PM
	 */
	@Test
	public void testCopyStat_4()
		throws Exception {
		DataNode fixture = new DataNode(new DataNode(new DataNode((DataNode) null, new byte[] {(byte) -1, (byte) 0, (byte) 1, Byte.MAX_VALUE, Byte.MIN_VALUE}, new Long(-1L), new StatPersisted()), new byte[] {}, new Long(1L), new StatPersisted()), new byte[] {}, new Long(1L), new StatPersisted());
		fixture.setChildren(null);
		Stat to = new Stat();

		fixture.copyStat(to);

		// add additional test code here
	}

	/**
	 * Run the void deserialize(InputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:20 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testDeserialize_1()
		throws Exception {
		DataNode fixture = new DataNode(new DataNode(new DataNode((DataNode) null, new byte[] {(byte) -1, (byte) 0, (byte) 1, Byte.MAX_VALUE, Byte.MIN_VALUE}, new Long(-1L), new StatPersisted()), new byte[] {}, new Long(1L), new StatPersisted()), new byte[] {}, new Long(1L), new StatPersisted());
		fixture.setChildren(new HashSet());
		InputArchive archive = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));
		String tag = "";

		fixture.deserialize(archive, tag);

		// add additional test code here
	}

	/**
	 * Run the void deserialize(InputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:20 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testDeserialize_2()
		throws Exception {
		DataNode fixture = new DataNode(new DataNode(new DataNode((DataNode) null, new byte[] {(byte) -1, (byte) 0, (byte) 1, Byte.MAX_VALUE, Byte.MIN_VALUE}, new Long(-1L), new StatPersisted()), new byte[] {}, new Long(1L), new StatPersisted()), new byte[] {}, new Long(1L), new StatPersisted());
		fixture.setChildren(new HashSet());
		InputArchive archive = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));
		String tag = "";

		fixture.deserialize(archive, tag);

		// add additional test code here
	}

	/**
	 * Run the void deserialize(InputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:20 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testDeserialize_3()
		throws Exception {
		DataNode fixture = new DataNode(new DataNode(new DataNode((DataNode) null, new byte[] {(byte) -1, (byte) 0, (byte) 1, Byte.MAX_VALUE, Byte.MIN_VALUE}, new Long(-1L), new StatPersisted()), new byte[] {}, new Long(1L), new StatPersisted()), new byte[] {}, new Long(1L), new StatPersisted());
		fixture.setChildren(new HashSet());
		InputArchive archive = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));
		String tag = "";

		fixture.deserialize(archive, tag);

		// add additional test code here
	}

	/**
	 * Run the void deserialize(InputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:20 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testDeserialize_4()
		throws Exception {
		DataNode fixture = new DataNode(new DataNode(new DataNode((DataNode) null, new byte[] {(byte) -1, (byte) 0, (byte) 1, Byte.MAX_VALUE, Byte.MIN_VALUE}, new Long(-1L), new StatPersisted()), new byte[] {}, new Long(1L), new StatPersisted()), new byte[] {}, new Long(1L), new StatPersisted());
		fixture.setChildren(new HashSet());
		InputArchive archive = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));
		String tag = "";

		fixture.deserialize(archive, tag);

		// add additional test code here
	}

	/**
	 * Run the void deserialize(InputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:20 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testDeserialize_5()
		throws Exception {
		DataNode fixture = new DataNode(new DataNode(new DataNode((DataNode) null, new byte[] {(byte) -1, (byte) 0, (byte) 1, Byte.MAX_VALUE, Byte.MIN_VALUE}, new Long(-1L), new StatPersisted()), new byte[] {}, new Long(1L), new StatPersisted()), new byte[] {}, new Long(1L), new StatPersisted());
		fixture.setChildren(new HashSet());
		InputArchive archive = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));
		String tag = "";

		fixture.deserialize(archive, tag);

		// add additional test code here
	}

	/**
	 * Run the void deserialize(InputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:20 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testDeserialize_6()
		throws Exception {
		DataNode fixture = new DataNode(new DataNode(new DataNode((DataNode) null, new byte[] {(byte) -1, (byte) 0, (byte) 1, Byte.MAX_VALUE, Byte.MIN_VALUE}, new Long(-1L), new StatPersisted()), new byte[] {}, new Long(1L), new StatPersisted()), new byte[] {}, new Long(1L), new StatPersisted());
		fixture.setChildren(new HashSet());
		InputArchive archive = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));
		String tag = "";

		fixture.deserialize(archive, tag);

		// add additional test code here
	}

	/**
	 * Run the Set<String> getChildren() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:20 PM
	 */
	@Test
	public void testGetChildren_1()
		throws Exception {
		DataNode fixture = new DataNode(new DataNode(new DataNode((DataNode) null, new byte[] {(byte) -1, (byte) 0, (byte) 1, Byte.MAX_VALUE, Byte.MIN_VALUE}, new Long(-1L), new StatPersisted()), new byte[] {}, new Long(1L), new StatPersisted()), new byte[] {}, new Long(1L), new StatPersisted());
		fixture.setChildren(new HashSet());

		Set<String> result = fixture.getChildren();

		// add additional test code here
		assertNotNull(result);
		assertEquals(0, result.size());
	}

	/**
	 * Run the boolean removeChild(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:20 PM
	 */
	@Test
	public void testRemoveChild_1()
		throws Exception {
		DataNode fixture = new DataNode(new DataNode(new DataNode((DataNode) null, new byte[] {(byte) -1, (byte) 0, (byte) 1, Byte.MAX_VALUE, Byte.MIN_VALUE}, new Long(-1L), new StatPersisted()), new byte[] {}, new Long(1L), new StatPersisted()), new byte[] {}, new Long(1L), new StatPersisted());
		fixture.setChildren(null);
		String child = "";

		boolean result = fixture.removeChild(child);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean removeChild(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:20 PM
	 */
	@Test
	public void testRemoveChild_2()
		throws Exception {
		DataNode fixture = new DataNode(new DataNode(new DataNode((DataNode) null, new byte[] {(byte) -1, (byte) 0, (byte) 1, Byte.MAX_VALUE, Byte.MIN_VALUE}, new Long(-1L), new StatPersisted()), new byte[] {}, new Long(1L), new StatPersisted()), new byte[] {}, new Long(1L), new StatPersisted());
		fixture.setChildren(new HashSet());
		String child = "";

		boolean result = fixture.removeChild(child);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean removeChild(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:20 PM
	 */
	@Test
	public void testRemoveChild_3()
		throws Exception {
		DataNode fixture = new DataNode(new DataNode(new DataNode((DataNode) null, new byte[] {(byte) -1, (byte) 0, (byte) 1, Byte.MAX_VALUE, Byte.MIN_VALUE}, new Long(-1L), new StatPersisted()), new byte[] {}, new Long(1L), new StatPersisted()), new byte[] {}, new Long(1L), new StatPersisted());
		fixture.setChildren(new HashSet());
		String child = "";

		boolean result = fixture.removeChild(child);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the void serialize(OutputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:20 PM
	 */
	@Test
	public void testSerialize_1()
		throws Exception {
		DataNode fixture = new DataNode(new DataNode(new DataNode((DataNode) null, new byte[] {(byte) -1, (byte) 0, (byte) 1, Byte.MAX_VALUE, Byte.MIN_VALUE}, new Long(-1L), new StatPersisted()), new byte[] {}, new Long(1L), new StatPersisted()), new byte[] {}, new Long(1L), new StatPersisted());
		fixture.setChildren(new HashSet());
		OutputArchive archive = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		String tag = "";

		fixture.serialize(archive, tag);

		// add additional test code here
	}

	/**
	 * Run the void serialize(OutputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:20 PM
	 */
	@Test
	public void testSerialize_2()
		throws Exception {
		DataNode fixture = new DataNode(new DataNode(new DataNode((DataNode) null, new byte[] {(byte) -1, (byte) 0, (byte) 1, Byte.MAX_VALUE, Byte.MIN_VALUE}, new Long(-1L), new StatPersisted()), new byte[] {}, new Long(1L), new StatPersisted()), new byte[] {}, new Long(1L), new StatPersisted());
		fixture.setChildren(new HashSet());
		OutputArchive archive = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		String tag = "";

		fixture.serialize(archive, tag);

		// add additional test code here
	}

	/**
	 * Run the void serialize(OutputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:20 PM
	 */
	@Test
	public void testSerialize_3()
		throws Exception {
		DataNode fixture = new DataNode(new DataNode(new DataNode((DataNode) null, new byte[] {(byte) -1, (byte) 0, (byte) 1, Byte.MAX_VALUE, Byte.MIN_VALUE}, new Long(-1L), new StatPersisted()), new byte[] {}, new Long(1L), new StatPersisted()), new byte[] {}, new Long(1L), new StatPersisted());
		fixture.setChildren(new HashSet());
		OutputArchive archive = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		String tag = "";

		fixture.serialize(archive, tag);

		// add additional test code here
	}

	/**
	 * Run the void serialize(OutputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:20 PM
	 */
	@Test
	public void testSerialize_4()
		throws Exception {
		DataNode fixture = new DataNode(new DataNode(new DataNode((DataNode) null, new byte[] {(byte) -1, (byte) 0, (byte) 1, Byte.MAX_VALUE, Byte.MIN_VALUE}, new Long(-1L), new StatPersisted()), new byte[] {}, new Long(1L), new StatPersisted()), new byte[] {}, new Long(1L), new StatPersisted());
		fixture.setChildren(new HashSet());
		OutputArchive archive = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		String tag = "";

		fixture.serialize(archive, tag);

		// add additional test code here
	}

	/**
	 * Run the void serialize(OutputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:20 PM
	 */
	@Test
	public void testSerialize_5()
		throws Exception {
		DataNode fixture = new DataNode(new DataNode(new DataNode((DataNode) null, new byte[] {(byte) -1, (byte) 0, (byte) 1, Byte.MAX_VALUE, Byte.MIN_VALUE}, new Long(-1L), new StatPersisted()), new byte[] {}, new Long(1L), new StatPersisted()), new byte[] {}, new Long(1L), new StatPersisted());
		fixture.setChildren(new HashSet());
		OutputArchive archive = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		String tag = "";

		fixture.serialize(archive, tag);

		// add additional test code here
	}

	/**
	 * Run the void serialize(OutputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:20 PM
	 */
	@Test
	public void testSerialize_6()
		throws Exception {
		DataNode fixture = new DataNode(new DataNode(new DataNode((DataNode) null, new byte[] {(byte) -1, (byte) 0, (byte) 1, Byte.MAX_VALUE, Byte.MIN_VALUE}, new Long(-1L), new StatPersisted()), new byte[] {}, new Long(1L), new StatPersisted()), new byte[] {}, new Long(1L), new StatPersisted());
		fixture.setChildren(new HashSet());
		OutputArchive archive = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		String tag = "";

		fixture.serialize(archive, tag);

		// add additional test code here
	}

	/**
	 * Run the void setChildren(HashSet<String>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:20 PM
	 */
	@Test
	public void testSetChildren_1()
		throws Exception {
		DataNode fixture = new DataNode(new DataNode(new DataNode((DataNode) null, new byte[] {(byte) -1, (byte) 0, (byte) 1, Byte.MAX_VALUE, Byte.MIN_VALUE}, new Long(-1L), new StatPersisted()), new byte[] {}, new Long(1L), new StatPersisted()), new byte[] {}, new Long(1L), new StatPersisted());
		fixture.setChildren(new HashSet());
		HashSet<String> children = new HashSet();

		fixture.setChildren(children);

		// add additional test code here
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:20 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:20 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 8:20 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(DataNodeTest.class);
	}
}