package com.uiuc.org.apache.zookeeper.server;

import java.io.EOFException;
import java.nio.ByteBuffer;
import org.apache.jute.Record;
import org.apache.zookeeper.data.ACL;
import org.apache.zookeeper.server.ByteBufferInputStream;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>ByteBufferInputStreamTest</code> contains tests for the class <code>{@link ByteBufferInputStream}</code>.
 *
 * @generatedBy CodePro at 10/16/13 8:17 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class ByteBufferInputStreamTest {
	/**
	 * Run the ByteBufferInputStream(ByteBuffer) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@Test
	public void testByteBufferInputStream_1()
		throws Exception {
		ByteBuffer bb = ByteBuffer.allocate(0);

		ByteBufferInputStream result = new ByteBufferInputStream(bb);

		// add additional test code here
		assertNotNull(result);
		assertEquals(-1, result.read());
		assertEquals(0, result.available());
		assertEquals(false, result.markSupported());
	}

	/**
	 * Run the int available() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@Test
	public void testAvailable_1()
		throws Exception {
		ByteBufferInputStream fixture = new ByteBufferInputStream(ByteBuffer.allocate(0));

		int result = fixture.available();

		// add additional test code here
		assertEquals(0, result);
	}

	/**
	 * Run the void byteBuffer2Record(ByteBuffer,Record) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@Test(expected = java.io.EOFException.class)
	public void testByteBuffer2Record_1()
		throws Exception {
		ByteBuffer bb = ByteBuffer.allocate(0);
		Record record = new ACL();

		ByteBufferInputStream.byteBuffer2Record(bb, record);

		// add additional test code here
	}

	/**
	 * Run the void byteBuffer2Record(ByteBuffer,Record) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@Test(expected = java.io.EOFException.class)
	public void testByteBuffer2Record_2()
		throws Exception {
		ByteBuffer bb = ByteBuffer.allocate(0);
		Record record = new ACL();

		ByteBufferInputStream.byteBuffer2Record(bb, record);

		// add additional test code here
	}

	/**
	 * Run the int read() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@Test
	public void testRead_1()
		throws Exception {
		ByteBufferInputStream fixture = new ByteBufferInputStream(ByteBuffer.allocate(0));

		int result = fixture.read();

		// add additional test code here
		assertEquals(-1, result);
	}

	/**
	 * Run the int read() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@Test
	public void testRead_2()
		throws Exception {
		ByteBufferInputStream fixture = new ByteBufferInputStream(ByteBuffer.allocate(0));

		int result = fixture.read();

		// add additional test code here
		assertEquals(-1, result);
	}

	/**
	 * Run the int read(byte[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@Test
	public void testRead_3()
		throws Exception {
		ByteBufferInputStream fixture = new ByteBufferInputStream(ByteBuffer.allocate(0));
		byte[] b = new byte[] {};

		int result = fixture.read(b);

		// add additional test code here
		assertEquals(-1, result);
	}

	/**
	 * Run the int read(byte[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@Test
	public void testRead_4()
		throws Exception {
		ByteBufferInputStream fixture = new ByteBufferInputStream(ByteBuffer.allocate(0));
		byte[] b = new byte[] {};

		int result = fixture.read(b);

		// add additional test code here
		assertEquals(-1, result);
	}

	/**
	 * Run the int read(byte[],int,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@Test
	public void testRead_5()
		throws Exception {
		ByteBufferInputStream fixture = new ByteBufferInputStream(ByteBuffer.allocate(0));
		byte[] b = new byte[] {};
		int off = 1;
		int len = 1;

		int result = fixture.read(b, off, len);

		// add additional test code here
		assertEquals(-1, result);
	}

	/**
	 * Run the int read(byte[],int,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@Test
	public void testRead_6()
		throws Exception {
		ByteBufferInputStream fixture = new ByteBufferInputStream(ByteBuffer.allocate(0));
		byte[] b = new byte[] {};
		int off = 1;
		int len = 1;

		int result = fixture.read(b, off, len);

		// add additional test code here
		assertEquals(-1, result);
	}

	/**
	 * Run the int read(byte[],int,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@Test
	public void testRead_7()
		throws Exception {
		ByteBufferInputStream fixture = new ByteBufferInputStream(ByteBuffer.allocate(0));
		byte[] b = new byte[] {};
		int off = 1;
		int len = 1;

		int result = fixture.read(b, off, len);

		// add additional test code here
		assertEquals(-1, result);
	}

	/**
	 * Run the long skip(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@Test
	public void testSkip_1()
		throws Exception {
		ByteBufferInputStream fixture = new ByteBufferInputStream(ByteBuffer.allocate(0));
		long n = 1L;

		long result = fixture.skip(n);

		// add additional test code here
		assertEquals(0L, result);
	}

	/**
	 * Run the long skip(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@Test
	public void testSkip_2()
		throws Exception {
		ByteBufferInputStream fixture = new ByteBufferInputStream(ByteBuffer.allocate(0));
		long n = 1L;

		long result = fixture.skip(n);

		// add additional test code here
		assertEquals(0L, result);
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 8:17 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(ByteBufferInputStreamTest.class);
	}
}