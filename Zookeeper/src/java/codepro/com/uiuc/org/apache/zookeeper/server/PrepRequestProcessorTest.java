package com.uiuc.org.apache.zookeeper.server;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.apache.jute.Record;
import org.apache.zookeeper.MultiTransactionRecord;
import org.apache.zookeeper.data.ACL;
import org.apache.zookeeper.data.Id;
import org.apache.zookeeper.proto.CheckVersionRequest;
import org.apache.zookeeper.proto.CreateRequest;
import org.apache.zookeeper.proto.DeleteRequest;
import org.apache.zookeeper.proto.SetACLRequest;
import org.apache.zookeeper.proto.SetDataRequest;
import org.apache.zookeeper.server.FinalRequestProcessor;
import org.apache.zookeeper.server.NIOServerCnxn;
import org.apache.zookeeper.server.NIOServerCnxnFactory;
import org.apache.zookeeper.server.PrepRequestProcessor;
import org.apache.zookeeper.server.Request;
import org.apache.zookeeper.server.RequestProcessor;
import org.apache.zookeeper.server.ZooKeeperServer;
import org.apache.zookeeper.txn.TxnHeader;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The class <code>PrepRequestProcessorTest</code> contains tests for the class <code>{@link PrepRequestProcessor}</code>.
 *
 * @generatedBy CodePro at 10/16/13 8:24 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class PrepRequestProcessorTest {
	/**
	 * Run the PrepRequestProcessor(ZooKeeperServer,RequestProcessor) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testPrepRequestProcessor_1()
		throws Exception {
		ZooKeeperServer zks = new ZooKeeperServer();
		RequestProcessor nextProcessor = new FinalRequestProcessor(new ZooKeeperServer());

		PrepRequestProcessor result = new PrepRequestProcessor(zks, nextProcessor);

		// add additional test code here
		assertNotNull(result);
		assertEquals("Thread[ProcessThread(sid:0 cport:-1):,1,main]", result.toString());
		assertEquals(false, result.isInterrupted());
		assertEquals("ProcessThread(sid:0 cport:-1):", result.getName());
		assertEquals(0, result.countStackFrames());
		assertEquals(9412L, result.getId());
		assertEquals(1, result.getPriority());
		assertEquals(false, result.isAlive());
		assertEquals(true, result.isDaemon());
	}

	/**
	 * Run the void addChangeRecord(ChangeRecord) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testAddChangeRecord_1()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));

		fixture.addChangeRecord(null);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.PrepRequestProcessor.addChangeRecord(PrepRequestProcessor.java:182)
	}

	/**
	 * Run the void checkACL(ZooKeeperServer,List<ACL>,int,List<Id>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testCheckACL_1()
		throws Exception {
		ZooKeeperServer zks = new ZooKeeperServer();
		List<ACL> acl = new LinkedList();
		int perm = 1;
		List<Id> ids = new LinkedList();

		PrepRequestProcessor.checkACL(zks, acl, perm, ids);

		// add additional test code here
	}

	/**
	 * Run the void checkACL(ZooKeeperServer,List<ACL>,int,List<Id>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testCheckACL_2()
		throws Exception {
		ZooKeeperServer zks = new ZooKeeperServer();
		List<ACL> acl = null;
		int perm = 1;
		List<Id> ids = new LinkedList();

		PrepRequestProcessor.checkACL(zks, acl, perm, ids);

		// add additional test code here
	}

	/**
	 * Run the void checkACL(ZooKeeperServer,List<ACL>,int,List<Id>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testCheckACL_3()
		throws Exception {
		ZooKeeperServer zks = new ZooKeeperServer();
		List<ACL> acl = new LinkedList();
		int perm = 1;
		List<Id> ids = new LinkedList();

		PrepRequestProcessor.checkACL(zks, acl, perm, ids);

		// add additional test code here
	}

	/**
	 * Run the void checkACL(ZooKeeperServer,List<ACL>,int,List<Id>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testCheckACL_4()
		throws Exception {
		ZooKeeperServer zks = new ZooKeeperServer();
		List<ACL> acl = new LinkedList();
		int perm = 1;
		List<Id> ids = new LinkedList();

		PrepRequestProcessor.checkACL(zks, acl, perm, ids);

		// add additional test code here
	}

	/**
	 * Run the void checkACL(ZooKeeperServer,List<ACL>,int,List<Id>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testCheckACL_5()
		throws Exception {
		ZooKeeperServer zks = new ZooKeeperServer();
		List<ACL> acl = new LinkedList();
		int perm = 1;
		List<Id> ids = new LinkedList();

		PrepRequestProcessor.checkACL(zks, acl, perm, ids);

		// add additional test code here
	}

	/**
	 * Run the void checkACL(ZooKeeperServer,List<ACL>,int,List<Id>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testCheckACL_6()
		throws Exception {
		ZooKeeperServer zks = new ZooKeeperServer();
		List<ACL> acl = new LinkedList();
		int perm = 1;
		List<Id> ids = new LinkedList();

		PrepRequestProcessor.checkACL(zks, acl, perm, ids);

		// add additional test code here
	}

	/**
	 * Run the void checkACL(ZooKeeperServer,List<ACL>,int,List<Id>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testCheckACL_7()
		throws Exception {
		ZooKeeperServer zks = new ZooKeeperServer();
		List<ACL> acl = new LinkedList();
		int perm = 1;
		List<Id> ids = new LinkedList();

		PrepRequestProcessor.checkACL(zks, acl, perm, ids);

		// add additional test code here
	}

	/**
	 * Run the void checkACL(ZooKeeperServer,List<ACL>,int,List<Id>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testCheckACL_8()
		throws Exception {
		ZooKeeperServer zks = new ZooKeeperServer();
		List<ACL> acl = new LinkedList();
		int perm = 1;
		List<Id> ids = new LinkedList();

		PrepRequestProcessor.checkACL(zks, acl, perm, ids);

		// add additional test code here
	}

	/**
	 * Run the void checkACL(ZooKeeperServer,List<ACL>,int,List<Id>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testCheckACL_9()
		throws Exception {
		ZooKeeperServer zks = new ZooKeeperServer();
		List<ACL> acl = new LinkedList();
		int perm = 1;
		List<Id> ids = new LinkedList();

		PrepRequestProcessor.checkACL(zks, acl, perm, ids);

		// add additional test code here
	}

	/**
	 * Run the void checkACL(ZooKeeperServer,List<ACL>,int,List<Id>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testCheckACL_10()
		throws Exception {
		ZooKeeperServer zks = new ZooKeeperServer();
		List<ACL> acl = new LinkedList();
		int perm = 1;
		List<Id> ids = new LinkedList();

		PrepRequestProcessor.checkACL(zks, acl, perm, ids);

		// add additional test code here
	}

	/**
	 * Run the void checkACL(ZooKeeperServer,List<ACL>,int,List<Id>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testCheckACL_11()
		throws Exception {
		ZooKeeperServer zks = new ZooKeeperServer();
		List<ACL> acl = new LinkedList();
		int perm = 1;
		List<Id> ids = new LinkedList();

		PrepRequestProcessor.checkACL(zks, acl, perm, ids);

		// add additional test code here
	}

	/**
	 * Run the void checkACL(ZooKeeperServer,List<ACL>,int,List<Id>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testCheckACL_12()
		throws Exception {
		ZooKeeperServer zks = new ZooKeeperServer();
		List<ACL> acl = new LinkedList();
		int perm = 1;
		List<Id> ids = new LinkedList();

		PrepRequestProcessor.checkACL(zks, acl, perm, ids);

		// add additional test code here
	}

	/**
	 * Run the HashMap<String, org.apache.zookeeper.server.ZooKeeperServer.ChangeRecord> getPendingChanges(MultiTransactionRecord) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testGetPendingChanges_1()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		MultiTransactionRecord multiRequest = new MultiTransactionRecord();

		HashMap<String, org.apache.zookeeper.server.ZooKeeperServer.ChangeRecord> result = fixture.getPendingChanges(multiRequest);

		// add additional test code here
		assertNotNull(result);
		assertEquals(0, result.size());
	}

	/**
	 * Run the HashMap<String, org.apache.zookeeper.server.ZooKeeperServer.ChangeRecord> getPendingChanges(MultiTransactionRecord) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testGetPendingChanges_2()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		MultiTransactionRecord multiRequest = new MultiTransactionRecord();

		HashMap<String, org.apache.zookeeper.server.ZooKeeperServer.ChangeRecord> result = fixture.getPendingChanges(multiRequest);

		// add additional test code here
		assertNotNull(result);
		assertEquals(0, result.size());
	}

	/**
	 * Run the HashMap<String, org.apache.zookeeper.server.ZooKeeperServer.ChangeRecord> getPendingChanges(MultiTransactionRecord) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testGetPendingChanges_3()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		MultiTransactionRecord multiRequest = new MultiTransactionRecord();

		HashMap<String, org.apache.zookeeper.server.ZooKeeperServer.ChangeRecord> result = fixture.getPendingChanges(multiRequest);

		// add additional test code here
		assertNotNull(result);
		assertEquals(0, result.size());
	}

	/**
	 * Run the HashMap<String, org.apache.zookeeper.server.ZooKeeperServer.ChangeRecord> getPendingChanges(MultiTransactionRecord) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testGetPendingChanges_4()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		MultiTransactionRecord multiRequest = new MultiTransactionRecord();

		HashMap<String, org.apache.zookeeper.server.ZooKeeperServer.ChangeRecord> result = fixture.getPendingChanges(multiRequest);

		// add additional test code here
		assertNotNull(result);
		assertEquals(0, result.size());
	}

	/**
	 * Run the org.apache.zookeeper.server.ZooKeeperServer.ChangeRecord getRecordForPath(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testGetRecordForPath_1()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		String path = "";

		org.apache.zookeeper.server.ZooKeeperServer.ChangeRecord result = fixture.getRecordForPath(path);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.PrepRequestProcessor.getRecordForPath(PrepRequestProcessor.java:159)
		assertNotNull(result);
	}

	/**
	 * Run the org.apache.zookeeper.server.ZooKeeperServer.ChangeRecord getRecordForPath(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testGetRecordForPath_2()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		String path = "";

		org.apache.zookeeper.server.ZooKeeperServer.ChangeRecord result = fixture.getRecordForPath(path);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.PrepRequestProcessor.getRecordForPath(PrepRequestProcessor.java:159)
		assertNotNull(result);
	}

	/**
	 * Run the org.apache.zookeeper.server.ZooKeeperServer.ChangeRecord getRecordForPath(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testGetRecordForPath_3()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		String path = "";

		org.apache.zookeeper.server.ZooKeeperServer.ChangeRecord result = fixture.getRecordForPath(path);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.PrepRequestProcessor.getRecordForPath(PrepRequestProcessor.java:159)
		assertNotNull(result);
	}

	/**
	 * Run the org.apache.zookeeper.server.ZooKeeperServer.ChangeRecord getRecordForPath(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testGetRecordForPath_4()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		String path = "";

		org.apache.zookeeper.server.ZooKeeperServer.ChangeRecord result = fixture.getRecordForPath(path);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.PrepRequestProcessor.getRecordForPath(PrepRequestProcessor.java:159)
		assertNotNull(result);
	}

	/**
	 * Run the void pRequest(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testPRequest_1()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 14, ByteBuffer.allocate(0), new LinkedList());
		request.zxid = 1L;
		request.txn = new ACL();
		request.hdr = new TxnHeader();

		fixture.pRequest(request);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void pRequest(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testPRequest_2()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 14, ByteBuffer.allocate(0), new LinkedList());
		request.zxid = 1L;
		request.txn = new ACL();
		request.hdr = new TxnHeader();

		fixture.pRequest(request);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void pRequest(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testPRequest_3()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 5, ByteBuffer.allocate(0), new LinkedList());
		request.zxid = 1L;
		request.txn = new ACL();
		request.hdr = new TxnHeader();

		fixture.pRequest(request);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void pRequest(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testPRequest_4()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 9, ByteBuffer.allocate(0), new LinkedList());
		request.setOwner(new Object());
		request.zxid = 1L;
		request.txn = new ACL();
		request.hdr = new TxnHeader();

		fixture.pRequest(request);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void pRequest(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testPRequest_5()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 3, ByteBuffer.allocate(0), new LinkedList());
		request.setOwner(new Object());
		request.zxid = 1L;
		request.txn = new ACL();
		request.hdr = new TxnHeader();

		fixture.pRequest(request);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void pRequest(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testPRequest_6()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 7, ByteBuffer.allocate(0), new LinkedList());
		request.zxid = 1L;
		request.txn = new ACL();
		request.hdr = new TxnHeader();

		fixture.pRequest(request);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void pRequest(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testPRequest_7()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());
		request.zxid = 1L;
		request.txn = new ACL();
		request.hdr = new TxnHeader();

		fixture.pRequest(request);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void pRequest(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testPRequest_8()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());
		request.zxid = 1L;
		request.txn = new ACL();
		request.hdr = new TxnHeader();

		fixture.pRequest(request);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void pRequest(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testPRequest_9()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 2, ByteBuffer.allocate(0), new LinkedList());
		request.zxid = 1L;
		request.txn = new ACL();
		request.hdr = new TxnHeader();

		fixture.pRequest(request);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void pRequest(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testPRequest_10()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 13, ByteBuffer.allocate(0), new LinkedList());
		request.zxid = 1L;
		request.txn = new ACL();
		request.hdr = new TxnHeader();

		fixture.pRequest(request);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void pRequest(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testPRequest_11()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 14, ByteBuffer.allocate(0), new LinkedList());
		request.zxid = 1L;
		request.txn = new ACL();
		request.hdr = new TxnHeader();

		fixture.pRequest(request);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void pRequest(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testPRequest_12()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 14, ByteBuffer.allocate(0), new LinkedList());
		request.zxid = 1L;
		request.txn = new ACL();
		request.hdr = new TxnHeader();

		fixture.pRequest(request);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void pRequest(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testPRequest_13()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 14, ByteBuffer.allocate(0), new LinkedList());
		request.zxid = 1L;
		request.txn = new ACL();
		request.hdr = new TxnHeader();

		fixture.pRequest(request);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void pRequest(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testPRequest_14()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, -10, ByteBuffer.allocate(0), new LinkedList());
		request.zxid = 1L;
		request.txn = new ACL();
		request.hdr = new TxnHeader();

		fixture.pRequest(request);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void pRequest(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testPRequest_15()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, -11, ByteBuffer.allocate(0), new LinkedList());
		request.zxid = 1L;
		request.txn = new ACL();
		request.hdr = new TxnHeader();

		fixture.pRequest(request);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void pRequest2Txn(int,long,Request,Record,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testPRequest2Txn_1()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		int type = 2;
		long zxid = 1L;
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());
		request.setOwner(new Object());
		request.hdr = new TxnHeader();
		Record record = new DeleteRequest("", -1);
		boolean deserialize = true;

		fixture.pRequest2Txn(type, zxid, request, record, deserialize);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void pRequest2Txn(int,long,Request,Record,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testPRequest2Txn_2()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		int type = 7;
		long zxid = 1L;
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());
		request.setOwner(new Object());
		request.hdr = new TxnHeader();
		Record record = new SetACLRequest("", new LinkedList(), 1);
		boolean deserialize = true;

		fixture.pRequest2Txn(type, zxid, request, record, deserialize);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void pRequest2Txn(int,long,Request,Record,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testPRequest2Txn_3()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		int type = 1;
		long zxid = 1L;
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());
		request.setOwner(new Object());
		request.txn = new ACL();
		request.hdr = new TxnHeader();
		Record record = new CreateRequest("", new byte[] {}, new LinkedList(), 1);
		boolean deserialize = true;

		fixture.pRequest2Txn(type, zxid, request, record, deserialize);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void pRequest2Txn(int,long,Request,Record,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testPRequest2Txn_4()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		int type = 1;
		long zxid = 1L;
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());
		request.setOwner(new Object());
		request.txn = new ACL();
		request.hdr = new TxnHeader();
		Record record = new CreateRequest("", new byte[] {}, new LinkedList(), 1);
		boolean deserialize = false;

		fixture.pRequest2Txn(type, zxid, request, record, deserialize);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void pRequest2Txn(int,long,Request,Record,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testPRequest2Txn_5()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		int type = 2;
		long zxid = 1L;
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());
		request.setOwner(new Object());
		request.txn = new ACL();
		request.hdr = new TxnHeader();
		Record record = new DeleteRequest("", 1);
		boolean deserialize = false;

		fixture.pRequest2Txn(type, zxid, request, record, deserialize);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void pRequest2Txn(int,long,Request,Record,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testPRequest2Txn_6()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		int type = 5;
		long zxid = 1L;
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());
		request.setOwner(new Object());
		request.txn = new ACL();
		request.hdr = new TxnHeader();
		Record record = new SetDataRequest("", new byte[] {}, -1);
		boolean deserialize = false;

		fixture.pRequest2Txn(type, zxid, request, record, deserialize);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void pRequest2Txn(int,long,Request,Record,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testPRequest2Txn_7()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		int type = 5;
		long zxid = 1L;
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());
		request.setOwner(new Object());
		request.txn = new ACL();
		request.hdr = new TxnHeader();
		Record record = new SetDataRequest("", new byte[] {}, 1);
		boolean deserialize = true;

		fixture.pRequest2Txn(type, zxid, request, record, deserialize);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void pRequest2Txn(int,long,Request,Record,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testPRequest2Txn_8()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		int type = 7;
		long zxid = 1L;
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());
		request.setOwner(new Object());
		request.txn = new ACL();
		request.hdr = new TxnHeader();
		Record record = new SetACLRequest("", new LinkedList(), -1);
		boolean deserialize = false;

		fixture.pRequest2Txn(type, zxid, request, record, deserialize);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void pRequest2Txn(int,long,Request,Record,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testPRequest2Txn_9()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		int type = 7;
		long zxid = 1L;
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());
		request.setOwner(new Object());
		request.txn = new ACL();
		request.hdr = new TxnHeader();
		Record record = new SetACLRequest("", new LinkedList(), 1);
		boolean deserialize = false;

		fixture.pRequest2Txn(type, zxid, request, record, deserialize);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void pRequest2Txn(int,long,Request,Record,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testPRequest2Txn_10()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		int type = -10;
		long zxid = 1L;
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());
		request.setOwner(new Object());
		request.txn = new ACL();
		request.hdr = new TxnHeader();
		Record record = new ACL();
		boolean deserialize = true;

		fixture.pRequest2Txn(type, zxid, request, record, deserialize);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void pRequest2Txn(int,long,Request,Record,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testPRequest2Txn_11()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		int type = -11;
		long zxid = 1L;
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());
		request.hdr = new TxnHeader();
		Record record = new ACL();
		boolean deserialize = true;

		fixture.pRequest2Txn(type, zxid, request, record, deserialize);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void pRequest2Txn(int,long,Request,Record,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testPRequest2Txn_12()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		int type = -11;
		long zxid = 1L;
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());
		request.hdr = new TxnHeader();
		Record record = new ACL();
		boolean deserialize = true;

		fixture.pRequest2Txn(type, zxid, request, record, deserialize);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void pRequest2Txn(int,long,Request,Record,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testPRequest2Txn_13()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		int type = -11;
		long zxid = 1L;
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());
		request.hdr = new TxnHeader();
		Record record = new ACL();
		boolean deserialize = true;

		fixture.pRequest2Txn(type, zxid, request, record, deserialize);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void pRequest2Txn(int,long,Request,Record,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testPRequest2Txn_14()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		int type = -11;
		long zxid = 1L;
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());
		request.hdr = new TxnHeader();
		Record record = new ACL();
		boolean deserialize = true;

		fixture.pRequest2Txn(type, zxid, request, record, deserialize);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void pRequest2Txn(int,long,Request,Record,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testPRequest2Txn_15()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		int type = 13;
		long zxid = 1L;
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());
		request.setOwner(new Object());
		request.txn = new ACL();
		request.hdr = new TxnHeader();
		Record record = new CheckVersionRequest("", -1);
		boolean deserialize = false;

		fixture.pRequest2Txn(type, zxid, request, record, deserialize);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void pRequest2Txn(int,long,Request,Record,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testPRequest2Txn_16()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		int type = 13;
		long zxid = 1L;
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());
		request.setOwner(new Object());
		request.txn = new ACL();
		request.hdr = new TxnHeader();
		Record record = new CheckVersionRequest("", 1);
		boolean deserialize = true;

		fixture.pRequest2Txn(type, zxid, request, record, deserialize);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void processRequest(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testProcessRequest_1()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());

		fixture.processRequest(request);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void rollbackPendingChanges(long,HashMap<String,ChangeRecord>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testRollbackPendingChanges_1()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		long zxid = 1L;
		HashMap<String, org.apache.zookeeper.server.ZooKeeperServer.ChangeRecord> pendingChangeRecords = new HashMap();

		fixture.rollbackPendingChanges(zxid, pendingChangeRecords);

		// add additional test code here
	}

	/**
	 * Run the void rollbackPendingChanges(long,HashMap<String,ChangeRecord>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testRollbackPendingChanges_2()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		long zxid = 1L;
		HashMap<String, org.apache.zookeeper.server.ZooKeeperServer.ChangeRecord> pendingChangeRecords = new HashMap();

		fixture.rollbackPendingChanges(zxid, pendingChangeRecords);

		// add additional test code here
	}

	/**
	 * Run the void rollbackPendingChanges(long,HashMap<String,ChangeRecord>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testRollbackPendingChanges_3()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		long zxid = 1L;
		HashMap<String, org.apache.zookeeper.server.ZooKeeperServer.ChangeRecord> pendingChangeRecords = new HashMap();

		fixture.rollbackPendingChanges(zxid, pendingChangeRecords);

		// add additional test code here
	}

	/**
	 * Run the void rollbackPendingChanges(long,HashMap<String,ChangeRecord>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testRollbackPendingChanges_4()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		long zxid = 1L;
		HashMap<String, org.apache.zookeeper.server.ZooKeeperServer.ChangeRecord> pendingChangeRecords = new HashMap();

		fixture.rollbackPendingChanges(zxid, pendingChangeRecords);

		// add additional test code here
	}

	/**
	 * Run the void rollbackPendingChanges(long,HashMap<String,ChangeRecord>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testRollbackPendingChanges_5()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		long zxid = 1L;
		HashMap<String, org.apache.zookeeper.server.ZooKeeperServer.ChangeRecord> pendingChangeRecords = new HashMap();

		fixture.rollbackPendingChanges(zxid, pendingChangeRecords);

		// add additional test code here
	}

	/**
	 * Run the void rollbackPendingChanges(long,HashMap<String,ChangeRecord>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testRollbackPendingChanges_6()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		long zxid = 1L;
		HashMap<String, org.apache.zookeeper.server.ZooKeeperServer.ChangeRecord> pendingChangeRecords = new HashMap();

		fixture.rollbackPendingChanges(zxid, pendingChangeRecords);

		// add additional test code here
	}

	/**
	 * Run the void rollbackPendingChanges(long,HashMap<String,ChangeRecord>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testRollbackPendingChanges_7()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		long zxid = 1L;
		HashMap<String, org.apache.zookeeper.server.ZooKeeperServer.ChangeRecord> pendingChangeRecords = new HashMap();

		fixture.rollbackPendingChanges(zxid, pendingChangeRecords);

		// add additional test code here
	}

	/**
	 * Run the void rollbackPendingChanges(long,HashMap<String,ChangeRecord>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testRollbackPendingChanges_8()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		long zxid = -1L;
		HashMap<String, org.apache.zookeeper.server.ZooKeeperServer.ChangeRecord> pendingChangeRecords = new HashMap();

		fixture.rollbackPendingChanges(zxid, pendingChangeRecords);

		// add additional test code here
	}

	/**
	 * Run the void rollbackPendingChanges(long,HashMap<String,ChangeRecord>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testRollbackPendingChanges_9()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		long zxid = 1L;
		HashMap<String, org.apache.zookeeper.server.ZooKeeperServer.ChangeRecord> pendingChangeRecords = new HashMap();

		fixture.rollbackPendingChanges(zxid, pendingChangeRecords);

		// add additional test code here
	}

	/**
	 * Run the void rollbackPendingChanges(long,HashMap<String,ChangeRecord>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testRollbackPendingChanges_10()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		long zxid = 1L;
		HashMap<String, org.apache.zookeeper.server.ZooKeeperServer.ChangeRecord> pendingChangeRecords = new HashMap();

		fixture.rollbackPendingChanges(zxid, pendingChangeRecords);

		// add additional test code here
	}

	/**
	 * Run the void rollbackPendingChanges(long,HashMap<String,ChangeRecord>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testRollbackPendingChanges_11()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		long zxid = 1L;
		HashMap<String, org.apache.zookeeper.server.ZooKeeperServer.ChangeRecord> pendingChangeRecords = new HashMap();

		fixture.rollbackPendingChanges(zxid, pendingChangeRecords);

		// add additional test code here
	}

	/**
	 * Run the void rollbackPendingChanges(long,HashMap<String,ChangeRecord>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testRollbackPendingChanges_12()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		long zxid = 1L;
		HashMap<String, org.apache.zookeeper.server.ZooKeeperServer.ChangeRecord> pendingChangeRecords = new HashMap();

		fixture.rollbackPendingChanges(zxid, pendingChangeRecords);

		// add additional test code here
	}

	/**
	 * Run the void rollbackPendingChanges(long,HashMap<String,ChangeRecord>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testRollbackPendingChanges_13()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		long zxid = 1L;
		HashMap<String, org.apache.zookeeper.server.ZooKeeperServer.ChangeRecord> pendingChangeRecords = new HashMap();

		fixture.rollbackPendingChanges(zxid, pendingChangeRecords);

		// add additional test code here
	}

	/**
	 * Run the void rollbackPendingChanges(long,HashMap<String,ChangeRecord>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testRollbackPendingChanges_14()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		long zxid = 1L;
		HashMap<String, org.apache.zookeeper.server.ZooKeeperServer.ChangeRecord> pendingChangeRecords = new HashMap();

		fixture.rollbackPendingChanges(zxid, pendingChangeRecords);

		// add additional test code here
	}

	/**
	 * Run the void rollbackPendingChanges(long,HashMap<String,ChangeRecord>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testRollbackPendingChanges_15()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		long zxid = 1L;
		HashMap<String, org.apache.zookeeper.server.ZooKeeperServer.ChangeRecord> pendingChangeRecords = new HashMap();

		fixture.rollbackPendingChanges(zxid, pendingChangeRecords);

		// add additional test code here
	}

	/**
	 * Run the void rollbackPendingChanges(long,HashMap<String,ChangeRecord>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testRollbackPendingChanges_16()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		long zxid = 1L;
		HashMap<String, org.apache.zookeeper.server.ZooKeeperServer.ChangeRecord> pendingChangeRecords = new HashMap();

		fixture.rollbackPendingChanges(zxid, pendingChangeRecords);

		// add additional test code here
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testRun_1()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));

		fixture.run();

		// add additional test code here
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testRun_2()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));

		fixture.run();

		// add additional test code here
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testRun_3()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));

		fixture.run();

		// add additional test code here
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testRun_4()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));

		fixture.run();

		// add additional test code here
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testRun_5()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));

		fixture.run();

		// add additional test code here
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testRun_6()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));

		fixture.run();

		// add additional test code here
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testRun_7()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));

		fixture.run();

		// add additional test code here
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testRun_8()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));

		fixture.run();

		// add additional test code here
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testRun_9()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));

		fixture.run();

		// add additional test code here
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testRun_10()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));

		fixture.run();

		// add additional test code here
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testRun_11()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));

		fixture.run();

		// add additional test code here
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testRun_12()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));

		fixture.run();

		// add additional test code here
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testRun_13()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));

		fixture.run();

		// add additional test code here
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testRun_14()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));

		fixture.run();

		// add additional test code here
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testRun_15()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));

		fixture.run();

		// add additional test code here
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testRun_16()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));

		fixture.run();

		// add additional test code here
	}

	/**
	 * Run the void setFailCreate(boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testSetFailCreate_1()
		throws Exception {
		boolean b = true;

		PrepRequestProcessor.setFailCreate(b);

		// add additional test code here
	}

	/**
	 * Run the void shutdown() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test
	public void testShutdown_1()
		throws Exception {
		PrepRequestProcessor fixture = new PrepRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));

		fixture.shutdown();

		// add additional test code here
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(PrepRequestProcessorTest.class);
	}
}