package com.uiuc.org.apache.zookeeper.server.persistence;

import java.io.ByteArrayOutputStream;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.net.URI;
import java.util.List;
import java.util.Properties;
import org.apache.jute.BinaryInputArchive;
import org.apache.jute.BinaryOutputArchive;
import org.apache.jute.InputArchive;
import org.apache.jute.OutputArchive;
import org.apache.jute.Record;
import org.apache.zookeeper.data.ACL;
import org.apache.zookeeper.server.persistence.Util;
import org.apache.zookeeper.txn.TxnHeader;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>UtilTest</code> contains tests for the class <code>{@link Util}</code>.
 *
 * @generatedBy CodePro at 10/16/13 7:35 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class UtilTest {
	/**
	 * Run the Util() constructor test.
	 *
	 * @generatedBy CodePro at 10/16/13 7:35 PM
	 */
	@Test
	public void testUtil_1()
		throws Exception {
		Util result = new Util();
		assertNotNull(result);
		// add additional test code here
	}

	/**
	 * Run the String getFormatConversionPolicy(Properties) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:35 PM
	 */
	@Test
	public void testGetFormatConversionPolicy_1()
		throws Exception {
		Properties props = new Properties();

		String result = Util.getFormatConversionPolicy(props);

		// add additional test code here
		assertEquals(null, result);
	}

	/**
	 * Run the File getLogDir(Properties) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:35 PM
	 */
	@Test
	public void testGetLogDir_1()
		throws Exception {
		Properties props = new Properties();

		File result = Util.getLogDir(props);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.io.File.<init>(File.java:222)
		//       at org.apache.zookeeper.server.persistence.Util.getLogDir(Util.java:118)
		assertNotNull(result);
	}

	/**
	 * Run the File getSnapDir(Properties) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:35 PM
	 */
	@Test
	public void testGetSnapDir_1()
		throws Exception {
		Properties props = new Properties();

		File result = Util.getSnapDir(props);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at java.io.File.<init>(File.java:222)
		//       at org.apache.zookeeper.server.persistence.Util.getSnapDir(Util.java:108)
		assertNotNull(result);
	}

	/**
	 * Run the long getZxidFromName(String,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:35 PM
	 */
	@Test
	public void testGetZxidFromName_1()
		throws Exception {
		String name = "";
		String prefix = "";

		long result = Util.getZxidFromName(name, prefix);

		// add additional test code here
		assertEquals(-1L, result);
	}

	/**
	 * Run the long getZxidFromName(String,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:35 PM
	 */
	@Test
	public void testGetZxidFromName_2()
		throws Exception {
		String name = "";
		String prefix = "";

		long result = Util.getZxidFromName(name, prefix);

		// add additional test code here
		assertEquals(-1L, result);
	}

	/**
	 * Run the long getZxidFromName(String,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:35 PM
	 */
	@Test
	public void testGetZxidFromName_3()
		throws Exception {
		String name = "";
		String prefix = "";

		long result = Util.getZxidFromName(name, prefix);

		// add additional test code here
		assertEquals(-1L, result);
	}

	/**
	 * Run the boolean isValidSnapshot(File) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:35 PM
	 */
	@Test
	public void testIsValidSnapshot_1()
		throws Exception {
		File f = null;

		boolean result = Util.isValidSnapshot(f);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean isValidSnapshot(File) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:35 PM
	 */
	@Test
	public void testIsValidSnapshot_2()
		throws Exception {
		File f = new File("");

		boolean result = Util.isValidSnapshot(f);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean isValidSnapshot(File) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:35 PM
	 */
	@Test
	public void testIsValidSnapshot_3()
		throws Exception {
		File f = new File("");

		boolean result = Util.isValidSnapshot(f);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean isValidSnapshot(File) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:35 PM
	 */
	@Test
	public void testIsValidSnapshot_4()
		throws Exception {
		File f = new File("");

		boolean result = Util.isValidSnapshot(f);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean isValidSnapshot(File) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:35 PM
	 */
	@Test
	public void testIsValidSnapshot_5()
		throws Exception {
		File f = new File("");

		boolean result = Util.isValidSnapshot(f);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean isValidSnapshot(File) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:35 PM
	 */
	@Test
	public void testIsValidSnapshot_6()
		throws Exception {
		File f = new File("");

		boolean result = Util.isValidSnapshot(f);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean isValidSnapshot(File) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:35 PM
	 */
	@Test
	public void testIsValidSnapshot_7()
		throws Exception {
		File f = new File("");

		boolean result = Util.isValidSnapshot(f);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean isValidSnapshot(File) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:35 PM
	 */
	@Test
	public void testIsValidSnapshot_8()
		throws Exception {
		File f = new File("");

		boolean result = Util.isValidSnapshot(f);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean isValidSnapshot(File) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:35 PM
	 */
	@Test
	public void testIsValidSnapshot_9()
		throws Exception {
		File f = new File("");

		boolean result = Util.isValidSnapshot(f);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean isValidSnapshot(File) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:35 PM
	 */
	@Test
	public void testIsValidSnapshot_10()
		throws Exception {
		File f = new File("");

		boolean result = Util.isValidSnapshot(f);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean isValidSnapshot(File) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:35 PM
	 */
	@Test
	public void testIsValidSnapshot_11()
		throws Exception {
		File f = new File("");

		boolean result = Util.isValidSnapshot(f);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean isValidSnapshot(File) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:35 PM
	 */
	@Test
	public void testIsValidSnapshot_12()
		throws Exception {
		File f = new File("");

		boolean result = Util.isValidSnapshot(f);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean isValidSnapshot(File) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:35 PM
	 */
	@Test
	public void testIsValidSnapshot_13()
		throws Exception {
		File f = new File("");

		boolean result = Util.isValidSnapshot(f);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean isValidSnapshot(File) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:35 PM
	 */
	@Test
	public void testIsValidSnapshot_14()
		throws Exception {
		File f = new File("");

		boolean result = Util.isValidSnapshot(f);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the URI makeFileLoggerURL(File,File) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:35 PM
	 */
	@Test
	public void testMakeFileLoggerURL_1()
		throws Exception {
		File dataDir = new File("");
		File dataLogDir = new File("");

		URI result = Util.makeFileLoggerURL(dataDir, dataLogDir);

		// add additional test code here
		assertNotNull(result);
		assertEquals("file:snapDir=;logDir=", result.toString());
		assertEquals(true, result.isAbsolute());
		assertEquals(null, result.getAuthority());
		assertEquals(null, result.getFragment());
		assertEquals(null, result.getPath());
		assertEquals(null, result.getQuery());
		assertEquals("file", result.getScheme());
		assertEquals(true, result.isOpaque());
		assertEquals(null, result.getHost());
		assertEquals(-1, result.getPort());
		assertEquals(null, result.getUserInfo());
		assertEquals("snapDir=;logDir=", result.getSchemeSpecificPart());
		assertEquals("file:snapDir=;logDir=", result.toASCIIString());
		assertEquals(null, result.getRawAuthority());
		assertEquals(null, result.getRawFragment());
		assertEquals(null, result.getRawPath());
		assertEquals(null, result.getRawQuery());
		assertEquals("snapDir=;logDir=", result.getRawSchemeSpecificPart());
		assertEquals(null, result.getRawUserInfo());
	}

	/**
	 * Run the URI makeFileLoggerURL(File,File,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:35 PM
	 */
	@Test
	public void testMakeFileLoggerURL_2()
		throws Exception {
		File dataDir = new File("");
		File dataLogDir = new File("");
		String convPolicy = "";

		URI result = Util.makeFileLoggerURL(dataDir, dataLogDir, convPolicy);

		// add additional test code here
		assertNotNull(result);
		assertEquals("file:snapDir=;logDir=;dbFormatConversion=", result.toString());
		assertEquals(true, result.isAbsolute());
		assertEquals(null, result.getAuthority());
		assertEquals(null, result.getFragment());
		assertEquals(null, result.getPath());
		assertEquals(null, result.getQuery());
		assertEquals("file", result.getScheme());
		assertEquals(true, result.isOpaque());
		assertEquals(null, result.getHost());
		assertEquals(-1, result.getPort());
		assertEquals(null, result.getUserInfo());
		assertEquals("snapDir=;logDir=;dbFormatConversion=", result.getSchemeSpecificPart());
		assertEquals("file:snapDir=;logDir=;dbFormatConversion=", result.toASCIIString());
		assertEquals(null, result.getRawAuthority());
		assertEquals(null, result.getRawFragment());
		assertEquals(null, result.getRawPath());
		assertEquals(null, result.getRawQuery());
		assertEquals("snapDir=;logDir=;dbFormatConversion=", result.getRawSchemeSpecificPart());
		assertEquals(null, result.getRawUserInfo());
	}

	/**
	 * Run the String makeLogName(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:35 PM
	 */
	@Test
	public void testMakeLogName_1()
		throws Exception {
		long zxid = 1L;

		String result = Util.makeLogName(zxid);

		// add additional test code here
		assertEquals("log.1", result);
	}

	/**
	 * Run the String makeSnapshotName(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:35 PM
	 */
	@Test
	public void testMakeSnapshotName_1()
		throws Exception {
		long zxid = 1L;

		String result = Util.makeSnapshotName(zxid);

		// add additional test code here
		assertEquals("snapshot.1", result);
	}

	/**
	 * Run the String makeURIString(String,String,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:35 PM
	 */
	@Test
	public void testMakeURIString_1()
		throws Exception {
		String dataDir = "";
		String dataLogDir = "";
		String convPolicy = "";

		String result = Util.makeURIString(dataDir, dataLogDir, convPolicy);

		// add additional test code here
		assertEquals("file:snapDir=;logDir=;dbFormatConversion=", result);
	}

	/**
	 * Run the String makeURIString(String,String,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:35 PM
	 */
	@Test
	public void testMakeURIString_2()
		throws Exception {
		String dataDir = "";
		String dataLogDir = "";
		String convPolicy = null;

		String result = Util.makeURIString(dataDir, dataLogDir, convPolicy);

		// add additional test code here
		assertEquals("file:snapDir=;logDir=", result);
	}

	/**
	 * Run the byte[] marshallTxnEntry(TxnHeader,Record) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:35 PM
	 */
	@Test
	public void testMarshallTxnEntry_1()
		throws Exception {
		TxnHeader hdr = new TxnHeader();
		Record txn = new ACL();

		byte[] result = Util.marshallTxnEntry(hdr, txn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.jute.BinaryOutputArchive.writeRecord(BinaryOutputArchive.java:123)
		//       at org.apache.zookeeper.data.ACL.serialize(ACL.java:49)
		//       at org.apache.zookeeper.server.persistence.Util.marshallTxnEntry(Util.java:263)
		assertNotNull(result);
	}

	/**
	 * Run the byte[] marshallTxnEntry(TxnHeader,Record) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:35 PM
	 */
	@Test
	public void testMarshallTxnEntry_2()
		throws Exception {
		TxnHeader hdr = new TxnHeader();
		Record txn = new ACL();

		byte[] result = Util.marshallTxnEntry(hdr, txn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.jute.BinaryOutputArchive.writeRecord(BinaryOutputArchive.java:123)
		//       at org.apache.zookeeper.data.ACL.serialize(ACL.java:49)
		//       at org.apache.zookeeper.server.persistence.Util.marshallTxnEntry(Util.java:263)
		assertNotNull(result);
	}

	/**
	 * Run the byte[] marshallTxnEntry(TxnHeader,Record) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:35 PM
	 */
	@Test
	public void testMarshallTxnEntry_3()
		throws Exception {
		TxnHeader hdr = new TxnHeader();
		Record txn = null;

		byte[] result = Util.marshallTxnEntry(hdr, txn);

		// add additional test code here
		assertNotNull(result);
		assertEquals(32, result.length);
		assertEquals((byte) 0, result[0]);
		assertEquals((byte) 0, result[1]);
		assertEquals((byte) 0, result[2]);
		assertEquals((byte) 0, result[3]);
		assertEquals((byte) 0, result[4]);
		assertEquals((byte) 0, result[5]);
		assertEquals((byte) 0, result[6]);
		assertEquals((byte) 0, result[7]);
		assertEquals((byte) 0, result[8]);
		assertEquals((byte) 0, result[9]);
		assertEquals((byte) 0, result[10]);
		assertEquals((byte) 0, result[11]);
		assertEquals((byte) 0, result[12]);
		assertEquals((byte) 0, result[13]);
		assertEquals((byte) 0, result[14]);
		assertEquals((byte) 0, result[15]);
		assertEquals((byte) 0, result[16]);
		assertEquals((byte) 0, result[17]);
		assertEquals((byte) 0, result[18]);
		assertEquals((byte) 0, result[19]);
		assertEquals((byte) 0, result[20]);
		assertEquals((byte) 0, result[21]);
		assertEquals((byte) 0, result[22]);
		assertEquals((byte) 0, result[23]);
		assertEquals((byte) 0, result[24]);
		assertEquals((byte) 0, result[25]);
		assertEquals((byte) 0, result[26]);
		assertEquals((byte) 0, result[27]);
		assertEquals((byte) 0, result[28]);
		assertEquals((byte) 0, result[29]);
		assertEquals((byte) 0, result[30]);
		assertEquals((byte) 0, result[31]);
	}

	/**
	 * Run the long padLogFile(FileOutputStream,long,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:35 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testPadLogFile_1()
		throws Exception {
		FileOutputStream f = new FileOutputStream(new FileDescriptor());
		long currentSize = 1L;
		long preAllocSize = 1L;

		long result = Util.padLogFile(f, currentSize, preAllocSize);

		// add additional test code here
		assertEquals(0L, result);
	}

	/**
	 * Run the long padLogFile(FileOutputStream,long,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:35 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testPadLogFile_2()
		throws Exception {
		FileOutputStream f = new FileOutputStream(new FileDescriptor());
		long currentSize = 1L;
		long preAllocSize = 1L;

		long result = Util.padLogFile(f, currentSize, preAllocSize);

		// add additional test code here
		assertEquals(0L, result);
	}

	/**
	 * Run the long padLogFile(FileOutputStream,long,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:35 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testPadLogFile_3()
		throws Exception {
		FileOutputStream f = new FileOutputStream(new FileDescriptor());
		long currentSize = 1L;
		long preAllocSize = 1L;

		long result = Util.padLogFile(f, currentSize, preAllocSize);

		// add additional test code here
		assertEquals(0L, result);
	}

	/**
	 * Run the byte[] readTxnBytes(InputArchive) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:35 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testReadTxnBytes_1()
		throws Exception {
		InputArchive ia = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));

		byte[] result = Util.readTxnBytes(ia);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the byte[] readTxnBytes(InputArchive) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:35 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testReadTxnBytes_2()
		throws Exception {
		InputArchive ia = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));

		byte[] result = Util.readTxnBytes(ia);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the byte[] readTxnBytes(InputArchive) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:35 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testReadTxnBytes_3()
		throws Exception {
		InputArchive ia = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));

		byte[] result = Util.readTxnBytes(ia);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the byte[] readTxnBytes(InputArchive) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:35 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testReadTxnBytes_4()
		throws Exception {
		InputArchive ia = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));

		byte[] result = Util.readTxnBytes(ia);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the byte[] readTxnBytes(InputArchive) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:35 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testReadTxnBytes_5()
		throws Exception {
		InputArchive ia = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));

		byte[] result = Util.readTxnBytes(ia);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the List<File> sortDataDir(File[],String,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:35 PM
	 */
	@Test
	public void testSortDataDir_1()
		throws Exception {
		File[] files = new File[] {};
		String prefix = "";
		boolean ascending = true;

		List<File> result = Util.sortDataDir(files, prefix, ascending);

		// add additional test code here
		assertNotNull(result);
		assertEquals(0, result.size());
	}

	/**
	 * Run the List<File> sortDataDir(File[],String,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:35 PM
	 */
	@Test
	public void testSortDataDir_2()
		throws Exception {
		File[] files = new File[] {};
		String prefix = "";
		boolean ascending = true;

		List<File> result = Util.sortDataDir(files, prefix, ascending);

		// add additional test code here
		assertNotNull(result);
		assertEquals(0, result.size());
	}

	/**
	 * Run the void writeTxnBytes(OutputArchive,byte[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:35 PM
	 */
	@Test
	public void testWriteTxnBytes_1()
		throws Exception {
		OutputArchive oa = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		byte[] bytes = new byte[] {};

		Util.writeTxnBytes(oa, bytes);

		// add additional test code here
	}

	/**
	 * Run the void writeTxnBytes(OutputArchive,byte[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:35 PM
	 */
	@Test
	public void testWriteTxnBytes_2()
		throws Exception {
		OutputArchive oa = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		byte[] bytes = new byte[] {};

		Util.writeTxnBytes(oa, bytes);

		// add additional test code here
	}

	/**
	 * Run the void writeTxnBytes(OutputArchive,byte[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:35 PM
	 */
	@Test
	public void testWriteTxnBytes_3()
		throws Exception {
		OutputArchive oa = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		byte[] bytes = new byte[] {};

		Util.writeTxnBytes(oa, bytes);

		// add additional test code here
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:35 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:35 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:35 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(UtilTest.class);
	}
}