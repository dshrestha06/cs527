package com.uiuc.org.apache.zookeeper.client;

import java.net.ConnectException;
import org.apache.zookeeper.client.FourLetterWordMain;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>FourLetterWordMainTest</code> contains tests for the class <code>{@link FourLetterWordMain}</code>.
 *
 * @generatedBy CodePro at 10/16/13 7:31 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class FourLetterWordMainTest {
	/**
	 * Run the FourLetterWordMain() constructor test.
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testFourLetterWordMain_1()
		throws Exception {
		FourLetterWordMain result = new FourLetterWordMain();
		assertNotNull(result);
		// add additional test code here
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testMain_1()
		throws Exception {
		String[] args = new String[] {};

		FourLetterWordMain.main(args);

		// add additional test code here
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test(expected = java.lang.NumberFormatException.class)
	public void testMain_2()
		throws Exception {
		String[] args = new String[] {"", "", null};

		FourLetterWordMain.main(args);

		// add additional test code here
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test(expected = java.net.ConnectException.class)
	public void testMain_3()
		throws Exception {
		String[] args = new String[] {"a", "0", "a"};

		FourLetterWordMain.main(args);

		// add additional test code here
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test(expected = java.net.ConnectException.class)
	public void testMain_4()
		throws Exception {
		String[] args = new String[] {"a", "0", "a"};

		FourLetterWordMain.main(args);

		// add additional test code here
	}

	/**
	 * Run the String send4LetterWord(String,int,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test(expected = java.net.ConnectException.class)
	public void testSend4LetterWord_1()
		throws Exception {
		String host = "";
		int port = 1;
		String cmd = "";

		String result = FourLetterWordMain.send4LetterWord(host, port, cmd);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the String send4LetterWord(String,int,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test(expected = java.net.ConnectException.class)
	public void testSend4LetterWord_2()
		throws Exception {
		String host = "";
		int port = 1;
		String cmd = "";

		String result = FourLetterWordMain.send4LetterWord(host, port, cmd);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the String send4LetterWord(String,int,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test(expected = java.net.ConnectException.class)
	public void testSend4LetterWord_3()
		throws Exception {
		String host = "";
		int port = 1;
		String cmd = "";

		String result = FourLetterWordMain.send4LetterWord(host, port, cmd);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the String send4LetterWord(String,int,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test(expected = java.net.ConnectException.class)
	public void testSend4LetterWord_4()
		throws Exception {
		String host = "";
		int port = 1;
		String cmd = "";

		String result = FourLetterWordMain.send4LetterWord(host, port, cmd);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the String send4LetterWord(String,int,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test(expected = java.net.ConnectException.class)
	public void testSend4LetterWord_5()
		throws Exception {
		String host = "";
		int port = 1;
		String cmd = "";

		String result = FourLetterWordMain.send4LetterWord(host, port, cmd);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the String send4LetterWord(String,int,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test(expected = java.net.ConnectException.class)
	public void testSend4LetterWord_6()
		throws Exception {
		String host = "";
		int port = 1;
		String cmd = "";

		String result = FourLetterWordMain.send4LetterWord(host, port, cmd);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the String send4LetterWord(String,int,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test(expected = java.net.ConnectException.class)
	public void testSend4LetterWord_7()
		throws Exception {
		String host = "";
		int port = 1;
		String cmd = "";

		String result = FourLetterWordMain.send4LetterWord(host, port, cmd);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the String send4LetterWord(String,int,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test(expected = java.net.ConnectException.class)
	public void testSend4LetterWord_8()
		throws Exception {
		String host = "";
		int port = 1;
		String cmd = "";

		String result = FourLetterWordMain.send4LetterWord(host, port, cmd);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the String send4LetterWord(String,int,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test(expected = java.net.ConnectException.class)
	public void testSend4LetterWord_9()
		throws Exception {
		String host = "";
		int port = 1;
		String cmd = "";

		String result = FourLetterWordMain.send4LetterWord(host, port, cmd);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the String send4LetterWord(String,int,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test(expected = java.net.ConnectException.class)
	public void testSend4LetterWord_10()
		throws Exception {
		String host = "";
		int port = 1;
		String cmd = "";

		String result = FourLetterWordMain.send4LetterWord(host, port, cmd);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the String send4LetterWord(String,int,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test(expected = java.net.ConnectException.class)
	public void testSend4LetterWord_11()
		throws Exception {
		String host = "";
		int port = 1;
		String cmd = "";

		String result = FourLetterWordMain.send4LetterWord(host, port, cmd);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(FourLetterWordMainTest.class);
	}
}