package com.uiuc.org.apache.zookeeper.server;

import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.LinkedList;
import java.util.List;
import org.apache.zookeeper.data.Id;
import org.apache.zookeeper.server.FinalRequestProcessor;
import org.apache.zookeeper.server.NIOServerCnxn;
import org.apache.zookeeper.server.NIOServerCnxnFactory;
import org.apache.zookeeper.server.Request;
import org.apache.zookeeper.server.RequestProcessor;
import org.apache.zookeeper.server.ServerCnxn;
import org.apache.zookeeper.server.SyncRequestProcessor;
import org.apache.zookeeper.server.ZooKeeperServer;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>SyncRequestProcessorTest</code> contains tests for the class <code>{@link SyncRequestProcessor}</code>.
 *
 * @generatedBy CodePro at 10/16/13 6:59 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class SyncRequestProcessorTest {
	/**
	 * Run the SyncRequestProcessor(ZooKeeperServer,RequestProcessor) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:59 PM
	 */
	@Test
	public void testSyncRequestProcessor_1()
		throws Exception {
		ZooKeeperServer zks = new ZooKeeperServer();
		RequestProcessor nextProcessor = new FinalRequestProcessor(new ZooKeeperServer());

		SyncRequestProcessor result = new SyncRequestProcessor(zks, nextProcessor);

		// add additional test code here
		assertNotNull(result);
		assertEquals("Thread[SyncThread:0,1,main]", result.toString());
		assertEquals(false, result.isInterrupted());
		assertEquals("SyncThread:0", result.getName());
		assertEquals(0, result.countStackFrames());
		assertEquals(4944L, result.getId());
		assertEquals(1, result.getPriority());
		assertEquals(false, result.isAlive());
		assertEquals(true, result.isDaemon());
	}

	/**
	 * Run the int getSnapCount() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:59 PM
	 */
	@Test
	public void testGetSnapCount_1()
		throws Exception {

		int result = SyncRequestProcessor.getSnapCount();

		// add additional test code here
		assertEquals(1, result);
	}

	/**
	 * Run the void processRequest(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:59 PM
	 */
	@Test
	public void testProcessRequest_1()
		throws Exception {
		SyncRequestProcessor fixture = new SyncRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		fixture.processRequest(new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList()));
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());

		fixture.processRequest(request);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:59 PM
	 */
	@Test
	public void testRun_1()
		throws Exception {
		SyncRequestProcessor fixture = new SyncRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		fixture.processRequest(new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList()));

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:59 PM
	 */
	@Test
	public void testRun_2()
		throws Exception {
		SyncRequestProcessor fixture = new SyncRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		fixture.processRequest(new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList()));

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:59 PM
	 */
	@Test
	public void testRun_3()
		throws Exception {
		SyncRequestProcessor fixture = new SyncRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		fixture.processRequest(new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList()));

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:59 PM
	 */
	@Test
	public void testRun_4()
		throws Exception {
		SyncRequestProcessor fixture = new SyncRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		fixture.processRequest(new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList()));

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:59 PM
	 */
	@Test
	public void testRun_5()
		throws Exception {
		SyncRequestProcessor fixture = new SyncRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		fixture.processRequest(new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList()));

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:59 PM
	 */
	@Test
	public void testRun_6()
		throws Exception {
		SyncRequestProcessor fixture = new SyncRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		fixture.processRequest(new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList()));

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:59 PM
	 */
	@Test
	public void testRun_7()
		throws Exception {
		SyncRequestProcessor fixture = new SyncRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		fixture.processRequest(new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList()));

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:59 PM
	 */
	@Test
	public void testRun_8()
		throws Exception {
		SyncRequestProcessor fixture = new SyncRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		fixture.processRequest(new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList()));

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:59 PM
	 */
	@Test
	public void testRun_9()
		throws Exception {
		SyncRequestProcessor fixture = new SyncRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		fixture.processRequest(new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList()));

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:59 PM
	 */
	@Test
	public void testRun_10()
		throws Exception {
		SyncRequestProcessor fixture = new SyncRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		fixture.processRequest(new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList()));

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:59 PM
	 */
	@Test
	public void testRun_11()
		throws Exception {
		SyncRequestProcessor fixture = new SyncRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		fixture.processRequest(new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList()));

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:59 PM
	 */
	@Test
	public void testRun_12()
		throws Exception {
		SyncRequestProcessor fixture = new SyncRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		fixture.processRequest(new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList()));

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:59 PM
	 */
	@Test
	public void testRun_13()
		throws Exception {
		SyncRequestProcessor fixture = new SyncRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		fixture.processRequest(new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList()));

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:59 PM
	 */
	@Test
	public void testRun_14()
		throws Exception {
		SyncRequestProcessor fixture = new SyncRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		fixture.processRequest(new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList()));

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:59 PM
	 */
	@Test
	public void testRun_15()
		throws Exception {
		SyncRequestProcessor fixture = new SyncRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		fixture.processRequest(new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList()));

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:59 PM
	 */
	@Test
	public void testRun_16()
		throws Exception {
		SyncRequestProcessor fixture = new SyncRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		fixture.processRequest(new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList()));

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void setSnapCount(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:59 PM
	 */
	@Test
	public void testSetSnapCount_1()
		throws Exception {
		int count = 1;

		SyncRequestProcessor.setSnapCount(count);

		// add additional test code here
	}

	/**
	 * Run the void shutdown() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:59 PM
	 */
	@Test
	public void testShutdown_1()
		throws Exception {
		SyncRequestProcessor fixture = new SyncRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		fixture.processRequest(new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList()));

		fixture.shutdown();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void shutdown() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:59 PM
	 */
	@Test
	public void testShutdown_2()
		throws Exception {
		SyncRequestProcessor fixture = new SyncRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		fixture.processRequest(new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList()));

		fixture.shutdown();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void shutdown() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:59 PM
	 */
	@Test
	public void testShutdown_3()
		throws Exception {
		SyncRequestProcessor fixture = new SyncRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		fixture.processRequest(new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList()));

		fixture.shutdown();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 6:59 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 6:59 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 6:59 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(SyncRequestProcessorTest.class);
	}
}