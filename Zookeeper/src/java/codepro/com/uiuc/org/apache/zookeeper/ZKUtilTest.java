package com.uiuc.org.apache.zookeeper;

import java.util.List;
import org.apache.zookeeper.AsyncCallback;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZKUtil;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.test.AsyncHammerTest;
import org.apache.zookeeper.test.system.SimpleClient;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>ZKUtilTest</code> contains tests for the class <code>{@link ZKUtil}</code>.
 *
 * @generatedBy CodePro at 10/16/13 7:29 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class ZKUtilTest {
	/**
	 * Run the ZKUtil() constructor test.
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testZKUtil_1()
		throws Exception {
		ZKUtil result = new ZKUtil();
		assertNotNull(result);
		// add additional test code here
	}

	/**
	 * Run the void deleteRecursive(ZooKeeper,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testDeleteRecursive_1()
		throws Exception {
		ZooKeeper zk = new ZooKeeper("", 1, new SimpleClient());
		String pathRoot = "";

		ZKUtil.deleteRecursive(zk, pathRoot);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZKUtil.deleteRecursive(ZKUtil.java:47)
	}

	/**
	 * Run the void deleteRecursive(ZooKeeper,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testDeleteRecursive_2()
		throws Exception {
		ZooKeeper zk = new ZooKeeper("", 1, new SimpleClient());
		String pathRoot = "";

		ZKUtil.deleteRecursive(zk, pathRoot);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZKUtil.deleteRecursive(ZKUtil.java:47)
	}

	/**
	 * Run the void deleteRecursive(ZooKeeper,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testDeleteRecursive_3()
		throws Exception {
		ZooKeeper zk = new ZooKeeper("", 1, new SimpleClient());
		String pathRoot = "";

		ZKUtil.deleteRecursive(zk, pathRoot);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZKUtil.deleteRecursive(ZKUtil.java:47)
	}

	/**
	 * Run the void deleteRecursive(ZooKeeper,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testDeleteRecursive_4()
		throws Exception {
		ZooKeeper zk = new ZooKeeper("", 1, new SimpleClient());
		String pathRoot = "";

		ZKUtil.deleteRecursive(zk, pathRoot);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZKUtil.deleteRecursive(ZKUtil.java:47)
	}

	/**
	 * Run the void deleteRecursive(ZooKeeper,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testDeleteRecursive_5()
		throws Exception {
		ZooKeeper zk = new ZooKeeper("", 1, new SimpleClient());
		String pathRoot = "";

		ZKUtil.deleteRecursive(zk, pathRoot);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZKUtil.deleteRecursive(ZKUtil.java:47)
	}

	/**
	 * Run the void deleteRecursive(ZooKeeper,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testDeleteRecursive_6()
		throws Exception {
		ZooKeeper zk = new ZooKeeper("", 1, new SimpleClient());
		String pathRoot = "";

		ZKUtil.deleteRecursive(zk, pathRoot);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZKUtil.deleteRecursive(ZKUtil.java:47)
	}

	/**
	 * Run the void deleteRecursive(ZooKeeper,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testDeleteRecursive_7()
		throws Exception {
		ZooKeeper zk = new ZooKeeper("", 1, new SimpleClient());
		String pathRoot = "";

		ZKUtil.deleteRecursive(zk, pathRoot);

		// add additional test code here
	}

	/**
	 * Run the void deleteRecursive(ZooKeeper,String,VoidCallback,Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testDeleteRecursive_8()
		throws Exception {
		ZooKeeper zk = new ZooKeeper("", 1, new SimpleClient());
		String pathRoot = "";
		org.apache.zookeeper.AsyncCallback.VoidCallback cb = new AsyncHammerTest();
		Object ctx = new Object();

		ZKUtil.deleteRecursive(zk, pathRoot, cb, ctx);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZKUtil.deleteRecursive(ZKUtil.java:78)
	}

	/**
	 * Run the void deleteRecursive(ZooKeeper,String,VoidCallback,Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testDeleteRecursive_9()
		throws Exception {
		ZooKeeper zk = new ZooKeeper("", 1, new SimpleClient());
		String pathRoot = "";
		org.apache.zookeeper.AsyncCallback.VoidCallback cb = new AsyncHammerTest();
		Object ctx = new Object();

		ZKUtil.deleteRecursive(zk, pathRoot, cb, ctx);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZKUtil.deleteRecursive(ZKUtil.java:78)
	}

	/**
	 * Run the void deleteRecursive(ZooKeeper,String,VoidCallback,Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testDeleteRecursive_10()
		throws Exception {
		ZooKeeper zk = new ZooKeeper("", 1, new SimpleClient());
		String pathRoot = "";
		org.apache.zookeeper.AsyncCallback.VoidCallback cb = new AsyncHammerTest();
		Object ctx = new Object();

		ZKUtil.deleteRecursive(zk, pathRoot, cb, ctx);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZKUtil.deleteRecursive(ZKUtil.java:78)
	}

	/**
	 * Run the void deleteRecursive(ZooKeeper,String,VoidCallback,Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testDeleteRecursive_11()
		throws Exception {
		ZooKeeper zk = new ZooKeeper("", 1, new SimpleClient());
		String pathRoot = "";
		org.apache.zookeeper.AsyncCallback.VoidCallback cb = new AsyncHammerTest();
		Object ctx = new Object();

		ZKUtil.deleteRecursive(zk, pathRoot, cb, ctx);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZKUtil.deleteRecursive(ZKUtil.java:78)
	}

	/**
	 * Run the void deleteRecursive(ZooKeeper,String,VoidCallback,Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testDeleteRecursive_12()
		throws Exception {
		ZooKeeper zk = new ZooKeeper("", 1, new SimpleClient());
		String pathRoot = "";
		org.apache.zookeeper.AsyncCallback.VoidCallback cb = new AsyncHammerTest();
		Object ctx = new Object();

		ZKUtil.deleteRecursive(zk, pathRoot, cb, ctx);

		// add additional test code here
	}

	/**
	 * Run the List<String> listSubTreeBFS(ZooKeeper,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testListSubTreeBFS_1()
		throws Exception {
		ZooKeeper zk = new ZooKeeper("", 1, new SimpleClient());
		String pathRoot = "";

		List<String> result = ZKUtil.listSubTreeBFS(zk, pathRoot);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.getChildren(ZooKeeper.java:1450)
		//       at org.apache.zookeeper.ZooKeeper.getChildren(ZooKeeper.java:1496)
		//       at org.apache.zookeeper.ZKUtil.listSubTreeBFS(ZKUtil.java:114)
		assertNotNull(result);
	}

	/**
	 * Run the List<String> listSubTreeBFS(ZooKeeper,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testListSubTreeBFS_2()
		throws Exception {
		ZooKeeper zk = new ZooKeeper("", 1, new SimpleClient());
		String pathRoot = "";

		List<String> result = ZKUtil.listSubTreeBFS(zk, pathRoot);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.getChildren(ZooKeeper.java:1450)
		//       at org.apache.zookeeper.ZooKeeper.getChildren(ZooKeeper.java:1496)
		//       at org.apache.zookeeper.ZKUtil.listSubTreeBFS(ZKUtil.java:114)
		assertNotNull(result);
	}

	/**
	 * Run the List<String> listSubTreeBFS(ZooKeeper,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testListSubTreeBFS_3()
		throws Exception {
		ZooKeeper zk = new ZooKeeper("", 1, new SimpleClient());
		String pathRoot = "";

		List<String> result = ZKUtil.listSubTreeBFS(zk, pathRoot);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.getChildren(ZooKeeper.java:1450)
		//       at org.apache.zookeeper.ZooKeeper.getChildren(ZooKeeper.java:1496)
		//       at org.apache.zookeeper.ZKUtil.listSubTreeBFS(ZKUtil.java:114)
		assertNotNull(result);
	}

	/**
	 * Run the List<String> listSubTreeBFS(ZooKeeper,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testListSubTreeBFS_4()
		throws Exception {
		ZooKeeper zk = new ZooKeeper("", 1, new SimpleClient());
		String pathRoot = "";

		List<String> result = ZKUtil.listSubTreeBFS(zk, pathRoot);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.getChildren(ZooKeeper.java:1450)
		//       at org.apache.zookeeper.ZooKeeper.getChildren(ZooKeeper.java:1496)
		//       at org.apache.zookeeper.ZKUtil.listSubTreeBFS(ZKUtil.java:114)
		assertNotNull(result);
	}

	/**
	 * Run the List<String> listSubTreeBFS(ZooKeeper,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testListSubTreeBFS_5()
		throws Exception {
		ZooKeeper zk = new ZooKeeper("", 1, new SimpleClient());
		String pathRoot = "";

		List<String> result = ZKUtil.listSubTreeBFS(zk, pathRoot);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.getChildren(ZooKeeper.java:1450)
		//       at org.apache.zookeeper.ZooKeeper.getChildren(ZooKeeper.java:1496)
		//       at org.apache.zookeeper.ZKUtil.listSubTreeBFS(ZKUtil.java:114)
		assertNotNull(result);
	}

	/**
	 * Run the List<String> listSubTreeBFS(ZooKeeper,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testListSubTreeBFS_6()
		throws Exception {
		ZooKeeper zk = new ZooKeeper("", 1, new SimpleClient());
		String pathRoot = "";

		List<String> result = ZKUtil.listSubTreeBFS(zk, pathRoot);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.getChildren(ZooKeeper.java:1450)
		//       at org.apache.zookeeper.ZooKeeper.getChildren(ZooKeeper.java:1496)
		//       at org.apache.zookeeper.ZKUtil.listSubTreeBFS(ZKUtil.java:114)
		assertNotNull(result);
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(ZKUtilTest.class);
	}
}