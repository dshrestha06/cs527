package com.uiuc.org.apache.zookeeper.server.auth;

import java.util.List;
import org.apache.zookeeper.server.auth.KerberosName;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>KerberosNameTest</code> contains tests for the class <code>{@link KerberosName}</code>.
 *
 * @generatedBy CodePro at 10/16/13 7:33 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class KerberosNameTest {
	/**
	 * Run the KerberosName(String) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testKerberosName_1()
		throws Exception {
		String name = "";

		KerberosName result = new KerberosName(name);

		// add additional test code here
		assertNotNull(result);
		assertEquals("", result.toString());
		assertEquals(null, result.getHostName());
		assertEquals("", result.getServiceName());
		assertEquals("", result.getShortName());
		assertEquals(null, result.getRealm());
		assertEquals("", result.getDefaultRealm());
	}

	/**
	 * Run the KerberosName(String) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testKerberosName_2()
		throws Exception {
		String name = "";

		KerberosName result = new KerberosName(name);

		// add additional test code here
		assertNotNull(result);
		assertEquals("", result.toString());
		assertEquals(null, result.getHostName());
		assertEquals("", result.getServiceName());
		assertEquals("", result.getShortName());
		assertEquals(null, result.getRealm());
		assertEquals("", result.getDefaultRealm());
	}

	/**
	 * Run the KerberosName(String) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testKerberosName_3()
		throws Exception {
		String name = "";

		KerberosName result = new KerberosName(name);

		// add additional test code here
		assertNotNull(result);
		assertEquals("", result.toString());
		assertEquals(null, result.getHostName());
		assertEquals("", result.getServiceName());
		assertEquals("", result.getShortName());
		assertEquals(null, result.getRealm());
		assertEquals("", result.getDefaultRealm());
	}

	/**
	 * Run the String getDefaultRealm() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testGetDefaultRealm_1()
		throws Exception {
		KerberosName fixture = new KerberosName("");

		String result = fixture.getDefaultRealm();

		// add additional test code here
		assertEquals("", result);
	}

	/**
	 * Run the String getHostName() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testGetHostName_1()
		throws Exception {
		KerberosName fixture = new KerberosName("");

		String result = fixture.getHostName();

		// add additional test code here
		assertEquals(null, result);
	}

	/**
	 * Run the String getRealm() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testGetRealm_1()
		throws Exception {
		KerberosName fixture = new KerberosName("");

		String result = fixture.getRealm();

		// add additional test code here
		assertEquals(null, result);
	}

	/**
	 * Run the String getServiceName() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testGetServiceName_1()
		throws Exception {
		KerberosName fixture = new KerberosName("");

		String result = fixture.getServiceName();

		// add additional test code here
		assertEquals("", result);
	}

	/**
	 * Run the String getShortName() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testGetShortName_1()
		throws Exception {
		KerberosName fixture = new KerberosName("");

		String result = fixture.getShortName();

		// add additional test code here
		assertEquals("", result);
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testMain_1()
		throws Exception {
		String[] args = new String[] {};

		KerberosName.main(args);

		// add additional test code here
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testMain_2()
		throws Exception {
		String[] args = new String[] {};

		KerberosName.main(args);

		// add additional test code here
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testMain_3()
		throws Exception {
		String[] args = new String[] {};

		KerberosName.main(args);

		// add additional test code here
	}

	/**
	 * Run the List<org.apache.zookeeper.server.auth.KerberosName.Rule> parseRules(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testParseRules_1()
		throws Exception {
		String rules = "";

		List<org.apache.zookeeper.server.auth.KerberosName.Rule> result = KerberosName.parseRules(rules);

		// add additional test code here
		assertNotNull(result);
		assertEquals(0, result.size());
	}

	/**
	 * Run the List<org.apache.zookeeper.server.auth.KerberosName.Rule> parseRules(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testParseRules_2()
		throws Exception {
		String rules = "";

		List<org.apache.zookeeper.server.auth.KerberosName.Rule> result = KerberosName.parseRules(rules);

		// add additional test code here
		assertNotNull(result);
		assertEquals(0, result.size());
	}

	/**
	 * Run the List<org.apache.zookeeper.server.auth.KerberosName.Rule> parseRules(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testParseRules_3()
		throws Exception {
		String rules = "";

		List<org.apache.zookeeper.server.auth.KerberosName.Rule> result = KerberosName.parseRules(rules);

		// add additional test code here
		assertNotNull(result);
		assertEquals(0, result.size());
	}

	/**
	 * Run the List<org.apache.zookeeper.server.auth.KerberosName.Rule> parseRules(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testParseRules_4()
		throws Exception {
		String rules = "";

		List<org.apache.zookeeper.server.auth.KerberosName.Rule> result = KerberosName.parseRules(rules);

		// add additional test code here
		assertNotNull(result);
		assertEquals(0, result.size());
	}

	/**
	 * Run the void printRules() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testPrintRules_1()
		throws Exception {

		KerberosName.printRules();

		// add additional test code here
	}

	/**
	 * Run the void printRules() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testPrintRules_2()
		throws Exception {

		KerberosName.printRules();

		// add additional test code here
	}

	/**
	 * Run the void setConfiguration() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testSetConfiguration_1()
		throws Exception {

		KerberosName.setConfiguration();

		// add additional test code here
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(KerberosNameTest.class);
	}
}