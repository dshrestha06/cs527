package com.uiuc.org.apache.zookeeper.server.auth;

import org.junit.runner.JUnitCore;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * The class <code>TestAll</code> builds a suite that can be used to run all
 * of the tests within its package as well as within any subpackages of its
 * package.
 *
 * @generatedBy CodePro at 10/16/13 7:33 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
	SaslServerCallbackHandlerTest.class,
	DigestLoginModuleTest.class,
	AuthenticationProviderTest.class,
	SASLAuthenticationProviderTest.class,
	KerberosNameTest.class,
	IPAuthenticationProviderTest.class,
	ProviderRegistryTest.class,
	DigestAuthenticationProviderTest.class,
})
public class TestAll {

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	public static void main(String[] args) {
		JUnitCore.runClasses(new Class[] { TestAll.class });
	}
}
