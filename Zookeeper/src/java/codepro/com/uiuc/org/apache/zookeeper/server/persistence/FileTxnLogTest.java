package com.uiuc.org.apache.zookeeper.server.persistence;

import java.io.File;
import java.util.zip.Checksum;
import org.apache.jute.Record;
import org.apache.zookeeper.data.ACL;
import org.apache.zookeeper.server.persistence.FileTxnLog;
import org.apache.zookeeper.server.persistence.TxnLog;
import org.apache.zookeeper.txn.TxnHeader;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>FileTxnLogTest</code> contains tests for the class <code>{@link FileTxnLog}</code>.
 *
 * @generatedBy CodePro at 10/16/13 7:34 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class FileTxnLogTest {
	/**
	 * Run the FileTxnLog(File) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testFileTxnLog_1()
		throws Exception {
		File logDir = new File("");

		FileTxnLog result = new FileTxnLog(logDir);

		// add additional test code here
		assertNotNull(result);
		assertEquals(-1L, result.getLastLoggedZxid());
		assertEquals(true, result.isForceSync());
	}

	/**
	 * Run the boolean append(TxnHeader,Record) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testAppend_1()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));
		TxnHeader hdr = new TxnHeader(1L, 1, 1L, 1L, 1);
		Record txn = new ACL();

		boolean result = fixture.append(hdr, txn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:183)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:145)
		//       at org.apache.zookeeper.server.persistence.FileTxnLog.append(FileTxnLog.java:205)
		assertTrue(result);
	}

	/**
	 * Run the boolean append(TxnHeader,Record) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testAppend_2()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));
		TxnHeader hdr = new TxnHeader(1L, 1, 1L, 1L, 1);
		Record txn = new ACL();

		boolean result = fixture.append(hdr, txn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:183)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:145)
		//       at org.apache.zookeeper.server.persistence.FileTxnLog.append(FileTxnLog.java:205)
		assertTrue(result);
	}

	/**
	 * Run the boolean append(TxnHeader,Record) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testAppend_3()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));
		TxnHeader hdr = new TxnHeader(1L, 1, 1L, 1L, 1);
		Record txn = new ACL();

		boolean result = fixture.append(hdr, txn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:183)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:145)
		//       at org.apache.zookeeper.server.persistence.FileTxnLog.append(FileTxnLog.java:205)
		assertTrue(result);
	}

	/**
	 * Run the boolean append(TxnHeader,Record) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testAppend_4()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));
		TxnHeader hdr = new TxnHeader(1L, 1, 1L, 1L, 1);
		Record txn = new ACL();

		boolean result = fixture.append(hdr, txn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:183)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:145)
		//       at org.apache.zookeeper.server.persistence.FileTxnLog.append(FileTxnLog.java:205)
		assertTrue(result);
	}

	/**
	 * Run the boolean append(TxnHeader,Record) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testAppend_5()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));
		TxnHeader hdr = new TxnHeader(1L, 1, 1L, 1L, 1);
		Record txn = new ACL();

		boolean result = fixture.append(hdr, txn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:183)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:145)
		//       at org.apache.zookeeper.server.persistence.FileTxnLog.append(FileTxnLog.java:205)
		assertTrue(result);
	}

	/**
	 * Run the boolean append(TxnHeader,Record) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testAppend_6()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));
		TxnHeader hdr = new TxnHeader(1L, 1, 1L, 1L, 1);
		Record txn = new ACL();

		boolean result = fixture.append(hdr, txn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:183)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:145)
		//       at org.apache.zookeeper.server.persistence.FileTxnLog.append(FileTxnLog.java:205)
		assertTrue(result);
	}

	/**
	 * Run the boolean append(TxnHeader,Record) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testAppend_7()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));
		TxnHeader hdr = new TxnHeader(1L, 1, 1L, 1L, 1);
		Record txn = new ACL();

		boolean result = fixture.append(hdr, txn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:183)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:145)
		//       at org.apache.zookeeper.server.persistence.FileTxnLog.append(FileTxnLog.java:205)
		assertTrue(result);
	}

	/**
	 * Run the boolean append(TxnHeader,Record) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testAppend_8()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));
		TxnHeader hdr = new TxnHeader(1L, 1, 1L, 1L, 1);
		Record txn = new ACL();

		boolean result = fixture.append(hdr, txn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:183)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:145)
		//       at org.apache.zookeeper.server.persistence.FileTxnLog.append(FileTxnLog.java:205)
		assertTrue(result);
	}

	/**
	 * Run the boolean append(TxnHeader,Record) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testAppend_9()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));
		TxnHeader hdr = new TxnHeader(1L, 1, 1L, 1L, 1);
		Record txn = new ACL();

		boolean result = fixture.append(hdr, txn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:183)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:145)
		//       at org.apache.zookeeper.server.persistence.FileTxnLog.append(FileTxnLog.java:205)
		assertTrue(result);
	}

	/**
	 * Run the boolean append(TxnHeader,Record) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testAppend_10()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));
		TxnHeader hdr = new TxnHeader(1L, 1, 1L, 1L, 1);
		Record txn = new ACL();

		boolean result = fixture.append(hdr, txn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:183)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:145)
		//       at org.apache.zookeeper.server.persistence.FileTxnLog.append(FileTxnLog.java:205)
		assertTrue(result);
	}

	/**
	 * Run the boolean append(TxnHeader,Record) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testAppend_11()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));
		TxnHeader hdr = new TxnHeader(1L, 1, 1L, 1L, 1);
		Record txn = new ACL();

		boolean result = fixture.append(hdr, txn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:183)
		//       at java.io.FileOutputStream.<init>(FileOutputStream.java:145)
		//       at org.apache.zookeeper.server.persistence.FileTxnLog.append(FileTxnLog.java:205)
		assertTrue(result);
	}

	/**
	 * Run the boolean append(TxnHeader,Record) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testAppend_12()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));
		TxnHeader hdr = null;
		Record txn = new ACL();

		boolean result = fixture.append(hdr, txn);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the void close() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testClose_1()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));

		fixture.close();

		// add additional test code here
	}

	/**
	 * Run the void close() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testClose_2()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));

		fixture.close();

		// add additional test code here
	}

	/**
	 * Run the void close() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testClose_3()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));

		fixture.close();

		// add additional test code here
	}

	/**
	 * Run the void close() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testClose_4()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));

		fixture.close();

		// add additional test code here
	}

	/**
	 * Run the void close() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testClose_5()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));

		fixture.close();

		// add additional test code here
	}

	/**
	 * Run the void close() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testClose_6()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));

		fixture.close();

		// add additional test code here
	}

	/**
	 * Run the void commit() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testCommit_1()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));

		fixture.commit();

		// add additional test code here
	}

	/**
	 * Run the void commit() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testCommit_2()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));

		fixture.commit();

		// add additional test code here
	}

	/**
	 * Run the void commit() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testCommit_3()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));

		fixture.commit();

		// add additional test code here
	}

	/**
	 * Run the void commit() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testCommit_4()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));

		fixture.commit();

		// add additional test code here
	}

	/**
	 * Run the void commit() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testCommit_5()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));

		fixture.commit();

		// add additional test code here
	}

	/**
	 * Run the void commit() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testCommit_6()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));

		fixture.commit();

		// add additional test code here
	}

	/**
	 * Run the void commit() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testCommit_7()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));

		fixture.commit();

		// add additional test code here
	}

	/**
	 * Run the void commit() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testCommit_8()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));

		fixture.commit();

		// add additional test code here
	}

	/**
	 * Run the void commit() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testCommit_9()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));

		fixture.commit();

		// add additional test code here
	}

	/**
	 * Run the void commit() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testCommit_10()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));

		fixture.commit();

		// add additional test code here
	}

	/**
	 * Run the void commit() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testCommit_11()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));

		fixture.commit();

		// add additional test code here
	}

	/**
	 * Run the void commit() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testCommit_12()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));

		fixture.commit();

		// add additional test code here
	}

	/**
	 * Run the void commit() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testCommit_13()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));

		fixture.commit();

		// add additional test code here
	}

	/**
	 * Run the void commit() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testCommit_14()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));

		fixture.commit();

		// add additional test code here
	}

	/**
	 * Run the void commit() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testCommit_15()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));

		fixture.commit();

		// add additional test code here
	}

	/**
	 * Run the void commit() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testCommit_16()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));

		fixture.commit();

		// add additional test code here
	}

	/**
	 * Run the long getDbId() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testGetDbId_1()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));

		long result = fixture.getDbId();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException: name can't be null
		//       at java.io.FilePermission.init(FilePermission.java:171)
		//       at java.io.FilePermission.<init>(FilePermission.java:249)
		//       at java.lang.SecurityManager.checkRead(SecurityManager.java:871)
		//       at java.io.FileInputStream.<init>(FileInputStream.java:113)
		//       at org.apache.zookeeper.server.persistence.FileTxnLog.readHeader(FileTxnLog.java:374)
		//       at org.apache.zookeeper.server.persistence.FileTxnLog.getDbId(FileTxnLog.java:394)
		assertEquals(0L, result);
	}

	/**
	 * Run the long getDbId() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testGetDbId_2()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));

		long result = fixture.getDbId();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException: name can't be null
		//       at java.io.FilePermission.init(FilePermission.java:171)
		//       at java.io.FilePermission.<init>(FilePermission.java:249)
		//       at java.lang.SecurityManager.checkRead(SecurityManager.java:871)
		//       at java.io.FileInputStream.<init>(FileInputStream.java:113)
		//       at org.apache.zookeeper.server.persistence.FileTxnLog.readHeader(FileTxnLog.java:374)
		//       at org.apache.zookeeper.server.persistence.FileTxnLog.getDbId(FileTxnLog.java:394)
		assertEquals(0L, result);
	}

	/**
	 * Run the long getDbId() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testGetDbId_3()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));

		long result = fixture.getDbId();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException: name can't be null
		//       at java.io.FilePermission.init(FilePermission.java:171)
		//       at java.io.FilePermission.<init>(FilePermission.java:249)
		//       at java.lang.SecurityManager.checkRead(SecurityManager.java:871)
		//       at java.io.FileInputStream.<init>(FileInputStream.java:113)
		//       at org.apache.zookeeper.server.persistence.FileTxnLog.readHeader(FileTxnLog.java:374)
		//       at org.apache.zookeeper.server.persistence.FileTxnLog.getDbId(FileTxnLog.java:394)
		assertEquals(0L, result);
	}

	/**
	 * Run the long getDbId() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testGetDbId_4()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));

		long result = fixture.getDbId();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException: name can't be null
		//       at java.io.FilePermission.init(FilePermission.java:171)
		//       at java.io.FilePermission.<init>(FilePermission.java:249)
		//       at java.lang.SecurityManager.checkRead(SecurityManager.java:871)
		//       at java.io.FileInputStream.<init>(FileInputStream.java:113)
		//       at org.apache.zookeeper.server.persistence.FileTxnLog.readHeader(FileTxnLog.java:374)
		//       at org.apache.zookeeper.server.persistence.FileTxnLog.getDbId(FileTxnLog.java:394)
		assertEquals(0L, result);
	}

	/**
	 * Run the long getDbId() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testGetDbId_5()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));

		long result = fixture.getDbId();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException: name can't be null
		//       at java.io.FilePermission.init(FilePermission.java:171)
		//       at java.io.FilePermission.<init>(FilePermission.java:249)
		//       at java.lang.SecurityManager.checkRead(SecurityManager.java:871)
		//       at java.io.FileInputStream.<init>(FileInputStream.java:113)
		//       at org.apache.zookeeper.server.persistence.FileTxnLog.readHeader(FileTxnLog.java:374)
		//       at org.apache.zookeeper.server.persistence.FileTxnLog.getDbId(FileTxnLog.java:394)
		assertEquals(0L, result);
	}

	/**
	 * Run the long getLastLoggedZxid() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testGetLastLoggedZxid_1()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));

		long result = fixture.getLastLoggedZxid();

		// add additional test code here
		assertEquals(-1L, result);
	}

	/**
	 * Run the long getLastLoggedZxid() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testGetLastLoggedZxid_2()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));

		long result = fixture.getLastLoggedZxid();

		// add additional test code here
		assertEquals(-1L, result);
	}

	/**
	 * Run the long getLastLoggedZxid() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testGetLastLoggedZxid_3()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));

		long result = fixture.getLastLoggedZxid();

		// add additional test code here
		assertEquals(-1L, result);
	}

	/**
	 * Run the long getLastLoggedZxid() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testGetLastLoggedZxid_4()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));

		long result = fixture.getLastLoggedZxid();

		// add additional test code here
		assertEquals(-1L, result);
	}

	/**
	 * Run the long getLastLoggedZxid() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testGetLastLoggedZxid_5()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));

		long result = fixture.getLastLoggedZxid();

		// add additional test code here
		assertEquals(-1L, result);
	}

	/**
	 * Run the File[] getLogFiles(File[],long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testGetLogFiles_1()
		throws Exception {
		File[] logDirList = new File[] {};
		long snapshotZxid = 1L;

		File[] result = FileTxnLog.getLogFiles(logDirList, snapshotZxid);

		// add additional test code here
		assertNotNull(result);
		assertEquals(0, result.length);
	}

	/**
	 * Run the File[] getLogFiles(File[],long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testGetLogFiles_2()
		throws Exception {
		File[] logDirList = new File[] {};
		long snapshotZxid = 1L;

		File[] result = FileTxnLog.getLogFiles(logDirList, snapshotZxid);

		// add additional test code here
		assertNotNull(result);
		assertEquals(0, result.length);
	}

	/**
	 * Run the File[] getLogFiles(File[],long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testGetLogFiles_3()
		throws Exception {
		File[] logDirList = new File[] {};
		long snapshotZxid = 1L;

		File[] result = FileTxnLog.getLogFiles(logDirList, snapshotZxid);

		// add additional test code here
		assertNotNull(result);
		assertEquals(0, result.length);
	}

	/**
	 * Run the File[] getLogFiles(File[],long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testGetLogFiles_4()
		throws Exception {
		File[] logDirList = new File[] {};
		long snapshotZxid = 1L;

		File[] result = FileTxnLog.getLogFiles(logDirList, snapshotZxid);

		// add additional test code here
		assertNotNull(result);
		assertEquals(0, result.length);
	}

	/**
	 * Run the boolean isForceSync() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testIsForceSync_1()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));

		boolean result = fixture.isForceSync();

		// add additional test code here
		assertEquals(true, result);
	}

	/**
	 * Run the boolean isForceSync() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testIsForceSync_2()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));

		boolean result = fixture.isForceSync();

		// add additional test code here
		assertEquals(true, result);
	}

	/**
	 * Run the Checksum makeChecksumAlgorithm() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testMakeChecksumAlgorithm_1()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));

		Checksum result = fixture.makeChecksumAlgorithm();

		// add additional test code here
		assertNotNull(result);
		assertEquals(1L, result.getValue());
	}

	/**
	 * Run the org.apache.zookeeper.server.persistence.TxnLog.TxnIterator read(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testRead_1()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));
		long zxid = 1L;

		org.apache.zookeeper.server.persistence.TxnLog.TxnIterator result = fixture.read(zxid);

		// add additional test code here
		assertNotNull(result);
		assertEquals(false, result.next());
		assertEquals(null, result.getHeader());
		assertEquals(null, result.getTxn());
	}

	/**
	 * Run the org.apache.zookeeper.server.persistence.TxnLog.TxnIterator read(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testRead_2()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));
		long zxid = 1L;

		org.apache.zookeeper.server.persistence.TxnLog.TxnIterator result = fixture.read(zxid);

		// add additional test code here
		assertNotNull(result);
		assertEquals(false, result.next());
		assertEquals(null, result.getHeader());
		assertEquals(null, result.getTxn());
	}

	/**
	 * Run the void rollLog() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testRollLog_1()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));

		fixture.rollLog();

		// add additional test code here
	}

	/**
	 * Run the void rollLog() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testRollLog_2()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));

		fixture.rollLog();

		// add additional test code here
	}

	/**
	 * Run the void rollLog() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testRollLog_3()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));

		fixture.rollLog();

		// add additional test code here
	}

	/**
	 * Run the void setPreallocSize(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testSetPreallocSize_1()
		throws Exception {
		long size = 1L;

		FileTxnLog.setPreallocSize(size);

		// add additional test code here
	}

	/**
	 * Run the boolean truncate(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testTruncate_1()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));
		long zxid = 1L;

		boolean result = fixture.truncate(zxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.persistence.FileTxnLog.truncate(FileTxnLog.java:352)
		assertTrue(result);
	}

	/**
	 * Run the boolean truncate(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testTruncate_2()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));
		long zxid = 1L;

		boolean result = fixture.truncate(zxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.persistence.FileTxnLog.truncate(FileTxnLog.java:352)
		assertTrue(result);
	}

	/**
	 * Run the boolean truncate(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testTruncate_3()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));
		long zxid = 1L;

		boolean result = fixture.truncate(zxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.persistence.FileTxnLog.truncate(FileTxnLog.java:352)
		assertTrue(result);
	}

	/**
	 * Run the boolean truncate(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testTruncate_4()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));
		long zxid = 1L;

		boolean result = fixture.truncate(zxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.persistence.FileTxnLog.truncate(FileTxnLog.java:352)
		assertTrue(result);
	}

	/**
	 * Run the boolean truncate(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testTruncate_5()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));
		long zxid = 1L;

		boolean result = fixture.truncate(zxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.persistence.FileTxnLog.truncate(FileTxnLog.java:352)
		assertTrue(result);
	}

	/**
	 * Run the boolean truncate(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testTruncate_6()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));
		long zxid = 1L;

		boolean result = fixture.truncate(zxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.persistence.FileTxnLog.truncate(FileTxnLog.java:352)
		assertTrue(result);
	}

	/**
	 * Run the boolean truncate(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testTruncate_7()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));
		long zxid = 1L;

		boolean result = fixture.truncate(zxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.persistence.FileTxnLog.truncate(FileTxnLog.java:352)
		assertTrue(result);
	}

	/**
	 * Run the boolean truncate(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testTruncate_8()
		throws Exception {
		FileTxnLog fixture = new FileTxnLog(new File(""));
		long zxid = 1L;

		boolean result = fixture.truncate(zxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.persistence.FileTxnLog.truncate(FileTxnLog.java:352)
		assertTrue(result);
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(FileTxnLogTest.class);
	}
}