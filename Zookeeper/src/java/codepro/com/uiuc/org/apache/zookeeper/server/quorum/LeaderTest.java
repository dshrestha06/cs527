package com.uiuc.org.apache.zookeeper.server.quorum;

import java.net.SocketAddress;
import java.util.List;
import org.apache.zookeeper.server.Request;
import org.apache.zookeeper.server.quorum.Leader;
import org.apache.zookeeper.server.quorum.LeaderZooKeeperServer;
import org.apache.zookeeper.server.quorum.LearnerHandler;
import org.apache.zookeeper.server.quorum.LearnerSyncRequest;
import org.apache.zookeeper.server.quorum.QuorumPacket;
import org.apache.zookeeper.server.quorum.QuorumPeer;
import org.apache.zookeeper.server.quorum.StateSummary;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>LeaderTest</code> contains tests for the class <code>{@link Leader}</code>.
 *
 * @generatedBy CodePro at 10/16/13 7:58 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class LeaderTest {
	/**
	 * Run the Leader(QuorumPeer,LeaderZooKeeperServer) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testLeader_1()
		throws Exception {
		QuorumPeer self = new QuorumPeer();
		LeaderZooKeeperServer zk = null;

		Leader result = new Leader(self, zk);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.Leader.<init>(Leader.java:184)
		assertNotNull(result);
	}

	/**
	 * Run the String getPacketType(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testGetPacketType_1()
		throws Exception {
		int packetType = 13;

		String result = Leader.getPacketType(packetType);

		// add additional test code here
		assertEquals("DIFF", result);
	}

	/**
	 * Run the String getPacketType(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testGetPacketType_2()
		throws Exception {
		int packetType = 14;

		String result = Leader.getPacketType(packetType);

		// add additional test code here
		assertEquals("TRUNC", result);
	}

	/**
	 * Run the String getPacketType(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testGetPacketType_3()
		throws Exception {
		int packetType = 15;

		String result = Leader.getPacketType(packetType);

		// add additional test code here
		assertEquals("SNAP", result);
	}

	/**
	 * Run the String getPacketType(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testGetPacketType_4()
		throws Exception {
		int packetType = 16;

		String result = Leader.getPacketType(packetType);

		// add additional test code here
		assertEquals("OBSERVERINFO", result);
	}

	/**
	 * Run the String getPacketType(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testGetPacketType_5()
		throws Exception {
		int packetType = 10;

		String result = Leader.getPacketType(packetType);

		// add additional test code here
		assertEquals("NEWLEADER", result);
	}

	/**
	 * Run the String getPacketType(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testGetPacketType_6()
		throws Exception {
		int packetType = 11;

		String result = Leader.getPacketType(packetType);

		// add additional test code here
		assertEquals("FOLLOWERINFO", result);
	}

	/**
	 * Run the String getPacketType(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testGetPacketType_7()
		throws Exception {
		int packetType = 12;

		String result = Leader.getPacketType(packetType);

		// add additional test code here
		assertEquals("UPTODATE", result);
	}

	/**
	 * Run the String getPacketType(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testGetPacketType_8()
		throws Exception {
		int packetType = 17;

		String result = Leader.getPacketType(packetType);

		// add additional test code here
		assertEquals("LEADERINFO", result);
	}

	/**
	 * Run the String getPacketType(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testGetPacketType_9()
		throws Exception {
		int packetType = 18;

		String result = Leader.getPacketType(packetType);

		// add additional test code here
		assertEquals("ACKEPOCH", result);
	}

	/**
	 * Run the String getPacketType(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testGetPacketType_10()
		throws Exception {
		int packetType = 1;

		String result = Leader.getPacketType(packetType);

		// add additional test code here
		assertEquals("REQUEST", result);
	}

	/**
	 * Run the String getPacketType(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testGetPacketType_11()
		throws Exception {
		int packetType = 2;

		String result = Leader.getPacketType(packetType);

		// add additional test code here
		assertEquals("PROPOSAL", result);
	}

	/**
	 * Run the String getPacketType(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testGetPacketType_12()
		throws Exception {
		int packetType = 3;

		String result = Leader.getPacketType(packetType);

		// add additional test code here
		assertEquals("ACK", result);
	}

	/**
	 * Run the String getPacketType(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testGetPacketType_13()
		throws Exception {
		int packetType = 4;

		String result = Leader.getPacketType(packetType);

		// add additional test code here
		assertEquals("COMMIT", result);
	}

	/**
	 * Run the String getPacketType(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testGetPacketType_14()
		throws Exception {
		int packetType = 5;

		String result = Leader.getPacketType(packetType);

		// add additional test code here
		assertEquals("PING", result);
	}

	/**
	 * Run the String getPacketType(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testGetPacketType_15()
		throws Exception {
		int packetType = 6;

		String result = Leader.getPacketType(packetType);

		// add additional test code here
		assertEquals("REVALIDATE", result);
	}

	/**
	 * Run the String getPacketType(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testGetPacketType_16()
		throws Exception {
		int packetType = 9;

		String result = Leader.getPacketType(packetType);

		// add additional test code here
		assertEquals("UNKNOWN", result);
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(LeaderTest.class);
	}
}