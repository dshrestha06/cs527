package com.uiuc.org.apache.zookeeper.server.quorum;

import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.LinkedList;
import java.util.List;
import org.apache.zookeeper.data.Id;
import org.apache.zookeeper.server.FinalRequestProcessor;
import org.apache.zookeeper.server.NIOServerCnxn;
import org.apache.zookeeper.server.NIOServerCnxnFactory;
import org.apache.zookeeper.server.Request;
import org.apache.zookeeper.server.RequestProcessor;
import org.apache.zookeeper.server.ServerCnxn;
import org.apache.zookeeper.server.ZooKeeperServer;
import org.apache.zookeeper.server.quorum.ObserverRequestProcessor;
import org.apache.zookeeper.server.quorum.ObserverZooKeeperServer;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>ObserverRequestProcessorTest</code> contains tests for the class <code>{@link ObserverRequestProcessor}</code>.
 *
 * @generatedBy CodePro at 10/16/13 7:59 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class ObserverRequestProcessorTest {
	/**
	 * Run the ObserverRequestProcessor(ObserverZooKeeperServer,RequestProcessor) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@Test
	public void testObserverRequestProcessor_1()
		throws Exception {
		ObserverZooKeeperServer zks = null;
		RequestProcessor nextProcessor = new FinalRequestProcessor(new ZooKeeperServer());

		ObserverRequestProcessor result = new ObserverRequestProcessor(zks, nextProcessor);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.ObserverRequestProcessor.<init>(ObserverRequestProcessor.java:57)
		assertNotNull(result);
	}

	/**
	 * Run the void processRequest(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@Test
	public void testProcessRequest_1()
		throws Exception {
		ObserverRequestProcessor fixture = new ObserverRequestProcessor((ObserverZooKeeperServer) null, new FinalRequestProcessor(new ZooKeeperServer()));
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());

		fixture.processRequest(request);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.ObserverRequestProcessor.<init>(ObserverRequestProcessor.java:57)
	}

	/**
	 * Run the void processRequest(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@Test
	public void testProcessRequest_2()
		throws Exception {
		ObserverRequestProcessor fixture = new ObserverRequestProcessor((ObserverZooKeeperServer) null, new FinalRequestProcessor(new ZooKeeperServer()));
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());

		fixture.processRequest(request);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.ObserverRequestProcessor.<init>(ObserverRequestProcessor.java:57)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@Test
	public void testRun_1()
		throws Exception {
		ObserverRequestProcessor fixture = new ObserverRequestProcessor((ObserverZooKeeperServer) null, new FinalRequestProcessor(new ZooKeeperServer()));

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.ObserverRequestProcessor.<init>(ObserverRequestProcessor.java:57)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@Test
	public void testRun_2()
		throws Exception {
		ObserverRequestProcessor fixture = new ObserverRequestProcessor((ObserverZooKeeperServer) null, new FinalRequestProcessor(new ZooKeeperServer()));

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.ObserverRequestProcessor.<init>(ObserverRequestProcessor.java:57)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@Test
	public void testRun_3()
		throws Exception {
		ObserverRequestProcessor fixture = new ObserverRequestProcessor((ObserverZooKeeperServer) null, new FinalRequestProcessor(new ZooKeeperServer()));

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.ObserverRequestProcessor.<init>(ObserverRequestProcessor.java:57)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@Test
	public void testRun_4()
		throws Exception {
		ObserverRequestProcessor fixture = new ObserverRequestProcessor((ObserverZooKeeperServer) null, new FinalRequestProcessor(new ZooKeeperServer()));

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.ObserverRequestProcessor.<init>(ObserverRequestProcessor.java:57)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@Test
	public void testRun_5()
		throws Exception {
		ObserverRequestProcessor fixture = new ObserverRequestProcessor((ObserverZooKeeperServer) null, new FinalRequestProcessor(new ZooKeeperServer()));

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.ObserverRequestProcessor.<init>(ObserverRequestProcessor.java:57)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@Test
	public void testRun_6()
		throws Exception {
		ObserverRequestProcessor fixture = new ObserverRequestProcessor((ObserverZooKeeperServer) null, new FinalRequestProcessor(new ZooKeeperServer()));

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.ObserverRequestProcessor.<init>(ObserverRequestProcessor.java:57)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@Test
	public void testRun_7()
		throws Exception {
		ObserverRequestProcessor fixture = new ObserverRequestProcessor((ObserverZooKeeperServer) null, new FinalRequestProcessor(new ZooKeeperServer()));

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.ObserverRequestProcessor.<init>(ObserverRequestProcessor.java:57)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@Test
	public void testRun_8()
		throws Exception {
		ObserverRequestProcessor fixture = new ObserverRequestProcessor((ObserverZooKeeperServer) null, new FinalRequestProcessor(new ZooKeeperServer()));

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.ObserverRequestProcessor.<init>(ObserverRequestProcessor.java:57)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@Test
	public void testRun_9()
		throws Exception {
		ObserverRequestProcessor fixture = new ObserverRequestProcessor((ObserverZooKeeperServer) null, new FinalRequestProcessor(new ZooKeeperServer()));

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.ObserverRequestProcessor.<init>(ObserverRequestProcessor.java:57)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@Test
	public void testRun_10()
		throws Exception {
		ObserverRequestProcessor fixture = new ObserverRequestProcessor((ObserverZooKeeperServer) null, new FinalRequestProcessor(new ZooKeeperServer()));

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.ObserverRequestProcessor.<init>(ObserverRequestProcessor.java:57)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@Test
	public void testRun_11()
		throws Exception {
		ObserverRequestProcessor fixture = new ObserverRequestProcessor((ObserverZooKeeperServer) null, new FinalRequestProcessor(new ZooKeeperServer()));

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.ObserverRequestProcessor.<init>(ObserverRequestProcessor.java:57)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@Test
	public void testRun_12()
		throws Exception {
		ObserverRequestProcessor fixture = new ObserverRequestProcessor((ObserverZooKeeperServer) null, new FinalRequestProcessor(new ZooKeeperServer()));

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.ObserverRequestProcessor.<init>(ObserverRequestProcessor.java:57)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@Test
	public void testRun_13()
		throws Exception {
		ObserverRequestProcessor fixture = new ObserverRequestProcessor((ObserverZooKeeperServer) null, new FinalRequestProcessor(new ZooKeeperServer()));

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.ObserverRequestProcessor.<init>(ObserverRequestProcessor.java:57)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@Test
	public void testRun_14()
		throws Exception {
		ObserverRequestProcessor fixture = new ObserverRequestProcessor((ObserverZooKeeperServer) null, new FinalRequestProcessor(new ZooKeeperServer()));

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.ObserverRequestProcessor.<init>(ObserverRequestProcessor.java:57)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@Test
	public void testRun_15()
		throws Exception {
		ObserverRequestProcessor fixture = new ObserverRequestProcessor((ObserverZooKeeperServer) null, new FinalRequestProcessor(new ZooKeeperServer()));

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.ObserverRequestProcessor.<init>(ObserverRequestProcessor.java:57)
	}

	/**
	 * Run the void shutdown() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@Test
	public void testShutdown_1()
		throws Exception {
		ObserverRequestProcessor fixture = new ObserverRequestProcessor((ObserverZooKeeperServer) null, new FinalRequestProcessor(new ZooKeeperServer()));

		fixture.shutdown();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.ObserverRequestProcessor.<init>(ObserverRequestProcessor.java:57)
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(ObserverRequestProcessorTest.class);
	}
}