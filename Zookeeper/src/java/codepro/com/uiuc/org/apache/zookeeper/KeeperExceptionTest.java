package com.uiuc.org.apache.zookeeper;

import java.util.LinkedList;
import java.util.List;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.OpResult;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>KeeperExceptionTest</code> contains tests for the class <code>{@link KeeperException}</code>.
 *
 * @generatedBy CodePro at 10/16/13 6:57 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class KeeperExceptionTest {
	/**
	 * Run the org.apache.zookeeper.KeeperException.Code code() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testCode_1()
		throws Exception {
		KeeperException fixture = new org.apache.zookeeper.KeeperException.UnimplementedException();

		org.apache.zookeeper.KeeperException.Code result = fixture.code();

		// add additional test code here
		assertNotNull(result);
		assertEquals(-6, result.intValue());
		assertEquals("UNIMPLEMENTED", result.name());
		assertEquals("UNIMPLEMENTED", result.toString());
		assertEquals(6, result.ordinal());
	}

	/**
	 * Run the KeeperException create(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testCreate_1()
		throws Exception {
		int code = 1;

		KeeperException result = KeeperException.create(code);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.KeeperException.create(KeeperException.java:91)
		//       at org.apache.zookeeper.KeeperException.create(KeeperException.java:73)
		assertNotNull(result);
	}

	/**
	 * Run the KeeperException create(Code) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testCreate_2()
		throws Exception {
		org.apache.zookeeper.KeeperException.Code code = org.apache.zookeeper.KeeperException.Code.APIERROR;

		KeeperException result = KeeperException.create(code);

		// add additional test code here
		assertNotNull(result);
		assertEquals("KeeperErrorCode = APIError", result.getMessage());
		assertEquals(null, result.getPath());
		assertEquals(-100, result.getCode());
		assertEquals(null, result.getResults());
		assertEquals(null, result.getCause());
		assertEquals("org.apache.zookeeper.KeeperException$APIErrorException: KeeperErrorCode = APIError", result.toString());
		assertEquals("KeeperErrorCode = APIError", result.getLocalizedMessage());
	}

	/**
	 * Run the KeeperException create(Code) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testCreate_3()
		throws Exception {
		org.apache.zookeeper.KeeperException.Code code = org.apache.zookeeper.KeeperException.Code.APIERROR;

		KeeperException result = KeeperException.create(code);

		// add additional test code here
		assertNotNull(result);
		assertEquals("KeeperErrorCode = APIError", result.getMessage());
		assertEquals(null, result.getPath());
		assertEquals(-100, result.getCode());
		assertEquals(null, result.getResults());
		assertEquals(null, result.getCause());
		assertEquals("org.apache.zookeeper.KeeperException$APIErrorException: KeeperErrorCode = APIError", result.toString());
		assertEquals("KeeperErrorCode = APIError", result.getLocalizedMessage());
	}

	/**
	 * Run the KeeperException create(Code) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testCreate_4()
		throws Exception {
		org.apache.zookeeper.KeeperException.Code code = org.apache.zookeeper.KeeperException.Code.APIERROR;

		KeeperException result = KeeperException.create(code);

		// add additional test code here
		assertNotNull(result);
		assertEquals("KeeperErrorCode = APIError", result.getMessage());
		assertEquals(null, result.getPath());
		assertEquals(-100, result.getCode());
		assertEquals(null, result.getResults());
		assertEquals(null, result.getCause());
		assertEquals("org.apache.zookeeper.KeeperException$APIErrorException: KeeperErrorCode = APIError", result.toString());
		assertEquals("KeeperErrorCode = APIError", result.getLocalizedMessage());
	}

	/**
	 * Run the KeeperException create(Code) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testCreate_5()
		throws Exception {
		org.apache.zookeeper.KeeperException.Code code = org.apache.zookeeper.KeeperException.Code.APIERROR;

		KeeperException result = KeeperException.create(code);

		// add additional test code here
		assertNotNull(result);
		assertEquals("KeeperErrorCode = APIError", result.getMessage());
		assertEquals(null, result.getPath());
		assertEquals(-100, result.getCode());
		assertEquals(null, result.getResults());
		assertEquals(null, result.getCause());
		assertEquals("org.apache.zookeeper.KeeperException$APIErrorException: KeeperErrorCode = APIError", result.toString());
		assertEquals("KeeperErrorCode = APIError", result.getLocalizedMessage());
	}

	/**
	 * Run the KeeperException create(Code) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testCreate_6()
		throws Exception {
		org.apache.zookeeper.KeeperException.Code code = org.apache.zookeeper.KeeperException.Code.APIERROR;

		KeeperException result = KeeperException.create(code);

		// add additional test code here
		assertNotNull(result);
		assertEquals("KeeperErrorCode = APIError", result.getMessage());
		assertEquals(null, result.getPath());
		assertEquals(-100, result.getCode());
		assertEquals(null, result.getResults());
		assertEquals(null, result.getCause());
		assertEquals("org.apache.zookeeper.KeeperException$APIErrorException: KeeperErrorCode = APIError", result.toString());
		assertEquals("KeeperErrorCode = APIError", result.getLocalizedMessage());
	}

	/**
	 * Run the KeeperException create(Code) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testCreate_7()
		throws Exception {
		org.apache.zookeeper.KeeperException.Code code = org.apache.zookeeper.KeeperException.Code.APIERROR;

		KeeperException result = KeeperException.create(code);

		// add additional test code here
		assertNotNull(result);
		assertEquals("KeeperErrorCode = APIError", result.getMessage());
		assertEquals(null, result.getPath());
		assertEquals(-100, result.getCode());
		assertEquals(null, result.getResults());
		assertEquals(null, result.getCause());
		assertEquals("org.apache.zookeeper.KeeperException$APIErrorException: KeeperErrorCode = APIError", result.toString());
		assertEquals("KeeperErrorCode = APIError", result.getLocalizedMessage());
	}

	/**
	 * Run the KeeperException create(Code) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testCreate_8()
		throws Exception {
		org.apache.zookeeper.KeeperException.Code code = org.apache.zookeeper.KeeperException.Code.APIERROR;

		KeeperException result = KeeperException.create(code);

		// add additional test code here
		assertNotNull(result);
		assertEquals("KeeperErrorCode = APIError", result.getMessage());
		assertEquals(null, result.getPath());
		assertEquals(-100, result.getCode());
		assertEquals(null, result.getResults());
		assertEquals(null, result.getCause());
		assertEquals("org.apache.zookeeper.KeeperException$APIErrorException: KeeperErrorCode = APIError", result.toString());
		assertEquals("KeeperErrorCode = APIError", result.getLocalizedMessage());
	}

	/**
	 * Run the KeeperException create(Code) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testCreate_9()
		throws Exception {
		org.apache.zookeeper.KeeperException.Code code = org.apache.zookeeper.KeeperException.Code.APIERROR;

		KeeperException result = KeeperException.create(code);

		// add additional test code here
		assertNotNull(result);
		assertEquals("KeeperErrorCode = APIError", result.getMessage());
		assertEquals(null, result.getPath());
		assertEquals(-100, result.getCode());
		assertEquals(null, result.getResults());
		assertEquals(null, result.getCause());
		assertEquals("org.apache.zookeeper.KeeperException$APIErrorException: KeeperErrorCode = APIError", result.toString());
		assertEquals("KeeperErrorCode = APIError", result.getLocalizedMessage());
	}

	/**
	 * Run the KeeperException create(Code) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testCreate_10()
		throws Exception {
		org.apache.zookeeper.KeeperException.Code code = org.apache.zookeeper.KeeperException.Code.APIERROR;

		KeeperException result = KeeperException.create(code);

		// add additional test code here
		assertNotNull(result);
		assertEquals("KeeperErrorCode = APIError", result.getMessage());
		assertEquals(null, result.getPath());
		assertEquals(-100, result.getCode());
		assertEquals(null, result.getResults());
		assertEquals(null, result.getCause());
		assertEquals("org.apache.zookeeper.KeeperException$APIErrorException: KeeperErrorCode = APIError", result.toString());
		assertEquals("KeeperErrorCode = APIError", result.getLocalizedMessage());
	}

	/**
	 * Run the KeeperException create(Code) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testCreate_11()
		throws Exception {
		org.apache.zookeeper.KeeperException.Code code = org.apache.zookeeper.KeeperException.Code.APIERROR;

		KeeperException result = KeeperException.create(code);

		// add additional test code here
		assertNotNull(result);
		assertEquals("KeeperErrorCode = APIError", result.getMessage());
		assertEquals(null, result.getPath());
		assertEquals(-100, result.getCode());
		assertEquals(null, result.getResults());
		assertEquals(null, result.getCause());
		assertEquals("org.apache.zookeeper.KeeperException$APIErrorException: KeeperErrorCode = APIError", result.toString());
		assertEquals("KeeperErrorCode = APIError", result.getLocalizedMessage());
	}

	/**
	 * Run the KeeperException create(Code) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testCreate_12()
		throws Exception {
		org.apache.zookeeper.KeeperException.Code code = org.apache.zookeeper.KeeperException.Code.APIERROR;

		KeeperException result = KeeperException.create(code);

		// add additional test code here
		assertNotNull(result);
		assertEquals("KeeperErrorCode = APIError", result.getMessage());
		assertEquals(null, result.getPath());
		assertEquals(-100, result.getCode());
		assertEquals(null, result.getResults());
		assertEquals(null, result.getCause());
		assertEquals("org.apache.zookeeper.KeeperException$APIErrorException: KeeperErrorCode = APIError", result.toString());
		assertEquals("KeeperErrorCode = APIError", result.getLocalizedMessage());
	}

	/**
	 * Run the KeeperException create(Code) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testCreate_13()
		throws Exception {
		org.apache.zookeeper.KeeperException.Code code = org.apache.zookeeper.KeeperException.Code.APIERROR;

		KeeperException result = KeeperException.create(code);

		// add additional test code here
		assertNotNull(result);
		assertEquals("KeeperErrorCode = APIError", result.getMessage());
		assertEquals(null, result.getPath());
		assertEquals(-100, result.getCode());
		assertEquals(null, result.getResults());
		assertEquals(null, result.getCause());
		assertEquals("org.apache.zookeeper.KeeperException$APIErrorException: KeeperErrorCode = APIError", result.toString());
		assertEquals("KeeperErrorCode = APIError", result.getLocalizedMessage());
	}

	/**
	 * Run the KeeperException create(Code) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testCreate_14()
		throws Exception {
		org.apache.zookeeper.KeeperException.Code code = org.apache.zookeeper.KeeperException.Code.APIERROR;

		KeeperException result = KeeperException.create(code);

		// add additional test code here
		assertNotNull(result);
		assertEquals("KeeperErrorCode = APIError", result.getMessage());
		assertEquals(null, result.getPath());
		assertEquals(-100, result.getCode());
		assertEquals(null, result.getResults());
		assertEquals(null, result.getCause());
		assertEquals("org.apache.zookeeper.KeeperException$APIErrorException: KeeperErrorCode = APIError", result.toString());
		assertEquals("KeeperErrorCode = APIError", result.getLocalizedMessage());
	}

	/**
	 * Run the KeeperException create(Code) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testCreate_15()
		throws Exception {
		org.apache.zookeeper.KeeperException.Code code = org.apache.zookeeper.KeeperException.Code.APIERROR;

		KeeperException result = KeeperException.create(code);

		// add additional test code here
		assertNotNull(result);
		assertEquals("KeeperErrorCode = APIError", result.getMessage());
		assertEquals(null, result.getPath());
		assertEquals(-100, result.getCode());
		assertEquals(null, result.getResults());
		assertEquals(null, result.getCause());
		assertEquals("org.apache.zookeeper.KeeperException$APIErrorException: KeeperErrorCode = APIError", result.toString());
		assertEquals("KeeperErrorCode = APIError", result.getLocalizedMessage());
	}

	/**
	 * Run the KeeperException create(Code) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testCreate_16()
		throws Exception {
		org.apache.zookeeper.KeeperException.Code code = org.apache.zookeeper.KeeperException.Code.APIERROR;

		KeeperException result = KeeperException.create(code);

		// add additional test code here
		assertNotNull(result);
		assertEquals("KeeperErrorCode = APIError", result.getMessage());
		assertEquals(null, result.getPath());
		assertEquals(-100, result.getCode());
		assertEquals(null, result.getResults());
		assertEquals(null, result.getCause());
		assertEquals("org.apache.zookeeper.KeeperException$APIErrorException: KeeperErrorCode = APIError", result.toString());
		assertEquals("KeeperErrorCode = APIError", result.getLocalizedMessage());
	}

	/**
	 * Run the KeeperException create(Code) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testCreate_17()
		throws Exception {
		org.apache.zookeeper.KeeperException.Code code = org.apache.zookeeper.KeeperException.Code.APIERROR;

		KeeperException result = KeeperException.create(code);

		// add additional test code here
		assertNotNull(result);
		assertEquals("KeeperErrorCode = APIError", result.getMessage());
		assertEquals(null, result.getPath());
		assertEquals(-100, result.getCode());
		assertEquals(null, result.getResults());
		assertEquals(null, result.getCause());
		assertEquals("org.apache.zookeeper.KeeperException$APIErrorException: KeeperErrorCode = APIError", result.toString());
		assertEquals("KeeperErrorCode = APIError", result.getLocalizedMessage());
	}

	/**
	 * Run the KeeperException create(int,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testCreate_18()
		throws Exception {
		int code = 1;
		String path = "";

		KeeperException result = KeeperException.create(code, path);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.KeeperException.create(KeeperException.java:91)
		//       at org.apache.zookeeper.KeeperException.create(KeeperException.java:62)
		assertNotNull(result);
	}

	/**
	 * Run the KeeperException create(Code,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testCreate_19()
		throws Exception {
		org.apache.zookeeper.KeeperException.Code code = org.apache.zookeeper.KeeperException.Code.APIERROR;
		String path = "";

		KeeperException result = KeeperException.create(code, path);

		// add additional test code here
		assertNotNull(result);
		assertEquals("KeeperErrorCode = APIError for ", result.getMessage());
		assertEquals("", result.getPath());
		assertEquals(-100, result.getCode());
		assertEquals(null, result.getResults());
		assertEquals(null, result.getCause());
		assertEquals("org.apache.zookeeper.KeeperException$APIErrorException: KeeperErrorCode = APIError for ", result.toString());
		assertEquals("KeeperErrorCode = APIError for ", result.getLocalizedMessage());
	}

	/**
	 * Run the int getCode() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testGetCode_1()
		throws Exception {
		KeeperException fixture = new org.apache.zookeeper.KeeperException.UnimplementedException();

		int result = fixture.getCode();

		// add additional test code here
		assertEquals(-6, result);
	}

	/**
	 * Run the String getCodeMessage(Code) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testGetCodeMessage_1()
		throws Exception {
		org.apache.zookeeper.KeeperException.Code code = org.apache.zookeeper.KeeperException.Code.APIERROR;

		String result = KeeperException.getCodeMessage(code);

		// add additional test code here
		assertEquals("APIError", result);
	}

	/**
	 * Run the String getCodeMessage(Code) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testGetCodeMessage_2()
		throws Exception {
		org.apache.zookeeper.KeeperException.Code code = org.apache.zookeeper.KeeperException.Code.APIERROR;

		String result = KeeperException.getCodeMessage(code);

		// add additional test code here
		assertEquals("APIError", result);
	}

	/**
	 * Run the String getCodeMessage(Code) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testGetCodeMessage_3()
		throws Exception {
		org.apache.zookeeper.KeeperException.Code code = org.apache.zookeeper.KeeperException.Code.APIERROR;

		String result = KeeperException.getCodeMessage(code);

		// add additional test code here
		assertEquals("APIError", result);
	}

	/**
	 * Run the String getCodeMessage(Code) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testGetCodeMessage_4()
		throws Exception {
		org.apache.zookeeper.KeeperException.Code code = org.apache.zookeeper.KeeperException.Code.APIERROR;

		String result = KeeperException.getCodeMessage(code);

		// add additional test code here
		assertEquals("APIError", result);
	}

	/**
	 * Run the String getCodeMessage(Code) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testGetCodeMessage_5()
		throws Exception {
		org.apache.zookeeper.KeeperException.Code code = org.apache.zookeeper.KeeperException.Code.APIERROR;

		String result = KeeperException.getCodeMessage(code);

		// add additional test code here
		assertEquals("APIError", result);
	}

	/**
	 * Run the String getCodeMessage(Code) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testGetCodeMessage_6()
		throws Exception {
		org.apache.zookeeper.KeeperException.Code code = org.apache.zookeeper.KeeperException.Code.APIERROR;

		String result = KeeperException.getCodeMessage(code);

		// add additional test code here
		assertEquals("APIError", result);
	}

	/**
	 * Run the String getCodeMessage(Code) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testGetCodeMessage_7()
		throws Exception {
		org.apache.zookeeper.KeeperException.Code code = org.apache.zookeeper.KeeperException.Code.APIERROR;

		String result = KeeperException.getCodeMessage(code);

		// add additional test code here
		assertEquals("APIError", result);
	}

	/**
	 * Run the String getCodeMessage(Code) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testGetCodeMessage_8()
		throws Exception {
		org.apache.zookeeper.KeeperException.Code code = org.apache.zookeeper.KeeperException.Code.APIERROR;

		String result = KeeperException.getCodeMessage(code);

		// add additional test code here
		assertEquals("APIError", result);
	}

	/**
	 * Run the String getCodeMessage(Code) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testGetCodeMessage_9()
		throws Exception {
		org.apache.zookeeper.KeeperException.Code code = org.apache.zookeeper.KeeperException.Code.APIERROR;

		String result = KeeperException.getCodeMessage(code);

		// add additional test code here
		assertEquals("APIError", result);
	}

	/**
	 * Run the String getCodeMessage(Code) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testGetCodeMessage_10()
		throws Exception {
		org.apache.zookeeper.KeeperException.Code code = org.apache.zookeeper.KeeperException.Code.APIERROR;

		String result = KeeperException.getCodeMessage(code);

		// add additional test code here
		assertEquals("APIError", result);
	}

	/**
	 * Run the String getCodeMessage(Code) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testGetCodeMessage_11()
		throws Exception {
		org.apache.zookeeper.KeeperException.Code code = org.apache.zookeeper.KeeperException.Code.APIERROR;

		String result = KeeperException.getCodeMessage(code);

		// add additional test code here
		assertEquals("APIError", result);
	}

	/**
	 * Run the String getCodeMessage(Code) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testGetCodeMessage_12()
		throws Exception {
		org.apache.zookeeper.KeeperException.Code code = org.apache.zookeeper.KeeperException.Code.APIERROR;

		String result = KeeperException.getCodeMessage(code);

		// add additional test code here
		assertEquals("APIError", result);
	}

	/**
	 * Run the String getCodeMessage(Code) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testGetCodeMessage_13()
		throws Exception {
		org.apache.zookeeper.KeeperException.Code code = org.apache.zookeeper.KeeperException.Code.APIERROR;

		String result = KeeperException.getCodeMessage(code);

		// add additional test code here
		assertEquals("APIError", result);
	}

	/**
	 * Run the String getCodeMessage(Code) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testGetCodeMessage_14()
		throws Exception {
		org.apache.zookeeper.KeeperException.Code code = org.apache.zookeeper.KeeperException.Code.APIERROR;

		String result = KeeperException.getCodeMessage(code);

		// add additional test code here
		assertEquals("APIError", result);
	}

	/**
	 * Run the String getCodeMessage(Code) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testGetCodeMessage_15()
		throws Exception {
		org.apache.zookeeper.KeeperException.Code code = org.apache.zookeeper.KeeperException.Code.APIERROR;

		String result = KeeperException.getCodeMessage(code);

		// add additional test code here
		assertEquals("APIError", result);
	}

	/**
	 * Run the String getCodeMessage(Code) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testGetCodeMessage_16()
		throws Exception {
		org.apache.zookeeper.KeeperException.Code code = org.apache.zookeeper.KeeperException.Code.APIERROR;

		String result = KeeperException.getCodeMessage(code);

		// add additional test code here
		assertEquals("APIError", result);
	}

	/**
	 * Run the String getMessage() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testGetMessage_1()
		throws Exception {
		KeeperException fixture = new org.apache.zookeeper.KeeperException.UnimplementedException();

		String result = fixture.getMessage();

		// add additional test code here
		assertEquals("KeeperErrorCode = Unimplemented", result);
	}

	/**
	 * Run the String getMessage() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testGetMessage_2()
		throws Exception {
		KeeperException fixture = new org.apache.zookeeper.KeeperException.UnimplementedException();

		String result = fixture.getMessage();

		// add additional test code here
		assertEquals("KeeperErrorCode = Unimplemented", result);
	}

	/**
	 * Run the String getPath() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testGetPath_1()
		throws Exception {
		KeeperException fixture = new org.apache.zookeeper.KeeperException.UnimplementedException();

		String result = fixture.getPath();

		// add additional test code here
		assertEquals(null, result);
	}

	/**
	 * Run the List<OpResult> getResults() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testGetResults_1()
		throws Exception {
		KeeperException fixture = new org.apache.zookeeper.KeeperException.UnimplementedException();

		List<OpResult> result = fixture.getResults();

		// add additional test code here
		assertEquals(null, result);
	}

	/**
	 * Run the List<OpResult> getResults() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testGetResults_2()
		throws Exception {
		KeeperException fixture = new org.apache.zookeeper.KeeperException.UnimplementedException();

		List<OpResult> result = fixture.getResults();

		// add additional test code here
		assertEquals(null, result);
	}

	/**
	 * Run the void setCode(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testSetCode_1()
		throws Exception {
		KeeperException fixture = new org.apache.zookeeper.KeeperException.UnimplementedException();
		int code = 1;

		fixture.setCode(code);

		// add additional test code here
	}

	/**
	 * Run the void setMultiResults(List<OpResult>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Test
	public void testSetMultiResults_1()
		throws Exception {
		KeeperException fixture = new org.apache.zookeeper.KeeperException.UnimplementedException();
		List<OpResult> results = new LinkedList();

		fixture.setMultiResults(results);

		// add additional test code here
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 6:57 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(KeeperExceptionTest.class);
	}
}