package com.uiuc.org.apache.zookeeper.server.quorum;

import org.apache.zookeeper.server.ZooKeeperServer;
import org.apache.zookeeper.server.quorum.Leader;
import org.apache.zookeeper.server.quorum.LeaderBean;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>LeaderBeanTest</code> contains tests for the class <code>{@link LeaderBean}</code>.
 *
 * @generatedBy CodePro at 10/16/13 7:58 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class LeaderBeanTest {
	/**
	 * Run the LeaderBean(Leader,ZooKeeperServer) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testLeaderBean_1()
		throws Exception {
		Leader leader = null;
		ZooKeeperServer zks = new ZooKeeperServer();

		LeaderBean result = new LeaderBean(leader, zks);

		// add additional test code here
		assertNotNull(result);
		assertEquals("Leader", result.getName());
		assertEquals("0x0", result.getCurrentZxid());
		assertEquals(false, result.isHidden());
		assertEquals("3.4.5--1, built on 10/16/2013 23:44 GMT", result.getVersion());
		assertEquals("Wed Oct 16 19:58:06 CDT 2013", result.getStartTime());
		assertEquals(-1, result.getMaxClientCnxnsPerHost());
		assertEquals(6000, result.getMinSessionTimeout());
		assertEquals(60000, result.getMaxSessionTimeout());
		assertEquals("10.9.16.66:-1", result.getClientPort());
		assertEquals(3000, result.getTickTime());
		assertEquals(0L, result.getOutstandingRequests());
		assertEquals(0L, result.getPacketsReceived());
		assertEquals(0L, result.getPacketsSent());
		assertEquals(0L, result.getMinRequestLatency());
		assertEquals(0L, result.getAvgRequestLatency());
		assertEquals(0L, result.getMaxRequestLatency());
	}

	/**
	 * Run the String followerInfo() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testFollowerInfo_1()
		throws Exception {
		LeaderBean fixture = new LeaderBean((Leader) null, new ZooKeeperServer());

		String result = fixture.followerInfo();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.LeaderBean.followerInfo(LeaderBean.java:47)
		assertNotNull(result);
	}

	/**
	 * Run the String followerInfo() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testFollowerInfo_2()
		throws Exception {
		LeaderBean fixture = new LeaderBean((Leader) null, new ZooKeeperServer());

		String result = fixture.followerInfo();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.LeaderBean.followerInfo(LeaderBean.java:47)
		assertNotNull(result);
	}

	/**
	 * Run the String getCurrentZxid() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testGetCurrentZxid_1()
		throws Exception {
		LeaderBean fixture = new LeaderBean((Leader) null, new ZooKeeperServer());

		String result = fixture.getCurrentZxid();

		// add additional test code here
		assertEquals("0x0", result);
	}

	/**
	 * Run the String getName() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testGetName_1()
		throws Exception {
		LeaderBean fixture = new LeaderBean((Leader) null, new ZooKeeperServer());

		String result = fixture.getName();

		// add additional test code here
		assertEquals("Leader", result);
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(LeaderBeanTest.class);
	}
}