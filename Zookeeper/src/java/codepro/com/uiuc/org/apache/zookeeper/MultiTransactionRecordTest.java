package com.uiuc.org.apache.zookeeper;

import java.io.ByteArrayOutputStream;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.util.Iterator;
import java.util.LinkedList;
import org.apache.jute.BinaryInputArchive;
import org.apache.jute.BinaryOutputArchive;
import org.apache.jute.InputArchive;
import org.apache.jute.OutputArchive;
import org.apache.zookeeper.MultiTransactionRecord;
import org.apache.zookeeper.Op;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>MultiTransactionRecordTest</code> contains tests for the class <code>{@link MultiTransactionRecord}</code>.
 *
 * @generatedBy CodePro at 10/16/13 7:23 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class MultiTransactionRecordTest {
	/**
	 * Run the MultiTransactionRecord() constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testMultiTransactionRecord_1()
		throws Exception {

		MultiTransactionRecord result = new MultiTransactionRecord();

		// add additional test code here
		assertNotNull(result);
		assertEquals(0, result.size());
	}

	/**
	 * Run the MultiTransactionRecord(Iterable<Op>) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testMultiTransactionRecord_2()
		throws Exception {
		Iterable<Op> ops = new LinkedList();

		MultiTransactionRecord result = new MultiTransactionRecord(ops);

		// add additional test code here
		assertNotNull(result);
		assertEquals(0, result.size());
	}

	/**
	 * Run the MultiTransactionRecord(Iterable<Op>) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testMultiTransactionRecord_3()
		throws Exception {
		Iterable<Op> ops = new LinkedList();

		MultiTransactionRecord result = new MultiTransactionRecord(ops);

		// add additional test code here
		assertNotNull(result);
		assertEquals(0, result.size());
	}

	/**
	 * Run the void add(Op) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testAdd_1()
		throws Exception {
		MultiTransactionRecord fixture = new MultiTransactionRecord();
		fixture.add(Op.check("", 0));
		Op op = Op.check("", 0);

		fixture.add(op);

		// add additional test code here
	}

	/**
	 * Run the void deserialize(InputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testDeserialize_1()
		throws Exception {
		MultiTransactionRecord fixture = new MultiTransactionRecord();
		fixture.add(Op.check("", 0));
		InputArchive archive = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));
		String tag = "";

		fixture.deserialize(archive, tag);

		// add additional test code here
	}

	/**
	 * Run the void deserialize(InputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testDeserialize_2()
		throws Exception {
		MultiTransactionRecord fixture = new MultiTransactionRecord();
		fixture.add(Op.check("", 0));
		InputArchive archive = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));
		String tag = "";

		fixture.deserialize(archive, tag);

		// add additional test code here
	}

	/**
	 * Run the void deserialize(InputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testDeserialize_3()
		throws Exception {
		MultiTransactionRecord fixture = new MultiTransactionRecord();
		fixture.add(Op.check("", 0));
		InputArchive archive = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));
		String tag = "";

		fixture.deserialize(archive, tag);

		// add additional test code here
	}

	/**
	 * Run the void deserialize(InputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testDeserialize_4()
		throws Exception {
		MultiTransactionRecord fixture = new MultiTransactionRecord();
		fixture.add(Op.check("", 0));
		InputArchive archive = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));
		String tag = "";

		fixture.deserialize(archive, tag);

		// add additional test code here
	}

	/**
	 * Run the void deserialize(InputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testDeserialize_5()
		throws Exception {
		MultiTransactionRecord fixture = new MultiTransactionRecord();
		fixture.add(Op.check("", 0));
		InputArchive archive = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));
		String tag = "";

		fixture.deserialize(archive, tag);

		// add additional test code here
	}

	/**
	 * Run the void deserialize(InputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testDeserialize_6()
		throws Exception {
		MultiTransactionRecord fixture = new MultiTransactionRecord();
		fixture.add(Op.check("", 0));
		InputArchive archive = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));
		String tag = "";

		fixture.deserialize(archive, tag);

		// add additional test code here
	}

	/**
	 * Run the void deserialize(InputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testDeserialize_7()
		throws Exception {
		MultiTransactionRecord fixture = new MultiTransactionRecord();
		fixture.add(Op.check("", 0));
		InputArchive archive = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));
		String tag = "";

		fixture.deserialize(archive, tag);

		// add additional test code here
	}

	/**
	 * Run the void deserialize(InputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testDeserialize_8()
		throws Exception {
		MultiTransactionRecord fixture = new MultiTransactionRecord();
		fixture.add(Op.check("", 0));
		InputArchive archive = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));
		String tag = "";

		fixture.deserialize(archive, tag);

		// add additional test code here
	}

	/**
	 * Run the void deserialize(InputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testDeserialize_9()
		throws Exception {
		MultiTransactionRecord fixture = new MultiTransactionRecord();
		fixture.add(Op.check("", 0));
		InputArchive archive = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));
		String tag = "";

		fixture.deserialize(archive, tag);

		// add additional test code here
	}

	/**
	 * Run the void deserialize(InputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testDeserialize_10()
		throws Exception {
		MultiTransactionRecord fixture = new MultiTransactionRecord();
		fixture.add(Op.check("", 0));
		InputArchive archive = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));
		String tag = "";

		fixture.deserialize(archive, tag);

		// add additional test code here
	}

	/**
	 * Run the void deserialize(InputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testDeserialize_11()
		throws Exception {
		MultiTransactionRecord fixture = new MultiTransactionRecord();
		fixture.add(Op.check("", 0));
		InputArchive archive = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));
		String tag = "";

		fixture.deserialize(archive, tag);

		// add additional test code here
	}

	/**
	 * Run the void deserialize(InputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testDeserialize_12()
		throws Exception {
		MultiTransactionRecord fixture = new MultiTransactionRecord();
		fixture.add(Op.check("", 0));
		InputArchive archive = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));
		String tag = "";

		fixture.deserialize(archive, tag);

		// add additional test code here
	}

	/**
	 * Run the void deserialize(InputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testDeserialize_13()
		throws Exception {
		MultiTransactionRecord fixture = new MultiTransactionRecord();
		fixture.add(Op.check("", 0));
		InputArchive archive = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));
		String tag = "";

		fixture.deserialize(archive, tag);

		// add additional test code here
	}

	/**
	 * Run the void deserialize(InputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testDeserialize_14()
		throws Exception {
		MultiTransactionRecord fixture = new MultiTransactionRecord();
		fixture.add(Op.check("", 0));
		InputArchive archive = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));
		String tag = "";

		fixture.deserialize(archive, tag);

		// add additional test code here
	}

	/**
	 * Run the boolean equals(Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testEquals_1()
		throws Exception {
		MultiTransactionRecord fixture = new MultiTransactionRecord();
		fixture.add(Op.check("", 0));
		MultiTransactionRecord o = new MultiTransactionRecord();
		o.add(Op.check("", 0));

		boolean result = fixture.equals(o);

		// add additional test code here
		assertEquals(true, result);
	}

	/**
	 * Run the boolean equals(Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testEquals_2()
		throws Exception {
		MultiTransactionRecord fixture = new MultiTransactionRecord();
		fixture.add(Op.check("", 0));
		Object o = new Object();

		boolean result = fixture.equals(o);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean equals(Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testEquals_3()
		throws Exception {
		MultiTransactionRecord fixture = new MultiTransactionRecord();
		fixture.add(Op.check("", 0));
		MultiTransactionRecord o = new MultiTransactionRecord();
		o.add(Op.check("", 0));

		boolean result = fixture.equals(o);

		// add additional test code here
		assertEquals(true, result);
	}

	/**
	 * Run the boolean equals(Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testEquals_4()
		throws Exception {
		MultiTransactionRecord fixture = new MultiTransactionRecord();
		fixture.add(Op.check("", 0));
		MultiTransactionRecord o = new MultiTransactionRecord();
		o.add(Op.check("", 0));

		boolean result = fixture.equals(o);

		// add additional test code here
		assertEquals(true, result);
	}

	/**
	 * Run the boolean equals(Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testEquals_5()
		throws Exception {
		MultiTransactionRecord fixture = new MultiTransactionRecord();
		fixture.add(Op.check("", 0));
		MultiTransactionRecord o = new MultiTransactionRecord();
		o.add(Op.check("", 0));

		boolean result = fixture.equals(o);

		// add additional test code here
		assertEquals(true, result);
	}

	/**
	 * Run the boolean equals(Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testEquals_6()
		throws Exception {
		MultiTransactionRecord fixture = new MultiTransactionRecord();
		fixture.add(Op.check("", 0));
		MultiTransactionRecord o = new MultiTransactionRecord();
		o.add(Op.check("", 0));

		boolean result = fixture.equals(o);

		// add additional test code here
		assertEquals(true, result);
	}

	/**
	 * Run the boolean equals(Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testEquals_7()
		throws Exception {
		MultiTransactionRecord fixture = new MultiTransactionRecord();
		fixture.add(Op.check("", 0));
		MultiTransactionRecord o = new MultiTransactionRecord();
		o.add(Op.check("", 0));

		boolean result = fixture.equals(o);

		// add additional test code here
		assertEquals(true, result);
	}

	/**
	 * Run the int hashCode() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testHashCode_1()
		throws Exception {
		MultiTransactionRecord fixture = new MultiTransactionRecord();
		fixture.add(Op.check("", 0));

		int result = fixture.hashCode();

		// add additional test code here
		assertEquals(25588, result);
	}

	/**
	 * Run the int hashCode() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testHashCode_2()
		throws Exception {
		MultiTransactionRecord fixture = new MultiTransactionRecord();
		fixture.add(Op.check("", 0));

		int result = fixture.hashCode();

		// add additional test code here
		assertEquals(25588, result);
	}

	/**
	 * Run the Iterator<Op> iterator() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testIterator_1()
		throws Exception {
		MultiTransactionRecord fixture = new MultiTransactionRecord();
		fixture.add(Op.check("", 0));

		Iterator<Op> result = fixture.iterator();

		// add additional test code here
		assertNotNull(result);
		assertEquals(true, result.hasNext());
	}

	/**
	 * Run the void serialize(OutputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testSerialize_1()
		throws Exception {
		MultiTransactionRecord fixture = new MultiTransactionRecord();
		fixture.add(Op.check("", 0));
		OutputArchive archive = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		String tag = "";

		fixture.serialize(archive, tag);

		// add additional test code here
	}

	/**
	 * Run the void serialize(OutputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testSerialize_2()
		throws Exception {
		MultiTransactionRecord fixture = new MultiTransactionRecord();
		fixture.add(Op.check("", 0));
		OutputArchive archive = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		String tag = "";

		fixture.serialize(archive, tag);

		// add additional test code here
	}

	/**
	 * Run the void serialize(OutputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testSerialize_3()
		throws Exception {
		MultiTransactionRecord fixture = new MultiTransactionRecord();
		fixture.add(Op.check("", 0));
		OutputArchive archive = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		String tag = "";

		fixture.serialize(archive, tag);

		// add additional test code here
	}

	/**
	 * Run the void serialize(OutputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testSerialize_4()
		throws Exception {
		MultiTransactionRecord fixture = new MultiTransactionRecord();
		fixture.add(Op.check("", 0));
		OutputArchive archive = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		String tag = "";

		fixture.serialize(archive, tag);

		// add additional test code here
	}

	/**
	 * Run the void serialize(OutputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testSerialize_5()
		throws Exception {
		MultiTransactionRecord fixture = new MultiTransactionRecord();
		fixture.add(Op.check("", 0));
		OutputArchive archive = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		String tag = "";

		fixture.serialize(archive, tag);

		// add additional test code here
	}

	/**
	 * Run the void serialize(OutputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testSerialize_6()
		throws Exception {
		MultiTransactionRecord fixture = new MultiTransactionRecord();
		fixture.add(Op.check("", 0));
		OutputArchive archive = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		String tag = "";

		fixture.serialize(archive, tag);

		// add additional test code here
	}

	/**
	 * Run the void serialize(OutputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testSerialize_7()
		throws Exception {
		MultiTransactionRecord fixture = new MultiTransactionRecord();
		fixture.add(Op.check("", 0));
		OutputArchive archive = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		String tag = "";

		fixture.serialize(archive, tag);

		// add additional test code here
	}

	/**
	 * Run the void serialize(OutputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testSerialize_8()
		throws Exception {
		MultiTransactionRecord fixture = new MultiTransactionRecord();
		fixture.add(Op.check("", 0));
		OutputArchive archive = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		String tag = "";

		fixture.serialize(archive, tag);

		// add additional test code here
	}

	/**
	 * Run the void serialize(OutputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testSerialize_9()
		throws Exception {
		MultiTransactionRecord fixture = new MultiTransactionRecord();
		fixture.add(Op.check("", 0));
		OutputArchive archive = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		String tag = "";

		fixture.serialize(archive, tag);

		// add additional test code here
	}

	/**
	 * Run the void serialize(OutputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testSerialize_10()
		throws Exception {
		MultiTransactionRecord fixture = new MultiTransactionRecord();
		fixture.add(Op.check("", 0));
		OutputArchive archive = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		String tag = "";

		fixture.serialize(archive, tag);

		// add additional test code here
	}

	/**
	 * Run the void serialize(OutputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testSerialize_11()
		throws Exception {
		MultiTransactionRecord fixture = new MultiTransactionRecord();
		fixture.add(Op.check("", 0));
		OutputArchive archive = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		String tag = "";

		fixture.serialize(archive, tag);

		// add additional test code here
	}

	/**
	 * Run the void serialize(OutputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testSerialize_12()
		throws Exception {
		MultiTransactionRecord fixture = new MultiTransactionRecord();
		fixture.add(Op.check("", 0));
		OutputArchive archive = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		String tag = "";

		fixture.serialize(archive, tag);

		// add additional test code here
	}

	/**
	 * Run the void serialize(OutputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testSerialize_13()
		throws Exception {
		MultiTransactionRecord fixture = new MultiTransactionRecord();
		fixture.add(Op.check("", 0));
		OutputArchive archive = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		String tag = "";

		fixture.serialize(archive, tag);

		// add additional test code here
	}

	/**
	 * Run the void serialize(OutputArchive,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testSerialize_14()
		throws Exception {
		MultiTransactionRecord fixture = new MultiTransactionRecord();
		fixture.add(Op.check("", 0));
		OutputArchive archive = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));
		String tag = "";

		fixture.serialize(archive, tag);

		// add additional test code here
	}

	/**
	 * Run the int size() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testSize_1()
		throws Exception {
		MultiTransactionRecord fixture = new MultiTransactionRecord();
		fixture.add(Op.check("", 0));

		int result = fixture.size();

		// add additional test code here
		assertEquals(1, result);
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(MultiTransactionRecordTest.class);
	}
}