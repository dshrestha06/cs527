package com.uiuc.org.apache.zookeeper.server.auth;

import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.server.NIOServerCnxn;
import org.apache.zookeeper.server.NIOServerCnxnFactory;
import org.apache.zookeeper.server.ServerCnxn;
import org.apache.zookeeper.server.ZooKeeperServer;
import org.apache.zookeeper.server.auth.IPAuthenticationProvider;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>IPAuthenticationProviderTest</code> contains tests for the class <code>{@link IPAuthenticationProvider}</code>.
 *
 * @generatedBy CodePro at 10/16/13 7:33 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class IPAuthenticationProviderTest {
	/**
	 * Run the String getScheme() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testGetScheme_1()
		throws Exception {
		IPAuthenticationProvider fixture = new IPAuthenticationProvider();

		String result = fixture.getScheme();

		// add additional test code here
		assertEquals("ip", result);
	}

	/**
	 * Run the org.apache.zookeeper.KeeperException.Code handleAuthentication(ServerCnxn,byte[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testHandleAuthentication_1()
		throws Exception {
		IPAuthenticationProvider fixture = new IPAuthenticationProvider();
		ServerCnxn cnxn = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		byte[] authData = new byte[] {};

		org.apache.zookeeper.KeeperException.Code result = fixture.handleAuthentication(cnxn, authData);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
		assertNotNull(result);
	}

	/**
	 * Run the boolean isAuthenticated() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testIsAuthenticated_1()
		throws Exception {
		IPAuthenticationProvider fixture = new IPAuthenticationProvider();

		boolean result = fixture.isAuthenticated();

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean isValid(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testIsValid_1()
		throws Exception {
		IPAuthenticationProvider fixture = new IPAuthenticationProvider();
		String id = "";

		boolean result = fixture.isValid(id);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean isValid(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testIsValid_2()
		throws Exception {
		IPAuthenticationProvider fixture = new IPAuthenticationProvider();
		String id = "";

		boolean result = fixture.isValid(id);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean matches(String,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testMatches_1()
		throws Exception {
		IPAuthenticationProvider fixture = new IPAuthenticationProvider();
		String id = "";
		String aclExpr = "";

		boolean result = fixture.matches(id, aclExpr);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean matches(String,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testMatches_2()
		throws Exception {
		IPAuthenticationProvider fixture = new IPAuthenticationProvider();
		String id = "a";
		String aclExpr = "";

		boolean result = fixture.matches(id, aclExpr);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean matches(String,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testMatches_3()
		throws Exception {
		IPAuthenticationProvider fixture = new IPAuthenticationProvider();
		String id = "a";
		String aclExpr = "";

		boolean result = fixture.matches(id, aclExpr);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean matches(String,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testMatches_4()
		throws Exception {
		IPAuthenticationProvider fixture = new IPAuthenticationProvider();
		String id = "";
		String aclExpr = "";

		boolean result = fixture.matches(id, aclExpr);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean matches(String,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testMatches_5()
		throws Exception {
		IPAuthenticationProvider fixture = new IPAuthenticationProvider();
		String id = "";
		String aclExpr = "";

		boolean result = fixture.matches(id, aclExpr);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean matches(String,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testMatches_6()
		throws Exception {
		IPAuthenticationProvider fixture = new IPAuthenticationProvider();
		String id = "";
		String aclExpr = "";

		boolean result = fixture.matches(id, aclExpr);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean matches(String,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testMatches_7()
		throws Exception {
		IPAuthenticationProvider fixture = new IPAuthenticationProvider();
		String id = "";
		String aclExpr = "";

		boolean result = fixture.matches(id, aclExpr);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean matches(String,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testMatches_8()
		throws Exception {
		IPAuthenticationProvider fixture = new IPAuthenticationProvider();
		String id = "";
		String aclExpr = "";

		boolean result = fixture.matches(id, aclExpr);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(IPAuthenticationProviderTest.class);
	}
}