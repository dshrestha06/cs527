package com.uiuc.org.apache.zookeeper.server.quorum;

import org.apache.zookeeper.server.quorum.StateSummary;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>StateSummaryTest</code> contains tests for the class <code>{@link StateSummary}</code>.
 *
 * @generatedBy CodePro at 10/16/13 8:16 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class StateSummaryTest {
	/**
	 * Run the StateSummary(long,long) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:16 PM
	 */
	@Test
	public void testStateSummary_1()
		throws Exception {
		long currentEpoch = 1L;
		long lastZxid = 1L;

		StateSummary result = new StateSummary(currentEpoch, lastZxid);

		// add additional test code here
		assertNotNull(result);
		assertEquals(1L, result.getLastZxid());
		assertEquals(1L, result.getCurrentEpoch());
	}

	/**
	 * Run the boolean equals(Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:16 PM
	 */
	@Test
	public void testEquals_1()
		throws Exception {
		StateSummary fixture = new StateSummary(1L, 1L);
		Object obj = new Object();

		boolean result = fixture.equals(obj);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean equals(Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:16 PM
	 */
	@Test
	public void testEquals_2()
		throws Exception {
		StateSummary fixture = new StateSummary(1L, 1L);
		Object obj = new StateSummary(1L, 1L);

		boolean result = fixture.equals(obj);

		// add additional test code here
		assertEquals(true, result);
	}

	/**
	 * Run the boolean equals(Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:16 PM
	 */
	@Test
	public void testEquals_3()
		throws Exception {
		StateSummary fixture = new StateSummary(1L, 1L);
		Object obj = new StateSummary(1L, 1L);

		boolean result = fixture.equals(obj);

		// add additional test code here
		assertEquals(true, result);
	}

	/**
	 * Run the boolean equals(Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:16 PM
	 */
	@Test
	public void testEquals_4()
		throws Exception {
		StateSummary fixture = new StateSummary(1L, 1L);
		Object obj = new StateSummary(1L, 1L);

		boolean result = fixture.equals(obj);

		// add additional test code here
		assertEquals(true, result);
	}

	/**
	 * Run the long getCurrentEpoch() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:16 PM
	 */
	@Test
	public void testGetCurrentEpoch_1()
		throws Exception {
		StateSummary fixture = new StateSummary(1L, 1L);

		long result = fixture.getCurrentEpoch();

		// add additional test code here
		assertEquals(1L, result);
	}

	/**
	 * Run the long getLastZxid() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:16 PM
	 */
	@Test
	public void testGetLastZxid_1()
		throws Exception {
		StateSummary fixture = new StateSummary(1L, 1L);

		long result = fixture.getLastZxid();

		// add additional test code here
		assertEquals(1L, result);
	}

	/**
	 * Run the int hashCode() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:16 PM
	 */
	@Test
	public void testHashCode_1()
		throws Exception {
		StateSummary fixture = new StateSummary(1L, 1L);

		int result = fixture.hashCode();

		// add additional test code here
		assertEquals(0, result);
	}

	/**
	 * Run the boolean isMoreRecentThan(StateSummary) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:16 PM
	 */
	@Test
	public void testIsMoreRecentThan_1()
		throws Exception {
		StateSummary fixture = new StateSummary(1L, 1L);
		StateSummary ss = new StateSummary(1L, 1L);

		boolean result = fixture.isMoreRecentThan(ss);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean isMoreRecentThan(StateSummary) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:16 PM
	 */
	@Test
	public void testIsMoreRecentThan_2()
		throws Exception {
		StateSummary fixture = new StateSummary(1L, 1L);
		StateSummary ss = new StateSummary(1L, 1L);

		boolean result = fixture.isMoreRecentThan(ss);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean isMoreRecentThan(StateSummary) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:16 PM
	 */
	@Test
	public void testIsMoreRecentThan_3()
		throws Exception {
		StateSummary fixture = new StateSummary(1L, 1L);
		StateSummary ss = new StateSummary(1L, 1L);

		boolean result = fixture.isMoreRecentThan(ss);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean isMoreRecentThan(StateSummary) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:16 PM
	 */
	@Test
	public void testIsMoreRecentThan_4()
		throws Exception {
		StateSummary fixture = new StateSummary(1L, 1L);
		StateSummary ss = new StateSummary(1L, 1L);

		boolean result = fixture.isMoreRecentThan(ss);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:16 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:16 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 8:16 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(StateSummaryTest.class);
	}
}