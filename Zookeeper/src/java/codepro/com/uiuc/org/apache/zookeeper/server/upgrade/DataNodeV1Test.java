package com.uiuc.org.apache.zookeeper.server.upgrade;

import static org.junit.Assert.assertNotNull;

import java.util.LinkedList;
import java.util.List;

import org.apache.zookeeper.data.ACL;
import org.apache.zookeeper.data.StatPersistedV1;
import org.apache.zookeeper.server.upgrade.DataNodeV1;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The class <code>DataNodeV1Test</code> contains tests for the class <code>{@link DataNodeV1}</code>.
 *
 * @generatedBy CodePro at 10/16/13 7:40 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class DataNodeV1Test {
	/**
	 * Run the DataNodeV1() constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test
	public void testDataNodeV1_1()
		throws Exception {

		DataNodeV1 result = new DataNodeV1();

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the DataNodeV1(DataNodeV1,byte[],List<ACL>,StatPersistedV1) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test
	public void testDataNodeV1_2()
		throws Exception {
		DataNodeV1 parent = null;
		byte[] data = new byte[] {};
		List<ACL> acl = new LinkedList();
		StatPersistedV1 stat = new StatPersistedV1();

		DataNodeV1 result = new DataNodeV1(parent, data, acl, stat);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(DataNodeV1Test.class);
	}
}