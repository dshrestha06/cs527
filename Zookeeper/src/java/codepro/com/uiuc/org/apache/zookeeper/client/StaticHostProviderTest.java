package com.uiuc.org.apache.zookeeper.client;

import java.net.InetSocketAddress;
import java.util.Collection;
import java.util.LinkedList;
import org.apache.zookeeper.client.StaticHostProvider;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>StaticHostProviderTest</code> contains tests for the class <code>{@link StaticHostProvider}</code>.
 *
 * @generatedBy CodePro at 10/16/13 7:31 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class StaticHostProviderTest {
	/**
	 * Run the StaticHostProvider(Collection<InetSocketAddress>) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testStaticHostProvider_1()
		throws Exception {
		Collection<InetSocketAddress> serverAddresses = new LinkedList();

		StaticHostProvider result = new StaticHostProvider(serverAddresses);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: A HostProvider may not be empty!
		//       at org.apache.zookeeper.client.StaticHostProvider.<init>(StaticHostProvider.java:69)
		assertNotNull(result);
	}

	/**
	 * Run the StaticHostProvider(Collection<InetSocketAddress>) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testStaticHostProvider_2()
		throws Exception {
		Collection<InetSocketAddress> serverAddresses = new LinkedList();

		StaticHostProvider result = new StaticHostProvider(serverAddresses);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: A HostProvider may not be empty!
		//       at org.apache.zookeeper.client.StaticHostProvider.<init>(StaticHostProvider.java:69)
		assertNotNull(result);
	}

	/**
	 * Run the StaticHostProvider(Collection<InetSocketAddress>) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testStaticHostProvider_3()
		throws Exception {
		Collection<InetSocketAddress> serverAddresses = new LinkedList();

		StaticHostProvider result = new StaticHostProvider(serverAddresses);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: A HostProvider may not be empty!
		//       at org.apache.zookeeper.client.StaticHostProvider.<init>(StaticHostProvider.java:69)
		assertNotNull(result);
	}

	/**
	 * Run the StaticHostProvider(Collection<InetSocketAddress>) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testStaticHostProvider_4()
		throws Exception {
		Collection<InetSocketAddress> serverAddresses = new LinkedList();

		StaticHostProvider result = new StaticHostProvider(serverAddresses);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: A HostProvider may not be empty!
		//       at org.apache.zookeeper.client.StaticHostProvider.<init>(StaticHostProvider.java:69)
		assertNotNull(result);
	}

	/**
	 * Run the StaticHostProvider(Collection<InetSocketAddress>) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testStaticHostProvider_5()
		throws Exception {
		Collection<InetSocketAddress> serverAddresses = new LinkedList();

		StaticHostProvider result = new StaticHostProvider(serverAddresses);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the InetSocketAddress next(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testNext_1()
		throws Exception {
		StaticHostProvider fixture = new StaticHostProvider(new LinkedList());
		long spinDelay = 0L;

		InetSocketAddress result = fixture.next(spinDelay);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: A HostProvider may not be empty!
		//       at org.apache.zookeeper.client.StaticHostProvider.<init>(StaticHostProvider.java:69)
		assertNotNull(result);
	}

	/**
	 * Run the InetSocketAddress next(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testNext_2()
		throws Exception {
		StaticHostProvider fixture = new StaticHostProvider(new LinkedList());
		long spinDelay = 1L;

		InetSocketAddress result = fixture.next(spinDelay);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: A HostProvider may not be empty!
		//       at org.apache.zookeeper.client.StaticHostProvider.<init>(StaticHostProvider.java:69)
		assertNotNull(result);
	}

	/**
	 * Run the InetSocketAddress next(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testNext_3()
		throws Exception {
		StaticHostProvider fixture = new StaticHostProvider(new LinkedList());
		long spinDelay = 1L;

		InetSocketAddress result = fixture.next(spinDelay);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: A HostProvider may not be empty!
		//       at org.apache.zookeeper.client.StaticHostProvider.<init>(StaticHostProvider.java:69)
		assertNotNull(result);
	}

	/**
	 * Run the void onConnected() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testOnConnected_1()
		throws Exception {
		StaticHostProvider fixture = new StaticHostProvider(new LinkedList());

		fixture.onConnected();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: A HostProvider may not be empty!
		//       at org.apache.zookeeper.client.StaticHostProvider.<init>(StaticHostProvider.java:69)
	}

	/**
	 * Run the int size() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Test
	public void testSize_1()
		throws Exception {
		StaticHostProvider fixture = new StaticHostProvider(new LinkedList());

		int result = fixture.size();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: A HostProvider may not be empty!
		//       at org.apache.zookeeper.client.StaticHostProvider.<init>(StaticHostProvider.java:69)
		assertEquals(0, result);
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:31 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(StaticHostProviderTest.class);
	}
}