package com.uiuc.org.apache.zookeeper.common;

import org.apache.zookeeper.common.PathTrie;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>PathTrieTest</code> contains tests for the class <code>{@link PathTrie}</code>.
 *
 * @generatedBy CodePro at 10/16/13 7:32 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class PathTrieTest {
	/**
	 * Run the PathTrie() constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test
	public void testPathTrie_1()
		throws Exception {

		PathTrie result = new PathTrie();

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the void addPath(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test
	public void testAddPath_1()
		throws Exception {
		PathTrie fixture = new PathTrie();
		String path = null;

		fixture.addPath(path);

		// add additional test code here
	}

	/**
	 * Run the void addPath(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test
	public void testAddPath_2()
		throws Exception {
		PathTrie fixture = new PathTrie();
		String path = "";

		fixture.addPath(path);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Invalid path 
		//       at org.apache.zookeeper.common.PathTrie.addPath(PathTrie.java:204)
	}

	/**
	 * Run the void addPath(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test
	public void testAddPath_3()
		throws Exception {
		PathTrie fixture = new PathTrie();
		String path = "";

		fixture.addPath(path);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Invalid path 
		//       at org.apache.zookeeper.common.PathTrie.addPath(PathTrie.java:204)
	}

	/**
	 * Run the void addPath(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test
	public void testAddPath_4()
		throws Exception {
		PathTrie fixture = new PathTrie();
		String path = "";

		fixture.addPath(path);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Invalid path 
		//       at org.apache.zookeeper.common.PathTrie.addPath(PathTrie.java:204)
	}

	/**
	 * Run the void addPath(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testAddPath_5()
		throws Exception {
		PathTrie fixture = new PathTrie();
		String path = "";

		fixture.addPath(path);

		// add additional test code here
	}

	/**
	 * Run the void deletePath(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test
	public void testDeletePath_1()
		throws Exception {
		PathTrie fixture = new PathTrie();
		String path = null;

		fixture.deletePath(path);

		// add additional test code here
	}

	/**
	 * Run the void deletePath(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test
	public void testDeletePath_2()
		throws Exception {
		PathTrie fixture = new PathTrie();
		String path = "";

		fixture.deletePath(path);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Invalid path 
		//       at org.apache.zookeeper.common.PathTrie.deletePath(PathTrie.java:228)
	}

	/**
	 * Run the void deletePath(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test
	public void testDeletePath_3()
		throws Exception {
		PathTrie fixture = new PathTrie();
		String path = "";

		fixture.deletePath(path);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Invalid path 
		//       at org.apache.zookeeper.common.PathTrie.deletePath(PathTrie.java:228)
	}

	/**
	 * Run the void deletePath(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test
	public void testDeletePath_4()
		throws Exception {
		PathTrie fixture = new PathTrie();
		String path = "";

		fixture.deletePath(path);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Invalid path 
		//       at org.apache.zookeeper.common.PathTrie.deletePath(PathTrie.java:228)
	}

	/**
	 * Run the void deletePath(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testDeletePath_5()
		throws Exception {
		PathTrie fixture = new PathTrie();
		String path = "";

		fixture.deletePath(path);

		// add additional test code here
	}

	/**
	 * Run the String findMaxPrefix(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test
	public void testFindMaxPrefix_1()
		throws Exception {
		PathTrie fixture = new PathTrie();
		String path = null;

		String result = fixture.findMaxPrefix(path);

		// add additional test code here
		assertEquals(null, result);
	}

	/**
	 * Run the String findMaxPrefix(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test
	public void testFindMaxPrefix_2()
		throws Exception {
		PathTrie fixture = new PathTrie();
		String path = "/";

		String result = fixture.findMaxPrefix(path);

		// add additional test code here
		assertEquals("/", result);
	}

	/**
	 * Run the String findMaxPrefix(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test
	public void testFindMaxPrefix_3()
		throws Exception {
		PathTrie fixture = new PathTrie();
		String path = "";

		String result = fixture.findMaxPrefix(path);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Invalid path 
		//       at org.apache.zookeeper.common.PathTrie.findMaxPrefix(PathTrie.java:259)
		assertNotNull(result);
	}

	/**
	 * Run the String findMaxPrefix(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test
	public void testFindMaxPrefix_4()
		throws Exception {
		PathTrie fixture = new PathTrie();
		String path = "";

		String result = fixture.findMaxPrefix(path);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Invalid path 
		//       at org.apache.zookeeper.common.PathTrie.findMaxPrefix(PathTrie.java:259)
		assertNotNull(result);
	}

	/**
	 * Run the String findMaxPrefix(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test
	public void testFindMaxPrefix_5()
		throws Exception {
		PathTrie fixture = new PathTrie();
		String path = "";

		String result = fixture.findMaxPrefix(path);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Invalid path 
		//       at org.apache.zookeeper.common.PathTrie.findMaxPrefix(PathTrie.java:259)
		assertNotNull(result);
	}

	/**
	 * Run the String findMaxPrefix(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test
	public void testFindMaxPrefix_6()
		throws Exception {
		PathTrie fixture = new PathTrie();
		String path = "";

		String result = fixture.findMaxPrefix(path);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Invalid path 
		//       at org.apache.zookeeper.common.PathTrie.findMaxPrefix(PathTrie.java:259)
		assertNotNull(result);
	}

	/**
	 * Run the String findMaxPrefix(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testFindMaxPrefix_7()
		throws Exception {
		PathTrie fixture = new PathTrie();
		String path = "";

		String result = fixture.findMaxPrefix(path);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:32 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(PathTrieTest.class);
	}
}