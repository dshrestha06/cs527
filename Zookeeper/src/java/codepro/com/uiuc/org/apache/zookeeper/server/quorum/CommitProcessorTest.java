package com.uiuc.org.apache.zookeeper.server.quorum;

import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.LinkedList;
import java.util.List;
import org.apache.zookeeper.data.Id;
import org.apache.zookeeper.server.FinalRequestProcessor;
import org.apache.zookeeper.server.NIOServerCnxn;
import org.apache.zookeeper.server.NIOServerCnxnFactory;
import org.apache.zookeeper.server.Request;
import org.apache.zookeeper.server.RequestProcessor;
import org.apache.zookeeper.server.ServerCnxn;
import org.apache.zookeeper.server.ZooKeeperServer;
import org.apache.zookeeper.server.quorum.CommitProcessor;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>CommitProcessorTest</code> contains tests for the class <code>{@link CommitProcessor}</code>.
 *
 * @generatedBy CodePro at 10/16/13 7:58 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class CommitProcessorTest {
	/**
	 * Run the CommitProcessor(RequestProcessor,String,boolean) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testCommitProcessor_1()
		throws Exception {
		RequestProcessor nextProcessor = new FinalRequestProcessor(new ZooKeeperServer());
		String id = "";
		boolean matchSyncs = true;

		CommitProcessor result = new CommitProcessor(nextProcessor, id, matchSyncs);

		// add additional test code here
		assertNotNull(result);
		assertEquals("Thread[CommitProcessor:,1,main]", result.toString());
		assertEquals(false, result.isInterrupted());
		assertEquals("CommitProcessor:", result.getName());
		assertEquals(0, result.countStackFrames());
		assertEquals(7890L, result.getId());
		assertEquals(1, result.getPriority());
		assertEquals(false, result.isAlive());
		assertEquals(true, result.isDaemon());
	}

	/**
	 * Run the void commit(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testCommit_1()
		throws Exception {
		CommitProcessor fixture = new CommitProcessor(new FinalRequestProcessor(new ZooKeeperServer()), "", true);
		Request request = null;

		fixture.commit(request);

		// add additional test code here
	}

	/**
	 * Run the void commit(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testCommit_2()
		throws Exception {
		CommitProcessor fixture = new CommitProcessor(new FinalRequestProcessor(new ZooKeeperServer()), "", true);
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());

		fixture.commit(request);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void commit(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testCommit_3()
		throws Exception {
		CommitProcessor fixture = new CommitProcessor(new FinalRequestProcessor(new ZooKeeperServer()), "", true);
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());

		fixture.commit(request);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void commit(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testCommit_4()
		throws Exception {
		CommitProcessor fixture = new CommitProcessor(new FinalRequestProcessor(new ZooKeeperServer()), "", true);
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());

		fixture.commit(request);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void processRequest(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testProcessRequest_1()
		throws Exception {
		CommitProcessor fixture = new CommitProcessor(new FinalRequestProcessor(new ZooKeeperServer()), "", true);
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());

		fixture.processRequest(request);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void processRequest(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testProcessRequest_2()
		throws Exception {
		CommitProcessor fixture = new CommitProcessor(new FinalRequestProcessor(new ZooKeeperServer()), "", true);
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());

		fixture.processRequest(request);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void processRequest(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testProcessRequest_3()
		throws Exception {
		CommitProcessor fixture = new CommitProcessor(new FinalRequestProcessor(new ZooKeeperServer()), "", true);
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());

		fixture.processRequest(request);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void processRequest(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testProcessRequest_4()
		throws Exception {
		CommitProcessor fixture = new CommitProcessor(new FinalRequestProcessor(new ZooKeeperServer()), "", true);
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());

		fixture.processRequest(request);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testRun_1()
		throws Exception {
		CommitProcessor fixture = new CommitProcessor(new FinalRequestProcessor(new ZooKeeperServer()), "", true);

		fixture.run();

		// add additional test code here
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testRun_2()
		throws Exception {
		CommitProcessor fixture = new CommitProcessor(new FinalRequestProcessor(new ZooKeeperServer()), "", true);

		fixture.run();

		// add additional test code here
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testRun_3()
		throws Exception {
		CommitProcessor fixture = new CommitProcessor(new FinalRequestProcessor(new ZooKeeperServer()), "", true);

		fixture.run();

		// add additional test code here
	}

	/**
	 * Run the void shutdown() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testShutdown_1()
		throws Exception {
		CommitProcessor fixture = new CommitProcessor(new FinalRequestProcessor(new ZooKeeperServer()), "", true);

		fixture.shutdown();

		// add additional test code here
	}

	/**
	 * Run the void shutdown() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Test
	public void testShutdown_2()
		throws Exception {
		CommitProcessor fixture = new CommitProcessor((RequestProcessor) null, "", true);

		fixture.shutdown();

		// add additional test code here
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:58 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(CommitProcessorTest.class);
	}
}