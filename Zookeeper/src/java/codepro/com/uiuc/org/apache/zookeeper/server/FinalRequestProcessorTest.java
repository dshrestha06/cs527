package com.uiuc.org.apache.zookeeper.server;

import org.apache.zookeeper.server.FinalRequestProcessor;
import org.apache.zookeeper.server.Request;
import org.apache.zookeeper.server.ZooKeeperServer;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>FinalRequestProcessorTest</code> contains tests for the class <code>{@link FinalRequestProcessor}</code>.
 *
 * @generatedBy CodePro at 10/16/13 8:18 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class FinalRequestProcessorTest {
	/**
	 * Run the FinalRequestProcessor(ZooKeeperServer) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:18 PM
	 */
	@Test
	public void testFinalRequestProcessor_1()
		throws Exception {
		ZooKeeperServer zks = new ZooKeeperServer();

		FinalRequestProcessor result = new FinalRequestProcessor(zks);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the void shutdown() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:18 PM
	 */
	@Test
	public void testShutdown_1()
		throws Exception {
		FinalRequestProcessor fixture = new FinalRequestProcessor(new ZooKeeperServer());

		fixture.shutdown();

		// add additional test code here
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:18 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:18 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 8:18 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(FinalRequestProcessorTest.class);
	}
}