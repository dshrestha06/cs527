package com.uiuc.org.apache.zookeeper;

import java.net.SocketAddress;
import java.util.LinkedList;
import java.util.List;
import org.apache.zookeeper.AsyncCallback;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.MultiTransactionRecord;
import org.apache.zookeeper.Op;
import org.apache.zookeeper.OpResult;
import org.apache.zookeeper.Transaction;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.client.ZooKeeperSaslClient;
import org.apache.zookeeper.data.ACL;
import org.apache.zookeeper.data.Stat;
import org.apache.zookeeper.test.AsyncHammerTest;
import org.apache.zookeeper.test.SyncCallTest;
import org.apache.zookeeper.test.system.SimpleClient;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>ZooKeeperTest</code> contains tests for the class <code>{@link ZooKeeper}</code>.
 *
 * @generatedBy CodePro at 10/16/13 7:29 PM
 * @author dshresth
 * @version .Revision: 1.0 .
 */
public class ZooKeeperTest {
	/**
	 * Run the ZooKeeper(String,int,Watcher) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testZooKeeper_1()
		throws Exception {
		String connectString = "";
		int sessionTimeout = 1;
		Watcher watcher = new SimpleClient();

		ZooKeeper result = new ZooKeeper(connectString, sessionTimeout, watcher);

		// add additional test code here
		assertNotNull(result);
		assertEquals("State:CONNECTING sessionid:0x0 local:null remoteserver:null lastZxid:0 xid:1 sent:0 recv:0 queuedpkts:0 pendingresp:0 queuedevents:0", result.toString());
		assertEquals(0, result.getSessionTimeout());
		assertEquals(0L, result.getSessionId());
	}

	/**
	 * Run the ZooKeeper(String,int,Watcher,boolean) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testZooKeeper_2()
		throws Exception {
		String connectString = "";
		int sessionTimeout = 1;
		Watcher watcher = new SimpleClient();
		boolean canBeReadOnly = true;

		ZooKeeper result = new ZooKeeper(connectString, sessionTimeout, watcher, canBeReadOnly);

		// add additional test code here
		assertNotNull(result);
		assertEquals("State:CONNECTING sessionid:0x0 local:null remoteserver:null lastZxid:0 xid:1 sent:0 recv:0 queuedpkts:0 pendingresp:0 queuedevents:0", result.toString());
		assertEquals(0, result.getSessionTimeout());
		assertEquals(0L, result.getSessionId());
		assertEquals(null, result.getSaslClient());
	}

	/**
	 * Run the ZooKeeper(String,int,Watcher,boolean) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testZooKeeper_3()
		throws Exception {
		String connectString = "";
		int sessionTimeout = 1;
		Watcher watcher = new SimpleClient();
		boolean canBeReadOnly = true;

		ZooKeeper result = new ZooKeeper(connectString, sessionTimeout, watcher, canBeReadOnly);

		// add additional test code here
		assertNotNull(result);
		assertEquals("State:CONNECTING sessionid:0x0 local:null remoteserver:null lastZxid:0 xid:1 sent:0 recv:0 queuedpkts:0 pendingresp:0 queuedevents:0", result.toString());
		assertEquals(0, result.getSessionTimeout());
		assertEquals(0L, result.getSessionId());
		assertEquals(null, result.getSaslClient());
	}

	/**
	 * Run the ZooKeeper(String,int,Watcher,boolean) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testZooKeeper_4()
		throws Exception {
		String connectString = "";
		int sessionTimeout = 1;
		Watcher watcher = new SimpleClient();
		boolean canBeReadOnly = true;

		ZooKeeper result = new ZooKeeper(connectString, sessionTimeout, watcher, canBeReadOnly);

		// add additional test code here
		assertNotNull(result);
		assertEquals("State:CONNECTING sessionid:0x0 local:null remoteserver:null lastZxid:0 xid:1 sent:0 recv:0 queuedpkts:0 pendingresp:0 queuedevents:0", result.toString());
		assertEquals(0, result.getSessionTimeout());
		assertEquals(0L, result.getSessionId());
		assertEquals(null, result.getSaslClient());
	}

	/**
	 * Run the ZooKeeper(String,int,Watcher,boolean) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testZooKeeper_5()
		throws Exception {
		String connectString = "";
		int sessionTimeout = 1;
		Watcher watcher = new SimpleClient();
		boolean canBeReadOnly = true;

		ZooKeeper result = new ZooKeeper(connectString, sessionTimeout, watcher, canBeReadOnly);

		// add additional test code here
		assertNotNull(result);
		assertEquals("State:CONNECTING sessionid:0x0 local:null remoteserver:null lastZxid:0 xid:1 sent:0 recv:0 queuedpkts:0 pendingresp:0 queuedevents:0", result.toString());
		assertEquals(0, result.getSessionTimeout());
		assertEquals(0L, result.getSessionId());
		assertEquals(null, result.getSaslClient());
	}

	/**
	 * Run the ZooKeeper(String,int,Watcher,long,byte[]) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testZooKeeper_6()
		throws Exception {
		String connectString = "";
		int sessionTimeout = 1;
		Watcher watcher = new SimpleClient();
		long sessionId = 1L;
		byte[] sessionPasswd = new byte[] {};

		ZooKeeper result = new ZooKeeper(connectString, sessionTimeout, watcher, sessionId, sessionPasswd);

		// add additional test code here
		assertNotNull(result);
		assertEquals("State:CONNECTING sessionid:0x1 local:null remoteserver:null lastZxid:0 xid:1 sent:0 recv:0 queuedpkts:0 pendingresp:0 queuedevents:0", result.toString());
		assertEquals(0, result.getSessionTimeout());
		assertEquals(1L, result.getSessionId());
		assertEquals(null, result.getSaslClient());
	}

	/**
	 * Run the ZooKeeper(String,int,Watcher,long,byte[],boolean) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testZooKeeper_7()
		throws Exception {
		String connectString = "";
		int sessionTimeout = 1;
		Watcher watcher = new SimpleClient();
		long sessionId = 1L;
		byte[] sessionPasswd = new byte[] {};
		boolean canBeReadOnly = true;

		ZooKeeper result = new ZooKeeper(connectString, sessionTimeout, watcher, sessionId, sessionPasswd, canBeReadOnly);

		// add additional test code here
		assertNotNull(result);
		assertEquals("State:CONNECTING sessionid:0x1 local:null remoteserver:null lastZxid:0 xid:1 sent:0 recv:0 queuedpkts:0 pendingresp:0 queuedevents:0", result.toString());
		assertEquals(0, result.getSessionTimeout());
		assertEquals(1L, result.getSessionId());
		assertEquals(null, result.getSaslClient());
	}

	/**
	 * Run the ZooKeeper(String,int,Watcher,long,byte[],boolean) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testZooKeeper_8()
		throws Exception {
		String connectString = "";
		int sessionTimeout = 1;
		Watcher watcher = new SimpleClient();
		long sessionId = 1L;
		byte[] sessionPasswd = new byte[] {};
		boolean canBeReadOnly = true;

		ZooKeeper result = new ZooKeeper(connectString, sessionTimeout, watcher, sessionId, sessionPasswd, canBeReadOnly);

		// add additional test code here
		assertNotNull(result);
		assertEquals("State:CONNECTING sessionid:0x1 local:null remoteserver:null lastZxid:0 xid:1 sent:0 recv:0 queuedpkts:0 pendingresp:0 queuedevents:0", result.toString());
		assertEquals(0, result.getSessionTimeout());
		assertEquals(1L, result.getSessionId());
		assertEquals(null, result.getSaslClient());
	}

	/**
	 * Run the ZooKeeper(String,int,Watcher,long,byte[],boolean) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testZooKeeper_9()
		throws Exception {
		String connectString = "";
		int sessionTimeout = 1;
		Watcher watcher = new SimpleClient();
		long sessionId = 1L;
		byte[] sessionPasswd = new byte[] {};
		boolean canBeReadOnly = true;

		ZooKeeper result = new ZooKeeper(connectString, sessionTimeout, watcher, sessionId, sessionPasswd, canBeReadOnly);

		// add additional test code here
		assertNotNull(result);
		assertEquals("State:CONNECTING sessionid:0x1 local:null remoteserver:null lastZxid:0 xid:1 sent:0 recv:0 queuedpkts:0 pendingresp:0 queuedevents:0", result.toString());
		assertEquals(0, result.getSessionTimeout());
		assertEquals(1L, result.getSessionId());
		assertEquals(null, result.getSaslClient());
	}

	/**
	 * Run the ZooKeeper(String,int,Watcher,long,byte[],boolean) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testZooKeeper_10()
		throws Exception {
		String connectString = "";
		int sessionTimeout = 1;
		Watcher watcher = new SimpleClient();
		long sessionId = 1L;
		byte[] sessionPasswd = new byte[] {};
		boolean canBeReadOnly = true;

		ZooKeeper result = new ZooKeeper(connectString, sessionTimeout, watcher, sessionId, sessionPasswd, canBeReadOnly);

		// add additional test code here
		assertNotNull(result);
		assertEquals("State:CONNECTING sessionid:0x1 local:null remoteserver:null lastZxid:0 xid:1 sent:0 recv:0 queuedpkts:0 pendingresp:0 queuedevents:0", result.toString());
		assertEquals(0, result.getSessionTimeout());
		assertEquals(1L, result.getSessionId());
		assertEquals(null, result.getSaslClient());
	}

	/**
	 * Run the void addAuthInfo(String,byte[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testAddAuthInfo_1()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String scheme = "";
		byte[] auth = new byte[] {};

		fixture.addAuthInfo(scheme, auth);

		// add additional test code here
	}

	/**
	 * Run the void close() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testClose_1()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());

		fixture.close();

		// add additional test code here
	}

	/**
	 * Run the void close() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testClose_2()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());

		fixture.close();

		// add additional test code here
	}

	/**
	 * Run the void close() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testClose_3()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());

		fixture.close();

		// add additional test code here
	}

	/**
	 * Run the void close() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testClose_4()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());

		fixture.close();

		// add additional test code here
	}

	/**
	 * Run the void close() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testClose_5()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());

		fixture.close();

		// add additional test code here
	}

	/**
	 * Run the void close() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testClose_6()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());

		fixture.close();

		// add additional test code here
	}

	/**
	 * Run the void close() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testClose_7()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());

		fixture.close();

		// add additional test code here
	}

	/**
	 * Run the void close() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testClose_8()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());

		fixture.close();

		// add additional test code here
	}

	/**
	 * Run the String create(String,byte[],List<ACL>,CreateMode) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testCreate_1()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		byte[] data = new byte[] {};
		List<ACL> acl = new LinkedList();
		CreateMode createMode = CreateMode.EPHEMERAL;

		String result = fixture.create(path, data, acl, createMode);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:35)
		//       at org.apache.zookeeper.ZooKeeper.create(ZooKeeper.java:766)
		assertNotNull(result);
	}

	/**
	 * Run the String create(String,byte[],List<ACL>,CreateMode) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testCreate_2()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		byte[] data = new byte[] {};
		List<ACL> acl = new LinkedList();
		CreateMode createMode = CreateMode.EPHEMERAL;

		String result = fixture.create(path, data, acl, createMode);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:35)
		//       at org.apache.zookeeper.ZooKeeper.create(ZooKeeper.java:766)
		assertNotNull(result);
	}

	/**
	 * Run the String create(String,byte[],List<ACL>,CreateMode) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testCreate_3()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		byte[] data = new byte[] {};
		List<ACL> acl = new LinkedList();
		CreateMode createMode = CreateMode.EPHEMERAL;

		String result = fixture.create(path, data, acl, createMode);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:35)
		//       at org.apache.zookeeper.ZooKeeper.create(ZooKeeper.java:766)
		assertNotNull(result);
	}

	/**
	 * Run the String create(String,byte[],List<ACL>,CreateMode) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testCreate_4()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		byte[] data = new byte[] {};
		List<ACL> acl = new LinkedList();
		CreateMode createMode = CreateMode.EPHEMERAL;

		String result = fixture.create(path, data, acl, createMode);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:35)
		//       at org.apache.zookeeper.ZooKeeper.create(ZooKeeper.java:766)
		assertNotNull(result);
	}

	/**
	 * Run the String create(String,byte[],List<ACL>,CreateMode) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testCreate_5()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		byte[] data = new byte[] {};
		List<ACL> acl = null;
		CreateMode createMode = CreateMode.EPHEMERAL;

		String result = fixture.create(path, data, acl, createMode);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:35)
		//       at org.apache.zookeeper.ZooKeeper.create(ZooKeeper.java:766)
		assertNotNull(result);
	}

	/**
	 * Run the String create(String,byte[],List<ACL>,CreateMode) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testCreate_6()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		byte[] data = new byte[] {};
		List<ACL> acl = new LinkedList();
		CreateMode createMode = CreateMode.EPHEMERAL;

		String result = fixture.create(path, data, acl, createMode);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the void create(String,byte[],List<ACL>,CreateMode,StringCallback,Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testCreate_7()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		byte[] data = new byte[] {};
		List<ACL> acl = new LinkedList();
		CreateMode createMode = CreateMode.EPHEMERAL;
		org.apache.zookeeper.AsyncCallback.StringCallback cb = new SimpleClient();
		Object ctx = new Object();

		fixture.create(path, data, acl, createMode, cb, ctx);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:35)
		//       at org.apache.zookeeper.ZooKeeper.create(ZooKeeper.java:803)
	}

	/**
	 * Run the void create(String,byte[],List<ACL>,CreateMode,StringCallback,Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testCreate_8()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		byte[] data = new byte[] {};
		List<ACL> acl = new LinkedList();
		CreateMode createMode = CreateMode.EPHEMERAL;
		org.apache.zookeeper.AsyncCallback.StringCallback cb = new SimpleClient();
		Object ctx = new Object();

		fixture.create(path, data, acl, createMode, cb, ctx);

		// add additional test code here
	}

	/**
	 * Run the void delete(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testDelete_1()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		int version = 1;

		fixture.delete(path, version);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.delete(ZooKeeper.java:851)
	}

	/**
	 * Run the void delete(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testDelete_2()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		int version = 1;

		fixture.delete(path, version);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.delete(ZooKeeper.java:851)
	}

	/**
	 * Run the void delete(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testDelete_3()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		int version = 1;

		fixture.delete(path, version);

		// add additional test code here
	}

	/**
	 * Run the void delete(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = org.apache.zookeeper.KeeperException.ConnectionLossException.class)
	public void testDelete_4()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "/";
		int version = 1;

		fixture.delete(path, version);

		// add additional test code here
	}

	/**
	 * Run the void delete(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = org.apache.zookeeper.KeeperException.ConnectionLossException.class)
	public void testDelete_5()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "/";
		int version = 1;

		fixture.delete(path, version);

		// add additional test code here
	}

	/**
	 * Run the void delete(String,int,VoidCallback,Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testDelete_6()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		int version = 1;
		org.apache.zookeeper.AsyncCallback.VoidCallback cb = new AsyncHammerTest();
		Object ctx = new Object();

		fixture.delete(path, version, cb, ctx);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.delete(ZooKeeper.java:975)
	}

	/**
	 * Run the void delete(String,int,VoidCallback,Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testDelete_7()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "/";
		int version = 1;
		org.apache.zookeeper.AsyncCallback.VoidCallback cb = new AsyncHammerTest();
		Object ctx = new Object();

		fixture.delete(path, version, cb, ctx);

		// add additional test code here
	}

	/**
	 * Run the void delete(String,int,VoidCallback,Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testDelete_8()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		int version = 1;
		org.apache.zookeeper.AsyncCallback.VoidCallback cb = new AsyncHammerTest();
		Object ctx = new Object();

		fixture.delete(path, version, cb, ctx);

		// add additional test code here
	}

	/**
	 * Run the Stat exists(String,Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testExists_1()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		Watcher watcher = new SimpleClient();

		Stat result = fixture.exists(path, watcher);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.exists(ZooKeeper.java:1020)
		assertNotNull(result);
	}

	/**
	 * Run the Stat exists(String,Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testExists_2()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		Watcher watcher = null;

		Stat result = fixture.exists(path, watcher);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.exists(ZooKeeper.java:1020)
		assertNotNull(result);
	}

	/**
	 * Run the Stat exists(String,Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testExists_3()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		Watcher watcher = new SimpleClient();

		Stat result = fixture.exists(path, watcher);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.exists(ZooKeeper.java:1020)
		assertNotNull(result);
	}

	/**
	 * Run the Stat exists(String,Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testExists_4()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		Watcher watcher = null;

		Stat result = fixture.exists(path, watcher);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.exists(ZooKeeper.java:1020)
		assertNotNull(result);
	}

	/**
	 * Run the Stat exists(String,Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testExists_5()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		Watcher watcher = new SimpleClient();

		Stat result = fixture.exists(path, watcher);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.exists(ZooKeeper.java:1020)
		assertNotNull(result);
	}

	/**
	 * Run the Stat exists(String,Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testExists_6()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		Watcher watcher = new SimpleClient();

		Stat result = fixture.exists(path, watcher);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the Stat exists(String,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testExists_7()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		boolean watch = false;

		Stat result = fixture.exists(path, watch);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.exists(ZooKeeper.java:1020)
		//       at org.apache.zookeeper.ZooKeeper.exists(ZooKeeper.java:1069)
		assertNotNull(result);
	}

	/**
	 * Run the Stat exists(String,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testExists_8()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		boolean watch = true;

		Stat result = fixture.exists(path, watch);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.exists(ZooKeeper.java:1020)
		//       at org.apache.zookeeper.ZooKeeper.exists(ZooKeeper.java:1069)
		assertNotNull(result);
	}

	/**
	 * Run the Stat exists(String,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testExists_9()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		boolean watch = true;

		Stat result = fixture.exists(path, watch);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.exists(ZooKeeper.java:1020)
		//       at org.apache.zookeeper.ZooKeeper.exists(ZooKeeper.java:1069)
		assertNotNull(result);
	}

	/**
	 * Run the void exists(String,Watcher,StatCallback,Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testExists_10()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		Watcher watcher = new SimpleClient();
		org.apache.zookeeper.AsyncCallback.StatCallback cb = new SimpleClient();
		Object ctx = new Object();

		fixture.exists(path, watcher, cb, ctx);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.exists(ZooKeeper.java:1081)
	}

	/**
	 * Run the void exists(String,Watcher,StatCallback,Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testExists_11()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		Watcher watcher = null;
		org.apache.zookeeper.AsyncCallback.StatCallback cb = new SimpleClient();
		Object ctx = new Object();

		fixture.exists(path, watcher, cb, ctx);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.exists(ZooKeeper.java:1081)
	}

	/**
	 * Run the void exists(String,Watcher,StatCallback,Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testExists_12()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		Watcher watcher = new SimpleClient();
		org.apache.zookeeper.AsyncCallback.StatCallback cb = new SimpleClient();
		Object ctx = new Object();

		fixture.exists(path, watcher, cb, ctx);

		// add additional test code here
	}

	/**
	 * Run the void exists(String,boolean,StatCallback,Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testExists_13()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		boolean watch = false;
		org.apache.zookeeper.AsyncCallback.StatCallback cb = new SimpleClient();
		Object ctx = new Object();

		fixture.exists(path, watch, cb, ctx);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.exists(ZooKeeper.java:1081)
		//       at org.apache.zookeeper.ZooKeeper.exists(ZooKeeper.java:1107)
	}

	/**
	 * Run the void exists(String,boolean,StatCallback,Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testExists_14()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		boolean watch = true;
		org.apache.zookeeper.AsyncCallback.StatCallback cb = new SimpleClient();
		Object ctx = new Object();

		fixture.exists(path, watch, cb, ctx);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.exists(ZooKeeper.java:1081)
		//       at org.apache.zookeeper.ZooKeeper.exists(ZooKeeper.java:1107)
	}

	/**
	 * Run the List<ACL> getACL(String,Stat) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testGetACL_1()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		Stat stat = new Stat();

		List<ACL> result = fixture.getACL(path, stat);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.getACL(ZooKeeper.java:1315)
		assertNotNull(result);
	}

	/**
	 * Run the List<ACL> getACL(String,Stat) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testGetACL_2()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		Stat stat = new Stat();

		List<ACL> result = fixture.getACL(path, stat);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.getACL(ZooKeeper.java:1315)
		assertNotNull(result);
	}

	/**
	 * Run the List<ACL> getACL(String,Stat) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testGetACL_3()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		Stat stat = new Stat();

		List<ACL> result = fixture.getACL(path, stat);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.getACL(ZooKeeper.java:1315)
		assertNotNull(result);
	}

	/**
	 * Run the List<ACL> getACL(String,Stat) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testGetACL_4()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		Stat stat = new Stat();

		List<ACL> result = fixture.getACL(path, stat);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the void getACL(String,Stat,ACLCallback,Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testGetACL_5()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		Stat stat = new Stat();
		org.apache.zookeeper.AsyncCallback.ACLCallback cb = null;
		Object ctx = new Object();

		fixture.getACL(path, stat, cb, ctx);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.getACL(ZooKeeper.java:1342)
	}

	/**
	 * Run the void getACL(String,Stat,ACLCallback,Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testGetACL_6()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		Stat stat = new Stat();
		org.apache.zookeeper.AsyncCallback.ACLCallback cb = null;
		Object ctx = new Object();

		fixture.getACL(path, stat, cb, ctx);

		// add additional test code here
	}

	/**
	 * Run the List<String> getChildWatches() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testGetChildWatches_1()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());

		List<String> result = fixture.getChildWatches();

		// add additional test code here
		assertNotNull(result);
		assertEquals(0, result.size());
	}

	/**
	 * Run the List<String> getChildren(String,Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testGetChildren_1()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		Watcher watcher = new SimpleClient();

		List<String> result = fixture.getChildren(path, watcher);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.getChildren(ZooKeeper.java:1450)
		assertNotNull(result);
	}

	/**
	 * Run the List<String> getChildren(String,Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testGetChildren_2()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		Watcher watcher = null;

		List<String> result = fixture.getChildren(path, watcher);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.getChildren(ZooKeeper.java:1450)
		assertNotNull(result);
	}

	/**
	 * Run the List<String> getChildren(String,Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testGetChildren_3()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		Watcher watcher = new SimpleClient();

		List<String> result = fixture.getChildren(path, watcher);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.getChildren(ZooKeeper.java:1450)
		assertNotNull(result);
	}

	/**
	 * Run the List<String> getChildren(String,Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testGetChildren_4()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		Watcher watcher = new SimpleClient();

		List<String> result = fixture.getChildren(path, watcher);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the List<String> getChildren(String,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testGetChildren_5()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		boolean watch = false;

		List<String> result = fixture.getChildren(path, watch);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.getChildren(ZooKeeper.java:1450)
		//       at org.apache.zookeeper.ZooKeeper.getChildren(ZooKeeper.java:1496)
		assertNotNull(result);
	}

	/**
	 * Run the List<String> getChildren(String,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testGetChildren_6()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		boolean watch = true;

		List<String> result = fixture.getChildren(path, watch);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.getChildren(ZooKeeper.java:1450)
		//       at org.apache.zookeeper.ZooKeeper.getChildren(ZooKeeper.java:1496)
		assertNotNull(result);
	}

	/**
	 * Run the List<String> getChildren(String,boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testGetChildren_7()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		boolean watch = true;

		List<String> result = fixture.getChildren(path, watch);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.getChildren(ZooKeeper.java:1450)
		//       at org.apache.zookeeper.ZooKeeper.getChildren(ZooKeeper.java:1496)
		assertNotNull(result);
	}

	/**
	 * Run the List<String> getChildren(String,Watcher,Stat) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testGetChildren_8()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		Watcher watcher = new SimpleClient();
		Stat stat = new Stat();

		List<String> result = fixture.getChildren(path, watcher, stat);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.getChildren(ZooKeeper.java:1568)
		assertNotNull(result);
	}

	/**
	 * Run the List<String> getChildren(String,Watcher,Stat) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testGetChildren_9()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		Watcher watcher = null;
		Stat stat = new Stat();

		List<String> result = fixture.getChildren(path, watcher, stat);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.getChildren(ZooKeeper.java:1568)
		assertNotNull(result);
	}

	/**
	 * Run the List<String> getChildren(String,Watcher,Stat) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testGetChildren_10()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		Watcher watcher = new SimpleClient();
		Stat stat = new Stat();

		List<String> result = fixture.getChildren(path, watcher, stat);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.getChildren(ZooKeeper.java:1568)
		assertNotNull(result);
	}

	/**
	 * Run the List<String> getChildren(String,Watcher,Stat) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testGetChildren_11()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		Watcher watcher = null;
		Stat stat = null;

		List<String> result = fixture.getChildren(path, watcher, stat);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.getChildren(ZooKeeper.java:1568)
		assertNotNull(result);
	}

	/**
	 * Run the List<String> getChildren(String,Watcher,Stat) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testGetChildren_12()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		Watcher watcher = new SimpleClient();
		Stat stat = new Stat();

		List<String> result = fixture.getChildren(path, watcher, stat);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the List<String> getChildren(String,boolean,Stat) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testGetChildren_13()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		boolean watch = false;
		Stat stat = new Stat();

		List<String> result = fixture.getChildren(path, watch, stat);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.getChildren(ZooKeeper.java:1568)
		//       at org.apache.zookeeper.ZooKeeper.getChildren(ZooKeeper.java:1621)
		assertNotNull(result);
	}

	/**
	 * Run the List<String> getChildren(String,boolean,Stat) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testGetChildren_14()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		boolean watch = true;
		Stat stat = new Stat();

		List<String> result = fixture.getChildren(path, watch, stat);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.getChildren(ZooKeeper.java:1568)
		//       at org.apache.zookeeper.ZooKeeper.getChildren(ZooKeeper.java:1621)
		assertNotNull(result);
	}

	/**
	 * Run the List<String> getChildren(String,boolean,Stat) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testGetChildren_15()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		boolean watch = true;
		Stat stat = new Stat();

		List<String> result = fixture.getChildren(path, watch, stat);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.getChildren(ZooKeeper.java:1568)
		//       at org.apache.zookeeper.ZooKeeper.getChildren(ZooKeeper.java:1621)
		assertNotNull(result);
	}

	/**
	 * Run the void getChildren(String,Watcher,Children2Callback,Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testGetChildren_16()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		Watcher watcher = new SimpleClient();
		org.apache.zookeeper.AsyncCallback.Children2Callback cb = new SyncCallTest();
		Object ctx = new Object();

		fixture.getChildren(path, watcher, cb, ctx);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.getChildren(ZooKeeper.java:1636)
	}

	/**
	 * Run the void getChildren(String,Watcher,Children2Callback,Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testGetChildren_17()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		Watcher watcher = null;
		org.apache.zookeeper.AsyncCallback.Children2Callback cb = new SyncCallTest();
		Object ctx = new Object();

		fixture.getChildren(path, watcher, cb, ctx);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.getChildren(ZooKeeper.java:1636)
	}

	/**
	 * Run the void getChildren(String,Watcher,Children2Callback,Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testGetChildren_18()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		Watcher watcher = new SimpleClient();
		org.apache.zookeeper.AsyncCallback.Children2Callback cb = new SyncCallTest();
		Object ctx = new Object();

		fixture.getChildren(path, watcher, cb, ctx);

		// add additional test code here
	}

	/**
	 * Run the void getChildren(String,Watcher,ChildrenCallback,Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testGetChildren_19()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		Watcher watcher = new SimpleClient();
		org.apache.zookeeper.AsyncCallback.ChildrenCallback cb = new SyncCallTest();
		Object ctx = new Object();

		fixture.getChildren(path, watcher, cb, ctx);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.getChildren(ZooKeeper.java:1508)
	}

	/**
	 * Run the void getChildren(String,Watcher,ChildrenCallback,Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testGetChildren_20()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		Watcher watcher = null;
		org.apache.zookeeper.AsyncCallback.ChildrenCallback cb = new SyncCallTest();
		Object ctx = new Object();

		fixture.getChildren(path, watcher, cb, ctx);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.getChildren(ZooKeeper.java:1508)
	}

	/**
	 * Run the void getChildren(String,Watcher,ChildrenCallback,Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testGetChildren_21()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		Watcher watcher = new SimpleClient();
		org.apache.zookeeper.AsyncCallback.ChildrenCallback cb = new SyncCallTest();
		Object ctx = new Object();

		fixture.getChildren(path, watcher, cb, ctx);

		// add additional test code here
	}

	/**
	 * Run the void getChildren(String,boolean,Children2Callback,Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testGetChildren_22()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		boolean watch = false;
		org.apache.zookeeper.AsyncCallback.Children2Callback cb = new SyncCallTest();
		Object ctx = new Object();

		fixture.getChildren(path, watch, cb, ctx);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.getChildren(ZooKeeper.java:1636)
		//       at org.apache.zookeeper.ZooKeeper.getChildren(ZooKeeper.java:1666)
	}

	/**
	 * Run the void getChildren(String,boolean,Children2Callback,Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testGetChildren_23()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		boolean watch = true;
		org.apache.zookeeper.AsyncCallback.Children2Callback cb = new SyncCallTest();
		Object ctx = new Object();

		fixture.getChildren(path, watch, cb, ctx);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.getChildren(ZooKeeper.java:1636)
		//       at org.apache.zookeeper.ZooKeeper.getChildren(ZooKeeper.java:1666)
	}

	/**
	 * Run the void getChildren(String,boolean,ChildrenCallback,Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testGetChildren_24()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		boolean watch = false;
		org.apache.zookeeper.AsyncCallback.ChildrenCallback cb = new SyncCallTest();
		Object ctx = new Object();

		fixture.getChildren(path, watch, cb, ctx);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.getChildren(ZooKeeper.java:1508)
		//       at org.apache.zookeeper.ZooKeeper.getChildren(ZooKeeper.java:1536)
	}

	/**
	 * Run the void getChildren(String,boolean,ChildrenCallback,Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testGetChildren_25()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		boolean watch = true;
		org.apache.zookeeper.AsyncCallback.ChildrenCallback cb = new SyncCallTest();
		Object ctx = new Object();

		fixture.getChildren(path, watch, cb, ctx);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.getChildren(ZooKeeper.java:1508)
		//       at org.apache.zookeeper.ZooKeeper.getChildren(ZooKeeper.java:1536)
	}

	/**
	 * Run the byte[] getData(String,Watcher,Stat) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testGetData_1()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		Watcher watcher = new SimpleClient();
		Stat stat = new Stat();

		byte[] result = fixture.getData(path, watcher, stat);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.getData(ZooKeeper.java:1133)
		assertNotNull(result);
	}

	/**
	 * Run the byte[] getData(String,Watcher,Stat) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testGetData_2()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		Watcher watcher = null;
		Stat stat = new Stat();

		byte[] result = fixture.getData(path, watcher, stat);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.getData(ZooKeeper.java:1133)
		assertNotNull(result);
	}

	/**
	 * Run the byte[] getData(String,Watcher,Stat) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testGetData_3()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		Watcher watcher = new SimpleClient();
		Stat stat = new Stat();

		byte[] result = fixture.getData(path, watcher, stat);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.getData(ZooKeeper.java:1133)
		assertNotNull(result);
	}

	/**
	 * Run the byte[] getData(String,Watcher,Stat) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testGetData_4()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		Watcher watcher = null;
		Stat stat = null;

		byte[] result = fixture.getData(path, watcher, stat);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.getData(ZooKeeper.java:1133)
		assertNotNull(result);
	}

	/**
	 * Run the byte[] getData(String,Watcher,Stat) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testGetData_5()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		Watcher watcher = new SimpleClient();
		Stat stat = new Stat();

		byte[] result = fixture.getData(path, watcher, stat);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the byte[] getData(String,boolean,Stat) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testGetData_6()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		boolean watch = false;
		Stat stat = new Stat();

		byte[] result = fixture.getData(path, watch, stat);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.getData(ZooKeeper.java:1133)
		//       at org.apache.zookeeper.ZooKeeper.getData(ZooKeeper.java:1180)
		assertNotNull(result);
	}

	/**
	 * Run the byte[] getData(String,boolean,Stat) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testGetData_7()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		boolean watch = true;
		Stat stat = new Stat();

		byte[] result = fixture.getData(path, watch, stat);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.getData(ZooKeeper.java:1133)
		//       at org.apache.zookeeper.ZooKeeper.getData(ZooKeeper.java:1180)
		assertNotNull(result);
	}

	/**
	 * Run the byte[] getData(String,boolean,Stat) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testGetData_8()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		boolean watch = true;
		Stat stat = new Stat();

		byte[] result = fixture.getData(path, watch, stat);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.getData(ZooKeeper.java:1133)
		//       at org.apache.zookeeper.ZooKeeper.getData(ZooKeeper.java:1180)
		assertNotNull(result);
	}

	/**
	 * Run the void getData(String,Watcher,DataCallback,Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testGetData_9()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		Watcher watcher = new SimpleClient();
		org.apache.zookeeper.AsyncCallback.DataCallback cb = new SimpleClient();
		Object ctx = new Object();

		fixture.getData(path, watcher, cb, ctx);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.getData(ZooKeeper.java:1192)
	}

	/**
	 * Run the void getData(String,Watcher,DataCallback,Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testGetData_10()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		Watcher watcher = null;
		org.apache.zookeeper.AsyncCallback.DataCallback cb = new SimpleClient();
		Object ctx = new Object();

		fixture.getData(path, watcher, cb, ctx);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.getData(ZooKeeper.java:1192)
	}

	/**
	 * Run the void getData(String,Watcher,DataCallback,Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testGetData_11()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		Watcher watcher = new SimpleClient();
		org.apache.zookeeper.AsyncCallback.DataCallback cb = new SimpleClient();
		Object ctx = new Object();

		fixture.getData(path, watcher, cb, ctx);

		// add additional test code here
	}

	/**
	 * Run the void getData(String,boolean,DataCallback,Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testGetData_12()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		boolean watch = false;
		org.apache.zookeeper.AsyncCallback.DataCallback cb = new SimpleClient();
		Object ctx = new Object();

		fixture.getData(path, watch, cb, ctx);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.getData(ZooKeeper.java:1192)
		//       at org.apache.zookeeper.ZooKeeper.getData(ZooKeeper.java:1218)
	}

	/**
	 * Run the void getData(String,boolean,DataCallback,Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testGetData_13()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		boolean watch = true;
		org.apache.zookeeper.AsyncCallback.DataCallback cb = new SimpleClient();
		Object ctx = new Object();

		fixture.getData(path, watch, cb, ctx);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.getData(ZooKeeper.java:1192)
		//       at org.apache.zookeeper.ZooKeeper.getData(ZooKeeper.java:1218)
	}

	/**
	 * Run the List<String> getDataWatches() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testGetDataWatches_1()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());

		List<String> result = fixture.getDataWatches();

		// add additional test code here
		assertNotNull(result);
		assertEquals(0, result.size());
	}

	/**
	 * Run the List<String> getExistWatches() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testGetExistWatches_1()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());

		List<String> result = fixture.getExistWatches();

		// add additional test code here
		assertNotNull(result);
		assertEquals(0, result.size());
	}

	/**
	 * Run the ZooKeeperSaslClient getSaslClient() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testGetSaslClient_1()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());

		ZooKeeperSaslClient result = fixture.getSaslClient();

		// add additional test code here
		assertEquals(null, result);
	}

	/**
	 * Run the long getSessionId() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testGetSessionId_1()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());

		long result = fixture.getSessionId();

		// add additional test code here
		assertEquals(0L, result);
	}

	/**
	 * Run the byte[] getSessionPasswd() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testGetSessionPasswd_1()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());

		byte[] result = fixture.getSessionPasswd();

		// add additional test code here
		assertNotNull(result);
		assertEquals(16, result.length);
		assertEquals((byte) 0, result[0]);
		assertEquals((byte) 0, result[1]);
		assertEquals((byte) 0, result[2]);
		assertEquals((byte) 0, result[3]);
		assertEquals((byte) 0, result[4]);
		assertEquals((byte) 0, result[5]);
		assertEquals((byte) 0, result[6]);
		assertEquals((byte) 0, result[7]);
		assertEquals((byte) 0, result[8]);
		assertEquals((byte) 0, result[9]);
		assertEquals((byte) 0, result[10]);
		assertEquals((byte) 0, result[11]);
		assertEquals((byte) 0, result[12]);
		assertEquals((byte) 0, result[13]);
		assertEquals((byte) 0, result[14]);
		assertEquals((byte) 0, result[15]);
	}

	/**
	 * Run the int getSessionTimeout() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testGetSessionTimeout_1()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());

		int result = fixture.getSessionTimeout();

		// add additional test code here
		assertEquals(0, result);
	}

	/**
	 * Run the org.apache.zookeeper.ZooKeeper.States getState() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testGetState_1()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());

		org.apache.zookeeper.ZooKeeper.States result = fixture.getState();

		// add additional test code here
		assertNotNull(result);
		assertEquals(true, result.isAlive());
		assertEquals(false, result.isConnected());
		assertEquals("CONNECTING", result.name());
		assertEquals("CONNECTING", result.toString());
		assertEquals(0, result.ordinal());
	}

	/**
	 * Run the List<OpResult> multi(Iterable<Op>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = org.apache.zookeeper.KeeperException.ConnectionLossException.class)
	public void testMulti_1()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		Iterable<Op> ops = new LinkedList();

		List<OpResult> result = fixture.multi(ops);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the List<OpResult> multi(Iterable<Op>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = org.apache.zookeeper.KeeperException.ConnectionLossException.class)
	public void testMulti_2()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		Iterable<Op> ops = new LinkedList();

		List<OpResult> result = fixture.multi(ops);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the List<OpResult> multi(Iterable<Op>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = org.apache.zookeeper.KeeperException.ConnectionLossException.class)
	public void testMulti_3()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		Iterable<Op> ops = new LinkedList();

		List<OpResult> result = fixture.multi(ops);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the List<OpResult> multiInternal(MultiTransactionRecord) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = org.apache.zookeeper.KeeperException.ConnectionLossException.class)
	public void testMultiInternal_1()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		MultiTransactionRecord request = new MultiTransactionRecord();

		List<OpResult> result = fixture.multiInternal(request);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the List<OpResult> multiInternal(MultiTransactionRecord) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = org.apache.zookeeper.KeeperException.ConnectionLossException.class)
	public void testMultiInternal_2()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		MultiTransactionRecord request = new MultiTransactionRecord();

		List<OpResult> result = fixture.multiInternal(request);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the List<OpResult> multiInternal(MultiTransactionRecord) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = org.apache.zookeeper.KeeperException.ConnectionLossException.class)
	public void testMultiInternal_3()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		MultiTransactionRecord request = new MultiTransactionRecord();

		List<OpResult> result = fixture.multiInternal(request);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the List<OpResult> multiInternal(MultiTransactionRecord) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = org.apache.zookeeper.KeeperException.ConnectionLossException.class)
	public void testMultiInternal_4()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		MultiTransactionRecord request = new MultiTransactionRecord();

		List<OpResult> result = fixture.multiInternal(request);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the List<OpResult> multiInternal(MultiTransactionRecord) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = org.apache.zookeeper.KeeperException.ConnectionLossException.class)
	public void testMultiInternal_5()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		MultiTransactionRecord request = new MultiTransactionRecord();

		List<OpResult> result = fixture.multiInternal(request);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the List<OpResult> multiInternal(MultiTransactionRecord) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = org.apache.zookeeper.KeeperException.ConnectionLossException.class)
	public void testMultiInternal_6()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		MultiTransactionRecord request = new MultiTransactionRecord();

		List<OpResult> result = fixture.multiInternal(request);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the void register(Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testRegister_1()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		Watcher watcher = new SimpleClient();

		fixture.register(watcher);

		// add additional test code here
	}

	/**
	 * Run the Stat setACL(String,List<ACL>,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testSetACL_1()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		List<ACL> acl = new LinkedList();
		int version = 1;

		Stat result = fixture.setACL(path, acl, version);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.setACL(ZooKeeper.java:1379)
		assertNotNull(result);
	}

	/**
	 * Run the Stat setACL(String,List<ACL>,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testSetACL_2()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		List<ACL> acl = new LinkedList();
		int version = 1;

		Stat result = fixture.setACL(path, acl, version);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.setACL(ZooKeeper.java:1379)
		assertNotNull(result);
	}

	/**
	 * Run the Stat setACL(String,List<ACL>,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testSetACL_3()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		List<ACL> acl = new LinkedList();
		int version = 1;

		Stat result = fixture.setACL(path, acl, version);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.setACL(ZooKeeper.java:1379)
		assertNotNull(result);
	}

	/**
	 * Run the Stat setACL(String,List<ACL>,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testSetACL_4()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		List<ACL> acl = null;
		int version = 1;

		Stat result = fixture.setACL(path, acl, version);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.setACL(ZooKeeper.java:1379)
		assertNotNull(result);
	}

	/**
	 * Run the Stat setACL(String,List<ACL>,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testSetACL_5()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		List<ACL> acl = new LinkedList();
		int version = 1;

		Stat result = fixture.setACL(path, acl, version);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the void setACL(String,List<ACL>,int,StatCallback,Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testSetACL_6()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		List<ACL> acl = new LinkedList();
		int version = 1;
		org.apache.zookeeper.AsyncCallback.StatCallback cb = new SimpleClient();
		Object ctx = new Object();

		fixture.setACL(path, acl, version, cb, ctx);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.setACL(ZooKeeper.java:1410)
	}

	/**
	 * Run the void setACL(String,List<ACL>,int,StatCallback,Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testSetACL_7()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		List<ACL> acl = new LinkedList();
		int version = 1;
		org.apache.zookeeper.AsyncCallback.StatCallback cb = new SimpleClient();
		Object ctx = new Object();

		fixture.setACL(path, acl, version, cb, ctx);

		// add additional test code here
	}

	/**
	 * Run the Stat setData(String,byte[],int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testSetData_1()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		byte[] data = new byte[] {};
		int version = 1;

		Stat result = fixture.setData(path, data, version);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.setData(ZooKeeper.java:1253)
		assertNotNull(result);
	}

	/**
	 * Run the Stat setData(String,byte[],int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testSetData_2()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		byte[] data = new byte[] {};
		int version = 1;

		Stat result = fixture.setData(path, data, version);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.setData(ZooKeeper.java:1253)
		assertNotNull(result);
	}

	/**
	 * Run the Stat setData(String,byte[],int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testSetData_3()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		byte[] data = new byte[] {};
		int version = 1;

		Stat result = fixture.setData(path, data, version);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.setData(ZooKeeper.java:1253)
		assertNotNull(result);
	}

	/**
	 * Run the Stat setData(String,byte[],int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testSetData_4()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		byte[] data = new byte[] {};
		int version = 1;

		Stat result = fixture.setData(path, data, version);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the void setData(String,byte[],int,StatCallback,Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testSetData_5()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		byte[] data = new byte[] {};
		int version = 1;
		org.apache.zookeeper.AsyncCallback.StatCallback cb = new SimpleClient();
		Object ctx = new Object();

		fixture.setData(path, data, version, cb, ctx);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.setData(ZooKeeper.java:1281)
	}

	/**
	 * Run the void setData(String,byte[],int,StatCallback,Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testSetData_6()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		byte[] data = new byte[] {};
		int version = 1;
		org.apache.zookeeper.AsyncCallback.StatCallback cb = new SimpleClient();
		Object ctx = new Object();

		fixture.setData(path, data, version, cb, ctx);

		// add additional test code here
	}

	/**
	 * Run the void sync(String,VoidCallback,Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testSync_1()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		org.apache.zookeeper.AsyncCallback.VoidCallback cb = new AsyncHammerTest();
		Object ctx = new Object();

		fixture.sync(path, cb, ctx);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Path length must be > 0
		//       at org.apache.zookeeper.common.PathUtils.validatePath(PathUtils.java:48)
		//       at org.apache.zookeeper.ZooKeeper.sync(ZooKeeper.java:1678)
	}

	/**
	 * Run the void sync(String,VoidCallback,Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testSync_2()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		String path = "";
		org.apache.zookeeper.AsyncCallback.VoidCallback cb = new AsyncHammerTest();
		Object ctx = new Object();

		fixture.sync(path, cb, ctx);

		// add additional test code here
	}

	/**
	 * Run the SocketAddress testableLocalSocketAddress() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testTestableLocalSocketAddress_1()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());

		SocketAddress result = fixture.testableLocalSocketAddress();

		// add additional test code here
		assertEquals(null, result);
	}

	/**
	 * Run the SocketAddress testableRemoteSocketAddress() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testTestableRemoteSocketAddress_1()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());

		SocketAddress result = fixture.testableRemoteSocketAddress();

		// add additional test code here
		assertEquals(null, result);
	}

	/**
	 * Run the boolean testableWaitForShutdown(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testTestableWaitForShutdown_1()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		int wait = 1;

		boolean result = fixture.testableWaitForShutdown(wait);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean testableWaitForShutdown(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testTestableWaitForShutdown_2()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		int wait = 1;

		boolean result = fixture.testableWaitForShutdown(wait);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean testableWaitForShutdown(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testTestableWaitForShutdown_3()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		int wait = 1;

		boolean result = fixture.testableWaitForShutdown(wait);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean testableWaitForShutdown(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testTestableWaitForShutdown_4()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		int wait = 1;

		boolean result = fixture.testableWaitForShutdown(wait);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean testableWaitForShutdown(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testTestableWaitForShutdown_5()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());
		int wait = 1;

		boolean result = fixture.testableWaitForShutdown(wait);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the String toString() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testToString_1()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());

		String result = fixture.toString();

		// add additional test code here
		assertEquals("State:CONNECTING sessionid:0x0 local:null remoteserver:null lastZxid:0 xid:1 sent:0 recv:0 queuedpkts:0 pendingresp:0 queuedevents:0", result);
	}

	/**
	 * Run the String toString() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testToString_2()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());

		String result = fixture.toString();

		// add additional test code here
		assertEquals("State:CONNECTING sessionid:0x0 local:null remoteserver:null lastZxid:0 xid:1 sent:0 recv:0 queuedpkts:0 pendingresp:0 queuedevents:0", result);
	}

	/**
	 * Run the Transaction transaction() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Test
	public void testTransaction_1()
		throws Exception {
		ZooKeeper fixture = new ZooKeeper("", 1, new SimpleClient());

		Transaction result = fixture.transaction();

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:29 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(ZooKeeperTest.class);
	}
}