package com.uiuc.org.apache.zookeeper.server.quorum;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import org.apache.zookeeper.server.NIOServerCnxnFactory;
import org.apache.zookeeper.server.Request;
import org.apache.zookeeper.server.ServerCnxnFactory;
import org.apache.zookeeper.server.ZKDatabase;
import org.apache.zookeeper.server.ZooKeeperServer;
import org.apache.zookeeper.server.persistence.FileTxnSnapLog;
import org.apache.zookeeper.server.quorum.Learner;
import org.apache.zookeeper.server.quorum.Observer;
import org.apache.zookeeper.server.quorum.ObserverZooKeeperServer;
import org.apache.zookeeper.server.quorum.QuorumPeer;
import org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical;
import org.apache.zookeeper.server.quorum.flexible.QuorumVerifier;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>ObserverZooKeeperServerTest</code> contains tests for the class <code>{@link ObserverZooKeeperServer}</code>.
 *
 * @generatedBy CodePro at 10/16/13 7:59 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class ObserverZooKeeperServerTest {
	/**
	 * Run the ObserverZooKeeperServer(FileTxnSnapLog,QuorumPeer,DataTreeBuilder,ZKDatabase) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@Test
	public void testObserverZooKeeperServer_1()
		throws Exception {
		FileTxnSnapLog logFactory = new FileTxnSnapLog(new File(""), new File(""));
		QuorumPeer self = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));
		self.setMaxSessionTimeout(1);
		self.setMinSessionTimeout(1);
		org.apache.zookeeper.server.ZooKeeperServer.DataTreeBuilder treeBuilder = new org.apache.zookeeper.server.ZooKeeperServer.BasicDataTreeBuilder();
		ZKDatabase zkDb = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));

		ObserverZooKeeperServer result = new ObserverZooKeeperServer(logFactory, self, treeBuilder, zkDb);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertNotNull(result);
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(ObserverZooKeeperServerTest.class);
	}
}