package com.uiuc.org.apache.zookeeper.server;

import java.nio.ByteBuffer;
import org.apache.jute.Record;
import org.apache.zookeeper.data.ACL;
import org.apache.zookeeper.server.ByteBufferOutputStream;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>ByteBufferOutputStreamTest</code> contains tests for the class <code>{@link ByteBufferOutputStream}</code>.
 *
 * @generatedBy CodePro at 10/16/13 8:18 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class ByteBufferOutputStreamTest {
	/**
	 * Run the ByteBufferOutputStream(ByteBuffer) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:18 PM
	 */
	@Test
	public void testByteBufferOutputStream_1()
		throws Exception {
		ByteBuffer bb = ByteBuffer.allocate(0);

		ByteBufferOutputStream result = new ByteBufferOutputStream(bb);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the void record2ByteBuffer(Record,ByteBuffer) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:18 PM
	 */
	@Test
	public void testRecord2ByteBuffer_1()
		throws Exception {
		Record record = new ACL();
		ByteBuffer bb = ByteBuffer.allocate(0);

		ByteBufferOutputStream.record2ByteBuffer(record, bb);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.nio.BufferOverflowException
		//       at java.nio.Buffer.nextPutIndex(Buffer.java:495)
		//       at java.nio.HeapByteBuffer.put(HeapByteBuffer.java:145)
		//       at org.apache.zookeeper.server.ByteBufferOutputStream.write(ByteBufferOutputStream.java:36)
		//       at java.io.DataOutputStream.writeInt(DataOutputStream.java:180)
		//       at org.apache.jute.BinaryOutputArchive.writeInt(BinaryOutputArchive.java:55)
		//       at org.apache.zookeeper.data.ACL.serialize(ACL.java:48)
		//       at org.apache.zookeeper.server.ByteBufferOutputStream.record2ByteBuffer(ByteBufferOutputStream.java:50)
	}

	/**
	 * Run the void record2ByteBuffer(Record,ByteBuffer) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:18 PM
	 */
	@Test
	public void testRecord2ByteBuffer_2()
		throws Exception {
		Record record = new ACL();
		ByteBuffer bb = ByteBuffer.allocate(0);

		ByteBufferOutputStream.record2ByteBuffer(record, bb);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.nio.BufferOverflowException
		//       at java.nio.Buffer.nextPutIndex(Buffer.java:495)
		//       at java.nio.HeapByteBuffer.put(HeapByteBuffer.java:145)
		//       at org.apache.zookeeper.server.ByteBufferOutputStream.write(ByteBufferOutputStream.java:36)
		//       at java.io.DataOutputStream.writeInt(DataOutputStream.java:180)
		//       at org.apache.jute.BinaryOutputArchive.writeInt(BinaryOutputArchive.java:55)
		//       at org.apache.zookeeper.data.ACL.serialize(ACL.java:48)
		//       at org.apache.zookeeper.server.ByteBufferOutputStream.record2ByteBuffer(ByteBufferOutputStream.java:50)
	}

	/**
	 * Run the void write(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:18 PM
	 */
	@Test
	public void testWrite_1()
		throws Exception {
		ByteBufferOutputStream fixture = new ByteBufferOutputStream(ByteBuffer.allocate(0));
		int b = 1;

		fixture.write(b);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.nio.BufferOverflowException
		//       at java.nio.Buffer.nextPutIndex(Buffer.java:495)
		//       at java.nio.HeapByteBuffer.put(HeapByteBuffer.java:145)
		//       at org.apache.zookeeper.server.ByteBufferOutputStream.write(ByteBufferOutputStream.java:36)
	}

	/**
	 * Run the void write(byte[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:18 PM
	 */
	@Test
	public void testWrite_2()
		throws Exception {
		ByteBufferOutputStream fixture = new ByteBufferOutputStream(ByteBuffer.allocate(0));
		byte[] b = new byte[] {};

		fixture.write(b);

		// add additional test code here
	}

	/**
	 * Run the void write(byte[],int,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:18 PM
	 */
	@Test
	public void testWrite_3()
		throws Exception {
		ByteBufferOutputStream fixture = new ByteBufferOutputStream(ByteBuffer.allocate(0));
		byte[] b = new byte[] {};
		int off = 1;
		int len = 1;

		fixture.write(b, off, len);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IndexOutOfBoundsException
		//       at java.nio.Buffer.checkBounds(Buffer.java:530)
		//       at java.nio.HeapByteBuffer.put(HeapByteBuffer.java:163)
		//       at org.apache.zookeeper.server.ByteBufferOutputStream.write(ByteBufferOutputStream.java:44)
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:18 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:18 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 8:18 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(ByteBufferOutputStreamTest.class);
	}
}