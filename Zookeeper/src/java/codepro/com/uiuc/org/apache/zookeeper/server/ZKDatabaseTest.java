package com.uiuc.org.apache.zookeeper.server;

import java.io.ByteArrayOutputStream;
import java.io.CharArrayWriter;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.PrintWriter;
import java.io.Writer;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import org.apache.jute.BinaryInputArchive;
import org.apache.jute.BinaryOutputArchive;
import org.apache.jute.InputArchive;
import org.apache.jute.OutputArchive;
import org.apache.jute.Record;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.data.ACL;
import org.apache.zookeeper.data.Id;
import org.apache.zookeeper.data.Stat;
import org.apache.zookeeper.server.DataNode;
import org.apache.zookeeper.server.DataTree;
import org.apache.zookeeper.server.NIOServerCnxn;
import org.apache.zookeeper.server.NIOServerCnxnFactory;
import org.apache.zookeeper.server.Request;
import org.apache.zookeeper.server.ServerCnxn;
import org.apache.zookeeper.server.ZKDatabase;
import org.apache.zookeeper.server.ZooKeeperServer;
import org.apache.zookeeper.server.persistence.FileTxnSnapLog;
import org.apache.zookeeper.server.quorum.Leader;
import org.apache.zookeeper.test.system.SimpleClient;
import org.apache.zookeeper.txn.TxnHeader;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>ZKDatabaseTest</code> contains tests for the class <code>{@link ZKDatabase}</code>.
 *
 * @generatedBy CodePro at 10/16/13 8:26 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class ZKDatabaseTest {
	/**
	 * Run the ZKDatabase(FileTxnSnapLog) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testZKDatabase_1()
		throws Exception {
		FileTxnSnapLog snapLog = new FileTxnSnapLog(new File(""), new File(""));

		ZKDatabase result = new ZKDatabase(snapLog);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertNotNull(result);
	}

	/**
	 * Run the void addCommittedProposal(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testAddCommittedProposal_1()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());
		request.hdr = new TxnHeader();
		request.zxid = 1L;
		request.txn = new ACL();

		fixture.addCommittedProposal(request);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void addCommittedProposal(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testAddCommittedProposal_2()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());
		request.hdr = new TxnHeader();
		request.zxid = 1L;

		fixture.addCommittedProposal(request);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void addCommittedProposal(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testAddCommittedProposal_3()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());
		request.hdr = new TxnHeader();
		request.zxid = 1L;

		fixture.addCommittedProposal(request);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void addCommittedProposal(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testAddCommittedProposal_4()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());
		request.hdr = new TxnHeader();
		request.zxid = 1L;
		request.txn = new ACL();

		fixture.addCommittedProposal(request);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void addCommittedProposal(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testAddCommittedProposal_5()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());
		request.hdr = new TxnHeader();
		request.zxid = 1L;
		request.txn = new ACL();

		fixture.addCommittedProposal(request);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void addCommittedProposal(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testAddCommittedProposal_6()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());
		request.hdr = new TxnHeader();
		request.zxid = 1L;
		request.txn = new ACL();

		fixture.addCommittedProposal(request);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void addCommittedProposal(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testAddCommittedProposal_7()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());
		request.hdr = new TxnHeader();
		request.zxid = 1L;
		request.txn = new ACL();

		fixture.addCommittedProposal(request);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void addCommittedProposal(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testAddCommittedProposal_8()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());
		request.hdr = new TxnHeader();
		request.zxid = 1L;
		request.txn = new ACL();

		fixture.addCommittedProposal(request);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void addCommittedProposal(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testAddCommittedProposal_9()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());
		request.hdr = new TxnHeader();
		request.zxid = 1L;
		request.txn = new ACL();

		fixture.addCommittedProposal(request);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void addCommittedProposal(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testAddCommittedProposal_10()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());
		request.hdr = new TxnHeader();
		request.zxid = 1L;
		request.txn = new ACL();

		fixture.addCommittedProposal(request);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void addCommittedProposal(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testAddCommittedProposal_11()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());
		request.hdr = new TxnHeader();
		request.zxid = 1L;
		request.txn = new ACL();

		fixture.addCommittedProposal(request);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the boolean append(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testAppend_1()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));
		Request si = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());

		boolean result = fixture.append(si);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertTrue(result);
	}

	/**
	 * Run the boolean append(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testAppend_2()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));
		Request si = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());

		boolean result = fixture.append(si);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertTrue(result);
	}

	/**
	 * Run the boolean append(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testAppend_3()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));
		Request si = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());

		boolean result = fixture.append(si);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertTrue(result);
	}

	/**
	 * Run the void clear() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testClear_1()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));

		fixture.clear();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void close() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testClose_1()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));

		fixture.close();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void close() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testClose_2()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));

		fixture.close();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void commit() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testCommit_1()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));

		fixture.commit();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void commit() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testCommit_2()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));

		fixture.commit();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the List<ACL> convertLong(Long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testConvertLong_1()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));
		Long aclL = new Long(1L);

		List<ACL> result = fixture.convertLong(aclL);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertNotNull(result);
	}

	/**
	 * Run the void deserializeSnapshot(InputArchive) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testDeserializeSnapshot_1()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));
		InputArchive ia = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));

		fixture.deserializeSnapshot(ia);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void deserializeSnapshot(InputArchive) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testDeserializeSnapshot_2()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));
		InputArchive ia = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));

		fixture.deserializeSnapshot(ia);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void dumpEphemerals(PrintWriter) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testDumpEphemerals_1()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));
		PrintWriter pwriter = new PrintWriter(new CharArrayWriter());

		fixture.dumpEphemerals(pwriter);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the List<ACL> getACL(String,Stat) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testGetACL_1()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));
		String path = "";
		Stat stat = new Stat();

		List<ACL> result = fixture.getACL(path, stat);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertNotNull(result);
	}

	/**
	 * Run the List<ACL> getACL(String,Stat) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testGetACL_2()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));
		String path = "";
		Stat stat = new Stat();

		List<ACL> result = fixture.getACL(path, stat);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertNotNull(result);
	}

	/**
	 * Run the int getAclSize() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testGetAclSize_1()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));

		int result = fixture.getAclSize();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertEquals(0, result);
	}

	/**
	 * Run the List<String> getChildren(String,Stat,Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testGetChildren_1()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));
		String path = "";
		Stat stat = new Stat();
		Watcher watcher = new SimpleClient();

		List<String> result = fixture.getChildren(path, stat, watcher);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertNotNull(result);
	}

	/**
	 * Run the List<String> getChildren(String,Stat,Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testGetChildren_2()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));
		String path = "";
		Stat stat = new Stat();
		Watcher watcher = new SimpleClient();

		List<String> result = fixture.getChildren(path, stat, watcher);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertNotNull(result);
	}

	/**
	 * Run the LinkedList<org.apache.zookeeper.server.quorum.Leader.Proposal> getCommittedLog() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testGetCommittedLog_1()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));

		LinkedList<org.apache.zookeeper.server.quorum.Leader.Proposal> result = fixture.getCommittedLog();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertNotNull(result);
	}

	/**
	 * Run the LinkedList<org.apache.zookeeper.server.quorum.Leader.Proposal> getCommittedLog() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testGetCommittedLog_2()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));

		LinkedList<org.apache.zookeeper.server.quorum.Leader.Proposal> result = fixture.getCommittedLog();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertNotNull(result);
	}

	/**
	 * Run the byte[] getData(String,Stat,Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testGetData_1()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));
		String path = "";
		Stat stat = new Stat();
		Watcher watcher = new SimpleClient();

		byte[] result = fixture.getData(path, stat, watcher);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertNotNull(result);
	}

	/**
	 * Run the byte[] getData(String,Stat,Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testGetData_2()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));
		String path = "";
		Stat stat = new Stat();
		Watcher watcher = new SimpleClient();

		byte[] result = fixture.getData(path, stat, watcher);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertNotNull(result);
	}

	/**
	 * Run the DataTree getDataTree() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testGetDataTree_1()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));

		DataTree result = fixture.getDataTree();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertNotNull(result);
	}

	/**
	 * Run the long getDataTreeLastProcessedZxid() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testGetDataTreeLastProcessedZxid_1()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));

		long result = fixture.getDataTreeLastProcessedZxid();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertEquals(0L, result);
	}

	/**
	 * Run the HashSet<String> getEphemerals(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testGetEphemerals_1()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));
		long sessionId = 1L;

		HashSet<String> result = fixture.getEphemerals(sessionId);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertNotNull(result);
	}

	/**
	 * Run the ReentrantReadWriteLock getLogLock() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testGetLogLock_1()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));

		ReentrantReadWriteLock result = fixture.getLogLock();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertNotNull(result);
	}

	/**
	 * Run the DataNode getNode(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testGetNode_1()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));
		String path = "";

		DataNode result = fixture.getNode(path);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertNotNull(result);
	}

	/**
	 * Run the int getNodeCount() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testGetNodeCount_1()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));

		int result = fixture.getNodeCount();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertEquals(0, result);
	}

	/**
	 * Run the ConcurrentHashMap<Long, Integer> getSessionWithTimeOuts() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testGetSessionWithTimeOuts_1()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));

		ConcurrentHashMap<Long, Integer> result = fixture.getSessionWithTimeOuts();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertNotNull(result);
	}

	/**
	 * Run the Collection<Long> getSessions() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testGetSessions_1()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));

		Collection<Long> result = fixture.getSessions();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertNotNull(result);
	}

	/**
	 * Run the long getmaxCommittedLog() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testGetmaxCommittedLog_1()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));

		long result = fixture.getmaxCommittedLog();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertEquals(0L, result);
	}

	/**
	 * Run the long getminCommittedLog() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testGetminCommittedLog_1()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));

		long result = fixture.getminCommittedLog();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertEquals(0L, result);
	}

	/**
	 * Run the boolean isInitialized() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testIsInitialized_1()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));

		boolean result = fixture.isInitialized();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertTrue(result);
	}

	/**
	 * Run the boolean isInitialized() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testIsInitialized_2()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));

		boolean result = fixture.isInitialized();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertTrue(result);
	}

	/**
	 * Run the boolean isSpecialPath(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testIsSpecialPath_1()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));
		String path = "";

		boolean result = fixture.isSpecialPath(path);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertTrue(result);
	}

	/**
	 * Run the boolean isSpecialPath(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testIsSpecialPath_2()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));
		String path = "";

		boolean result = fixture.isSpecialPath(path);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertTrue(result);
	}

	/**
	 * Run the void killSession(long,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testKillSession_1()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));
		long sessionId = 1L;
		long zxid = 1L;

		fixture.killSession(sessionId, zxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the long loadDataBase() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testLoadDataBase_1()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));

		long result = fixture.loadDataBase();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertEquals(0L, result);
	}

	/**
	 * Run the long loadDataBase() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testLoadDataBase_2()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));

		long result = fixture.loadDataBase();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertEquals(0L, result);
	}

	/**
	 * Run the org.apache.zookeeper.server.DataTree.ProcessTxnResult processTxn(TxnHeader,Record) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testProcessTxn_1()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));
		TxnHeader hdr = new TxnHeader();
		Record txn = new ACL();

		org.apache.zookeeper.server.DataTree.ProcessTxnResult result = fixture.processTxn(hdr, txn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertNotNull(result);
	}

	/**
	 * Run the void removeCnxn(ServerCnxn) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testRemoveCnxn_1()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));
		ServerCnxn cnxn = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());

		fixture.removeCnxn(cnxn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void rollLog() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testRollLog_1()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));

		fixture.rollLog();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void rollLog() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testRollLog_2()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));

		fixture.rollLog();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void serializeSnapshot(OutputArchive) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testSerializeSnapshot_1()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));
		OutputArchive oa = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));

		fixture.serializeSnapshot(oa);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void serializeSnapshot(OutputArchive) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testSerializeSnapshot_2()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));
		OutputArchive oa = new BinaryOutputArchive(new DataOutputStream(new ByteArrayOutputStream()));

		fixture.serializeSnapshot(oa);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void setDataTreeInit(boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testSetDataTreeInit_1()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));
		boolean b = true;

		fixture.setDataTreeInit(b);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void setWatches(long,List<String>,List<String>,List<String>,Watcher) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testSetWatches_1()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));
		long relativeZxid = 1L;
		List<String> dataWatches = new LinkedList();
		List<String> existWatches = new LinkedList();
		List<String> childWatches = new LinkedList();
		Watcher watcher = new SimpleClient();

		fixture.setWatches(relativeZxid, dataWatches, existWatches, childWatches, watcher);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void setlastProcessedZxid(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testSetlastProcessedZxid_1()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));
		long zxid = 1L;

		fixture.setlastProcessedZxid(zxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the Stat statNode(String,ServerCnxn) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testStatNode_1()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));
		String path = "";
		ServerCnxn serverCnxn = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());

		Stat result = fixture.statNode(path, serverCnxn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertNotNull(result);
	}

	/**
	 * Run the Stat statNode(String,ServerCnxn) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testStatNode_2()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));
		String path = "";
		ServerCnxn serverCnxn = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());

		Stat result = fixture.statNode(path, serverCnxn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertNotNull(result);
	}

	/**
	 * Run the boolean truncateLog(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testTruncateLog_1()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));
		long zxid = 1L;

		boolean result = fixture.truncateLog(zxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertTrue(result);
	}

	/**
	 * Run the boolean truncateLog(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testTruncateLog_2()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));
		long zxid = 1L;

		boolean result = fixture.truncateLog(zxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertTrue(result);
	}

	/**
	 * Run the boolean truncateLog(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testTruncateLog_3()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));
		long zxid = 1L;

		boolean result = fixture.truncateLog(zxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertTrue(result);
	}

	/**
	 * Run the boolean truncateLog(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Test
	public void testTruncateLog_4()
		throws Exception {
		ZKDatabase fixture = new ZKDatabase(new FileTxnSnapLog(new File(""), new File("")));
		long zxid = 1L;

		boolean result = fixture.truncateLog(zxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertTrue(result);
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 8:26 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(ZKDatabaseTest.class);
	}
}