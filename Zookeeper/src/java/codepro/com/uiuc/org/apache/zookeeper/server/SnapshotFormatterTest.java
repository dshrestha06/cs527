package com.uiuc.org.apache.zookeeper.server;

import java.io.FileNotFoundException;
import org.apache.zookeeper.server.SnapshotFormatter;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>SnapshotFormatterTest</code> contains tests for the class <code>{@link SnapshotFormatter}</code>.
 *
 * @generatedBy CodePro at 10/16/13 8:24 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class SnapshotFormatterTest {
	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test(expected = java.io.FileNotFoundException.class)
	public void testMain_1()
		throws Exception {
		String[] args = new String[] {""};

		SnapshotFormatter.main(args);

		// add additional test code here
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test(expected = java.lang.SecurityException.class)
	public void testMain_2()
		throws Exception {
		String[] args = new String[] {"", null};

		SnapshotFormatter.main(args);

		// add additional test code here
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test(expected = java.lang.SecurityException.class)
	public void testMain_3()
		throws Exception {
		String[] args = new String[] {"", null};

		SnapshotFormatter.main(args);

		// add additional test code here
	}

	/**
	 * Run the void run(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test(expected = java.io.FileNotFoundException.class)
	public void testRun_1()
		throws Exception {
		SnapshotFormatter fixture = new SnapshotFormatter();
		String snapshotFileName = "";

		fixture.run(snapshotFileName);

		// add additional test code here
	}

	/**
	 * Run the void run(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test(expected = java.io.FileNotFoundException.class)
	public void testRun_2()
		throws Exception {
		SnapshotFormatter fixture = new SnapshotFormatter();
		String snapshotFileName = "";

		fixture.run(snapshotFileName);

		// add additional test code here
	}

	/**
	 * Run the void run(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Test(expected = java.io.FileNotFoundException.class)
	public void testRun_3()
		throws Exception {
		SnapshotFormatter fixture = new SnapshotFormatter();
		String snapshotFileName = "";

		fixture.run(snapshotFileName);

		// add additional test code here
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 8:24 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(SnapshotFormatterTest.class);
	}
}