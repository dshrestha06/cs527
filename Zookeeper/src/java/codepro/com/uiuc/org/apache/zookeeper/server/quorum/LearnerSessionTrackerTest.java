package com.uiuc.org.apache.zookeeper.server.quorum;

import java.io.CharArrayWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.zookeeper.server.SessionTracker;
import org.apache.zookeeper.server.ZooKeeperServer;
import org.apache.zookeeper.server.quorum.LearnerSessionTracker;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>LearnerSessionTrackerTest</code> contains tests for the class <code>{@link LearnerSessionTracker}</code>.
 *
 * @generatedBy CodePro at 10/16/13 7:59 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class LearnerSessionTrackerTest {
	/**
	 * Run the LearnerSessionTracker(SessionExpirer,ConcurrentHashMap<Long,Integer>,long) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@Test
	public void testLearnerSessionTracker_1()
		throws Exception {
		org.apache.zookeeper.server.SessionTracker.SessionExpirer expirer = new ZooKeeperServer();
		ConcurrentHashMap<Long, Integer> sessionsWithTimeouts = new ConcurrentHashMap();
		long id = 1L;

		LearnerSessionTracker result = new LearnerSessionTracker(expirer, sessionsWithTimeouts, id);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the void addSession(long,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@Test
	public void testAddSession_1()
		throws Exception {
		LearnerSessionTracker fixture = new LearnerSessionTracker(new ZooKeeperServer(), new ConcurrentHashMap(), 1L);
		long sessionId = 1L;
		int sessionTimeout = 1;

		fixture.addSession(sessionId, sessionTimeout);

		// add additional test code here
	}

	/**
	 * Run the void checkSession(long,Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@Test
	public void testCheckSession_1()
		throws Exception {
		LearnerSessionTracker fixture = new LearnerSessionTracker(new ZooKeeperServer(), new ConcurrentHashMap(), 1L);
		long sessionId = 1L;
		Object owner = new Object();

		fixture.checkSession(sessionId, owner);

		// add additional test code here
	}

	/**
	 * Run the long createSession(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@Test
	public void testCreateSession_1()
		throws Exception {
		LearnerSessionTracker fixture = new LearnerSessionTracker(new ZooKeeperServer(), new ConcurrentHashMap(), 1L);
		int sessionTimeout = 1;

		long result = fixture.createSession(sessionTimeout);

		// add additional test code here
		assertEquals(90568889549717504L, result);
	}

	/**
	 * Run the void dumpSessions(PrintWriter) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@Test
	public void testDumpSessions_1()
		throws Exception {
		LearnerSessionTracker fixture = new LearnerSessionTracker(new ZooKeeperServer(), new ConcurrentHashMap(), 1L);
		PrintWriter pwriter = new PrintWriter(new CharArrayWriter());

		fixture.dumpSessions(pwriter);

		// add additional test code here
	}

	/**
	 * Run the void removeSession(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@Test
	public void testRemoveSession_1()
		throws Exception {
		LearnerSessionTracker fixture = new LearnerSessionTracker(new ZooKeeperServer(), new ConcurrentHashMap(), 1L);
		long sessionId = 1L;

		fixture.removeSession(sessionId);

		// add additional test code here
	}

	/**
	 * Run the void setOwner(long,Object) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@Test
	public void testSetOwner_1()
		throws Exception {
		LearnerSessionTracker fixture = new LearnerSessionTracker(new ZooKeeperServer(), new ConcurrentHashMap(), 1L);
		long sessionId = 1L;
		Object owner = new Object();

		fixture.setOwner(sessionId, owner);

		// add additional test code here
	}

	/**
	 * Run the void setSessionClosing(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@Test
	public void testSetSessionClosing_1()
		throws Exception {
		LearnerSessionTracker fixture = new LearnerSessionTracker(new ZooKeeperServer(), new ConcurrentHashMap(), 1L);
		long sessionId = 1L;

		fixture.setSessionClosing(sessionId);

		// add additional test code here
	}

	/**
	 * Run the void shutdown() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@Test
	public void testShutdown_1()
		throws Exception {
		LearnerSessionTracker fixture = new LearnerSessionTracker(new ZooKeeperServer(), new ConcurrentHashMap(), 1L);

		fixture.shutdown();

		// add additional test code here
	}

	/**
	 * Run the HashMap<Long, Integer> snapshot() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@Test
	public void testSnapshot_1()
		throws Exception {
		LearnerSessionTracker fixture = new LearnerSessionTracker(new ZooKeeperServer(), new ConcurrentHashMap(), 1L);

		HashMap<Long, Integer> result = fixture.snapshot();

		// add additional test code here
		assertNotNull(result);
		assertEquals(0, result.size());
	}

	/**
	 * Run the boolean touchSession(long,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@Test
	public void testTouchSession_1()
		throws Exception {
		LearnerSessionTracker fixture = new LearnerSessionTracker(new ZooKeeperServer(), new ConcurrentHashMap(), 1L);
		long sessionId = 1L;
		int sessionTimeout = 1;

		boolean result = fixture.touchSession(sessionId, sessionTimeout);

		// add additional test code here
		assertEquals(true, result);
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(LearnerSessionTrackerTest.class);
	}
}