package com.uiuc.org.apache.zookeeper.server.auth;

import java.util.HashMap;
import java.util.Map;
import javax.security.auth.Subject;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.login.Configuration;
import org.apache.zookeeper.JaasConfiguration;
import org.apache.zookeeper.server.auth.DigestLoginModule;
import org.apache.zookeeper.server.auth.SaslServerCallbackHandler;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>DigestLoginModuleTest</code> contains tests for the class <code>{@link DigestLoginModule}</code>.
 *
 * @generatedBy CodePro at 10/16/13 7:33 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class DigestLoginModuleTest {
	/**
	 * Run the boolean abort() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testAbort_1()
		throws Exception {
		DigestLoginModule fixture = new DigestLoginModule();

		boolean result = fixture.abort();

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean commit() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testCommit_1()
		throws Exception {
		DigestLoginModule fixture = new DigestLoginModule();

		boolean result = fixture.commit();

		// add additional test code here
		assertEquals(true, result);
	}

	/**
	 * Run the void initialize(Subject,CallbackHandler,Map<String,?>,Map<String,?>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testInitialize_1()
		throws Exception {
		DigestLoginModule fixture = new DigestLoginModule();
		Subject subject = new Subject();
		CallbackHandler callbackHandler = new SaslServerCallbackHandler(new JaasConfiguration());
		Map<String, Object> sharedState = new HashMap();
		Map<String, Object> options = new HashMap();

		fixture.initialize(subject, callbackHandler, sharedState, options);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.io.IOException: Could not find a 'Server' entry in this configuration: Server cannot start.
		//       at org.apache.zookeeper.server.auth.SaslServerCallbackHandler.<init>(SaslServerCallbackHandler.java:57)
	}

	/**
	 * Run the void initialize(Subject,CallbackHandler,Map<String,?>,Map<String,?>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testInitialize_2()
		throws Exception {
		DigestLoginModule fixture = new DigestLoginModule();
		Subject subject = new Subject();
		CallbackHandler callbackHandler = new SaslServerCallbackHandler(new JaasConfiguration());
		Map<String, Object> sharedState = new HashMap();
		Map<String, Object> options = new HashMap();

		fixture.initialize(subject, callbackHandler, sharedState, options);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.io.IOException: Could not find a 'Server' entry in this configuration: Server cannot start.
		//       at org.apache.zookeeper.server.auth.SaslServerCallbackHandler.<init>(SaslServerCallbackHandler.java:57)
	}

	/**
	 * Run the boolean login() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testLogin_1()
		throws Exception {
		DigestLoginModule fixture = new DigestLoginModule();

		boolean result = fixture.login();

		// add additional test code here
		assertEquals(true, result);
	}

	/**
	 * Run the boolean logout() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testLogout_1()
		throws Exception {
		DigestLoginModule fixture = new DigestLoginModule();

		boolean result = fixture.logout();

		// add additional test code here
		assertEquals(true, result);
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(DigestLoginModuleTest.class);
	}
}