package com.uiuc.org.apache.zookeeper.server.quorum;

import org.apache.zookeeper.server.quorum.Observer;
import org.apache.zookeeper.server.quorum.ObserverZooKeeperServer;
import org.apache.zookeeper.server.quorum.QuorumPacket;
import org.apache.zookeeper.server.quorum.QuorumPeer;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>ObserverTest</code> contains tests for the class <code>{@link Observer}</code>.
 *
 * @generatedBy CodePro at 10/16/13 8:00 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class ObserverTest {
	/**
	 * Run the Observer(QuorumPeer,ObserverZooKeeperServer) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testObserver_1()
		throws Exception {
		QuorumPeer self = new QuorumPeer();
		ObserverZooKeeperServer observerZooKeeperServer = null;

		Observer result = new Observer(self, observerZooKeeperServer);

		// add additional test code here
		assertNotNull(result);
		assertEquals("Observer null pendingRevalidationCount:0", result.toString());
		assertEquals(null, result.getSocket());
		assertEquals(0, result.getPendingRevalidationsCount());
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(ObserverTest.class);
	}
}