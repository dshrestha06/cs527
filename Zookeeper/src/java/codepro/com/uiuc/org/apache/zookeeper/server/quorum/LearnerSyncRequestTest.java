package com.uiuc.org.apache.zookeeper.server.quorum;

import java.nio.ByteBuffer;
import java.util.LinkedList;
import java.util.List;
import org.apache.zookeeper.data.Id;
import org.apache.zookeeper.server.quorum.LearnerHandler;
import org.apache.zookeeper.server.quorum.LearnerSyncRequest;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>LearnerSyncRequestTest</code> contains tests for the class <code>{@link LearnerSyncRequest}</code>.
 *
 * @generatedBy CodePro at 10/16/13 7:59 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class LearnerSyncRequestTest {
	/**
	 * Run the LearnerSyncRequest(LearnerHandler,long,int,int,ByteBuffer,List<Id>) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@Test
	public void testLearnerSyncRequest_1()
		throws Exception {
		LearnerHandler fh = null;
		long sessionId = 1L;
		int xid = 1;
		int type = 1;
		ByteBuffer bb = ByteBuffer.allocate(0);
		List<Id> authInfo = new LinkedList();

		LearnerSyncRequest result = new LearnerSyncRequest(fh, sessionId, xid, type, bb, authInfo);

		// add additional test code here
		assertNotNull(result);
		assertEquals("sessionid:0x1 type:create cxid:0x1 zxid:0xfffffffffffffffe txntype:unknown reqpath:n/a", result.toString());
		assertEquals(null, result.getException());
		assertEquals(null, result.getOwner());
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:59 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(LearnerSyncRequestTest.class);
	}
}