package com.uiuc.org.apache.zookeeper;

import java.util.Set;
import org.apache.zookeeper.ClientWatchManager;
import org.apache.zookeeper.Watcher;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>ClientWatchManagerTest</code> contains tests for the class <code>{@link ClientWatchManager}</code>.
 *
 * @generatedBy CodePro at 10/16/13 7:22 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class ClientWatchManagerTest {
	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:22 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:22 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:22 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(ClientWatchManagerTest.class);
	}
}