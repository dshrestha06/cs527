package com.uiuc.org.apache.zookeeper.server.quorum;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import org.apache.zookeeper.server.NIOServerCnxnFactory;
import org.apache.zookeeper.server.ServerCnxnFactory;
import org.apache.zookeeper.server.quorum.QuorumBean;
import org.apache.zookeeper.server.quorum.QuorumPeer;
import org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical;
import org.apache.zookeeper.server.quorum.flexible.QuorumVerifier;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>QuorumBeanTest</code> contains tests for the class <code>{@link QuorumBean}</code>.
 *
 * @generatedBy CodePro at 10/16/13 8:00 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class QuorumBeanTest {
	/**
	 * Run the QuorumBean(QuorumPeer) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testQuorumBean_1()
		throws Exception {
		QuorumPeer peer = new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical(""));

		QuorumBean result = new QuorumBean(peer);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertNotNull(result);
	}

	/**
	 * Run the String getName() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testGetName_1()
		throws Exception {
		QuorumBean fixture = new QuorumBean(new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical("")));

		String result = fixture.getName();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertNotNull(result);
	}

	/**
	 * Run the int getQuorumSize() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testGetQuorumSize_1()
		throws Exception {
		QuorumBean fixture = new QuorumBean(new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical("")));

		int result = fixture.getQuorumSize();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertEquals(0, result);
	}

	/**
	 * Run the boolean isHidden() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Test
	public void testIsHidden_1()
		throws Exception {
		QuorumBean fixture = new QuorumBean(new QuorumPeer(new HashMap(), new File(""), new File(""), 1, 1L, 1, 1, 1, new NIOServerCnxnFactory(), new QuorumHierarchical("")));

		boolean result = fixture.isHidden();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertTrue(result);
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 8:00 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(QuorumBeanTest.class);
	}
}