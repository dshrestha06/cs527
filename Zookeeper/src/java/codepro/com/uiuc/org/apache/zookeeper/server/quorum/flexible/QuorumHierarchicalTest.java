package com.uiuc.org.apache.zookeeper.server.quorum.flexible;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Properties;
import org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>QuorumHierarchicalTest</code> contains tests for the class <code>{@link QuorumHierarchical}</code>.
 *
 * @generatedBy CodePro at 10/16/13 7:41 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class QuorumHierarchicalTest {
	/**
	 * Run the QuorumHierarchical(String) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testQuorumHierarchical_1()
		throws Exception {
		String filename = "";

		QuorumHierarchical result = new QuorumHierarchical(filename);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertNotNull(result);
	}

	/**
	 * Run the QuorumHierarchical(String) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testQuorumHierarchical_2()
		throws Exception {
		String filename = "";

		QuorumHierarchical result = new QuorumHierarchical(filename);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    org.apache.zookeeper.server.quorum.QuorumPeerConfig$ConfigException: Error processing 
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.readConfigFile(QuorumHierarchical.java:167)
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.<init>(QuorumHierarchical.java:88)
		assertNotNull(result);
	}

	/**
	 * Run the QuorumHierarchical(Properties) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testQuorumHierarchical_3()
		throws Exception {
		Properties qp = new Properties();

		QuorumHierarchical result = new QuorumHierarchical(qp);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the QuorumHierarchical(int,HashMap<Long,Long>,HashMap<Long,Long>) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testQuorumHierarchical_4()
		throws Exception {
		int numGroups = 1;
		HashMap<Long, Long> serverWeight = new HashMap();
		HashMap<Long, Long> serverGroup = new HashMap();

		QuorumHierarchical result = new QuorumHierarchical(numGroups, serverWeight, serverGroup);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the boolean containsQuorum(HashSet<Long>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testContainsQuorum_1()
		throws Exception {
		QuorumHierarchical fixture = new QuorumHierarchical(1, new HashMap(), new HashMap());
		HashSet<Long> set = new HashSet();

		boolean result = fixture.containsQuorum(set);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean containsQuorum(HashSet<Long>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testContainsQuorum_2()
		throws Exception {
		QuorumHierarchical fixture = new QuorumHierarchical(1, new HashMap(), new HashMap());
		HashSet<Long> set = new HashSet();

		boolean result = fixture.containsQuorum(set);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean containsQuorum(HashSet<Long>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testContainsQuorum_3()
		throws Exception {
		QuorumHierarchical fixture = new QuorumHierarchical(1, new HashMap(), new HashMap());
		HashSet<Long> set = new HashSet();

		boolean result = fixture.containsQuorum(set);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean containsQuorum(HashSet<Long>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testContainsQuorum_4()
		throws Exception {
		QuorumHierarchical fixture = new QuorumHierarchical(1, new HashMap(), new HashMap());
		HashSet<Long> set = new HashSet();

		boolean result = fixture.containsQuorum(set);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the long getWeight(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Test
	public void testGetWeight_1()
		throws Exception {
		QuorumHierarchical fixture = new QuorumHierarchical(1, new HashMap(), new HashMap());
		long id = 1L;

		long result = fixture.getWeight(id);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.quorum.flexible.QuorumHierarchical.getWeight(QuorumHierarchical.java:136)
		assertEquals(0L, result);
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:41 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(QuorumHierarchicalTest.class);
	}
}