package com.uiuc.org.apache.zookeeper.server;

import java.net.InetSocketAddress;
import org.apache.zookeeper.server.NettyServerCnxnFactory;
import org.apache.zookeeper.server.ServerCnxn;
import org.apache.zookeeper.server.ZooKeeperServer;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>NettyServerCnxnFactoryTest</code> contains tests for the class <code>{@link NettyServerCnxnFactory}</code>.
 *
 * @generatedBy CodePro at 10/16/13 8:21 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class NettyServerCnxnFactoryTest {
	/**
	 * Run the NettyServerCnxnFactory() constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Test
	public void testNettyServerCnxnFactory_1()
		throws Exception {

		NettyServerCnxnFactory result = new NettyServerCnxnFactory();

		// add additional test code here
		assertNotNull(result);
		assertEquals(null, result.getLocalAddress());
		assertEquals(60, result.getMaxClientCnxnsPerHost());
		assertEquals(0, result.getNumAliveConnections());
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 8:21 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(NettyServerCnxnFactoryTest.class);
	}
}