package com.uiuc.org.apache.zookeeper.server.persistence;

import java.io.File;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.jute.Record;
import org.apache.zookeeper.data.ACL;
import org.apache.zookeeper.data.Id;
import org.apache.zookeeper.server.DataTree;
import org.apache.zookeeper.server.NIOServerCnxn;
import org.apache.zookeeper.server.NIOServerCnxnFactory;
import org.apache.zookeeper.server.Request;
import org.apache.zookeeper.server.ServerCnxn;
import org.apache.zookeeper.server.ZooKeeperServer;
import org.apache.zookeeper.server.persistence.FileTxnSnapLog;
import org.apache.zookeeper.txn.CreateSessionTxn;
import org.apache.zookeeper.txn.TxnHeader;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>FileTxnSnapLogTest</code> contains tests for the class <code>{@link FileTxnSnapLog}</code>.
 *
 * @generatedBy CodePro at 10/16/13 7:34 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class FileTxnSnapLogTest {
	/**
	 * Run the FileTxnSnapLog(File,File) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testFileTxnSnapLog_1()
		throws Exception {
		File dataDir = new File("");
		File snapDir = new File("");

		FileTxnSnapLog result = new FileTxnSnapLog(dataDir, snapDir);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertNotNull(result);
	}

	/**
	 * Run the FileTxnSnapLog(File,File) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testFileTxnSnapLog_2()
		throws Exception {
		File dataDir = new File("");
		File snapDir = new File("");

		FileTxnSnapLog result = new FileTxnSnapLog(dataDir, snapDir);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertNotNull(result);
	}

	/**
	 * Run the FileTxnSnapLog(File,File) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testFileTxnSnapLog_3()
		throws Exception {
		File dataDir = new File("");
		File snapDir = new File("");

		FileTxnSnapLog result = new FileTxnSnapLog(dataDir, snapDir);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertNotNull(result);
	}

	/**
	 * Run the FileTxnSnapLog(File,File) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testFileTxnSnapLog_4()
		throws Exception {
		File dataDir = new File("");
		File snapDir = new File("");

		FileTxnSnapLog result = new FileTxnSnapLog(dataDir, snapDir);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertNotNull(result);
	}

	/**
	 * Run the FileTxnSnapLog(File,File) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testFileTxnSnapLog_5()
		throws Exception {
		File dataDir = new File("");
		File snapDir = new File("");

		FileTxnSnapLog result = new FileTxnSnapLog(dataDir, snapDir);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertNotNull(result);
	}

	/**
	 * Run the FileTxnSnapLog(File,File) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testFileTxnSnapLog_6()
		throws Exception {
		File dataDir = new File("");
		File snapDir = new File("");

		FileTxnSnapLog result = new FileTxnSnapLog(dataDir, snapDir);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertNotNull(result);
	}

	/**
	 * Run the boolean append(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testAppend_1()
		throws Exception {
		FileTxnSnapLog fixture = new FileTxnSnapLog(new File(""), new File(""));
		Request si = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());
		si.txn = new ACL();
		si.hdr = new TxnHeader();

		boolean result = fixture.append(si);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
		assertTrue(result);
	}

	/**
	 * Run the boolean append(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testAppend_2()
		throws Exception {
		FileTxnSnapLog fixture = new FileTxnSnapLog(new File(""), new File(""));
		Request si = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());
		si.txn = new ACL();
		si.hdr = new TxnHeader();

		boolean result = fixture.append(si);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
		assertTrue(result);
	}

	/**
	 * Run the boolean append(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testAppend_3()
		throws Exception {
		FileTxnSnapLog fixture = new FileTxnSnapLog(new File(""), new File(""));
		Request si = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());
		si.txn = new ACL();
		si.hdr = new TxnHeader();

		boolean result = fixture.append(si);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
		assertTrue(result);
	}

	/**
	 * Run the void close() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testClose_1()
		throws Exception {
		FileTxnSnapLog fixture = new FileTxnSnapLog(new File(""), new File(""));

		fixture.close();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void close() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testClose_2()
		throws Exception {
		FileTxnSnapLog fixture = new FileTxnSnapLog(new File(""), new File(""));

		fixture.close();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void close() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testClose_3()
		throws Exception {
		FileTxnSnapLog fixture = new FileTxnSnapLog(new File(""), new File(""));

		fixture.close();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void commit() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testCommit_1()
		throws Exception {
		FileTxnSnapLog fixture = new FileTxnSnapLog(new File(""), new File(""));

		fixture.commit();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void commit() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testCommit_2()
		throws Exception {
		FileTxnSnapLog fixture = new FileTxnSnapLog(new File(""), new File(""));

		fixture.commit();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the File findMostRecentSnapshot() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testFindMostRecentSnapshot_1()
		throws Exception {
		FileTxnSnapLog fixture = new FileTxnSnapLog(new File(""), new File(""));

		File result = fixture.findMostRecentSnapshot();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertNotNull(result);
	}

	/**
	 * Run the File findMostRecentSnapshot() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testFindMostRecentSnapshot_2()
		throws Exception {
		FileTxnSnapLog fixture = new FileTxnSnapLog(new File(""), new File(""));

		File result = fixture.findMostRecentSnapshot();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertNotNull(result);
	}

	/**
	 * Run the List<File> findNRecentSnapshots(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testFindNRecentSnapshots_1()
		throws Exception {
		FileTxnSnapLog fixture = new FileTxnSnapLog(new File(""), new File(""));
		int n = 1;

		List<File> result = fixture.findNRecentSnapshots(n);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertNotNull(result);
	}

	/**
	 * Run the List<File> findNRecentSnapshots(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testFindNRecentSnapshots_2()
		throws Exception {
		FileTxnSnapLog fixture = new FileTxnSnapLog(new File(""), new File(""));
		int n = 1;

		List<File> result = fixture.findNRecentSnapshots(n);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertNotNull(result);
	}

	/**
	 * Run the File getDataDir() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testGetDataDir_1()
		throws Exception {
		FileTxnSnapLog fixture = new FileTxnSnapLog(new File(""), new File(""));

		File result = fixture.getDataDir();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertNotNull(result);
	}

	/**
	 * Run the long getLastLoggedZxid() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testGetLastLoggedZxid_1()
		throws Exception {
		FileTxnSnapLog fixture = new FileTxnSnapLog(new File(""), new File(""));

		long result = fixture.getLastLoggedZxid();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertEquals(0L, result);
	}

	/**
	 * Run the File getSnapDir() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testGetSnapDir_1()
		throws Exception {
		FileTxnSnapLog fixture = new FileTxnSnapLog(new File(""), new File(""));

		File result = fixture.getSnapDir();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertNotNull(result);
	}

	/**
	 * Run the File[] getSnapshotLogs(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testGetSnapshotLogs_1()
		throws Exception {
		FileTxnSnapLog fixture = new FileTxnSnapLog(new File(""), new File(""));
		long zxid = 1L;

		File[] result = fixture.getSnapshotLogs(zxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertNotNull(result);
	}

	/**
	 * Run the void processTransaction(TxnHeader,DataTree,Map<Long,Integer>,Record) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testProcessTransaction_1()
		throws Exception {
		FileTxnSnapLog fixture = new FileTxnSnapLog(new File(""), new File(""));
		TxnHeader hdr = new TxnHeader(1L, 1, 1L, 1L, 2);
		DataTree dt = new DataTree();
		Map<Long, Integer> sessions = new HashMap();
		Record txn = new ACL();

		fixture.processTransaction(hdr, dt, sessions, txn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void processTransaction(TxnHeader,DataTree,Map<Long,Integer>,Record) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testProcessTransaction_2()
		throws Exception {
		FileTxnSnapLog fixture = new FileTxnSnapLog(new File(""), new File(""));
		TxnHeader hdr = new TxnHeader(1L, 1, 1L, 1L, -10);
		DataTree dt = new DataTree();
		Map<Long, Integer> sessions = new HashMap();
		Record txn = new CreateSessionTxn(1);

		fixture.processTransaction(hdr, dt, sessions, txn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void processTransaction(TxnHeader,DataTree,Map<Long,Integer>,Record) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testProcessTransaction_3()
		throws Exception {
		FileTxnSnapLog fixture = new FileTxnSnapLog(new File(""), new File(""));
		TxnHeader hdr = new TxnHeader(1L, 1, 1L, 1L, -10);
		DataTree dt = new DataTree();
		Map<Long, Integer> sessions = new HashMap();
		Record txn = new CreateSessionTxn(1);

		fixture.processTransaction(hdr, dt, sessions, txn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void processTransaction(TxnHeader,DataTree,Map<Long,Integer>,Record) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testProcessTransaction_4()
		throws Exception {
		FileTxnSnapLog fixture = new FileTxnSnapLog(new File(""), new File(""));
		TxnHeader hdr = new TxnHeader(1L, 1, 1L, 1L, -11);
		DataTree dt = new DataTree();
		Map<Long, Integer> sessions = new HashMap();
		Record txn = new ACL();

		fixture.processTransaction(hdr, dt, sessions, txn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void processTransaction(TxnHeader,DataTree,Map<Long,Integer>,Record) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testProcessTransaction_5()
		throws Exception {
		FileTxnSnapLog fixture = new FileTxnSnapLog(new File(""), new File(""));
		TxnHeader hdr = new TxnHeader(1L, 1, 1L, 1L, -11);
		DataTree dt = new DataTree();
		Map<Long, Integer> sessions = new HashMap();
		Record txn = new ACL();

		fixture.processTransaction(hdr, dt, sessions, txn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void processTransaction(TxnHeader,DataTree,Map<Long,Integer>,Record) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testProcessTransaction_6()
		throws Exception {
		FileTxnSnapLog fixture = new FileTxnSnapLog(new File(""), new File(""));
		TxnHeader hdr = new TxnHeader(1L, 1, 1L, 1L, 1);
		DataTree dt = new DataTree();
		Map<Long, Integer> sessions = new HashMap();
		Record txn = new ACL();

		fixture.processTransaction(hdr, dt, sessions, txn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void processTransaction(TxnHeader,DataTree,Map<Long,Integer>,Record) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testProcessTransaction_7()
		throws Exception {
		FileTxnSnapLog fixture = new FileTxnSnapLog(new File(""), new File(""));
		TxnHeader hdr = new TxnHeader(1L, 1, 1L, 1L, 1);
		DataTree dt = new DataTree();
		Map<Long, Integer> sessions = new HashMap();
		Record txn = new ACL();

		fixture.processTransaction(hdr, dt, sessions, txn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void processTransaction(TxnHeader,DataTree,Map<Long,Integer>,Record) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testProcessTransaction_8()
		throws Exception {
		FileTxnSnapLog fixture = new FileTxnSnapLog(new File(""), new File(""));
		TxnHeader hdr = new TxnHeader(1L, 1, 1L, 1L, -10);
		DataTree dt = new DataTree();
		Map<Long, Integer> sessions = new HashMap();
		Record txn = new CreateSessionTxn(1);

		fixture.processTransaction(hdr, dt, sessions, txn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void processTransaction(TxnHeader,DataTree,Map<Long,Integer>,Record) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testProcessTransaction_9()
		throws Exception {
		FileTxnSnapLog fixture = new FileTxnSnapLog(new File(""), new File(""));
		TxnHeader hdr = new TxnHeader(1L, 1, 1L, 1L, -10);
		DataTree dt = new DataTree();
		Map<Long, Integer> sessions = new HashMap();
		Record txn = new CreateSessionTxn(1);

		fixture.processTransaction(hdr, dt, sessions, txn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void processTransaction(TxnHeader,DataTree,Map<Long,Integer>,Record) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testProcessTransaction_10()
		throws Exception {
		FileTxnSnapLog fixture = new FileTxnSnapLog(new File(""), new File(""));
		TxnHeader hdr = new TxnHeader(1L, 1, 1L, 1L, -11);
		DataTree dt = new DataTree();
		Map<Long, Integer> sessions = new HashMap();
		Record txn = new ACL();

		fixture.processTransaction(hdr, dt, sessions, txn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void processTransaction(TxnHeader,DataTree,Map<Long,Integer>,Record) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testProcessTransaction_11()
		throws Exception {
		FileTxnSnapLog fixture = new FileTxnSnapLog(new File(""), new File(""));
		TxnHeader hdr = new TxnHeader(1L, 1, 1L, 1L, -11);
		DataTree dt = new DataTree();
		Map<Long, Integer> sessions = new HashMap();
		Record txn = new ACL();

		fixture.processTransaction(hdr, dt, sessions, txn);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the long restore(DataTree,Map<Long,Integer>,PlayBackListener) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testRestore_1()
		throws Exception {
		FileTxnSnapLog fixture = new FileTxnSnapLog(new File(""), new File(""));
		DataTree dt = new DataTree();
		Map<Long, Integer> sessions = new HashMap();
		org.apache.zookeeper.server.persistence.FileTxnSnapLog.PlayBackListener listener = null;

		long result = fixture.restore(dt, sessions, listener);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertEquals(0L, result);
	}

	/**
	 * Run the long restore(DataTree,Map<Long,Integer>,PlayBackListener) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testRestore_2()
		throws Exception {
		FileTxnSnapLog fixture = new FileTxnSnapLog(new File(""), new File(""));
		DataTree dt = new DataTree();
		dt.lastProcessedZxid = 1L;
		Map<Long, Integer> sessions = new HashMap();
		org.apache.zookeeper.server.persistence.FileTxnSnapLog.PlayBackListener listener = null;

		long result = fixture.restore(dt, sessions, listener);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertEquals(0L, result);
	}

	/**
	 * Run the long restore(DataTree,Map<Long,Integer>,PlayBackListener) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testRestore_3()
		throws Exception {
		FileTxnSnapLog fixture = new FileTxnSnapLog(new File(""), new File(""));
		DataTree dt = new DataTree();
		dt.lastProcessedZxid = 1L;
		Map<Long, Integer> sessions = new HashMap();
		org.apache.zookeeper.server.persistence.FileTxnSnapLog.PlayBackListener listener = null;

		long result = fixture.restore(dt, sessions, listener);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertEquals(0L, result);
	}

	/**
	 * Run the long restore(DataTree,Map<Long,Integer>,PlayBackListener) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testRestore_4()
		throws Exception {
		FileTxnSnapLog fixture = new FileTxnSnapLog(new File(""), new File(""));
		DataTree dt = new DataTree();
		dt.lastProcessedZxid = 0;
		Map<Long, Integer> sessions = new HashMap();
		org.apache.zookeeper.server.persistence.FileTxnSnapLog.PlayBackListener listener = null;

		long result = fixture.restore(dt, sessions, listener);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertEquals(0L, result);
	}

	/**
	 * Run the long restore(DataTree,Map<Long,Integer>,PlayBackListener) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testRestore_5()
		throws Exception {
		FileTxnSnapLog fixture = new FileTxnSnapLog(new File(""), new File(""));
		DataTree dt = new DataTree();
		dt.lastProcessedZxid = 1L;
		Map<Long, Integer> sessions = new HashMap();
		org.apache.zookeeper.server.persistence.FileTxnSnapLog.PlayBackListener listener = null;

		long result = fixture.restore(dt, sessions, listener);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertEquals(0L, result);
	}

	/**
	 * Run the long restore(DataTree,Map<Long,Integer>,PlayBackListener) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testRestore_6()
		throws Exception {
		FileTxnSnapLog fixture = new FileTxnSnapLog(new File(""), new File(""));
		DataTree dt = new DataTree();
		dt.lastProcessedZxid = 1L;
		Map<Long, Integer> sessions = new HashMap();
		org.apache.zookeeper.server.persistence.FileTxnSnapLog.PlayBackListener listener = null;

		long result = fixture.restore(dt, sessions, listener);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertEquals(0L, result);
	}

	/**
	 * Run the long restore(DataTree,Map<Long,Integer>,PlayBackListener) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testRestore_7()
		throws Exception {
		FileTxnSnapLog fixture = new FileTxnSnapLog(new File(""), new File(""));
		DataTree dt = new DataTree();
		dt.lastProcessedZxid = 1L;
		Map<Long, Integer> sessions = new HashMap();
		org.apache.zookeeper.server.persistence.FileTxnSnapLog.PlayBackListener listener = null;

		long result = fixture.restore(dt, sessions, listener);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertEquals(0L, result);
	}

	/**
	 * Run the long restore(DataTree,Map<Long,Integer>,PlayBackListener) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testRestore_8()
		throws Exception {
		FileTxnSnapLog fixture = new FileTxnSnapLog(new File(""), new File(""));
		DataTree dt = new DataTree();
		dt.lastProcessedZxid = 1L;
		Map<Long, Integer> sessions = new HashMap();
		org.apache.zookeeper.server.persistence.FileTxnSnapLog.PlayBackListener listener = null;

		long result = fixture.restore(dt, sessions, listener);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertEquals(0L, result);
	}

	/**
	 * Run the void rollLog() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testRollLog_1()
		throws Exception {
		FileTxnSnapLog fixture = new FileTxnSnapLog(new File(""), new File(""));

		fixture.rollLog();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void rollLog() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testRollLog_2()
		throws Exception {
		FileTxnSnapLog fixture = new FileTxnSnapLog(new File(""), new File(""));

		fixture.rollLog();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void save(DataTree,ConcurrentHashMap<Long,Integer>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testSave_1()
		throws Exception {
		FileTxnSnapLog fixture = new FileTxnSnapLog(new File(""), new File(""));
		DataTree dataTree = new DataTree();
		dataTree.lastProcessedZxid = 1L;
		ConcurrentHashMap<Long, Integer> sessionsWithTimeouts = new ConcurrentHashMap();

		fixture.save(dataTree, sessionsWithTimeouts);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the void save(DataTree,ConcurrentHashMap<Long,Integer>) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testSave_2()
		throws Exception {
		FileTxnSnapLog fixture = new FileTxnSnapLog(new File(""), new File(""));
		DataTree dataTree = new DataTree();
		dataTree.lastProcessedZxid = 1L;
		ConcurrentHashMap<Long, Integer> sessionsWithTimeouts = new ConcurrentHashMap();

		fixture.save(dataTree, sessionsWithTimeouts);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
	}

	/**
	 * Run the boolean truncateLog(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testTruncateLog_1()
		throws Exception {
		FileTxnSnapLog fixture = new FileTxnSnapLog(new File(""), new File(""));
		long zxid = 1L;

		boolean result = fixture.truncateLog(zxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertTrue(result);
	}

	/**
	 * Run the boolean truncateLog(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testTruncateLog_2()
		throws Exception {
		FileTxnSnapLog fixture = new FileTxnSnapLog(new File(""), new File(""));
		long zxid = 1L;

		boolean result = fixture.truncateLog(zxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertTrue(result);
	}

	/**
	 * Run the boolean truncateLog(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testTruncateLog_3()
		throws Exception {
		FileTxnSnapLog fixture = new FileTxnSnapLog(new File(""), new File(""));
		long zxid = 1L;

		boolean result = fixture.truncateLog(zxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertTrue(result);
	}

	/**
	 * Run the boolean truncateLog(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testTruncateLog_4()
		throws Exception {
		FileTxnSnapLog fixture = new FileTxnSnapLog(new File(""), new File(""));
		long zxid = 1L;

		boolean result = fixture.truncateLog(zxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertTrue(result);
	}

	/**
	 * Run the boolean truncateLog(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Test
	public void testTruncateLog_5()
		throws Exception {
		FileTxnSnapLog fixture = new FileTxnSnapLog(new File(""), new File(""));
		long zxid = 1L;

		boolean result = fixture.truncateLog(zxid);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Cannot write to files while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkWrite(CodeProJUnitSecurityManager.java:76)
		//       at java.io.File.mkdir(File.java:1155)
		//       at java.io.File.mkdirs(File.java:1184)
		//       at org.apache.zookeeper.server.persistence.FileTxnSnapLog.<init>(FileTxnSnapLog.java:84)
		assertTrue(result);
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:34 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(FileTxnSnapLogTest.class);
	}
}