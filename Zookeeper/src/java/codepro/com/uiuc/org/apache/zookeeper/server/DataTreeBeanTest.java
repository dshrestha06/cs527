package com.uiuc.org.apache.zookeeper.server;

import org.apache.zookeeper.server.DataTree;
import org.apache.zookeeper.server.DataTreeBean;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>DataTreeBeanTest</code> contains tests for the class <code>{@link DataTreeBean}</code>.
 *
 * @generatedBy CodePro at 10/16/13 8:20 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class DataTreeBeanTest {
	/**
	 * Run the DataTreeBean(DataTree) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:20 PM
	 */
	@Test
	public void testDataTreeBean_1()
		throws Exception {
		DataTree dataTree = new DataTree();

		DataTreeBean result = new DataTreeBean(dataTree);

		// add additional test code here
		assertNotNull(result);
		assertEquals("InMemoryDataTree", result.getName());
		assertEquals(false, result.isHidden());
		assertEquals("0x0", result.getLastZxid());
		assertEquals(4, result.getNodeCount());
		assertEquals(27L, result.approximateDataSize());
		assertEquals(0, result.countEphemerals());
		assertEquals(0, result.getWatchCount());
	}

	/**
	 * Run the long approximateDataSize() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:20 PM
	 */
	@Test
	public void testApproximateDataSize_1()
		throws Exception {
		DataTreeBean fixture = new DataTreeBean(new DataTree());

		long result = fixture.approximateDataSize();

		// add additional test code here
		assertEquals(27L, result);
	}

	/**
	 * Run the int countEphemerals() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:20 PM
	 */
	@Test
	public void testCountEphemerals_1()
		throws Exception {
		DataTreeBean fixture = new DataTreeBean(new DataTree());

		int result = fixture.countEphemerals();

		// add additional test code here
		assertEquals(0, result);
	}

	/**
	 * Run the String getLastZxid() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:20 PM
	 */
	@Test
	public void testGetLastZxid_1()
		throws Exception {
		DataTreeBean fixture = new DataTreeBean(new DataTree());

		String result = fixture.getLastZxid();

		// add additional test code here
		assertEquals("0x0", result);
	}

	/**
	 * Run the String getName() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:20 PM
	 */
	@Test
	public void testGetName_1()
		throws Exception {
		DataTreeBean fixture = new DataTreeBean(new DataTree());

		String result = fixture.getName();

		// add additional test code here
		assertEquals("InMemoryDataTree", result);
	}

	/**
	 * Run the int getNodeCount() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:20 PM
	 */
	@Test
	public void testGetNodeCount_1()
		throws Exception {
		DataTreeBean fixture = new DataTreeBean(new DataTree());

		int result = fixture.getNodeCount();

		// add additional test code here
		assertEquals(4, result);
	}

	/**
	 * Run the int getWatchCount() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:20 PM
	 */
	@Test
	public void testGetWatchCount_1()
		throws Exception {
		DataTreeBean fixture = new DataTreeBean(new DataTree());

		int result = fixture.getWatchCount();

		// add additional test code here
		assertEquals(0, result);
	}

	/**
	 * Run the boolean isHidden() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:20 PM
	 */
	@Test
	public void testIsHidden_1()
		throws Exception {
		DataTreeBean fixture = new DataTreeBean(new DataTree());

		boolean result = fixture.isHidden();

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:20 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:20 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 8:20 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(DataTreeBeanTest.class);
	}
}