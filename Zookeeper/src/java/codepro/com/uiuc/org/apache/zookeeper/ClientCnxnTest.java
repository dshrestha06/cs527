package com.uiuc.org.apache.zookeeper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.LinkedList;

import org.apache.jute.Record;
import org.apache.zookeeper.AsyncCallback;
import org.apache.zookeeper.ClientCnxn;
import org.apache.zookeeper.ClientWatchManager;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.client.HostProvider;
import org.apache.zookeeper.client.StaticHostProvider;
import org.apache.zookeeper.client.ZooKeeperSaslClient;
import org.apache.zookeeper.data.ACL;
import org.apache.zookeeper.proto.ReplyHeader;
import org.apache.zookeeper.proto.RequestHeader;
import org.apache.zookeeper.test.AsyncHammerTest;
import org.apache.zookeeper.test.system.SimpleClient;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The class <code>ClientCnxnTest</code> contains tests for the class <code>{@link ClientCnxn}</code>.
 *
 * @generatedBy CodePro at 10/16/13 7:23 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class ClientCnxnTest {
	/**
	 * Run the ClientCnxn(String,HostProvider,int,ZooKeeper,ClientWatchManager,ClientCnxnSocket,boolean) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testClientCnxn_1()
		throws Exception {
		String chrootPath = "";
		HostProvider hostProvider = new StaticHostProvider(new LinkedList());
		int sessionTimeout = 1;
		ZooKeeper zooKeeper = new ZooKeeper("", 1, new SimpleClient());
		ClientWatchManager watcher = null;
		boolean canBeReadOnly = true;

		ClientCnxn result = new ClientCnxn(chrootPath, hostProvider, sessionTimeout, zooKeeper, watcher, null, canBeReadOnly);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: A HostProvider may not be empty!
		//       at org.apache.zookeeper.client.StaticHostProvider.<init>(StaticHostProvider.java:69)
		assertNotNull(result);
	}

	/**
	 * Run the ClientCnxn(String,HostProvider,int,ZooKeeper,ClientWatchManager,ClientCnxnSocket,long,byte[],boolean) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testClientCnxn_2()
		throws Exception {
		String chrootPath = "";
		HostProvider hostProvider = new StaticHostProvider(new LinkedList());
		int sessionTimeout = 1;
		ZooKeeper zooKeeper = new ZooKeeper("", 1, new SimpleClient());
		ClientWatchManager watcher = null;
		long sessionId = 1L;
		byte[] sessionPasswd = new byte[] {};
		boolean canBeReadOnly = true;

		ClientCnxn result = new ClientCnxn(chrootPath, hostProvider, sessionTimeout, zooKeeper, watcher, null, sessionId, sessionPasswd, canBeReadOnly);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: A HostProvider may not be empty!
		//       at org.apache.zookeeper.client.StaticHostProvider.<init>(StaticHostProvider.java:69)
		assertNotNull(result);
	}

	/**
	 * Run the void addAuthInfo(String,byte[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testAddAuthInfo_1()
		throws Exception {
		ClientCnxn fixture = new ClientCnxn("", new StaticHostProvider(new LinkedList()), 1, new ZooKeeper("", 1, new SimpleClient()), (ClientWatchManager) null, null, 1L, new byte[] {}, true);
		fixture.zooKeeperSaslClient = new ZooKeeperSaslClient("");
		fixture.getXid();
		String scheme = "";
		byte[] auth = new byte[] {};

		fixture.addAuthInfo(scheme, auth);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: A HostProvider may not be empty!
		//       at org.apache.zookeeper.client.StaticHostProvider.<init>(StaticHostProvider.java:69)
	}

	/**
	 * Run the void addAuthInfo(String,byte[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testAddAuthInfo_2()
		throws Exception {
		ClientCnxn fixture = new ClientCnxn("", new StaticHostProvider(new LinkedList()), 1, new ZooKeeper("", 1, new SimpleClient()), (ClientWatchManager) null, null, 1L, new byte[] {}, true);
		fixture.zooKeeperSaslClient = new ZooKeeperSaslClient("");
		fixture.getXid();
		String scheme = "";
		byte[] auth = new byte[] {};

		fixture.addAuthInfo(scheme, auth);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: A HostProvider may not be empty!
		//       at org.apache.zookeeper.client.StaticHostProvider.<init>(StaticHostProvider.java:69)
	}

	/**
	 * Run the void close() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testClose_1()
		throws Exception {
		ClientCnxn fixture = new ClientCnxn("", new StaticHostProvider(new LinkedList()), 1, new ZooKeeper("", 1, new SimpleClient()), (ClientWatchManager) null, null, 1L, new byte[] {}, true);
		fixture.zooKeeperSaslClient = new ZooKeeperSaslClient("");
		fixture.getXid();

		fixture.close();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: A HostProvider may not be empty!
		//       at org.apache.zookeeper.client.StaticHostProvider.<init>(StaticHostProvider.java:69)
	}

	/**
	 * Run the void close() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testClose_2()
		throws Exception {
		ClientCnxn fixture = new ClientCnxn("", new StaticHostProvider(new LinkedList()), 1, new ZooKeeper("", 1, new SimpleClient()), (ClientWatchManager) null, null, 1L, new byte[] {}, true);
		fixture.zooKeeperSaslClient = new ZooKeeperSaslClient("");
		fixture.getXid();

		fixture.close();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: A HostProvider may not be empty!
		//       at org.apache.zookeeper.client.StaticHostProvider.<init>(StaticHostProvider.java:69)
	}

	/**
	 * Run the void close() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testClose_3()
		throws Exception {
		ClientCnxn fixture = new ClientCnxn("", new StaticHostProvider(new LinkedList()), 1, new ZooKeeper("", 1, new SimpleClient()), (ClientWatchManager) null, null, 1L, new byte[] {}, true);
		fixture.zooKeeperSaslClient = new ZooKeeperSaslClient("");
		fixture.getXid();

		fixture.close();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: A HostProvider may not be empty!
		//       at org.apache.zookeeper.client.StaticHostProvider.<init>(StaticHostProvider.java:69)
	}

	/**
	 * Run the void disconnect() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testDisconnect_1()
		throws Exception {
		ClientCnxn fixture = new ClientCnxn("", new StaticHostProvider(new LinkedList()), 1, new ZooKeeper("", 1, new SimpleClient()), (ClientWatchManager) null, null, 1L, new byte[] {}, true);
		fixture.zooKeeperSaslClient = new ZooKeeperSaslClient("");
		fixture.getXid();

		fixture.disconnect();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: A HostProvider may not be empty!
		//       at org.apache.zookeeper.client.StaticHostProvider.<init>(StaticHostProvider.java:69)
	}

	/**
	 * Run the void disconnect() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testDisconnect_2()
		throws Exception {
		ClientCnxn fixture = new ClientCnxn("", new StaticHostProvider(new LinkedList()), 1, new ZooKeeper("", 1, new SimpleClient()), (ClientWatchManager) null, null, 1L, new byte[] {}, true);
		fixture.zooKeeperSaslClient = new ZooKeeperSaslClient("");
		fixture.getXid();

		fixture.disconnect();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: A HostProvider may not be empty!
		//       at org.apache.zookeeper.client.StaticHostProvider.<init>(StaticHostProvider.java:69)
	}

	/**
	 * Run the void enableWrite() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testEnableWrite_1()
		throws Exception {
		ClientCnxn fixture = new ClientCnxn("", new StaticHostProvider(new LinkedList()), 1, new ZooKeeper("", 1, new SimpleClient()), (ClientWatchManager) null, null, 1L, new byte[] {}, true);
		fixture.zooKeeperSaslClient = new ZooKeeperSaslClient("");
		fixture.getXid();

		fixture.enableWrite();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: A HostProvider may not be empty!
		//       at org.apache.zookeeper.client.StaticHostProvider.<init>(StaticHostProvider.java:69)
	}

	/**
	 * Run the boolean getDisableAutoResetWatch() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testGetDisableAutoResetWatch_1()
		throws Exception {

		boolean result = ClientCnxn.getDisableAutoResetWatch();

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean getDisableAutoResetWatch() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testGetDisableAutoResetWatch_2()
		throws Exception {

		boolean result = ClientCnxn.getDisableAutoResetWatch();

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the long getLastZxid() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testGetLastZxid_1()
		throws Exception {
		ClientCnxn fixture = new ClientCnxn("", new StaticHostProvider(new LinkedList()), 1, new ZooKeeper("", 1, new SimpleClient()), (ClientWatchManager) null, null, 1L, new byte[] {}, true);
		fixture.zooKeeperSaslClient = new ZooKeeperSaslClient("");
		fixture.getXid();

		long result = fixture.getLastZxid();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: A HostProvider may not be empty!
		//       at org.apache.zookeeper.client.StaticHostProvider.<init>(StaticHostProvider.java:69)
		assertEquals(0L, result);
	}

	/**
	 * Run the long getSessionId() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testGetSessionId_1()
		throws Exception {
		ClientCnxn fixture = new ClientCnxn("", new StaticHostProvider(new LinkedList()), 1, new ZooKeeper("", 1, new SimpleClient()), (ClientWatchManager) null, null, 1L, new byte[] {}, true);
		fixture.zooKeeperSaslClient = new ZooKeeperSaslClient("");
		fixture.getXid();

		long result = fixture.getSessionId();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: A HostProvider may not be empty!
		//       at org.apache.zookeeper.client.StaticHostProvider.<init>(StaticHostProvider.java:69)
		assertEquals(0L, result);
	}

	/**
	 * Run the byte[] getSessionPasswd() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testGetSessionPasswd_1()
		throws Exception {
		ClientCnxn fixture = new ClientCnxn("", new StaticHostProvider(new LinkedList()), 1, new ZooKeeper("", 1, new SimpleClient()), (ClientWatchManager) null, null, 1L, new byte[] {}, true);
		fixture.zooKeeperSaslClient = new ZooKeeperSaslClient("");
		fixture.getXid();

		byte[] result = fixture.getSessionPasswd();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: A HostProvider may not be empty!
		//       at org.apache.zookeeper.client.StaticHostProvider.<init>(StaticHostProvider.java:69)
		assertNotNull(result);
	}

	/**
	 * Run the int getSessionTimeout() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testGetSessionTimeout_1()
		throws Exception {
		ClientCnxn fixture = new ClientCnxn("", new StaticHostProvider(new LinkedList()), 1, new ZooKeeper("", 1, new SimpleClient()), (ClientWatchManager) null, null, 1L, new byte[] {}, true);
		fixture.zooKeeperSaslClient = new ZooKeeperSaslClient("");
		fixture.getXid();

		int result = fixture.getSessionTimeout();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: A HostProvider may not be empty!
		//       at org.apache.zookeeper.client.StaticHostProvider.<init>(StaticHostProvider.java:69)
		assertEquals(0, result);
	}

	/**
	 * Run the org.apache.zookeeper.ZooKeeper.States getState() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testGetState_1()
		throws Exception {
		ClientCnxn fixture = new ClientCnxn("", new StaticHostProvider(new LinkedList()), 1, new ZooKeeper("", 1, new SimpleClient()), (ClientWatchManager) null, null, 1L, new byte[] {}, true);
		fixture.zooKeeperSaslClient = new ZooKeeperSaslClient("");
		fixture.getXid();

		org.apache.zookeeper.ZooKeeper.States result = fixture.getState();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: A HostProvider may not be empty!
		//       at org.apache.zookeeper.client.StaticHostProvider.<init>(StaticHostProvider.java:69)
		assertNotNull(result);
	}

	/**
	 * Run the int getXid() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testGetXid_1()
		throws Exception {
		ClientCnxn fixture = new ClientCnxn("", new StaticHostProvider(new LinkedList()), 1, new ZooKeeper("", 1, new SimpleClient()), (ClientWatchManager) null, null, 1L, new byte[] {}, true);
		fixture.zooKeeperSaslClient = new ZooKeeperSaslClient("");
		fixture.getXid();

		int result = fixture.getXid();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: A HostProvider may not be empty!
		//       at org.apache.zookeeper.client.StaticHostProvider.<init>(StaticHostProvider.java:69)
		assertEquals(0, result);
	}

	/**
	 * Run the org.apache.zookeeper.ClientCnxn.Packet queuePacket(RequestHeader,ReplyHeader,Record,Record,AsyncCallback,String,String,Object,WatchRegistration) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testQueuePacket_1()
		throws Exception {
		ClientCnxn fixture = new ClientCnxn("", new StaticHostProvider(new LinkedList()), 1, new ZooKeeper("", 1, new SimpleClient()), (ClientWatchManager) null, null, 1L, new byte[] {}, true);
		fixture.zooKeeperSaslClient = new ZooKeeperSaslClient("");
		fixture.getXid();
		RequestHeader h = new RequestHeader(1, -11);
		ReplyHeader r = new ReplyHeader();
		Record request = new ACL();
		Record response = new ACL();
		AsyncCallback cb = new AsyncHammerTest();
		String clientPath = "";
		String serverPath = "";
		Object ctx = new Object();

		org.apache.zookeeper.ClientCnxn.Packet result = fixture.queuePacket(h, r, request, response, cb, clientPath, serverPath, ctx, null);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: A HostProvider may not be empty!
		//       at org.apache.zookeeper.client.StaticHostProvider.<init>(StaticHostProvider.java:69)
		assertNotNull(result);
	}

	/**
	 * Run the org.apache.zookeeper.ClientCnxn.Packet queuePacket(RequestHeader,ReplyHeader,Record,Record,AsyncCallback,String,String,Object,WatchRegistration) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testQueuePacket_2()
		throws Exception {
		ClientCnxn fixture = new ClientCnxn("", new StaticHostProvider(new LinkedList()), 1, new ZooKeeper("", 1, new SimpleClient()), (ClientWatchManager) null, null, 1L, new byte[] {}, true);
		fixture.zooKeeperSaslClient = new ZooKeeperSaslClient("");
		fixture.getXid();
		RequestHeader h = new RequestHeader(1, 1);
		ReplyHeader r = new ReplyHeader();
		Record request = new ACL();
		Record response = new ACL();
		AsyncCallback cb = new AsyncHammerTest();
		String clientPath = "";
		String serverPath = "";
		Object ctx = new Object();

		org.apache.zookeeper.ClientCnxn.Packet result = fixture.queuePacket(h, r, request, response, cb, clientPath, serverPath, ctx, null);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: A HostProvider may not be empty!
		//       at org.apache.zookeeper.client.StaticHostProvider.<init>(StaticHostProvider.java:69)
		assertNotNull(result);
	}

	/**
	 * Run the org.apache.zookeeper.ClientCnxn.Packet queuePacket(RequestHeader,ReplyHeader,Record,Record,AsyncCallback,String,String,Object,WatchRegistration) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testQueuePacket_3()
		throws Exception {
		ClientCnxn fixture = new ClientCnxn("", new StaticHostProvider(new LinkedList()), 1, new ZooKeeper("", 1, new SimpleClient()), (ClientWatchManager) null, null, 1L, new byte[] {}, true);
		fixture.zooKeeperSaslClient = new ZooKeeperSaslClient("");
		fixture.getXid();
		RequestHeader h = new RequestHeader();
		ReplyHeader r = new ReplyHeader();
		Record request = new ACL();
		Record response = new ACL();
		AsyncCallback cb = new AsyncHammerTest();
		String clientPath = "";
		String serverPath = "";
		Object ctx = new Object();

		org.apache.zookeeper.ClientCnxn.Packet result = fixture.queuePacket(h, r, request, response, cb, clientPath, serverPath, ctx, null);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: A HostProvider may not be empty!
		//       at org.apache.zookeeper.client.StaticHostProvider.<init>(StaticHostProvider.java:69)
		assertNotNull(result);
	}

	/**
	 * Run the org.apache.zookeeper.ClientCnxn.Packet queuePacket(RequestHeader,ReplyHeader,Record,Record,AsyncCallback,String,String,Object,WatchRegistration) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testQueuePacket_4()
		throws Exception {
		ClientCnxn fixture = new ClientCnxn("", new StaticHostProvider(new LinkedList()), 1, new ZooKeeper("", 1, new SimpleClient()), (ClientWatchManager) null, null, 1L, new byte[] {}, true);
		fixture.zooKeeperSaslClient = new ZooKeeperSaslClient("");
		fixture.getXid();
		RequestHeader h = new RequestHeader();
		ReplyHeader r = new ReplyHeader();
		Record request = new ACL();
		Record response = new ACL();
		AsyncCallback cb = new AsyncHammerTest();
		String clientPath = "";
		String serverPath = "";
		Object ctx = new Object();

		org.apache.zookeeper.ClientCnxn.Packet result = fixture.queuePacket(h, r, request, response, cb, clientPath, serverPath, ctx, null);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: A HostProvider may not be empty!
		//       at org.apache.zookeeper.client.StaticHostProvider.<init>(StaticHostProvider.java:69)
		assertNotNull(result);
	}

	/**
	 * Run the void sendPacket(Record,Record,AsyncCallback,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testSendPacket_1()
		throws Exception {
		ClientCnxn fixture = new ClientCnxn("", new StaticHostProvider(new LinkedList()), 1, new ZooKeeper("", 1, new SimpleClient()), (ClientWatchManager) null, null, 1L, new byte[] {}, true);
		fixture.zooKeeperSaslClient = new ZooKeeperSaslClient("");
		fixture.getXid();
		Record request = new ACL();
		Record response = new ACL();
		AsyncCallback cb = new AsyncHammerTest();
		int opCode = 1;

		fixture.sendPacket(request, response, cb, opCode);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: A HostProvider may not be empty!
		//       at org.apache.zookeeper.client.StaticHostProvider.<init>(StaticHostProvider.java:69)
	}

	/**
	 * Run the void sendPacket(Record,Record,AsyncCallback,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testSendPacket_2()
		throws Exception {
		ClientCnxn fixture = new ClientCnxn("", new StaticHostProvider(new LinkedList()), 1, new ZooKeeper("", 1, new SimpleClient()), (ClientWatchManager) null, null, 1L, new byte[] {}, true);
		fixture.zooKeeperSaslClient = new ZooKeeperSaslClient("");
		fixture.getXid();
		Record request = new ACL();
		Record response = new ACL();
		AsyncCallback cb = new AsyncHammerTest();
		int opCode = 1;

		fixture.sendPacket(request, response, cb, opCode);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: A HostProvider may not be empty!
		//       at org.apache.zookeeper.client.StaticHostProvider.<init>(StaticHostProvider.java:69)
	}

	/**
	 * Run the void setDisableAutoResetWatch(boolean) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testSetDisableAutoResetWatch_1()
		throws Exception {
		boolean b = true;

		ClientCnxn.setDisableAutoResetWatch(b);

		// add additional test code here
	}

	/**
	 * Run the void start() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testStart_1()
		throws Exception {
		ClientCnxn fixture = new ClientCnxn("", new StaticHostProvider(new LinkedList()), 1, new ZooKeeper("", 1, new SimpleClient()), (ClientWatchManager) null, null, 1L, new byte[] {}, true);
		fixture.zooKeeperSaslClient = new ZooKeeperSaslClient("");
		fixture.getXid();

		fixture.start();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: A HostProvider may not be empty!
		//       at org.apache.zookeeper.client.StaticHostProvider.<init>(StaticHostProvider.java:69)
	}

	/**
	 * Run the ReplyHeader submitRequest(RequestHeader,Record,Record,WatchRegistration) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testSubmitRequest_1()
		throws Exception {
		ClientCnxn fixture = new ClientCnxn("", new StaticHostProvider(new LinkedList()), 1, new ZooKeeper("", 1, new SimpleClient()), (ClientWatchManager) null, null, 1L, new byte[] {}, true);
		fixture.zooKeeperSaslClient = new ZooKeeperSaslClient("");
		fixture.getXid();
		RequestHeader h = new RequestHeader();
		Record request = new ACL();
		Record response = new ACL();

		ReplyHeader result = fixture.submitRequest(h, request, response, null);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: A HostProvider may not be empty!
		//       at org.apache.zookeeper.client.StaticHostProvider.<init>(StaticHostProvider.java:69)
		assertNotNull(result);
	}

	/**
	 * Run the ReplyHeader submitRequest(RequestHeader,Record,Record,WatchRegistration) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testSubmitRequest_2()
		throws Exception {
		ClientCnxn fixture = new ClientCnxn("", new StaticHostProvider(new LinkedList()), 1, new ZooKeeper("", 1, new SimpleClient()), (ClientWatchManager) null, null, 1L, new byte[] {}, true);
		fixture.zooKeeperSaslClient = new ZooKeeperSaslClient("");
		fixture.getXid();
		RequestHeader h = new RequestHeader();
		Record request = new ACL();
		Record response = new ACL();

		ReplyHeader result = fixture.submitRequest(h, request, response, null);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: A HostProvider may not be empty!
		//       at org.apache.zookeeper.client.StaticHostProvider.<init>(StaticHostProvider.java:69)
		assertNotNull(result);
	}

	/**
	 * Run the ReplyHeader submitRequest(RequestHeader,Record,Record,WatchRegistration) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testSubmitRequest_3()
		throws Exception {
		ClientCnxn fixture = new ClientCnxn("", new StaticHostProvider(new LinkedList()), 1, new ZooKeeper("", 1, new SimpleClient()), (ClientWatchManager) null, null, 1L, new byte[] {}, true);
		fixture.zooKeeperSaslClient = new ZooKeeperSaslClient("");
		fixture.getXid();
		RequestHeader h = new RequestHeader();
		Record request = new ACL();
		Record response = new ACL();

		ReplyHeader result = fixture.submitRequest(h, request, response, null);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: A HostProvider may not be empty!
		//       at org.apache.zookeeper.client.StaticHostProvider.<init>(StaticHostProvider.java:69)
		assertNotNull(result);
	}

	/**
	 * Run the String toString() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Test
	public void testToString_1()
		throws Exception {
		ClientCnxn fixture = new ClientCnxn("", new StaticHostProvider(new LinkedList()), 1, new ZooKeeper("", 1, new SimpleClient()), (ClientWatchManager) null, null, 1L, new byte[] {}, true);
		fixture.zooKeeperSaslClient = new ZooKeeperSaslClient("");
		fixture.getXid();

		String result = fixture.toString();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: A HostProvider may not be empty!
		//       at org.apache.zookeeper.client.StaticHostProvider.<init>(StaticHostProvider.java:69)
		assertNotNull(result);
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:23 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(ClientCnxnTest.class);
	}
}