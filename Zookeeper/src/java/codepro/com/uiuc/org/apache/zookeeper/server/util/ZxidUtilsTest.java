package com.uiuc.org.apache.zookeeper.server.util;

import org.apache.zookeeper.server.util.ZxidUtils;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>ZxidUtilsTest</code> contains tests for the class <code>{@link ZxidUtils}</code>.
 *
 * @generatedBy CodePro at 10/16/13 7:40 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class ZxidUtilsTest {
	/**
	 * Run the long getCounterFromZxid(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test
	public void testGetCounterFromZxid_1()
		throws Exception {
		long zxid = 1L;

		long result = ZxidUtils.getCounterFromZxid(zxid);

		// add additional test code here
		assertEquals(1L, result);
	}

	/**
	 * Run the long getEpochFromZxid(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test
	public void testGetEpochFromZxid_1()
		throws Exception {
		long zxid = 1L;

		long result = ZxidUtils.getEpochFromZxid(zxid);

		// add additional test code here
		assertEquals(0L, result);
	}

	/**
	 * Run the long makeZxid(long,long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test
	public void testMakeZxid_1()
		throws Exception {
		long epoch = 1L;
		long counter = 1L;

		long result = ZxidUtils.makeZxid(epoch, counter);

		// add additional test code here
		assertEquals(4294967297L, result);
	}

	/**
	 * Run the String zxidToString(long) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test
	public void testZxidToString_1()
		throws Exception {
		long zxid = 1L;

		String result = ZxidUtils.zxidToString(zxid);

		// add additional test code here
		assertEquals("1", result);
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(ZxidUtilsTest.class);
	}
}