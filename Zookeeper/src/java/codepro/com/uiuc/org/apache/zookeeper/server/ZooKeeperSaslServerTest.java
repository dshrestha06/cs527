package com.uiuc.org.apache.zookeeper.server;

import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.login.Configuration;
import org.apache.zookeeper.JaasConfiguration;
import org.apache.zookeeper.Login;
import org.apache.zookeeper.server.ZooKeeperSaslServer;
import org.apache.zookeeper.server.auth.SaslServerCallbackHandler;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>ZooKeeperSaslServerTest</code> contains tests for the class <code>{@link ZooKeeperSaslServer}</code>.
 *
 * @generatedBy CodePro at 10/16/13 8:27 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class ZooKeeperSaslServerTest {
	/**
	 * Run the ZooKeeperSaslServer(Login) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:27 PM
	 */
	@Test
	public void testZooKeeperSaslServer_1()
		throws Exception {
		Login login = new Login("", new SaslServerCallbackHandler(new JaasConfiguration()));

		ZooKeeperSaslServer result = new ZooKeeperSaslServer(login);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.io.IOException: Could not find a 'Server' entry in this configuration: Server cannot start.
		//       at org.apache.zookeeper.server.auth.SaslServerCallbackHandler.<init>(SaslServerCallbackHandler.java:57)
		assertNotNull(result);
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:27 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:27 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 8:27 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(ZooKeeperSaslServerTest.class);
	}
}