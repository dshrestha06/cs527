package com.uiuc.org.apache.zookeeper.server.quorum;

import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.LinkedList;
import java.util.List;
import org.apache.zookeeper.data.Id;
import org.apache.zookeeper.server.FinalRequestProcessor;
import org.apache.zookeeper.server.NIOServerCnxn;
import org.apache.zookeeper.server.NIOServerCnxnFactory;
import org.apache.zookeeper.server.Request;
import org.apache.zookeeper.server.RequestProcessor;
import org.apache.zookeeper.server.ServerCnxn;
import org.apache.zookeeper.server.ZooKeeperServer;
import org.apache.zookeeper.server.quorum.ReadOnlyRequestProcessor;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>ReadOnlyRequestProcessorTest</code> contains tests for the class <code>{@link ReadOnlyRequestProcessor}</code>.
 *
 * @generatedBy CodePro at 10/16/13 8:14 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class ReadOnlyRequestProcessorTest {
	/**
	 * Run the ReadOnlyRequestProcessor(ZooKeeperServer,RequestProcessor) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testReadOnlyRequestProcessor_1()
		throws Exception {
		ZooKeeperServer zks = new ZooKeeperServer();
		RequestProcessor nextProcessor = new FinalRequestProcessor(new ZooKeeperServer());

		ReadOnlyRequestProcessor result = new ReadOnlyRequestProcessor(zks, nextProcessor);

		// add additional test code here
		assertNotNull(result);
		assertEquals("Thread[ReadOnlyRequestProcessor:0,1,main]", result.toString());
		assertEquals(false, result.isInterrupted());
		assertEquals("ReadOnlyRequestProcessor:0", result.getName());
		assertEquals(0, result.countStackFrames());
		assertEquals(8436L, result.getId());
		assertEquals(1, result.getPriority());
		assertEquals(false, result.isAlive());
		assertEquals(true, result.isDaemon());
	}

	/**
	 * Run the void processRequest(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testProcessRequest_1()
		throws Exception {
		ReadOnlyRequestProcessor fixture = new ReadOnlyRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		fixture.processRequest(new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList()));
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());

		fixture.processRequest(request);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void processRequest(Request) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testProcessRequest_2()
		throws Exception {
		ReadOnlyRequestProcessor fixture = new ReadOnlyRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		fixture.processRequest(new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList()));
		Request request = new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList());

		fixture.processRequest(request);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testRun_1()
		throws Exception {
		ReadOnlyRequestProcessor fixture = new ReadOnlyRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		fixture.processRequest(new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList()));

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testRun_2()
		throws Exception {
		ReadOnlyRequestProcessor fixture = new ReadOnlyRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		fixture.processRequest(new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList()));

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testRun_3()
		throws Exception {
		ReadOnlyRequestProcessor fixture = new ReadOnlyRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		fixture.processRequest(new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList()));

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testRun_4()
		throws Exception {
		ReadOnlyRequestProcessor fixture = new ReadOnlyRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		fixture.processRequest(new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList()));

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testRun_5()
		throws Exception {
		ReadOnlyRequestProcessor fixture = new ReadOnlyRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		fixture.processRequest(new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList()));

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void run() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testRun_6()
		throws Exception {
		ReadOnlyRequestProcessor fixture = new ReadOnlyRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));
		fixture.processRequest(new Request(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), 1L, 1, 1, ByteBuffer.allocate(0), new LinkedList()));

		fixture.run();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void shutdown() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Test
	public void testShutdown_1()
		throws Exception {
		ReadOnlyRequestProcessor fixture = new ReadOnlyRequestProcessor(new ZooKeeperServer(), new FinalRequestProcessor(new ZooKeeperServer()));

		fixture.shutdown();

		// add additional test code here
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 8:14 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(ReadOnlyRequestProcessorTest.class);
	}
}