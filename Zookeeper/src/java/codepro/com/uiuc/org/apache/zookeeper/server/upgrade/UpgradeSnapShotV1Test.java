package com.uiuc.org.apache.zookeeper.server.upgrade;

import java.io.DataInput;
import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PipedInputStream;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.jute.BinaryInputArchive;
import org.apache.jute.InputArchive;
import org.apache.zookeeper.server.DataTree;
import org.apache.zookeeper.server.upgrade.UpgradeSnapShotV1;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>UpgradeSnapShotV1Test</code> contains tests for the class <code>{@link UpgradeSnapShotV1}</code>.
 *
 * @generatedBy CodePro at 10/16/13 7:40 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class UpgradeSnapShotV1Test {
	/**
	 * Run the UpgradeSnapShotV1(File,File) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test
	public void testUpgradeSnapShotV1_1()
		throws Exception {
		File dataDir = new File("");
		File snapShotDir = new File("");

		UpgradeSnapShotV1 result = new UpgradeSnapShotV1(dataDir, snapShotDir);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the DataTree getNewDataTree() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testGetNewDataTree_1()
		throws Exception {
		UpgradeSnapShotV1 fixture = new UpgradeSnapShotV1(new File(""), new File(""));

		DataTree result = fixture.getNewDataTree();

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the DataTree getNewDataTree() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testGetNewDataTree_2()
		throws Exception {
		UpgradeSnapShotV1 fixture = new UpgradeSnapShotV1(new File(""), new File(""));

		DataTree result = fixture.getNewDataTree();

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the DataTree getNewDataTree() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testGetNewDataTree_3()
		throws Exception {
		UpgradeSnapShotV1 fixture = new UpgradeSnapShotV1(new File(""), new File(""));

		DataTree result = fixture.getNewDataTree();

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the ConcurrentHashMap<Long, Integer> getSessionWithTimeOuts() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test
	public void testGetSessionWithTimeOuts_1()
		throws Exception {
		UpgradeSnapShotV1 fixture = new UpgradeSnapShotV1(new File(""), new File(""));

		ConcurrentHashMap<Long, Integer> result = fixture.getSessionWithTimeOuts();

		// add additional test code here
		assertNotNull(result);
		assertEquals(0, result.size());
	}

	/**
	 * Run the long playLog(InputArchive) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testPlayLog_1()
		throws Exception {
		UpgradeSnapShotV1 fixture = new UpgradeSnapShotV1(new File(""), new File(""));
		InputArchive logStream = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));

		long result = fixture.playLog(logStream);

		// add additional test code here
		assertEquals(0L, result);
	}

	/**
	 * Run the long playLog(InputArchive) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testPlayLog_2()
		throws Exception {
		UpgradeSnapShotV1 fixture = new UpgradeSnapShotV1(new File(""), new File(""));
		InputArchive logStream = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));

		long result = fixture.playLog(logStream);

		// add additional test code here
		assertEquals(0L, result);
	}

	/**
	 * Run the long playLog(InputArchive) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testPlayLog_3()
		throws Exception {
		UpgradeSnapShotV1 fixture = new UpgradeSnapShotV1(new File(""), new File(""));
		InputArchive logStream = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));

		long result = fixture.playLog(logStream);

		// add additional test code here
		assertEquals(0L, result);
	}

	/**
	 * Run the long playLog(InputArchive) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testPlayLog_4()
		throws Exception {
		UpgradeSnapShotV1 fixture = new UpgradeSnapShotV1(new File(""), new File(""));
		InputArchive logStream = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));

		long result = fixture.playLog(logStream);

		// add additional test code here
		assertEquals(0L, result);
	}

	/**
	 * Run the long playLog(InputArchive) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testPlayLog_5()
		throws Exception {
		UpgradeSnapShotV1 fixture = new UpgradeSnapShotV1(new File(""), new File(""));
		InputArchive logStream = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));

		long result = fixture.playLog(logStream);

		// add additional test code here
		assertEquals(0L, result);
	}

	/**
	 * Run the long playLog(InputArchive) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testPlayLog_6()
		throws Exception {
		UpgradeSnapShotV1 fixture = new UpgradeSnapShotV1(new File(""), new File(""));
		InputArchive logStream = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));

		long result = fixture.playLog(logStream);

		// add additional test code here
		assertEquals(0L, result);
	}

	/**
	 * Run the long playLog(InputArchive) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testPlayLog_7()
		throws Exception {
		UpgradeSnapShotV1 fixture = new UpgradeSnapShotV1(new File(""), new File(""));
		InputArchive logStream = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));

		long result = fixture.playLog(logStream);

		// add additional test code here
		assertEquals(0L, result);
	}

	/**
	 * Run the long playLog(InputArchive) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testPlayLog_8()
		throws Exception {
		UpgradeSnapShotV1 fixture = new UpgradeSnapShotV1(new File(""), new File(""));
		InputArchive logStream = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));

		long result = fixture.playLog(logStream);

		// add additional test code here
		assertEquals(0L, result);
	}

	/**
	 * Run the long playLog(InputArchive) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testPlayLog_9()
		throws Exception {
		UpgradeSnapShotV1 fixture = new UpgradeSnapShotV1(new File(""), new File(""));
		InputArchive logStream = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));

		long result = fixture.playLog(logStream);

		// add additional test code here
		assertEquals(0L, result);
	}

	/**
	 * Run the long playLog(InputArchive) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testPlayLog_10()
		throws Exception {
		UpgradeSnapShotV1 fixture = new UpgradeSnapShotV1(new File(""), new File(""));
		InputArchive logStream = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));

		long result = fixture.playLog(logStream);

		// add additional test code here
		assertEquals(0L, result);
	}

	/**
	 * Run the long playLog(InputArchive) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testPlayLog_11()
		throws Exception {
		UpgradeSnapShotV1 fixture = new UpgradeSnapShotV1(new File(""), new File(""));
		InputArchive logStream = new BinaryInputArchive(new DataInputStream(new PipedInputStream()));

		long result = fixture.playLog(logStream);

		// add additional test code here
		assertEquals(0L, result);
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:40 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(UpgradeSnapShotV1Test.class);
	}
}