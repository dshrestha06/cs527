package com.uiuc.org.apache.zookeeper.server;

import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import org.apache.zookeeper.server.ConnectionBean;
import org.apache.zookeeper.server.NIOServerCnxn;
import org.apache.zookeeper.server.NIOServerCnxnFactory;
import org.apache.zookeeper.server.ServerCnxn;
import org.apache.zookeeper.server.ZooKeeperServer;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>ConnectionBeanTest</code> contains tests for the class <code>{@link ConnectionBean}</code>.
 *
 * @generatedBy CodePro at 10/16/13 8:18 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class ConnectionBeanTest {
	/**
	 * Run the ConnectionBean(ServerCnxn,ZooKeeperServer) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:18 PM
	 */
	@Test
	public void testConnectionBean_1()
		throws Exception {
		ServerCnxn connection = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		ZooKeeperServer zk = new ZooKeeperServer();

		ConnectionBean result = new ConnectionBean(connection, zk);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
		assertNotNull(result);
	}

	/**
	 * Run the ConnectionBean(ServerCnxn,ZooKeeperServer) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:18 PM
	 */
	@Test
	public void testConnectionBean_2()
		throws Exception {
		ServerCnxn connection = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		ZooKeeperServer zk = new ZooKeeperServer();

		ConnectionBean result = new ConnectionBean(connection, zk);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
		assertNotNull(result);
	}

	/**
	 * Run the ConnectionBean(ServerCnxn,ZooKeeperServer) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:18 PM
	 */
	@Test
	public void testConnectionBean_3()
		throws Exception {
		ServerCnxn connection = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		ZooKeeperServer zk = new ZooKeeperServer();

		ConnectionBean result = new ConnectionBean(connection, zk);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
		assertNotNull(result);
	}

	/**
	 * Run the ConnectionBean(ServerCnxn,ZooKeeperServer) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:18 PM
	 */
	@Test(expected = java.lang.NullPointerException.class)
	public void testConnectionBean_4()
		throws Exception {
		ServerCnxn connection = new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory());
		ZooKeeperServer zk = new ZooKeeperServer();

		ConnectionBean result = new ConnectionBean(connection, zk);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the long getAvgLatency() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:18 PM
	 */
	@Test
	public void testGetAvgLatency_1()
		throws Exception {
		ConnectionBean fixture = new ConnectionBean(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), new ZooKeeperServer());

		long result = fixture.getAvgLatency();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
		assertEquals(0L, result);
	}

	/**
	 * Run the String[] getEphemeralNodes() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:18 PM
	 */
	@Test
	public void testGetEphemeralNodes_1()
		throws Exception {
		ConnectionBean fixture = new ConnectionBean(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), new ZooKeeperServer());

		String[] result = fixture.getEphemeralNodes();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
		assertNotNull(result);
	}

	/**
	 * Run the String[] getEphemeralNodes() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:18 PM
	 */
	@Test
	public void testGetEphemeralNodes_2()
		throws Exception {
		ConnectionBean fixture = new ConnectionBean(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), new ZooKeeperServer());

		String[] result = fixture.getEphemeralNodes();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
		assertNotNull(result);
	}

	/**
	 * Run the String getLastCxid() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:18 PM
	 */
	@Test
	public void testGetLastCxid_1()
		throws Exception {
		ConnectionBean fixture = new ConnectionBean(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), new ZooKeeperServer());

		String result = fixture.getLastCxid();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
		assertNotNull(result);
	}

	/**
	 * Run the long getLastLatency() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:18 PM
	 */
	@Test
	public void testGetLastLatency_1()
		throws Exception {
		ConnectionBean fixture = new ConnectionBean(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), new ZooKeeperServer());

		long result = fixture.getLastLatency();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
		assertEquals(0L, result);
	}

	/**
	 * Run the String getLastOperation() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:18 PM
	 */
	@Test
	public void testGetLastOperation_1()
		throws Exception {
		ConnectionBean fixture = new ConnectionBean(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), new ZooKeeperServer());

		String result = fixture.getLastOperation();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
		assertNotNull(result);
	}

	/**
	 * Run the String getLastResponseTime() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:18 PM
	 */
	@Test
	public void testGetLastResponseTime_1()
		throws Exception {
		ConnectionBean fixture = new ConnectionBean(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), new ZooKeeperServer());

		String result = fixture.getLastResponseTime();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
		assertNotNull(result);
	}

	/**
	 * Run the String getLastZxid() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:18 PM
	 */
	@Test
	public void testGetLastZxid_1()
		throws Exception {
		ConnectionBean fixture = new ConnectionBean(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), new ZooKeeperServer());

		String result = fixture.getLastZxid();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
		assertNotNull(result);
	}

	/**
	 * Run the long getMaxLatency() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:18 PM
	 */
	@Test
	public void testGetMaxLatency_1()
		throws Exception {
		ConnectionBean fixture = new ConnectionBean(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), new ZooKeeperServer());

		long result = fixture.getMaxLatency();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
		assertEquals(0L, result);
	}

	/**
	 * Run the long getMinLatency() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:18 PM
	 */
	@Test
	public void testGetMinLatency_1()
		throws Exception {
		ConnectionBean fixture = new ConnectionBean(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), new ZooKeeperServer());

		long result = fixture.getMinLatency();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
		assertEquals(0L, result);
	}

	/**
	 * Run the String getName() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:18 PM
	 */
	@Test
	public void testGetName_1()
		throws Exception {
		ConnectionBean fixture = new ConnectionBean(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), new ZooKeeperServer());

		String result = fixture.getName();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
		assertNotNull(result);
	}

	/**
	 * Run the long getOutstandingRequests() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:18 PM
	 */
	@Test
	public void testGetOutstandingRequests_1()
		throws Exception {
		ConnectionBean fixture = new ConnectionBean(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), new ZooKeeperServer());

		long result = fixture.getOutstandingRequests();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
		assertEquals(0L, result);
	}

	/**
	 * Run the long getPacketsReceived() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:18 PM
	 */
	@Test
	public void testGetPacketsReceived_1()
		throws Exception {
		ConnectionBean fixture = new ConnectionBean(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), new ZooKeeperServer());

		long result = fixture.getPacketsReceived();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
		assertEquals(0L, result);
	}

	/**
	 * Run the long getPacketsSent() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:18 PM
	 */
	@Test
	public void testGetPacketsSent_1()
		throws Exception {
		ConnectionBean fixture = new ConnectionBean(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), new ZooKeeperServer());

		long result = fixture.getPacketsSent();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
		assertEquals(0L, result);
	}

	/**
	 * Run the String getSessionId() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:18 PM
	 */
	@Test
	public void testGetSessionId_1()
		throws Exception {
		ConnectionBean fixture = new ConnectionBean(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), new ZooKeeperServer());

		String result = fixture.getSessionId();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
		assertNotNull(result);
	}

	/**
	 * Run the int getSessionTimeout() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:18 PM
	 */
	@Test
	public void testGetSessionTimeout_1()
		throws Exception {
		ConnectionBean fixture = new ConnectionBean(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), new ZooKeeperServer());

		int result = fixture.getSessionTimeout();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
		assertEquals(0, result);
	}

	/**
	 * Run the String getSourceIP() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:18 PM
	 */
	@Test
	public void testGetSourceIP_1()
		throws Exception {
		ConnectionBean fixture = new ConnectionBean(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), new ZooKeeperServer());

		String result = fixture.getSourceIP();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
		assertNotNull(result);
	}

	/**
	 * Run the String getSourceIP() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:18 PM
	 */
	@Test
	public void testGetSourceIP_2()
		throws Exception {
		ConnectionBean fixture = new ConnectionBean(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), new ZooKeeperServer());

		String result = fixture.getSourceIP();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
		assertNotNull(result);
	}

	/**
	 * Run the String getStartedTime() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:18 PM
	 */
	@Test
	public void testGetStartedTime_1()
		throws Exception {
		ConnectionBean fixture = new ConnectionBean(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), new ZooKeeperServer());

		String result = fixture.getStartedTime();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
		assertNotNull(result);
	}

	/**
	 * Run the boolean isHidden() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:18 PM
	 */
	@Test
	public void testIsHidden_1()
		throws Exception {
		ConnectionBean fixture = new ConnectionBean(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), new ZooKeeperServer());

		boolean result = fixture.isHidden();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
		assertTrue(result);
	}

	/**
	 * Run the void resetCounters() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:18 PM
	 */
	@Test
	public void testResetCounters_1()
		throws Exception {
		ConnectionBean fixture = new ConnectionBean(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), new ZooKeeperServer());

		fixture.resetCounters();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void terminateConnection() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:18 PM
	 */
	@Test
	public void testTerminateConnection_1()
		throws Exception {
		ConnectionBean fixture = new ConnectionBean(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), new ZooKeeperServer());

		fixture.terminateConnection();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the void terminateSession() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:18 PM
	 */
	@Test
	public void testTerminateSession_1()
		throws Exception {
		ConnectionBean fixture = new ConnectionBean(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), new ZooKeeperServer());

		fixture.terminateSession();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
	}

	/**
	 * Run the String toString() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:18 PM
	 */
	@Test
	public void testToString_1()
		throws Exception {
		ConnectionBean fixture = new ConnectionBean(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), new ZooKeeperServer());

		String result = fixture.toString();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at org.apache.zookeeper.server.NIOServerCnxn.<init>(NIOServerCnxn.java:113)
		assertNotNull(result);
	}

	/**
	 * Run the String toString() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 8:18 PM
	 */
	@Test(expected = java.lang.NullPointerException.class)
	public void testToString_2()
		throws Exception {
		ConnectionBean fixture = new ConnectionBean(new NIOServerCnxn(new ZooKeeperServer(), SocketChannel.open(), (SelectionKey) null, new NIOServerCnxnFactory()), new ZooKeeperServer());

		String result = fixture.toString();

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:18 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 8:18 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 8:18 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(ConnectionBeanTest.class);
	}
}