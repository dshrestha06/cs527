package com.uiuc.org.apache.zookeeper.server.auth;

import java.io.IOException;
import javax.security.auth.callback.Callback;
import javax.security.auth.login.Configuration;
import org.apache.zookeeper.JaasConfiguration;
import org.apache.zookeeper.server.auth.SaslServerCallbackHandler;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>SaslServerCallbackHandlerTest</code> contains tests for the class <code>{@link SaslServerCallbackHandler}</code>.
 *
 * @generatedBy CodePro at 10/16/13 7:33 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class SaslServerCallbackHandlerTest {
	/**
	 * Run the SaslServerCallbackHandler(Configuration) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testSaslServerCallbackHandler_1()
		throws Exception {
		Configuration configuration = new JaasConfiguration();

		SaslServerCallbackHandler result = new SaslServerCallbackHandler(configuration);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the SaslServerCallbackHandler(Configuration) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testSaslServerCallbackHandler_2()
		throws Exception {
		Configuration configuration = new JaasConfiguration();

		SaslServerCallbackHandler result = new SaslServerCallbackHandler(configuration);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the SaslServerCallbackHandler(Configuration) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testSaslServerCallbackHandler_3()
		throws Exception {
		Configuration configuration = new JaasConfiguration();

		SaslServerCallbackHandler result = new SaslServerCallbackHandler(configuration);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the SaslServerCallbackHandler(Configuration) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testSaslServerCallbackHandler_4()
		throws Exception {
		Configuration configuration = new JaasConfiguration();

		SaslServerCallbackHandler result = new SaslServerCallbackHandler(configuration);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the SaslServerCallbackHandler(Configuration) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test(expected = java.io.IOException.class)
	public void testSaslServerCallbackHandler_5()
		throws Exception {
		Configuration configuration = new JaasConfiguration();

		SaslServerCallbackHandler result = new SaslServerCallbackHandler(configuration);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the void handle(Callback[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testHandle_1()
		throws Exception {
		SaslServerCallbackHandler fixture = new SaslServerCallbackHandler(new JaasConfiguration());
		Callback[] callbacks = new Callback[] {};

		fixture.handle(callbacks);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.io.IOException: Could not find a 'Server' entry in this configuration: Server cannot start.
		//       at org.apache.zookeeper.server.auth.SaslServerCallbackHandler.<init>(SaslServerCallbackHandler.java:57)
	}

	/**
	 * Run the void handle(Callback[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testHandle_2()
		throws Exception {
		SaslServerCallbackHandler fixture = new SaslServerCallbackHandler(new JaasConfiguration());
		Callback[] callbacks = new Callback[] {};

		fixture.handle(callbacks);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.io.IOException: Could not find a 'Server' entry in this configuration: Server cannot start.
		//       at org.apache.zookeeper.server.auth.SaslServerCallbackHandler.<init>(SaslServerCallbackHandler.java:57)
	}

	/**
	 * Run the void handle(Callback[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testHandle_3()
		throws Exception {
		SaslServerCallbackHandler fixture = new SaslServerCallbackHandler(new JaasConfiguration());
		Callback[] callbacks = new Callback[] {};

		fixture.handle(callbacks);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.io.IOException: Could not find a 'Server' entry in this configuration: Server cannot start.
		//       at org.apache.zookeeper.server.auth.SaslServerCallbackHandler.<init>(SaslServerCallbackHandler.java:57)
	}

	/**
	 * Run the void handle(Callback[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testHandle_4()
		throws Exception {
		SaslServerCallbackHandler fixture = new SaslServerCallbackHandler(new JaasConfiguration());
		Callback[] callbacks = new Callback[] {};

		fixture.handle(callbacks);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.io.IOException: Could not find a 'Server' entry in this configuration: Server cannot start.
		//       at org.apache.zookeeper.server.auth.SaslServerCallbackHandler.<init>(SaslServerCallbackHandler.java:57)
	}

	/**
	 * Run the void handle(Callback[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testHandle_5()
		throws Exception {
		SaslServerCallbackHandler fixture = new SaslServerCallbackHandler(new JaasConfiguration());
		Callback[] callbacks = new Callback[] {};

		fixture.handle(callbacks);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.io.IOException: Could not find a 'Server' entry in this configuration: Server cannot start.
		//       at org.apache.zookeeper.server.auth.SaslServerCallbackHandler.<init>(SaslServerCallbackHandler.java:57)
	}

	/**
	 * Run the void handle(Callback[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Test
	public void testHandle_6()
		throws Exception {
		SaslServerCallbackHandler fixture = new SaslServerCallbackHandler(new JaasConfiguration());
		Callback[] callbacks = new Callback[] {};

		fixture.handle(callbacks);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.io.IOException: Could not find a 'Server' entry in this configuration: Server cannot start.
		//       at org.apache.zookeeper.server.auth.SaslServerCallbackHandler.<init>(SaslServerCallbackHandler.java:57)
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:33 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(SaslServerCallbackHandlerTest.class);
	}
}