package com.uiuc.com.uiuc.org.apache.zookeeper.server.quorum;

import org.junit.*;
import static org.junit.Assert.*;
import com.uiuc.org.apache.zookeeper.server.quorum.ServerMXBeanTest;

/**
 * The class <code>ServerMXBeanTestTest</code> contains tests for the class <code>{@link ServerMXBeanTest}</code>.
 *
 * @generatedBy CodePro at 10/16/13 7:28 PM
 * @author dshresth
 * @version $Revision: 1.0 $
 */
public class ServerMXBeanTestTest {
	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:28 PM
	 */
	@Test
	public void testMain_1()
		throws Exception {
		String[] args = new String[] {};

		ServerMXBeanTest.main(args);

		// add additional test code here
	}

	/**
	 * Run the void setUp() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:28 PM
	 */
	@Test
	public void testSetUp_1()
		throws Exception {
		ServerMXBeanTest fixture = new ServerMXBeanTest();

		fixture.setUp();

		// add additional test code here
	}

	/**
	 * Run the void tearDown() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 10/16/13 7:28 PM
	 */
	@Test
	public void testTearDown_1()
		throws Exception {
		ServerMXBeanTest fixture = new ServerMXBeanTest();

		fixture.tearDown();

		// add additional test code here
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:28 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 10/16/13 7:28 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 10/16/13 7:28 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(ServerMXBeanTestTest.class);
	}
}