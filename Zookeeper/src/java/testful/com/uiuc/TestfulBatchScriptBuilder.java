package com.uiuc;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.zookeeper.AsyncCallback;

/**
 * 
 *
 */
public class TestfulBatchScriptBuilder {
	public static void main(String[] args) throws Exception {
		File file = new File(AsyncCallback.class.getResource(
				"AsyncCallback.class").getFile());
		File root = file.getParentFile();

		// tweak this to make it work in linux
		root = new File(root.getAbsolutePath().replace("bin", "src/java/main"));

		List<String> fileList = buildFileListRecursively(root);

		FileWriter fw = new FileWriter(new File("TestfulInstrument.bat"));
		FileWriter fw1 = new FileWriter(new File("TestfulCut.bat"));

		for (String fileName : fileList) {
			String trimmedFile = fileName.substring(0, fileName.length() - 5);
			String instrumentLine = "java -jar instrumenter-2.0.0.alpha.jar -dirSource ./src/java/main "
					+ trimmedFile;
			fw.write(instrumentLine + "\r\n");

			String testfulLine = "java -jar testful-2.0.0.alpha.jar -time 10 -cut "
					+ trimmedFile;
			fw1.write(testfulLine + "\r\n");
		}

		fw.flush();
		fw.close();
		fw1.flush();
		fw1.close();
	}

	private static List<String> buildFileListRecursively(File root) {
		List<String> fileList = new ArrayList<String>();
		File[] children = root.listFiles();
		for (File child : children) {
			if (child.isDirectory())
				fileList.addAll(buildFileListRecursively(child));
			else if (child.getName().endsWith("java")) {
				fileList.add(getStringRepresentation(child));
			}
		}
		return fileList;
	}

	private static String getStringRepresentation(File file) {
		StringBuffer sb = new StringBuffer();
		do {
			sb.insert(0, file.getName());
			sb.insert(0, ".");
			file = file.getParentFile();
		} while (!file.getName().equalsIgnoreCase("org"));

		sb.insert(0, "org");
		return sb.toString();
	}
}
