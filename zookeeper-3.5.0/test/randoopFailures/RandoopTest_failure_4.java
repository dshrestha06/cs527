package randoopFailures;

import junit.framework.*;

public class RandoopTest_failure_4 extends TestCase {

  public static boolean debug = false;

  public void test1() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest_failure_4.test1");


    org.apache.zookeeper.server.quorum.QuorumPeer var0 = new org.apache.zookeeper.server.quorum.QuorumPeer();
    org.apache.zookeeper.server.quorum.LocalPeerBean var1 = new org.apache.zookeeper.server.quorum.LocalPeerBean(var0);
    org.apache.zookeeper.server.quorum.Vote var6 = new org.apache.zookeeper.server.quorum.Vote(0L, (-1L), 10L, 10L);
    long var7 = var6.getPeerEpoch();
    var0.setCurrentVote(var6);
    var0.run();

  }

}
