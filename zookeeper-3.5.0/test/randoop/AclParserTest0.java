package randoop;

import junit.framework.*;

public class AclParserTest0 extends TestCase {

  public static boolean debug = false;

  public void test1() throws Throwable {

    if (debug) System.out.printf("%nAclParserTest0.test1");


    java.util.List var1 = org.apache.zookeeper.cli.AclParser.parse("hi!");
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var1);

  }

  public void test2() throws Throwable {

    if (debug) System.out.printf("%nAclParserTest0.test2");


    org.apache.zookeeper.cli.AclParser var0 = new org.apache.zookeeper.cli.AclParser();

  }

  public void test3() throws Throwable {

    if (debug) System.out.printf("%nAclParserTest0.test3");


    java.util.List var1 = org.apache.zookeeper.cli.AclParser.parse("");
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var1);

  }

}
